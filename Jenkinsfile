pipeline {
    // Use Hybris 6.7.0.1 build agent
    agent {
        label 'hybris6701'
    }

    triggers {
        // Need to poll every minute until this is added:
        // https://issues.jenkins-ci.org/browse/JENKINS-44309
        pollSCM('H H/2 * * *')
    }

    // Default build options:
    options {
        // Cancel build if it exceeds this time:
        timeout(time: 1, unit: 'HOURS')

        // Show timestamps in build output
        //timestamps()

        // Only keep console output (& artifacts) of limited amount of builds:
        buildDiscarder(logRotator(numToKeepStr: '45'))

        // If you want to stop if a JUnit fails (=build unstable):
        skipStagesAfterUnstable()

        // Colorize non-blueocean jenkins logs:
        ansiColor('xterm')

    }

    // Variables:
    environment {
        recipe_name = "publicsector"
    }


    // Build stages:
    stages {
        stage('Copy repo to /data') {
            steps {
                sh "cp -rv * /data/"
            }
        }
        
        stage('Install recipe') {
            steps {
                sh """
                    cd /data/installer
                    ./install.sh -r ${recipe_name}
                """
            }
        }
        stage('Run ant clean all') {
            steps {
                sh """
                    cd /data/hybris/bin/platform
                    . ./setantenv.sh
                    ant clean all
                """
            }
        }
        stage('Run ant unittests') {
            steps {
                sh '''
                    cd /data/hybris/bin/platform
                    . ./setantenv.sh
                    ant unittests -Dtestclasses.packages "de.hybris.platform.testframework.*"
                '''
                // Copy test result to workspace as the junit plugin needs it there
                sh """
                    cp /data/hybris/log/junit/*.xml $WORKSPACE/
                """

                junit 'TESTS*.xml'
            }
        }
    }

    post {
        unstable {
            emailext (
                subject: "Unit test failed: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                body: """<p>Unit test failed: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>""",
                recipientProviders: [[$class: 'FailingTestSuspectsRecipientProvider']]
            )

        }

        failure {
            script {
                currentBuild.result = 'FAILURE'
            }
            emailext (
                subject: "Build failed: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                body: """<p>Build failed: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>""",
                recipientProviders: [[$class: 'DevelopersRecipientProvider']]
            )
        }

    }
}
            
