To enable environment specific configuration copy and rename environment specific custom.properties prior to installing the recipe

cp ./environments/[dev|test|stage].custom.properties custom.properties
