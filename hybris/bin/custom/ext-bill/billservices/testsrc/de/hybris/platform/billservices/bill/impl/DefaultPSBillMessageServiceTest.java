/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billservices.dao.impl.DefaultPSBillMessageDao;
import de.hybris.platform.billservices.model.PSBillMessageModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit test for {@link DefaultPSBillMessageService}
 */
@UnitTest
public class DefaultPSBillMessageServiceTest
{
	private static final String BILL_TYPE_CODE = "123456789012";

	@InjectMocks
	private DefaultPSBillMessageService psBillMessageService;

	@Mock
	private DefaultPSBillMessageDao psBillMessageDao;

	@Mock
	private PSBillMessageModel billMessage;

	/**
	 * Runs before test method.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		psBillMessageService = new DefaultPSBillMessageService();
		psBillMessageService.setPsBillMessageDao(psBillMessageDao);

	}

	@Test
	public void testGetMessagesWithNullBillType()
	{
		Assert.assertEquals(0, psBillMessageService.getMessagesByBillType(null).size());
	}

	@Test
	public void testGetMessagesWithBillType()
	{
		final List<PSBillMessageModel> messages = new ArrayList<PSBillMessageModel>();
		messages.add(billMessage);
		Mockito.when(psBillMessageDao.findMessagesByBillType(BILL_TYPE_CODE)).thenReturn(messages);
		Assert.assertEquals(1, psBillMessageService.getMessagesByBillType(BILL_TYPE_CODE).size());
	}
}
