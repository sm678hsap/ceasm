/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billservices.dao.impl.DefaultPSBillPaymentDao;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.billservices.strategies.impl.DefaultPSBillPushStrategy;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Unit Test class for {@link DefaultPSBillPaymentService}
 */
@UnitTest
public class DefaultPSBillPaymentServiceTest {
	@SuppressWarnings("javadoc")
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	private static final Long CUSTOMER_PK = 100000002L;
	private static final String CUSTOMER_UID = "jane.citizentest@stateofrosebud.com";
	private static final String BILL_CODE = "COUNCILRATE";
	private static final String BILL_ID = "123456789";
	private static final String LAST_NAME = "Citizen";
	private static final String BILL_TYPE_CODE = "TXR001";

	@InjectMocks
	private DefaultPSBillPaymentService psBillPaymentService;

	@Mock
	private DefaultPSBillPaymentDao psBillPaymentDao;
	@Mock
	private DefaultPSBillPushStrategy psBillPushStrategy;
	@Mock
	private ModelService modelService;
	@Mock
	private PaymentService paymentService;
	@Mock
	private PSBillPaymentModel psBillPayment;
	@Mock
	private DefaultPSBillMessageService psBillMessageService;
	@Mock
	private UserService userService;
	@Mock
	private EventService eventService;

	/*----- santhosh data ----*/

	@Mock
	private CustomerModel customerModel;

	@Mock
	private PageableData pageableData;

	@BeforeClass
	public static void tenantStuff() {
		Registry.setCurrentTenantByID("junit");
	}

	/**
	 * Runs before test method.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		psBillPaymentService = new DefaultPSBillPaymentService();
		psBillPaymentService.setPsBillPaymentDao(psBillPaymentDao);
		psBillPaymentService.setModelService(modelService);
		psBillPaymentService.setPaymentService(paymentService);
		psBillPaymentService.setPsBillPushStrategy(psBillPushStrategy);
		psBillPaymentService.setPsBillMessageService(psBillMessageService);
		psBillPaymentService.setUserService(userService);
		psBillPaymentService.setEventService(eventService);

		Mockito.when(psBillPayment.getLastName()).thenReturn(LAST_NAME);
		Mockito.when(psBillPayment.getBillId()).thenReturn(BILL_ID);
		Mockito.when(psBillPayment.getCode()).thenReturn(BILL_CODE);
	}

	protected static class MockCustomerModel extends CustomerModel {
		@Override
		public de.hybris.platform.core.PK getPk() {
			return de.hybris.platform.core.PK.fromLong(CUSTOMER_PK);
		}
	}

	/**
	 * Test get a null bill .
	 */
	@Test
	public void testGetNullBill() {
		final PSBillPaymentModel billPayment = psBillPaymentService.getBill(BILL_ID, LAST_NAME);
		final PSBillPaymentModel billPayment2 = psBillPaymentService.getBill(BILL_CODE);
		Assert.assertNull(billPayment);
		Assert.assertNull(billPayment2);
	}

	@Test
	public void testGetBillByIdAndLastName() {
		Mockito.when(psBillPushStrategy.getBill(BILL_ID, LAST_NAME)).thenReturn(psBillPayment);
		Assert.assertEquals(BILL_CODE, psBillPaymentService.getBill(BILL_ID, LAST_NAME).getCode());
	}

	@Test
	public void testGetBillByCode() {
		Mockito.when(psBillPushStrategy.getBill(BILL_CODE)).thenReturn(psBillPayment);
		Assert.assertEquals(BILL_ID, psBillPaymentService.getBill(BILL_CODE).getBillId());
	}

	/**
	 * Test get customer bills .
	 */
	@Test
	public void testGetBillsByCustomer() {
		final CustomerModel customer = new MockCustomerModel();
		customer.setUid(CUSTOMER_UID);

		final PSBillPaymentModel psBillPaymentModel = mock(PSBillPaymentModel.class);
		Mockito.when(psBillPaymentModel.getCode()).thenReturn(BILL_CODE);

		final List<PSBillPaymentModel> listBillPayments = new ArrayList<PSBillPaymentModel>();
		listBillPayments.add(psBillPaymentModel);

		Mockito.when(psBillPushStrategy.getBills(customer)).thenReturn(listBillPayments);
		final List<PSBillPaymentModel> billPayments = psBillPaymentService.getBills(customer);
		Assert.assertEquals(listBillPayments.size(), billPayments.size());
	}

	/**
	 * Test get customer bills .
	 */
	@Test
	public void testGetEmptyBillsByCustomer() {
		final CustomerModel customer = new MockCustomerModel();
		customer.setUid(CUSTOMER_UID);

		final PSBillPaymentModel psBillPayment = mock(PSBillPaymentModel.class);
		Mockito.when(psBillPayment.getCode()).thenReturn(BILL_CODE);

		final List<PSBillPaymentModel> billPayments = psBillPaymentService.getBills(customer);
		Assert.assertEquals(0, billPayments.size());
	}

	/**
	 * Test create bills .
	 */
	@Test
	public void testCreateBill() {
		final PSBillPaymentModel psBillPaymentModel = new PSBillPaymentModel();
		psBillPaymentModel.setCode("TESTCODE");
		psBillPaymentModel.setBillId("123");
		psBillPaymentModel.setBillPaymentStatus(BillPaymentStatus.UNPAID);
		psBillPaymentModel.setBillDate(new Date("01/07/2016"));
		psBillPaymentModel.setBillDueDate(new Date("31/07/2016"));
		psBillPaymentModel.setTotalBillAmount(BigDecimal.valueOf(100.00));
		psBillPaymentModel.setOutstandingBillAmount(BigDecimal.valueOf(10.00));
		psBillPaymentModel.setLastName(LAST_NAME);
		final List<PSBillPaymentModel> billPayments = Arrays.asList(psBillPaymentModel);
		psBillPaymentService.createOrUpdateBills(billPayments);

		final PSBillPaymentModel billPayment = psBillPaymentService.getBill("123", LAST_NAME);
		Assert.assertNull(billPayment);
	}

	/**
	 * Test on setting of payment information to bill.
	 *
	 */
	@Test
	public void testSetPaymentInfo() {
		final PSBillPaymentModel psBillPaymentModel = new PSBillPaymentModel();
		final PaymentInfoModel paymentInfo = new PaymentInfoModel();
		psBillPaymentService.setPaymentInfo(psBillPaymentModel, paymentInfo);
		Assert.assertNotNull(psBillPaymentModel.getPaymentInfo());
	}

	/**
	 * Test on processing payment of a bill payment transaction.
	 *
	 * @throws Exception
	 */
	@Test
	public void testProcessPayment() throws Exception // NOSONAR
	{
		final String billCode = "TestBillCode";
		final String merchantTransactionCode = "TestMerchantCode";
		final String securityCode = "TestSecurityCode";
		final String paymentProvider = "TestPaymentProvider";
		final PSBillPaymentModel psBillPaymentModel = new PSBillPaymentModel();
		psBillPaymentModel.setCode(billCode);
		psBillPaymentModel.setOutstandingBillAmount(BigDecimal.valueOf(100.0));

		Mockito.when(psBillPaymentService.getBill(billCode)).thenReturn(psBillPaymentModel);

		final BigDecimal amount = BigDecimal.valueOf(10.0);
		final Currency currency = Currency.getInstance("USD");
		final CreditCardPaymentInfoModel ccPaymentInfoModel = new CreditCardPaymentInfoModel();
		final AddressModel billingAddress = new AddressModel();
		ccPaymentInfoModel.setBillingAddress(billingAddress);
		ccPaymentInfoModel.setSubscriptionId("TestSubscriptionId");
		final PaymentTransactionEntryModel transactionEntryModel = new PaymentTransactionEntryModel();
		transactionEntryModel.setTransactionStatus(TransactionStatus.ACCEPTED.name());
		final PaymentTransactionModel paymentTransaction = new PaymentTransactionModel();
		paymentTransaction.setPlannedAmount(BigDecimal.valueOf(150.0));
		transactionEntryModel.setPaymentTransaction(paymentTransaction);

		Mockito.when(paymentService.authorize(merchantTransactionCode, amount, currency,
				ccPaymentInfoModel.getBillingAddress(), ccPaymentInfoModel.getSubscriptionId(), securityCode,
				paymentProvider)).thenReturn(transactionEntryModel);

		final Set<PaymentTransactionModel> newBillPaymentTransactions = new HashSet<>();
		psBillPaymentModel.setPaymentTransactions(newBillPaymentTransactions);
		final PaymentTransactionEntryModel txnEntry = new PaymentTransactionEntryModel();
		txnEntry.setTransactionStatus(TransactionStatus.ACCEPTED.name());

		Mockito.when(paymentService.capture(paymentTransaction)).thenReturn(txnEntry);

		setCustomerAndPerformAuthentication();

		psBillPaymentService.processPayment(billCode, ccPaymentInfoModel, amount, currency, securityCode,
				paymentProvider, merchantTransactionCode);

		Assert.assertEquals(0, paymentTransaction.getOutstandingAmount().doubleValue(), 0.0);
		Assert.assertEquals(50.0, paymentTransaction.getOverPaidAmount().doubleValue(), 0.0);
	}

	private void setCustomerAndPerformAuthentication() {
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setType(CustomerType.GUEST);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);

		Config.setParameter("payment.source.internal", "internal");
		final GrantedAuthority auth = new SimpleGrantedAuthority("admin");
		final ArrayList authList = new ArrayList<>();
		authList.add(auth);

		final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
				customerModel.getUid(), null, authList);
		newAuthentication.setDetails(customerModel);
		SecurityContextHolder.getContext().setAuthentication(newAuthentication);
	}

	@Test
	public void testGetMessagesByBill() {
		final PSBillTypeModel type = mock(PSBillTypeModel.class);
		Mockito.when(type.getCode()).thenReturn(BILL_TYPE_CODE);

		final PSBillMessageModel message = mock(PSBillMessageModel.class);
		Mockito.when(message.getPsBillType()).thenReturn(type);

		final PSBillPaymentModel bill = mock(PSBillPaymentModel.class);
		Mockito.when(bill.getCode()).thenReturn(BILL_CODE);
		Mockito.when(bill.getBillType()).thenReturn(type);

		final List<PSBillMessageModel> messages = new ArrayList<PSBillMessageModel>();
		messages.add(message);

		Mockito.when(psBillMessageService.getMessagesByBillType(BILL_TYPE_CODE)).thenReturn(messages);
		Assert.assertNotNull(psBillPaymentService.getMessagesForBill(bill));
		Assert.assertEquals(1, psBillPaymentService.getMessagesForBill(bill).size());
	}

	@Test
	public void testGetMessagesByBillForEmptyBillType() {
		final PSBillPaymentModel bill = mock(PSBillPaymentModel.class);
		Mockito.when(bill.getCode()).thenReturn(BILL_CODE);
		Mockito.when(bill.getBillType()).thenReturn(null);

		Assert.assertEquals(0, psBillPaymentService.getMessagesForBill(bill).size());
	}

	@Test
	public void testSetBillType() {
		final PSBillTypeModel billType = new PSBillTypeModel();
		billType.setCode(BILL_TYPE_CODE);

		final PSBillPaymentModel bill = new PSBillPaymentModel();

		psBillPaymentService.setBillType(bill, billType);
		verify(modelService, times(1)).save(any(PSBillPaymentModel.class));
	}

	/**
	 * Test on setting of bill receipt fields.
	 *
	 */
	@Test
	public void testSetBillReceiptFields() {
		final PaymentTransactionModel paymentTransactionModel = new PaymentTransactionModel();
		final CustomerModel customerModel = new CustomerModel();

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		psBillPaymentService.setBillReceiptFields(paymentTransactionModel, "internal");

		Assert.assertNotNull(paymentTransactionModel.getPaidBy());
		Assert.assertEquals("internal", paymentTransactionModel.getPaymentSource());
	}

	@Test
	public void testSetBillReceiptFieldsForAnonymousUser() {
		final PaymentTransactionModel paymentTransactionModel = new PaymentTransactionModel();
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setUid("anonymous");

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(Boolean.valueOf(userService.isAnonymousUser(customerModel))).thenReturn(Boolean.valueOf(true));
		psBillPaymentService.setBillReceiptFields(paymentTransactionModel, "internal");

		Assert.assertNull(paymentTransactionModel.getPaidBy());
		Assert.assertEquals("internal", paymentTransactionModel.getPaymentSource());
	}

	@Test
	public void testgetBillsByStatusList() {

		final BillPaymentStatus billPaymentStatus = BillPaymentStatus.PAID;
		final List<BillPaymentStatus> listData1 = new ArrayList();
		listData1.add(billPaymentStatus);
		final SearchPageData<PSBillPaymentModel> searchPageData = new SearchPageData<>();

		Mockito.when(psBillPaymentDao.getBillsByStatus(customerModel, pageableData, listData1))
				.thenReturn(searchPageData);
	}

}
