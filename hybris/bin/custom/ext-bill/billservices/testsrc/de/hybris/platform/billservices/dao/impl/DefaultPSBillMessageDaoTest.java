/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


/**
 * Unit test for {@link DefaultPSBillMessageDao}
 */
@UnitTest
public class DefaultPSBillMessageDaoTest
{
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	private DefaultPSBillMessageDao psBillMessageDao;

	/**
	 * Runs before test method.
	 */
	@Before
	public void setUp()
	{
		psBillMessageDao = new DefaultPSBillMessageDao();
	}

	@Test
	public void testFindMessagesByNullBillType()
	{
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("bill pk code must not be null");

		psBillMessageDao.findMessagesByBillType(null);
	}
}
