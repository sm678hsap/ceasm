/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.strategies.impl;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billservices.dao.PSBillPaymentDao;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit test for {@link DefaultPSBillPushStrategy}
 */
@UnitTest
public class DefaultPSBillPushStrategyTest
{
	private static final String BILL_CODE = "COUNCILRATE";
	private static final String CUSTOMER_UID = "jane.citizentest@stateofrosebud.com";
	private static final String BILL_ID = "123456789";
	private static final String LAST_NAME = "Citizen";
	private static final Long CUSTOMER_PK = 100000002L;

	@InjectMocks
	private DefaultPSBillPushStrategy psBillPushStrategy;

	@Mock
	private PSBillPaymentDao psBillPaymentDao;

	@Mock
	private PSBillPaymentModel psBillPayment;

	/**
	 * Runs before test method.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		psBillPushStrategy = new DefaultPSBillPushStrategy();
		psBillPushStrategy.setPsBillPaymentDao(psBillPaymentDao);

		Mockito.when(psBillPayment.getLastName()).thenReturn(LAST_NAME);
		Mockito.when(psBillPayment.getBillId()).thenReturn(BILL_ID);
		Mockito.when(psBillPayment.getCode()).thenReturn(BILL_CODE);
	}

	/**
	 * Test get a null bill .
	 */
	@Test
	public void testGetNullBill()
	{
		final PSBillPaymentModel billPayment = psBillPushStrategy.getBill(BILL_ID, LAST_NAME);
		final PSBillPaymentModel billPayment2 = psBillPushStrategy.getBill(BILL_CODE);
		Assert.assertNull(billPayment);
		Assert.assertNull(billPayment2);
	}

	@Test
	public void testGetBillByIdAndLastName()
	{
		Mockito.when(psBillPaymentDao.getBill(BILL_ID, LAST_NAME)).thenReturn(psBillPayment);
		Assert.assertEquals(BILL_CODE, psBillPushStrategy.getBill(BILL_ID, LAST_NAME).getCode());
	}

	@Test
	public void testGetBillByCode()
	{
		Mockito.when(psBillPaymentDao.getBill(BILL_CODE)).thenReturn(psBillPayment);
		Assert.assertEquals(BILL_ID, psBillPushStrategy.getBill(BILL_CODE).getBillId());
	}

	/**
	 * Test get customer bills .
	 */
	@Test
	public void testGetBillsByCustomer()
	{

		final CustomerModel customer = new MockCustomerModel();
		customer.setUid(CUSTOMER_UID);

		final PSBillPaymentModel psBillPaymentModel = mock(PSBillPaymentModel.class);
		Mockito.when(psBillPaymentModel.getCode()).thenReturn(BILL_CODE);

		final List<PSBillPaymentModel> listBillPayments = new ArrayList<PSBillPaymentModel>();
		listBillPayments.add(psBillPaymentModel);

		Mockito.when(psBillPaymentDao.getBills(CUSTOMER_PK.toString())).thenReturn(listBillPayments);
		final List<PSBillPaymentModel> billPayments = psBillPushStrategy.getBills(customer);
		Assert.assertEquals(listBillPayments.size(), billPayments.size());
	}

	/**
	 * Test get customer bills .
	 */
	@Test
	public void testGetEmptyBillsByCustomer()
	{
		final CustomerModel customer = new MockCustomerModel();
		customer.setUid(CUSTOMER_UID);

		final PSBillPaymentModel psBillPayment = mock(PSBillPaymentModel.class);
		Mockito.when(psBillPayment.getCode()).thenReturn(BILL_CODE);

		final List<PSBillPaymentModel> billPayments = psBillPushStrategy.getBills(customer);
		Assert.assertEquals(0, billPayments.size());
	}

	protected static class MockCustomerModel extends CustomerModel
	{
		@Override
		public de.hybris.platform.core.PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(CUSTOMER_PK);
		}
	}
}
