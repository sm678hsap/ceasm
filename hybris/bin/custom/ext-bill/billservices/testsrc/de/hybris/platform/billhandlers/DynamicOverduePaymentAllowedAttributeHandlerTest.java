/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billhandlers;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test suite for {@link DynamicOverduePaymentAllowedAttributeHandler}
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DynamicOverduePaymentAllowedAttributeHandlerTest {

	private static final String LATE_PAYMENT_NOT_ALLOWED = "LATE_PAYMENT_NOT_ALLOWED";

	@InjectMocks
	private DynamicOverduePaymentAllowedAttributeHandler dynamicOverduePaymentAllowedAttributeHandler;
	@Mock
	private PSBillTypeModel psBillTypeModel;

	@Mock
	private PSBillMessageModel psBillMessageModel;

	@Mock
	private de.hybris.platform.billservices.enums.PSBillMessageCondition psBillMessageCondition;

	@Test
	public void testGet() {
		final Collection<PSBillMessageModel> psBillMessageModels = new ArrayList<>();
		psBillMessageModels.add(psBillMessageModel);
		given(psBillTypeModel.getPsBillMessages()).willReturn(psBillMessageModels);
		given(psBillMessageModel.getMessageCondition()).willReturn(psBillMessageCondition);
		given(psBillMessageModel.getMessageCondition().getCode()).willReturn(LATE_PAYMENT_NOT_ALLOWED);
		dynamicOverduePaymentAllowedAttributeHandler.get(psBillTypeModel);
	}
}
