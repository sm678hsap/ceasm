/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billservices.dao.PSBillTypeDao;
import de.hybris.platform.billservices.model.PSBillTypeModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit test for {@link DefaultPSBillTypeService}
 */
@UnitTest
public class DefaultPSBillTypeServiceTest
{
	private static final String BILL_TYPE_CODE = "CouncilRate";
	private static final String BILL_PK = "123456789012";

	@InjectMocks
	private DefaultPSBillTypeService psBillTypeService;

	@Mock
	private PSBillTypeDao psBillTypeDao;

	@Mock
	private PSBillTypeModel billType;

	/**
	 * Runs before test method.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		psBillTypeService = new DefaultPSBillTypeService();
		psBillTypeService.setPsBillTypeDao(psBillTypeDao);

		Mockito.when(billType.getCode()).thenReturn(BILL_TYPE_CODE);
	}

	@Test
	public void testFindBillTypeOnNullCode()
	{
		Assert.assertNull(psBillTypeService.getBillType(null));
	}

	@Test
	public void testFindBillType()
	{
		Mockito.when(psBillTypeDao.findBillType(BILL_TYPE_CODE)).thenReturn(billType);
		Assert.assertEquals(billType, psBillTypeService.getBillType(BILL_TYPE_CODE));
	}

	@Test
	public void testFindBillTypeByNullBill()
	{
		Assert.assertNull(psBillTypeService.getBillType(null));
	}

	@Test
	public void testFindBillTypeByBill()
	{
		Mockito.when(psBillTypeDao.findBillTypeByBill(BILL_PK)).thenReturn(billType);
		Assert.assertEquals(billType, psBillTypeService.getBillTypeByBill(BILL_PK));
	}
}
