/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill;

import de.hybris.platform.billservices.model.PSBillTypeModel;


/**
 * Interface class for PSBillType implementation methods.
 */
public interface PSBillTypeService
{
	/**
	 * Gets bill type by code.
	 *
	 * @param code
	 *           String the bill type code
	 * @return PSBillTypeModel
	 */
	PSBillTypeModel getBillType(final String code);

	/**
	 * Gets bill type by bill pk.
	 *
	 * @param billPk
	 *           String the bill's PK.
	 * @return PSBillTypeModel
	 */
	PSBillTypeModel getBillTypeByBill(final String billPk);
}
