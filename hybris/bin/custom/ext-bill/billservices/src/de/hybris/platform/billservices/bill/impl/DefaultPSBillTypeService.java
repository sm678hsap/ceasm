/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill.impl;

import de.hybris.platform.billservices.bill.PSBillTypeService;
import de.hybris.platform.billservices.dao.PSBillTypeDao;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation class for {@link PSBillTypeService}
 */
public class DefaultPSBillTypeService implements PSBillTypeService
{
	private PSBillTypeDao psBillTypeDao;
	private ModelService modelService;

	@Override
	public PSBillTypeModel getBillType(final String code)
	{
		return getPsBillTypeDao().findBillType(code);
	}

	@Override
	public PSBillTypeModel getBillTypeByBill(final String billPk)
	{
		return getPsBillTypeDao().findBillTypeByBill(billPk);
	}

	protected PSBillTypeDao getPsBillTypeDao()
	{
		return psBillTypeDao;
	}

	@Required
	public void setPsBillTypeDao(final PSBillTypeDao psBillTypeDao)
	{
		this.psBillTypeDao = psBillTypeDao;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
