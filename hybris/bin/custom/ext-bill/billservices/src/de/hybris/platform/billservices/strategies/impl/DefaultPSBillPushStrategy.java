/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.strategies.impl;

import de.hybris.platform.billservices.dao.PSBillPaymentDao;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.strategies.PSBillPushStrategy;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation for {@link PSBillPushStrategy}
 */
public class DefaultPSBillPushStrategy implements PSBillPushStrategy
{
	private PSBillPaymentDao psBillPaymentDao;

	@Override
	public List<PSBillPaymentModel> getBills(final CustomerModel customer)
	{
		if (customer != null)
		{
			return getPsBillPaymentDao().getBills(customer.getPk().toString());
		}
		return Collections.<PSBillPaymentModel> emptyList();
	}

	@Override
	public PSBillPaymentModel getBill(final String billId, final String lastName)
	{
		return getPsBillPaymentDao().getBill(billId, lastName);
	}

	@Override
	public PSBillPaymentModel getBill(final String code)
	{
		return getPsBillPaymentDao().getBill(code);
	}

	protected PSBillPaymentDao getPsBillPaymentDao()
	{
		return psBillPaymentDao;
	}

	@Required
	public void setPsBillPaymentDao(final PSBillPaymentDao psBillPaymentDao)
	{
		this.psBillPaymentDao = psBillPaymentDao;
	}
}
