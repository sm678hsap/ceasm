/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.strategies;

import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


/**
 * Interface for retrieving bills and pushing the data to a third party provider.
 */
public interface PSBillPushStrategy
{
	/**
	 * Get all customer's bills
	 *
	 * @param customer
	 *           Customer
	 * @return List of Bill Payments
	 */
	List<PSBillPaymentModel> getBills(CustomerModel customer);

	/**
	 * Get a bill from bill Id and customer last name
	 *
	 * @param billId
	 *           Biller Id
	 * @param lastName
	 *           Customer Last Name
	 * @return Bill Payment
	 */
	PSBillPaymentModel getBill(String billId, String lastName);

	/**
	 * Get a bill from bill code
	 *
	 * @param code
	 *           Biller code
	 * @return Bill Payment
	 */
	PSBillPaymentModel getBill(String code);
}
