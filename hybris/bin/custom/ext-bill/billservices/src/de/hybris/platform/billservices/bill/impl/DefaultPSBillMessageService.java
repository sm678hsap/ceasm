/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill.impl;

import de.hybris.platform.billservices.bill.PSBillMessageService;
import de.hybris.platform.billservices.dao.PSBillMessageDao;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation class for {@link PSBillMessageService}
 */
public class DefaultPSBillMessageService implements PSBillMessageService
{
	private PSBillMessageDao psBillMessageDao;
	private ModelService modelService;

	@Override
	public List<PSBillMessageModel> getMessagesByBillType(final String billTypeCode)
	{
		return getPsBillMessageDao().findMessagesByBillType(billTypeCode);
	}

	protected PSBillMessageDao getPsBillMessageDao()
	{
		return psBillMessageDao;
	}

	@Required
	public void setPsBillMessageDao(final PSBillMessageDao psBillMessageDao)
	{
		this.psBillMessageDao = psBillMessageDao;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
