/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billservices.substitute.impl;

import de.hybris.platform.billservices.substitute.PSBillExtensionSubstitutionService;

import java.util.Map;


/**
 * DefaultExtensionSubsitutionService
 */
public class DefaultPSBillExtensionSubsitutionService implements PSBillExtensionSubstitutionService
{
	private Map<String, String> extensionSubstitutionMap;

	@Override
	public String getSubstitutedExtension(final String extension)
	{
		final String sub = extensionSubstitutionMap.get(extension);

		if (sub == null || sub.isEmpty())
		{
			return extension;
		}

		return sub;
	}

	protected Map<String, String> getExtensionSubstitutionMap()
	{
		return extensionSubstitutionMap;
	}

	public void setExtensionSubstitutionMap(final Map<String, String> extensionSubstitutionMap)
	{
		this.extensionSubstitutionMap = extensionSubstitutionMap;
	}

}
