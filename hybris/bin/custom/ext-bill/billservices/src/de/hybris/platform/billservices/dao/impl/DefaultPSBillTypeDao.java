/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.dao.impl;

import de.hybris.platform.billservices.dao.PSBillTypeDao;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.Map;


/**
 * Implementation class of {@link PSBillTypeDao }
 */
public class DefaultPSBillTypeDao extends AbstractItemDao implements PSBillTypeDao
{
	protected static final String SELECTCLAUSE = "SELECT {" + PSBillTypeModel.PK + "} FROM {" + PSBillTypeModel._TYPECODE
			+ "! AS p} ";

	protected static final String PSBILLTYPE_QUERY_BYCODE = SELECTCLAUSE + "WHERE  { p:" + PSBillTypeModel.CODE + "} = ?code";

	protected static final String PSBILLTYPE_QUERY_BY_BILL = "SELECT {" + PSBillTypeModel.PK + "} " + "FROM {"
			+ PSBillPaymentModel._TYPECODE + "! AS b " + "WHERE {b:" + PSBillPaymentModel.PK + "}=?billPk";


	@Override
	public PSBillTypeModel findBillType(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "bill type code must not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put("code", code);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PSBILLTYPE_QUERY_BYCODE, params);
		final SearchResult<PSBillTypeModel> result = getFlexibleSearchService().search(fsq);

		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

	@Override
	public PSBillTypeModel findBillTypeByBill(final String billPk)
	{
		ServicesUtil.validateParameterNotNull(billPk, "billPk code must not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put("billPk", billPk);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PSBILLTYPE_QUERY_BY_BILL, params);
		final SearchResult<PSBillTypeModel> result = getFlexibleSearchService().search(fsq);

		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}
}
