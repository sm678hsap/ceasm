/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.dao;

import de.hybris.platform.billservices.model.PSBillTypeModel;


/**
 * Interface class for PSBillType DAO implementation.
 */
public interface PSBillTypeDao
{
	/**
	 * Gets bill type by code.
	 *
	 * @param code
	 *           the bill type code
	 *
	 * @return PSBillTypeModel
	 */
	PSBillTypeModel findBillType(final String code);

	/**
	 * Gets bill type by bill pk.
	 *
	 * @param billPk
	 *           the Bill's PK.
	 * @return PSBillTypeModel
	 */
	PSBillTypeModel findBillTypeByBill(final String billPk);
}
