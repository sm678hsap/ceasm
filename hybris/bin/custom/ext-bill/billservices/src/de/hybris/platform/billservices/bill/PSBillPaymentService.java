/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill;

import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;


/**
 * Interface class for Public Sector Bill Payment.
 */
public interface PSBillPaymentService
{
	/**
	 * Create & Update bills
	 *
	 * @param psBillPayments
	 *           Bill Payment
	 */
	void createOrUpdateBills(List<PSBillPaymentModel> psBillPayments);

	/**
	 * Get all customer's bills
	 *
	 * @param customer
	 *           Customer
	 * @return List of Bill Payments
	 */
	List<PSBillPaymentModel> getBills(CustomerModel customer);

	/**
	 * Get a bill from bill Id and customer last name
	 *
	 * @param billId
	 *           Biller Id
	 * @param lastName
	 *           Customer Last Name
	 * @return Bill Payment
	 */
	PSBillPaymentModel getBill(String billId, String lastName);

	/**
	 * Get a bill from bill code
	 *
	 * @param code
	 *           Biller code
	 * @return Bill Payment
	 */
	PSBillPaymentModel getBill(String code);

	/**
	 * Sets payment information to the Public Sector Bill.
	 *
	 * @param psBillPayment
	 *           PSBillPaymentModel
	 * @param paymentInfo
	 *           PaymentInfoModel
	 */
	void setPaymentInfo(final PSBillPaymentModel psBillPayment, final PaymentInfoModel paymentInfo);

	/**
	 * Gets the messages if there are any from Bill's type.
	 *
	 * @param psBillPayment
	 *           PSBillPaymentModel the bill
	 * @return List<PSBillMessageModel>
	 */
	List<PSBillMessageModel> getMessagesForBill(final PSBillPaymentModel psBillPayment);

	/**
	 * Sets the bill type of the Bill.
	 *
	 * @param billType
	 *           PSBillTypeModel the bill type
	 * @param psBillPayment
	 *           PSBillPaymentModel the bill
	 */
	void setBillType(final PSBillPaymentModel psBillPayment, final PSBillTypeModel billType);

	/**
	 * Sets payment source and paid by fields in paymentTransactionModel
	 *
	 * @param paymentTransactionModel
	 *            PaymentTransactionModel for payment transaction details
	 * @param paymentSource
	 *            property configured for payment source
	 */
	void setBillReceiptFields(final PaymentTransactionModel paymentTransactionModel, String paymentSource);

	/**
	 * Processes bill payment.
	 *
	 * @param billCode
	 *            bill code
	 * @param ccPaymentInfoModel
	 *            CreditCardPaymentInfoModel for credit card payment info
	 *            details
	 * @param amount
	 *            big decimal value of bill amount
	 * @param currency
	 *            Currency object for currency details
	 * @param securityCode
	 *            security code
	 * @param paymentProvider
	 *            payment provider name
	 * @param merchantTransactionCode
	 *            merchant transaction code generated
	 * @return PaymentTransactionEntryModel
	 */
	PaymentTransactionEntryModel processPayment(String billCode, CreditCardPaymentInfoModel ccPaymentInfoModel, BigDecimal amount,
			Currency currency, String securityCode, String paymentProvider, String merchantTransactionCode);

	/**
	 * Filters bills based on billPaymentStatus
	 *
	 * @param customerModel
	 *            CustomerModel for customer details
	 * @param pageableData
	 *            PageableData data object for page details
	 * @param statuses
	 *            list of type BillPaymentStatus for statuses application to
	 *            bills
	 * @return List of bills based on bill payment status
	 */
	SearchPageData<PSBillPaymentModel> getBillsByStatusList(CustomerModel customerModel, PageableData pageableData,
			List<BillPaymentStatus> statuses);
}
