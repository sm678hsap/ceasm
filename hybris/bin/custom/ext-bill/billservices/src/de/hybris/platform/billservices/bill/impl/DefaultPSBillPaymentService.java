/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.billservices.bill.PSBillMessageService;
import de.hybris.platform.billservices.bill.PSBillPaymentService;
import de.hybris.platform.billservices.dao.PSBillPaymentDao;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.event.PSBillReceiptCreationEvent;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.billservices.strategies.PSBillPushStrategy;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * Implementation class of {@link PSBillPaymentService}
 */
public class DefaultPSBillPaymentService implements PSBillPaymentService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSBillPaymentService.class);
	private static final String BILL_VALIDATION_ERR_MSG = "PS Bill Payment cannot be null";
	private static final String PAYMENT_SOURCE_INTERNAL_KEY = "payment.source.internal";

	private PSBillPaymentDao psBillPaymentDao;
	private ModelService modelService;
	private PaymentService paymentService;
	private PSBillMessageService psBillMessageService;
	private PSBillPushStrategy psBillPushStrategy;
	private UserService userService;
	private EventService eventService;

	@Override
	public void createOrUpdateBills(final List<PSBillPaymentModel> psBillPayments)
	{
		getPsBillPaymentDao().createOrUpdateBills(psBillPayments);
	}

	@Override
	public List<PSBillPaymentModel> getBills(final CustomerModel customer)
	{
		return getPsBillPushStrategy().getBills(customer);
	}

	@Override
	public PSBillPaymentModel getBill(final String billId, final String lastName)
	{
		return getPsBillPushStrategy().getBill(billId, lastName);
	}

	@Override
	public PSBillPaymentModel getBill(final String code)
	{
		return getPsBillPushStrategy().getBill(code);
	}

	@Override
	public void setPaymentInfo(final PSBillPaymentModel psBillPayment, final PaymentInfoModel paymentInfo)
	{
		validateParameterNotNull(psBillPayment, BILL_VALIDATION_ERR_MSG);
		psBillPayment.setPaymentInfo(paymentInfo);
		getModelService().save(psBillPayment);
		LOG.info("updating the payment info for bill {}", psBillPayment.getCode());
	}

	/**
	 * Sets payment status to the bill payment model.
	 *
	 * @param psBillPayment
	 * @param paymentStatus
	 */
	protected void setPaymentStatus(final PSBillPaymentModel psBillPayment, final BillPaymentStatus paymentStatus)
	{
		validateParameterNotNull(psBillPayment, BILL_VALIDATION_ERR_MSG);
		psBillPayment.setBillPaymentStatus(paymentStatus);
		getModelService().save(psBillPayment);
		LOG.info("updating the payment status for bill {}", psBillPayment.getCode());
	}

	@Override
	public List<PSBillMessageModel> getMessagesForBill(final PSBillPaymentModel psBillPayment)
	{
		validateParameterNotNull(psBillPayment, BILL_VALIDATION_ERR_MSG);
		final PSBillTypeModel billType = psBillPayment.getBillType();
		if (billType != null)
		{
			return getPsBillMessageService().getMessagesByBillType(billType.getCode());
		}
		LOG.debug("no bill types found for bill payment ", psBillPayment);
		return Collections.<PSBillMessageModel> emptyList();
	}

	@Override
	public void setBillType(final PSBillPaymentModel psBillPayment, final PSBillTypeModel billType)
	{
		validateParameterNotNull(psBillPayment, BILL_VALIDATION_ERR_MSG);
		psBillPayment.setBillType(billType);
		getModelService().save(psBillPayment);
		LOG.info("updating bill payment {} with the given bill type ", psBillPayment.getCode());
	}

	@Override
	public void setBillReceiptFields(final PaymentTransactionModel paymentTransactionModel, final String paymentSource)
	{
		if (!userService.isAnonymousUser(userService.getCurrentUser()))
		{
			paymentTransactionModel.setPaidBy(userService.getCurrentUser());
		}
		paymentTransactionModel.setPaymentSource(paymentSource);
		getModelService().save(paymentTransactionModel);
		LOG.info("updating bill payment transaction {} with the payment source ", paymentSource);

	}

	@Override
	public SearchPageData<PSBillPaymentModel> getBillsByStatusList(final CustomerModel customerModel,
			final PageableData pageableData, final List<BillPaymentStatus> statuses)
	{
		validateParameterNotNull(customerModel, "Customer model cannot be null");
		validateParameterNotNull(pageableData, "PageableData must not be null");
		return getPsBillPaymentDao().getBillsByStatus(customerModel, pageableData, statuses);
	}

	@Override
	public PaymentTransactionEntryModel processPayment(final String billCode, final CreditCardPaymentInfoModel ccPaymentInfoModel,
			final BigDecimal amount, final Currency currency, final String securityCode, final String paymentProvider,
			final String merchantTransactionCode)
	{
		PaymentTransactionEntryModel transactionEntryModel = null;
		final PSBillPaymentModel psBillPaymentModel = getBill(billCode);

		if (psBillPaymentModel != null)
		{
			transactionEntryModel = getPaymentService().authorize(merchantTransactionCode, amount, currency,
					ccPaymentInfoModel.getBillingAddress(), ccPaymentInfoModel.getSubscriptionId(), securityCode, paymentProvider);
			if (transactionEntryModel != null)
			{
				final PaymentTransactionModel paymentTransaction = transactionEntryModel.getPaymentTransaction();
				paymentTransaction.setCurrency(transactionEntryModel.getCurrency());
				if (TransactionStatus.ACCEPTED.name().equals(transactionEntryModel.getTransactionStatus())
						|| TransactionStatus.REVIEW.name().equals(transactionEntryModel.getTransactionStatus()))
				{
					LOG.info("Updating the bill with transaction info ");
					final Set<PaymentTransactionModel> newBillPaymentTransactions = new HashSet<>();
					newBillPaymentTransactions.add(paymentTransaction);
					newBillPaymentTransactions.addAll(psBillPaymentModel.getPaymentTransactions());
					psBillPaymentModel.setPaymentTransactions(newBillPaymentTransactions);
					paymentTransaction.setInfo(ccPaymentInfoModel);
					getModelService().saveAll(paymentTransaction, psBillPaymentModel);
					captureBillPayment(paymentTransaction, billCode);
					setBillReceiptFields(paymentTransaction, Config.getParameter(PAYMENT_SOURCE_INTERNAL_KEY));
					setOverPaidAmount(psBillPaymentModel, paymentTransaction);
					setBillDueAmountAndStatus(psBillPaymentModel, paymentTransaction);
					triggerBillReceiptCreationEvent(paymentTransaction);
				}
				else
				{
					LOG.info("TransactionStatus is rejected or have error. Removing PaymentTransaction and TransactionEntry.");
					getModelService().removeAll(Arrays.asList(paymentTransaction, transactionEntryModel));
				}
			}
		}

		if (CustomerType.GUEST.equals(((CustomerModel) getUserService().getCurrentUser()).getType()))
		{
			getUserService().setCurrentUser(getUserService().getAnonymousUser());
			LOG.info("updating user to anonymous");
			final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
			final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
					getUserService().getCurrentUser().getUid(), null, oldAuthentication.getAuthorities());
			newAuthentication.setDetails(oldAuthentication.getDetails());
			SecurityContextHolder.getContext().setAuthentication(newAuthentication);
		}
		return transactionEntryModel;
	}
	
	/**
	 @deprecated As of HCEA 3.1 this is deprecated as it was misspelled, replaced by {@link DefaultPSBillPaymentService#triggerBillReceiptCreationEvent(PaymentTransactionModel)}
	 * This method will be removed in HCEA 4.0
	 */
	@Deprecated
	protected void triggerBillReceiptCReationEvent(final PaymentTransactionModel paymentTransaction)
	{
		this.triggerBillReceiptCreationEvent(paymentTransaction);
	}
	
	protected void triggerBillReceiptCreationEvent(final PaymentTransactionModel paymentTransaction)
	{
		final PSBillReceiptCreationEvent event = new PSBillReceiptCreationEvent();
		event.setPaymentTransaction(paymentTransaction);
		getEventService().publishEvent(event);
	}

	/**
	 * @deprecated As of HCEA 3.1 this is deprecated as it was misspelled, replaced by {@link DefaultPSBillPaymentService#captureBillPayment(PaymentTransactionModel, String)}
	 * This method will be removed in HCEA 4.0
	 */
	@Deprecated 
	protected boolean caputureBillPayment(final PaymentTransactionModel paymentTransaction, final String billCode)
	{
		return this.captureBillPayment(paymentTransaction, billCode);
	}
	
	protected boolean captureBillPayment(final PaymentTransactionModel paymentTransaction, final String billCode)
	{
		final PaymentTransactionEntryModel txnEntry = getPaymentService().capture(paymentTransaction);

		if (TransactionStatus.ACCEPTED.name().equals(txnEntry.getTransactionStatus()))
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("The payment transaction has been captured. Bill: " + billCode + ". Txn: " + paymentTransaction.getCode());
			}
		}
		else
		{
			LOG.error("The payment transaction capture has failed. Bill: " + billCode + ". Txn: " + paymentTransaction.getCode());
			return false;
		}
		return true;
	}

	/**
	 * Sets payment status and due amount to the bill payment model.
	 *
	 * @param psBillPayment
	 * @param transaction
	 */
	protected void setBillDueAmountAndStatus(final PSBillPaymentModel psBillPayment, final PaymentTransactionModel transaction)
	{
		validateParameterNotNull(psBillPayment, BILL_VALIDATION_ERR_MSG);

		if (transaction.getPlannedAmount().doubleValue() > 0)
		{
			final BigDecimal outstandingAmount = psBillPayment.getOutstandingBillAmount().subtract(transaction.getPlannedAmount());
			psBillPayment.setOutstandingBillAmount(outstandingAmount.signum() == -1 ? BigDecimal.ZERO : outstandingAmount);
			transaction.setOutstandingAmount(psBillPayment.getOutstandingBillAmount());

			if (psBillPayment.getOutstandingBillAmount().signum() != 1) // If
			// psBillPayment.getOutstandingBillAmount()
			// is
			// negative
			// or
			// zero
			{
				setPaymentStatus(psBillPayment, BillPaymentStatus.PAID);
			}
			else
			{
				setPaymentStatus(psBillPayment, BillPaymentStatus.PARTPAID);
			}
			getModelService().saveAll(psBillPayment, transaction);
		}
	}

	/**
	 * Sets and Over Paid Amount to the bill payment model.
	 *
	 * @param psBillPayment
	 * @param transaction
	 */
	protected void setOverPaidAmount(final PSBillPaymentModel psBillPayment, final PaymentTransactionModel transaction)
	{
		validateParameterNotNull(psBillPayment, BILL_VALIDATION_ERR_MSG);

		if (transaction.getPlannedAmount().doubleValue() > 0)
		{
			final BigDecimal overPaidBillValue = transaction.getPlannedAmount().subtract(psBillPayment.getOutstandingBillAmount());
			if (overPaidBillValue.signum() == 1) // If overPaidBillValue is
			// greater than 0
			{
				psBillPayment.setOverPaidAmount(overPaidBillValue);
				transaction.setOverPaidAmount(psBillPayment.getOverPaidAmount());
			}
			getModelService().saveAll(psBillPayment, transaction);
		}
	}

	protected PSBillPaymentDao getPsBillPaymentDao()
	{
		return psBillPaymentDao;
	}

	@Required
	public void setPsBillPaymentDao(final PSBillPaymentDao psBillPaymentDao)
	{
		this.psBillPaymentDao = psBillPaymentDao;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected PaymentService getPaymentService()
	{
		return paymentService;
	}

	@Required
	public void setPaymentService(final PaymentService paymentService)
	{
		this.paymentService = paymentService;
	}

	protected PSBillMessageService getPsBillMessageService()
	{
		return psBillMessageService;
	}

	@Required
	public void setPsBillMessageService(final PSBillMessageService psBillMessageService)
	{
		this.psBillMessageService = psBillMessageService;
	}

	protected PSBillPushStrategy getPsBillPushStrategy()
	{
		return psBillPushStrategy;
	}

	@Required
	public void setPsBillPushStrategy(final PSBillPushStrategy psBillPushStrategy)
	{
		this.psBillPushStrategy = psBillPushStrategy;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}
}
