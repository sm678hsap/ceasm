/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.dao;

import de.hybris.platform.billservices.model.PSBillMessageModel;

import java.util.List;


/**
 * Interface class for PSBillMessage DAO.
 */
public interface PSBillMessageDao
{
	/**
	 * Gets bill messages by bill type.
	 *
	 * @param billPk
	 *           String the bill's PK.
	 * @return List<PSBillMessageModel>
	 *
	 */
	List<PSBillMessageModel> findMessagesByBillType(final String billPk);
}
