/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billhandlers;

import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Iterator;


/**
 * Attribute handler for dynamic isPartPaymentAllowed attribute of PSBillTypeModel.
 */
public class DynamicPartPaymentAllowedAttributeHandler implements DynamicAttributeHandler<Boolean, PSBillTypeModel>
{
	public static final String PART_PAYMENT_NOT_ALLOWED = "PART_PAYMENT_NOT_ALLOWED";

	@Override
	public Boolean get(final PSBillTypeModel billType)
	{
		Boolean isPartPaymentNotAllowed = false;

		if (billType == null)
		{
			throw new IllegalArgumentException("Bill type is required");
		}
		else
		{
			final Iterator iter = billType.getPsBillMessages().iterator();
			while (iter.hasNext())
			{
				final PSBillMessageModel message = (PSBillMessageModel) iter.next();
				isPartPaymentNotAllowed = (Boolean) (message == null ? ""
						: PART_PAYMENT_NOT_ALLOWED.equalsIgnoreCase(message.getMessageCondition().getCode()));

				if (isPartPaymentNotAllowed)
				{
					break;
				}

			}
			return !isPartPaymentNotAllowed;
		}
	}

	@Override
	public void set(final PSBillTypeModel arg0, final Boolean arg1)
	{
		throw new UnsupportedOperationException();
	}

}
