/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billservices.retention.hook;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used to remove the bills and its related objects from a customer
 */
public class PSServicesCustomerBillsCleanupHook extends PSBillAbstractItemCleanupHook<CustomerModel> {
	private static final Logger LOG = LoggerFactory.getLogger(PSServicesCustomerBillsCleanupHook.class);

	@Override
	public void cleanupRelatedObjects(final CustomerModel customerModel) {
		validateParameterNotNullStandardMessage("customerModel", customerModel);

		if (LOG.isDebugEnabled()) {
			LOG.debug("Cleaning up bills related objects for Customer : {}", customerModel);
		}

		for (final PSBillPaymentModel bill : customerModel.getPsBillPayments()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Cleaning up bill {}, id {} ", bill.getCode(), bill.getBillId());
			}

			cleanUpBillPaymentTransactions(bill);

			getModelService().remove(bill);
			this.removeAuditRecords(bill);
		}
	}

	private void cleanUpBillPaymentTransactions(final PSBillPaymentModel bill) {
		for (final PaymentTransactionModel billPaymentTransaction : bill.getPaymentTransactions()) {
			for (final PaymentTransactionEntryModel billPaymentTransactionEntry : billPaymentTransaction.getEntries()) {
				getModelService().remove(billPaymentTransactionEntry);
				this.removeAuditRecords(billPaymentTransactionEntry);
			}
			getModelService().remove(billPaymentTransaction);
			this.removeAuditRecords(billPaymentTransaction);
		}
	}
}
