/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.bill;

import de.hybris.platform.billservices.model.PSBillMessageModel;

import java.util.List;


/**
 * Interface class for PSBillMessage service methods.
 */
public interface PSBillMessageService
{
	/**
	 * Gets bill messages by bill type.
	 *
	 * @param billTypeCode
	 *           String the Bill's code.
	 *
	 * @return List<PSBillMessageModel>
	 */
	List<PSBillMessageModel> getMessagesByBillType(final String billTypeCode);
}
