/*
 * [y] hybris Platform

 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.dao.impl;


import de.hybris.platform.billservices.dao.PSBillPaymentDao;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation class for Bill Payment DAO.
 */
public class DefaultPSBillPaymentDao extends AbstractItemDao implements PSBillPaymentDao
{   
	private static final String BY_DUE_DATE_AND_STATUS = "byDueDateAndStatus";
	protected static final String SELECTCLAUSE = "SELECT {" + PSBillPaymentModel.PK + "} FROM {" + PSBillPaymentModel._TYPECODE
			+ "! AS p} ";

	protected static final String PSBILLPAYMENT_QUERY_BY_CODE = SELECTCLAUSE + "WHERE  { p:" + PSBillPaymentModel.CODE
			+ "} = ?code";

	protected static final String PSBILLPAYMENT_QUERY_BY_BILLID_AND_LASTNAME = SELECTCLAUSE + "WHERE  { p:"
			+ PSBillPaymentModel.BILLID + "} = ?billid  AND " + "LOWER(" + "{ p:" + PSBillPaymentModel.LASTNAME + "})" + " = LOWER("
			+ "?lastName)";

	protected static final String PSBILLPAYMENT_QUERY_BY_CUSTOMER = "SELECT {" + PSBillPaymentModel.PK + "} " + "FROM {"
			+ PSBillPaymentModel._TYPECODE + "! AS p " + "JOIN " + CustomerModel._TYPECODE + " AS c ON {p:"
			+ PSBillPaymentModel.CUSTOMER + "}={c:" + CustomerModel.PK + "}} " + "WHERE {c:" + CustomerModel.PK + "}=?customerPk"
			+ " order by {" + PSBillPaymentModel.BILLDUEDATE + "} ASC";

	/**
	 * Please, note that the CASE for Status is required to sort bills by only PAID and NOT PAID (UNPAID/PART-PAID) status.
	 */
	protected static final String BILL_STATUS_QUERY_BY_CUSTOMER = "SELECT {" + PSBillPaymentModel.PK + "}, " +
			"(CASE WHEN ({status:CODE}='PAID') THEN 1 ELSE 0 END) as statusJoined "
			+ "FROM {"
			+ PSBillPaymentModel._TYPECODE + "! AS p " + "JOIN " + CustomerModel._TYPECODE + " AS c ON {p:"
			+ PSBillPaymentModel.CUSTOMER + "}={c:" + CustomerModel.PK + "}" + "JOIN " + EnumerationValueModel._TYPECODE
			+ " AS status ON {p:" + PSBillPaymentModel.BILLPAYMENTSTATUS + "}={status:" + EnumerationValueModel.PK + "}} "
			+ "WHERE {c:" + CustomerModel.PK + "}=?customerPk";

	protected static final String FILTER_PSBILLPAYMENT_QUERY_BY_PAYMENT_STATUS = " AND {" + PSBillPaymentModel.BILLPAYMENTSTATUS
			+ "} IN (?statuses)";

	protected static final String SORT_QUERY_BY_BILL_DUE_DATE = " ORDER BY {" + PSBillPaymentModel.BILLDUEDATE + "} ASC";

	/**
	 * Please, note that the STATUSJOINED column is created in the query BILL_STATUS_QUERY_BY_CUSTOMER above
	 * and used to sort not paid bills before paid bills by due date
	 */

	protected static final String SORT_QUERY_BY_BILL_DUE_DATE_AND_STATUS = " ORDER BY "
			+ "statusJoined, {" + PSBillPaymentModel.BILLDUEDATE + "} ASC";

	private PagedFlexibleSearchService pagedFlexibleSearchService;

	@Override
	public void createOrUpdateBills(final List<PSBillPaymentModel> psBillPayments)
	{
		if (psBillPayments != null)
		{
			getModelService().saveAll(psBillPayments);
		}
	}

	@Override
	public PSBillPaymentModel getBill(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "bill code must not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put("code", code);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PSBILLPAYMENT_QUERY_BY_CODE, params);
		final SearchResult<PSBillPaymentModel> result = getFlexibleSearchService().search(fsq);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

	@Override
	public PSBillPaymentModel getBill(final String billId, final String lastName)
	{
		ServicesUtil.validateParameterNotNull(billId, "The given bill id is null!");
		ServicesUtil.validateParameterNotNull(lastName, "The given customer last name is null!");

		final Map<String, Object> params = new HashMap<>();
		params.put("billid", billId);
		params.put("lastName", lastName);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PSBILLPAYMENT_QUERY_BY_BILLID_AND_LASTNAME, params);
		final SearchResult<PSBillPaymentModel> result = getFlexibleSearchService().search(fsq);
		return result.getCount() > 0 ? result.getResult().get(0) : null;

	}

	@Override
	public List<PSBillPaymentModel> getBills(final String customerPK)
	{
		ServicesUtil.validateParameterNotNull(customerPK, "The given customer pk is null!");

		final Map<String, Object> params = new HashMap<>();
		params.put("customerPk", customerPK);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PSBILLPAYMENT_QUERY_BY_CUSTOMER, params);
		final SearchResult<PSBillPaymentModel> result = getFlexibleSearchService().search(fsq);
		return result.getCount() > 0 ? result.getResult() : Collections.<PSBillPaymentModel> emptyList();
	}

	@Override
	public SearchPageData<PSBillPaymentModel> getBillsByStatus(final CustomerModel customerModel, final PageableData pageableData,
			final List<BillPaymentStatus> statuses)
	{
		ServicesUtil.validateParameterNotNull(customerModel, "The given customer id is null!");
		ServicesUtil.validateParameterNotNull(pageableData, "The given pageable data is null!");

		final Map<String, Object> params = new HashMap<>();
		params.put("customerPk", customerModel.getPk().getLongValueAsString());
		params.put("pageableData", pageableData);
		String filterClause = StringUtils.EMPTY;
		if (CollectionUtils.isNotEmpty(statuses))
		{
			params.put("statuses", statuses);
			filterClause = FILTER_PSBILLPAYMENT_QUERY_BY_PAYMENT_STATUS;
		}

		List<SortQueryData> sortQueries = new ArrayList<SortQueryData>();
		if (CollectionUtils.isEmpty(statuses))
		{
			sortQueries = Arrays.asList(
					createSortQueryData("byDueDate", createQuery(BILL_STATUS_QUERY_BY_CUSTOMER, SORT_QUERY_BY_BILL_DUE_DATE)),
					createSortQueryData(BY_DUE_DATE_AND_STATUS,
							createQuery(BILL_STATUS_QUERY_BY_CUSTOMER, SORT_QUERY_BY_BILL_DUE_DATE_AND_STATUS)));
		}
		else
		{
			sortQueries = Arrays.asList(
					createSortQueryData("byDueDate",
							createQuery(BILL_STATUS_QUERY_BY_CUSTOMER, filterClause, SORT_QUERY_BY_BILL_DUE_DATE)),
					createSortQueryData(BY_DUE_DATE_AND_STATUS,
							createQuery(BILL_STATUS_QUERY_BY_CUSTOMER, filterClause, SORT_QUERY_BY_BILL_DUE_DATE_AND_STATUS)));
		}
		return getPagedFlexibleSearchService().search(sortQueries, BY_DUE_DATE_AND_STATUS, params, pageableData);
	}

	protected SortQueryData createSortQueryData(final String sortCode, final String query)
	{
		final SortQueryData result = new SortQueryData();
		result.setSortCode(sortCode);
		result.setQuery(query);
		return result;
	}

	protected String createQuery(final String... queryClauses)
	{
		final StringBuilder queryBuilder = new StringBuilder();

		for (final String queryClause : queryClauses)
		{
			queryBuilder.append(queryClause);
		}
		return queryBuilder.toString();
	}

	protected PagedFlexibleSearchService getPagedFlexibleSearchService()
	{
		return pagedFlexibleSearchService;
	}

	@Required
	public void setPagedFlexibleSearchService(final PagedFlexibleSearchService pagedFlexibleSearchService)
	{
		this.pagedFlexibleSearchService = pagedFlexibleSearchService;
	}
}
