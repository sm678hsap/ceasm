/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.event;

import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * RelationshipRequestEvent Event.
 */
public class PSBillReceiptCreationEvent extends AbstractEvent
{

	private PaymentTransactionModel paymentTransaction;

	public PaymentTransactionModel getPaymentTransaction()
	{
		return paymentTransaction;
	}

	public void setPaymentTransaction(final PaymentTransactionModel paymentTransaction)
	{
		this.paymentTransaction = paymentTransaction;
	}

}
