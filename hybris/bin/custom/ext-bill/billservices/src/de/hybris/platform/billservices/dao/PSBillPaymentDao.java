/*
 * [y] hybris Platform

 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.dao;

import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


/**
 * Interface class for BillPayment DAO.
 */
public interface PSBillPaymentDao
{
	/**
	 * Create or update bills
	 *
	 * @param psBillPayments
	 *           List<PSBillPaymentModel> bill payment
	 */
	void createOrUpdateBills(List<PSBillPaymentModel> psBillPayments);

	/**
	 * Get a bill by bill code
	 *
	 * @param code
	 *           String bill code
	 * @return PSBillPaymentModel
	 */
	PSBillPaymentModel getBill(String code);

	/**
	 * Get a bill by bill id and customer last name
	 *
	 * @param billId
	 *           bill id
	 * @param lastName
	 *           customer last name
	 * @return PSBillPaymentModel
	 */
	PSBillPaymentModel getBill(String billId, String lastName);

	/**
	 * get bills by customer id
	 *
	 * @param customerPK
	 *           customer pk
	 * @return List<PSBillPaymentModel>
	 */
	List<PSBillPaymentModel> getBills(String customerPK);

	/**
	 * Filters bills based on billPaymentStatus
	 *
	 * @param customerModel
	 * @param pageableData
	 * @param statuses
	 * @return List of bills based on bill payment status
	 */
	SearchPageData<PSBillPaymentModel> getBillsByStatus(CustomerModel customerModel, PageableData pageableData,
			List<BillPaymentStatus> statuses);
}
