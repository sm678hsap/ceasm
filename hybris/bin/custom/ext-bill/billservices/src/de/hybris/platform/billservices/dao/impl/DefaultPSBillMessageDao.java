/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billservices.dao.impl;

import de.hybris.platform.billservices.dao.PSBillMessageDao;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Implementation class of {@link PSBillMessageDao}
 */
public class DefaultPSBillMessageDao extends AbstractItemDao implements PSBillMessageDao
{
	protected static final String PSBILLMESSAGE_QUERY_BY_BILLTYPE = "SELECT {" + PSBillMessageModel.PK + "} " + "FROM {"
			+ PSBillTypeModel._TYPECODE + "! AS bt }" + "WHERE {bt:" + PSBillTypeModel.CODE + "}=?billTypeCode";

	@Override
	public List<PSBillMessageModel> findMessagesByBillType(final String billTypeCode)
	{
		ServicesUtil.validateParameterNotNull(billTypeCode, "bill pk code must not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put("billTypeCode", billTypeCode);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PSBILLMESSAGE_QUERY_BY_BILLTYPE, params);
		final SearchResult<PSBillMessageModel> result = getFlexibleSearchService().search(fsq);

		return result.getCount() > 0 ? result.getResult() : Collections.<PSBillMessageModel> emptyList();
	}
}
