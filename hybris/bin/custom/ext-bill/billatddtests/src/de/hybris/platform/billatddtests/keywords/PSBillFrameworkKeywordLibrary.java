/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billatddtests.keywords;


import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.billfacades.bill.PSDocumentGenerationFacade;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.process.xslfo.context.PSBillReceiptContext;
import de.hybris.platform.billfacades.strategies.document.generation.pdf.fop.PSDocumentDataProcessStrategy;
import de.hybris.platform.billfacades.utils.PSFopDocumentGenerationUtils;
import de.hybris.platform.billservices.bill.PSBillPaymentService;
import de.hybris.platform.billservices.bill.PSBillTypeService;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;


/**
 * Robot test Implementation class of {@link PSBillFrameworkKeywordLibrary}
 */
public class PSBillFrameworkKeywordLibrary extends AbstractKeywordLibrary
{

	private static final String SITE_BASE_URL_RESOLUTION_SERVICE = "siteBaseUrlResolutionService";
	private static final String URL_ENCODER_SERVICE = "urlEncoderService";
	private static final String MEDIA_SERVICE = "mediaService";
	private static final String CATALOG_VERSION_SERVICE = "catalogVersionService";
	private static final String USER_SERVICE = "userService";
	private static final String CONFIGURATION_SERVICE = "configurationService";
	private static final String BILL_RECEIPT_CONTEXT = "billReceiptContext";

	@Autowired
	private CustomerAccountService customerAccountService;

	@Autowired
	private PSBillPaymentService psBillPaymentService;

	@Autowired
	private UserService userService;

	@Autowired
	private PSBillTypeService psBillTypeService;

	@Autowired
	private PSDocumentDataProcessStrategy pSDocumentDataProcessStrategy;

	@Autowired
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Autowired
	private UrlEncoderService urlEncoderService;

	@Autowired
	private MediaService mediaService;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private CheckoutFlowFacade checkoutFlowFacade;

	@Autowired
	private CommerceCheckoutService commerceCheckoutService;

	@Autowired
	private PSDocumentGenerationFacade psDocumentGenerationFacade;

	@Autowired
	private ConfigurationService configurationService;

	/**
	 * Get a Bill information by Bill Code
	 *
	 * @param code
	 *           Bill code
	 */
	public void getBillByCode(final String code)
	{
		final PSBillPaymentModel billPayment = psBillPaymentService.getBill(code);

		Assert.assertNotNull(billPayment);
	}

	/**
	 * Get a Bill information by Bill id and customer last name
	 *
	 * @param billId
	 *           Bill id
	 * @param lastName
	 *           Last Name
	 */
	public void getBillByCodeAndName(final String billId, final String lastName)
	{
		final PSBillPaymentModel billPayment = psBillPaymentService.getBill(billId, lastName);

		Assert.assertNotNull(billPayment);
	}

	/**
	 * Get customer bills
	 *
	 * @param uid
	 *           customer uid
	 */
	public void getBills(final String uid)
	{
		final CustomerModel customer = (CustomerModel) userService.getUserForUID(uid);
		final List<PSBillPaymentModel> bills = psBillPaymentService.getBills(customer);

		Assert.assertNotNull(bills);
	}

	/**
	 * Process bill payment
	 *
	 * @param code
	 *           bill code
	 * @throws Exception
	 */
	public void processBillPayment(final String code)
	{
		final CustomerModel customer = (CustomerModel) userService.getUserForUID("jane.citizen@stateofrosebud.com");

		userService.setCurrentUser(customer);

		if (customer.getDefaultPaymentInfo() == null)
		{
			final List<CreditCardPaymentInfoModel> ccPaymentInfoModelList = customerAccountService
					.getCreditCardPaymentInfos(customer, true);
			if (CollectionUtils.isNotEmpty(ccPaymentInfoModelList))
			{
				customerAccountService.setDefaultPaymentInfo(customer, ccPaymentInfoModelList.get(ccPaymentInfoModelList.size() - 1));
			}
		}

		final boolean requestSecurityCode = CheckoutPciOptionEnum.DEFAULT.equals(checkoutFlowFacade.getSubscriptionPciOption());
		final String paymentProvider = commerceCheckoutService.getPaymentProvider();

		final BigDecimal amount = BigDecimal.valueOf(2322.89);
		final Currency currency = Currency.getInstance("USD");
		final String merchantCode = customer.getUid() + "-" + UUID.randomUUID();

		final PaymentTransactionEntryModel transactionEntryModel = psBillPaymentService.processPayment(code,
				(CreditCardPaymentInfoModel) customer.getDefaultPaymentInfo(), amount, currency,
				Boolean.toString(requestSecurityCode), paymentProvider, merchantCode);


		final PSBillPaymentModel bill = psBillPaymentService.getBill(code);

		Assert.assertNotNull(bill.getBillPaymentStatus());
		Assert.assertEquals(BillPaymentStatus.PAID, bill.getBillPaymentStatus());
		Assert.assertEquals(TransactionStatus.ACCEPTED.name(), transactionEntryModel.getTransactionStatus());

		final Set<PaymentTransactionModel> paymentTransactions = bill.getPaymentTransactions();
		for (final PaymentTransactionModel paymentTransaction : paymentTransactions)
		{
			Assert.assertNotNull(paymentTransaction.getPaidBy());
			Assert.assertNotNull(paymentTransaction.getPaymentSource());
		}

	}

	/**
	 * Get messages for a bill.
	 *
	 * @param code
	 *           the bill code.
	 */
	public void getMessagesForBill(final String code)
	{
		final PSBillPaymentModel bill = psBillPaymentService.getBill(code);
		psBillPaymentService.getMessagesForBill(bill);

		Assert.assertNotNull(psBillPaymentService.getMessagesForBill(bill));
	}

	/**
	 * <p>
	 * <i>verify number of messages of a bill</i>
	 * <p>
	 *
	 * @param billCode
	 *           the bill Code.
	 * @param expectedNoOfMessages
	 *           the expected number of messages for this bill.
	 */
	public void verifyNumberOfMessagesOfABill(final String billCode, final int expectedNoOfMessages)
	{
		final PSBillPaymentModel bill = psBillPaymentService.getBill(billCode);
		final List<PSBillMessageModel> messages = psBillPaymentService.getMessagesForBill(bill);
		if (messages != null && !messages.isEmpty())
		{
			Assert.assertEquals(expectedNoOfMessages, messages.size());
		}
		else
		{
			Assert.fail("No messages for bill: " + billCode);
		}
	}

	/**
	 * <p>
	 * <i>verify the number of type of a bill</i>
	 * <p>
	 *
	 * @param billTypeCode
	 *           the service to add to cart.
	 * @param expectedNoOfType
	 *           the expected number of type of this product.
	 */
	public void verifyNumberOfBillType(final String billTypeCode, final int expectedNoOfType)
	{
		final PSBillTypeModel billType = psBillTypeService.getBillType(billTypeCode);

		if (billType != null)
		{
			Assert.assertEquals(expectedNoOfType, 1);
		}
		else
		{
			Assert.fail("No type assigned for bill type: " + billTypeCode);
		}
	}

	/**
	 *
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify generate receipt PDF with billCode and receiptId</i>
	 * <p>
	 *
	 * @param billCode
	 * @param receiptId
	 */
	public void verifyGenerateReceiptPDFWithBillCodeAndReceiptId(final String billCode, final String receiptId)
	{
		registerBeanInApplicationContext();

		final PSBillReceiptData billReceiptData = pSDocumentDataProcessStrategy.getDocumentData(billCode, receiptId);

		Assert.assertNotNull(billReceiptData);

		final String body = psDocumentGenerationFacade.getBillPaymentReceiptBody(billReceiptData);

		final byte[] pdf = PSFopDocumentGenerationUtils.generatePdf(body);

		Assert.assertNotNull(pdf);
	}

	private void registerBeanInApplicationContext()
	{
		// Register Service from Web Application Context to Global Application Context
		final ConfigurableApplicationContext ctx = (ConfigurableApplicationContext) Registry.getApplicationContext();
		final DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) ctx.getBeanFactory();
		final AbstractBeanDefinition billReceiptContextDefinition = BeanDefinitionBuilder
				.rootBeanDefinition(PSBillReceiptContext.class).getBeanDefinition();

		// Add bean properties
		final MutablePropertyValues propertyValues = new MutablePropertyValues();
		propertyValues.add(SITE_BASE_URL_RESOLUTION_SERVICE, siteBaseUrlResolutionService);
		propertyValues.add(URL_ENCODER_SERVICE, urlEncoderService);
		propertyValues.add(MEDIA_SERVICE, mediaService);
		propertyValues.add(CATALOG_VERSION_SERVICE, catalogVersionService);
		propertyValues.add(USER_SERVICE, userService);
		propertyValues.add(CONFIGURATION_SERVICE, configurationService);

		billReceiptContextDefinition.setPropertyValues(propertyValues);
		beanFactory.registerBeanDefinition(BILL_RECEIPT_CONTEXT, billReceiptContextDefinition);
	}

}
