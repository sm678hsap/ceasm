*** Settings ***
Library          de.hybris.platform.commerceservicesatddtests.keywords.CommerceServicesKeywordLibrary
Test Setup       import sampledata
Resource			  impex/Impex_Keywords.txt
Resource		  	  commerce/CommerceServices_Keywords.txt
Resource         bill/BillFramework_Keywords.txt

*** Test Cases ***

Test_BillFramework_Loading_Via_Code
    [Documentation]    load customer bill by bill code.
    load customer bill "Rate-100001"

Test_BillFramework_Loading_Via_Code_And_Name
    [Documentation]    load customer bill by bill id and last name.
    load customer bill by "E-10001" and name "Citizen"

Test_BillFramework_Get_Customer_Bills
    [Documentation]    load customer bills.
    load customer bills "jane.citizen@stateofrosebud.com"

Test_BillFramework_Get_Messages_For_Bill
    [Documentation]    get messages for bill.
    get messages for bill "Rate-100001"
    
Test_BillFramework_Verify_Messages_For_Bill
    [Documentation]    verify number of messages of a bill.
    verify number of messages of a bill "Rate-100001" 1
    
Test_BillFramework_Verify_Number_Of_BillType
    [Documentation]    verify the number of bill type.
    verify number of bill type "120001" 1
    
Test_BillFramework_Verify_Generate_Receipt_PDF_With_BillCode_And_ReceiptId
    [Documentation]    Testing: verify generate receipt PDF with billCode and receiptId
    login customer with id "jane.citizen@stateofrosebud.com"
    verify generate receipt pdf with billCode "Rate-100002" and receiptId "R-10002"