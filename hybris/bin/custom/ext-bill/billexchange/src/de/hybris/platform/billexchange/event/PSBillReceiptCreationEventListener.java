/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billexchange.event;

import de.hybris.platform.billservices.event.PSBillReceiptCreationEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskService;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Event listener for PSBillReceiptCreationEvent.
 */
public class PSBillReceiptCreationEventListener extends AbstractEventListener<PSBillReceiptCreationEvent>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSBillReceiptCreationEventListener.class);
	private ModelService modelService;
	private TaskService taskService;
	private String taskRunnerBean;

	@Override
	protected void onEvent(final PSBillReceiptCreationEvent billReceiptCreationEvent)
	{
		final TaskModel taskModel = getModelService().create(TaskModel.class);
		taskModel.setContext(billReceiptCreationEvent.getPaymentTransaction());
		taskModel.setRunnerBean(getTaskRunnerBean());
		taskModel.setExecutionDate(new Date());

		LOG.info("Task scheduled for bill receipt outbound feed");
		getTaskService().scheduleTask(taskModel);
	}

	/**
	 * @return modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           - modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return taskService
	 */
	public TaskService getTaskService()
	{
		return taskService;
	}

	/**
	 * @param taskService
	 *           - taskService to set
	 */
	@Required
	public void setTaskService(final TaskService taskService)
	{
		this.taskService = taskService;
	}

	/**
	 * @return taskRunnerBean
	 */
	public String getTaskRunnerBean()
	{
		return taskRunnerBean;
	}

	/**
	 * @param taskRunnerBean
	 *           - taskRunnerBean to set
	 */
	@Required
	public void setTaskRunnerBean(final String taskRunnerBean)
	{
		this.taskRunnerBean = taskRunnerBean;
	}

}
