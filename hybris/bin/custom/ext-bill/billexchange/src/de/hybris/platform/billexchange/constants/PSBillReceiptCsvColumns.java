/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billexchange.constants;

/**
 * The Public Sector Bill Receipt CSV Columns.
 */
public class PSBillReceiptCsvColumns
{
	public static final String CODE = "code";

	public static final String BILL_ID = "billId";

	public static final String CURRENCY = "currency";

	public static final String PAID_BY = "paidBy";

	public static final String PAYMENT_SOURCE = "paymentSource";

	public static final String AMOUNT = "amount";

	public static final String CREDIT_CARD_OWNER = "creditCardOwner";

	public static final String CREDIT_CARD_NUMBER = "creditCardNumber";

	public static final String CREDIT_CARD_TYPE = "creditCardType";

	public static final String VALID_TO_MONTH = "validToMonth";

	public static final String VALID_TO_YEAR = "validToYear";

	public static final String CREDIT_CARD_USER = "creditCardUser";

	//Billing address

	public static final String BILLING_ADDRESS_TITLE = "billingAddressTitle";

	public static final String BILLING_ADDRESS_FIRST_NAME = "billingAddressFirstName";

	public static final String BILLING_ADDRESS_LAST_NAME = "billingAddressLastName";

	public static final String BILLING_ADDRESS_STREET_NAME = "billingAddressStreetName";

	public static final String BILLING_ADDRESS_POSTAL_CODE = "billingAddressPostalCode";

	public static final String BILLING_ADDRESS_TOWN = "billingAddressTown";

	public static final String BILLING_ADDRESS_COUNTRY = "billingAddressCountry";

	public static final String BILLING_ADDRESS_PHONE1 = "billingAddressPhone1";

	public static final String BILLING_ADDRESS_EMAIL = "billingAddressEmail";


	private PSBillReceiptCsvColumns()
	{
		//empty to avoid instantiating this class
	}
}
