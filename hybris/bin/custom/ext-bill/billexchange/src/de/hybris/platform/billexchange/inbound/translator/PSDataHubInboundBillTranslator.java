/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.billexchange.inbound.translator;


import de.hybris.platform.billexchange.inbound.PSDataHubInboundBillHelper;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;


/**
 * Translator for bill inbound. It triggers the event whenever a new bill is added
 */
public class PSDataHubInboundBillTranslator extends PSDataHubTranslator<PSDataHubInboundBillHelper>
{

	public static final String HELPER_BEAN = "psDataHubBillInboundHelper";

	/***
	 * Constructor on which invokes parent class constructor with helper bean , which in turns gets bean from application
	 * context.
	 *
	 */
	public PSDataHubInboundBillTranslator()
	{
		super(HELPER_BEAN);
	}

	@Override
	public void performImport(final String billCode, final Item processedItem) throws ImpExException
	{
		getInboundHelper().processBillInformation(billCode);
	}
}
