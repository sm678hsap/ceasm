/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billexchange.outbound.impl;

import de.hybris.platform.billexchange.outbound.PSSendToDataHubResult;

import org.springframework.http.HttpStatus;


/**
 * Container for the Result of the Datahub sending process
 */
public class DefaultPSSendToDataHubResult implements PSSendToDataHubResult
{
	public static final PSSendToDataHubResult OKAY = new DefaultPSSendToDataHubResult(HttpStatus.OK, "Sent successfully");
	public static final PSSendToDataHubResult SENDING_FAILED = new DefaultPSSendToDataHubResult(
			PSSendToDataHubResult.SENDING_FAILED_CODE, "Sending failed for unknown reason");
	public static final PSSendToDataHubResult MESSAGE_HANDLING_ERROR = new DefaultPSSendToDataHubResult(
			PSSendToDataHubResult.MESSAGE_HANDLING_ERROR, "Sending failed for unknown reason");
	private final String errorText;
	private HttpStatus httpStatus;
	private final int errorCode;

	/**
	 * Default constructor for creation with existing httpStatus and errorText.
	 *
	 * @param httpStatus
	 * @param errorText
	 */
	public DefaultPSSendToDataHubResult(final HttpStatus httpStatus, final String errorText)
	{
		this.httpStatus = httpStatus;
		this.errorText = errorText;
		this.errorCode = httpStatus.value();
	}

	/**
	 * Default constructor for creation with existing errorCode and errorText.
	 *
	 * @param errorCode
	 * @param errorText
	 */
	public DefaultPSSendToDataHubResult(final int errorCode, final String errorText)
	{
		this.errorCode = errorCode;
		this.errorText = errorText;
	}

	@Override
	public boolean isSuccess()
	{
		return httpStatus != null && getErrorCode() == 200;
	}

	@Override
	public int getErrorCode()
	{
		return this.errorCode;
	}

	@Override
	public String getErrorText()
	{
		return errorText;
	}

	@Override
	public String toString()
	{
		return "httpStatus=" + httpStatus + ", errorText=" + errorText;
	}
}
