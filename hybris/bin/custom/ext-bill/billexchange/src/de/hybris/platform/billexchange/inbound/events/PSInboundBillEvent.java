/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.billexchange.inbound.events;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * This class is to rise up bill in bound events
 */
public class PSInboundBillEvent extends AbstractEvent implements ClusterAwareEvent
{

	private static final long serialVersionUID = -3839203917371395971L;
	private final String billId;

	/**
	 * @param billId
	 */
	public PSInboundBillEvent(final String billId)
	{
		super();
		this.billId = billId;
	}

	/**
	 * @return billId
	 */
	public String getBillId()
	{
		return billId;
	}

	@Override
	public boolean publish(final int sourceNodeId, final int targetNodeId)
	{
		return sourceNodeId == targetNodeId;
	}

}
