/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billexchange.outbound.impl;

import de.hybris.platform.billexchange.constants.PSBillReceiptCsvColumns;
import de.hybris.platform.billexchange.outbound.PSRawItemContributor;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * This a Public Sector Bill Receipt Contributor class which creates Bill Receipt rows and implements
 * {@link PSRawItemContributor}
 */
public class DefaultPSBillReceiptContributor implements PSRawItemContributor<PaymentTransactionModel>
{
	private final Set<String> columns = new HashSet<>(Arrays.asList(PSBillReceiptCsvColumns.CODE, PSBillReceiptCsvColumns.BILL_ID,
			PSBillReceiptCsvColumns.CURRENCY, PSBillReceiptCsvColumns.PAID_BY, PSBillReceiptCsvColumns.PAYMENT_SOURCE,
			PSBillReceiptCsvColumns.AMOUNT, PSBillReceiptCsvColumns.CREDIT_CARD_OWNER, PSBillReceiptCsvColumns.CREDIT_CARD_NUMBER,
			PSBillReceiptCsvColumns.CREDIT_CARD_TYPE, PSBillReceiptCsvColumns.VALID_TO_MONTH, PSBillReceiptCsvColumns.VALID_TO_YEAR,
			PSBillReceiptCsvColumns.CREDIT_CARD_USER, PSBillReceiptCsvColumns.BILLING_ADDRESS_TITLE,
			PSBillReceiptCsvColumns.BILLING_ADDRESS_FIRST_NAME, PSBillReceiptCsvColumns.BILLING_ADDRESS_LAST_NAME,
			PSBillReceiptCsvColumns.BILLING_ADDRESS_STREET_NAME, PSBillReceiptCsvColumns.BILLING_ADDRESS_TOWN,
			PSBillReceiptCsvColumns.BILLING_ADDRESS_COUNTRY, PSBillReceiptCsvColumns.BILLING_ADDRESS_POSTAL_CODE,
			PSBillReceiptCsvColumns.BILLING_ADDRESS_PHONE1, PSBillReceiptCsvColumns.BILLING_ADDRESS_EMAIL));

	@Override
	public Set<String> getColumns()
	{
		return columns;
	}

	@Override
	public List<Map<String, Object>> createRows(final PaymentTransactionModel paymentTransaction)
	{
		final Map<String, Object> row = new HashMap<>();
		final CreditCardPaymentInfoModel payment = (CreditCardPaymentInfoModel) paymentTransaction.getInfo();
		final List<Map<String, Object>> result = new ArrayList<>();

		row.put(PSBillReceiptCsvColumns.CODE, paymentTransaction.getCode());
		row.put(PSBillReceiptCsvColumns.BILL_ID, paymentTransaction.getPsBillPayment().getBillId());
		row.put(PSBillReceiptCsvColumns.CURRENCY, paymentTransaction.getCurrency().getIsocode());
		row.put(PSBillReceiptCsvColumns.PAID_BY, paymentTransaction.getPaidBy().getUid());
		row.put(PSBillReceiptCsvColumns.PAYMENT_SOURCE, paymentTransaction.getPaymentSource());
		row.put(PSBillReceiptCsvColumns.AMOUNT, paymentTransaction.getPlannedAmount().toString());

		setBillReceiptPaymentInfoContributor(row, payment);
		result.add(row);
		return result;
	}

	/**
	 * Sets Payment Info and Billing address details.
	 *
	 * @param row
	 * @param payment
	 */
	private void setBillReceiptPaymentInfoContributor(final Map<String, Object> row, final CreditCardPaymentInfoModel payment)
	{
		if (payment != null)
		{
			row.put(PSBillReceiptCsvColumns.CREDIT_CARD_OWNER, payment.getCcOwner());
			row.put(PSBillReceiptCsvColumns.CREDIT_CARD_NUMBER, payment.getNumber());
			row.put(PSBillReceiptCsvColumns.CREDIT_CARD_TYPE, payment.getType().getCode());
			row.put(PSBillReceiptCsvColumns.VALID_TO_MONTH, payment.getValidToMonth());
			row.put(PSBillReceiptCsvColumns.VALID_TO_YEAR, payment.getValidToYear());
			row.put(PSBillReceiptCsvColumns.CREDIT_CARD_USER, payment.getUser().getUid());

			final AddressModel billingAddress = payment.getBillingAddress();
			if (billingAddress != null)
			{
				if (billingAddress.getTitle() != null)
				{
					row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_TITLE, billingAddress.getTitle().getCode());
				}
				row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_FIRST_NAME, billingAddress.getFirstname());
				row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_LAST_NAME, billingAddress.getLastname());
				row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_STREET_NAME, billingAddress.getStreetname());
				row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_TOWN, billingAddress.getTown());
				row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_COUNTRY, billingAddress.getCountry().getIsocode());
				row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_POSTAL_CODE, billingAddress.getPostalcode());
				row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_PHONE1, billingAddress.getPhone1());
				row.put(PSBillReceiptCsvColumns.BILLING_ADDRESS_EMAIL, billingAddress.getEmail());
			}
		}
	}
}
