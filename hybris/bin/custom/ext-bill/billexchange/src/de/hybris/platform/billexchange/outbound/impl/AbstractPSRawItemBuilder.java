/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billexchange.outbound.impl;

import de.hybris.platform.billexchange.outbound.PSRawItemBuilder;
import de.hybris.platform.billexchange.outbound.PSRawItemContributor;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;




/**
 * Default raw item builder delegating the creation of the individual lines of the raw item to the registered instances
 * of {@link PSRawItemContributor}. The results are merged into one list. Fields not provided by all contributors are
 * defaulted to ""
 *
 * @param <T>
 *           The item model for which the raw item shall be assembled
 */
public abstract class AbstractPSRawItemBuilder<T extends AbstractItemModel> implements PSRawItemBuilder<T>
{
	private final Set<String> columns = new TreeSet<>();
	private List<PSRawItemContributor<T>> contributors = Collections.emptyList();

	@Override
	public List<PSRawItemContributor<T>> getContributors()
	{
		return contributors;
	}

	@Override
	public void setContributors(final List<PSRawItemContributor<T>> contributors)
	{
		this.contributors = contributors;
	}

	@Override
	public Set<String> getColumns()
	{
		if (columns.isEmpty())
		{
			for (final PSRawItemContributor<T> contributor : getContributors())
			{
				columns.addAll(contributor.getColumns());
			}
		}
		return columns;
	}

	@Override
	public List<Map<String, Object>> rowsAsNameValuePairs(final T model)
	{
		final List<Map<String, Object>> allRowsAsNameValue = new ArrayList<>();
		final Set<String> allColumns = getColumns();
		for (final PSRawItemContributor<T> contributor : contributors)
		{
			final List<Map<String, Object>> rows = contributor.createRows(model);
			for (final Map<String, Object> row : rows)
			{
				for (final String column : allColumns)
				{
					if (row.get(column) == null)
					{
						row.put(column, "");
					}
				}
				allRowsAsNameValue.add(row);
			}
		}
		return allRowsAsNameValue;
	}

	@Override
	public void addContributor(final PSRawItemContributor<T> c)
	{
		this.contributors.add(c);
	}

	protected boolean isDebug()
	{
		return getLogger().isDebugEnabled();
	}

	protected abstract Logger getLogger();

}
