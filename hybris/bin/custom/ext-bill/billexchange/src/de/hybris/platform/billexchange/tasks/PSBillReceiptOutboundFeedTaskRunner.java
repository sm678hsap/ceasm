/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billexchange.tasks;

import de.hybris.platform.billexchange.outbound.PSSendToDataHubHelper;
import de.hybris.platform.billexchange.outbound.PSSendToDataHubResult;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * TaskRunner to send a bill's receipt to datahub. Invoked when PSBillReceiptCreationEvent is published.
 */
public class PSBillReceiptOutboundFeedTaskRunner implements TaskRunner<TaskModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSBillReceiptOutboundFeedTaskRunner.class);
	private PSSendToDataHubHelper<PaymentTransactionModel> sendBillReceiptToDataHubHelper;

	@Override
	public void run(final TaskService taskService, final TaskModel taskModel) throws RetryLaterException
	{
		final PaymentTransactionModel paymentTransaction = (PaymentTransactionModel) taskModel.getContext();

		final PSSendToDataHubResult result = sendBillReceiptToDataHubHelper.createAndSendRawItem(paymentTransaction);
		if (result.isSuccess())
		{
			LOG.info("Outbound feed for Bill receipt id:" + paymentTransaction.getCode() + "is SUCCESS");
		}
		else
		{
			LOG.info("Outbound feed for Bill receipt id:" + paymentTransaction.getCode() + "has FAILED");
		}
	}

	@Override
	public void handleError(final TaskService taskService, final TaskModel taskModel, final Throwable error)
	{
		LOG.error("Outbound feed for bill receipt failed", error);
	}

	/**
	 * @return sendBillReceiptToDataHubHelper
	 */
	public PSSendToDataHubHelper<PaymentTransactionModel> getSendBillReceiptToDataHubHelper()
	{
		return sendBillReceiptToDataHubHelper;
	}

	/**
	 * @param sendBillReceiptToDataHubHelper
	 *           - sendBillReceiptToDataHubHelper to set
	 */
	@Required
	public void setSendBillReceiptToDataHubHelper(
			final PSSendToDataHubHelper<PaymentTransactionModel> sendBillReceiptToDataHubHelper)
	{
		this.sendBillReceiptToDataHubHelper = sendBillReceiptToDataHubHelper;
	}
}
