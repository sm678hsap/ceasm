/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.billexchange.inbound.events;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.apache.log4j.Logger;


/**
 * Listener for Inbound bill event
 */
public class PSInboundBillEventListener extends AbstractEventListener<PSInboundBillEvent>
{
	private static final Logger LOGGER = Logger.getLogger(PSInboundBillEventListener.class);

	/**
	 * event listener for in bound bill event
	 *
	 * @param billInboundEvent
	 *
	 */
	@Override
	protected void onEvent(final PSInboundBillEvent billInboundEvent)
	{
		LOGGER.info("on event of bill inbound event");
	}

}
