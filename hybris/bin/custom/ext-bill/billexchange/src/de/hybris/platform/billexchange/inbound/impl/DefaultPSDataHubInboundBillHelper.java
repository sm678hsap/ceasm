/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billexchange.inbound.impl;

import de.hybris.platform.billexchange.inbound.PSDataHubInboundBillHelper;
import de.hybris.platform.billexchange.inbound.events.PSInboundBillEvent;
import de.hybris.platform.servicelayer.event.EventService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * DefaultPSDataHubInboundBillHelper implementation of {@link PSDataHubInboundBillHelper}
 */
public class DefaultPSDataHubInboundBillHelper implements PSDataHubInboundBillHelper
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSDataHubInboundBillHelper.class);
	private EventService eventService;

	@Override
	public void processBillInformation(final String billId)
	{
		LOG.info("triggering the bill inbound event for bill id {}", billId);
		getEventService().publishEvent(new PSInboundBillEvent(billId));
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}
