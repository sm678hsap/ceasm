package de.hybris.platform.billexchange.inbound.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billexchange.inbound.events.PSInboundBillEvent;
import de.hybris.platform.servicelayer.event.EventService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSDataHubInboundBillHelperTest
{
	private static final String BILL_ID = "E-1001";

	@InjectMocks
	private DefaultPSDataHubInboundBillHelper classUnderTest;
	@Mock
	private EventService eventService;

	@Before
	public void setUp() throws Exception // NOSONAR
	{
		classUnderTest = new DefaultPSDataHubInboundBillHelper();
		classUnderTest.setEventService(eventService);
	}

	@Test
	public void testProcessInformation()
	{
		Mockito.doNothing().when(eventService).publishEvent(Mockito.any(PSInboundBillEvent.class));
		classUnderTest.processBillInformation(BILL_ID);
		Mockito.verify(eventService).publishEvent(Mockito.any(PSInboundBillEvent.class));
	}

}
