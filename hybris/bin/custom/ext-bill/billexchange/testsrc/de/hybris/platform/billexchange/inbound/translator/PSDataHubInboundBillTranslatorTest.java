package de.hybris.platform.billexchange.inbound.translator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billexchange.inbound.PSDataHubInboundBillHelper;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSDataHubInboundBillTranslatorTest
{
	private static final String BILL_CODE = "E-1001";

	@InjectMocks
	private PSDataHubInboundBillTranslator classUnderTest;
	@Mock
	private Item processedItem;

	@Mock
	private PSDataHubInboundBillHelper inboundHelper;

	@Before
	public void setUp()
	{
		classUnderTest.setInboundHelper(inboundHelper);
	}

	@Test
	public void testPerformImport() throws ImpExException
	{
		Mockito.doNothing().when(inboundHelper).processBillInformation(BILL_CODE);
		classUnderTest.performImport(BILL_CODE, Mockito.mock(Item.class));
		Mockito.verify(inboundHelper).processBillInformation(BILL_CODE);
	}

}
