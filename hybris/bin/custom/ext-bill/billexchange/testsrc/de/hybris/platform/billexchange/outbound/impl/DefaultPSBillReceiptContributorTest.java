/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billexchange.outbound.impl;

import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billexchange.constants.PSBillReceiptCsvColumns;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;


@SuppressWarnings("javadoc")
@UnitTest
public class DefaultPSBillReceiptContributorTest
{

	private DefaultPSBillReceiptContributor billContributor;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		billContributor = new DefaultPSBillReceiptContributor();
	}

	@Test
	public void testGetColumns()
	{
		final Set<String> columns = billContributor.getColumns();

		assertTrue(columns.contains(PSBillReceiptCsvColumns.CODE));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILL_ID));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.CURRENCY));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.PAID_BY));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.PAYMENT_SOURCE));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.AMOUNT));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.CREDIT_CARD_OWNER));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.CREDIT_CARD_NUMBER));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.CREDIT_CARD_TYPE));

		assertTrue(columns.contains(PSBillReceiptCsvColumns.VALID_TO_MONTH));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.VALID_TO_YEAR));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.CREDIT_CARD_USER));

		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_TITLE));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_FIRST_NAME));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_LAST_NAME));

		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_STREET_NAME));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_POSTAL_CODE));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_TOWN));

		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_COUNTRY));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_PHONE1));
		assertTrue(columns.contains(PSBillReceiptCsvColumns.BILLING_ADDRESS_EMAIL));
	}
}
