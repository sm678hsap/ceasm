/**
 *
 */
package de.hybris.platform.billfacades.strategies.document.generation.pdf.fop.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.bill.data.PSBillTransactionData;
import de.hybris.platform.billservices.bill.PSBillPaymentService;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test suite for {@link PSBillReceiptDataProcessStrategy}
 *
 */
@UnitTest
public class PSBillReceiptDataProcessStrategyTest
{
	@InjectMocks
	private PSBillReceiptDataProcessStrategy pSBillReceiptDataProcessStrategy;

	private static final String BILL_ID = "billId";

	private static final String RECEIPT_ID = "receiptId";

	@Mock
	private PSBillReceiptData billReceipt;

	@Mock
	private PSBillPaymentModel psBillPaymentModel;
	@Mock
	private PSBillPaymentData psBillPaymentData;
	@Mock
	private PSBillTransactionData psBillTransactionData;
	@Mock
	private PSBillReceiptData psBillReceiptData;

	@Mock
	private Converter<PSBillPaymentModel, PSBillReceiptData> psBillReceiptConverter;
	@Mock
	private PSBillPaymentService psBillPaymentService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		pSBillReceiptDataProcessStrategy = new PSBillReceiptDataProcessStrategy();
		pSBillReceiptDataProcessStrategy.setPsBillPaymentService(psBillPaymentService);
		pSBillReceiptDataProcessStrategy.setPsBillReceiptConverter(psBillReceiptConverter);
	}

	@Test
	public void getDocumentDataTest()
	{
		Mockito.when(psBillPaymentService.getBill(BILL_ID)).thenReturn(psBillPaymentModel);

		Mockito.when(psBillReceiptConverter.convert(psBillPaymentModel)).thenReturn(psBillReceiptData);


		Mockito.when(billReceipt.getBill()).thenReturn(psBillPaymentData);
		final List<PSBillTransactionData> list = new ArrayList<>();
		list.add(psBillTransactionData);
		Mockito.when(psBillPaymentData.getTransactionInfos()).thenReturn(list);
		pSBillReceiptDataProcessStrategy.getDocumentData(BILL_ID, RECEIPT_ID);

	}


}
