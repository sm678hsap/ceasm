/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.bill.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.bill.impl.DefaultPSDocumentGenerationFacade;
import de.hybris.platform.billfacades.factory.document.generation.pdf.fop.PSBillReceiptDocumentContextFactory;
import de.hybris.platform.billfacades.strategies.document.generation.pdf.fop.PSDocumentDataProcessStrategy;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.site.BaseSiteService;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.NotImplementedException;
import org.apache.velocity.VelocityContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@UnitTest
public class DefaultPSDocumentGenerationFacadeTest
{

	@InjectMocks
	DefaultPSDocumentGenerationFacade defaultPSDocumentGenerationFacade;

	@Mock
	private BaseSiteService baseSiteService;

	@Mock
	private RendererService rendererService;

	@Mock
	private BaseSiteModel baseSiteModel;

	@Mock
	private RendererTemplateModel rendererTemplateModel;

	@Mock
	private PSBillReceiptDocumentContextFactory psBillReceiptDocumentContextFactory;

	@Mock
	private VelocityContext velocityContext;

	@Mock
	PSDocumentDataProcessStrategy pSDocumentDataProcessStrategy;

	@Before
	public void beforeSetup()
	{
		MockitoAnnotations.initMocks(this);
		defaultPSDocumentGenerationFacade.setBaseSiteService(baseSiteService);
		defaultPSDocumentGenerationFacade.setRendererService(rendererService);
		defaultPSDocumentGenerationFacade.setPsBillReceiptDocumentContextFactory(psBillReceiptDocumentContextFactory);
		defaultPSDocumentGenerationFacade.setPsDocumentDataProcessStrategy(pSDocumentDataProcessStrategy);
	}

	@Test
	public void testBytesIsReturnedForValidBillId() throws IOException
	{
		final PSBillReceiptData data = new PSBillReceiptData();
		Mockito.when(pSDocumentDataProcessStrategy.getDocumentData("1", "2")).thenReturn(data);
		final HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
		Mockito.when(baseSiteService.getCurrentBaseSite()).thenReturn(new BaseSiteModel());
		final ServletOutputStream stream = Mockito.mock(ServletOutputStream.class);
		Mockito.when(httpServletResponse.getOutputStream()).thenReturn(stream);
		Mockito.when(rendererService.getRendererTemplateForCode(Mockito.anyString())).thenReturn(rendererTemplateModel);
		Mockito.when(psBillReceiptDocumentContextFactory.create(Mockito.any(PSBillReceiptData.class),
				Mockito.any(RendererTemplateModel.class))).thenReturn(velocityContext);
		defaultPSDocumentGenerationFacade.generate("PDF", data, httpServletResponse);
		Mockito.verify(rendererService).render(Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test(expected = NotImplementedException.class)
	public void testThrowsImplementedExceptionForInvalidBillId() throws IOException
	{
		final PSBillReceiptData data = new PSBillReceiptData();
		Mockito.when(pSDocumentDataProcessStrategy.getDocumentData("1", "2")).thenReturn(data);
		final HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);

		defaultPSDocumentGenerationFacade.generate("xml", data, httpServletResponse);
	}

	@Test
	public void verifyReturnsNullForBillPaymentReceiptBody()
	{
		final PSBillReceiptData billReceiptData = new PSBillReceiptData();
		final RendererTemplateModel bodyTemplate = new RendererTemplateModel();
		final String templateId = "publicsector";
		Mockito.when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
		baseSiteModel.setUid("publicsector");
		Mockito.when(rendererService.getRendererTemplateForCode(templateId)).thenReturn(rendererTemplateModel);
		Mockito.when(psBillReceiptDocumentContextFactory.create(billReceiptData, bodyTemplate)).thenReturn(velocityContext);
		Assert.assertNotNull(defaultPSDocumentGenerationFacade.getBillPaymentReceiptBody(billReceiptData));
	}

	@Test
	public void verifyReturnsNullForGenerateBillReceipt()
	{
		final String billCode = "testBillCode";
		final String receiptId = "testReceiptId";
		final PSBillReceiptData data = new PSBillReceiptData();
		Mockito.when(pSDocumentDataProcessStrategy.getDocumentData("testBillCode", "testReceiptId")).thenReturn(data);
		Assert.assertNotNull(defaultPSDocumentGenerationFacade.generateBillReceipt(billCode, receiptId));
	}

	public static byte[] hexStringToByteArray(final String s)
	{
		final int len = s.length();
		final byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2)
		{
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}
}
