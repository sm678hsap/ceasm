/**
 *
 */
package de.hybris.platform.billfacades.process.xslfo.context;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.bill.data.PSBillTransactionData;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SiteTheme;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.user.UserService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test suite for {@link AbstractPSBillReceiptContext}
 *
 */
@UnitTest
public class DefaultPSBillReceiptContextTest
{
	private static final String ENCODING_ATTRIBUTE = "encodingAttribute";

	private static final String BASE_URL = "baseUrl";

	private static final String SECURE_BASE_URL = "secureBaseUrl";

	private static final String MEDIA_BASE_URL = "mediaBaseUrl";

	private static final String MEDIA_SECURE_BASE_URL = "mediaSecureBaseUrl";

	private static final String THEME = "theme";

	private static final String BASE_SITE = "baseSite";

	private static final String BASE_THEME_URL = "baseThemeUrl";

	private static final String RECEIPT_LOGO_SUFFIX = "_bill_receipt_logo";

	private static final String CUSTOMER_DATA = "customerData";

	private static final String PAID_BY_UID = "paidByUid";

	private static final String USER_BY_UID = "userByUid";

	@InjectMocks
	private PSBillReceiptContext psBillReceiptContext;

	@Mock
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Mock
	private UrlEncoderService urlEncoderService;

	@Mock
	private MediaService mediaService;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private UserService userService;

	@Mock
	private BaseSiteModel baseSiteModel;

	@Mock
	private MediaModel mediaModel;

	@Mock
	private PSBillReceiptData psBillReceiptData;

	@Mock
	private PSBillPaymentData psBillPaymentData;

	@Mock
	private PSBillTransactionData psBillTransactionData;

	@Mock
	private CustomerData customerData;

	@Mock
	private UserModel userModel;

	@Mock
	private SiteTheme siteTheme;

	@Mock
	private CustomerModel customerModel;

	@Mock
	private CustomerType customerType;


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		psBillReceiptContext = new PSBillReceiptContext();
		psBillReceiptContext.setSiteBaseUrlResolutionService(siteBaseUrlResolutionService);
		psBillReceiptContext.setUrlEncoderService(urlEncoderService);
		psBillReceiptContext.setMediaService(mediaService);
		psBillReceiptContext.setCatalogVersionService(catalogVersionService);
		psBillReceiptContext.setUserService(userService);
		baseSiteModel.setUid(RECEIPT_LOGO_SUFFIX);
	}

	@Test
	public void testinit()
	{
		given(urlEncoderService.getUrlEncodingPattern()).willReturn(BASE_SITE);
		given(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, ENCODING_ATTRIBUTE, false, "")).willReturn(BASE_URL);
		given(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, ENCODING_ATTRIBUTE, false, ""))
				.willReturn(BASE_THEME_URL);
		given(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, ENCODING_ATTRIBUTE, true, ""))
				.willReturn(SECURE_BASE_URL);
		given(siteBaseUrlResolutionService.getMediaUrlForSite(baseSiteModel, false)).willReturn(MEDIA_BASE_URL);
		given(siteBaseUrlResolutionService.getMediaUrlForSite(baseSiteModel, true)).willReturn(MEDIA_SECURE_BASE_URL);
		given(baseSiteModel.getTheme()).willReturn(siteTheme);
		given(baseSiteModel.getTheme().getCode()).willReturn(THEME);
		given(psBillReceiptData.getBill()).willReturn(psBillPaymentData);
		given(customerData.getUid()).willReturn(CUSTOMER_DATA);
		given(psBillTransactionData.getPaidBy()).willReturn(customerData);
		given(psBillTransactionData.getPaidBy().getUid()).willReturn(USER_BY_UID);
		given(psBillReceiptData.getReceiptInfo()).willReturn(psBillTransactionData);
		given(customerModel.getType()).willReturn(CustomerType.GUEST);
		given(userService.getUserForUID(PAID_BY_UID)).willReturn(userModel);
		psBillReceiptContext.init(baseSiteModel, psBillReceiptData);
	}
}
