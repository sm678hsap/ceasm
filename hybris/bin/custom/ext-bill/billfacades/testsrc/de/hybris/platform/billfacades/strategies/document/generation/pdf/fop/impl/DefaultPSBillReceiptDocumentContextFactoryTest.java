/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.strategies.document.generation.pdf.fop.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.factory.document.generation.pdf.fop.impl.DefaultPSBillReceiptDocumentContextFactory;
import de.hybris.platform.billfacades.process.xslfo.context.PSBillReceiptContext;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.site.BaseSiteService;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;


/**
 * Unit test of {@link DefaultPSBillReceiptDocumentContextFactory}
 */
@UnitTest
public class DefaultPSBillReceiptDocumentContextFactoryTest
{
	private static final String SAMPLE_SITE = "sample-site";

	@InjectMocks
	private DefaultPSBillReceiptDocumentContextFactory billReceiptDocumentContextFactory;

	@Mock
	private BaseSiteService baseSiteService;

	@Mock
	private RendererTemplateModel rendererTemplate;

	@Mock
	private PSBillReceiptContext pSBillReceiptContext;

	@Mock
	private ApplicationContext applicationContext;

	@Mock
	private PSBillReceiptData billReceiptData;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		billReceiptDocumentContextFactory = new DefaultPSBillReceiptDocumentContextFactory()
		{
			@Override
			protected ApplicationContext getApplicationContext()
			{
				return applicationContext;
			}
		};
		billReceiptDocumentContextFactory.setBaseSiteService(baseSiteService);

		final BaseSiteModel baseSiteModel = new BaseSiteModel();
		baseSiteModel.setUid(SAMPLE_SITE);
		Mockito.when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
	}

	@Test
	public void shouldReturnValidContextForValidTemplate()
	{
		Mockito.when(rendererTemplate.getContextClass()).thenReturn(PSBillReceiptContext.class.getName());
		Mockito.when(applicationContext.getBeansOfType(PSBillReceiptContext.class))
				.thenReturn(Collections.singletonMap("billReceiptContext", pSBillReceiptContext));

		Assert.assertNotNull(billReceiptDocumentContextFactory.create(billReceiptData, rendererTemplate));
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionWhenRendererTemplateIsNull()
	{
		billReceiptDocumentContextFactory.create(billReceiptData, null);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionIfContextBeanIsNotAvailable()
	{
		Mockito.when(rendererTemplate.getContextClass()).thenReturn(PSBillReceiptContext.class.getName());
		Mockito.when(applicationContext.getBean(PSBillReceiptContext.class)).thenReturn(null);

		billReceiptDocumentContextFactory.create(billReceiptData, rendererTemplate);
	}
}
