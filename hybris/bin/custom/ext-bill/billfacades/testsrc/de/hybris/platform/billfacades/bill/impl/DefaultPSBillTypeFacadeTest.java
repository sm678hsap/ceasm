/**
 *
 */
package de.hybris.platform.billfacades.bill.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billfacades.bill.data.PSBillMessageData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;
import de.hybris.platform.billservices.bill.PSBillMessageService;
import de.hybris.platform.billservices.bill.PSBillTypeService;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test suite for {@link DefaultPSBillTypeFacade }
 *
 */
@UnitTest
public class DefaultPSBillTypeFacadeTest
{

	private static final String CODE = "code";

	private static final String BILL_PK = "billPk";

	private static final String BILL_TYPE_CODE = "billTypeCode";

	@InjectMocks
	private DefaultPSBillTypeFacade publicSectorBillTypeFacade;

	@Mock
	private PSBillMessageService psBillMessageService;

	@Mock
	private Converter<PSBillMessageModel, PSBillMessageData> psBillMessageConverter;

	@Mock
	private PSBillTypeService psBillTypeService;

	@Mock
	private Converter<PSBillTypeModel, PSBillTypeData> psBillTypeConverter;

	@Mock
	private PSBillTypeModel psBillTypeModel;

	@Mock
	private PSBillMessageModel psBillMessageModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		publicSectorBillTypeFacade = new DefaultPSBillTypeFacade();
		publicSectorBillTypeFacade.setPsBillMessageConverter(psBillMessageConverter);
		publicSectorBillTypeFacade.setPsBillMessageService(psBillMessageService);
		publicSectorBillTypeFacade.setPsBillTypeConverter(psBillTypeConverter);
		publicSectorBillTypeFacade.setPsBillTypeService(psBillTypeService);

	}

	@Test
	public void testGetBillType()
	{
		given(psBillTypeService.getBillType(CODE)).willReturn(psBillTypeModel);
		publicSectorBillTypeFacade.getBillType(CODE);
	}

	@Test
	public void testGetBillTypeByBill()
	{
		given(psBillTypeService.getBillTypeByBill(BILL_PK)).willReturn(psBillTypeModel);
		publicSectorBillTypeFacade.getBillTypeByBill(BILL_PK);
	}

	@Test
	public void testGetMessagesByBillType()
	{
		final List<PSBillMessageModel> messages = new ArrayList<>();
		given(psBillMessageService.getMessagesByBillType(BILL_TYPE_CODE)).willReturn(messages);
		messages.add(psBillMessageModel);
		publicSectorBillTypeFacade.getMessagesByBillType(BILL_TYPE_CODE);
	}

}