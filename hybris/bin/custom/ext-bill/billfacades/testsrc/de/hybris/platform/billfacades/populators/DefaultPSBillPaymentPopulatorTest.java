/**
 *
 */
package de.hybris.platform.billfacades.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.enums.BillPaymentType;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test suite for {@link PSBillPaymentData}
 *
 */
@UnitTest
public class DefaultPSBillPaymentPopulatorTest
{
	private static final String BILL_DESCRIPTION = "billDescription";

	private static final String BILL_ID = "billId";

	private static final String BILL_UNIT_ID = "billUnitId";

	private static final String CODE = "code";

	private static final String CUSTOMER_REFERENCE_NUMBER = "customerReferenceNumber";

	private static final String EMAIL_ON_RECORD = "emailOnRecord";

	private static final String FULL_BILL_URL = "fullBillUrl";

	private static final String LAST_NAME = "lastName";

	private static final BigDecimal SUB_TOTAL_VALUE = BigDecimal.valueOf(100);

	private static final BigDecimal SUB_TOTAL_VALUE_NEW = BigDecimal.valueOf(200);

	@InjectMocks
	private PSBillPaymentPopulator publicSectorBillPaymentPopulator;

	@Mock
	private UserService userService;

	@Mock
	private Converter<CreditCardPaymentInfoModel, PaymentInfoData> paymentInfoDataConverter;

	@Mock
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;

	@Mock
	private Converter<PSBillTypeModel, PSBillTypeData> psBillTypeConverter;

	@Mock
	private Converter<CurrencyModel, CurrencyData> currencyConverter;

	@Mock
	private Converter<UserModel, CustomerData> customerConverter;

	@Mock
	private PSBillPaymentModel psBillPaymentModel;

	@Mock
	private PSBillPaymentData psBillPaymentData;

	@Mock
	private CustomerData customerData;

	@Mock
	private CustomerModel customerModel;

	@Mock
	private BillPaymentStatus billPaymentStatus;

	@Mock
	private BillPaymentType billPaymentType;

	@Mock
	private PaymentTransactionModel paymentTransactionModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		customerData = new CustomerData();
		customerModel = new CustomerModel();
		publicSectorBillPaymentPopulator = new PSBillPaymentPopulator();
		publicSectorBillPaymentPopulator.setUserService(userService);
		publicSectorBillPaymentPopulator.setCreditCardPaymentInfoConverter(creditCardPaymentInfoConverter);
		publicSectorBillPaymentPopulator.setCurrencyConverter(currencyConverter);
		publicSectorBillPaymentPopulator.setCustomerConverter(customerConverter);
		publicSectorBillPaymentPopulator.setPaymentInfoDataConverter(paymentInfoDataConverter);
		publicSectorBillPaymentPopulator.setPsBillTypeConverter(psBillTypeConverter);
	}


	@Test
	public void testPopulate() throws ConversionException
	{
		final PSBillTypeModel psBillTypeModel = new PSBillTypeModel();
		final PaymentInfoModel paymentInfoModel = new PaymentInfoModel();
		given(psBillPaymentModel.getBillDescription()).willReturn(BILL_DESCRIPTION);
		given(psBillPaymentModel.getBillDuration()).willReturn(Integer.valueOf(10));
		given(psBillPaymentModel.getBillId()).willReturn(BILL_ID);
		given(psBillPaymentModel.getBillUnitId()).willReturn(BILL_UNIT_ID);
		given(psBillPaymentModel.getCode()).willReturn(CODE);
		given(psBillPaymentModel.getCustomerReferenceNumber()).willReturn(CUSTOMER_REFERENCE_NUMBER);
		given(psBillPaymentModel.getEmailOnRecord()).willReturn(EMAIL_ON_RECORD);
		given(psBillPaymentModel.getFullBillUrl()).willReturn(FULL_BILL_URL);
		given(psBillPaymentModel.getLastName()).willReturn(LAST_NAME);
		given(psBillPaymentModel.getCustomer()).willReturn(customerModel);
		given(psBillPaymentModel.getTotalBillAmount()).willReturn(SUB_TOTAL_VALUE);
		given(psBillPaymentModel.getBillPaymentStatus()).willReturn(billPaymentStatus);
		given(psBillPaymentModel.getBillPaymentType()).willReturn(billPaymentType);
		given(psBillPaymentModel.getOutstandingBillAmount()).willReturn(SUB_TOTAL_VALUE_NEW);
		given(psBillPaymentModel.getBillType()).willReturn(psBillTypeModel);
		given(psBillPaymentModel.getPaymentInfo()).willReturn(paymentInfoModel);
		final CurrencyModel currencyModel = new CurrencyModel();
		given(psBillPaymentModel.getCurrency()).willReturn(currencyModel);
		final Date date = new Date();
		given(psBillPaymentModel.getBillDueDate()).willReturn(date);
		given(psBillPaymentModel.getBillDate()).willReturn(new Date());
		final Set<PaymentTransactionModel> result = new HashSet<>();
		result.add(paymentTransactionModel);
		given(psBillPaymentModel.getPaymentTransactions()).willReturn(result);
		psBillPaymentData = new PSBillPaymentData();
		publicSectorBillPaymentPopulator.populate(psBillPaymentModel, psBillPaymentData);

	}
}
