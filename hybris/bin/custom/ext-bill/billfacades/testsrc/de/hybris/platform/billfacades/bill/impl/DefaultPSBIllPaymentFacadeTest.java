/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.bill.impl;

import static org.mockito.Matchers.any;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;
import de.hybris.platform.billservices.bill.PSBillPaymentService;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * DefaultPSBIllPaymentFacadeTest
 *
 *
 */
@UnitTest
public class DefaultPSBIllPaymentFacadeTest
{
	@InjectMocks
	DefaultPSBillPaymentFacade psBillPaymentFacade;

	@Mock
	private I18NService i18nService;

	@Mock
	private CustomerAccountService customerAccountService;

	@Mock
	private UserService userService;

	@Mock
	private PSBillPaymentService psBillPaymentService;

	@Mock
	private CommerceCheckoutService commerceCheckoutService;

	@Mock
	private ModelService modelService;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private Converter<UserModel, CustomerData> customerConverter;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private CartService cartService;

	@Mock
	private PageableData pageableData;

	@Mock
	private List<BillPaymentStatus> statuses;

	@Mock
	private Converter<PSBillPaymentModel, PSBillPaymentData> psBillConverter;


	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		psBillPaymentFacade = new DefaultPSBillPaymentFacade();
		/*
		 * psBillPaymentService = new DefaultPSBillPaymentService(); customerAccountService = new
		 * DefaultCustomerAccountService(); i18nService = new DefaultI18NService(); commerceCheckoutService = new
		 * DefaultCommerceCheckoutService();
		 */

		psBillPaymentFacade.setI18nService(i18nService);
		psBillPaymentFacade.setCustomerAccountService(customerAccountService);
		psBillPaymentFacade.setUserService(userService);
		psBillPaymentFacade.setPsBillPaymentService(psBillPaymentService);
		psBillPaymentFacade.setCommerceCheckoutService(commerceCheckoutService);
		psBillPaymentFacade.setModelService(modelService);
		psBillPaymentFacade.setCommonI18NService(commonI18NService);
		psBillPaymentFacade.setCustomerConverter(customerConverter);
		psBillPaymentFacade.setCustomerFacade(customerFacade);
		psBillPaymentFacade.setCartService(cartService);
		psBillPaymentFacade.setPsBillConverter(psBillConverter);
	}

	@Test
	public void testIsAmountValidForPaymentMethod()
	{
		final PSBillPaymentData billPaymentData = new PSBillPaymentData();
		billPaymentData.setOutstandingBillAmount(BigDecimal.valueOf(297.45));
		final PSBillTypeData billType = Mockito.mock(PSBillTypeData.class);

		billType.setIsOverPaymentAllowed(Boolean.TRUE);
		billPaymentData.setBillType(billType);
		Assert.assertTrue(psBillPaymentFacade.isAmountValidForPayment(new Double(29.45), billPaymentData));

		billType.setIsOverPaymentAllowed(Boolean.FALSE);
		billPaymentData.setBillType(billType);
		Assert.assertFalse(psBillPaymentFacade.isAmountValidForPayment(new Double(297.46), billPaymentData));

		Assert.assertTrue(psBillPaymentFacade.isAmountValidForPayment(new Double(297.45), billPaymentData));

	}

	@Test
	public void testIsBillOverDueMethod()
	{
		final PSBillPaymentData billPaymentData = new PSBillPaymentData();
		billPaymentData.setBillDueDate(getTomorrowDate());

		final PSBillTypeData billTypeData = new PSBillTypeData();
		billTypeData.setIsPaymentAllowed(Boolean.TRUE);
		billPaymentData.setBillType(billTypeData);

		Assert.assertFalse(psBillPaymentFacade.isBillOverDue(billPaymentData).booleanValue());

		billPaymentData.setBillDueDate(getYesterdayDate());
		billTypeData.setIsPaymentAllowed(Boolean.FALSE);
		billPaymentData.setBillType(billTypeData);

		Assert.assertTrue(psBillPaymentFacade.isBillOverDue(billPaymentData).booleanValue());
	}

	@Test
	public void testGetBillsByStatusList()
	{
		final List<PSBillPaymentModel> bills = new ArrayList<PSBillPaymentModel>();
		final SearchPageData<PSBillPaymentModel> billResults = Mockito.mock(SearchPageData.class);
		Mockito.when(billResults.getResults()).thenReturn(bills);
		final CustomerData customer = new CustomerData();
		final CustomerModel customerModel = new CustomerModel();
		customer.setUid("test");
		Mockito.when(userService.getUserForUID(customer.getUid())).thenReturn(customerModel);

		pageableData.setCurrentPage(0);
		pageableData.setPageSize(100);

		SearchPageData result = new SearchPageData();

		final List<PSBillPaymentData> list1 = new ArrayList<PSBillPaymentData>();
		Mockito.when(psBillPaymentService.getBillsByStatusList(customerModel, pageableData, statuses)).thenReturn(billResults);
		Mockito.when(Converters.convertAll(billResults.getResults(), psBillConverter)).thenReturn(list1);
		result = psBillPaymentFacade.getBillsByStatusList(customer, pageableData, statuses);
		Assert.assertTrue(result.getResults().isEmpty());

		final List<PSBillPaymentData> list = new ArrayList<PSBillPaymentData>();
		list.add(Mockito.mock(PSBillPaymentData.class));
		Mockito.when(psBillPaymentService.getBillsByStatusList(customerModel, pageableData, statuses)).thenReturn(billResults);
		Mockito.when(Converters.convertAll(billResults.getResults(), psBillConverter)).thenReturn(list);
		result = psBillPaymentFacade.getBillsByStatusList(customer, pageableData, statuses);
		Assert.assertFalse(result.getResults().isEmpty());
	}

	@Test
	public void testProcessPaymentNullPaymentTransaction() throws Exception // NOSONAR
	{
		final String billCode = "TestBill";
		final BigDecimal amount = BigDecimal.valueOf(2.0);
		final String securityCode = "TestSecurityCode";
		final String paymentProvider = "TestPaymentProvider";

		final String currency = "USD";
		final Currency currencyObj = Currency.getInstance("USD");
		final String paymentId = "TestPaymentId";
		final CreditCardPaymentInfoModel ccPaymentInfoModel = new CreditCardPaymentInfoModel();

		final PaymentTransactionEntryModel paymentTransactionEntryModel = new PaymentTransactionEntryModel();
		paymentTransactionEntryModel.setCode("TestTransactionEntryModel");

		final CustomerModel customer = new CustomerModel();
		customer.setUid("TestUser");
		final String merchantCode = customer.getUid() + "-" + UUID.randomUUID();
		Mockito.when(i18nService.getBestMatchingJavaCurrency(currency)).thenReturn(currencyObj);

		Mockito.when(userService.getCurrentUser()).thenReturn(customer);
		Mockito
				.when(customerAccountService.getCreditCardPaymentInfoForCode((CustomerModel) userService.getCurrentUser(), paymentId))
				.thenReturn(ccPaymentInfoModel);

		Mockito.when(commerceCheckoutService.getPaymentProvider()).thenReturn(paymentProvider);


		Mockito.when(psBillPaymentService.processPayment(billCode, ccPaymentInfoModel, amount, currencyObj, securityCode,
				paymentProvider, merchantCode)).thenReturn(paymentTransactionEntryModel);


		Assert.assertNull(psBillPaymentFacade.processPayment(billCode, paymentId, amount, currency, securityCode));
	}

	@Test
	public void testProcessPayment() throws Exception // NOSONAR
	{
		final String billCode = "TestBill";
		final BigDecimal amount = BigDecimal.valueOf(2.0);
		final String securityCode = "TestSecurityCode";
		final String paymentProvider = "TestPaymentProvider";

		final String currency = "USD";
		final Currency currencyObj = Currency.getInstance("USD");
		final String paymentId = "TestPaymentId";
		final CreditCardPaymentInfoModel ccPaymentInfoModel = new CreditCardPaymentInfoModel();

		final PaymentTransactionEntryModel paymentTransactionEntryModel = new PaymentTransactionEntryModel();
		final PaymentTransactionModel paymentTransactionModel = new PaymentTransactionModel();
		paymentTransactionModel.setCode("TestTransactionModel");
		paymentTransactionEntryModel.setPaymentTransaction(paymentTransactionModel);

		final CustomerModel customer = new CustomerModel();
		customer.setUid("TestUser");
		Mockito.when(i18nService.getBestMatchingJavaCurrency(currency)).thenReturn(currencyObj);

		Mockito.when(userService.getCurrentUser()).thenReturn(customer);
		Mockito
				.when(customerAccountService.getCreditCardPaymentInfoForCode((CustomerModel) userService.getCurrentUser(), paymentId))
				.thenReturn(ccPaymentInfoModel);

		Mockito.when(commerceCheckoutService.getPaymentProvider()).thenReturn(paymentProvider);

		Mockito.when(psBillPaymentService.processPayment(any(), any(), any(), any(), any(), any(), any()))
				.thenReturn(paymentTransactionEntryModel);

		Assert.assertEquals(psBillPaymentFacade.processPayment(billCode, paymentId, amount, currency, securityCode),
				"TestTransactionModel");
	}

	@Test
	public void testCreateGuestUserForBillPayment() throws Exception // NOSONAR
	{
		final LanguageModel languageModel = new LanguageModel();
		final CurrencyModel currencyModel = new CurrencyModel();

		final CustomerModel customer = new CustomerModel();
		customer.setUid("TestUser");
		final CustomerData customerData = new CustomerData();

		final CustomerModel guestCustomer = new CustomerModel();
		guestCustomer.setUid("GuestUser");

		Mockito.when(userService.getCurrentUser()).thenReturn(customer);
		Mockito.when(new Boolean(userService.isAnonymousUser(customer))).thenReturn(Boolean.TRUE);

		Mockito.when(modelService.create(CustomerModel.class)).thenReturn(guestCustomer);

		Mockito.when(commonI18NService.getCurrentLanguage()).thenReturn(languageModel);
		Mockito.when(commonI18NService.getCurrentCurrency()).thenReturn(currencyModel);

		Mockito.when(userService.getUserForUID(guestCustomer.getUid())).thenReturn(customer);

		Mockito.when(customerConverter.convert(customer)).thenReturn(customerData);

		final GrantedAuthority auth = new SimpleGrantedAuthority("admin");

		final ArrayList authList = new ArrayList<>();
		authList.add(auth);

		final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
				guestCustomer.getUid(), null, authList);
		newAuthentication.setDetails(customer);

		SecurityContextHolder.getContext().setAuthentication(newAuthentication);

		psBillPaymentFacade.createGuestUserForBillPayment("TestBillId", "TestBillName");
	}


	@Test
	public void testUpdateGuestUserEmail()
	{
		final CustomerModel customer = new CustomerModel();
		customer.setUid("TestUser");
		customer.setOriginalUid("TestUser");
		customer.setType(CustomerType.GUEST);

		final CustomerData customerData = new CustomerData();
		customerData.setUid("TestUserId");

		Mockito.when(userService.getCurrentUser()).thenReturn(customer);
		Mockito.when(customerFacade.getCurrentCustomer()).thenReturn(customerData);


		final GrantedAuthority auth = new SimpleGrantedAuthority("admin");

		final ArrayList authList = new ArrayList<>();
		authList.add(auth);

		final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(customer.getUid(),
				null, authList);
		newAuthentication.setDetails(customer);

		SecurityContextHolder.getContext().setAuthentication(newAuthentication);

		psBillPaymentFacade.updateGuestUserEmail("NewEmailId");

		Assert.assertNotEquals("TestUser", customer.getUid());
		Assert.assertTrue(customer.getUid().contains("NewEmailId"));

	}


	@Test
	public void testIsCurrentUserRegistered()
	{

		final CustomerModel customer = new CustomerModel();
		customer.setUid("TestUser");
		customer.setType(CustomerType.REGISTERED);

		Mockito.when(userService.getCurrentUser()).thenReturn(customer);

		Assert.assertTrue(psBillPaymentFacade.isCurrentUserRegistered());

		customer.setType(CustomerType.GUEST);

		Mockito.when(userService.getCurrentUser()).thenReturn(customer);

		Assert.assertFalse(psBillPaymentFacade.isCurrentUserRegistered());
	}


	@Test
	public void testIsCurrentUserGuest()
	{

		final CustomerModel customer = new CustomerModel();
		customer.setUid("TestUser");
		customer.setType(CustomerType.GUEST);

		Mockito.when(userService.getCurrentUser()).thenReturn(customer);

		Assert.assertTrue(psBillPaymentFacade.isCurrentUserGuest());


		customer.setType(CustomerType.REGISTERED);

		Mockito.when(userService.getCurrentUser()).thenReturn(customer);

		Assert.assertFalse(psBillPaymentFacade.isCurrentUserGuest());
	}

	private Date getYesterdayDate()
	{
		return new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
	}

	private Date getTomorrowDate()
	{
		return new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000);
	}

	@Test
	public void testBillConfirmationForGuestUserEmail()
	{
		final String testString = "sample.citizen@stateofrosebud.com";
		final String guestUserEmail = "98chh86fhh|sample.citizen@stateofrosebud.com";
		Assert.assertEquals(testString, psBillPaymentFacade.guestUserEmailForBillPayment(guestUserEmail));
	}



}
