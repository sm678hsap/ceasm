/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Utility class to generate a PDF using Apache FOP library
 */
public class PSFopDocumentGenerationUtils
{
	private static final Logger LOG = LoggerFactory.getLogger(PSFopDocumentGenerationUtils.class);

	private static final String TRANSFORMER_FACTORY = "org.apache.xalan.xsltc.trax.TransformerFactoryImpl";

	private static final byte[] EMPTY_BYTE_ARRAY = {};

	private PSFopDocumentGenerationUtils()
	{
	}

	/**
	 * Generates a PDF from the given body parameter using Apache FOP library
	 *
	 * @param body
	 * @return byte[]
	 */
	public static byte[] generatePdf(final String body)
	{

		// Source for dynamic variables
		final StreamSource source = new StreamSource(new StringReader("<root></root>"));
		// Source for file template xsl-fo
		StreamSource transformSource;
		// FOP Factory instance
		transformSource = new StreamSource(new StringReader(body));
		try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream())
		{
			final FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
			// FOP Agent for transformation
			final FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
			foUserAgent.setAccessibility(true);

			final Transformer xslTransfromer = getXSLTransformer(transformSource);
			final Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, outputStream);
			final Result res = new SAXResult(fop.getDefaultHandler());
			xslTransfromer.transform(source, res);
			LOG.info("PDF generated successfully !");
			return outputStream.toByteArray();
		}
		catch (final TransformerException | IOException | FOPException e)
		{
			LOG.error(e.getMessage(), e);
		}
		return EMPTY_BYTE_ARRAY;
	}


	/**
	 * Get the xml transformer by given stream source with secure processing feature enabled.
	 *
	 * @param source
	 *           the stream source
	 * @return xml transformer
	 * @throws TransformerConfigurationException
	 */
	public static Transformer getXSLTransformer(final StreamSource source) throws TransformerConfigurationException
	{
		final TransformerFactory factory = TransformerFactory.newInstance(TRANSFORMER_FACTORY, null);
		factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		return factory.newTransformer(source);
	}
}
