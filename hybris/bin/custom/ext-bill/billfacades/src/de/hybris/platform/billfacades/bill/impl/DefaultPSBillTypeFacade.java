/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billfacades.bill.impl;

import de.hybris.platform.billfacades.bill.PSBillTypeFacade;
import de.hybris.platform.billfacades.bill.data.PSBillMessageData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;
import de.hybris.platform.billservices.bill.PSBillMessageService;
import de.hybris.platform.billservices.bill.PSBillTypeService;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation class for {@link PSBillTypeFacade}
 */
public class DefaultPSBillTypeFacade implements PSBillTypeFacade
{
	private PSBillMessageService psBillMessageService;
	private PSBillTypeService psBillTypeService;
	private Converter<PSBillMessageModel, PSBillMessageData> psBillMessageConverter;
	private Converter<PSBillTypeModel, PSBillTypeData> psBillTypeConverter;

	@Override
	public PSBillTypeData getBillType(final String code)
	{
		final PSBillTypeModel billType = getPsBillTypeService().getBillType(code);
		return getPsBillTypeConverter().convert(billType);
	}

	@Override
	public PSBillTypeData getBillTypeByBill(final String billPk)
	{
		final PSBillTypeModel billType = getPsBillTypeService().getBillTypeByBill(billPk);
		return getPsBillTypeConverter().convert(billType);
	}

	@Override
	public List<PSBillMessageData> getMessagesByBillType(final String billTypeCode)
	{
		final List<PSBillMessageModel> messages = getPsBillMessageService().getMessagesByBillType(billTypeCode);
		if (CollectionUtils.isNotEmpty(messages))
		{
			return getPsBillMessageConverter().convertAll(messages);
		}
		return Collections.emptyList();
	}

	protected PSBillMessageService getPsBillMessageService()
	{
		return psBillMessageService;
	}

	@Required
	public void setPsBillMessageService(final PSBillMessageService psBillMessageService)
	{
		this.psBillMessageService = psBillMessageService;
	}

	protected Converter<PSBillMessageModel, PSBillMessageData> getPsBillMessageConverter()
	{
		return psBillMessageConverter;
	}

	@Required
	public void setPsBillMessageConverter(final Converter<PSBillMessageModel, PSBillMessageData> psBillMessageConverter)
	{
		this.psBillMessageConverter = psBillMessageConverter;
	}

	protected PSBillTypeService getPsBillTypeService()
	{
		return psBillTypeService;
	}

	@Required
	public void setPsBillTypeService(final PSBillTypeService psBillTypeService)
	{
		this.psBillTypeService = psBillTypeService;
	}

	protected Converter<PSBillTypeModel, PSBillTypeData> getPsBillTypeConverter()
	{
		return psBillTypeConverter;
	}

	@Required
	public void setPsBillTypeConverter(final Converter<PSBillTypeModel, PSBillTypeData> psBillTypeConverter)
	{
		this.psBillTypeConverter = psBillTypeConverter;
	}
}
