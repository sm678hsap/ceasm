/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.process.xslfo.context;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;


/**
 * Bill Receipt Context for populating Bill Receipt templates
 */
public class PSBillReceiptContext extends AbstractPSBillReceiptContext
{

	@Override
	public void init(final BaseSiteModel baseSite, final PSBillReceiptData billReceiptData)
	{
		super.init(baseSite, billReceiptData);
	}

}
