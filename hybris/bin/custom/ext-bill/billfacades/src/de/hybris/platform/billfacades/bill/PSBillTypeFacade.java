/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billfacades.bill;

import de.hybris.platform.billfacades.bill.data.PSBillMessageData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;

import java.util.List;


/**
 * Interface for PSBillType facade implementation.
 */
public interface PSBillTypeFacade
{
	/**
	 * Gets bill type by code.
	 *
	 * @param code
	 *           String the bill type code
	 * @return PSBillTypeData
	 */
	PSBillTypeData getBillType(final String code);

	/**
	 * Gets bill type by bill pk.
	 *
	 * @param billPk
	 *           String the bill's PK.
	 * @return PSBillTypeData
	 */
	PSBillTypeData getBillTypeByBill(final String billPk);

	/**
	 * Gets bill messages by bill type.
	 *
	 * @param billTypeCode
	 *           String the Bill's type code.
	 *
	 * @return List<PSBillMessageData>
	 */
	List<PSBillMessageData> getMessagesByBillType(final String billTypeCode);
}
