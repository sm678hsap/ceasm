/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billfacades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillTransactionData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Populates {@link PSBillPaymentData}
 */
public class PSBillPaymentPopulator implements Populator<PSBillPaymentModel, PSBillPaymentData>
{
	private Converter<CreditCardPaymentInfoModel, PaymentInfoData> paymentInfoDataConverter;
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;
	private Converter<PSBillTypeModel, PSBillTypeData> psBillTypeConverter;
	private Converter<CurrencyModel, CurrencyData> currencyConverter;
	private Converter<UserModel, CustomerData> customerConverter;
	private UserService userService;

	@Override
	public void populate(final PSBillPaymentModel source, final PSBillPaymentData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setBillDescription(source.getBillDescription());
		target.setBillDuration(source.getBillDuration());
		target.setBillId(source.getBillId());
		target.setBillUnitId(source.getBillUnitId());
		target.setCode(source.getCode());
		target.setCustomerReferenceNumber(source.getCustomerReferenceNumber());
		target.setEmailOnRecord(source.getEmailOnRecord());
		target.setFullBillUrl(source.getFullBillUrl());
		target.setLastName(source.getLastName());
		if (source.getCustomer() != null)
		{
			target.setCustomer(getCustomerConverter().convert(source.getCustomer()));
		}
		populatePaymentAttributes(source, target);
		addBillType(source, target);
		addPaymentInformation(source, target);
		addCurrency(source, target);
		addTransactionInfo(source.getPaymentTransactions(), target);
	}

	/**
	 * Adds TransactionInfo to bill data.
	 *
	 * @param paymentTransactions Set<PaymentTransactionModel> to be populated as a list of PSBillTransactionData and added to the PS Bill Payment Data
	 * @param target PSBillPaymentData that will receive the new list of PSBillTransactionData
	 */
	protected void addTransactionInfo(final Set<PaymentTransactionModel> paymentTransactions, final PSBillPaymentData target)
	{

		final List<PSBillTransactionData> billTransactions = new ArrayList<>();
		for (final PaymentTransactionModel paymentTransaction : paymentTransactions)
		{
			final PSBillTransactionData bill = new PSBillTransactionData();
			final PaymentInfoModel paymentInfo = paymentTransaction.getInfo();
			bill.setTransactionId(paymentTransaction.getCode());
			final String receiptId = paymentTransaction.getCode();
			if (paymentInfo instanceof CreditCardPaymentInfoModel)
			{
				final CCPaymentInfoData paymentInfoData = getCreditCardpaymentInfoConverter()
						.convert((CreditCardPaymentInfoModel) paymentInfo);
				bill.setPaymentInfo(paymentInfoData);
			}
			final Optional<PaymentTransactionEntryModel> transactionEntry = paymentTransaction.getEntries().stream().findFirst();
			if (transactionEntry.isPresent())
			{
				final PaymentTransactionEntryModel transaction = transactionEntry.get();
				bill.setCurrency(transaction.getCurrency().getSymbol());
				bill.setPaidAmount(transaction.getAmount());
				final BigDecimal bdOutstandingAmount = transaction.getPaymentTransaction().getOutstandingAmount();
				bill.setOutstandingAmount(bdOutstandingAmount.setScale(2, RoundingMode.HALF_UP));
				bill.setPaidDate(transaction.getTime());
				bill.setPaidBy(getCustomerConverter().convert(transaction.getPaymentTransaction().getPaidBy()));
				final BigDecimal bdOverPaidAmount = transaction.getPaymentTransaction().getOverPaidAmount();
				bill.setOverPaidAmount(bdOverPaidAmount.setScale(2, RoundingMode.HALF_UP));

				//get customer who paid the bill and check whether it's a GUEST customer type or not
				CustomerModel paidByCustomer = (CustomerModel) getUserService().getUserForUID(bill.getPaidBy().getUid());
				if ( null != paidByCustomer && CustomerType.GUEST.equals(paidByCustomer.getType()) )
				{
					final String receiptIdForGuestUser = StringUtils.substringAfter(receiptId, "|");
					bill.setReceiptId(
							receiptIdForGuestUser.substring(receiptIdForGuestUser.indexOf('-') + 1, receiptIdForGuestUser.length()));
				}
				else
				{
					bill.setReceiptId(receiptId.substring(receiptId.indexOf('-') + 1, receiptId.length()));
				}
			}
			billTransactions.add(bill);
		}
		target.setTransactionInfos(billTransactions);
	}

	/**
	 * Populates the bill payment attributes.
	 *
	 * @param source PSBillPaymentModel used to populate PSBillPaymentData
	 * @param target PSBillPaymentData data object to be populated by bill payment model
	 */
	protected void populatePaymentAttributes(final PSBillPaymentModel source, final PSBillPaymentData target)
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setBillPaymentStatus(source.getBillPaymentStatus());
		target.setBillPaymentType(source.getBillPaymentType());
		target.setTotalBillAmount(source.getTotalBillAmount().setScale(2, RoundingMode.HALF_UP));
		target.setOutstandingBillAmount(source.getOutstandingBillAmount().setScale(2, RoundingMode.HALF_UP));
		target.setBillDueDate(source.getBillDueDate());
		target.setBillDate(source.getBillDate());
		target.setBillOverDue(isBillOverDue(source));
	}

	/**
	 * Checks if bill is overdue
	 *
	 * @param source
	 *           PSBillPaymentModel source
	 * @return boolean
	 */
	private static boolean isBillOverDue(final PSBillPaymentModel source)
	{
		final LocalDate currentTime = LocalDate.now();
		final LocalDate billDueDate = source.getBillDueDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return currentTime.isAfter(billDueDate);
	}

	/**
	 * Adds bill type to bill data.
	 *
     * @param source PSBillPaymentModel used to get the PSBillTypeModel and convert it into PSBillTypeData
     * @param target PSBillPaymentData data object that will receive the converted PSBillTypeData
	 */
	protected void addBillType(final PSBillPaymentModel source, final PSBillPaymentData target)
	{
		final PSBillTypeModel billType = source.getBillType();
		final PSBillTypeData billTypeData = getPsBillTypeConverter().convert(billType);
		target.setBillType(billTypeData);
	}

	/**
	 * Adds bill payment information to bill data.
	 *
     * @param source PSBillPaymentModel used to get the PaymentInfoModel and if it's type CreditCardPayment convert it into PaymentInfoData
     * @param target PSBillPaymentData data object that will receive the converted PaymentInfoData
	 */
	protected void addPaymentInformation(final PSBillPaymentModel source, final PSBillPaymentData target)
	{
		final PaymentInfoModel paymentInfo = source.getPaymentInfo();
		if (paymentInfo instanceof CreditCardPaymentInfoModel)
		{
			final PaymentInfoData paymentInfoData = getPaymentInfoDataConverter().convert((CreditCardPaymentInfoModel) paymentInfo);
			target.setPaymentInfo(paymentInfoData);
		}
	}

	/**
	 * Adds currency to bill data.
	 *
     * @param source PSBillPaymentModel used to get the CurrencyModel and convert it to CurrencyData
     * @param target PSBillPaymentData data object that will receive the converted CurrencyData
	 */
	protected void addCurrency(final PSBillPaymentModel source, final PSBillPaymentData target)
	{
		final CurrencyModel currency = source.getCurrency();
		final CurrencyData currencyData = getCurrencyConverter().convert(currency);
		target.setCurrency(currencyData);
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	protected Converter<PSBillTypeModel, PSBillTypeData> getPsBillTypeConverter()
	{
		return psBillTypeConverter;
	}

	@Required
	public void setPsBillTypeConverter(final Converter<PSBillTypeModel, PSBillTypeData> psBillTypeConverter)
	{
		this.psBillTypeConverter = psBillTypeConverter;
	}

	protected Converter<CreditCardPaymentInfoModel, PaymentInfoData> getPaymentInfoDataConverter()
	{
		return paymentInfoDataConverter;
	}

	@Required
	public void setPaymentInfoDataConverter(final Converter<CreditCardPaymentInfoModel, PaymentInfoData> paymentInfoDataConverter)
	{
		this.paymentInfoDataConverter = paymentInfoDataConverter;
	}

	protected Converter<CurrencyModel, CurrencyData> getCurrencyConverter()
	{
		return currencyConverter;
	}

	@Required
	public void setCurrencyConverter(final Converter<CurrencyModel, CurrencyData> currencyConverter)
	{
		this.currencyConverter = currencyConverter;
	}

	protected Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> getCreditCardpaymentInfoConverter()
	{
		return creditCardPaymentInfoConverter;
	}

	@Required
	public void setCreditCardPaymentInfoConverter(
			final Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardpaymentInfoDataConverter)
	{
		this.creditCardPaymentInfoConverter = creditCardpaymentInfoDataConverter;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
