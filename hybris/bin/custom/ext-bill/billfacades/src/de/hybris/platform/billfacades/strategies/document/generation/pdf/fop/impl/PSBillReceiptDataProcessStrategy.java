
/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.strategies.document.generation.pdf.fop.impl;

import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.bill.data.PSBillTransactionData;
import de.hybris.platform.billfacades.strategies.document.generation.pdf.fop.PSDocumentDataProcessStrategy;
import de.hybris.platform.billservices.bill.PSBillPaymentService;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 *
 * BillReceiptDataProcessStrategy for processing the bill data
 */
public class PSBillReceiptDataProcessStrategy implements PSDocumentDataProcessStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(PSBillReceiptDataProcessStrategy.class);
	private Converter<PSBillPaymentModel, PSBillReceiptData> psBillReceiptConverter;
	private PSBillPaymentService psBillPaymentService;

	@Override
	public PSBillReceiptData getDocumentData(final String billCode, final String receiptId)
	{
		final PSBillPaymentModel bill = getPsBillPaymentService().getBill(billCode);
		final PSBillReceiptData billReceipt;
		billReceipt = getPsBillReceiptConverter().convert(bill);
		final boolean isTransactionExists = billReceipt != null && billReceipt.getBill() != null
				&& CollectionUtils.isNotEmpty(billReceipt.getBill().getTransactionInfos());
		if (isTransactionExists)
		{
			LOG.debug("transaction info exists for given bill {}", billCode);
			final Optional<PSBillTransactionData> billTransaction = billReceipt.getBill().getTransactionInfos().stream()
					.filter(e -> e.getTransactionId().equals(receiptId)).findFirst();
			billReceipt.setReceiptInfo(billTransaction.orElse(null));
		}
		return billReceipt;
	}

	protected PSBillPaymentService getPsBillPaymentService()
	{
		return psBillPaymentService;
	}

	@Required
	public void setPsBillPaymentService(final PSBillPaymentService psBillPaymentService)
	{
		this.psBillPaymentService = psBillPaymentService;
	}

	protected Converter<PSBillPaymentModel, PSBillReceiptData> getPsBillReceiptConverter()
	{
		return psBillReceiptConverter;
	}

	@Required
	public void setPsBillReceiptConverter(final Converter<PSBillPaymentModel, PSBillReceiptData> psBillReceiptConverter)
	{
		this.psBillReceiptConverter = psBillReceiptConverter;
	}
}
