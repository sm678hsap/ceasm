/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.strategies.document.generation.pdf.fop;

import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;


/**
 * Document Data Process Service
 */
public interface PSDocumentDataProcessStrategy
{

	/**
	 * Get the bill Document Data populated from the given item reference id.
	 *
	 * @param billCode
	 * @param receiptId
	 * @return PSBillReceiptData
	 */
	PSBillReceiptData getDocumentData(String billCode, String receiptId);
}
