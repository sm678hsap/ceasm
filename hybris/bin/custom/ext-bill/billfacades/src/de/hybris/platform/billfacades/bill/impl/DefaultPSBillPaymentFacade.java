/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billfacades.bill.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.billfacades.bill.PSBillPaymentFacade;
import de.hybris.platform.billfacades.bill.data.PSBillMessageData;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.bill.data.PSBillTransactionData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;
import de.hybris.platform.billservices.bill.PSBillPaymentService;
import de.hybris.platform.billservices.bill.PSBillTypeService;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.enums.PSBillMessageCondition;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * Implementation class for {@link PSBillPaymentFacade}
 */
public class DefaultPSBillPaymentFacade implements PSBillPaymentFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSBillPaymentFacade.class);
	private PSBillPaymentService psBillPaymentService;
	private PSBillTypeService psBillTypeService;
	private Converter<PSBillPaymentModel, PSBillPaymentData> psBillConverter;
	private Converter<PSBillMessageModel, PSBillMessageData> psBillMessageConverter;
	private Converter<PSBillPaymentModel, PSBillReceiptData> psBillReceiptConverter;
	private UserService userService;
	private CommerceCheckoutService commerceCheckoutService;
	private CustomerAccountService customerAccountService;
	private I18NService i18nService;
	private ModelService modelService;
	private CommonI18NService commonI18NService;
	private CartService cartService;
	private CustomerFacade customerFacade;
	private Converter<UserModel, CustomerData> customerConverter;

	@Override
	public void createOrUpdateBills(final List<PSBillPaymentData> psBillPayments)
	{
		final List<PSBillPaymentModel> bills = new ArrayList<>();
		psBillPayments.forEach(billData -> {
			final PSBillPaymentModel bill = getPsBillPaymentService().getBill(billData.getCode());
			bills.add(bill);
		});
		getPsBillPaymentService().createOrUpdateBills(bills);
	}

	@Override
	public List<PSBillPaymentData> getBills(final String customerId)
	{
		final CustomerModel customerModel = (CustomerModel) getUserService().getUserForUID(customerId);
		final List<PSBillPaymentModel> bills = getPsBillPaymentService().getBills(customerModel);
		if (CollectionUtils.isNotEmpty(bills))
		{
			return getPsBillConverter().convertAll(bills);
		}
		return Collections.emptyList();
	}

	@Override
	public PSBillPaymentData getBill(final String billId, final String lastName)
	{
		final PSBillPaymentModel bill = getPsBillPaymentService().getBill(billId, lastName);
		if (bill != null)
		{
			return getPsBillConverter().convert(bill);
		}
		LOG.warn("Unable to retrieve the bill for the given bill {} and last name {}", billId, lastName);
		return null;
	}

	@Override
	public PSBillPaymentData getBill(final String code)
	{
		final PSBillPaymentModel bill = getPsBillPaymentService().getBill(code);
		if (bill != null)
		{
			return getPsBillConverter().convert(bill);
		}
		LOG.warn("Unable to retrieve the bill for the given bill {}", code);
		return null;
	}

	@Override
	public SearchPageData<PSBillPaymentData> getBillsByStatusList(final CustomerData customer, final PageableData pageableData,
			final List<BillPaymentStatus> statuses)
	{
		final CustomerModel customerModel = (CustomerModel) getUserService().getUserForUID(customer.getUid());
		final SearchPageData<PSBillPaymentModel> billResults = getPsBillPaymentService().getBillsByStatusList(customerModel,
				pageableData, statuses);
		return convertPageData(billResults, getPsBillConverter());
	}

	protected <S, T> SearchPageData<T> convertPageData(final SearchPageData<S> source, final Converter<S, T> converter)
	{
		final SearchPageData<T> result = new SearchPageData<T>();
		result.setPagination(source.getPagination());
		result.setSorts(source.getSorts());
		result.setResults(Converters.convertAll(source.getResults(), converter));
		return result;
	}

	@Override
	public void setPaymentInfo(final PSBillPaymentData psBillPayment, final PaymentInfoData paymentInfo)
	{
		psBillPayment.setPaymentInfo(paymentInfo);
	}

	@Override
	public List<PSBillMessageData> getMessagesForBill(final PSBillPaymentData psBillPayment)
	{
		final PSBillPaymentModel bill = getPsBillPaymentService().getBill(psBillPayment.getCode());
		final List<PSBillMessageModel> messages = getPsBillPaymentService().getMessagesForBill(bill);
		if (CollectionUtils.isNotEmpty(messages))
		{
			return getPsBillMessageConverter().convertAll(messages);
		}
		return Collections.emptyList();
	}

	@Override
	public PSBillMessageData getMessageForBillAndCondition(final PSBillPaymentData psBillPayment,
			final PSBillMessageCondition condition)
	{
		PSBillMessageData message = null;
		final List<PSBillMessageData> messages = psBillPayment.getBillType().getMessages();
		if (CollectionUtils.isNotEmpty(messages))
		{
			message = messages.stream().filter(messageData -> messageData.getMessageCondition().equals(condition)).findFirst()
					.orElse(null);
		}
		return message;
	}

	@Override
	public void setBillType(final PSBillPaymentData psBillPayment, final PSBillTypeData billType)
	{
		final PSBillPaymentModel bill = getPsBillPaymentService().getBill(psBillPayment.getCode());
		final PSBillTypeModel billData = getPsBillTypeService().getBillType(billType.getCode());
		psBillPayment.setBillType(billType);

		getPsBillPaymentService().setBillType(bill, billData);
	}

	@Override
	public boolean isAmountValidForPayment(final Double amount, final PSBillPaymentData bill)
	{
		BigDecimal amountToBeValidated = BigDecimal.valueOf(amount.doubleValue());
		BigDecimal amountDue = BigDecimal.valueOf(bill.getOutstandingBillAmount().doubleValue());
		amountToBeValidated = amountToBeValidated.setScale(2, RoundingMode.HALF_UP);
		amountDue = amountDue.setScale(2, RoundingMode.HALF_UP);

		final boolean isAmountBelowDueAmount = amountToBeValidated.compareTo(amountDue) <= 0;
		final boolean isOverPaymentAllowed = bill.getBillType().getIsOverPaymentAllowed().booleanValue();
		return isAmountBelowDueAmount || isOverPaymentAllowed;
	}

	@Override
	public Boolean isBillOverDue(final PSBillPaymentData bill)
	{
		return Boolean.valueOf(
				bill.getBillDueDate().before(new Date()) && !Boolean.TRUE.equals(bill.getBillType().getIsOverduePaymentAllowed()));
	}

	@Override
	public PSBillReceiptData getBillReceipt(final String billCode, final String receiptId)
	{
		if (StringUtils.isNotBlank(billCode) && StringUtils.isNotBlank(receiptId))
		{
			final PSBillPaymentModel bill = getPsBillPaymentService().getBill(billCode);
			final PSBillReceiptData billReceipt = getPsBillReceiptConverter().convert(bill);
			final boolean isTransactionExists = billReceipt != null && billReceipt.getBill() != null
					&& CollectionUtils.isNotEmpty(billReceipt.getBill().getTransactionInfos());
			if (isTransactionExists)
			{
				final Optional<PSBillTransactionData> billTransaction = billReceipt.getBill().getTransactionInfos().stream()
						.filter(e -> e.getTransactionId().equals(receiptId)).findFirst();
				billReceipt.setReceiptInfo(billTransaction.orElse(null));
			}
			return billReceipt;
		}
		LOG.debug("bill code {} or/and receipt id {} is null", billCode, receiptId);
		return null;
	}

	@Override
	public String processPayment(final String billCode, final String paymentId, final BigDecimal amount, final String currency,
			final String securityCode)
	{

		final Currency currencyObj = getI18nService().getBestMatchingJavaCurrency(currency);
		final CreditCardPaymentInfoModel ccPaymentInfoModel = getCustomerAccountService()
				.getCreditCardPaymentInfoForCode((CustomerModel) getUserService().getCurrentUser(), paymentId);
		final String merchantCode = getUserService().getCurrentUser().getUid() + "-" + UUID.randomUUID();
		final PaymentTransactionEntryModel pte = getPsBillPaymentService().processPayment(billCode, ccPaymentInfoModel, amount,
				currencyObj, securityCode, getCommerceCheckoutService().getPaymentProvider(), merchantCode);
		if (pte != null && pte.getPaymentTransaction() != null)
		{
			return pte.getPaymentTransaction().getCode();
		}
		LOG.debug("unable to process payment for billCode {}", billCode);
		return null;
	}

	@Override
	public void createGuestUserForBillPayment(final String billId, final String name) throws DuplicateUidException
	{
		final CustomerModel sessionUser = (CustomerModel) getUserService().getCurrentUser();
		if (!CustomerType.REGISTERED.equals(sessionUser.getType()))
		{
			validateParameterNotNullStandardMessage("billId", billId);
			final CustomerModel guestCustomer = getModelService().create(CustomerModel.class);
			final String guid = UUID.randomUUID().toString();

			//takes care of localizing the name based on the site language
			guestCustomer.setUid(guid);
			guestCustomer.setName(name);
			guestCustomer.setType(CustomerType.valueOf(CustomerType.GUEST.getCode()));
			guestCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
			guestCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
			guestCustomer.setOriginalUid(guid);
			LOG.info("creating guest user with name {} for bill {}", billId, name);
			getCustomerAccountService().registerGuestForAnonymousCheckout(guestCustomer, guid);
			final CustomerModel userModel = (CustomerModel) getUserService().getUserForUID(guestCustomer.getUid());
			getCustomerFacade().updateCartWithGuestForAnonymousCheckout(getCustomerConverter().convert(userModel));
			getCartService().changeCurrentCartUser(userModel);
			getUserService().setCurrentUser(userModel);
			LOG.info("updating current user with newly created user");

			final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
			final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
					guestCustomer.getUid(), null, oldAuthentication.getAuthorities());
			newAuthentication.setDetails(oldAuthentication.getDetails());
			SecurityContextHolder.getContext().setAuthentication(newAuthentication);
		}
	}

	@Override
	public void updateGuestUserEmail(final String email)
	{
		final CustomerModel sessionUser = (CustomerModel) getUserService().getCurrentUser();
		if (sessionUser.getType() != null && CustomerType.GUEST.equals(sessionUser.getType()))
		{
			sessionUser.setOriginalUid(sessionUser.getOriginalUid().split("\\|")[0] + "|" + email);
			sessionUser.setUid(sessionUser.getUid().split("\\|")[0] + "|" + email);
			getModelService().save(sessionUser);
			LOG.info("updating the session guest user's guid by appending email id");

			// Replace the spring security authentication with the new UID
			final String newUid = customerFacade.getCurrentCustomer().getUid().toLowerCase();
			final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
			final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(newUid, null,
					oldAuthentication.getAuthorities());
			newAuthentication.setDetails(oldAuthentication.getDetails());
			SecurityContextHolder.getContext().setAuthentication(newAuthentication);
		}
	}

	@Override
	public String guestUserEmailForBillPayment(final String guestUserEmail)
	{
		return StringUtils.substringAfter(guestUserEmail, "|");
	}

	@Override
	public boolean isCurrentUserRegistered()
	{
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		return CustomerType.REGISTERED.equals(currentCustomer.getType());
	}

	@Override
	public boolean isCurrentUserGuest()
	{
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		return CustomerType.GUEST.equals(currentCustomer.getType());
	}

	@Override
	public CustomerData getCustomerDataForUid(final String uid)
	{
		final UserModel userModel = getUserService().getUserForUID(uid);
		return getCustomerConverter().convert(userModel);
	}

	protected PSBillPaymentService getPsBillPaymentService()
	{
		return psBillPaymentService;
	}

	@Required
	public void setPsBillPaymentService(final PSBillPaymentService psBillPaymentService)
	{
		this.psBillPaymentService = psBillPaymentService;
	}

	protected Converter<PSBillPaymentModel, PSBillPaymentData> getPsBillConverter()
	{
		return psBillConverter;
	}

	@Required
	public void setPsBillConverter(final Converter<PSBillPaymentModel, PSBillPaymentData> psBillConverter)
	{
		this.psBillConverter = psBillConverter;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected Converter<PSBillMessageModel, PSBillMessageData> getPsBillMessageConverter()
	{
		return psBillMessageConverter;
	}

	@Required
	public void setPsBillMessageConverter(final Converter<PSBillMessageModel, PSBillMessageData> psBillMessageConverter)
	{
		this.psBillMessageConverter = psBillMessageConverter;
	}

	protected PSBillTypeService getPsBillTypeService()
	{
		return psBillTypeService;
	}

	@Required
	public void setPsBillTypeService(final PSBillTypeService psBillTypeService)
	{
		this.psBillTypeService = psBillTypeService;
	}

	protected CommerceCheckoutService getCommerceCheckoutService()
	{
		return commerceCheckoutService;
	}

	@Required
	public void setCommerceCheckoutService(final CommerceCheckoutService commerceCheckoutService)
	{
		this.commerceCheckoutService = commerceCheckoutService;
	}

	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected I18NService getI18nService()
	{
		return i18nService;
	}

	@Required
	public void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	@Required
	public void setCustomerFacade(final CustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	protected Converter<PSBillPaymentModel, PSBillReceiptData> getPsBillReceiptConverter()
	{
		return psBillReceiptConverter;
	}

	@Required
	public void setPsBillReceiptConverter(final Converter<PSBillPaymentModel, PSBillReceiptData> psBillReceiptConverter)
	{
		this.psBillReceiptConverter = psBillReceiptConverter;
	}
}
