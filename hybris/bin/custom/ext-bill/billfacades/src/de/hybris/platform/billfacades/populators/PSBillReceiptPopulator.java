/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billfacades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;


/**
 * Populates {@link BillReceiptData}
 */
public class PSBillReceiptPopulator implements Populator<PSBillPaymentModel, PSBillReceiptData>
{
	private Converter<PSBillPaymentModel, PSBillPaymentData> psBillConverter;

	@Override
	public void populate(final PSBillPaymentModel source, final PSBillReceiptData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		final PSBillPaymentData billpaymentData = getPsBillConverter().convert(source);
		target.setBill(billpaymentData);
	}

	protected Converter<PSBillPaymentModel, PSBillPaymentData> getPsBillConverter()
	{
		return psBillConverter;
	}

	@Required
	public void setPsBillConverter(final Converter<PSBillPaymentModel, PSBillPaymentData> psBillConverter)
	{
		this.psBillConverter = psBillConverter;
	}
}
