/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.process.xslfo.context;

import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.tools.generic.EscapeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Abstract Bill Receipt Context
 */
public abstract class AbstractPSBillReceiptContext extends VelocityContext
{
	public static final Logger LOG = LoggerFactory.getLogger(AbstractPSBillReceiptContext.class);

	public static final String DATE_FORMATTER = "dateFormatter";
	public static final String DATE_FORMAT_PROP = "billfacades.bill.receipt.date.format";
	public static final String BASE_URL = "baseUrl";
	public static final String SECURE_BASE_URL = "secureBaseUrl";
	public static final String MEDIA_BASE_URL = "mediaBaseUrl";
	public static final String MEDIA_SECURE_BASE_URL = "mediaSecureBaseUrl";
	public static final String THEME = "theme";
	public static final String BASE_SITE = "baseSite";
	public static final String BASE_THEME_URL = "baseThemeUrl";
	public static final String URL_RESOLUTION_SERVICE = "urlResolutionService";
	public static final String BILL_RECEIPT_DATA = "billReceiptData";
	public static final String RECEIPT_LOGO_MEDIA = "receiptLogoMedia";
	public static final String RECEIPT_LOGO_SUFFIX = "_bill_receipt_logo";
	public static final String ESCAPE_TOOL = "esc";

	private String urlEncodingAttributes;
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
	private UrlEncoderService urlEncoderService;
	private MediaService mediaService;
	private CatalogVersionService catalogVersionService;
	private UserService userService;
	private ConfigurationService configurationService;

	/**
	 * Initialize context data.
	 *
	 * @param baseSite
	 * @param billReceiptData
	 */
	public void init(final BaseSiteModel baseSite, final PSBillReceiptData billReceiptData)
	{
		put(BASE_SITE, baseSite);
		setUrlEncodingAttributes(getUrlEncoderService().getUrlEncodingPattern());
		put(BASE_URL, getSiteBaseUrlResolutionService().getWebsiteUrlForSite(baseSite, getUrlEncodingAttributes(), false, ""));
		put(BASE_THEME_URL, getSiteBaseUrlResolutionService().getWebsiteUrlForSite(baseSite, "", false, ""));
		put(SECURE_BASE_URL,
				getSiteBaseUrlResolutionService().getWebsiteUrlForSite(baseSite, getUrlEncodingAttributes(), true, ""));
		put(MEDIA_BASE_URL, getSiteBaseUrlResolutionService().getMediaUrlForSite(baseSite, false));
		put(MEDIA_SECURE_BASE_URL, getSiteBaseUrlResolutionService().getMediaUrlForSite(baseSite, true));
		put(THEME, baseSite.getTheme() != null ? baseSite.getTheme().getCode() : null);
		put(URL_RESOLUTION_SERVICE, getSiteBaseUrlResolutionService());
		put(DATE_FORMATTER, new SimpleDateFormat(getConfigurationService().getConfiguration().getString(DATE_FORMAT_PROP)));
		put(ESCAPE_TOOL, new EscapeTool());

		final String receiptLogoCode = baseSite.getUid() + RECEIPT_LOGO_SUFFIX;
		put(RECEIPT_LOGO_MEDIA, getMediaByCode(receiptLogoCode));
		put(BILL_RECEIPT_DATA, billReceiptData);
		final PSBillPaymentData bill = billReceiptData.getBill();
		final String paidByUid = billReceiptData.getReceiptInfo().getPaidBy().getUid();
		if (bill.getCustomer() == null || !(paidByUid).equalsIgnoreCase(bill.getCustomer().getUid()))
		{
			put("isPaidByVisible", Boolean.TRUE);
			if (CustomerType.GUEST.equals(((CustomerModel) getUserService().getUserForUID(paidByUid)).getType()))
			{
				put("isPaidByGuestUser", Boolean.TRUE);
				put("guestUserEmail", StringUtils.substringAfter(paidByUid, "|"));
			}
		}
	}

	protected MediaModel getMediaByCode(final String mediaCode)
	{
		if (StringUtils.isNotEmpty(mediaCode))
		{
			for (final CatalogVersionModel catalogVersionModel : getCatalogVersionService().getSessionCatalogVersions())
			{
				final MediaModel media = getMediaByCodeAndCatalogVersion(mediaCode, catalogVersionModel);
				if (media != null)
				{
					return media;
				}
			}
		}
		return null;
	}

	protected MediaModel getMediaByCodeAndCatalogVersion(final String mediaCode, final CatalogVersionModel catalogVersionModel)
	{
		try
		{
			return getMediaService().getMedia(catalogVersionModel, mediaCode);
		}
		catch (final UnknownIdentifierException ignore)
		{
			// Ignore this exception
			LOG.error("Unknownidentifier exception for media code {}", mediaCode, ignore);
		}
		return null;
	}

	public BaseSiteModel getBaseSite()
	{
		return (BaseSiteModel) get(BASE_SITE);
	}

	protected SiteBaseUrlResolutionService getSiteBaseUrlResolutionService()
	{
		return siteBaseUrlResolutionService;
	}

	@Required
	public void setSiteBaseUrlResolutionService(final SiteBaseUrlResolutionService siteBaseUrlResolutionService)
	{
		this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
	}

	protected String getUrlEncodingAttributes()
	{
		return urlEncodingAttributes;
	}

	public void setUrlEncodingAttributes(final String urlEncodingAttributes)
	{
		this.urlEncodingAttributes = urlEncodingAttributes;
	}

	protected UrlEncoderService getUrlEncoderService()
	{
		return urlEncoderService;
	}

	@Required
	public void setUrlEncoderService(final UrlEncoderService urlEncoderService)
	{
		this.urlEncoderService = urlEncoderService;
	}

	protected MediaService getMediaService()
	{
		return mediaService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
