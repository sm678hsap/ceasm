/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billfacades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.billfacades.bill.data.PSBillMessageData;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Populates {@link PSBillMessageData}
 */
public class PSBillMessagePopulator implements Populator<PSBillMessageModel, PSBillMessageData>
{

	@Override
	public void populate(final PSBillMessageModel source, final PSBillMessageData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setMessageCondition(source.getMessageCondition());
		target.setBillType(target.getBillType());
		target.setShortMessage(source.getShortMessage());
		target.setLongMessage(source.getLongMessage());
	}

}
