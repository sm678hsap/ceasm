/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billfacades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.billfacades.bill.data.PSBillMessageData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;
import de.hybris.platform.billservices.model.PSBillMessageModel;
import de.hybris.platform.billservices.model.PSBillTypeModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * Populates {@link PSBillTypeData}
 */
public class PSBillTypePopulator implements Populator<PSBillTypeModel, PSBillTypeData>
{
	private Converter<PSBillMessageModel, PSBillMessageData> psBillMessageConverter;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final PSBillTypeModel source, final PSBillTypeData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		target.setExternalCode(source.getExternalCode());
		target.setReminderDaysLag(source.getReminderDaysLag());

		populatePaymentConditions(source, target);
		populateEmailCondition(source, target);
		addMessages(source, target);
	}

	/**
	 * Populates bill payment conditions to bill type data.
	 *
	 * @param source
	 * @param target
	 */
	protected void populatePaymentConditions(final PSBillTypeModel source, final PSBillTypeData target)
	{
		target.setIsOverduePaymentAllowed(source.getIsOverduePaymentAllowed());
		target.setIsOverPaymentAllowed(source.getIsOverPaymentAllowed());
		target.setIsPartPaymentAllowed(source.getIsPartPaymentAllowed());
		target.setIsPaymentAllowed(source.getIsPaymentAllowed());
	}

	/**
	 * Populates bill email conditions to bill type data.
	 *
	 * @param source
	 * @param target
	 */
	protected void populateEmailCondition(final PSBillTypeModel source, final PSBillTypeData target)
	{
		target.setSendConfirmationEmail(source.getSendConfirmationEmail());
		target.setSendReminderEmail(source.getSendReminderEmail());
	}

	/**
	 * Adds bill messages.
	 *
	 * @param source
	 * @param target
	 */
	protected void addMessages(final PSBillTypeModel source, final PSBillTypeData target)
	{
		final List<PSBillMessageData> messageDataList = new ArrayList<>();
		for (final PSBillMessageModel message : source.getPsBillMessages())
		{
			final PSBillMessageData messageData = getPsBillMessageConverter().convert(message);
			messageDataList.add(messageData);
		}
		target.setMessages(messageDataList);
	}

	protected Converter<PSBillMessageModel, PSBillMessageData> getPsBillMessageConverter()
	{
		return psBillMessageConverter;
	}

	@Required
	public void setPsBillMessageConverter(final Converter<PSBillMessageModel, PSBillMessageData> psBillMessageConverter)
	{
		this.psBillMessageConverter = psBillMessageConverter;
	}

}
