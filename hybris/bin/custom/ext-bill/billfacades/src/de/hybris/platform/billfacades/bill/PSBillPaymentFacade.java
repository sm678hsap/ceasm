/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.billfacades.bill;

import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.billfacades.bill.data.PSBillMessageData;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.bill.data.PSBillTypeData;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.enums.PSBillMessageCondition;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import java.math.BigDecimal;
import java.util.List;


/**
 * Interface for PSBillPayment facade implementation.
 */
public interface PSBillPaymentFacade
{
	/**
	 * Create & Update bills
	 *
	 * @param psBillPayments
	 *           Bill Payment
	 */
	void createOrUpdateBills(List<PSBillPaymentData> psBillPayments);

	/**
	 * Get all customer's bills
	 *
	 * @param customerId
	 *           Customer Id
	 * @return List<PSBillPaymentData> Returns list of type PSBillPaymentData
	 */
	List<PSBillPaymentData> getBills(String customerId);

	/**
	 * Get a bill from bill Id and customer last name
	 *
	 * @param billId
	 *           Biller Id
	 * @param lastName
	 *           Customer Last Name
	 * @return Bill Payment
	 */
	PSBillPaymentData getBill(String billId, String lastName);

	/**
	 * Get a bill from bill code
	 *
	 * @param code
	 *           Biller code
	 * @return Bill Payment
	 */
	PSBillPaymentData getBill(String code);

	/**
	 * Sets payment information to the Public Sector Bill.
	 *
	 * @param psBillPayment
	 *           PSBillPaymentData
	 * @param paymentInfo
	 *           PaymentInfoData
	 */
	void setPaymentInfo(final PSBillPaymentData psBillPayment, final PaymentInfoData paymentInfo);

	/**
	 * Gets the messages if there are any from Bill's type.
	 *
	 * @param psBillPayment
	 *           PSBillPaymentData the bill
	 * @return List<PSBillMessageData>
	 */
	List<PSBillMessageData> getMessagesForBill(final PSBillPaymentData psBillPayment);

	/**
	 * Gets the messages if there are any from Bill's type for given condition.
	 *
	 * @param psBillPayment
	 *           psBillPayment represents the data object for bill details
	 * @param condition
	 *           condition represents enum which has different types of message conditions applicable to a bill
	 * @return PSBillMessageData data object for bill message details
	 */
	PSBillMessageData getMessageForBillAndCondition(final PSBillPaymentData psBillPayment, PSBillMessageCondition condition);

	/**
	 * Sets the bill type of the Bill.
	 *
	 * @param billType
	 *           PSBillTypeData the bill type
	 * @param psBillPayment
	 *           PSBillPaymentData the psBillPayment
	 */
	void setBillType(final PSBillPaymentData psBillPayment, final PSBillTypeData billType);

	/**
	 * Returns whether the amount is valid for given bill
	 *
	 * @param amount
	 *           amount represents double value of bill amount
	 * @param bill
	 *           bill represents PSBillPaymentData data object for bill details
	 * @return Boolean
	 */
	boolean isAmountValidForPayment(final Double amount, final PSBillPaymentData bill);

	/**
	 * Returns whether the bill is overdue
	 *
	 * @param bill
	 *           PSBillPaymentData the bill
	 * @return Boolean
	 */
	Boolean isBillOverDue(final PSBillPaymentData bill);


	/**
	 * Creates guest user for bill payment
	 *
	 * @param billId
	 *           the bill id
	 * @param name
	 *           the bill id once the bill is retrieved and holds the user's name once the payment is done
	 * @throws DuplicateUidException
	 *            if uid used to pay the bill is same as registered user's uid
	 */
	void createGuestUserForBillPayment(final String billId, final String name) throws DuplicateUidException;

	/**
	 * Updates guest user email
	 *
	 * @param email
	 *           the email id used for guest user payment
	 */
	void updateGuestUserEmail(final String email);


	/**
	 * returns billReceipt Data for given billId and receipt id
	 *
	 * @param billId
	 *           bill id
	 * @param receiptCode
	 *           receipt code
	 * @return PSBillReceiptData data object for bill & receipt details
	 */
	PSBillReceiptData getBillReceipt(String billId, String receiptCode);

	/**
	 * Returns true if session user is registered
	 *
	 * @return boolean
	 */
	boolean isCurrentUserRegistered();

	/**
	 * Returns true if session user is guest
	 *
	 * @return boolean
	 */
	boolean isCurrentUserGuest();

	/**
	 * Method to process bill payment including Authorization and Capture
	 *
	 * @param billCode
	 *           bill code
	 * @param paymentId
	 *           payment id
	 * @param amount
	 *           BigDecimal value of bill amount
	 * @param currency
	 *           currency
	 * @param securityCode
	 *           security code
	 * @return String - returns PaymentTransactionEntryModel code if payment process is successful or else it returns
	 *         null
	 */
	String processPayment(String billCode, String paymentId, BigDecimal amount, String currency, String securityCode);

	/**
	 * Method to process the email id for the Guest User
	 *
	 * @param guestUserEmail
	 *           the guest user email id
	 * @return String
	 */
	String guestUserEmailForBillPayment(String guestUserEmail);

	/**
	 * Method to fetch Customer data object for given customer uid
	 *
	 * @param uid
	 *           customer uid
	 * @return Customer data object for customer details
	 */
	CustomerData getCustomerDataForUid(String uid);

	/**
	 * Filters bills based on billPaymentStatus
	 *
	 * @param customer
	 *           CustomerData data object
	 * @param pageableData
	 *           PageableData data object
	 * @param statuses
	 *           enum for bill statues applicable to bills
	 * @return List of bills based on bill payment status
	 */
	SearchPageData<PSBillPaymentData> getBillsByStatusList(CustomerData customer, PageableData pageableData,
			List<BillPaymentStatus> statuses);

}
