/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.factory.document.generation.pdf.fop.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.factory.document.generation.pdf.fop.PSBillReceiptDocumentContextFactory;
import de.hybris.platform.billfacades.process.xslfo.context.AbstractPSBillReceiptContext;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.site.BaseSiteService;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;


/**
 * Default Bill Receipt Document Context Factory used to create velocity context for rendering receipt documents
 */
public class DefaultPSBillReceiptDocumentContextFactory implements PSBillReceiptDocumentContextFactory
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSBillReceiptDocumentContextFactory.class);
	private BaseSiteService baseSiteService;

	@Override
	public VelocityContext create(final PSBillReceiptData billReceiptData, final RendererTemplateModel renderTemplate)
	{
		final BaseSiteModel baseSite = baseSiteService.getCurrentBaseSite();
		final AbstractPSBillReceiptContext context = resolveContext(renderTemplate);
		context.init(baseSite, billReceiptData);
		return context;
	}

	protected <T extends VelocityContext> T resolveContext(final RendererTemplateModel renderTemplate)
	{
		try
		{
			final Class<T> contextClass = (Class<T>) Class.forName(renderTemplate.getContextClass());
			final Map<String, T> context = getApplicationContext().getBeansOfType(contextClass);
			if (MapUtils.isNotEmpty(context))
			{
				LOG.debug("found context for given class", renderTemplate.getContextClass());
				return context.entrySet().iterator().next().getValue();
			}
			else
			{
				throw new IllegalStateException("Cannot find bean in application context for context class [" + contextClass + "]");
			}
		}
		catch (final ClassNotFoundException e)
		{
			LOG.error("failed to create bill receipt context", e);
			throw new IllegalStateException("Cannot find bill receipt context class", e);
		}
	}

	protected ApplicationContext getApplicationContext()
	{
		return Registry.getApplicationContext();
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}
}
