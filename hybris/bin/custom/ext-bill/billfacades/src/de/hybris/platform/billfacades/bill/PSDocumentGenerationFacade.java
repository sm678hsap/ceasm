/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.bill;


import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;


/**
 * Document Generation Facade
 */
public interface PSDocumentGenerationFacade
{

	/**
	 * Retrieve the byte[] with desired bill receipt data and document type and write to the response if not null.
	 *
	 * @param documentType
	 *           the document type
	 * @param billReceiptData
	 *           PSBillReceiptData data object for bill & receiptInfo details
	 * @param response
	 *           HttpServletResponse response object
	 * @return byte[]
	 * @throws IOException
	 *            if errors occurs while writing a pdf
	 */
	byte[] generate(final String documentType, final PSBillReceiptData billReceiptData, final HttpServletResponse response)
			throws IOException;

	/**
	 * Retrieve the byte[] with desired billCode,receipt ID and document type and write to the response if not null.
	 *
	 * @param documentType
	 *           the document type
	 * @param billCode
	 *           the bill code
	 * @param receiptCode
	 *           the receipt code
	 * @param response
	 *           HttpServletResponse response object
	 * @return byte[]
	 * @throws IOException
	 *            if errors occurs while writing a pdf
	 */
	byte[] generate(final String documentType, final String billCode, final String receiptCode, final HttpServletResponse response)
			throws IOException;

	/**
	 * Retrieve the PSBillReceiptData with desired billCode,receipt ID and return output
	 *
	 * @param billCode
	 *           the bill code
	 * @param receiptId
	 *           the receipt id
	 * @return PSBillReceiptData data object for for bill & receiptInfo details
	 */
	PSBillReceiptData generateBillReceipt(final String billCode, final String receiptId);

	/**
	 * Creates the PDF receipt body from the bill's receipt data
	 *
	 * @param billReceiptData
	 *           PSBillReceiptData data object for bill & receiptInfo details
	 * @return String
	 */
	String getBillPaymentReceiptBody(final PSBillReceiptData billReceiptData);


}

