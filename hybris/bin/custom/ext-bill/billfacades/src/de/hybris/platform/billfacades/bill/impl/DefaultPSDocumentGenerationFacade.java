/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.bill.impl;


import de.hybris.platform.billfacades.bill.PSDocumentGenerationFacade;
import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.billfacades.factory.document.generation.pdf.fop.PSBillReceiptDocumentContextFactory;
import de.hybris.platform.billfacades.strategies.document.generation.pdf.fop.PSDocumentDataProcessStrategy;
import de.hybris.platform.billfacades.utils.PSFopDocumentGenerationUtils;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.site.BaseSiteService;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalTime;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Document Generation Facade implementation of {@link PSDocumentGenerationFacade}
 */
public class DefaultPSDocumentGenerationFacade implements PSDocumentGenerationFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSDocumentGenerationFacade.class);

	private PSDocumentDataProcessStrategy psDocumentDataProcessStrategy;
	private BaseSiteService baseSiteService;
	private RendererService rendererService;
	private PSBillReceiptDocumentContextFactory psBillReceiptDocumentContextFactory;
	public static final String PDF_DOCUMENT = "PDF";
	public static final String CONTENT_DISPOSITION = "Content-Disposition";
	public static final String INLINE_PDF = "inline; filename=";

	@Override
	public byte[] generate(final String documentType, final PSBillReceiptData billReceiptData, final HttpServletResponse response)
			throws IOException
	{
		if (documentType.equals(PDF_DOCUMENT))
		{
			final String body = getBillPaymentReceiptBody(billReceiptData);
			final byte[] pdf = PSFopDocumentGenerationUtils.generatePdf(body);
			if (response != null)
			{
				LOG.info("writing pdf to response object");
				writePdfToResponse(pdf, response);
			}
			return pdf;
		}
		else
		{
			LOG.error("Document type must be PDF.");
			throw new NotImplementedException();
		}
	}

	@Override
	public byte[] generate(final String documentType, final String billCode, final String receiptId,
			final HttpServletResponse response) throws IOException
	{
		final PSBillReceiptData policyData = generateBillReceipt(billCode, receiptId);
		return generate(documentType, policyData, response);
	}

	@Override
	public PSBillReceiptData generateBillReceipt(final String billCode, final String receiptId)
	{
		return getPsDocumentDataProcessStrategy().getDocumentData(billCode, receiptId);
	}

	protected void writePdfToResponse(final byte[] pdf, final HttpServletResponse response) throws IOException
	{
		if (pdf != null)
		{
			final LocalTime time = LocalTime.now();
			final String filename = time + ".pdf";
			response.setContentType("application/pdf");
			response.setHeader(CONTENT_DISPOSITION, INLINE_PDF + filename);
			response.setContentLength(pdf.length);
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			return;
		}
	}

	@Override
	public String getBillPaymentReceiptBody(final PSBillReceiptData billReceiptData)
	{
		final String templateId = getTemplateId();
		if (StringUtils.isNotEmpty(templateId))
		{
			final RendererTemplateModel bodyTemplate = getRendererService().getRendererTemplateForCode(templateId);
			final StringWriter renderedBody = new StringWriter();
			final VelocityContext context = getPsBillReceiptDocumentContextFactory().create(billReceiptData, bodyTemplate);
			getRendererService().render(bodyTemplate, context, renderedBody);
			return renderedBody.getBuffer().toString();
		}
		return null;
	}

	private String getTemplateId()
	{
		return getBaseSiteService().getCurrentBaseSite().getUid() + "_billpayment_receipt";
	}

	protected PSDocumentDataProcessStrategy getPsDocumentDataProcessStrategy()
	{
		return psDocumentDataProcessStrategy;
	}

	@Required
	public void setPsDocumentDataProcessStrategy(final PSDocumentDataProcessStrategy psDocumentDataProcessStrategy)
	{
		this.psDocumentDataProcessStrategy = psDocumentDataProcessStrategy;
	}

	protected RendererService getRendererService()
	{
		return rendererService;
	}

	@Required
	public void setRendererService(final RendererService rendererService)
	{
		this.rendererService = rendererService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected PSBillReceiptDocumentContextFactory getPsBillReceiptDocumentContextFactory()
	{
		return psBillReceiptDocumentContextFactory;
	}

	@Required
	public void setPsBillReceiptDocumentContextFactory(
			final PSBillReceiptDocumentContextFactory psBillReceiptDocumentContextFactory)
	{
		this.psBillReceiptDocumentContextFactory = psBillReceiptDocumentContextFactory;
	}
}
