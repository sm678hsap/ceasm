/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.billfacades.factory.document.generation.pdf.fop;

import de.hybris.platform.billfacades.bill.data.PSBillReceiptData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;

import org.apache.velocity.VelocityContext;


/**
 * BillReceiptDocumentContextFactory
 */
public interface PSBillReceiptDocumentContextFactory
{
	/**
	 * Create the velocity context for rendering receipt.
	 *
	 * @param billReceiptData
	 *           the receipt data
	 * @param renderTemplate
	 *           the renderer template
	 * @return the velocity context
	 */
	VelocityContext create(PSBillReceiptData billReceiptData, RendererTemplateModel renderTemplate);
}
