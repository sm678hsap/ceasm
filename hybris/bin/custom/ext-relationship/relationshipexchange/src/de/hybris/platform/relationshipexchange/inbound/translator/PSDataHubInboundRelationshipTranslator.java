/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.relationshipexchange.inbound.translator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.relationshipexchange.constants.RelationshipexchangeConstants;
import de.hybris.platform.relationshipexchange.inbound.PSDataHubInboundRelationshipHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Translator for relationship inbound. It triggers the event whenever a relationship is added/updated
 */
public class PSDataHubInboundRelationshipTranslator extends PSDataHubTranslator<PSDataHubInboundRelationshipHelper>
{
	public static final Logger LOG = LoggerFactory.getLogger(PSDataHubInboundRelationshipTranslator.class);
	public static final String HELPER_BEAN = "psDataHubRelationshipInboundHelper";
	private static final String MINOR_PREFIX = "minor-";

	/***
	 * Constructor on which invokes parent class constructor with helper bean , which in turns gets bean from application
	 * context.
	 *
	 */
	public PSDataHubInboundRelationshipTranslator()
	{
		super(HELPER_BEAN);
	}

	@Override
	public void performImport(final String sourceUser, final Item processedItem) throws ImpExException
	{
		validateParameterNotNull(sourceUser, "Source uid cannot be null!");

		if (!getInboundHelper().isUserExisting(sourceUser) || getInboundHelper().isUserAMinor(sourceUser))
		{
			throw new ImpExException("Invalid source user!");
		}

		if (processedItem != null)
		{
			final String targetUserUid = getTargetUser(processedItem);
			final String targetEmail = getTargetEmail(processedItem);

			if (targetUserUid != null && !getInboundHelper().isUserExisting(targetUserUid))
			{
				throw new ImpExException("Target user does not exist!");
			}

			if (targetUserUid == null && targetEmail != null && !targetEmail.contains(MINOR_PREFIX))
			{

				getInboundHelper().sendRelationshipRequestEmail(sourceUser, targetUserUid, targetEmail, getFirstName(processedItem),
						getLastName(processedItem), getTitle(processedItem));
			}
		}
	}

	/**
	 * Retrieves target user value from the impex items.
	 *
	 * @param processedItem
	 * @return
	 * @throws ImpExException
	 */
	private String getTargetUser(final Item processedItem) throws ImpExException
	{
		String targetUser = null;
		try
		{
			final Object obj = processedItem.getAttribute(RelationshipexchangeConstants.TARGET_USER_UID);
			if (obj instanceof Customer)
			{
				targetUser = ((Customer) obj).getUid();
			}
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("error in retrieving target user ", e);
			throw new ImpExException(e);
		}
		return targetUser;
	}

	private String getTargetEmail(final Item processedItem) throws ImpExException
	{
		String targetEmail = null;
		try
		{
			final Object obj = processedItem.getAttribute(RelationshipexchangeConstants.TARGET_EMAIL);
			if (obj != null)
			{
				targetEmail = obj.toString();
			}
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("error in getting target email", e);
			throw new ImpExException(e);
		}
		return targetEmail;

	}

	private String getFirstName(final Item processedItem) throws ImpExException
	{
		String firstName = null;
		try
		{
			final Object obj = processedItem.getAttribute(RelationshipexchangeConstants.FIRSTNAME);
			if (obj != null)
			{
				firstName = obj.toString();
			}
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("error in getting first name", e);
			throw new ImpExException(e);
		}
		return firstName;

	}

	private String getLastName(final Item processedItem) throws ImpExException
	{
		String lastName = null;
		try
		{
			final Object obj = processedItem.getAttribute(RelationshipexchangeConstants.LASTNAME);
			if (obj != null)
			{
				lastName = obj.toString();
			}
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("error in getting last name", e);
			throw new ImpExException(e);
		}
		return lastName;

	}

	private String getTitle(final Item processedItem) throws ImpExException
	{
		String title = null;
		try
		{
			final Object obj = processedItem.getAttribute(RelationshipexchangeConstants.TITLE);
			if (obj instanceof TitleModel)
			{
				title = ((TitleModel) obj).getCode();
			}
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("error in getting title", e);
			throw new ImpExException(e);
		}
		return title;
	}
}
