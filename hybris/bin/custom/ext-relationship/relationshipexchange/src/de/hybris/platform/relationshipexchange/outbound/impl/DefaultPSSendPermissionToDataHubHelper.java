/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipexchange.outbound.impl;


import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.util.Config;


/**
 * Class definition as a workaround for Spring since it cannot handle parameterized types
 */
public class DefaultPSSendPermissionToDataHubHelper extends AbstractPSSendToDataHubHelper<PSPermissionModel>
{
	private static final String PERMISSION_FEED_NAME = "permission.feed.name";

	@Override
	public String getFeed()
	{
		return Config.getParameter(PERMISSION_FEED_NAME) != null ? Config.getParameter(PERMISSION_FEED_NAME) : super.getFeed();
	}
}
