/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipexchange.outbound.impl;

import de.hybris.platform.relationshipexchange.constants.PSPermissionCsvColumnConstants;
import de.hybris.platform.relationshipexchange.outbound.PSRawItemContributor;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;


/**
 * This a Permission Contributor class which creates Permission rows and implements {@link PSRawItemContributor}
 */
public class DefaultPSPermissionContributor implements PSRawItemContributor<PSPermissionModel>
{
	private static final String DATE_FORMAT_FOR_PERMISSION = "dd/MM/yyyy HH:mm";

	private final Set<String> columns = new HashSet<>(Arrays.asList(PSPermissionCsvColumnConstants.DISAPPROVAL_DATE_TIME,
			PSPermissionCsvColumnConstants.APPROVAL_DATE_TIME, PSPermissionCsvColumnConstants.SOURCE_USER,
			PSPermissionCsvColumnConstants.TARGET_USER, PSPermissionCsvColumnConstants.PERMISSION_STATUS,
			PSPermissionCsvColumnConstants.PERMISSIBLE_AREA_ITEM_TYPE, PSPermissionCsvColumnConstants.IS_REQUESTED));

	@Override
	public Set<String> getColumns()
	{
		return columns;
	}

	@Override
	public List<Map<String, Object>> createRows(final PSPermissionModel permission)
	{
		final List<Map<String, Object>> result = new ArrayList<>();
		result.add(setPermissionInfoContributor(permission));
		return result;
	}

	@Override
	public List<Map<String, Object>> createRows(final List<PSPermissionModel> permissions)
	{
		final List<Map<String, Object>> result = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(permissions))
		{
			permissions.forEach(item -> result.add(setPermissionInfoContributor(item)));
		}
		return result;
	}

	/**
	 * Sets permission info and returns the map
	 *
	 * @param row
	 * @param permission
	 * @return Map
	 */
	private Map<String, Object> setPermissionInfoContributor(final PSPermissionModel permission)
	{
		final Map<String, Object> row = new HashMap<>();
		if (permission != null)
		{
			row.put(PSPermissionCsvColumnConstants.DISAPPROVAL_DATE_TIME, getFormattedDate(permission.getDisApprovalDateTime()));
			row.put(PSPermissionCsvColumnConstants.APPROVAL_DATE_TIME, getFormattedDate(permission.getApprovalDateTime()));
			row.put(PSPermissionCsvColumnConstants.SOURCE_USER, permission.getSourceUser().getUid());
			row.put(PSPermissionCsvColumnConstants.TARGET_USER, permission.getTargetUser().getUid());
			row.put(PSPermissionCsvColumnConstants.PERMISSION_STATUS, permission.getPermissionStatus().getCode());
			row.put(PSPermissionCsvColumnConstants.PERMISSIBLE_AREA_ITEM_TYPE, permission.getPermissibleAreaItemType().getCode());
			row.put(PSPermissionCsvColumnConstants.IS_REQUESTED, permission.getIsRequested());
			row.put(PSPermissionCsvColumnConstants.CREATIONTIME, getFormattedDate(permission.getCreationtime()));
		}
		return row;
	}

	private String getFormattedDate(final Date date)
	{
		if (date != null)
		{
			return new SimpleDateFormat(DATE_FORMAT_FOR_PERMISSION).format(date);
		}
		return StringUtils.EMPTY;
	}
}
