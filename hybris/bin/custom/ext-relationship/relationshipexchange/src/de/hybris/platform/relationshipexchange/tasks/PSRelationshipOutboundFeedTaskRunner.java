/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipexchange.tasks;

import de.hybris.platform.relationshipexchange.outbound.PSSendToDataHubHelper;
import de.hybris.platform.relationshipexchange.outbound.PSSendToDataHubResult;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * TaskRunner to send relationship to datahub. Invoked when PSRelationshipEvent is published.
 */
public class PSRelationshipOutboundFeedTaskRunner implements TaskRunner<TaskModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSRelationshipOutboundFeedTaskRunner.class);
	private PSSendToDataHubHelper<PSRelationshipModel> relationshipToDatahubHelper;

	@Override
	public void run(final TaskService taskService, final TaskModel taskModel) throws RetryLaterException
	{
		final PSRelationshipModel relationshipModel = (PSRelationshipModel) taskModel.getContext();

		final PSSendToDataHubResult result = getRelationshipToDatahubHelper().createAndSendRawItem(relationshipModel);
		if (result.isSuccess())
		{
			LOG.info("Outbound feed for relationship is SUCCESS");
		}
		else
		{
			LOG.info("Outbound feed for relationship FAILED");
		}
	}

	@Override
	public void handleError(final TaskService taskService, final TaskModel taskModel, final Throwable error)
	{
		LOG.error("Outbound feed for relationship failed", error);
	}

	public PSSendToDataHubHelper<PSRelationshipModel> getRelationshipToDatahubHelper()
	{
		return relationshipToDatahubHelper;
	}

	@Required
	public void setRelationshipToDatahubHelper(final PSSendToDataHubHelper<PSRelationshipModel> relationshipToDatahubHelper)
	{
		this.relationshipToDatahubHelper = relationshipToDatahubHelper;
	}
}
