/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipexchange.inbound.impl;

import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipexchange.inbound.PSDataHubInboundRelationshipHelper;
import de.hybris.platform.relationshipservices.event.PSRelationshipRequestEvent;
import de.hybris.platform.relationshipservices.relationship.data.PSRelationshipParameter;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * DefaultPSDataHubInboundRelationshipHelper implementation of {@link PSDataHubInboundRelationshipHelper}
 */
public class DefaultPSDataHubInboundRelationshipHelper implements PSDataHubInboundRelationshipHelper
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSDataHubInboundRelationshipHelper.class);

	private static final String BASESTORE_UID = "basestore.uid";
	private static final String BASESITE_UID = "basesite.uid";

	private EventService eventService;
	private BaseSiteService baseSiteService;
	private CommonI18NService commonI18NService;
	private BaseStoreService baseStoreService;
	private UserService userService;
	private PSRelationshipService psRelationshipService;
	private ModelService modelService;

	@Override
	public void sendRelationshipRequestEmail(final String sourceUser, final String targetUser, final String targetEmail,
			final String firstName, final String lastName, final String title)
	{
		LOG.info("triggering the relationship inbound event");

		final UserModel sUser = getUserService().getUserForUID(sourceUser);
		final PSRelationshipParameter relationshipParameters = new PSRelationshipParameter();
		relationshipParameters.setRelationshipStatus(PSRelationshipStatus.PENDING);
		relationshipParameters.setSourceUser(sUser);
		if (targetUser != null && getUserService().isUserExisting(targetUser))
		{
			final UserModel tUser = getUserService().getUserForUID(targetUser);
			relationshipParameters.setTargetUser(tUser);
			relationshipParameters.setFirstName(firstName);
			relationshipParameters.setLastName(lastName);
			relationshipParameters.setTitle(getTitle(title));

		}
		else
		{
			relationshipParameters.setTargetEmail(targetEmail);
			relationshipParameters.setTitle(getTitle(title));
			relationshipParameters.setFirstName(firstName);
			relationshipParameters.setLastName(lastName);

		}
		getEventService().publishEvent(initializeEvent(relationshipParameters));
	}

	private PSRelationshipRequestEvent initializeEvent(final PSRelationshipParameter relationshipParameter)
	{
		final PSRelationshipRequestEvent event = new PSRelationshipRequestEvent();
		event.setRelationshipParameter(relationshipParameter);
		event.setSite(getBaseSiteService().getBaseSiteForUID(getBaseSiteUid()));
		event.setBaseStore(getBaseStoreService().getBaseStoreForUid(getBaseStoreUid()));
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		event.setLanguage(getCommonI18NService().getCurrentLanguage());

		return event;
	}

	@Override
	public Boolean isUserAMinor(final String userId)
	{
		final UserModel targetUser = getUserService().getUserForUID(userId);
		return targetUser == null ? false : targetUser.getIsMinor();
	}

	@Override
	public Boolean isUserExisting(final String userId)
	{
		return getUserService().isUserExisting(userId);
	}

	private TitleModel getTitle(final String titleCode)
	{
		return StringUtils.isEmpty(titleCode) ? null : getUserService().getTitleForCode(titleCode);
	}

	protected String getBaseStoreUid()
	{
		return Config.getParameter(BASESTORE_UID);
	}

	protected String getBaseSiteUid()
	{
		return Config.getParameter(BASESITE_UID);
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected PSRelationshipService getPsRelationshipService()
	{
		return psRelationshipService;
	}

	@Required
	public void setPsRelationshipService(final PSRelationshipService psRelationshipService)
	{
		this.psRelationshipService = psRelationshipService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
