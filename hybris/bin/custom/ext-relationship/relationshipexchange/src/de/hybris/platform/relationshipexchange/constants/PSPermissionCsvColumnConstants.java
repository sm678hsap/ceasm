/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipexchange.constants;



/**
 * Global class for all Permission csv constants.
 */
public final class PSPermissionCsvColumnConstants
{
	private PSPermissionCsvColumnConstants()
	{
		//empty to avoid instantiating this constant class
	}

	public static final String APPROVAL_DATE_TIME = "approvalDateTime";
	public static final String DISAPPROVAL_DATE_TIME = "disApprovalDateTime";
	public static final String SOURCE_USER = "sourceUser";
	public static final String TARGET_USER = "targetUser";
	public static final String PERMISSION_STATUS = "permissionStatus";
	public static final String IS_REQUESTED = "isRequested";
	public static final String PERMISSIBLE_AREA_ITEM_TYPE = "permissibleAreaItemType";
	public static final String CREATIONTIME = "creationtime";

}
