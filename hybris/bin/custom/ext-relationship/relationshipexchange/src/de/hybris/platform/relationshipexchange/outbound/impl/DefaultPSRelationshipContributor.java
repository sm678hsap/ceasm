/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipexchange.outbound.impl;

import de.hybris.platform.relationshipexchange.constants.PSRelationshipCsvColumnConstants;
import de.hybris.platform.relationshipexchange.outbound.PSRawItemContributor;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;


/**
 * This a Public Sector Relationship Contributor class which creates Relationship rows and implements
 * {@link PSRawItemContributor}
 */
public class DefaultPSRelationshipContributor implements PSRawItemContributor<PSRelationshipModel>
{

	private static final String DATE_FORMAT_FOR_RELATIONSHIP = "dd/MM/yyyy HH:mm";
	private final Set<String> columns = new HashSet<>(Arrays.asList(PSRelationshipCsvColumnConstants.RELATIONSHIP_ID,
			PSRelationshipCsvColumnConstants.REQUEST_DATE_TIME, PSRelationshipCsvColumnConstants.SOURCE_USER,
			PSRelationshipCsvColumnConstants.TARGET_USER, PSRelationshipCsvColumnConstants.TARGET_EMAIL,
			PSRelationshipCsvColumnConstants.IS_SOURCE_POA_HOLDER, PSRelationshipCsvColumnConstants.IS_TARGET_POA_HOLDER,
			PSRelationshipCsvColumnConstants.APPROVAL_DATE_TIME, PSRelationshipCsvColumnConstants.DISAPPROVAL_DATE_TIME,
			PSRelationshipCsvColumnConstants.RELATIONSHIP_STATUS, PSRelationshipCsvColumnConstants.TITLE,
			PSRelationshipCsvColumnConstants.FIRST_NAME, PSRelationshipCsvColumnConstants.LAST_NAME));

	@Override
	public Set<String> getColumns()
	{
		return columns;
	}

	@Override
	public List<Map<String, Object>> createRows(final PSRelationshipModel relationship)
	{
		final List<Map<String, Object>> result = new ArrayList<>();
		result.add(setRelationshipInfoContributor(relationship));

		return result;
	}

	@Override
	public List<Map<String, Object>> createRows(final List<PSRelationshipModel> model)
	{
		final List<Map<String, Object>> result = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(model))
		{
			model.forEach(item -> result.add(setRelationshipInfoContributor(item)));
		}
		return result;
	}

	/**
	 * Sets Relationship info in contributor
	 *
	 * @param row
	 * @param relationship
	 * @return Map
	 */
	private Map<String, Object> setRelationshipInfoContributor(final PSRelationshipModel relationship)
	{
		final Map<String, Object> row = new HashMap<>();
		if (relationship != null)
		{
			row.put(PSRelationshipCsvColumnConstants.REQUEST_DATE_TIME, getFormattedDate(relationship.getRequestDateTime()));
			row.put(PSRelationshipCsvColumnConstants.SOURCE_USER, relationship.getSourceUser().getUid());
			row.put(PSRelationshipCsvColumnConstants.TARGET_USER,
					relationship.getTargetUser() != null ? relationship.getTargetUser().getUid() : StringUtils.EMPTY);
			row.put(PSRelationshipCsvColumnConstants.TARGET_EMAIL,
					relationship.getTargetEmail() != null ? relationship.getTargetEmail() : StringUtils.EMPTY);
			row.put(PSRelationshipCsvColumnConstants.IS_SOURCE_POA_HOLDER, relationship.getIsSourcePoaHolder());
			row.put(PSRelationshipCsvColumnConstants.IS_TARGET_POA_HOLDER, relationship.getIsTargetPoaHolder());
			row.put(PSRelationshipCsvColumnConstants.APPROVAL_DATE_TIME, getFormattedDate(relationship.getApprovalDateTime()));
			row.put(PSRelationshipCsvColumnConstants.DISAPPROVAL_DATE_TIME, getFormattedDate(relationship.getDisApprovalDateTime()));
			row.put(PSRelationshipCsvColumnConstants.RELATIONSHIP_STATUS, relationship.getRelationshipStatus().getCode());
			row.put(PSRelationshipCsvColumnConstants.TITLE,
					relationship.getTitle() == null ? StringUtils.EMPTY : relationship.getTitle().getCode());
			row.put(PSRelationshipCsvColumnConstants.FIRST_NAME,
					relationship.getFirstName() == null ? StringUtils.EMPTY : relationship.getFirstName());
			row.put(PSRelationshipCsvColumnConstants.LAST_NAME,
					relationship.getLastName() == null ? StringUtils.EMPTY : relationship.getLastName());
			row.put(PSRelationshipCsvColumnConstants.RELATIONSHIP_ID, relationship.getPk().getLongValueAsString());
		}
		return row;
	}

	private String getFormattedDate(final Date date)
	{
		if (date != null)
		{
			return new SimpleDateFormat(DATE_FORMAT_FOR_RELATIONSHIP).format(date);
		}
		return StringUtils.EMPTY;
	}

}

