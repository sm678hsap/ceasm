/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.relationshipexchange.inbound.events;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Listener for Inbound relationship event
 */
public class PSInboundRelationshipEventListener extends AbstractEventListener<PSInboundRelationshipEvent>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSInboundRelationshipEventListener.class);

	/**
	 * event listener for in bound relationship event
	 *
	 * @param relationshipInboundEvent
	 *
	 */
	@Override
	protected void onEvent(final PSInboundRelationshipEvent relationshipInboundEvent)
	{
		LOG.info("on event of relationship inbound event");
	}

}
