/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipexchange.event.listener;

import de.hybris.platform.relationshipservices.permission.event.PSPermissionEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskService;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Event listener for PSPermissionEvent.
 */
public class PSPermissionEventListener extends AbstractEventListener<PSPermissionEvent>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSPermissionEventListener.class);
	private ModelService modelService;
	private TaskService taskService;
	private String taskRunnerBean;

	@Override
	protected void onEvent(final PSPermissionEvent permissionEvent)
	{
		final TaskModel taskModel = getModelService().create(TaskModel.class);
		taskModel.setContext(permissionEvent.getPermissions());
		taskModel.setRunnerBean(getTaskRunnerBean());
		taskModel.setExecutionDate(new Date());

		LOG.info("Task scheduled for permission outbound feed");
		getTaskService().scheduleTask(taskModel);
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public TaskService getTaskService()
	{
		return taskService;
	}

	@Required
	public void setTaskService(final TaskService taskService)
	{
		this.taskService = taskService;
	}

	public String getTaskRunnerBean()
	{
		return taskRunnerBean;
	}

	@Required
	public void setTaskRunnerBean(final String taskRunnerBean)
	{
		this.taskRunnerBean = taskRunnerBean;
	}
}
