/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipexchange.tasks;

import de.hybris.platform.relationshipexchange.outbound.PSSendToDataHubHelper;
import de.hybris.platform.relationshipexchange.outbound.PSSendToDataHubResult;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * TaskRunner to send permissions to datahub. Invoked when PSPermissionEvent is published.
 */
public class PSPermissionOutboundFeedTaskRunner implements TaskRunner<TaskModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSPermissionOutboundFeedTaskRunner.class);
	private PSSendToDataHubHelper<PSPermissionModel> permissionToDataHubHelper;

	@Override
	public void run(final TaskService taskService, final TaskModel taskModel) throws RetryLaterException
	{
		final List<PSPermissionModel> permissions = (List<PSPermissionModel>) taskModel.getContext();
		final PSSendToDataHubResult result = getPermissionToDataHubHelper().createAndSendRawItem(permissions);
		if (result.isSuccess())
		{
			LOG.info("Outbound feed for permission is SUCCESS");
		}
		else
		{
			LOG.info("Outbound feed for permission FAILED");
		}
	}

	@Override
	public void handleError(final TaskService taskService, final TaskModel taskModel, final Throwable error)
	{
		LOG.error("Outbound feed for permission failed", error);
	}

	public PSSendToDataHubHelper<PSPermissionModel> getPermissionToDataHubHelper()
	{
		return permissionToDataHubHelper;
	}

	@Required
	public void setPermissionToDataHubHelper(final PSSendToDataHubHelper<PSPermissionModel> permissionToDataHubHelper)
	{
		this.permissionToDataHubHelper = permissionToDataHubHelper;
	}
}
