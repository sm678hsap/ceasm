/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipexchange.outbound.impl;

import de.hybris.platform.relationshipexchange.outbound.PSRawItemBuilder;
import de.hybris.platform.relationshipexchange.outbound.PSSendToDataHubHelper;
import de.hybris.platform.relationshipexchange.outbound.PSSendToDataHubResult;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.hybris.datahub.core.rest.DataHubCommunicationException;
import com.hybris.datahub.core.rest.DataHubOutboundException;
import com.hybris.datahub.core.services.DataHubOutboundService;


/**
 * Helper for creating a raw items and sending it to the Data Hub
 *
 * @param <T>
 */
public class AbstractPSSendToDataHubHelper<T extends AbstractItemModel> implements PSSendToDataHubHelper<T>
{

	private static final Logger LOG = LoggerFactory.getLogger(AbstractPSSendToDataHubHelper.class);

	private static final String DEFAULT_FEED = "DEFAULT_FEED";
	private static final String DATAHUB_ERROR = "Error while sending to datahub";

	private DataHubOutboundService dataHubOutboundService;
	private String rawItemType;
	private PSRawItemBuilder<T> rawItemBuilder;

	@Override
	public PSSendToDataHubResult createAndSendRawItem(final List<T> models)
	{
		try
		{
			getDataHubOutboundService().sendToDataHub(getFeed(), getRawItemType(), getRawItemBuilder().rowsAsNameValuePairs(models));
		}
		//sendToDataHub throws DataHubOutboundException which is parent class to DataHubCommunicationException
		catch (final DataHubOutboundException exception)
		{
			LOG.error(DATAHUB_ERROR, exception);
			return new DefaultPSSendToDataHubResult(PSSendToDataHubResult.SENDING_FAILED_CODE, exception.getMessage());
		}
		catch (final Exception exception)
		{
			LOG.error(DATAHUB_ERROR, exception);
			return new DefaultPSSendToDataHubResult(PSSendToDataHubResult.SENDING_FAILED_CODE, exception.getMessage());
		}
		return DefaultPSSendToDataHubResult.OKAY;
	}

	@Override
	public PSSendToDataHubResult createAndSendRawItem(final T model)
	{
		try
		{
			getDataHubOutboundService().sendToDataHub(getFeed(), getRawItemType(), getRawItemBuilder().rowsAsNameValuePairs(model));
		}
		catch (final DataHubOutboundException exception)
		{
			LOG.error(DATAHUB_ERROR, exception);
			return new DefaultPSSendToDataHubResult(PSSendToDataHubResult.SENDING_FAILED_CODE, exception.getMessage());
		}
		catch (final Exception exception)
		{
			LOG.error(DATAHUB_ERROR, exception);
			return new DefaultPSSendToDataHubResult(PSSendToDataHubResult.SENDING_FAILED_CODE, exception.getMessage());
		}
		return DefaultPSSendToDataHubResult.OKAY;
	}

	/**
	 * Get datahub feed based on service request
	 *
	 * @return
	 */
	public String getFeed()
	{
		return DEFAULT_FEED;
	}

	protected PSRawItemBuilder<T> getRawItemBuilder()
	{
		return rawItemBuilder;
	}

	@Required
	public void setRawItemBuilder(final PSRawItemBuilder<T> csvBuilder)
	{
		this.rawItemBuilder = csvBuilder;
	}

	protected DataHubOutboundService getDataHubOutboundService()
	{
		return dataHubOutboundService;
	}

	@Required
	public void setDataHubOutboundService(final DataHubOutboundService dataHubOutboundService)
	{
		this.dataHubOutboundService = dataHubOutboundService;
	}

	public String getRawItemType()
	{
		return rawItemType;
	}

	@Required
	public void setRawItemType(final String rawItem)
	{
		this.rawItemType = rawItem;
	}
}
