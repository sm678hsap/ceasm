/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipexchange.outbound;

/**
 * Result container for sending raw items to DataHub
 */
public interface PSSendToDataHubResult
{

	int SENDING_FAILED_CODE = -1;

	int MESSAGE_HANDLING_ERROR = -2;

	/**
	 *
	 * @return true if sending was successful
	 */
	boolean isSuccess();

	/**
	 *
	 * @return errorCode
	 */
	int getErrorCode();

	/**
	 *
	 * @return error text
	 */
	String getErrorText();
}
