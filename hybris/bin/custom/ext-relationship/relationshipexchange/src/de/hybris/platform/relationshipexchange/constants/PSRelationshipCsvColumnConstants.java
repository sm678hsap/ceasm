/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipexchange.constants;



/**
 * class for all relationship csv column constants.
 */
public final class PSRelationshipCsvColumnConstants
{
	private PSRelationshipCsvColumnConstants()
	{
		//empty to avoid instantiating this constant class
	}

	public static final String REQUEST_DATE_TIME = "requestDateTime";
	public static final String SOURCE_USER = "sourceUser";
	public static final String TARGET_EMAIL = "targetEmail";
	public static final String TARGET_USER = "targetUser";
	public static final String IS_SOURCE_POA_HOLDER = "isSourcePoaHolder";
	public static final String IS_TARGET_POA_HOLDER = "isTargetPoaHolder";
	public static final String DISAPPROVAL_DATE_TIME = "disApprovalDateTime";
	public static final String TITLE = "title";
	public static final String LAST_NAME = "lastName";
	public static final String FIRST_NAME = "firstName";
	public static final String RELATIONSHIP_STATUS = "relationshipStatus";
	public static final String APPROVAL_DATE_TIME = "approvalDateTime";
	public static final String RELATIONSHIP_ID = "relationshipId";

}
