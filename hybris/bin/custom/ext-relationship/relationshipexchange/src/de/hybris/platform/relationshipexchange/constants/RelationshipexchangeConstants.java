/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipexchange.constants;



/**
 * Global class for all Relationshipexchange constants. You can add global constants for your extension into this class.
 */
public final class RelationshipexchangeConstants extends GeneratedRelationshipexchangeConstants
{
	public static final String EXTENSIONNAME = "relationshipexchange";
	public static final String TARGET_USER_UID = "targetUser";
	public static final String SOURCE_USER_UID = "sourceUser";
	public static final String TARGET_EMAIL = "targetEmail";
	public static final String TITLE = "title";
	public static final String FIRSTNAME = "firstName";
	public static final String LASTNAME = "lastName";
	public static final String USERID = "uid";
	public static final String MINORFLAG = "isMinor";
	public static final String CITIZENID = "citizenId";
	public static final String STATUS = "relationshipStatus";

	private RelationshipexchangeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "relationshipexchangePlatformLogo";
}
