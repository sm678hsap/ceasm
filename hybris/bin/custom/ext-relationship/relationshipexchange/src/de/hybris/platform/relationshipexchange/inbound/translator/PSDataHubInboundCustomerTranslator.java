/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.relationshipexchange.inbound.translator;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.relationshipexchange.constants.RelationshipexchangeConstants;
import de.hybris.platform.relationshipexchange.inbound.PSDataHubInboundRelationshipHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Translator for create inbound customer's validation.
 */
public class PSDataHubInboundCustomerTranslator extends PSDataHubTranslator<PSDataHubInboundRelationshipHelper>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSDataHubInboundCustomerTranslator.class);

	public static final String MINOR_PREFIX = "minor-";
	public static final String HELPER_BEAN = "psDataHubRelationshipInboundHelper";


	/***
	 * Constructor on which invokes parent class constructor with helper bean , which in turns gets bean from application
	 * context.
	 *
	 */
	public PSDataHubInboundCustomerTranslator()
	{
		super(HELPER_BEAN);
	}

	@Override
	public void performImport(final String isImported, final Item processedItem) throws ImpExException
	{
		final String userId = getUserId(processedItem);
		if (userId != null && userId.startsWith(MINOR_PREFIX) && !getMinorFlag(processedItem))
		{
				throw new ImpExException("isMinor flag should be set true for minor records.");
		}

		if (getMinorFlag(processedItem) && getCitizenshipId(processedItem) == null)
		{
			throw new ImpExException("Citizen Id can not be blank for minor users");
		}
	}

	private String getUserId(final Item processedItem) throws ImpExException
	{
		String userId = null;
		try
		{
			userId = processedItem.getAttribute(RelationshipexchangeConstants.USERID).toString();
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("Error getting user id", e);
		}
		return userId;
	}

	private boolean getMinorFlag(final Item processedItem) throws ImpExException
	{
		boolean isMinor = false;
		try
		{
			isMinor = "true".equalsIgnoreCase(processedItem.getAttribute(RelationshipexchangeConstants.MINORFLAG).toString());
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("Error getting minor flag", e);
		}
		return isMinor;
	}

	private String getCitizenshipId(final Item processedItem) throws ImpExException
	{
		String citizenshipId = null;
		try
		{
			citizenshipId = processedItem.getAttribute(RelationshipexchangeConstants.CITIZENID).toString();
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("Error getting citizen id", e);

		}
		return citizenshipId;
	}
}
