/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.relationshipexchange.inbound.events;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * This class is for relationship in bound events
 */
public class PSInboundRelationshipEvent extends AbstractEvent implements ClusterAwareEvent
{
	private static final long serialVersionUID = -3839203917371395960L;
	private final String sourceUser;
	private final String targetUser;

	public PSInboundRelationshipEvent(final String sourceUser, final String targetUser)
	{
		super();
		this.sourceUser = sourceUser;
		this.targetUser = targetUser;
	}

	protected String getSourceUser()
	{
		return sourceUser;
	}

	protected String getTargetUser()
	{
		return targetUser;
	}

	@Override
	public boolean publish(final int sourceNodeId, final int targetNodeId)
	{
		return sourceNodeId == targetNodeId;
	}
}
