/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.relationshipexchange.inbound;


import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;


/**
 * Data Hub Inbound Helper for relationship exchange
 */
public interface PSDataHubInboundRelationshipHelper
{
	/**
	 * Send relationship request email
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param targetEmail
	 * @param firstName
	 * @param lastName
	 * @param title
	 */
	void sendRelationshipRequestEmail(final String sourceUser, final String targetUser, final String targetEmail,
			final String firstName,
			final String lastName, final String title);

	/**
	 * Checks if user is a minor.
	 *
	 * @param userId
	 * @return
	 */
	Boolean isUserAMinor(final String userId);

	/**
	 * Checks if user exists.
	 *
	 * @param userId
	 * @return
	 */
	Boolean isUserExisting(final String userId);
}
