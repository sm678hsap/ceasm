package de.hybris.platform.relationshipexchange.inbound.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.relationshipexchange.inbound.events.PSInboundRelationshipEvent;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSDataHubInboundRelationshipHelperTest
{
   private static final String SOURCE_USER_ID = "source@email.com";
   private static final String TARGET_USER_ID = "target@email.com";
   private static final String FIRSTNAME = "John";
   private static final String LASTNAME = "Citizen";
   private static final String TARGET_EMAIL = "target@email.com";
   private static final String TITLE = "mr";

   @InjectMocks
   private DefaultPSDataHubInboundRelationshipHelper classUnderTest;
   @Mock
   private EventService eventService;
   @Mock
   private BaseSiteService baseSiteService;
   @Mock
   private CommonI18NService commonI18NService;
   @Mock
   private BaseStoreService baseStoreService;
   @Mock
   private UserService userService;
   @Mock
   private PSRelationshipService psRelationshipService;
   @Mock
   private ModelService modelService;

   @Before
   public void setUp()
   {
      classUnderTest = new DefaultPSDataHubInboundRelationshipHelper();
      classUnderTest.setEventService(eventService);
      classUnderTest.setBaseSiteService(baseSiteService);
      classUnderTest.setCommonI18NService(commonI18NService);
      classUnderTest.setBaseStoreService(baseStoreService);
      classUnderTest.setUserService(userService);
      classUnderTest.setPsRelationshipService(psRelationshipService);
      classUnderTest.setModelService(modelService);
   }

   @Test
   public void testRelationshipEmail()
   {
      Mockito.doNothing().when(eventService).publishEvent(Mockito.any(PSInboundRelationshipEvent.class));
      classUnderTest.sendRelationshipRequestEmail(SOURCE_USER_ID, TARGET_USER_ID, TARGET_EMAIL, FIRSTNAME, LASTNAME, TITLE);
      Mockito.verify(eventService).publishEvent(Mockito.any(PSInboundRelationshipEvent.class));
   }

}
