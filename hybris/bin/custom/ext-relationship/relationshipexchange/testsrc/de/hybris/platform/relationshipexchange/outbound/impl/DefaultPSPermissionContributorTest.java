/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipexchange.outbound.impl;

import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipexchange.constants.PSPermissionCsvColumnConstants;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;


@SuppressWarnings("javadoc")
@UnitTest
public class DefaultPSPermissionContributorTest
{

	private DefaultPSPermissionContributor permissionContributor;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		permissionContributor = new DefaultPSPermissionContributor();
	}

	@Test
	public void testGetColumns()
	{
		final Set<String> columns = permissionContributor.getColumns();

		assertTrue(columns.contains(PSPermissionCsvColumnConstants.DISAPPROVAL_DATE_TIME));
		assertTrue(columns.contains(PSPermissionCsvColumnConstants.SOURCE_USER));
		assertTrue(columns.contains(PSPermissionCsvColumnConstants.TARGET_USER));
		assertTrue(columns.contains(PSPermissionCsvColumnConstants.APPROVAL_DATE_TIME));
		assertTrue(columns.contains(PSPermissionCsvColumnConstants.PERMISSION_STATUS));
		assertTrue(columns.contains(PSPermissionCsvColumnConstants.PERMISSIBLE_AREA_ITEM_TYPE));
		assertTrue(columns.contains(PSPermissionCsvColumnConstants.IS_REQUESTED));

	}


	@Test
	public void testCreateRowsForSinglePermission()
	{

		final PSPermissionModel permission = new PSPermissionModel();
		permission.setApprovalDateTime(new Date());
		permission.setTargetUser(new UserModel());
		permission.setSourceUser(new UserModel());
		permission.setPermissibleAreaItemType(new PSPermissibleAreaItemTypeModel());
		permission.setIsRequested(false);
		permission.setPermissionStatus(PSPermissionStatus.ACTIVE);

		final List<Map<String, Object>> rowsList = permissionContributor.createRows(permission);

		Assert.assertTrue(
				rowsList.get(0).get(PSPermissionCsvColumnConstants.PERMISSION_STATUS).equals(PSPermissionStatus.ACTIVE.name()));

	}

	@Test
	public void testCreateRowsForPermissionList()
	{

		final PSPermissionModel permission = new PSPermissionModel();
		permission.setApprovalDateTime(new Date());
		permission.setTargetUser(new UserModel());
		permission.setSourceUser(new UserModel());
		permission.setPermissibleAreaItemType(new PSPermissibleAreaItemTypeModel());
		permission.setIsRequested(false);
		permission.setPermissionStatus(PSPermissionStatus.INACTIVE);

		final List<PSPermissionModel> permissions = Collections.singletonList(permission);
		final List<Map<String, Object>> rowsList = permissionContributor.createRows(permissions);

		Assert.assertTrue(
				rowsList.get(0).get(PSPermissionCsvColumnConstants.PERMISSION_STATUS).equals(PSPermissionStatus.INACTIVE.name()));

	}
}
