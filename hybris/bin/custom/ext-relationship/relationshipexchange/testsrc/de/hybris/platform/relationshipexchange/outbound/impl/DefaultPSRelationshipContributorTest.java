/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipexchange.outbound.impl;

import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipexchange.constants.PSRelationshipCsvColumnConstants;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@SuppressWarnings("javadoc")
@UnitTest
public class DefaultPSRelationshipContributorTest
{

	private DefaultPSRelationshipContributor relationshipContributor;

	@Mock
	private PSRelationshipModel relationship;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		relationshipContributor = new DefaultPSRelationshipContributor();
		Mockito.when(relationship.getPk()).thenReturn(PK.parse("0123456789"));
		Mockito.when(relationship.getRequestDateTime()).thenReturn(new Date());
		Mockito.when(relationship.getApprovalDateTime()).thenReturn(new Date());
		Mockito.when(relationship.getSourceUser()).thenReturn(new UserModel());
		Mockito.when(relationship.getTargetEmail()).thenReturn("target@stateofrosebud.com");
		Mockito.when(relationship.getFirstName()).thenReturn("First name");
		Mockito.when(relationship.getLastName()).thenReturn("last name");
		Mockito.when(relationship.getRelationshipStatus()).thenReturn(PSRelationshipStatus.PENDING);
		final TitleModel title = Mockito.mock(TitleModel.class);
		Mockito.when(title.getName()).thenReturn("Mr");
		Mockito.when(relationship.getTitle()).thenReturn(title);
	}

	@Test
	public void testGetColumns()
	{
		final Set<String> columns = relationshipContributor.getColumns();
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.REQUEST_DATE_TIME));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.SOURCE_USER));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.TARGET_USER));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.TARGET_EMAIL));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.IS_SOURCE_POA_HOLDER));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.IS_TARGET_POA_HOLDER));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.APPROVAL_DATE_TIME));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.DISAPPROVAL_DATE_TIME));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.RELATIONSHIP_STATUS));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.TITLE));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.FIRST_NAME));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.LAST_NAME));
		assertTrue(columns.contains(PSRelationshipCsvColumnConstants.RELATIONSHIP_ID));
	}

	@Test
	public void testCreateRowsForSinglePermission()
	{
		final List<Map<String, Object>> rowsList = relationshipContributor.createRows(relationship);

		Assert.assertTrue(rowsList.get(0).get(PSRelationshipCsvColumnConstants.FIRST_NAME).equals("First name"));
		Assert.assertTrue(rowsList.get(0).get(PSRelationshipCsvColumnConstants.LAST_NAME).equals("last name"));
	}

	@Test
	public void testCreateRowsForPermissionList()
	{
		final List<PSRelationshipModel> relationships = Collections.singletonList(relationship);
		final List<Map<String, Object>> rowsList = relationshipContributor.createRows(relationships);

		Assert.assertTrue(rowsList.get(0).get(PSRelationshipCsvColumnConstants.FIRST_NAME).equals("First name"));
		Assert.assertTrue(rowsList.get(0).get(PSRelationshipCsvColumnConstants.LAST_NAME).equals("last name"));
	}
}
