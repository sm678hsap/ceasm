/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipexchange.outbound.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;


@SuppressWarnings("javadoc")
@UnitTest
public class DefaultPSSendRelationshipToDataHubHelperTest
{

	private DefaultPSSendRelationshipToDataHubHelper abstractPSSendToDataHubHelper;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		abstractPSSendToDataHubHelper = new DefaultPSSendRelationshipToDataHubHelper();
	}

	@Test
	public void testGetFeedValid()
	{
		final String feedName = abstractPSSendToDataHubHelper.getFeed();

		Assert.assertEquals("RelationshipFeed", feedName);

	}
}
