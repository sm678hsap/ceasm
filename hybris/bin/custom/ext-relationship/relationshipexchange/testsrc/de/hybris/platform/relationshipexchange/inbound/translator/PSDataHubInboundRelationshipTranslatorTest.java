/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.relationshipexchange.inbound.translator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.relationshipexchange.inbound.PSDataHubInboundRelationshipHelper;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSDataHubInboundRelationshipTranslatorTest {

    private static final String SOURCE_USER_ID = "source@email.com";
    private static final String TARGET_USER_ID = "target@email.com";
    private static final String FIRSTNAME = "John";
    private static final String LASTNAME = "Citizen";
    private static final String TARGET_EMAIL = "target@email.com";
    private static final String TITLE = "mr";

    @InjectMocks
    private PSDataHubInboundRelationshipTranslator classUnderTest;

    @Mock
    private PSDataHubInboundRelationshipHelper inboundHelper;

    @Mock
    private Item item;

    @Mock
    private ModelService modelService;

    @Mock
    private UserService userService;

    @Before
    public void setUp() {
        classUnderTest.setInboundHelper(inboundHelper);
    }

    @Test(expected = ImpExException.class)
    public void testRelationshipRequestEmail() throws ImpExException {
        Mockito.doNothing().when(inboundHelper).sendRelationshipRequestEmail(SOURCE_USER_ID, TARGET_USER_ID, TARGET_EMAIL, FIRSTNAME,
                LASTNAME, TITLE);
        classUnderTest.performImport(SOURCE_USER_ID, item);
        Mockito.verify(inboundHelper).sendRelationshipRequestEmail(SOURCE_USER_ID, TARGET_USER_ID, TARGET_EMAIL, FIRSTNAME, LASTNAME,
                TITLE);
    }

}
