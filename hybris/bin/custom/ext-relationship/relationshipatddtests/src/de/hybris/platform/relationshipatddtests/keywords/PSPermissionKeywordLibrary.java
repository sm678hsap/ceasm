package de.hybris.platform.relationshipatddtests.keywords;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationshipfacades.permission.PSPermissionFacade;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Created by ailencollado on 8/03/2017.
 */
public class PSPermissionKeywordLibrary extends AbstractKeywordLibrary
{

	@Autowired
	private PSPermissionService psPermissionService;

	@Autowired
	private UserService userService;

	@Autowired
	private PSPermissionFacade psPermissionfacade;

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify permissible areas for permissions</i>
	 * <p>
	 *
	 * @param source
	 * @param target
	 * @throws de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException
	 */
	public void verifyGetPermissibleAreasForPermissions(final String source, final String target)
	{
		final UserModel sourceUser = userService.getUserForUID(source);
		final UserModel targetUser = userService.getUserForUID(target);

		final List<PSPermissionModel> permissions = psPermissionService.getPermissionsForStatus(sourceUser, targetUser,
				Arrays.asList(PSPermissionStatus.ACTIVE));
		final List<PSPermissibleAreaItemTypeModel> permissibleAreas = psPermissionService
				.getPermissibleAreasForPermissions(permissions);
		Assert.assertTrue("permissibleAreas should not be empty", CollectionUtils.isNotEmpty(permissibleAreas));

	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>request more permissions for source and target users</i>
	 * <p>
	 *
	 * @param source
	 * @param target
	 * @param typeCode
	 * @throws de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException
	 */
	public void requestMorePermissions(final String source, final String target, final String typeCode)
	{
		psPermissionfacade.requestMorePermission(source, target, Arrays.asList(typeCode), PSPermissionStatus.PENDING);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify more permissions for source and target users</i>
	 * <p>
	 *
	 * @param source
	 * @param target
	 * @param permissibleAreaCode
	 * @throws de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException
	 */
	public void verifyMorePermissionsWereAdded(final String source, final String target, final String permissibleAreaCode)
	{

		final UserModel sourceUser = userService.getUserForUID(source);
		final UserModel targetUser = userService.getUserForUID(target);

		final List<PSPermissionModel> permissions = psPermissionService.getPermissionsForStatus(sourceUser, targetUser,
				Arrays.asList(PSPermissionStatus.PENDING));
		Assert.assertNotNull(permissions);
		Assert.assertTrue(permissions.get(0).getIsRequested());
		Assert.assertEquals(permissibleAreaCode, permissions.get(0).getPermissibleAreaItemType().getCode());
	}

	/**
	 * Change permissions.
	 *
	 * @param source
	 * @param target
	 * @param permission
	 */
	public void changePermissions(final String source, final String target, final String permission)
	{
		psPermissionfacade.changePermissions(source, target, Arrays.asList(permission), false);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify more permissions for source and target users</i>
	 * <p>
	 *
	 * @param source
	 * @param target
	 * @throws de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException
	 */
	public void verifyInactivePermission(final String source, final String target)
	{

		final List<PSPermissionData> inactivePermissions = psPermissionfacade.getPermissionsForStatus(source, target,
				Arrays.asList(PSPermissionStatus.PENDING));

		Assert.assertNotNull(inactivePermissions);
	}
}
