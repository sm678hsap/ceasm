/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipatddtests.keywords;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.relationship.data.PSPermissibleAreaData;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationship.data.PSRelationshipData;
import de.hybris.platform.relationshipfacades.permission.PSPermissionFacade;
import de.hybris.platform.relationshipfacades.relationship.PSRelationshipFacade;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


public class PSRelationshipKeywordLibrary extends AbstractKeywordLibrary
{
	@Autowired
	private PSRelationshipFacade psRelationshipfacade;

	@Autowired
	private PSPermissionFacade psPermissionfacade;

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>create relation between users with permissions</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param grantedPermissions
	 * @param requestedPermissions
	 * @throws RelationshipAlreadyExistException
	 *
	 */
	public void createRelationBetweenUsersWithPermissions(final String sourceUser, final String targetUser,
			final String grantedPermissions, final String requestedPermissions) throws RelationshipAlreadyExistException
	{
		final List<String> grantedPermissionsAsList = convertDelimitedStringToList(grantedPermissions, ",");
		final List<String> requestedPermissionsAsList = convertDelimitedStringToList(requestedPermissions, ",");
		psRelationshipfacade.addRelationship(sourceUser, targetUser, grantedPermissionsAsList, requestedPermissionsAsList,
				StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify relation is created between users ${sourceUser} ${targetUser}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 */
	public void verifyRelationIsCreatedBetweenUsers(final String sourceUser, final String targetUser)
	{
		final PSRelationshipData relationshipData = psRelationshipfacade.getRelation(sourceUser, targetUser,
				PSRelationshipStatus.PENDING);
		Assert.assertNotNull("Relationship should exist.", relationshipData);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify permissions are created for users ${sourceUser} ${targetUser} ${grantedPermissions}
	 * ${requestedPermissions}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 */
	public void verifyPermissionsAreCreatedForUsers(final String sourceUser, final String targetUser,
			final String grantedPermissions, final String requestedPermissions)
	{
		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.PENDING);

		final List<PSPermissionData> permissionDataFromSourceToTarget = psPermissionfacade.getPermissionsForStatus(sourceUser,
				targetUser, status);
		final List<PSPermissionData> permissionDataFromTargetToSource = psPermissionfacade.getPermissionsForStatus(targetUser,
				sourceUser, status);

		final List<String> grantedPermissionsAsList = convertDelimitedStringToList(grantedPermissions, ",");
		final List<String> requestedPermissionsAsList = convertDelimitedStringToList(requestedPermissions, ",");
		// Verify granted permissions
		Assert.assertTrue("permissionDataFromSourceToTarget should not be empty",
				CollectionUtils.isNotEmpty(permissionDataFromSourceToTarget));
		if (CollectionUtils.isNotEmpty(permissionDataFromSourceToTarget))
		{
			Assert.assertTrue("Size should be equal", permissionDataFromSourceToTarget.size() == grantedPermissionsAsList.size());
		}
		for (final PSPermissionData permissionData : permissionDataFromSourceToTarget)
		{
			Assert.assertTrue(grantedPermissionsAsList.contains(permissionData.getPermissibleAreaItem().getShareableType()));
		}

		// Verify requested permissions
		Assert.assertTrue("permissionDataFromTargetToSource should not be empty",
				CollectionUtils.isNotEmpty(permissionDataFromTargetToSource));
		if (CollectionUtils.isNotEmpty(permissionDataFromTargetToSource))
		{
			Assert.assertTrue("Size should be equal", permissionDataFromTargetToSource.size() == requestedPermissionsAsList.size());
		}
		for (final PSPermissionData permissionData : permissionDataFromTargetToSource)
		{
			Assert.assertTrue(requestedPermissionsAsList.contains(permissionData.getPermissibleAreaItem().getShareableType()));
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>user approves relationship between users ${sourceUser} ${targetUser}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 */
	public void userApprovesRelationshipBetweenUsers(final String sourceUser, final String targetUser)
	{
		final PSRelationshipData relationshipData = psRelationshipfacade.getRelation(sourceUser, targetUser,
				PSRelationshipStatus.PENDING);
		Assert.assertNotNull(relationshipData);
		psRelationshipfacade.approveRelationRequest(relationshipData.getId());
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify relationship is approved ${sourceUser} ${targetUser}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 */
	public void verifyRelationshipIsApproved(final String sourceUser, final String targetUser)
	{
		final PSRelationshipData relationshipData = psRelationshipfacade.getRelation(sourceUser, targetUser,
				PSRelationshipStatus.ACTIVE);
		Assert.assertNotNull(relationshipData);
		Assert.assertNotNull(relationshipData.getApprovalDateTime());
		Assert.assertTrue(relationshipData.getStatus().getCode().equals(PSRelationshipStatus.ACTIVE.getCode()));
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify permissions are approved ${sourceUser} ${targetUser} ${grantedPermissions} ${requestedPermissions}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 */
	public void verifyPermissionsAreApproved(final String sourceUser, final String targetUser, final String grantedPermissions,
			final String requestedPermissions)
	{
		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.ACTIVE);
		final List<PSPermissionData> permissionDataFromSourceToTarget = psPermissionfacade.getPermissionsForStatus(sourceUser,
				targetUser, status);
		final List<PSPermissionData> permissionDataFromTargetToSource = psPermissionfacade.getPermissionsForStatus(targetUser,
				sourceUser, status);

		final List<String> grantedPermissionsAsList = convertDelimitedStringToList(grantedPermissions, ",");
		final List<String> requestedPermissionsAsList = convertDelimitedStringToList(requestedPermissions, ",");

		// Verify approval time is set
		Assert.assertTrue(CollectionUtils.isNotEmpty(permissionDataFromSourceToTarget));
		if (CollectionUtils.isNotEmpty(permissionDataFromSourceToTarget))
		{
			Assert.assertTrue(permissionDataFromSourceToTarget.size() == grantedPermissionsAsList.size());
		}
		for (final PSPermissionData permissionData : permissionDataFromSourceToTarget)
		{
			Assert.assertNotNull(permissionData.getApprovalDateTime());
			Assert.assertNull(permissionData.getDisApprovalDateTime());
		}

		Assert.assertTrue(CollectionUtils.isNotEmpty(permissionDataFromTargetToSource));
		if (CollectionUtils.isNotEmpty(permissionDataFromTargetToSource))
		{
			Assert.assertTrue(permissionDataFromTargetToSource.size() == requestedPermissionsAsList.size());
		}
		for (final PSPermissionData permissionData : permissionDataFromTargetToSource)
		{
			Assert.assertNotNull(permissionData.getApprovalDateTime());
			Assert.assertNull(permissionData.getDisApprovalDateTime());
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>user disapproves permission ${sourceUser} ${targetUser} ${permission}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param permission
	 */
	public void userDisapprovesPermission(final String sourceUser, final String targetUser, final String permission)
	{
		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.ACTIVE);
		final List<PSPermissionData> permissionData = psPermissionfacade.getPermissionsForStatus(sourceUser, targetUser, status);

		Assert.assertTrue(CollectionUtils.isNotEmpty(permissionData));
		for (final PSPermissionData data : permissionData)
		{
			if (data.getPermissibleAreaItem().getShareableType().equals(permission))
			{
				psPermissionfacade.rejectPermissionRequest(Collections.singletonList(data.getId()));
			}
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify permission is disabled ${sourceUser} ${targetUser} ${permission}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param permission
	 */
	public void verifyPermissionIsDisabled(final String sourceUser, final String targetUser, final String permission)
	{
		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.REJECTED);
		final List<PSPermissionData> permissionData = psPermissionfacade.getPermissionsForStatus(sourceUser, targetUser, status);

		Assert.assertTrue(CollectionUtils.isNotEmpty(permissionData));
		for (final PSPermissionData data : permissionData)
		{
			Assert.assertNotNull(data.getDisApprovalDateTime());
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>relationship is disabled for users ${sourceUser} ${targetUser}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 */
	public void relationshipIsDisabledForUsers(final String sourceUser, final String targetUser)
	{
		final PSRelationshipData relationshipData = psRelationshipfacade.getRelation(sourceUser, targetUser,
				PSRelationshipStatus.ACTIVE);
		psRelationshipfacade.rejectRelationRequest(relationshipData.getId());

		final List<PSPermissionData> permissionDataFromSourceToTarget = psPermissionfacade.getPermissionsForStatus(sourceUser,
				targetUser, null);
		final List<PSPermissionData> permissionDataFromTargetToSource = psPermissionfacade.getPermissionsForStatus(targetUser,
				sourceUser, null);

		final List<String> permissionIds = new ArrayList<>();
		for (final PSPermissionData permissionData : permissionDataFromSourceToTarget)
		{
			permissionIds.add(permissionData.getId());
		}

		for (final PSPermissionData permissionData : permissionDataFromTargetToSource)
		{
			permissionIds.add(permissionData.getId());
		}

		psPermissionfacade.rejectPermissionRequest(permissionIds);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify relationship and permissions are disabled ${sourceUser} ${targetUser} ${grantedPermissions}
	 * ${requestedPermissions}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param grantedPermissions
	 * @param requestedPermissions
	 */
	public void verifyRelationshipAndPermissionsAreDisabled(final String sourceUser, final String targetUser,
			final String grantedPermissions, final String requestedPermissions)
	{
		final PSRelationshipData relationshipData = psRelationshipfacade.getRelation(sourceUser, targetUser,
				PSRelationshipStatus.REJECTED);
		Assert.assertNotNull(relationshipData);
		Assert.assertNotNull(relationshipData.getDisApprovalDateTime());

		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.REJECTED);
		final List<PSPermissionData> permissionDataFromSourceToTarget = psPermissionfacade.getPermissionsForStatus(sourceUser,
				targetUser, status);
		final List<PSPermissionData> permissionDataFromTargetToSource = psPermissionfacade.getPermissionsForStatus(targetUser,
				sourceUser, status);
		final List<String> grantedPermissionsAsList = convertDelimitedStringToList(grantedPermissions, ",");
		final List<String> requestedPermissionsAsList = convertDelimitedStringToList(requestedPermissions, ",");

		// Verify dis approval time is set
		Assert.assertTrue(CollectionUtils.isNotEmpty(permissionDataFromSourceToTarget));
		if (CollectionUtils.isNotEmpty(permissionDataFromSourceToTarget))
		{
			Assert.assertTrue(permissionDataFromSourceToTarget.size() == grantedPermissionsAsList.size());
		}
		for (final PSPermissionData permissionData : permissionDataFromSourceToTarget)
		{
			Assert.assertNotNull(permissionData.getDisApprovalDateTime());
		}

		Assert.assertTrue(CollectionUtils.isNotEmpty(permissionDataFromTargetToSource));
		if (CollectionUtils.isNotEmpty(permissionDataFromTargetToSource))
		{
			Assert.assertTrue(permissionDataFromTargetToSource.size() == requestedPermissionsAsList.size());
		}
		for (final PSPermissionData permissionData : permissionDataFromTargetToSource)
		{
			Assert.assertNotNull(permissionData.getDisApprovalDateTime());
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>add permission for users ${sourceUser} ${targetUser} ${newPermission}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param newPermission
	 * @param isRequested
	 * @throws RelationshipDoesNotExistException
	 *
	 */
	public void addPermissionForUsers(final String sourceUser, final String targetUser, final String newPermission,
			final boolean isRequested)
	{
		psPermissionfacade.addPermission(sourceUser, targetUser, newPermission, PSPermissionStatus.PENDING, isRequested);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify permission for users ${sourceUser} ${targetUser} ${newPermission}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param newPermission
	 *
	 */
	public void verifyPermissionForUsers(final String sourceUser, final String targetUser, final String newPermission)
	{
		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.PENDING);
		final List<PSPermissionData> permissions = psPermissionfacade.getPermissionsForStatus(sourceUser, targetUser, status);

		PSPermissionData permissionData = null;
		for (final PSPermissionData permission : permissions)
		{
			if (permission.getPermissibleAreaItem().getShareableType().equals(newPermission))
			{
				permissionData = permission;
				break;
			}
		}
		if (permissionData != null && permissionData.getTargetUser() != null)
		{
			Assert.assertEquals(permissionData.getTargetUser().getUid(), targetUser); // NOSONAR
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify permission for users ${sourceUser} ${targetUser} ${newPermission}</i>
	 * <p>
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param newPermission
	 * @throws RelationshipAlreadyExistException
	 *
	 */
	public void verifyPermissionsForUsers(final String sourceUser, final String targetUser, final String newPermission)
	{
		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.PENDING);
		final List<PSPermissionData> permissionDataFromSourceToTarget = psPermissionfacade.getPermissionsForStatus(sourceUser,
				targetUser, status);

		Assert.assertTrue("permissionDataFromSourceToTarget should not be empty",
				CollectionUtils.isNotEmpty(permissionDataFromSourceToTarget));
		if (CollectionUtils.isNotEmpty(permissionDataFromSourceToTarget))
		{
			Assert.assertTrue(permissionDataFromSourceToTarget.get(0).getPermissibleAreaItem().getShareableType()
					.equalsIgnoreCase(newPermission));
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>get pending requests for user by UID</i>
	 * <p>
	 *
	 * @param userUID
	 * @return List<PSRelationshipData>
	 */
	public List<PSRelationshipData> getPendingRequestsForUserByUID(final String userUID)
	{
		return psRelationshipfacade.getPendingRequests(userUID);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>accept pending request for relation ${relationId} ${isGivenOrRequested}</i>
	 * <p>
	 *
	 * @param relationshipId
	 * @param isGivenOrRequested
	 */
	public void acceptPendingRequestForRelation(final String relationshipId, final boolean isGivenOrRequested)
	{
		psPermissionfacade.updatePendingRequest(relationshipId, true, isGivenOrRequested);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>reject pending request for relation ${relationId} ${isGivenOrRequested}</i>
	 * <p>
	 *
	 * @param relationshipId
	 * @param isGivenOrRequested
	 */
	public void rejectPendingRequestForRelation(final String relationshipId, final boolean isGivenOrRequested)
	{
		psPermissionfacade.updatePendingRequest(relationshipId, false, isGivenOrRequested);
	}

	private static List<String> convertDelimitedStringToList(final String data, final String delimiter)
	{
		final String[] output = data.split(delimiter);
		return Arrays.asList(output);
	}

	public void verifyRetrievePermissibleItemTypes(final Boolean isVisibleToUser, final Boolean isActive)
	{
		final List<PSPermissibleAreaData> permissibleList = psPermissionfacade.getPermissibleItemTypes(isVisibleToUser, isActive);
		Assert.assertTrue("permisssible area item should not be empty", CollectionUtils.isNotEmpty(permissibleList));
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify if source user is able to access target user permission for</i>
	 * <p>
	 *
	 * @param customerId
	 * @param poaReceiver
	 * @param item
	 * @return boolean
	 */
	public boolean verifyIfSourceUserIsAbleToAccessTargetUserPermissionFor(final String customerId, final String poaReceiver,
			final String item) throws RelationshipDoesNotExistException
	{
		return psPermissionfacade.isPermitted(customerId, poaReceiver, item);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>fetches users active and pending relationships count </i>
	 * <p>
	 *
	 * @param sourceUserEmail
	 */
	public void verifyRetrievingUsersRelationshipsCountFor(final String sourceUserEmail)
	{
		final Integer activeAndPendingRelationshipsCount = psRelationshipfacade
				.getUserActiveAndPendingRelationshipsCount(sourceUserEmail);
		Assert.assertEquals(Integer.valueOf(3), activeAndPendingRelationshipsCount);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>fetches users relationships orders </i>
	 * <p>
	 *
	 * @param sourceUserEmail
	 * @param expectedCount
	 */
	public void verifyRetrievingUsersRelationshipsOrdersFor(final String sourceUserEmail, final int expectedCount)
	{
		final List<OrderData> orders = psRelationshipfacade.getOrdersForUserRelationships(sourceUserEmail);
		Assert.assertEquals(expectedCount, orders.size());
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>fetches users relationships drafts </i>
	 * <p>
	 *
	 * @param sourceUserEmail
	 * @param expectedCount
	 */
	public void verifyRetrievingUsersRelationshipsDraftsFor(final String sourceUserEmail, final int expectedCount)
	{
		final List<CartData> carts = psRelationshipfacade.getDraftsForUserRelationships(sourceUserEmail);
		Assert.assertEquals(expectedCount, carts.size());
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>fetches users relationships addresses </i>
	 * <p>
	 *
	 * @param sourceUserEmail
	 * @param expectedCount
	 */
	public void verifyRetrievingUsersRelationshipsAddressFor(final String sourceUserEmail, final int expectedCount)
	{
		final List<AddressData> addresses = psRelationshipfacade.getAddressesForUserRelationships(sourceUserEmail);
		Assert.assertEquals(expectedCount, addresses.size());
	}

}
