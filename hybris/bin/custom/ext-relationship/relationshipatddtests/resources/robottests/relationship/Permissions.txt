*** Settings ***
Test Setup        import user data from resource /impex/testPermissionData.impex
Resource          relationship/Permission_Keywords.txt


*** Variables ***
${source}=    allison.citizen@stateofrosebud.com
${target}=    john.citizen@stateofrosebud.com
${typeCode}=    Order
${permissibleAreaCode}=    order
${changeTypeCode}=    PSRelationship


*** Test Cases ***
Test_Verify_Get_Permissible_Areas_For_Permissions
    [Documentation]    Verify permissible areas of the given permission
    verify get permissible areas for permissions ${source} ${target}

Test_Request_More_Permissions
    [Documentation]    Give more permissions test
    request more permissions ${source} ${target} ${typeCode}
    verify more permissions were added ${source} ${target} ${permissibleAreaCode}

Test_Change_Permissions
    [Documentation]    Change permissions test
    change permissions ${source} ${target} ${changeTypeCode}
    verify inactive permission ${source} ${target}
