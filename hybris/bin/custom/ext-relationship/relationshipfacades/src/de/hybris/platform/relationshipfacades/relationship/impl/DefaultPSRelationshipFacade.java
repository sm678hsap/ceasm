/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.relationship.impl;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationship.data.PSRelationshipData;
import de.hybris.platform.relationshipfacades.permission.PSPermissionFacade;
import de.hybris.platform.relationshipfacades.permission.itemtypes.populators.PSPermissionDataPopulator;
import de.hybris.platform.relationshipfacades.relationship.PSRelationshipFacade;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.relationshipservices.relationship.data.PSRelationshipParameter;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * DefaultPSRelationshipFacade implementation of {@link PSRelationshipFacade}
 */
public class DefaultPSRelationshipFacade implements PSRelationshipFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSRelationshipFacade.class);
	private static final String NON_POA_RELATIONSHIP = "non-poa";
	private static final String POA_HOLDER_RELATIONSHIP = "poa-holder";
	private static final String POA_RECEIVER_RELATIONSHIP = "poa-receiver";
	private static final int RELATIONSHIPS_NOT_AVAILABLE = 0;

	private PSRelationshipService relationshipService;
	private PSPermissionService permissionService;
	private UserService userService;
	private Converter<PSRelationshipModel, PSRelationshipData> relationshipConverter;
	private Converter<UserModel, CustomerData> customerConverter;
	private CartService cartService;
	private ModelService modelService;
	private CheckoutFacade checkoutFacade;
	private CustomerAccountService customerAccountService;
	private CommerceCommonI18NService commerceCommonI18NService;
	private Converter<AddressModel, AddressData> addressConverter;
	private PSPermissionFacade permissionFacade;
	private Converter<OrderModel, OrderData> orderConverter;
	private Converter<CartModel, CartData> cartConverter;
	private PSPermissionDataPopulator psPermissionDataPopulator;

	@Override
	public void addRelationship(final String sourceUserEmail, final String targetUserEmail, final List<String> grantedTypeCodes,
			final List<String> requestedTypeCodes, final String titleCode, final String firstName, final String lastName)
			throws RelationshipAlreadyExistException
	{
		if (getUserService().isUserExisting(sourceUserEmail))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);
			final PSRelationshipParameter relationshipParameters = new PSRelationshipParameter();
			relationshipParameters.setRelationshipStatus(PSRelationshipStatus.PENDING);
			relationshipParameters.setSourceUser(sourceUser);
			if (getUserService().isUserExisting(targetUserEmail))
			{
				final UserModel targetUser = getUserService().getUserForUID(targetUserEmail);
				relationshipParameters.setTargetUser(targetUser);
				relationshipParameters.setFirstName(firstName);
				relationshipParameters.setLastName(lastName);
				relationshipParameters.setTitle(getTitle(titleCode));
				getRelationshipService().addRelationship(relationshipParameters);
				LOG.info("Target user [{}] exists. Adding the permissions", targetUserEmail);
				permissionFacade.addPermission(sourceUserEmail, targetUserEmail, grantedTypeCodes, PSPermissionStatus.PENDING, false);
				permissionFacade.addPermission(targetUserEmail, sourceUserEmail, requestedTypeCodes, PSPermissionStatus.PENDING,
						true);
			}
			else
			{
				LOG.info("Target user [{}] does not exist", targetUserEmail);
				relationshipParameters.setTargetEmail(targetUserEmail);
				relationshipParameters.setTitle(getTitle(titleCode));
				relationshipParameters.setFirstName(firstName);
				relationshipParameters.setLastName(lastName);
				getRelationshipService().addRelationship(relationshipParameters);
			}
		}
		else
		{
			throw new UnknownIdentifierException("source user does not exist");
		}
	}

	private TitleModel getTitle(final String titleCode)
	{
		return StringUtils.isEmpty(titleCode) ? null : getUserService().getTitleForCode(titleCode);
	}

	@Override
	public List<PSRelationshipData> getAllActiveRelationsForUser(final String sourceUserEmail)
	{
		if (StringUtils.isNotEmpty(sourceUserEmail) && getUserService().isUserExisting(sourceUserEmail))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);
			final List<PSRelationshipModel> relationshipModel = getRelationshipService().getRelations(sourceUser,
					PSRelationshipStatus.ACTIVE);
			return getRelationshipConverter().convertAll(relationshipModel);
		}
		return Collections.emptyList();
	}

	@Override
	public Integer getUserActiveAndPendingRelationshipsCount(final String sourceUserEmail)
	{
		if (StringUtils.isNotEmpty(sourceUserEmail) && getUserService().isUserExisting(sourceUserEmail))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);
			return getRelationshipService().getUserActiveAndPendingRelationshipsCount(sourceUser);
		}
		return Integer.valueOf(RELATIONSHIPS_NOT_AVAILABLE);
	}

	@Override
	public PSRelationshipData getActiveRelation(final String sourceUserEmail, final String targetUserEmail)
	{
		if (getUserService().isUserExisting(sourceUserEmail) && (getUserService().isUserExisting(targetUserEmail)))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);
			final UserModel targetUser = getUserService().getUserForUID(targetUserEmail);
			final PSRelationshipModel relationshipModel = getRelationshipService().getRelation(sourceUser, targetUser,
					PSRelationshipStatus.ACTIVE);
			return getRelationshipConverter().convert(relationshipModel);
		}
		return null;
	}

	@Override
	public PSRelationshipData getPendingRelation(final String sourceUserEmail, final String targetUserEmail)
	{
		if (getUserService().isUserExisting(sourceUserEmail))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);
			if (getUserService().isUserExisting(targetUserEmail))
			{
				LOG.debug("Target user exists {}", targetUserEmail);
				final UserModel targetUser = getUserService().getUserForUID(targetUserEmail);
				final PSRelationshipModel relationshipModel = getRelationshipService().getRelation(sourceUser, targetUser,
						PSRelationshipStatus.PENDING);
				return getRelationshipConverter().convert(relationshipModel);
			}
			else
			{
				LOG.debug("Target user does not exists {}", targetUserEmail);
				final PSRelationshipModel relationshipModel = getRelationshipService().getRelation(sourceUser, targetUserEmail,
						PSRelationshipStatus.PENDING);
				return getRelationshipConverter().convert(relationshipModel);
			}
		}
		LOG.debug("Source user does not exists {}", sourceUserEmail);
		return null;
	}

	@Override
	public List<PSRelationshipData> getRelationForStatus(final String sourceUserEmail, final PSRelationshipStatus status)
	{
		if (getUserService().isUserExisting(sourceUserEmail))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);
			final List<PSRelationshipModel> relationshipModel = getRelationshipService().getRelations(sourceUser, status);
			return getRelationshipConverter().convertAll(relationshipModel);
		}
		LOG.debug("Source user does not exists {}", sourceUserEmail);
		return Collections.emptyList();
	}

	@Override
	public void updateTargetUserInRelationship(final String relationshipId, final String targetUserEmail)
	{
		final PSRelationshipModel relationshipModel = getRelationshipService().getRelationForPk(relationshipId);
		if (relationshipModel != null && getUserService().isUserExisting(targetUserEmail))
		{
			getRelationshipService().updateTargetUserInRelationship(relationshipModel,
					getUserService().getUserForUID(targetUserEmail));
		}
	}

	@Override
	public PSRelationshipData getRelation(final String sourceUserEmail, final String targetUserEmail,
			final PSRelationshipStatus status)
	{
		final PSRelationshipModel relationshipModel = getRelationship(sourceUserEmail, targetUserEmail, status);
		return (relationshipModel == null) ? null : getRelationshipConverter().convert(relationshipModel);
	}

	private PSRelationshipModel getRelationship(final String sourceUserEmail, final String targetUserEmail,
			final PSRelationshipStatus status)
	{
		final boolean isEmailsNotNull = StringUtils.isNotEmpty(sourceUserEmail) && StringUtils.isNotEmpty(targetUserEmail);
		PSRelationshipModel relationshipModel = null;
		if (isEmailsNotNull && getUserService().isUserExisting(sourceUserEmail))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);

			if (getUserService().isUserExisting(targetUserEmail))
			{
				final UserModel targetUser = getUserService().getUserForUID(targetUserEmail);
				relationshipModel = getRelationshipService().getRelation(sourceUser, targetUser, status);
				if (relationshipModel == null)
				{
					//if the relationship sourceUser / targetUser / Status is not found,
					//then try to fetch the relationship based on the targetEmail
					relationshipModel = getRelationshipService().getRelation(sourceUser, targetUserEmail, status);
				}
				else
				{
					//this log message is incorrect, since the relationship was FOUND
					//LOG.warn("No relationship exists for the source user {} and target user {}", sourceUser, targetUserEmail);
				}
			}
			else
			{
				relationshipModel = getRelationshipService().getRelation(sourceUser, targetUserEmail, status);
			}
		}
		return relationshipModel;
	}

	@Override
	public PSRelationshipData getActiveOrPendingRelation(final String sourceUserEmail, final String targetUserEmail)
	{
		PSRelationshipModel relationship = getRelationship(sourceUserEmail, targetUserEmail, PSRelationshipStatus.ACTIVE);
		if (relationship == null)
		{
			relationship = getRelationship(sourceUserEmail, targetUserEmail, PSRelationshipStatus.PENDING);
		}
		return (relationship == null) ? null : getRelationshipConverter().convert(relationship);
	}

	@Override
	public PSRelationshipData getRelationForId(final String relationshipId)
	{
		final PSRelationshipModel relationshipModel = getRelationshipService().getRelationForPk(relationshipId);
		return getRelationshipConverter().convert(relationshipModel);
	}

	@Override
	public void updateRelation(final String relationshipId, final PSRelationshipStatus relationshipStatus)
	{
		if (StringUtils.isNotEmpty(relationshipId) && relationshipStatus != null)
		{
			final PSRelationshipModel relationshipModel = getRelationshipService().getRelationForPk(relationshipId);
			if (relationshipModel != null)
			{
				getRelationshipService().updateRelation(relationshipModel, relationshipStatus);
			}
			else
			{
				LOG.warn("unable to find the the relationship for the given relationship ID {}", relationshipId);
			}
		}
	}

	@Override
	public void approveRelationRequest(final String relationshipId)
	{
		getPermissionService().approveRelationRequest(relationshipId);
	}

	@Override
	public void rejectRelationRequest(final String relationshipId)
	{
		if (StringUtils.isNotEmpty(relationshipId))
		{
			relationshipService.rejectRelationRequest(relationshipService.getRelationForPk(relationshipId));
		}
	}

	@Override
	public List<PSRelationshipData> getRelationsForUserAndPermissibleItemType(final String userId, final String itemType)
	{
		if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(itemType))
		{
			return relationsPermittedForItemTypeAndUsers(userId, itemType, getAllActiveRelationsForUser(userId));
		}
		return Collections.emptyList();
	}

	private List<PSRelationshipData> relationsPermittedForItemTypeAndUsers(final String userId, final String itemType,
			final List<PSRelationshipData> relations)
	{
		final List<PSRelationshipData> relationsWithActivePermissionForType = new ArrayList();
		for (final PSRelationshipData relation : relations)
		{
			try
			{
				if (relation.getTargetUser() != null && permissionFacade.isPermitted(userId,
						userId.equals(relation.getTargetUser().getUid()) ? relation.getSourceUser().getUid()
								: relation.getTargetUser().getUid(),
						itemType))
				{
					relationsWithActivePermissionForType.add(relation);
				}
			}
			catch (final RelationshipDoesNotExistException e)
			{
				LOG.error("relationship does not exists {}", e);
			}
		}
		return relationsWithActivePermissionForType;
	}

	@Override
	public List<CustomerData> getRelationshipUsersForUserAndPermissibleItemType(final String userId, final String itemType)

	{
		final List<PSRelationshipData> relationships = getRelationsForUserAndPermissibleItemType(userId, itemType);

		if (CollectionUtils.isNotEmpty(relationships))
		{
			return relationships.stream()
					.map(relation -> (userId.equals(relation.getTargetUser().getUid()) ? relation.getSourceUser()
							: relation.getTargetUser()))
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	@Override
	public boolean isUserExists(final String email)
	{
		return getUserService().isUserExisting(email);
	}

	@Override
	public Map<String, List<PSRelationshipData>> getRelationForStatusByType(final String sourceUserId,
			final PSRelationshipStatus status, final Map<String, String> allPendingRequestCountMap)
	{
		final List<PSRelationshipData> userAllRelationships = getRelationForStatus(sourceUserId, status);

		return createRelationDataByType(sourceUserId, userAllRelationships, allPendingRequestCountMap);
	}

	@Override
	public List<PSRelationshipData> getPendingRequests(final String userEmail)
	{
		final List<PSRelationshipData> relationshipDatas = getRelationForStatus(userEmail, null);

		if (CollectionUtils.isNotEmpty(relationshipDatas))
		{
			return filterPendingRequests(userEmail, relationshipDatas);
		}
		return Collections.emptyList();
	}

	private List<PSRelationshipData> filterPendingRequests(final String userEmail,
			final List<PSRelationshipData> relationshipDatas)
	{
		return relationshipDatas.stream().filter(relationship -> (relationship.getStatus().equals(PSRelationshipStatus.PENDING)
				&& (userEmail.equalsIgnoreCase(relationship.getTargetEmail())
						|| (relationship.getTargetUser() != null && userEmail.equalsIgnoreCase(relationship.getTargetUser().getUid()))))
				|| relationship.isPendingGivenPermissionsToCurrentUser()
				|| relationship.isPendingRequestedPermissionsFromCurrentUser()).collect(Collectors.toCollection(ArrayList::new));
	}

	private int getPendingRequestsCount(final String userEmail, final List<PSRelationshipData> relationshipDatas)
	{
		int pendingCount = 0;
		for (final PSRelationshipData relationship : relationshipDatas)
		{

			if (relationship.getStatus().equals(PSRelationshipStatus.PENDING) && (userEmail
					.equalsIgnoreCase(relationship.getTargetEmail())
					|| (relationship.getTargetUser() != null && userEmail.equalsIgnoreCase(relationship.getTargetUser().getUid()))))
			{
				if (relationship.getPermissionsGivenOnRequestPendingCount() > 0
						|| relationship.getPermissionsReceivedOnOwnPendingCount() > 0)
				{
					pendingCount = relationship.getPermissionsGivenOnRequestPendingCount() > 0 ? pendingCount + 1 : pendingCount;
					pendingCount = relationship.getPermissionsReceivedOnOwnPendingCount() > 0 ? pendingCount + 1 : pendingCount;
				}
				else
				{
					pendingCount++;
				}
			}
			if (relationship.getStatus().equals(PSRelationshipStatus.ACTIVE))
			{

				pendingCount = relationship.getPermissionsGivenOnRequestPendingCount() > 0 ? pendingCount + 1 : pendingCount;
				pendingCount = relationship.getPermissionsReceivedOnOwnPendingCount() > 0 ? pendingCount + 1 : pendingCount;
			}
		}
		return pendingCount;
	}

	@Override
	public void updateCartUserInContext(final String userId)
	{
		if (StringUtils.isNotEmpty(userId) && getUserService().isUserExisting(userId) && getCartService().hasSessionCart())
		{

			final CartModel sessionCart = getCartService().getSessionCart();
			sessionCart.setUserInContext(getUserService().getUserForUID(userId));
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);
			LOG.info("Updating the user in context for cart {} with user {}", sessionCart, userId);
		}
	}

	@Override
	public CustomerData getUserInContextForCurrentCart()
	{
		if (getCheckoutFacade().hasCheckoutCart() && getUserService().getCurrentUser() != null)
		{
			final CustomerData userInContext = getCheckoutFacade().getCheckoutCart().getUserInContext();
			final String currentUserUid = getUserService().getCurrentUser().getUid();
			if (userInContext != null && !StringUtils.equals(currentUserUid, userInContext.getUid()))
			{
				return userInContext;
			}
		}
		LOG.debug("Either there is no session cart or session user");
		return null;
	}

	@Override
	public Map<String, List<PSRelationshipData>> getRelationshipDataByType(final String sourceUserId, final String targetUserId,
			final Map<String, String> allPendingRequestCountMap, final PSRelationshipStatus status)
	{
		final PSRelationshipData relationship = getRelation(sourceUserId, targetUserId, status);
		final List<PSRelationshipData> userAllRelationships = new ArrayList<>();
		userAllRelationships.add(relationship);

		return createRelationDataByType(sourceUserId, userAllRelationships, allPendingRequestCountMap);
	}

	@Override
	public Map<String, List<PSRelationshipData>> createRelationDataByType(final String sourceUserId,
			final List<PSRelationshipData> userAllRelationships, final Map<String, String> allPendingRequestCountMap)
	{
		if (userAllRelationships != null)
		{

			final List<PSRelationshipData> nonPOARelationships = new ArrayList<>();
			final List<PSRelationshipData> poaHolderRelationships = new ArrayList<>();
			final List<PSRelationshipData> poaReceiverRelationships = new ArrayList<>();

			for (final PSRelationshipData currentRelationship : userAllRelationships)
			{
				psPermissionDataPopulator.setPermissionsData(currentRelationship, sourceUserId);

				if (BooleanUtils.isTrue(currentRelationship.getIsSourcePoaHolder())
						|| BooleanUtils.isTrue(currentRelationship.getIsTargetPoaHolder()))
				{

					setPOARelationshipData(poaHolderRelationships, poaReceiverRelationships, currentRelationship, sourceUserId);

				}
				else
				{
					setNonPOARelationshipData(nonPOARelationships, currentRelationship, sourceUserId);
				}
			}

			allPendingRequestCountMap.put(sourceUserId,
					Integer.toString(getPendingRequestsCount(sourceUserId, userAllRelationships)));

			final HashMap<String, List<PSRelationshipData>> allUserRelationshipsByCategory = new HashMap<String, List<PSRelationshipData>>();

			allUserRelationshipsByCategory.put(NON_POA_RELATIONSHIP, nonPOARelationships);
			allUserRelationshipsByCategory.put(POA_HOLDER_RELATIONSHIP, poaHolderRelationships);
			allUserRelationshipsByCategory.put(POA_RECEIVER_RELATIONSHIP, poaReceiverRelationships);
			return allUserRelationshipsByCategory;
		}
		return null;
	}

	private void setPOARelationshipData(final List<PSRelationshipData> poaHolderRelationships,
			final List<PSRelationshipData> poaReceiverRelationships, final PSRelationshipData currentRelationship,
			final String sourceUserId)
	{
		if (sourceUserId.equalsIgnoreCase(currentRelationship.getSourceUser().getUid()))
		{
			if (BooleanUtils.isTrue(currentRelationship.getIsSourcePoaHolder()))
			{
				poaHolderRelationships.add(currentRelationship);
			}
			else
			{
				poaReceiverRelationships.add(currentRelationship);
			}
		}
		else
		{
			if (BooleanUtils.isTrue(currentRelationship.getIsSourcePoaHolder()))
			{
				poaReceiverRelationships.add(currentRelationship);
			}
			else
			{
				poaHolderRelationships.add(currentRelationship);
			}
		}

	}

	private void setNonPOARelationshipData(final List<PSRelationshipData> nonPOARelationships,
			final PSRelationshipData currentRelationship, final String sourceUserId)
	{
		if (!currentRelationship.getStatus().equals(PSRelationshipStatus.PENDING)
				|| (currentRelationship.getStatus().equals(PSRelationshipStatus.PENDING)
						&& currentRelationship.getSourceUser().getUid().equalsIgnoreCase(sourceUserId)))
		{
			nonPOARelationships.add(currentRelationship);
		}
	}


	@Override
	public CustomerData getCustomerDataForUid(final String uid)
	{
		final UserModel userModel = getUserService().getUserForUID(uid);
		return getCustomerConverter().convert(userModel);
	}

	@Override
	public List<AddressData> getRelationshipAddressBookByUser(final String userId)
	{
		final CustomerModel relUser = (CustomerModel) getUserService().getUserForUID(userId);
		final Collection<AddressModel> addresses = getCustomerAccountService().getAddressBookDeliveryEntries(relUser);

		if (CollectionUtils.isNotEmpty(addresses))
		{
			final List<AddressData> result = new ArrayList<AddressData>();
			final Collection<CountryModel> deliveryCountries = getCommerceCommonI18NService().getAllCountries();
			final AddressData defaultAddress = getDefaultAddress(relUser);

			for (final AddressModel address : addresses)
			{
				filterInvalidAddresses(deliveryCountries, result, address, defaultAddress);
			}

			return result;
		}

		return Collections.emptyList();
	}

	private void filterInvalidAddresses(final Collection<CountryModel> deliveryCountries, final List<AddressData> result,
			final AddressModel address, final AddressData defaultAddress)
	{
		if (address.getCountry() != null && deliveryCountries != null && deliveryCountries.contains(address.getCountry()))
		{
			final AddressData addressData = getAddressConverter().convert(address);

			if (defaultAddress != null && defaultAddress.getId() != null && defaultAddress.getId().equals(addressData.getId()))
			{
				addressData.setDefaultAddress(true);
				result.add(0, addressData);
			}
			else
			{
				result.add(addressData);
			}
		}
	}

	/**
	 * returns Default Address
	 *
	 * @param currentCustomer
	 */
	private AddressData getDefaultAddress(final CustomerModel currentCustomer)
	{
		AddressData defaultAddressData = null;

		final AddressModel defaultAddress = getCustomerAccountService().getDefaultAddress(currentCustomer);
		if (defaultAddress != null)
		{
			defaultAddressData = getAddressConverter().convert(defaultAddress);
		}
		else
		{
			final List<AddressModel> addresses = getCustomerAccountService().getAddressBookEntries(currentCustomer);
			if (CollectionUtils.isNotEmpty(addresses))
			{
				defaultAddressData = getAddressConverter().convert(addresses.get(0));
			}
		}
		return defaultAddressData;
	}

	@Override
	public PSRelationshipData cancelPendingRelationship(final String relationshipId)
	{
		final PSRelationshipData relationshipData = getRelationForId(relationshipId);

		final String relationshipSourceUser = relationshipData.getSourceUser().getUid();

		String relationshipTargetUser = relationshipData.getTargetEmail();
		if (relationshipData.getTargetUser() != null)
		{
			relationshipTargetUser = relationshipData.getTargetUser().getUid();
		}

		updateRelation(relationshipId, PSRelationshipStatus.CANCELLED);

		List<PSPermissionData> permissions = permissionFacade.getGivenOrRequestedPermissionsForTargetUser(relationshipSourceUser,
				relationshipTargetUser, Collections.singletonList(PSPermissionStatus.PENDING), true);

		permissionFacade.cancelPermissionRequest(permissions.stream().map(PSPermissionData::getId).collect(Collectors.toList()));

		permissions = permissionFacade.getGivenOrRequestedPermissionsForTargetUser(relationshipSourceUser, relationshipTargetUser,
				Collections.singletonList(PSPermissionStatus.PENDING), false);

		permissionFacade.cancelPermissionRequest(permissions.stream().map(PSPermissionData::getId).collect(Collectors.toList()));
		return relationshipData;
	}

	@Override
	public CustomerData getCustomerForPK(final String customerPK)
	{
		final CustomerModel customerModel = getRelationshipService().getCustomerForPK(customerPK);
		return getCustomerConverter().convert(customerModel);
	}


	@Override
	public List<OrderData> getOrdersForUserRelationships(final String userId)
	{
		if (StringUtils.isNotEmpty(userId))
		{
			final UserModel user = getUserService().getUserForUID(userId);
			final List<OrderModel> orders = getRelationshipService().getOrdersForUserRelationships(user);
			return getOrderConverter().convertAll(orders);
		}
		return Collections.emptyList();
	}

	@Override
	public List<CartData> getDraftsForUserRelationships(final String userId)
	{
		if (StringUtils.isNotEmpty(userId))
		{
			final UserModel user = getUserService().getUserForUID(userId);
			final List<CartModel> drafts = getRelationshipService().getDraftsForUserRelationships(user);
			return getCartConverter().convertAll(drafts);
		}
		return Collections.emptyList();
	}

	@Override
	public List<AddressData> getAddressesForUserRelationships(final String userId)
	{
		if (StringUtils.isNotEmpty(userId))
		{
			final UserModel user = getUserService().getUserForUID(userId);
			final List<AddressModel> addresses = getRelationshipService().getAddressesForUserRelationships(user);
			return getAddressConverter().convertAll(addresses);
		}
		return Collections.emptyList();
	}

	protected PSRelationshipService getRelationshipService()
	{
		return relationshipService;
	}

	@Required
	public void setRelationshipService(final PSRelationshipService relationshipService)
	{
		this.relationshipService = relationshipService;
	}

	protected PSPermissionService getPermissionService()
	{
		return permissionService;
	}

	@Required
	public void setPermissionService(final PSPermissionService permissionService)
	{
		this.permissionService = permissionService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected Converter<PSRelationshipModel, PSRelationshipData> getRelationshipConverter()
	{
		return relationshipConverter;
	}

	@Required
	public void setRelationshipConverter(final Converter<PSRelationshipModel, PSRelationshipData> relationshipConverter)
	{
		this.relationshipConverter = relationshipConverter;
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	@Required
	public void setCheckoutFacade(final CheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}

	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	protected Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}

	protected PSPermissionFacade getPermissionFacade()
	{
		return permissionFacade;
	}

	@Required
	public void setPermissionFacade(final PSPermissionFacade permissionFacade)
	{
		this.permissionFacade = permissionFacade;
	}

	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}

	@Required
	public void setOrderConverter(final Converter<OrderModel, OrderData> orderConverter)
	{
		this.orderConverter = orderConverter;
	}

	protected Converter<CartModel, CartData> getCartConverter()
	{
		return cartConverter;
	}

	@Required
	public void setCartConverter(final Converter<CartModel, CartData> cartConverter)
	{
		this.cartConverter = cartConverter;
	}

	/**
	 * @return the psPermissionDataPopulator
	 */
	protected PSPermissionDataPopulator getPsPermissionDataPopulator()
	{
		return psPermissionDataPopulator;
	}

	/**
	 * @param psPermissionDataPopulator
	 *           the psPermissionDataPopulator to set
	 */
	@Required
	public void setPsPermissionDataPopulator(final PSPermissionDataPopulator psPermissionDataPopulator)
	{
		this.psPermissionDataPopulator = psPermissionDataPopulator;
	}

}
