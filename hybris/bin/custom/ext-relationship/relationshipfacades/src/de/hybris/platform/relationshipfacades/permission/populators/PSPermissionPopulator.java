/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.permission.populators;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationship.data.PSPermissibleAreaData;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;


/**
 * PSPermissionPopulator class
 */
public class PSPermissionPopulator<SOURCE extends PSPermissionModel, TARGET extends PSPermissionData>
		implements Populator<SOURCE, TARGET>
{
	private Converter<UserModel, CustomerData> customerConverter;
	private Converter<AbstractPSPermissibleAreaModel, PSPermissibleAreaData> permissibleAreaItemConverter;

	/**
	 * Adds information about permission
	 *
	 * @param source
	 * @param target
	 *
	 * @throws ConversionException
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		target.setId(source.getPk().getLongValueAsString());
		target.setSourceUser(getCustomerConverter().convert(source.getSourceUser()));
		target.setTargetUser(getCustomerConverter().convert(source.getTargetUser()));
		target.setStatus(source.getPermissionStatus());
		target.setApprovalDateTime(source.getApprovalDateTime());
		target.setDisApprovalDateTime(source.getDisApprovalDateTime());
		target.setPermissibleAreaItem(getPermissibleAreaItemConverter().convert(source.getPermissibleAreaItemType()));
		target.setIsRequested(source.getIsRequested());
	}

	public Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	public Converter<AbstractPSPermissibleAreaModel, PSPermissibleAreaData> getPermissibleAreaItemConverter()
	{
		return permissibleAreaItemConverter;
	}

	@Required
	public void setPermissibleAreaItemConverter(
			final Converter<AbstractPSPermissibleAreaModel, PSPermissibleAreaData> permissibleAreaItemConverter)
	{
		this.permissibleAreaItemConverter = permissibleAreaItemConverter;
	}

}
