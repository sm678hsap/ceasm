/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.relationship.populators;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationship.data.PSRelationshipData;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * PSRelationshipPopulator class
 */
public class PSRelationshipPopulator<SOURCE extends PSRelationshipModel, TARGET extends PSRelationshipData>
		implements Populator<SOURCE, TARGET>
{
	private PSPermissionService permissionService;
	private PSRelationshipService relationshipService;
	private Converter<UserModel, CustomerData> customerConverter;
	private Converter<PSPermissionModel, PSPermissionData> permissionConverter;

	/**
	 * Adds information about relationship
	 *
	 * @param source
	 * @param target
	 *
	 * @throws ConversionException
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		target.setSourceUser(getCustomerConverter().convert(source.getSourceUser()));
		if (source.getTargetUser() != null)
		{
			target.setTargetUser(getCustomerConverter().convert(source.getTargetUser()));
		}


		target.setTargetEmail(source.getTargetEmail());
		if (source.getTitle() != null)
		{

			target.setTitle(source.getTitle().getName());
		}
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());

		target.setStatus(source.getRelationshipStatus());
		target.setId(source.getPk().getLongValueAsString());
		target.setApprovalDateTime(source.getApprovalDateTime());
		target.setDisApprovalDateTime(source.getDisApprovalDateTime());
		target.setIsSourcePoaHolder(source.getIsSourcePoaHolder());
		target.setIsTargetPoaHolder(source.getIsTargetPoaHolder());

		final List<PSPermissionData> givenPermissionsData = getGivenOrRequestedPermissionsForCurrentUser(source, true);
		final List<PSPermissionData> requestedPermissionsData = getGivenOrRequestedPermissionsForCurrentUser(source, false);

		target.setGivenPermissionsToCurrentUser(givenPermissionsData);
		target.setRequestedPermissionsFromCurrentUser(requestedPermissionsData);
		target.setPendingGivenPermissionsToCurrentUser(hasPendingPermission(givenPermissionsData));
		target.setPendingRequestedPermissionsFromCurrentUser(hasPendingPermission(requestedPermissionsData));

	}

	/**
	 * returns given or requested permissions to/from current user where PSPermissionStatus is ACTIVE and PENDING
	 *
	 * @param source
	 * @param isGivenOrRequested
	 * @return List<PSPermissionData>
	 */
	protected List<PSPermissionData> getGivenOrRequestedPermissionsForCurrentUser(final SOURCE source,
			final boolean isGivenOrRequested)
	{
		if (source.getSourceUser() != null && source.getTargetUser() != null)
		{
			final UserModel targetUser = relationshipService.getUserInContext();
			final UserModel sourceUser = source.getSourceUser().getUid().equals(targetUser.getUid()) ? source.getTargetUser()
					: source.getSourceUser();
			final List<PSPermissionStatus> permissionStatus = Arrays.asList(PSPermissionStatus.ACTIVE, PSPermissionStatus.PENDING);

			final List<PSPermissionModel> permissions = getPermissionService()
					.getGivenOrRequestedPermissionsForTargetUser(sourceUser, targetUser, permissionStatus, isGivenOrRequested);
			return getPermissionConverter().convertAll(permissions);
		}
		return Collections.emptyList();
	}

	protected boolean hasPendingPermission(final List<PSPermissionData> permissionsData)
	{
		if (CollectionUtils.isNotEmpty(permissionsData))
		{
			return permissionsData.stream().filter(permission -> PSPermissionStatus.PENDING.equals(permission.getStatus()))
					.findFirst().isPresent();
		}
		return false;

	}

	public Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	protected PSPermissionService getPermissionService()
	{
		return permissionService;
	}

	@Required
	public void setPermissionService(final PSPermissionService permissionService)
	{
		this.permissionService = permissionService;
	}

	protected Converter<PSPermissionModel, PSPermissionData> getPermissionConverter()
	{
		return permissionConverter;
	}

	@Required
	public void setPermissionConverter(final Converter<PSPermissionModel, PSPermissionData> permissionConverter)
	{
		this.permissionConverter = permissionConverter;
	}

	public PSRelationshipService getRelationshipService()
	{
		return relationshipService;
	}

	@Required
	public void setRelationshipService(final PSRelationshipService relationshipService)
	{
		this.relationshipService = relationshipService;
	}
}
