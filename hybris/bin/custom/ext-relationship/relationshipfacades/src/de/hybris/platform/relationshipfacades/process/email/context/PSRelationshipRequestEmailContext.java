/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipProcessModel;

import org.apache.velocity.tools.generic.EscapeTool;


/**
 * Email Context for Relationship Request email.
 */
public class PSRelationshipRequestEmailContext extends AbstractEmailContext<PSRelationshipProcessModel>
{
	private static final String ISGUESTUSER = "isGuestUser";
	private static final String ESCAPE_TOOL = "esc";

	@Override
	public void init(final PSRelationshipProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);

		put(EMAIL, businessProcessModel.getTargetEmail());
		put(ISGUESTUSER, businessProcessModel.getGuestUser());
		put(DISPLAY_NAME, businessProcessModel.getCustomerDisplayName());
		put(ESCAPE_TOOL, new EscapeTool());
	}

	@Override
	protected BaseSiteModel getSite(final PSRelationshipProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final PSRelationshipProcessModel businessProcessModel)
	{
		return null;
	}

	@Override
	protected LanguageModel getEmailLanguage(final PSRelationshipProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}
}
