/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.permission;

import de.hybris.platform.relationship.data.PSPermissibleAreaData;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;

import java.util.List;


/**
 * PSPermissionFacade interface
 */
public interface PSPermissionFacade
{

	/**
	 * adds permission for given users ,item and status
	 *
	 * @param sourceUserId
	 *           Email of user that requests for permission.
	 * @param targetUserId
	 *           Email of user whose permissions are requested.
	 * @param permissibleAreaItemTypeCode
	 *           Code for permissible item type.
	 * @param status
	 *           Status of permission request.
	 * @param isRequested
	 *           true if permission is requested, false otherwise
	 */
	void addPermission(final String sourceUserId, final String targetUserId, final String permissibleAreaItemTypeCode,
			final PSPermissionStatus status, boolean isRequested);

	/**
	 * adds permission for given users, item and status
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested.
	 * @param typeCode
	 *           Code for permissible item type.
	 * @param status
	 *           Status of permission request.
	 * @param isRequested
	 *           true if permission is requested, false otherwise
	 */
	void addPermission(String sourceUserEmail, String targetUserEmail, List<String> typeCode, PSPermissionStatus status,
			boolean isRequested);

	/**
	 * verifies whether the accessRequestor user has permissions to access objectOwner's given item type
	 *
	 * @param accessRequestorId
	 *           Id of the user that requests for permission
	 * @param objectOwnerId
	 *           Id of the user whose permissions are requested
	 * @param permissibleAreaItemTypeCode
	 *           Code for permissible item type
	 * @return boolean value for the current user permitted to add target user
	 *
	 * @throws RelationshipDoesNotExistException
	 *            Exception which is used when relationship doesn't exist between 2 users
	 */
	boolean isPermitted(final String accessRequestorId, final String objectOwnerId, final String permissibleAreaItemTypeCode)
			throws RelationshipDoesNotExistException;

	/**
	 * updates permission for given permission ids with given status
	 *
	 * @param permissionIds
	 *           List of permission id's (List<String>)
	 * @param status
	 *           Status of permission request.
	 *
	 */
	void updatePermissionStatus(List<String> permissionIds, PSPermissionStatus status);


	/**
	 * returns permission list for given users and status
	 *
	 * @param sourceUserId
	 *           Email of user that requests for permission
	 * @param targetUserId
	 *           Email of user whose permissions are requested.
	 * @param status
	 *           List of PSPermissionStatus.
	 * @return List<PSPermissionData>
	 */
	List<PSPermissionData> getPermissionsForStatus(String sourceUserId, String targetUserId, List<PSPermissionStatus> status);

	/**
	 * returns permission list for given users ,status and type which are active
	 *
	 * @param sourceUserId
	 *           Email of user that requests for permission
	 * @param targetUserId
	 *           Email of user whose permissions are requested.
	 * @param status
	 *           Status of permission request
	 * @return List<PSPermissionData>
	 */
	List<PSPermissionData> getPermissionForActiveTypes(final String sourceUserId, final String targetUserId,
			PSPermissionStatus status);

	/**
	 * returns given or requested permissions to/from target user with provided PSPermissionStatus and active
	 * PSPermissibleAreaItemType.
	 *
	 * @param sourceUserId
	 *           Email of user that requests for permission
	 * @param targetUserId
	 *           Email of user whose permissions are requested
	 * @param status
	 *           List of PSPermissionStatus
	 * @param isGivenOrRequested
	 *           represents boolean value for the type of request
	 * @return List<PSPermissionData>
	 */
	List<PSPermissionData> getGivenOrRequestedPermissionsForTargetUser(final String sourceUserId, final String targetUserId,
			List<PSPermissionStatus> status, boolean isGivenOrRequested);

	/**
	 * approves permission for given permission ids
	 *
	 * @param permissionIds
	 *           List of permission ids
	 */
	void approvePermissionRequest(List<String> permissionIds);


	/**
	 * rejects permission for given permission ids
	 *
	 * @param permissionIds
	 *           List of permission ids
	 */
	void rejectPermissionRequest(List<String> permissionIds);

	/**
	 * revokes permission for given permission ids
	 *
	 * @param permissionIds
	 *           List of permission ids
	 */
	void revokePermissionRequest(List<String> permissionIds);

	/**
	 * Cancel permission for given permission ids
	 *
	 * @param permissionIds
	 *           List of permission ids
	 */
	void cancelPermissionRequest(List<String> permissionIds);

	/**
	 * returns permissible area item type information for given flags
	 *
	 * @param isVisibleToUser
	 *           flag is true for all permissible item types other than relationships
	 * @param isActive
	 *           boolean value for the status of the request
	 * @return List<PSPermissibleAreaData>
	 */
	List<PSPermissibleAreaData> getPermissibleItemTypes(Boolean isVisibleToUser, Boolean isActive);


	/**
	 * Update Relationship and given or requested Permissions with provided status.
	 *
	 * @param relationshipId
	 *           relationshipId
	 * @param isAcceptedOrRejected
	 *           boolean value for the status of the request
	 * @param isGivenOrRequested
	 *           boolean value for the type of the request
	 */
	void updatePendingRequest(String relationshipId, boolean isAcceptedOrRejected, boolean isGivenOrRequested);

	/**
	 * Update Relationship and given or requested Permissions with provided item type codes.
	 *
	 * @param relationshipId
	 *           service request id
	 * @param isAcceptedOrRejected
	 *           boolean value for the status of the request
	 * @param isGivenOrRequested
	 *           boolean value for the type of the request
	 * @param grantedTypeCodes
	 *           List<String> codes for granted type
	 * @param rejectedTypeCodes
	 *           List<String> codes for rejected type
	 */
	void updatePendingRequestForTypeCodes(final String relationshipId, final boolean isAcceptedOrRejected,
			final boolean isGivenOrRequested, final List<String> grantedTypeCodes, final List<String> rejectedTypeCodes);


	/**
	 * Requests additional permission to the existing permissions
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested
	 * @param typeCodes
	 *           List<String> codes for permissible type
	 * @param status
	 *           Status of permission request.
	 */
	void requestMorePermission(final String sourceUserEmail, final String targetUserEmail, final List<String> typeCodes,
			final PSPermissionStatus status);

	/**
	 * Changing existing permissions either add or remove a permission.
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested
	 * @param typeCodes
	 *           List<String> codes for permissible type
	 * @param isRequested
	 *           true if permission is requested, false otherwise
	 */
	void changePermissions(final String sourceUserEmail, final String targetUserEmail, final List<String> typeCodes,
			final boolean isRequested);

	/**
	 * Gets existing permissible areas of the user.
	 *
	 * @param permissionsForSourceToTargetUser
	 *           List of PSPermissionModel
	 * @return PSPermissibleAreaData
	 */
	List<PSPermissibleAreaData> getExistingPermissibleAreas(final List<PSPermissionModel> permissionsForSourceToTargetUser);

}
