/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.permission.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationship.data.PSPermissibleAreaData;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationshipfacades.permission.PSPermissionFacade;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * DefaultPSPermissionFacade implementation of {@link PSPermissionFacade}
 */
public class DefaultPSPermissionFacade implements PSPermissionFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSPermissionFacade.class);

	private PSPermissionService permissionService;
	private UserService userService;
	private Converter<PSPermissionModel, PSPermissionData> permissionConverter;
	private Converter<AbstractPSPermissibleAreaModel, PSPermissibleAreaData> permissibleAreaConverter;

	@Override
	public void addPermission(final String sourceUserEmail, final String targetUserEmail, final List<String> typeCode,
			final PSPermissionStatus status, final boolean isRequested) throws UnknownIdentifierException
	{
		final boolean doesSourceAndTargetUserEmailExist = StringUtils.isNotEmpty(sourceUserEmail)
				&& StringUtils.isNotEmpty(targetUserEmail);
		if (doesSourceAndTargetUserEmailExist && CollectionUtils.isNotEmpty(typeCode)
				&& getUserService().isUserExisting(sourceUserEmail) && getUserService().isUserExisting(targetUserEmail))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);
			final UserModel targetUser = getUserService().getUserForUID(targetUserEmail);
			getPermissionService().addPermission(sourceUser, targetUser, typeCode, status, isRequested);
		}
	}

	@Override
	public boolean isPermitted(final String accessRequestorId, final String objectOwnerId,
			final String permissibleAreaItemTypeCode) throws RelationshipDoesNotExistException
	{
		if (StringUtils.isNotEmpty(accessRequestorId) && StringUtils.isNotEmpty(objectOwnerId)
				&& StringUtils.isNotEmpty(permissibleAreaItemTypeCode) && getUserService().isUserExisting(accessRequestorId)
				&& (getUserService().isUserExisting(objectOwnerId)))
		{
			final UserModel accessRequestor = getUserService().getUserForUID(accessRequestorId);
			final UserModel objectOwner = getUserService().getUserForUID(objectOwnerId);

			return getPermissionService().isPermitted(accessRequestor, objectOwner, permissibleAreaItemTypeCode);
		}
		LOG.debug("given user dont have access {} from object owner {} for permissible area item type {}", accessRequestorId,
				objectOwnerId, permissibleAreaItemTypeCode);
		return false;
	}

	@Override
	public void addPermission(final String sourceUserId, final String targetUserId, final String permissibleAreaItemTypeCode,
			final PSPermissionStatus status, final boolean isRequested)
	{
		final boolean doesSourceAndTargetUserEmailExist = StringUtils.isNotEmpty(sourceUserId)
				&& StringUtils.isNotEmpty(targetUserId);
		if (doesSourceAndTargetUserEmailExist && StringUtils.isNotEmpty(permissibleAreaItemTypeCode)
				&& getUserService().isUserExisting(sourceUserId) && getUserService().isUserExisting(targetUserId))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserId);
			final UserModel targetUser = getUserService().getUserForUID(targetUserId);
			getPermissionService().addPermission(sourceUser, targetUser, permissibleAreaItemTypeCode, status, isRequested);
		}
	}

	@Override
	public void updatePermissionStatus(final List<String> permissionIds, final PSPermissionStatus status)
	{
		if (CollectionUtils.isNotEmpty(permissionIds) && status != null)
		{
			getPermissionService().updatePermissionStatus(permissionIds, status);
		}
	}

	@Override
	public List<PSPermissionData> getPermissionsForStatus(final String sourceUserId, final String targetUserId,
			final List<PSPermissionStatus> status)
	{
		if (StringUtils.isNotEmpty(sourceUserId) && StringUtils.isNotEmpty(targetUserId)
				&& getUserService().isUserExisting(sourceUserId) && (getUserService().isUserExisting(targetUserId)))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserId);
			final UserModel targetUser = getUserService().getUserForUID(targetUserId);
			final List<PSPermissionModel> permissions = getPermissionService().getPermissionsForStatus(sourceUser, targetUser,
					status);
			return getPermissionConverter().convertAll(permissions);
		}

		return Collections.emptyList();
	}

	@Override
	public List<PSPermissionData> getPermissionForActiveTypes(final String sourceUserId, final String targetUserId,
			final PSPermissionStatus status)
	{
		if (getUserService().isUserExisting(sourceUserId) && (getUserService().isUserExisting(targetUserId)))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserId);
			final UserModel targetUser = getUserService().getUserForUID(targetUserId);
			final List<PSPermissionModel> permissions = getPermissionService().getPermissionForActiveTypes(sourceUser, targetUser,
					status);
			return getPermissionConverter().convertAll(permissions);
		}
		return Collections.emptyList();
	}

	@Override
	public List<PSPermissionData> getGivenOrRequestedPermissionsForTargetUser(final String sourceUserId, final String targetUserId,
			final List<PSPermissionStatus> status, final boolean isGivenOrRequested)
	{
		if (getUserService().isUserExisting(sourceUserId) && (getUserService().isUserExisting(targetUserId)))
		{
			final UserModel sourceUser = getUserService().getUserForUID(sourceUserId);
			final UserModel targetUser = getUserService().getUserForUID(targetUserId);
			final List<PSPermissionModel> permissions = getPermissionService()
					.getGivenOrRequestedPermissionsForTargetUser(sourceUser, targetUser, status, isGivenOrRequested);
			return getPermissionConverter().convertAll(permissions);
		}
		return Collections.emptyList();
	}

	@Override
	public void approvePermissionRequest(final List<String> permissionIds)
	{
		updatePermissionStatus(permissionIds, PSPermissionStatus.ACTIVE);
	}

	@Override
	public void rejectPermissionRequest(final List<String> permissionIds)
	{
		updatePermissionStatus(permissionIds, PSPermissionStatus.REJECTED);

	}

	@Override
	public void revokePermissionRequest(final List<String> permissionIds)
	{
		updatePermissionStatus(permissionIds, PSPermissionStatus.INACTIVE);
	}

	@Override
	public void cancelPermissionRequest(final List<String> permissionIds)
	{
		updatePermissionStatus(permissionIds, PSPermissionStatus.CANCELLED);
	}



	@Override
	public List<PSPermissibleAreaData> getPermissibleItemTypes(final Boolean isVisibleToUser, final Boolean isActive)
	{
		final List<AbstractPSPermissibleAreaModel> permissibleItemTypes = getPermissionService()
				.getPermissibleItemTypes(isVisibleToUser, isActive);

		if (CollectionUtils.isNotEmpty(permissibleItemTypes))
		{
			return getPermissibleAreaConverter().convertAll(permissibleItemTypes);
		}
		return Collections.emptyList();
	}

	@Override
	public void updatePendingRequest(final String relationshipId, final boolean isAcceptedOrRejected,
			final boolean isGivenOrRequested)
	{
		getPermissionService().updatePendingRequest(relationshipId, isAcceptedOrRejected, isGivenOrRequested);
	}

	@Override
	public void updatePendingRequestForTypeCodes(final String relationshipId, final boolean isAcceptedOrRejected,
			final boolean isGivenOrRequested, final List<String> grantedTypeCodes, final List<String> rejectedTypeCodes)
	{
		getPermissionService().updatePendingRequestForTypeCodes(relationshipId, isAcceptedOrRejected, isGivenOrRequested,
				grantedTypeCodes, rejectedTypeCodes);
	}


	@Override
	public void requestMorePermission(final String sourceUserEmail, final String targetUserEmail, final List<String> typeCodes,
			final PSPermissionStatus status)
	{
		if (getUserService().isUserExisting(sourceUserEmail) && getUserService().isUserExisting(targetUserEmail))
		{
			addPermission(sourceUserEmail, targetUserEmail, typeCodes, status, true);
		}
		else
		{
			throw new UnknownIdentifierException("Source and or target user does not exist");
		}
	}

	@Override
	public void changePermissions(final String sourceUserEmail, final String targetUserEmail, final List<String> typeCodes,
			final boolean isRequested)
	{
		if (getUserService().isUserExisting(sourceUserEmail) && getUserService().isUserExisting(targetUserEmail))
		{
			getPermissionService().changePermissions(sourceUserEmail, targetUserEmail, typeCodes, isRequested);
		}
		else
		{
			throw new UnknownIdentifierException("Source and or target user does not exist");
		}
	}

	@Override
	public List<PSPermissibleAreaData> getExistingPermissibleAreas(final List<PSPermissionModel> permissionsForSourceToTargetUser)
	{
		final List<PSPermissibleAreaItemTypeModel> permissibleItemTypes = getPermissionService()
				.getPermissibleAreasForPermissions(permissionsForSourceToTargetUser);

		if (CollectionUtils.isNotEmpty(permissibleItemTypes))
		{
			return getPermissibleAreaConverter().convertAll(permissibleItemTypes);
		}
		return Collections.emptyList();
	}

	protected PSPermissionService getPermissionService()
	{
		return permissionService;
	}

	@Required
	public void setPermissionService(final PSPermissionService permissionService)
	{
		this.permissionService = permissionService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected Converter<PSPermissionModel, PSPermissionData> getPermissionConverter()
	{
		return permissionConverter;
	}

	@Required
	public void setPermissionConverter(final Converter<PSPermissionModel, PSPermissionData> permissionConverter)
	{
		this.permissionConverter = permissionConverter;
	}

	protected Converter<AbstractPSPermissibleAreaModel, PSPermissibleAreaData> getPermissibleAreaConverter()
	{
		return permissibleAreaConverter;
	}

	@Required
	public void setPermissibleAreaConverter(
			final Converter<AbstractPSPermissibleAreaModel, PSPermissibleAreaData> permissibleAreaConverter)
	{
		this.permissibleAreaConverter = permissibleAreaConverter;
	}

}
