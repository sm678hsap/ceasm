/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.relationship;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.relationship.data.PSRelationshipData;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;

import java.util.List;
import java.util.Map;


/**
 * PSRelationshipFacade interface
 */
public interface PSRelationshipFacade
{
	/**
	 * Add new relationship between source and target user with the given relationship status
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested.
	 * @param grantedTypeCodes
	 *           List of Codes for permissible type
	 * @param requestedTypeCodes
	 *           List of Codes for requested type
	 * @param titleCode
	 *           title of the requester
	 * @param firstName
	 *           first Name of the requester
	 * @param lastName
	 *           last Name of the requester
	 * @throws RelationshipAlreadyExistException
	 *            Exception which is used when relationship already exist
	 *
	 */
	void addRelationship(final String sourceUserEmail, final String targetUserEmail, final List<String> grantedTypeCodes,
			final List<String> requestedTypeCodes, String titleCode, String firstName, String lastName)
			throws RelationshipAlreadyExistException;

	/**
	 * Fetch all relations for users with status
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested.
	 * @return PSRelationshipData
	 */
	PSRelationshipData getActiveRelation(String sourceUserEmail, String targetUserEmail);

	/**
	 * Fetch all relations for users with status
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested.
	 * @return PSRelationshipData
	 */
	PSRelationshipData getPendingRelation(String sourceUserEmail, String targetUserEmail);

	/**
	 * Fetch all relations for users with status
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @param status
	 *           Status of the relationship request
	 * @return List<PSRelationshipData>
	 */
	List<PSRelationshipData> getRelationForStatus(String sourceUserEmail, PSRelationshipStatus status);

	/**
	 * Fetch relation for source email, target email and relationship status
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested.
	 * @param status
	 *           Status of the relationship request
	 * @return PSRelationshipData
	 */
	PSRelationshipData getRelation(String sourceUserEmail, String targetUserEmail, PSRelationshipStatus status);

	/**
	 * Fetch relation for id.
	 *
	 * @param relationshipId
	 *           PSRelationshipModel for the relationshipId
	 * @return PSRelationshipData
	 */
	PSRelationshipData getRelationForId(String relationshipId);

	/**
	 * Fetch relation for source email, target email and for active and pending relationship status
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested.
	 * @return PSRelationshipData
	 */
	PSRelationshipData getActiveOrPendingRelation(String sourceUserEmail, String targetUserEmail);

	/**
	 * Fetch relation for source email and relationship status
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @return List<PSRelationshipData>
	 */
	List<PSRelationshipData> getAllActiveRelationsForUser(String sourceUserEmail);

	/**
	 * Update relationship with status
	 *
	 * @param relationshipId
	 *           PSRelationshipModel for the relationshipId
	 * @param relationshipStatus
	 *           Status of the relationship request
	 */
	void updateRelation(String relationshipId, PSRelationshipStatus relationshipStatus);

	/**
	 * Approve relationship request by making status 'Active' and setting approval time
	 *
	 * @param relationshipId
	 *           PSRelationshipModel for the relationshipId
	 */
	void approveRelationRequest(String relationshipId);

	/**
	 * Reject relationship request by making status 'Rejected' and setting disapproval time
	 *
	 * @param relationshipId
	 *           PSRelationshipModel for the relationshipId
	 */
	void rejectRelationRequest(String relationshipId);

	/**
	 * returns the relations for given user and permission item type
	 *
	 * @param userId
	 *           Email of user
	 * @param itemType
	 *           type of permission
	 * @return List<PSRelationshipData>
	 */
	List<PSRelationshipData> getRelationsForUserAndPermissibleItemType(final String userId, final String itemType);

	/**
	 * returns the relationship user for given user and permission item type
	 *
	 * @param userId
	 *           Email of user
	 * @param itemType
	 *           type of permission
	 * @return List<CustomerData>
	 */
	List<CustomerData> getRelationshipUsersForUserAndPermissibleItemType(final String userId, final String itemType);

	/**
	 * Fetch relation for source email and relationship status by type(non-poa), (poa-holder), (poa-receiver)
	 *
	 * @param sourceUserId
	 *           Email of user that requests for permission.
	 * @param status
	 *           Status of the relationship request
	 * @param allPendingRequestCountMap
	 *           Map of userid & relations
	 * @return List<List<PSRelationshipData>>
	 */
	Map<String, List<PSRelationshipData>> getRelationForStatusByType(final String sourceUserId, final PSRelationshipStatus status,
			final Map<String, String> allPendingRequestCountMap);

	/**
	 * returns whether user exists
	 *
	 * @param email
	 *           Email of user
	 * @return boolean
	 */
	boolean isUserExists(String email);

	/**
	 * Fetch all relations for users with status
	 *
	 * @param userEmail
	 *           Email of user
	 * @return List<PSRelationshipData>
	 */
	List<PSRelationshipData> getPendingRequests(String userEmail);

	/**
	 * updates the cart user in context with given userId
	 *
	 * @param userId
	 *           Email of user
	 */
	void updateCartUserInContext(final String userId);

	/**
	 * gets current user in context
	 *
	 * @return CustomerData
	 */
	CustomerData getUserInContextForCurrentCart();

	/**
	 * Fetch relationships for sourceUserId, targetUserId and relationship status
	 *
	 * @param sourceUserId
	 *           Email of user that requests for permission.
	 * @param targetUserId
	 *           Email of user whose permissions are requested.
	 * @param allPendingRequestCountMap
	 *           Map of userid & relations
	 * @param status
	 *           Status of the relationship request
	 *
	 * @return List<List<PSRelationshipData>>
	 */
	Map<String, List<PSRelationshipData>> getRelationshipDataByType(String sourceUserId, String targetUserId,
			Map<String, String> allPendingRequestCountMap, PSRelationshipStatus status);

	/**
	 * gets customer data based on uid
	 *
	 * @param uid
	 *           uid of the Customer
	 *
	 * @return CustomerData
	 */
	CustomerData getCustomerDataForUid(String uid);

	/**
	 * gets customer data based on PK
	 *
	 * @param customerPK
	 *           Primary key of the customer
	 * @return CustomerData
	 */
	CustomerData getCustomerForPK(String customerPK);

	/**
	 * Fetch all relationships by category for non poa, poa holder and poa receiver
	 *
	 * @param sourceUserId
	 *           Email of user that requests for permission.
	 * @param allPendingRequestCountMap
	 *           Map of userid & relations
	 * @param userAllRelationships
	 *           List of PSRelationshipData
	 * @return List<List<PSRelationshipData>>
	 */
	Map<String, List<PSRelationshipData>> createRelationDataByType(String sourceUserId,
			List<PSRelationshipData> userAllRelationships, Map<String, String> allPendingRequestCountMap);

	/**
	 * Gets relationship address book by user
	 *
	 * @param userId
	 *           Email of user
	 * @return List<AddressData>
	 */
	List<AddressData> getRelationshipAddressBookByUser(String userId);

	/**
	 * Updates target user in relationship with given user of targetUserEmail if exists
	 *
	 * @param relationshipId
	 *           relationshipId
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested.
	 *
	 */
	void updateTargetUserInRelationship(String relationshipId, String targetUserEmail);

	/**
	 * Method to cancel pending relationship and its permissions
	 *
	 * @return PSRelationshipData
	 * @param relationshipId
	 *           relationshipId
	 */
	PSRelationshipData cancelPendingRelationship(String relationshipId);

	/**
	 * Gets List of Order for user Relationships
	 *
	 * @param userId
	 *           Email of user
	 * @return List<OrderData> of OrderData data object for user relationships order details
	 */
	List<OrderData> getOrdersForUserRelationships(String userId);

	/**
	 * Gets List of Saved Carts for user's relationships.
	 *
	 * @param userId
	 *           Email of user
	 * @return List<CartData> of CartData data object for user relationships draft details
	 */
	List<CartData> getDraftsForUserRelationships(String userId);

	/**
	 * Gets List of Addresses for user Relationships
	 *
	 * @param userId
	 *           Email of user
	 * @return List<AddressData> of AddressData data object for user relationships address details
	 */
	List<AddressData> getAddressesForUserRelationships(String userId);

	/**
	 * Fetch active and pending relationships count for source email
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission.
	 * @return Integer
	 */
	Integer getUserActiveAndPendingRelationshipsCount(String sourceUserEmail);
}
