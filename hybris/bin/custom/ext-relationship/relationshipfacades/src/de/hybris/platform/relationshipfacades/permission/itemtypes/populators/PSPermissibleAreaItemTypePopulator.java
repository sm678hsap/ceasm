/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.permission.itemtypes.populators;

import de.hybris.platform.relationship.data.PSPermissibleAreaData;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * PSPermissibleAreaItemTypePopulator class
 */
public class PSPermissibleAreaItemTypePopulator
		extends AbstractPSPermissibleAreaPopulator<AbstractPSPermissibleAreaModel, PSPermissibleAreaData>
{

	/**
	 * Adds information about ItemTypes
	 *
	 * @param source
	 * @param target
	 *
	 * @throws ConversionException
	 */
	@Override
	public void populate(final AbstractPSPermissibleAreaModel source, final PSPermissibleAreaData target)
	{
		super.addCommon(source, target);
		if (source instanceof PSPermissibleAreaItemTypeModel
				&& ((PSPermissibleAreaItemTypeModel) source).getShareableType() != null)
		{

				target.setShareableType(((PSPermissibleAreaItemTypeModel) source).getShareableType().getCode());

		}
	}
}
