/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.permission.itemtypes.populators;

import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationship.data.PSRelationshipData;
import de.hybris.platform.relationshipfacades.permission.impl.DefaultPSPermissionFacade;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;


/**
 *
 */
public class PSPermissionDataPopulator
{

	private DefaultPSPermissionFacade psPermissionFacade;

	public void setPermissionsData(final PSRelationshipData relationshipData, final String sourceUserId)
	{
		if (relationshipData.getSourceUser() != null && relationshipData.getTargetUser() != null)
		{
			final String otherUserId = relationshipData.getSourceUser().getUid().equalsIgnoreCase(sourceUserId)
					? relationshipData.getTargetUser().getUid() : relationshipData.getSourceUser().getUid();

			final List<PSPermissionData> getPermissionsAsSourceForActiveAndPendingStatuses = psPermissionFacade
					.getPermissionsForStatus(sourceUserId, otherUserId, null).stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.ACTIVE) || e.getStatus().equals(PSPermissionStatus.PENDING))
					.collect(Collectors.toList());

			final List<PSPermissionData> permissionsGivenOnRequest = getPermissionsAsSourceForActiveAndPendingStatuses.stream()
					.filter(e -> e.getIsRequested().equals(Boolean.TRUE)).collect(Collectors.toList());
			final int permissionsGivenOnRequestPendingCount = (int) permissionsGivenOnRequest.stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.PENDING)).count();
			final int permissionsGivenOnRequestActiveCount = (int) permissionsGivenOnRequest.stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.ACTIVE)).count();

			final List<PSPermissionData> permissionsGivenOnOwn = getPermissionsAsSourceForActiveAndPendingStatuses.stream()
					.filter(e -> !e.getIsRequested().equals(Boolean.TRUE)).collect(Collectors.toList());
			final int permissionsGivenOnOwnPendingCount = (int) permissionsGivenOnOwn.stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.PENDING)).count();
			final int permissionsGivenOnOwnActiveCount = (int) permissionsGivenOnOwn.stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.ACTIVE)).count();


			final List<PSPermissionData> getPermissionsAsTargetForActiveOrPendingStatus = psPermissionFacade
					.getPermissionsForStatus(otherUserId, sourceUserId, null).stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.ACTIVE) || e.getStatus().equals(PSPermissionStatus.PENDING))
					.collect(Collectors.toList());

			final List<PSPermissionData> permissionsReceivedOnRequest = getPermissionsAsTargetForActiveOrPendingStatus.stream()
					.filter(e -> e.getIsRequested().equals(Boolean.TRUE)).collect(Collectors.toList());
			final int permissionsReceivedOnRequestPendingCount = (int) permissionsReceivedOnRequest.stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.PENDING)).count();
			final int permissionsReceivedOnRequestActiveCount = (int) permissionsReceivedOnRequest.stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.ACTIVE)).count();

			final List<PSPermissionData> permissionsReceivedOnOwn = getPermissionsAsTargetForActiveOrPendingStatus.stream()
					.filter(e -> !e.getIsRequested().equals(Boolean.TRUE)).collect(Collectors.toList());
			final int permissionsReceivedOnOwnPendingCount = (int) permissionsReceivedOnOwn.stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.PENDING)).count();
			final int permissionsReceivedOnOwnActiveCount = (int) permissionsReceivedOnOwn.stream()
					.filter(e -> e.getStatus().equals(PSPermissionStatus.ACTIVE)).count();

			relationshipData.setPermissionsGivenOnRequest(permissionsGivenOnRequest);
			relationshipData.setPermissionsGivenOnOwn(permissionsGivenOnOwn);
			relationshipData.setPermissionsReceivedOnRequest(permissionsReceivedOnRequest);
			relationshipData.setPermissionsReceivedOnOwn(permissionsReceivedOnOwn);
			relationshipData.setPermissionsGivenOnRequestPendingCount(permissionsGivenOnRequestPendingCount);
			relationshipData.setPermissionsGivenOnOwnPendingCount(permissionsGivenOnOwnPendingCount);
			relationshipData.setPermissionsReceivedOnRequestPendingCount(permissionsReceivedOnRequestPendingCount);
			relationshipData.setPermissionsReceivedOnOwnPendingCount(permissionsReceivedOnOwnPendingCount);
			relationshipData.setPermissionsGivenOnRequestActiveCount(permissionsGivenOnRequestActiveCount);
			relationshipData.setPermissionsGivenOnOwnActiveCount(permissionsGivenOnOwnActiveCount);
			relationshipData.setPermissionsReceivedOnRequestActiveCount(permissionsReceivedOnRequestActiveCount);
			relationshipData.setPermissionsReceivedOnOwnActiveCount(permissionsReceivedOnOwnActiveCount);
		}
	}


	/**
	 * @return the psPermissionFacade
	 */
	protected DefaultPSPermissionFacade getPsPermissionFacade()
	{
		return psPermissionFacade;
	}

	/**
	 * @param psPermissionFacade
	 *           the psPermissionFacade to set
	 */
	@Required
	public void setPsPermissionFacade(final DefaultPSPermissionFacade psPermissionFacade)
	{
		this.psPermissionFacade = psPermissionFacade;
	}

}
