/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.permission.itemtypes.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.relationship.data.AbstractPSPermissibleAreaData;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * AbstractPSPermissibleAreaPopulator class
 */
public abstract class AbstractPSPermissibleAreaPopulator<SOURCE extends AbstractPSPermissibleAreaModel, TARGET extends AbstractPSPermissibleAreaData>
		implements Populator<SOURCE, TARGET>
{

	/**
	 * Adds basic information about ItemTypes
	 *
	 * @param source
	 * @param target
	 *
	 * @throws ConversionException
	 */
	public void addCommon(final SOURCE source, final TARGET target) throws ConversionException
	{
		target.setDisplayName(source.getName());
		target.setVisibleByUser(source.getVisibleByUser());
		target.setActive(source.isActive());
	}
}
