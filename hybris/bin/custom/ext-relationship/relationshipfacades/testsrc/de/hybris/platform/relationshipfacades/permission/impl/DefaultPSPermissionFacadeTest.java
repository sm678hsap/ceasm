/*
 * [y] hybris Platform
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.permission.impl;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationship.data.PSPermissibleAreaData;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for DefaultpsPermissionFacade
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSPermissionFacadeTest
{
	private static final String DOCUMENTS = "documents";
	private static final String DOCUMENTS_TYPE = "Documents";
	private static final String BILLS = "bills";

	private static final String TARGET_EMAIL_ID = "target@source.com";
	private static final String SOURCE_EMAIL_ID = "source@source.com";

	@InjectMocks
	DefaultPSPermissionFacade psPermissionFacade;

	@Mock
	private PSPermissionService permissionService;

	@Mock
	private UserService userService;

	@Mock
	private Converter<PSPermissionModel, PSPermissionData> permissionConverter;

	@Mock
	private Converter<AbstractPSPermissibleAreaModel, PSPermissibleAreaData> permissibleAreaConverter;

	protected static class MockPSPermissionModel extends PSPermissionModel
	{
		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(9999l);
		}
	}

	protected static class MockAddressModel extends AddressModel
	{
		private final long id;

		public MockAddressModel(final long id)
		{
			this.id = id;
		}

		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(id);
		}
	}

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		psPermissionFacade.setPermissionService(permissionService);
		psPermissionFacade.setPermissionConverter(permissionConverter);
		psPermissionFacade.setUserService(userService);
		psPermissionFacade.setPermissibleAreaConverter(permissibleAreaConverter);
	}


	@Test
	public void testAddPermissionMethod() throws UnknownIdentifierException
	{
		Mockito.doNothing().when(permissionService).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(List.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());

		Mockito.doNothing().when(permissionService).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(String.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);
		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);

		final List<String> typeCodes = new ArrayList<>();
		typeCodes.add("typeCodes");
		psPermissionFacade.addPermission(null, null, typeCodes, null, true);
		verify(permissionService, Mockito.times(0)).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(List.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());

		final List<String> typeCodesEmpty = new ArrayList<>();
		psPermissionFacade.addPermission(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodesEmpty, PSPermissionStatus.ACTIVE, true);
		verify(permissionService, Mockito.times(0)).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(List.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());

		psPermissionFacade.addPermission(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodes, null, true);
		verify(permissionService, Mockito.times(1)).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(List.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());

		psPermissionFacade.addPermission(null, null, "typeCode", null, true);
		verify(permissionService, Mockito.times(0)).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(String.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());

		psPermissionFacade.addPermission(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, "typeCode", null, true);
		verify(permissionService, Mockito.times(1)).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(String.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());

		psPermissionFacade.addPermission(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, "typeCode", PSPermissionStatus.ACTIVE, true);
		verify(permissionService, Mockito.atLeast(1)).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(String.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());

		psPermissionFacade.addPermission(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodes, PSPermissionStatus.ACTIVE, true);
		verify(permissionService, Mockito.atLeast(1)).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(List.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());
	}


	@Test
	public void testIsPermittedMethod() throws RelationshipDoesNotExistException
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.FALSE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);

		Assert.assertFalse(psPermissionFacade.isPermitted(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, DOCUMENTS_TYPE));
		Assert.assertFalse(psPermissionFacade.isPermitted(SOURCE_EMAIL_ID, "", DOCUMENTS_TYPE));
		Assert.assertFalse(psPermissionFacade.isPermitted("", TARGET_EMAIL_ID, DOCUMENTS_TYPE));
		Assert.assertFalse(psPermissionFacade.isPermitted(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, ""));
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.FALSE);
		Assert.assertFalse(psPermissionFacade.isPermitted(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, DOCUMENTS_TYPE));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito
				.when(Boolean.valueOf(
						permissionService.isPermitted(Mockito.any(UserModel.class), Mockito.any(UserModel.class), Mockito.anyString())))
				.thenReturn(Boolean.TRUE);
		Assert.assertTrue(psPermissionFacade.isPermitted(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, DOCUMENTS_TYPE));
	}

	@Test
	public void testUpdatePermissionStatusMethod()
	{
		psPermissionFacade.updatePermissionStatus(null, PSPermissionStatus.ACTIVE);
		verify(permissionService, Mockito.times(0)).updatePermissionStatus(Mockito.anyList(),
				Mockito.any(PSPermissionStatus.class));
		Mockito.doNothing().when(permissionService).updatePermissionStatus(Mockito.anyList(),
				Mockito.any(PSPermissionStatus.class));

		psPermissionFacade.updatePermissionStatus(Collections.EMPTY_LIST, PSPermissionStatus.ACTIVE);
		verify(permissionService, Mockito.times(0)).updatePermissionStatus(Mockito.anyList(),
				Mockito.any(PSPermissionStatus.class));

		psPermissionFacade.updatePermissionStatus(Collections.singletonList(SOURCE_EMAIL_ID), PSPermissionStatus.ACTIVE);
		verify(permissionService, Mockito.times(1)).updatePermissionStatus(Mockito.anyList(),
				Mockito.any(PSPermissionStatus.class));
	}

	@Test
	public void testGetPermissionsMethod()
	{

		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.ACTIVE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);
		final List<PSPermissionModel> permissions = new ArrayList<>();
		permissions.add(mock(PSPermissionModel.class));
		Mockito.when(permissionService.getPermissionsForStatus(Mockito.any(UserModel.class), Mockito.any(UserModel.class),
				Mockito.anyList())).thenReturn(permissions);
		final PSPermissionData relationshipData = new PSPermissionData();

		Mockito.when(permissionConverter.convertAll(Mockito.anyList())).thenReturn(Collections.singletonList(relationshipData));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.FALSE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.FALSE);
		Assert.assertTrue(
				CollectionUtils.isEmpty(psPermissionFacade.getPermissionsForStatus(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, status)));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Assert.assertTrue(
				CollectionUtils.isEmpty(psPermissionFacade.getPermissionsForStatus(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, status)));
		Assert.assertTrue(CollectionUtils.isEmpty(psPermissionFacade.getPermissionsForStatus("", TARGET_EMAIL_ID, status)));
		Assert.assertTrue(CollectionUtils.isEmpty(psPermissionFacade.getPermissionsForStatus(SOURCE_EMAIL_ID, "", status)));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Assert.assertNotNull(psPermissionFacade.getPermissionsForStatus(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, status));

	}

	@Test
	public void testGetPermissionsForActiveTypesMethod()
	{
		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.ACTIVE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);
		final List<PSPermissionModel> permissions = new ArrayList<>();
		permissions.add(mock(PSPermissionModel.class));
		Mockito.when(permissionService.getPermissionForActiveTypes(Mockito.any(UserModel.class), Mockito.any(UserModel.class),
				Mockito.any(PSPermissionStatus.class))).thenReturn(permissions);
		final PSPermissionData relationshipData = new PSPermissionData();

		Mockito.when(permissionConverter.convertAll(Mockito.anyList())).thenReturn(Collections.singletonList(relationshipData));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.FALSE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.FALSE);
		Assert.assertTrue(
				CollectionUtils.isEmpty(psPermissionFacade.getPermissionsForStatus(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, status)));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Assert.assertTrue(
				CollectionUtils.isEmpty(psPermissionFacade.getPermissionsForStatus(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, status)));
		Assert.assertTrue(CollectionUtils.isEmpty(psPermissionFacade.getPermissionsForStatus("", TARGET_EMAIL_ID, status)));
		Assert.assertTrue(CollectionUtils.isEmpty(psPermissionFacade.getPermissionsForStatus(SOURCE_EMAIL_ID, "", status)));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Assert.assertNotNull(psPermissionFacade.getPermissionsForStatus(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, status));
	}

	@Test
	public void testGetPermissibleItemTypes()
	{
		final List<AbstractPSPermissibleAreaModel> permissibleItemList = new ArrayList();
		permissibleItemList.add(new AbstractPSPermissibleAreaModel());
		Mockito.when(permissionService.getPermissibleItemTypes(Boolean.valueOf(Matchers.anyBoolean()),
				Boolean.valueOf(Matchers.anyBoolean()))).thenReturn(permissibleItemList);
		psPermissionFacade.getPermissibleItemTypes(Boolean.TRUE, Boolean.FALSE);
		verify(permissibleAreaConverter).convertAll(permissibleItemList);
		final List<AbstractPSPermissibleAreaModel> permissibleEmptyItemList = new ArrayList();
		Mockito.when(permissionService.getPermissibleItemTypes(Boolean.valueOf(Matchers.anyBoolean()),
				Boolean.valueOf(Matchers.anyBoolean()))).thenReturn(permissibleItemList);
		psPermissionFacade.getPermissibleItemTypes(Boolean.TRUE, Boolean.FALSE);
		verify(permissibleAreaConverter, Mockito.times(0)).convertAll(permissibleEmptyItemList);
	}


	@Test
	public void testUpdatePendingRequest()
	{
		Mockito.doNothing().when(permissionService).updatePendingRequest("r124", false, false);
		psPermissionFacade.updatePendingRequest("r124", false, false);
	}

	@Test
	public void testUpdatePendingRequestForTypeCodesWithEmptyRelationshipId()
	{
		Mockito.doNothing().when(permissionService).updatePendingRequestForTypeCodes("", false, false, Collections.EMPTY_LIST,
				Collections.EMPTY_LIST);
		psPermissionFacade.updatePendingRequestForTypeCodes("", false, false, Collections.EMPTY_LIST, Collections.EMPTY_LIST);

	}

	@Test
	public void testGetGivenOrRequestedPermissionsForTargetUserMethod()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(Mockito.anyString()))).thenReturn(Boolean.FALSE);
		Assert.assertEquals(psPermissionFacade.getGivenOrRequestedPermissionsForTargetUser(SOURCE_EMAIL_ID, TARGET_EMAIL_ID,
				Mockito.anyList(), false), Collections.emptyList());

		Mockito.when(Boolean.valueOf(userService.isUserExisting(Mockito.anyString()))).thenReturn(Boolean.TRUE);
		Mockito.when(userService.getUserForUID(Mockito.anyString())).thenReturn(mock(UserModel.class));
		final List<PSPermissionModel> permission = new ArrayList();
		permission.add(new PSPermissionModel());
		Mockito.when(
				permissionService.getGivenOrRequestedPermissionsForTargetUser(Mockito.any(), Mockito.any(), Mockito.any(), eq(false)))
				.thenReturn(permission);
		final List<PSPermissionData> permissionData = new ArrayList();
		permissionData.add(new PSPermissionData());
		Mockito.when(permissionConverter.convertAll(Mockito.any())).thenReturn(permissionData);
		final List<PSPermissionStatus> permissionStatus = new ArrayList();
		permissionStatus.add(PSPermissionStatus.PENDING);
		psPermissionFacade.getGivenOrRequestedPermissionsForTargetUser(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, permissionStatus, false);
		verify(permissionService, Mockito.times(1)).getGivenOrRequestedPermissionsForTargetUser(Mockito.any(), Mockito.any(),
				Mockito.any(), eq(false));
	}

	@Test
	public void testRequestMorePermissions()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel sourceUser = Mockito.mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(sourceUser);

		final UserModel targetUser = Mockito.mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(targetUser);

		final List<String> typeCodes = new ArrayList<>();
		typeCodes.add(DOCUMENTS);

		psPermissionFacade.requestMorePermission(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodes, PSPermissionStatus.PENDING);
		Mockito.verify(permissionService).addPermission(sourceUser, targetUser, typeCodes, PSPermissionStatus.PENDING, true);
	}

	@Test
	public void changePermissions() throws RelationshipAlreadyExistException
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = Mockito.mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = Mockito.mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);

		final List<String> typeCodes = new ArrayList<>();
		typeCodes.add(BILLS);

		final PSPermissibleAreaItemTypeModel existingPermissibleArea = Mockito.mock(PSPermissibleAreaItemTypeModel.class);
		final ComposedTypeModel composedType1 = Mockito.mock(ComposedTypeModel.class);
		composedType1.setName(DOCUMENTS);
		existingPermissibleArea.setShareableType(composedType1);

		final List<PSPermissibleAreaItemTypeModel> existingPermissibleAreas = new ArrayList<>();
		existingPermissibleAreas.add(existingPermissibleArea);

		final List<PSPermissionModel> permissionsForSourceToTargetUser = new ArrayList<>();
		permissionsForSourceToTargetUser.add(Mockito.mock(PSPermissionModel.class));

		final PSPermissibleAreaItemTypeModel changePermissibleArea = Mockito.mock(PSPermissibleAreaItemTypeModel.class);
		final ComposedTypeModel composedType2 = Mockito.mock(ComposedTypeModel.class);
		composedType2.setName(BILLS);
		changePermissibleArea.setShareableType(composedType2);

		Mockito.when(permissionService.getPermissionsForStatus(user, user2, null)).thenReturn(permissionsForSourceToTargetUser);
		Mockito.when(permissionService.getPermissibleAreasForPermissions(permissionsForSourceToTargetUser))
				.thenReturn(existingPermissibleAreas);

		final List<PSPermissibleAreaItemTypeModel> changedPermissibleAreas = new ArrayList<>();
		changedPermissibleAreas.add(changePermissibleArea);

		Mockito.when(permissionService.getPermissibleAreasForTypeCodes(typeCodes)).thenReturn(changedPermissibleAreas);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID) && userService.isUserExisting(TARGET_EMAIL_ID)))
				.thenReturn(Boolean.TRUE);

		final PSRelationshipModel relationship = Mockito.mock(PSRelationshipModel.class);
		Mockito.when(relationship.getSourceUser()).thenReturn(user);
		Mockito.when(relationship.getTargetUser()).thenReturn(user2);

		psPermissionFacade.changePermissions(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodes, true);
		Mockito.verify(permissionService).changePermissions(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodes, true);

	}

}
