/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.permission.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationship.data.PSPermissibleAreaData;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * PSPermissionPopulator unit test
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSPermissionPopulatorTest
{
	private static final String LONG_VALUE = "longValue";

	@InjectMocks
	private PSPermissionPopulator psPermissionPopulator;

	@Mock
	private Converter<UserModel, CustomerData> customerConverter;

	@Mock
	private PSPermissionModel psPermissionModel;

	@Mock
	private PSPermissionData psPermissionData;


	@Mock
	private UserModel userModel;

	@Mock
	private CustomerData customerData;

	@Mock
	private PSPermissionStatus psPermissionStatus;

	@Mock
	private AbstractPSPermissibleAreaModel abstractPSPermissibleAreaModel;

	@Mock
	private PSPermissibleAreaData psPermissibleAreaData;

	@Mock
	private Converter<AbstractPSPermissibleAreaModel, PSPermissibleAreaData> permissibleAreaItemConverter;

	@Before
	public void setUp()
	{
		psPermissionPopulator = new PSPermissionPopulator<>();
		psPermissionPopulator.setCustomerConverter(customerConverter);
		psPermissionPopulator.setPermissibleAreaItemConverter(permissibleAreaItemConverter);
	}

	@Test
	public void testPopulate()
	{
		given(psPermissionModel.getPk().getLongValueAsString()).willReturn(LONG_VALUE);
		given(psPermissionModel.getSourceUser()).willReturn(userModel);
		given(psPermissionModel.getTargetUser()).willReturn(userModel);
		given(customerConverter.convert(userModel)).willReturn(customerData);
		given(psPermissionModel.getPermissionStatus()).willReturn(psPermissionStatus);
		given(psPermissionModel.getApprovalDateTime()).willReturn(new Date());
		given(psPermissionModel.getDisApprovalDateTime()).willReturn(new Date());
		given(psPermissionModel.getPermissibleAreaItemType()).willReturn(abstractPSPermissibleAreaModel);
		given(permissibleAreaItemConverter.convert(abstractPSPermissibleAreaModel)).willReturn(psPermissibleAreaData);
		given(psPermissionModel.getIsRequested()).willReturn(Boolean.TRUE);
	}
}
