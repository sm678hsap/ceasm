/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.order.converters.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * PSRelationshipCartPopulator unit test
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSRelationshipCartPopulatorTest
{
	@InjectMocks
	private PSRelationshipCartPopulator psRelationshipCartPopulator;

	@Mock
	private Converter<UserModel, CustomerData> customerConverter;

	@Mock
	private CartModel cartModel;

	@Mock
	private CartData cartData;

	@Mock
	private UserModel userModel;

	@Mock
	private CustomerData customerData;

	@Before
	public void setUp()
	{
		psRelationshipCartPopulator = new PSRelationshipCartPopulator();
		psRelationshipCartPopulator.setCustomerConverter(customerConverter);
	}

	@Test
	public void testPopulate()
	{
		given(cartModel.getUserInContext()).willReturn(userModel);
		given(customerConverter.convert(userModel)).willReturn(customerData);
		psRelationshipCartPopulator.populate(cartModel, cartData);
	}
}
