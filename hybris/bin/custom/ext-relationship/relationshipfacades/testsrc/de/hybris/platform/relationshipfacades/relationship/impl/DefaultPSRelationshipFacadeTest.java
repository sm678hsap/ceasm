/*
 * [y] hybris Platform
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipfacades.relationship.impl;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.relationship.data.PSPermissionData;
import de.hybris.platform.relationship.data.PSRelationshipData;
import de.hybris.platform.relationshipfacades.permission.PSPermissionFacade;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.relationshipservices.relationship.data.PSRelationshipParameter;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for DefaultPSRelationshipFacade
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSRelationshipFacadeTest
{
	private static final String USER_NOT_EXISTING = "001-NotExisting";
	private static final String DOCUMENTS = "documents";
	private static final String RELATIONSHIP_ID_SAMPLE = "sample";
	private static final String RELATIONSHIP_ID = "001d";
	private static final String DOCUMENTS_TYPE = "Documents";
	private static final String BILLS = "bills";

	private static final String TARGET_EMAIL_ID = "target@source.com";
	private static final String SOURCE_EMAIL_ID = "source@source.com";

	private static final String NON_POA_RELATIONSHIP = "non-poa";
	private static final String POA_HOLDER_RELATIONSHIP = "poa-holder";
	private static final String POA_RECEIVER_RELATIONSHIP = "poa-receiver";

	@InjectMocks
	DefaultPSRelationshipFacade psRelationshipFacade;

	@Mock
	private PSRelationshipService relationshipService;

	@Mock
	private PSPermissionFacade permissionFacade;

	@Mock
	private PSPermissionService permissionService;

	@Mock
	private UserService userService;

	@Mock
	private Converter<PSRelationshipModel, PSRelationshipData> relationshipConverter;

	@Mock
	private CustomerAccountService customerAccountService;

	private AddressModel addressModel;

	private CustomerModel customerModel;

	@Mock
	private ModelService modelService;

	@Mock
	private Converter<AddressModel, AddressData> addressConverter;

	@Mock
	private Converter<OrderModel, OrderData> orderConverter;

	@Mock
	private Converter<CartModel, CartData> cartConverter;


	protected static class MockPSPermissionModel extends PSPermissionModel
	{
		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(9999l);
		}
	}

	protected static class MockAddressModel extends AddressModel
	{
		private final long id;

		public MockAddressModel(final long id)
		{
			this.id = id;
		}

		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(id);
		}
	}

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		psRelationshipFacade.setPermissionService(permissionService);
		psRelationshipFacade.setUserService(userService);
		psRelationshipFacade.setRelationshipConverter(relationshipConverter);
		psRelationshipFacade.setRelationshipService(relationshipService);
		psRelationshipFacade.setCustomerAccountService(customerAccountService);
		psRelationshipFacade.setModelService(modelService);
		psRelationshipFacade.setPermissionFacade(permissionFacade);
		psRelationshipFacade.setOrderConverter(orderConverter);
		psRelationshipFacade.setAddressConverter(addressConverter);
		psRelationshipFacade.setCartConverter(cartConverter);

	}

	public void testIfAddRelationshipForUnknownUserMethod() throws RelationshipAlreadyExistException
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(USER_NOT_EXISTING))).thenReturn(Boolean.FALSE);
		Mockito.doNothing().when(relationshipService).addRelationship(Matchers.any(PSRelationshipParameter.class));

		verify(relationshipService, Mockito.times(0)).addRelationship(Matchers.any(PSRelationshipParameter.class));
		psRelationshipFacade.addRelationship(USER_NOT_EXISTING, null, null, null, null, null, null);
	}

	@Test
	public void testAddRelationshipForExistingSourceAndTargetUserMethod()
			throws RelationshipAlreadyExistException, UnknownIdentifierException
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);

		Mockito.doNothing().when(relationshipService).addRelationship(Matchers.any(PSRelationshipParameter.class));

		final List<String> typeCodes = new ArrayList<>();
		typeCodes.add(DOCUMENTS);

		final List<String> typeCodes2 = new ArrayList<>();
		typeCodes2.add(DOCUMENTS);
		Mockito.doNothing().when(permissionService).addPermission(Matchers.any(UserModel.class), Matchers.any(UserModel.class),
				Matchers.any(List.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());
		Mockito.doNothing().when(permissionFacade).addPermission(Matchers.anyString(), Matchers.anyString(),
				Matchers.any(List.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());

		psRelationshipFacade.addRelationship(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodes, typeCodes2, StringUtils.EMPTY,
				StringUtils.EMPTY, StringUtils.EMPTY);

		verify(permissionFacade, Mockito.times(2)).addPermission(Matchers.anyString(), Matchers.anyString(),
				Matchers.any(List.class), Matchers.any(PSPermissionStatus.class), Matchers.anyBoolean());
		verify(relationshipService).addRelationship(Matchers.any(PSRelationshipParameter.class));
	}

	@Test
	public void testAddRelationshipForExistingSourceAndNotExistingTargetUserEmailMethod()
			throws RelationshipAlreadyExistException, UnknownIdentifierException
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.FALSE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);

		Mockito.doNothing().when(relationshipService).addRelationship(Matchers.any(PSRelationshipParameter.class));

		final List<String> typeCodes = new ArrayList<>();
		typeCodes.add(DOCUMENTS);

		final List<String> typeCodes2 = new ArrayList<>();
		typeCodes2.add(DOCUMENTS);

		psRelationshipFacade.addRelationship(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodes, typeCodes2, StringUtils.EMPTY,
				StringUtils.EMPTY, StringUtils.EMPTY);

		verify(relationshipService).addRelationship(Matchers.any(PSRelationshipParameter.class));
		verify(permissionService, Mockito.times(0)).addPermission(user2, user, typeCodes2, PSPermissionStatus.PENDING, false);
	}



	@Test
	public void testGetAllActiveRelationForUserMethod()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final List<PSRelationshipModel> relationshipModel = new ArrayList<>();
		final List<PSRelationshipData> relationshipData = new ArrayList<>();

		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipData);

		Assert.assertNotNull(psRelationshipFacade.getAllActiveRelationsForUser(SOURCE_EMAIL_ID));
		Assert.assertNotNull(psRelationshipFacade.getRelationForStatus(SOURCE_EMAIL_ID, PSRelationshipStatus.PENDING));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.FALSE);

		Assert.assertTrue(CollectionUtils.isEmpty(psRelationshipFacade.getAllActiveRelationsForUser(SOURCE_EMAIL_ID)));
		Assert.assertTrue(
				CollectionUtils.isEmpty(psRelationshipFacade.getRelationForStatus(SOURCE_EMAIL_ID, PSRelationshipStatus.PENDING)));

	}


	@Test
	public void testGetUserActiveAndPendingRelationshipsCountMethod()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final Integer activeAndPendingRelationshipsCount = Integer.valueOf(0);
		Mockito.when(relationshipService.getUserActiveAndPendingRelationshipsCount(Mockito.any(UserModel.class)))
				.thenReturn(activeAndPendingRelationshipsCount);
		Assert.assertNotNull(psRelationshipFacade.getUserActiveAndPendingRelationshipsCount(SOURCE_EMAIL_ID));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.FALSE);
	}

	@Test
	public void testGetActiveAndPendingRelationForUserMethods()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);

		final PSRelationshipData relationship = new PSRelationshipData();
		Mockito.when(relationshipConverter.convert(Mockito.any(PSRelationshipModel.class))).thenReturn(relationship);

		final PSRelationshipModel relationshipModel = new PSRelationshipModel();

		Mockito.when(relationshipService.getRelation(Mockito.any(UserModel.class), Mockito.any(UserModel.class),
				Mockito.any(PSRelationshipStatus.class))).thenReturn(relationshipModel);

		Assert.assertNotNull(psRelationshipFacade.getActiveRelation(SOURCE_EMAIL_ID, TARGET_EMAIL_ID));
		Assert.assertNotNull(psRelationshipFacade.getPendingRelation(SOURCE_EMAIL_ID, TARGET_EMAIL_ID));
		Assert.assertNotNull(psRelationshipFacade.getRelation(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, PSRelationshipStatus.ACTIVE));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.FALSE);

		Mockito.when(relationshipService.getRelation(Mockito.any(UserModel.class), Mockito.anyString(),
				Mockito.any(PSRelationshipStatus.class))).thenReturn(relationshipModel);
		Assert.assertNotNull(psRelationshipFacade.getPendingRelation(SOURCE_EMAIL_ID, TARGET_EMAIL_ID));

		Mockito.when(relationshipService.getRelation(Mockito.any(UserModel.class), Mockito.anyString(),
				Mockito.any(PSRelationshipStatus.class))).thenReturn(null);
		Mockito.when(relationshipConverter.convert(Mockito.any(PSRelationshipModel.class))).thenReturn(null);
		Assert.assertNull(psRelationshipFacade.getPendingRelation(SOURCE_EMAIL_ID, TARGET_EMAIL_ID));
		Mockito.when(relationshipConverter.convert(Mockito.any(PSRelationshipModel.class))).thenReturn(null);

		Assert.assertNull(psRelationshipFacade.getActiveRelation(SOURCE_EMAIL_ID, TARGET_EMAIL_ID));
		Assert.assertNull(psRelationshipFacade.getPendingRelation(SOURCE_EMAIL_ID, TARGET_EMAIL_ID));
		Assert.assertNull(psRelationshipFacade.getRelation(SOURCE_EMAIL_ID, null, PSRelationshipStatus.ACTIVE));

	}

	@Test
	public void testUpdateApproveAndRejectRelationMethods()
	{
		psRelationshipFacade.updateRelation("", null);
		psRelationshipFacade.rejectRelationRequest("");
		verify(relationshipService, Mockito.times(0)).updateRelation(Mockito.any(PSRelationshipModel.class),
				Mockito.any(PSRelationshipStatus.class));

		psRelationshipFacade.updateRelation(RELATIONSHIP_ID_SAMPLE, null);

		verify(relationshipService, Mockito.times(0)).updateRelation(Mockito.any(PSRelationshipModel.class),
				Mockito.any(PSRelationshipStatus.class));

		Mockito.when(relationshipService.getRelationForPk(Mockito.any(String.class))).thenReturn(mock(PSRelationshipModel.class));
		Mockito.doNothing().when(relationshipService).updateRelation(Mockito.any(PSRelationshipModel.class),
				Mockito.any(PSRelationshipStatus.class));

		Mockito.when(relationshipService.getRelationForPk(Mockito.any(String.class))).thenReturn(null);
		psRelationshipFacade.updateRelation(RELATIONSHIP_ID_SAMPLE, PSRelationshipStatus.ACTIVE);
		verify(relationshipService, Mockito.times(0)).updateRelation(Mockito.any(PSRelationshipModel.class),
				Mockito.any(PSRelationshipStatus.class));

		Mockito.when(relationshipService.getRelationForPk(Mockito.any(String.class))).thenReturn(mock(PSRelationshipModel.class));
		psRelationshipFacade.updateRelation(RELATIONSHIP_ID_SAMPLE, PSRelationshipStatus.ACTIVE);
		psRelationshipFacade.rejectRelationRequest(RELATIONSHIP_ID_SAMPLE);
		verify(relationshipService, Mockito.times(1)).updateRelation(Mockito.any(PSRelationshipModel.class),
				Mockito.any(PSRelationshipStatus.class));
		verify(relationshipService, Mockito.times(1)).rejectRelationRequest(Mockito.any(PSRelationshipModel.class));
	}

	@Test
	public void testApproveRelationMethod()
	{
		psRelationshipFacade.approveRelationRequest("");
		verify(relationshipService, Mockito.times(0)).approveRelationRequest(Mockito.any(PSRelationshipModel.class));

		Mockito.when(relationshipService.getRelationForPk(Mockito.any(String.class))).thenReturn(null);
		psRelationshipFacade.approveRelationRequest(RELATIONSHIP_ID);
		verify(relationshipService, Mockito.times(0)).approveRelationRequest(Mockito.any(PSRelationshipModel.class));

		Mockito.when(relationshipService.getRelationForPk(Mockito.any(String.class))).thenReturn(mock(PSRelationshipModel.class));
		Mockito.doNothing().when(relationshipService).approveRelationRequest(Mockito.any(PSRelationshipModel.class));

		psRelationshipFacade.approveRelationRequest(RELATIONSHIP_ID);
		verify(permissionService, Mockito.atLeast(1)).approveRelationRequest(Mockito.any(String.class));
	}

	@Test
	public void testGetPendingRequestForPendingRelationship()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final List<PSRelationshipModel> relationshipModel = new ArrayList<>();
		final List<PSRelationshipData> relationshipDatas = new ArrayList<>();
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipDatas);

		final List<PSRelationshipData> relationshipDatas1 = new ArrayList<>();
		final PSRelationshipData relationshipData = new PSRelationshipData();
		relationshipData.setStatus(PSRelationshipStatus.PENDING);
		final CustomerData userData = new CustomerData();
		userData.setUid(SOURCE_EMAIL_ID);
		relationshipData.setSourceUser(userData);
		relationshipDatas1.add(relationshipData);
		final PSRelationshipData relationshipData1 = new PSRelationshipData();
		relationshipData1.setStatus(PSRelationshipStatus.ACTIVE);
		final CustomerData userData1 = new CustomerData();
		userData1.setUid(SOURCE_EMAIL_ID);
		relationshipData1.setTargetUser(userData1);
		relationshipDatas1.add(relationshipData1);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipDatas1);
		Assert.assertEquals(0, psRelationshipFacade.getPendingRequests(SOURCE_EMAIL_ID).size());
	}

	@Test
	public void testGetPendingRequestForPendingRelationshipAndPermission()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);
		final List<PSRelationshipData> relationshipDatas = new ArrayList<>();
		final List<PSRelationshipModel> relationshipModel = new ArrayList<>();
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipDatas);

		final PSRelationshipData relationshipData = new PSRelationshipData();
		relationshipData.setStatus(PSRelationshipStatus.ACTIVE);
		relationshipData.setPendingGivenPermissionsToCurrentUser(true);
		final CustomerData userData2 = new CustomerData();
		userData2.setUid(RELATIONSHIP_ID);
		relationshipData.setTargetUser(userData2);
		relationshipDatas.add(relationshipData);
		final PSRelationshipData relationshipData1 = new PSRelationshipData();
		relationshipData1.setStatus(PSRelationshipStatus.PENDING);
		final CustomerData userData3 = new CustomerData();
		userData3.setUid(SOURCE_EMAIL_ID);
		relationshipData1.setTargetUser(userData3);
		relationshipDatas.add(relationshipData1);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipDatas);
		Assert.assertEquals(psRelationshipFacade.getPendingRequests(SOURCE_EMAIL_ID).size(), 2);
	}

	@Test
	public void testGetPendingRequestForPendingGivenPermission()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);
		final List<PSRelationshipData> relationshipDatas = new ArrayList<>();
		final List<PSRelationshipModel> relationshipModel = new ArrayList<>();
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipDatas);

		final PSRelationshipData relationshipData5 = new PSRelationshipData();
		relationshipData5.setStatus(PSRelationshipStatus.ACTIVE);
		relationshipData5.setPendingGivenPermissionsToCurrentUser(true);
		final CustomerData userData5 = new CustomerData();
		userData5.setUid(RELATIONSHIP_ID);
		relationshipData5.setTargetUser(userData5);
		relationshipDatas.add(relationshipData5);
		final PSRelationshipData relationshipData4 = new PSRelationshipData();
		relationshipData4.setStatus(PSRelationshipStatus.ACTIVE);
		relationshipData4.setTargetUser(userData5);
		relationshipDatas.add(relationshipData4);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipDatas);
		Assert.assertEquals(psRelationshipFacade.getPendingRequests(SOURCE_EMAIL_ID).size(), 1);
	}

	@Test
	public void testGetPendingRequestForPendingRequestPermission()
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);
		final List<PSRelationshipData> relationshipDatas = new ArrayList<>();
		final List<PSRelationshipModel> relationshipModel = new ArrayList<>();
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipDatas);

		final PSRelationshipData relationshipData6 = new PSRelationshipData();
		relationshipData6.setStatus(PSRelationshipStatus.ACTIVE);
		relationshipData6.setPendingRequestedPermissionsFromCurrentUser(true);
		final CustomerData userData5 = new CustomerData();
		relationshipData6.setTargetUser(userData5);
		relationshipDatas.add(relationshipData6);
		final PSRelationshipData relationshipData7 = new PSRelationshipData();
		relationshipData7.setStatus(PSRelationshipStatus.ACTIVE);
		relationshipData7.setTargetUser(userData5);
		relationshipDatas.add(relationshipData7);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipDatas);
		Assert.assertEquals(psRelationshipFacade.getPendingRequests(SOURCE_EMAIL_ID).size(), 1);

	}

	@Test
	public void testUpdateTargetUserInRelationshipMethodIfRelationshipIsNull()
	{
		Mockito.when(relationshipService.getRelationForPk(Mockito.anyString())).thenReturn(null);
		psRelationshipFacade.updateTargetUserInRelationship(RELATIONSHIP_ID, TARGET_EMAIL_ID);
		verify(relationshipService, Mockito.times(0)).updateTargetUserInRelationship(Mockito.any(), Mockito.any());
	}

	@Test
	public void testUpdateTargetUserInRelationshipMethodIfTargetUserIsNotExisting()
	{
		Mockito.when(relationshipService.getRelationForPk(Mockito.anyString())).thenReturn(Mockito.mock(PSRelationshipModel.class));

		Mockito.when(new Boolean(userService.isUserExisting(Mockito.anyString()))).thenReturn(Boolean.FALSE);
		psRelationshipFacade.updateTargetUserInRelationship(RELATIONSHIP_ID, TARGET_EMAIL_ID);
		verify(relationshipService, Mockito.times(0)).updateTargetUserInRelationship(Mockito.any(), Mockito.any());
	}

	@Test
	public void testUpdateTargetUserInRelationshipMethodIfTargetUserAndRelationshipExists()
	{
		Mockito.when(relationshipService.getRelationForPk(Mockito.anyString())).thenReturn(Mockito.mock(PSRelationshipModel.class));
		Mockito.when(new Boolean(userService.isUserExisting(Mockito.anyString()))).thenReturn(Boolean.TRUE);
		Mockito.when(userService.getUserForUID(Mockito.anyString())).thenReturn(Mockito.mock(UserModel.class));
		psRelationshipFacade.updateTargetUserInRelationship(RELATIONSHIP_ID, TARGET_EMAIL_ID);
		verify(relationshipService, Mockito.times(1)).updateTargetUserInRelationship(Mockito.any(), Mockito.any());
	}

	@Test
	public void testGetRelationForStatusByTypeMethodForPOAHolder()
	{
		final CustomerData sourceCustomer = new CustomerData();
		sourceCustomer.setUid(SOURCE_EMAIL_ID);
		final CustomerData targetCustomer = new CustomerData();
		targetCustomer.setUid(TARGET_EMAIL_ID);
		final Map<String, String> allPendingRequestCountMap = new HashMap<>();
		Map<String, List<PSRelationshipData>> resultMap = null;

		final List<PSRelationshipModel> relationshipModel = new ArrayList<>();
		final List<PSRelationshipData> relationshipData = new ArrayList<>();
		final PSRelationshipData psRelationshipData1 = new PSRelationshipData();
		psRelationshipData1.setIsSourcePoaHolder(Boolean.TRUE);
		psRelationshipData1.setSourceUser(sourceCustomer);
		psRelationshipData1.setTargetUser(targetCustomer);
		psRelationshipData1.setStatus(PSRelationshipStatus.PENDING);
		psRelationshipData1.setPendingRequestedPermissionsFromCurrentUser(true);
		relationshipData.add(psRelationshipData1);

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipData);
		resultMap = psRelationshipFacade.getRelationForStatusByType(sourceCustomer.getUid(), null, allPendingRequestCountMap);

		Assert.assertEquals("0", allPendingRequestCountMap.get(sourceCustomer.getUid()));
		Assert.assertTrue(!CollectionUtils.isEmpty(resultMap.get(POA_HOLDER_RELATIONSHIP)));
	}

	@Test
	public void testGetRelationForStatusByTypeMethodForPOAReceiver()
	{
		final CustomerData sourceCustomer = new CustomerData();
		sourceCustomer.setUid(SOURCE_EMAIL_ID);
		final CustomerData targetCustomer = new CustomerData();
		targetCustomer.setUid(TARGET_EMAIL_ID);
		final Map<String, String> allPendingRequestCountMap = new HashMap<>();
		Map<String, List<PSRelationshipData>> resultMap = null;

		final List<PSRelationshipModel> relationshipModel = new ArrayList<>();
		final List<PSRelationshipData> relationshipData = new ArrayList<>();
		final PSRelationshipData psRelationshipData1 = new PSRelationshipData();
		psRelationshipData1.setIsSourcePoaHolder(Boolean.TRUE);
		psRelationshipData1.setSourceUser(sourceCustomer);
		psRelationshipData1.setTargetUser(targetCustomer);
		psRelationshipData1.setStatus(PSRelationshipStatus.PENDING);
		psRelationshipData1.setPendingRequestedPermissionsFromCurrentUser(true);

		relationshipData.add(psRelationshipData1);

		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipData);

		resultMap = psRelationshipFacade.getRelationForStatusByType(targetCustomer.getUid(), null, allPendingRequestCountMap);
		Assert.assertEquals("1", allPendingRequestCountMap.get(targetCustomer.getUid()));
		Assert.assertTrue(!CollectionUtils.isEmpty(resultMap.get(POA_RECEIVER_RELATIONSHIP)));
	}

	@Test
	public void testGetRelationForStatusByTypeMethodForNonPOARelationship()
	{
		final CustomerData sourceCustomer = new CustomerData();
		sourceCustomer.setUid(SOURCE_EMAIL_ID);
		final CustomerData targetCustomer = new CustomerData();
		targetCustomer.setUid(TARGET_EMAIL_ID);
		final Map<String, String> allPendingRequestCountMap = new HashMap<>();
		Map<String, List<PSRelationshipData>> resultMap = null;

		final List<PSRelationshipModel> relationshipModel = new ArrayList<>();
		final List<PSRelationshipData> relationshipData = new ArrayList<>();
		final PSRelationshipData psRelationshipData1 = new PSRelationshipData();
		psRelationshipData1.setIsSourcePoaHolder(Boolean.TRUE);
		psRelationshipData1.setSourceUser(sourceCustomer);
		psRelationshipData1.setTargetUser(targetCustomer);
		psRelationshipData1.setStatus(PSRelationshipStatus.PENDING);
		psRelationshipData1.setPendingRequestedPermissionsFromCurrentUser(true);
		relationshipData.add(psRelationshipData1);
		psRelationshipData1.setIsSourcePoaHolder(Boolean.FALSE);
		psRelationshipData1.setIsTargetPoaHolder(Boolean.FALSE);

		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convertAll(Mockito.anyList())).thenReturn(relationshipData);

		resultMap = psRelationshipFacade.getRelationForStatusByType(targetCustomer.getUid(), null, allPendingRequestCountMap);
		Assert.assertEquals("1", allPendingRequestCountMap.get(targetCustomer.getUid()));
		Assert.assertTrue(CollectionUtils.isEmpty(resultMap.get(NON_POA_RELATIONSHIP)));
	}

	@Test
	public void testGetRelationsForGivenUserAndPermissionItemType() throws RelationshipDoesNotExistException
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(Mockito.anyString()))).thenReturn(Boolean.TRUE);
		Mockito.when(userService.getUserForUID(Mockito.anyString())).thenReturn(mock(UserModel.class));

		final List<PSRelationshipModel> relationships = new ArrayList();
		final PSRelationshipModel relationship = new PSRelationshipModel();
		relationships.add(relationship);
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationships);

		final PSRelationshipData relationshipData = new PSRelationshipData();
		final CustomerData user = new CustomerData();
		user.setUid(SOURCE_EMAIL_ID);
		final CustomerData user2 = new CustomerData();
		user2.setUid(TARGET_EMAIL_ID);
		relationshipData.setSourceUser(user);
		relationshipData.setTargetUser(user2);
		final List<PSRelationshipData> relationshipDatas = new ArrayList();
		relationshipDatas.add(relationshipData);
		Mockito.when(relationshipConverter.convertAll(relationships)).thenReturn(relationshipDatas);

		Mockito
				.when(Boolean.valueOf(
						permissionService.isPermitted(Mockito.any(UserModel.class), Mockito.any(UserModel.class), Mockito.anyString())))
				.thenReturn(Boolean.TRUE);

		Mockito.when(Boolean.valueOf(permissionFacade.isPermitted(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())))
				.thenReturn(Boolean.TRUE);

		final List<PSRelationshipData> relations = psRelationshipFacade.getRelationsForUserAndPermissibleItemType(SOURCE_EMAIL_ID,
				"itemType");
		Assert.assertEquals(relations.size(), 1);
		List<CustomerData> customers = psRelationshipFacade.getRelationshipUsersForUserAndPermissibleItemType(SOURCE_EMAIL_ID,
				"itemType");
		final CustomerData user3 = new CustomerData();
		user3.setUid(SOURCE_EMAIL_ID + ".au");
		final CustomerData user4 = new CustomerData();
		user4.setUid(TARGET_EMAIL_ID + ".au");
		relationshipData.setSourceUser(user3);
		relationshipData.setTargetUser(user2);
		relationshipDatas.add(relationshipData);
		Mockito.when(relationshipConverter.convertAll(relationships)).thenReturn(relationshipDatas);

		customers = psRelationshipFacade.getRelationshipUsersForUserAndPermissibleItemType(SOURCE_EMAIL_ID, "itemType");
		Assert.assertEquals(customers.size(), 2);
	}

	@Test
	public void testGetRelationsForGivenUserAndPermissionItemTypeForSourceEmailId() throws RelationshipDoesNotExistException
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.FALSE);
		Assert.assertEquals(psRelationshipFacade.getRelationsForUserAndPermissibleItemType(SOURCE_EMAIL_ID, "itemType"),
				Collections.emptyList());
	}

	@Test
	public void testGetRelationsForGivenUserAndPermissionItemTypeForRelationships() throws RelationshipDoesNotExistException
	{
		final List<PSRelationshipModel> relationships = new ArrayList();
		final PSRelationshipModel relationshipModel = new PSRelationshipModel();
		relationships.add(relationshipModel);
		Mockito.when(relationshipService.getRelations(Mockito.any(UserModel.class), Mockito.any(PSRelationshipStatus.class)))
				.thenReturn(relationships);

		Mockito.when(relationshipConverter.convertAll(relationships)).thenReturn(Collections.emptyList());
		Assert.assertEquals(psRelationshipFacade.getRelationsForUserAndPermissibleItemType(SOURCE_EMAIL_ID, "itemType"),
				Collections.emptyList());
	}

	@Test
	public void testGetRelationsForGivenUserAndPermissionItemTypeForEmptyCollection() throws RelationshipDoesNotExistException
	{
		Assert.assertEquals(psRelationshipFacade.getRelationsForUserAndPermissibleItemType("", ""), Collections.emptyList());
	}

	@Test
	public void testUpdateUserInContext()
	{
		psRelationshipFacade.setModelService(mock(ModelService.class));
		final CartModel cart = mock(CartModel.class);

		final CartService cartService = mock(CartService.class);
		cartService.setSessionCart(cart);
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.FALSE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		psRelationshipFacade.setCartService(cartService);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(Mockito.anyString()))).thenReturn(Boolean.FALSE);
		psRelationshipFacade.updateCartUserInContext("");
		verify(psRelationshipFacade.getModelService(), Mockito.times(0)).save(Mockito.any());
		psRelationshipFacade.updateCartUserInContext(SOURCE_EMAIL_ID);
		verify(psRelationshipFacade.getModelService(), Mockito.times(0)).save(Mockito.any());

		Mockito.when(Boolean.valueOf(userService.isUserExisting(Mockito.anyString()))).thenReturn(Boolean.TRUE);
		psRelationshipFacade.updateCartUserInContext(SOURCE_EMAIL_ID);
		verify(psRelationshipFacade.getModelService(), Mockito.times(0)).save(Mockito.any());

		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		psRelationshipFacade.updateCartUserInContext(SOURCE_EMAIL_ID);
		verify(psRelationshipFacade.getModelService(), Mockito.times(1)).save(Mockito.any());
	}

	@Test
	public void testGetRelationshipAddressBookByUser() throws RelationshipAlreadyExistException, UnknownIdentifierException
	{
		final List<AddressModel> addressModels = new ArrayList<AddressModel>();
		addressModel = new MockAddressModel(9999L);
		addressModel.setVisibleInAddressBook(Boolean.TRUE);
		addressModel.setShippingAddress(Boolean.TRUE);

		customerModel = new CustomerModel();
		customerModel.setUid(SOURCE_EMAIL_ID);
		addressModel.setOwner(customerModel);
		addressModels.add(addressModel);
		customerModel.setAddresses(addressModels);
		customerModel.setDefaultShipmentAddress(addressModel);
		userService.setCurrentUser(customerModel);

		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(customerModel);
		modelService.saveAll();
		Assert.assertNotNull(psRelationshipFacade.getRelationshipAddressBookByUser(customerModel.getUid()));
	}

	@Test
	public void testCancelPendingRelationshipMethod()
	{
		final PSRelationshipModel relationshipModel = new PSRelationshipModel();
		PSRelationshipData relationshipData = new PSRelationshipData();

		final CustomerData sourceCustomer = new CustomerData();
		sourceCustomer.setUid(SOURCE_EMAIL_ID);

		final CustomerData targetCustomer = new CustomerData();
		targetCustomer.setUid(TARGET_EMAIL_ID);


		relationshipData.setSourceUser(sourceCustomer);
		relationshipData.setTargetUser(targetCustomer);

		Mockito.when(relationshipService.getRelationForPk(RELATIONSHIP_ID)).thenReturn(relationshipModel);
		Mockito.when(relationshipConverter.convert(relationshipModel)).thenReturn(relationshipData);

		verify(relationshipService, Mockito.times(0)).updateRelation(Mockito.any(PSRelationshipModel.class),
				Mockito.any(PSRelationshipStatus.class));

		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);

		final List<PSPermissionModel> permission = new ArrayList();
		permission.add(new PSPermissionModel());
		Mockito.when(
				permissionService.getGivenOrRequestedPermissionsForTargetUser(Mockito.any(), Mockito.any(), Mockito.any(), eq(false)))
				.thenReturn(permission);
		final List<PSPermissionData> permissionData = new ArrayList();
		permissionData.add(new PSPermissionData());

		Mockito.doNothing().when(permissionService).updatePermissionStatus(Mockito.anyList(),
				Mockito.any(PSPermissionStatus.class));

		relationshipData = psRelationshipFacade.cancelPendingRelationship(RELATIONSHIP_ID);

		Assert.assertNotNull(relationshipData);
	}


	@Test
	public void testGetOrdersForUserRelationships()
	{
		final String userId = "TestUser";
		final UserModel testUser = new UserModel();
		testUser.setUid("TestUser");

		final List<OrderModel> orders = new ArrayList<>();
		orders.add(Mockito.mock(OrderModel.class));

		final List<OrderData> orderData = new ArrayList<>();
		orderData.add(Mockito.mock(OrderData.class));

		Mockito.when(userService.getUserForUID(userId)).thenReturn(testUser);
		Mockito.when(relationshipService.getOrdersForUserRelationships(testUser)).thenReturn(orders);

		Mockito.when(orderConverter.convertAll(Mockito.anyList())).thenReturn(orderData);

		psRelationshipFacade.getOrdersForUserRelationships(userId);
	}

	@Test
	public void testGetDraftsForUserRelationships()
	{
		final String userId = "TestUser";
		final UserModel testUser = new UserModel();
		testUser.setUid("TestUser");

		final List<CartModel> carts = new ArrayList<>();
		carts.add(Mockito.mock(CartModel.class));

		final List<CartData> cartData = new ArrayList<>();
		cartData.add(Mockito.mock(CartData.class));

		Mockito.when(userService.getUserForUID(userId)).thenReturn(testUser);
		Mockito.when(relationshipService.getDraftsForUserRelationships(testUser)).thenReturn(carts);

		Mockito.when(cartConverter.convertAll(Mockito.anyList())).thenReturn(cartData);

		psRelationshipFacade.getDraftsForUserRelationships(userId);
	}

	@Test
	public void testGetAddressesForUserRelationships()
	{
		final String userId = "TestUser";
		final UserModel testUser = new UserModel();
		testUser.setUid("TestUser");

		final List<AddressModel> addresses = new ArrayList<>();
		addresses.add(Mockito.mock(AddressModel.class));

		final List<AddressData> addressData = new ArrayList<>();
		addressData.add(Mockito.mock(AddressData.class));

		Mockito.when(userService.getUserForUID(userId)).thenReturn(testUser);
		Mockito.when(relationshipService.getAddressesForUserRelationships(testUser)).thenReturn(addresses);

		Mockito.when(addressConverter.convertAll(Mockito.anyList())).thenReturn(addressData);

		psRelationshipFacade.getAddressesForUserRelationships(userId);
	}

}
