/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class for PSRelationshipInterceptorTest.
 */
@UnitTest
public class PSRelationshipSourceTargetUserValidateInterceptorTest
{

   @InjectMocks
   private PSRelationshipSourceTargetUserValidateInterceptor interceptor;

   private UserModel sourceUserModel;

   private UserModel targetUserModel;

   @Before
   public void setUp()
   {
      MockitoAnnotations.initMocks(this);

      sourceUserModel = new UserModel();
      sourceUserModel.setUid("jane.citizen@stateofrosebud.com");

      targetUserModel = new UserModel();
      targetUserModel.setUid("elton.john@stateofrosebud.com");

   }

   @Test(expected = InterceptorException.class)
   public void shouldThrowExceptionWhenSameSourceTargetUser() throws InterceptorException
   {
      final InterceptorContext ctx = Mockito.mock(InterceptorContext.class);
      final PSRelationshipModel relationship = new PSRelationshipModel();
      relationship.setSourceUser(sourceUserModel);
      relationship.setTargetUser(sourceUserModel);
      interceptor.onPrepare(relationship, ctx);
   }

   @Test(expected = InterceptorException.class)
   public void shouldThrowExceptionWhenSourceTargetUserEmailsAreSame() throws InterceptorException
   {
      final InterceptorContext ctx = Mockito.mock(InterceptorContext.class);
      final PSRelationshipModel relationship = new PSRelationshipModel();
      relationship.setSourceUser(sourceUserModel);
      relationship.setTargetEmail("jane.citizen@stateofrosebud.com");
      interceptor.onPrepare(relationship, ctx);
   }

   @Test
   public void shouldNotThrowExceptionWhenTargetUserEmailIsDifferentFromSource() throws InterceptorException
   {
      final InterceptorContext ctx = Mockito.mock(InterceptorContext.class);
      final PSRelationshipModel relationship = new PSRelationshipModel();
      relationship.setSourceUser(sourceUserModel);
      relationship.setTargetEmail("elton.john@stateofrosebud.com");
      interceptor.onPrepare(relationship, ctx);
   }

   @Test
   public void shouldNotThrowExceptionWhenTargetUserIsDifferentFromSource() throws InterceptorException
   {
      final InterceptorContext ctx = Mockito.mock(InterceptorContext.class);
      final PSRelationshipModel relationship = new PSRelationshipModel();
      relationship.setSourceUser(sourceUserModel);
      relationship.setTargetUser(targetUserModel);
      interceptor.onPrepare(relationship, ctx);
   }

}

