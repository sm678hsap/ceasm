/*
 * [y] hybris Platform

 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.jalo.PSPermissibleAreaItemType;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit Testing for {@link PSDocumentManagementDaoImpl}
 */
@UnitTest
public class DefaultPSRelationshipDaoTest
{

	@Mock
	private ModelService modelService;

	@Mock
	private FlexibleSearchService flexibleSearchService;




	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@InjectMocks
	private DefaultPSRelationshipDao psRelationshipDao;

	@Before
	public void setUp() throws ImpExException
	{
		Registry.activateMasterTenant();
		MockitoAnnotations.initMocks(this);
		psRelationshipDao.setModelService(modelService);
		psRelationshipDao.setFlexibleSearchService(flexibleSearchService);
	}

	@Test
	public void testGetAddressesForUserRelationships()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(AddressModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		final List<CountryModel> deliveryCountries = new ArrayList<>();
		deliveryCountries.add(new CountryModel());

		Assert.assertNotNull(psRelationshipDao.getAddressesForUserRelationships(user, new ComposedTypeModel(), deliveryCountries));
	}

	@Test
	public void testGetOrdersForUserRelationships()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(OrderModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(psRelationshipDao.getOrdersForUserRelationships(user, new ComposedTypeModel()));
	}

	@Test
	public void testGetDraftsForUserRelationships()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(CartModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(psRelationshipDao.getDraftsForUserRelationships(user, new ComposedTypeModel()));
	}


	@Test
	public void testGetUserRelationsWithPermissionToShareableType()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(CustomerModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(psRelationshipDao.getUserRelationsWithPermissionToShareableType(user, new PSPermissibleAreaItemType(),
				PSPermissionStatus.ACTIVE));
	}

	@Test
	public void testGetPOAReceiversForUserWithStatus()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(CustomerModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(psRelationshipDao.getPOAReceiversForUserWithStatus(user, PSRelationshipStatus.ACTIVE));
	}

	@Test
	public void testGetPOAHoldersForUserWithStatus()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(CustomerModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(psRelationshipDao.getPOAHoldersForUserWithStatus(user, PSRelationshipStatus.ACTIVE));
	}

}
