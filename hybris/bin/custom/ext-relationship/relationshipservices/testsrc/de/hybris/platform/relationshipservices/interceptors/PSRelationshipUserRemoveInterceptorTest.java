/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.model.MockModelService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class for PSRelationshipUserRemoveInterceptor.
 */
@UnitTest
public class PSRelationshipUserRemoveInterceptorTest
{

	@InjectMocks
	private PSRelationshipUserRemoveInterceptor interceptor;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldNotThrowExceptionWhenUserHasNoRelations() throws InterceptorException
	{
		final InterceptorContext ctx = Mockito.mock(InterceptorContext.class);
		final UserModel user = new UserModel();
		user.setSourceRelationships(Collections.EMPTY_SET);
		user.setTargetRelationships(Collections.EMPTY_SET);
		interceptor.onRemove(user, ctx);
	}

	@Test
	public void shouldNotThrowExceptionWhenUserHasRelationsAndStausIsNotActiveOrPending() throws InterceptorException
	{
		final InterceptorContext ctx = Mockito.mock(InterceptorContext.class);
		final UserModel user = new UserModel();
		final PSRelationshipModel relation1 = new PSRelationshipModel();
		relation1.setRelationshipStatus(PSRelationshipStatus.CANCELLED);
		user.setSourceRelationships(Collections.singleton(relation1));
		user.setTargetRelationships(Collections.EMPTY_SET);
		interceptor.onRemove(user, ctx);
	}

	@Test(expected = InterceptorException.class)
	public void shouldThrowExceptionWhenUserHasActiveRelations() throws InterceptorException
	{
		final UserModel user = new UserModel();
		final PSRelationshipModel relation1 = new PSRelationshipModel();
		relation1.setRelationshipStatus(PSRelationshipStatus.ACTIVE);
		final PSRelationshipModel relation2 = new PSRelationshipModel();
		relation2.setRelationshipStatus(PSRelationshipStatus.PENDING);
		user.setSourceRelationships(Collections.singleton(relation1));
		user.setTargetRelationships(Collections.singleton(relation2));
		final InterceptorContext ctx = Mockito.mock(InterceptorContext.class);
		final ModelService model = Mockito.mock(MockModelService.class);
		Mockito.when(model.isRemoved(Mockito.any())).thenReturn(false);
		Mockito.when(ctx.getModelService()).thenReturn(model);
		Mockito.when(ctx.getElementsRegisteredFor(PersistenceOperation.DELETE)).thenReturn(Collections.EMPTY_SET);
		interceptor.onRemove(user, ctx);
	}
}
