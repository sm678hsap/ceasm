/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.permission.service.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.permission.dao.PSPermissionDao;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class for DefaultPSPermissionService.
 */
@UnitTest
public class DefaultPSPermissionServiceTest
{
	private static final String RELATIONSHIP_ID = "001d";
	private static final String TARGET_EMAIL_ID = "target@source.com";
	private static final String SOURCE_EMAIL_ID = "source@source.com";
	private static final String BILLS = "bills";
	private static final String DOCUMENTS = "documents";

	@InjectMocks
	private final DefaultPSPermissionService permissionService = new DefaultPSPermissionService();

	@Mock
	private PSPermissionDao permissionDao;

	@Mock
	private PSRelationshipService relationshipService;

	@Mock
	private TypeService typeService;

	@Mock
	private UserModel sourceUser;

	@Mock
	private UserModel targetUser;

	@Mock
	private PSPermissibleAreaItemTypeModel permissibleAreaModel;

	@Mock
	private PSRelationshipModel relationshipMock;

	@Mock
	private ComposedTypeModel composedTypeModel;

	@Mock
	private UserService userService;

	@Mock
	private EventService eventService;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		permissionService.setEventService(eventService);
	}

	@Test
	public void testIsPermittedWhenTargetUserIsNull() throws RelationshipDoesNotExistException
	{
		Assert.assertFalse(permissionService.isPermitted(null, targetUser, "PSPermissibleAreaItemTypeModel"));
	}

	@Test
	public void testIsPermittedWhenSourceUserIsNull() throws RelationshipDoesNotExistException
	{
		Assert.assertFalse(permissionService.isPermitted(sourceUser, null, "PSPermissibleAreaItemTypeModel"));
	}

	@Test
	public void testIsPermittedWhenPermissibleAreaItemIsNull() throws RelationshipDoesNotExistException
	{
		Assert.assertFalse(permissionService.isPermitted(sourceUser, targetUser, null));
	}

	@Test(expected = RelationshipDoesNotExistException.class)
	public void testIsPermittedWhenRelationshipDoesntExist() throws RelationshipDoesNotExistException
	{
		Mockito.when(relationshipService.getRelation(sourceUser, targetUser, PSRelationshipStatus.ACTIVE)).thenReturn(null);
		permissionService.isPermitted(sourceUser, targetUser, "PSPermissibleAreaItemTypeModel");
	}

	@Test
	public void testIsPermittedWhenRelationshipExist() throws RelationshipDoesNotExistException
	{
		Mockito.when(relationshipService.getRelation(sourceUser, targetUser, PSRelationshipStatus.ACTIVE))
				.thenReturn(relationshipMock);
		Mockito.when(relationshipMock.getSourceUser()).thenReturn(sourceUser);
		Mockito.when(relationshipMock.getTargetUser()).thenReturn(targetUser);
		Mockito.when(sourceUser.getUid()).thenReturn("customer@customer.com");
		Mockito.when(targetUser.getUid()).thenReturn("customer1@customer.com");
		Mockito.when(relationshipMock.getIsSourcePoaHolder()).thenReturn(false);
		Mockito.when(relationshipMock.getIsTargetPoaHolder()).thenReturn(false);
		Mockito.when(typeService.getComposedTypeForCode("PSPermissibleAreaItemTypeModel")).thenReturn(composedTypeModel);
		Mockito.when(Boolean.valueOf(permissionDao.isPermitted(sourceUser, targetUser, composedTypeModel)))
				.thenReturn(Boolean.TRUE);
		Assert.assertTrue(permissionService.isPermitted(sourceUser, targetUser, "PSPermissibleAreaItemTypeModel"));
	}

	@Test
	public void testIsPermittedWhenSourceUserIsPOAHolder() throws RelationshipDoesNotExistException
	{
		Mockito.when(relationshipService.getRelation(Mockito.any(UserModel.class), Mockito.any(UserModel.class),
				Mockito.any(PSRelationshipStatus.class))).thenReturn(relationshipMock);
		Mockito.when(relationshipMock.getSourceUser()).thenReturn(sourceUser);
		Mockito.when(relationshipMock.getTargetUser()).thenReturn(targetUser);
		Mockito.when(sourceUser.getUid()).thenReturn("customer@customer.com");
		Mockito.when(targetUser.getUid()).thenReturn("customer1@customer.com");
		Mockito.when(relationshipMock.getSourceUser()).thenReturn(sourceUser);
		Mockito.when(relationshipMock.getIsSourcePoaHolder()).thenReturn(true);

		Assert.assertTrue(permissionService.isPermitted(sourceUser, targetUser, "PSPermissibleAreaItemTypeModel"));
	}

	@Test
	public void testIsPermittedWhenTargetUserIsPOAHolder() throws RelationshipDoesNotExistException
	{
		Mockito.when(relationshipService.getRelation(Mockito.any(UserModel.class), Mockito.any(UserModel.class),
				Mockito.any(PSRelationshipStatus.class))).thenReturn(relationshipMock);
		Mockito.when(relationshipMock.getSourceUser()).thenReturn(targetUser);
		Mockito.when(relationshipMock.getTargetUser()).thenReturn(sourceUser);
		Mockito.when(sourceUser.getUid()).thenReturn("customer@customer.com");
		Mockito.when(targetUser.getUid()).thenReturn("customer1@customer.com");

		Mockito.when(relationshipMock.getIsTargetPoaHolder()).thenReturn(true);
		Assert.assertTrue(permissionService.isPermitted(sourceUser, targetUser, "PSPermissibleAreaItemTypeModel"));
	}

	@Test
	public void testAddPermission()
	{
		permissionService.addPermission(sourceUser, targetUser, permissibleAreaModel, PSPermissionStatus.ACTIVE, false);

		Mockito.verify(permissionDao).addPermission(sourceUser, targetUser, permissibleAreaModel, PSPermissionStatus.ACTIVE, false);
	}

	@Test
	public void testUpdatePermissionStatusForStatusActive()
	{
		final List<String> permissionIds = new ArrayList<>();
		permissionIds.add("addressBook");
		permissionService.updatePermissionStatus(permissionIds, PSPermissionStatus.ACTIVE);

		Mockito.verify(permissionDao).updatePermissionStatus(Matchers.eq(permissionIds), Matchers.eq(PSPermissionStatus.ACTIVE),
				Matchers.any(Date.class), Matchers.eq(null));
	}

	@Test
	public void testUpdatePermissionStatusForStatusCancelled()
	{
		final List<String> permissionIds = new ArrayList<>();
		permissionIds.add("addressBook");
		permissionService.updatePermissionStatus(permissionIds, PSPermissionStatus.CANCELLED);

		Mockito.verify(permissionDao).updatePermissionStatus(Matchers.eq(permissionIds), Matchers.eq(PSPermissionStatus.CANCELLED),
				Matchers.eq(null), Matchers.any(Date.class));
	}

	@Test
	public void testUpdatePermissionStatusForStatusInActive()
	{
		final List<String> permissionIds = new ArrayList<>();
		permissionIds.add("addressBook");
		permissionService.updatePermissionStatus(permissionIds, PSPermissionStatus.INACTIVE);

		Mockito.verify(permissionDao).updatePermissionStatus(Matchers.eq(permissionIds), Matchers.eq(PSPermissionStatus.INACTIVE),
				Matchers.eq(null), Matchers.any(Date.class));
	}

	@Test
	public void testUpdatePermissionStatusForStatusRejected()
	{
		final List<String> permissionIds = new ArrayList<>();
		permissionIds.add("addressBook");
		permissionService.updatePermissionStatus(permissionIds, PSPermissionStatus.REJECTED);

		Mockito.verify(permissionDao).updatePermissionStatus(Matchers.eq(permissionIds), Matchers.eq(PSPermissionStatus.REJECTED),
				Matchers.eq(null), Matchers.any(Date.class));
	}

	@Test
	public void testUpdatePermissionStatusForStatusPending()
	{
		final List<String> permissionIds = new ArrayList<>();
		permissionIds.add("addressBook");
		permissionService.updatePermissionStatus(permissionIds, PSPermissionStatus.PENDING);

		Mockito.verify(permissionDao).updatePermissionStatus(permissionIds, PSPermissionStatus.PENDING, null, null);
	}

	@Test
	public void testGetPermissions()
	{
		final List<PSPermissionStatus> status = new ArrayList<>();
		status.add(PSPermissionStatus.PENDING);

		permissionService.getPermissionsForStatus(sourceUser, targetUser, status);

		Mockito.verify(permissionDao).getPermissionsForUserAndStatuses(sourceUser, targetUser,
				Collections.singletonList(PSPermissionStatus.PENDING));
	}

	@Test
	public void testGetPermissionForActiveTypes()
	{
		permissionService.getPermissionForActiveTypes(sourceUser, targetUser, PSPermissionStatus.PENDING);

		Mockito.verify(permissionDao).getPermissionWithPermissibleAreaStatus(sourceUser, targetUser,
				Collections.singletonList(PSPermissionStatus.PENDING), true);
	}

	@Test
	public void testApprovePermissionRequestForEmptyIds()
	{
		permissionService.approvePermissionRequest(new ArrayList<>());

		Mockito.verifyZeroInteractions(permissionDao);
	}

	@Test
	public void testApprovePermissionRequestForSomeId()
	{
		permissionService.approvePermissionRequest(Collections.singletonList(StringUtils.EMPTY));

		Mockito.verify(permissionDao).updatePermissionStatus(Matchers.eq(Collections.singletonList(StringUtils.EMPTY)),
				Matchers.eq(PSPermissionStatus.ACTIVE), Matchers.any(Date.class), Matchers.eq(null));
	}

	@Test
	public void testRejectPermissionRequestForEmptyIds()
	{
		permissionService.rejectPermissionRequest(new ArrayList<>());

		Mockito.verifyZeroInteractions(permissionDao);
	}

	@Test
	public void testRejectPermissionRequestForSomeId()
	{
		permissionService.rejectPermissionRequest(Collections.singletonList(StringUtils.EMPTY));

		Mockito.verify(permissionDao).updatePermissionStatus(Matchers.eq(Collections.singletonList(StringUtils.EMPTY)),
				Matchers.eq(PSPermissionStatus.REJECTED), Matchers.eq(null), Matchers.any(Date.class));
	}

	@Test
	public void testRevokePermissionRequestForEmptyIds()
	{
		permissionService.revokePermissionRequest(new ArrayList<>());

		Mockito.verifyZeroInteractions(permissionDao);
	}

	@Test
	public void testRevokePermissionRequestForSomeId()
	{
		permissionService.revokePermissionRequest(Collections.singletonList(StringUtils.EMPTY));

		Mockito.verify(permissionDao).updatePermissionStatus(Matchers.eq(Collections.singletonList(StringUtils.EMPTY)),
				Matchers.eq(PSPermissionStatus.INACTIVE), Matchers.eq(null), Matchers.any(Date.class));
	}

	@Test
	public void testGetExistingPermissibleAreasForPermissions()
	{
		final List<PSPermissionModel> permissions = new ArrayList<>();
		final PSPermissionModel permission = Mockito.mock(PSPermissionModel.class);
		final PSPermissibleAreaItemTypeModel permissibleArea = Mockito.mock(PSPermissibleAreaItemTypeModel.class);

		Mockito.when(permission.getPermissibleAreaItemType()).thenReturn(permissibleArea);
		permissions.add(permission);

		Assert.assertEquals(1, permissionService.getPermissibleAreasForPermissions(permissions).size());
	}

	@Test
	public void testGetChangedPermissibleAreasForTypeCodes()
	{
		final List<String> typeCodes = new ArrayList<>();
		final List<ComposedTypeModel> composedTypes = new ArrayList();
		typeCodes.add("DOCUMENTS");
		final ComposedTypeModel type = Mockito.mock(ComposedTypeModel.class);
		composedTypes.add(type);

		Mockito.when(typeService.getComposedTypeForCode(typeCodes.get(0))).thenReturn(type);
		permissionService.getPermissibleAreasForTypeCodes(typeCodes);

		Mockito.verify(permissionDao).getPermissibleAreasForTypeAndStatus(composedTypes, true);
	}

	@Test
	public void testApproveRelationMethod()
	{
		permissionService.approveRelationRequest("");
		verify(relationshipService, Mockito.times(0)).approveRelationRequest(Mockito.any(PSRelationshipModel.class));

		Mockito.when(relationshipService.getRelationForPk(Mockito.any(String.class))).thenReturn(null);
		permissionService.approveRelationRequest(RELATIONSHIP_ID);
		verify(relationshipService, Mockito.times(0)).approveRelationRequest(Mockito.any(PSRelationshipModel.class));

		Mockito.when(relationshipService.getRelationForPk(Mockito.any(String.class))).thenReturn(mock(PSRelationshipModel.class));
		final List<PSPermissionModel> permissions = new ArrayList<>();
		final PSPermissionModel permission = Mockito.mock(PSPermissionModel.class);
		Mockito.when(permission.getPk()).thenReturn(PK.fromLong(12345678));
		permissions.add(permission);

		Mockito.when(permissionService.getPermissionsForStatus(Mockito.any(UserModel.class), Mockito.any(UserModel.class),
				Mockito.anyList())).thenReturn(permissions);

		permissionService.approveRelationRequest(RELATIONSHIP_ID);
		verify(relationshipService).approveRelationRequest(Mockito.any(PSRelationshipModel.class));
		verify(permissionDao, Mockito.atLeast(1)).updatePermissionStatus(Mockito.anyList(), Mockito.any(PSPermissionStatus.class),
				Mockito.any(Date.class), Mockito.any(Date.class));
	}

	@Test
	public void testUpdatePendingRequest()
	{
		final PSRelationshipModel relationshipModel = mock(PSRelationshipModel.class);
		Mockito.when(relationshipService.getRelationForPk(Mockito.anyString())).thenReturn(relationshipModel);
		Mockito.when(relationshipService.getRelationForPk("r124")).thenReturn(relationshipModel);
		Mockito.when(relationshipModel.getRelationshipStatus()).thenReturn(PSRelationshipStatus.PENDING);

		setupPermissionTestData(relationshipModel);

		permissionService.updatePendingRequest("r124", false, true);
		verify(permissionDao, Mockito.atLeast(1)).updatePermissionStatus(Mockito.anyList(), Mockito.any(PSPermissionStatus.class),
				Mockito.any(Date.class), Mockito.any(Date.class));

	}

	@Test
	public void testUpdatePendingRequestForTypeCodesWithPendingRelationship()
	{
		final PSRelationshipModel relationshipModel = mock(PSRelationshipModel.class);
		Mockito.when(relationshipModel.getRelationshipStatus()).thenReturn(PSRelationshipStatus.PENDING);

		final List<String> grantedTypeCodes = new ArrayList();
		final List<String> rejectedTypeCodes = new ArrayList();
		grantedTypeCodes.add("addressBook");
		rejectedTypeCodes.add("documents");

		Mockito.when(relationshipService.getRelationForPk(Mockito.anyString())).thenReturn(relationshipModel);
		Mockito.doNothing().when(relationshipService).updateRelation(Mockito.any(), Mockito.any());

		setupPermissionTestData(relationshipModel);

		permissionService.updatePendingRequestForTypeCodes("r124", false, false, grantedTypeCodes, rejectedTypeCodes);
		verify(permissionDao, Mockito.atLeast(1)).updatePermissionStatus(Mockito.anyList(), Mockito.any(PSPermissionStatus.class),
				Mockito.any(Date.class), Mockito.any(Date.class));

		permissionService.updatePendingRequestForTypeCodes("r124", false, false, grantedTypeCodes, Collections.EMPTY_LIST);
		verify(permissionDao, Mockito.atLeast(1)).updatePermissionStatus(Mockito.anyList(), Mockito.any(PSPermissionStatus.class),
				Mockito.any(Date.class), Mockito.any(Date.class));
	}

	private void setupPermissionTestData(final PSRelationshipModel relationshipModel)
	{
		final List<PSPermissionModel> permissions = new ArrayList<>();
		final ComposedTypeModel shareableType = Mockito.mock(ComposedTypeModel.class);
		final PSPermissibleAreaItemTypeModel pait = Mockito.mock(PSPermissibleAreaItemTypeModel.class);
		final PSPermissionModel permission = Mockito.mock(PSPermissionModel.class);
		Mockito.when(permission.getPk()).thenReturn(PK.fromLong(12345678));
		Mockito.when(permission.getPermissibleAreaItemType()).thenReturn(pait);
		Mockito.when(permission.getPermissionStatus()).thenReturn(PSPermissionStatus.PENDING);
		Mockito.when(pait.getShareableType()).thenReturn(shareableType);
		Mockito.when(shareableType.getCode()).thenReturn("addressBook");
		permissions.add(permission);
		Mockito.when(relationshipModel.getSourceUser()).thenReturn(sourceUser);
		Mockito.when(relationshipModel.getTargetUser()).thenReturn(targetUser);
		Mockito.when(sourceUser.getUid()).thenReturn("customer@customer.com");
		Mockito.when(targetUser.getUid()).thenReturn("customer1@customer.com");
		Mockito.when(relationshipService.getUserInContext()).thenReturn(targetUser);

		doReturn(permissions).when(permissionDao).getGivenOrRequestedPermissionsForTargetUser(sourceUser, targetUser,
				Arrays.asList(PSPermissionStatus.ACTIVE, PSPermissionStatus.PENDING), true, true);
		doReturn(permissions).when(permissionDao).getGivenOrRequestedPermissionsForTargetUser(sourceUser, targetUser,
				Arrays.asList(PSPermissionStatus.ACTIVE, PSPermissionStatus.PENDING), true, false);
	}

	@Test
	public void testChangePermissions() throws RelationshipAlreadyExistException
	{
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID))).thenReturn(Boolean.TRUE);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(TARGET_EMAIL_ID))).thenReturn(Boolean.TRUE);

		final UserModel user = Mockito.mock(UserModel.class);
		Mockito.when(userService.getUserForUID(SOURCE_EMAIL_ID)).thenReturn(user);

		final UserModel user2 = Mockito.mock(UserModel.class);
		Mockito.when(userService.getUserForUID(TARGET_EMAIL_ID)).thenReturn(user2);

		final List<String> typeCodes = new ArrayList<>();
		typeCodes.add(BILLS);

		final PSPermissibleAreaItemTypeModel existingPermissibleArea = Mockito.mock(PSPermissibleAreaItemTypeModel.class);
		final ComposedTypeModel composedType1 = Mockito.mock(ComposedTypeModel.class);
		composedType1.setName(DOCUMENTS);
		existingPermissibleArea.setShareableType(composedType1);

		final List<PSPermissibleAreaItemTypeModel> existingPermissibleAreas = new ArrayList<>();
		existingPermissibleAreas.add(existingPermissibleArea);

		final List<PSPermissionModel> permissionsForSourceToTargetUser = new ArrayList<>();
		permissionsForSourceToTargetUser.add(Mockito.mock(PSPermissionModel.class));

		final PSPermissibleAreaItemTypeModel changePermissibleArea = Mockito.mock(PSPermissibleAreaItemTypeModel.class);
		final ComposedTypeModel composedType2 = Mockito.mock(ComposedTypeModel.class);
		composedType2.setName(BILLS);
		changePermissibleArea.setShareableType(composedType2);

		Mockito.when(permissionService.getPermissionsForStatus(user, user2, null)).thenReturn(permissionsForSourceToTargetUser);

		final List<PSPermissibleAreaItemTypeModel> changedPermissibleAreas = new ArrayList<>();
		changedPermissibleAreas.add(changePermissibleArea);

		Mockito.when(permissionService.getPermissibleAreasForTypeCodes(typeCodes)).thenReturn(changedPermissibleAreas);
		Mockito.when(Boolean.valueOf(userService.isUserExisting(SOURCE_EMAIL_ID) && userService.isUserExisting(TARGET_EMAIL_ID)))
				.thenReturn(Boolean.TRUE);

		final PSRelationshipModel relationship = Mockito.mock(PSRelationshipModel.class);
		Mockito.when(relationship.getSourceUser()).thenReturn(user);
		Mockito.when(relationship.getTargetUser()).thenReturn(user2);

		permissionService.changePermissions(SOURCE_EMAIL_ID, TARGET_EMAIL_ID, typeCodes, false);
		Mockito.verify(permissionDao).addPermission(user, user2, changePermissibleArea, PSPermissionStatus.PENDING, false);
	}
}
