/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.service.impl;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.event.PSRelationshipRequestEvent;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.relationship.dao.PSRelationshipDao;
import de.hybris.platform.relationshipservices.relationship.data.PSRelationshipParameter;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for DefaultPSRelationshipService
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSRelationshipServiceTest
{
	private static final String SESSION_USER_IN_CONTEXT = "userInContext";
	private static final String SAMPLE_EMAIL = "sample-email-address";
	private static final String CUSTOMER_ID = "CUSTOMER_ID";

	List<PSRelationshipModel> existingRelationships = new ArrayList<>();

	@InjectMocks
	private final PSRelationshipService psRelationshipService = new DefaultPSRelationshipService();

	@Mock
	private PSRelationshipDao psRelationshipDao;

	@Mock
	private ModelService modelService;

	@Mock
	private EventService eventService;

	@Mock
	private UserModel sourceUser;

	@Mock
	private UserModel targetUser;

	@Mock
	private SessionService sessionService;

	@Mock
	private UserService userService;

	@Mock
	private PSRelationshipModel relationshipMock;

	@Mock
	private BaseSiteService baseSiteService;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private TypeService typeService;

	@Mock
	private CommerceCommonI18NService commerceCommonI18NService;

	@Test(expected = RelationshipAlreadyExistException.class)
	public void testAddRelatioshipWhenRelationshipExistsForRegisteredusers() throws RelationshipAlreadyExistException
	{
		existingRelationships.add(relationshipMock);
		Mockito.when(psRelationshipDao.getRelationshipForRegisteredUsersWithStatuses(sourceUser, targetUser,
				Collections.singletonList(PSRelationshipStatus.ACTIVE))).thenReturn(existingRelationships);

		psRelationshipService
				.addRelationship(createRelationshipParameter(sourceUser, targetUser, null, PSRelationshipStatus.ACTIVE));
	}

	@Test(expected = RelationshipAlreadyExistException.class)
	public void testAddRelatioshipWhenRelationshipExistsForRegisteredAndGuestUsers() throws RelationshipAlreadyExistException
	{
		final List<PSRelationshipStatus> statuses = new ArrayList<>();
		statuses.add(PSRelationshipStatus.ACTIVE);
		statuses.add(PSRelationshipStatus.PENDING);

		existingRelationships.add(relationshipMock);
		Mockito.when(psRelationshipDao.getRelationshipForRegisteredUserAndGuestUserWithStatuses(sourceUser, SAMPLE_EMAIL, statuses))
				.thenReturn(existingRelationships);

		psRelationshipService.addRelationship(createRelationshipParameter(sourceUser, null, SAMPLE_EMAIL, null));
	}

	@Test
	public void testAddRelatioshipForRegisteredUsers() throws RelationshipAlreadyExistException
	{
		Mockito.when(psRelationshipDao.getRelationshipForRegisteredUsersWithStatuses(sourceUser, targetUser,
				Collections.singletonList(PSRelationshipStatus.ACTIVE))).thenReturn(null);

		psRelationshipService
				.addRelationship(createRelationshipParameter(sourceUser, targetUser, null, PSRelationshipStatus.ACTIVE));

		verify(psRelationshipDao).createRelationshipBetweenRegisteredUsersForStatus(sourceUser, targetUser,
				PSRelationshipStatus.ACTIVE);
		verify(eventService).publishEvent(Mockito.any(PSRelationshipRequestEvent.class));
	}

	@Test
	public void testAddRelatioshipForRegisteredUsersWithStatusNull() throws RelationshipAlreadyExistException
	{
		final List<PSRelationshipStatus> statuses = new ArrayList<>();
		statuses.add(PSRelationshipStatus.ACTIVE);
		statuses.add(PSRelationshipStatus.PENDING);
		Mockito.when(psRelationshipDao.getRelationshipForRegisteredUsersWithStatuses(sourceUser, targetUser, statuses))
				.thenReturn(null);

		psRelationshipService.addRelationship(createRelationshipParameter(sourceUser, targetUser, null, null));

		verify(psRelationshipDao).createRelationshipBetweenRegisteredUsersForStatus(sourceUser, targetUser,
				PSRelationshipStatus.PENDING);
		verify(eventService).publishEvent(Mockito.any(PSRelationshipRequestEvent.class));
	}

	@Test
	public void testAddRelatioshipForRegisteredAndGuestUsers() throws RelationshipAlreadyExistException
	{
		Mockito.when(psRelationshipDao.getRelationshipForRegisteredUserAndGuestUserWithStatuses(sourceUser, SAMPLE_EMAIL,
				Collections.singletonList(PSRelationshipStatus.ACTIVE))).thenReturn(null);

		psRelationshipService
				.addRelationship(createRelationshipParameter(sourceUser, null, SAMPLE_EMAIL, PSRelationshipStatus.ACTIVE));

		verify(psRelationshipDao).createRelationshipBetweenRegisteredUserAndGuestUserForStatus(sourceUser, SAMPLE_EMAIL,
				PSRelationshipStatus.ACTIVE);
		verify(eventService).publishEvent(Mockito.any(PSRelationshipRequestEvent.class));
	}

	private PSRelationshipParameter createRelationshipParameter(final UserModel sourceUser2, final UserModel targetUser2,
			final String targetEmail, final PSRelationshipStatus relationshipStatus)
	{
		final PSRelationshipParameter relationshipParameter = new PSRelationshipParameter();
		relationshipParameter.setRelationshipStatus(relationshipStatus);
		relationshipParameter.setSourceUser(sourceUser2);
		relationshipParameter.setTargetUser(targetUser2);
		relationshipParameter.setTargetEmail(targetEmail);
		return relationshipParameter;
	}

	@Test
	public void testGetRelations()
	{
		Mockito.when(psRelationshipDao.getRelations(sourceUser, PSRelationshipStatus.PENDING)).thenReturn(null);

		Assert.assertNull(psRelationshipService.getRelations(sourceUser, PSRelationshipStatus.PENDING));
		verify(psRelationshipDao).getRelations(sourceUser, PSRelationshipStatus.PENDING);
	}

	@Test
	public void testGetUserActiveAndPendingRelationshipsCount()
	{
		Mockito.when(psRelationshipDao.getUserActiveAndPendingRelationshipsCount(sourceUser)).thenReturn(null);

		Assert.assertNull(psRelationshipService.getUserActiveAndPendingRelationshipsCount(sourceUser));
		verify(psRelationshipDao).getUserActiveAndPendingRelationshipsCount(sourceUser);
	}


	@Test
	public void testGetRelation()
	{
		Mockito.when(psRelationshipDao.getRelation(sourceUser, targetUser, PSRelationshipStatus.PENDING)).thenReturn(null);

		Assert.assertNull(psRelationshipService.getRelation(sourceUser, targetUser, PSRelationshipStatus.PENDING));
		verify(psRelationshipDao).getRelation(sourceUser, targetUser, PSRelationshipStatus.PENDING);
	}

	@Test
	public void testUpdateRelation()
	{
		psRelationshipService.updateRelation(relationshipMock, PSRelationshipStatus.PENDING);
		verify(psRelationshipDao).updateRelation(relationshipMock, PSRelationshipStatus.PENDING);
	}

	@Test
	public void testApproveRelationRequest()
	{
		psRelationshipService.approveRelationRequest(relationshipMock);
		verify(psRelationshipDao).approveRelationRequest(relationshipMock);
	}

	@Test
	public void testRejectRelationRequest()
	{
		psRelationshipService.rejectRelationRequest(relationshipMock);
		verify(psRelationshipDao).rejectRelationRequest(relationshipMock);
	}


	@Test
	public void testUpdateRelationshipTargetUser()
	{
		final PSRelationshipModel relationship = new PSRelationshipModel();
		final UserModel user = new UserModel();
		psRelationshipService.updateTargetUserInRelationship(relationship, user);
		verify(modelService).save(relationship);
	}

	@Test
	public void testGetUserInContextIsSignedInUser()
	{
		doReturn(null).when(sessionService).getAttribute(eq(SESSION_USER_IN_CONTEXT));

		psRelationshipService.getUserInContext();
		verify(userService, times(1)).getCurrentUser();
		verify(userService, times(0)).getUserForUID(anyString());
	}

	@Test
	public void testGetUserInContextIsNotSignedInUser()
	{
		doReturn(CUSTOMER_ID).when(sessionService).getAttribute(eq(SESSION_USER_IN_CONTEXT));

		psRelationshipService.getUserInContext();
		verify(userService, times(0)).getCurrentUser();
		verify(userService, times(1)).getUserForUID(anyString());
	}

	@Test
	public void testGetOrdersForUserRelationships()
	{

		final List<OrderModel> orders = new ArrayList<>();
		Mockito.when(psRelationshipDao.getOrdersForUserRelationships(sourceUser, new ComposedTypeModel())).thenReturn(orders);
		Mockito.when(typeService.getComposedTypeForCode(PSServiceProductModel._TYPECODE)).thenReturn(new ComposedTypeModel());

		Assert.assertNotNull(psRelationshipService.getOrdersForUserRelationships(sourceUser));
	}

	@Test
	public void testGetDraftsForUserRelationships()
	{

		final List<CartModel> carts = new ArrayList<>();
		Mockito.when(psRelationshipDao.getDraftsForUserRelationships(sourceUser, new ComposedTypeModel())).thenReturn(carts);
		Mockito.when(typeService.getComposedTypeForCode(PSServiceProductModel._TYPECODE)).thenReturn(new ComposedTypeModel());

		Assert.assertNotNull(psRelationshipService.getDraftsForUserRelationships(sourceUser));
	}

	@Test
	public void testGetAddressesForUserRelationships()
	{

		final List<CountryModel> countries = new ArrayList<>();

		final List<AddressModel> addresses = new ArrayList<>();
		Mockito.when(commerceCommonI18NService.getAllCountries()).thenReturn(countries);
		Mockito.when(psRelationshipDao.getAddressesForUserRelationships(sourceUser, new ComposedTypeModel(), countries))
				.thenReturn(addresses);
		Mockito.when(typeService.getComposedTypeForCode(PSServiceProductModel._TYPECODE)).thenReturn(new ComposedTypeModel());

		Assert.assertNotNull(psRelationshipService.getAddressesForUserRelationships(sourceUser));
	}
}
