package de.hybris.platform.relationshipservices.user;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.mockito.Mockito.when;


/**
 * Junit class for get minor user state
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSGetMinorUserStateHandlerTest
{

	private static String MINOR = "MINOR";
	private static String MAJOR = "MAJOR";
	private static String CANBETRANSFERED = "CANBETRANSFERED";

	@InjectMocks
	private PSGetMinorUserState minorUserStateHandler;

	@Mock
	private ModelService modelService;

	@Mock
	private UserModel user;


	@Test
	public void testGetMinorStateByEmptyDOBAndNoMinorUser()
	{
		when(user.getMinorState()).thenReturn(MAJOR);
	}

	@Test
	public void testGetMinorStateByEmptyDOBAndMinorUser()
	{
		user.setIsMinor(true);
		modelService.save(user);
		when(user.getMinorState()).thenReturn(MINOR);
		Assert.assertTrue(user.getMinorState().equalsIgnoreCase(MINOR));
	}

	@Test
	public void testGetMinorStateByGivenDOBUnder18()
	{
		Date dob = new DateTime(2017, 1, 1, 0, 0).toDate();
		user.setDob(dob);
		modelService.save(user);
		when(user.getMinorState()).thenReturn(MINOR);
		Assert.assertTrue(user.getMinorState().equalsIgnoreCase(MINOR));

	}

	@Test
	public void testGetMinorStateByGivenDOBOver18()
	{
		Date dob = new DateTime(1980, 1, 1, 0, 0).toDate();
		user.setDob(dob);
		modelService.save(user);
		when(user.getMinorState()).thenReturn(MAJOR);
		Assert.assertTrue(user.getMinorState().equalsIgnoreCase(MAJOR));
	}

	@Test
	public void testGetMinorStateByGivenDOBOver18ByUserType()
	{
		Date dob = new DateTime(1980, 1, 1, 0, 0).toDate();
		user.setDob(dob);
		if (user instanceof CustomerModel)
		{
			((CustomerModel) user).setType(CustomerType.UNIDENTIFIED);

		}
		modelService.save(user);
		when(user.getMinorState()).thenReturn(CANBETRANSFERED);
		Assert.assertTrue(user.getMinorState().equalsIgnoreCase(CANBETRANSFERED));
	}
}
