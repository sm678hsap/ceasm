/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.relationshipservices.event;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.relationshipservices.model.PSRelationshipProcessModel;
import de.hybris.platform.relationshipservices.relationship.data.PSRelationshipParameter;
import de.hybris.platform.servicelayer.model.ModelService;


/**
 * The class of RelationshipRequestEmailEventListenerTest.
 *
 */
@UnitTest
public class RelationshipRequestEmailEventListenerTest
{

	@InjectMocks
	private PSRelationshipRequestEventListener relRequestListener;

	@Mock
	private ModelService modelService;

	@Mock
	private BusinessProcessService businessProcessService;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		relRequestListener.setBusinessProcessService(businessProcessService);
		relRequestListener.setModelService(modelService);
	}

	@Test
	public void testOnEvent()
	{
		final PSRelationshipProcessModel psRelationshipRequest = Mockito.mock(PSRelationshipProcessModel.class);

		final PSRelationshipRequestEvent relRequest = Mockito.mock(PSRelationshipRequestEvent.class);
		final PSRelationshipParameter relParam = Mockito.mock(PSRelationshipParameter.class);

		Mockito.when(relRequest.getRelationshipParameter()).thenReturn(Mockito.mock(PSRelationshipParameter.class));
		Mockito.when(businessProcessService.createProcess(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Mockito.mock(PSRelationshipProcessModel.class));

		Mockito.doNothing().when(modelService).save(psRelationshipRequest);
		Mockito.doNothing().when(businessProcessService).startProcess(psRelationshipRequest);

		relRequestListener.onEvent(relRequest);

		Mockito.verify(modelService).save(Mockito.any(PSRelationshipProcessModel.class));

	}

}
