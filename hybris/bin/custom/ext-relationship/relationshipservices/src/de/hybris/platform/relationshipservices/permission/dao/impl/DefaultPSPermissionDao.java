/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.permission.dao.impl;

import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.permission.dao.PSPermissionDao;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;


/**
 * Implementation of PSPermissionDao
 */
public class DefaultPSPermissionDao extends AbstractItemDao implements PSPermissionDao
{
	private static final String SELECT_PERMISSIONS = "SELECT {" + PSPermissionModel.PK + "} FROM {" + PSPermissionModel._TYPECODE
			+ "}";
	private static final String WHERE_PERMISSION_STATUS_IN = " AND {" + PSPermissionModel.PERMISSIONSTATUS
			+ "} in ( ?itemPermissionStatus )";
	private static final String WHERE_PERMISSION_ID_IN = " WHERE {" + PSPermissionModel.PK + "} in ( ?itemPK )";

	private static final String SELECT_PERMISSIBLE_ITEM = "SELECT {" + PSPermissibleAreaItemTypeModel.PK + "} FROM {"
			+ PSPermissibleAreaItemTypeModel._TYPECODE + "}";

	private static final String SELECT_ABSTRACT_PERMISSIBLE_ITEM = "SELECT {" + AbstractPSPermissibleAreaModel.PK + "} FROM {"
			+ PSPermissibleAreaItemTypeModel._TYPECODE + "}";

	private static final String WHERE_PERMISSIBLE_ITEM_ACTIVE_OR_INACTIVE = " WHERE {" + AbstractPSPermissibleAreaModel.ACTIVE
			+ "} = ?active";
	private static final String WHERE_PERMISSIBLE_ITEM_VISIBLE_TO_USER = " WHERE {" + AbstractPSPermissibleAreaModel.VISIBLEBYUSER
			+ "} = ?visibleToUser";
	private static final String WHERE_PERMISSIBLE_ITEM_TYPE = " WHERE {" + PSPermissibleAreaItemTypeModel.SHAREABLETYPE
			+ "} = ?itemType";
	private static final String WHERE_PERMISSIBLE_ITEM_TYPES_IN = " WHERE {" + PSPermissibleAreaItemTypeModel.SHAREABLETYPE
			+ "} IN ( ?itemType )";
	private static final String AND_ITEM_TYPE = " AND {" + PSPermissibleAreaItemTypeModel.SHAREABLETYPE + "} =?itemType";

	private static final String AND_ITEM_ACTIVE_STATUS = " AND {" + AbstractPSPermissibleAreaModel.ACTIVE + "} =?active";

	private static final String SUB_SELECT_FOR_ACTIVE_PERMISSIBLE_ITEM_TYPE = " AND {" + PSPermissionModel.PERMISSIBLEAREAITEMTYPE
			+ "} IN ( {{" + SELECT_PERMISSIBLE_ITEM + WHERE_PERMISSIBLE_ITEM_ACTIVE_OR_INACTIVE + "}})";

	private static final String SUB_SELECT_FOR_PERMISSIBLE_ITEM_TYPE_AND_STATUSES = " AND {"
			+ PSPermissionModel.PERMISSIBLEAREAITEMTYPE + "} IN ( {{" + SELECT_PERMISSIBLE_ITEM
			+ WHERE_PERMISSIBLE_ITEM_ACTIVE_OR_INACTIVE + AND_ITEM_TYPE + "}})";

	private static final String FIND_PERMISSION_FOR_USER = SELECT_PERMISSIONS + " WHERE {" + PSPermissionModel.SOURCEUSER
			+ "} = ?sourceUser AND {" + PSPermissionModel.TARGETUSER + "} = ?targetUser";

	private static final String FIND_GIVEN_OR_REQUESTED_PERMISSION_FOR_TARGET_USER = SELECT_PERMISSIONS + " WHERE {"
			+ PSPermissionModel.SOURCEUSER + "} = ?sourceUser AND {" + PSPermissionModel.TARGETUSER + "} = ?targetUser AND {"
			+ PSPermissionModel.ISREQUESTED + "} = ?isRequested";

	private static final String TARGET_USER = "targetUser";
	private static final String SOURCE_USER = "sourceUser";
	private static final String STATUS_CANT_BE_NULL = "Status cannot be null";
	private static final String SOURCE_USER_CANNOT_BE_NULL = "Source user cannot be null";
	private static final String TARGET_USER_CANNOT_BE_NULL = "Target user cannot be null";
	private static final String COMPOSED_TYPE_CANNOT_BE_NULL = "Composed type cannot be null";
	private static final String ITEM_PERMISSION_STATUS = "itemPermissionStatus";
	private static final String ACTIVE = "active";
	private static final String IS_REQUESTED = "isRequested";
	private static final String ITEM_TYPE = "itemType";

	@Override
	public List<PSPermissionModel> getPermissionsForUserAndStatuses(final UserModel sourceUser, final UserModel targetUser,
			final List<PSPermissionStatus> status)
	{
		ServicesUtil.validateParameterNotNull(sourceUser, SOURCE_USER_CANNOT_BE_NULL);
		ServicesUtil.validateParameterNotNull(targetUser, TARGET_USER_CANNOT_BE_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(SOURCE_USER, sourceUser);
		params.put(TARGET_USER, targetUser);
		final FlexibleSearchQuery query;
		if (CollectionUtils.isEmpty(status))
		{
			query = new FlexibleSearchQuery(FIND_PERMISSION_FOR_USER);
			query.addQueryParameters(params);
		}
		else
		{
			params.put(ITEM_PERMISSION_STATUS, status);
			query = new FlexibleSearchQuery(FIND_PERMISSION_FOR_USER + WHERE_PERMISSION_STATUS_IN);
			query.addQueryParameters(params);
		}

		final SearchResult<PSPermissionModel> permissions = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(permissions.getResult()) ? permissions.getResult() : null;
	}

	@Override
	public List<PSPermissionModel> getPermissionsForType(final UserModel sourceUser, final UserModel targetUser,
			final List<PSPermissionStatus> statuses, final ComposedTypeModel composedType, final boolean active)
	{
		ServicesUtil.validateParameterNotNull(sourceUser, SOURCE_USER_CANNOT_BE_NULL);
		ServicesUtil.validateParameterNotNull(targetUser, TARGET_USER_CANNOT_BE_NULL);
		ServicesUtil.validateParameterNotNull(composedType, COMPOSED_TYPE_CANNOT_BE_NULL);

		String searchQuery;
		final Map<String, Object> params = new HashMap<>();
		params.put(SOURCE_USER, sourceUser);
		params.put(TARGET_USER, targetUser);
		params.put(ITEM_TYPE, composedType);
		params.put(ACTIVE, Boolean.valueOf(active));

		if (CollectionUtils.isNotEmpty(statuses))
		{
			params.put(ITEM_PERMISSION_STATUS, statuses);
			searchQuery = FIND_PERMISSION_FOR_USER + WHERE_PERMISSION_STATUS_IN + SUB_SELECT_FOR_PERMISSIBLE_ITEM_TYPE_AND_STATUSES;
		}
		else
		{
			searchQuery = FIND_PERMISSION_FOR_USER + SUB_SELECT_FOR_PERMISSIBLE_ITEM_TYPE_AND_STATUSES;
		}

		final FlexibleSearchQuery query = new FlexibleSearchQuery(searchQuery);
		query.addQueryParameters(params);
		final SearchResult<PSPermissionModel> permissions = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(permissions.getResult()) ? permissions.getResult() : null;
	}

	@Override
	public List<PSPermissibleAreaItemTypeModel> getPermissibleAreasForTypeAndStatus(final ComposedTypeModel composedType,
			final boolean active)
	{
		ServicesUtil.validateParameterNotNull(composedType, COMPOSED_TYPE_CANNOT_BE_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(ITEM_TYPE, composedType);
		params.put(ACTIVE, Boolean.valueOf(active));

		final FlexibleSearchQuery query = new FlexibleSearchQuery(
				SELECT_PERMISSIBLE_ITEM + WHERE_PERMISSIBLE_ITEM_TYPE + AND_ITEM_ACTIVE_STATUS);
		query.addQueryParameters(params);

		final SearchResult<PSPermissibleAreaItemTypeModel> permissibleAreas = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(permissibleAreas.getResult()) ? permissibleAreas.getResult() : null;
	}

	@Override
	public List<PSPermissibleAreaItemTypeModel> getPermissibleAreasForTypeAndStatus(final List<ComposedTypeModel> composedTypes,
			final boolean active)
	{
		ServicesUtil.validateParameterNotNull(CollectionUtils.isNotEmpty(composedTypes) ? composedTypes : null,
				"Composed Type cannot be null");

		final Map<String, Object> params = new HashMap<>();
		params.put(ITEM_TYPE, composedTypes);
		final FlexibleSearchQuery query;
		params.put(ACTIVE, Boolean.valueOf(active));

		query = new FlexibleSearchQuery(SELECT_PERMISSIBLE_ITEM + WHERE_PERMISSIBLE_ITEM_TYPES_IN + AND_ITEM_ACTIVE_STATUS);
		query.addQueryParameters(params);

		final SearchResult<PSPermissibleAreaItemTypeModel> permissibleAreaItemTypes = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(permissibleAreaItemTypes.getResult()) ? permissibleAreaItemTypes.getResult() : null;
	}

	@Override
	public PSPermissionModel addPermission(final UserModel sourceUser, final UserModel targetUser,
			final PSPermissibleAreaItemTypeModel permissibleArea, final PSPermissionStatus status, final boolean isRequested)
	{
		final PSPermissionModel permission = getModelService().create(PSPermissionModel.class);
		permission.setSourceUser(sourceUser);
		permission.setTargetUser(targetUser);
		permission.setPermissibleAreaItemType(permissibleArea);
		permission.setPermissionStatus(status);
		permission.setIsRequested(Boolean.valueOf(isRequested));
		getModelService().save(permission);
		return permission;
	}

	@Override
	public List<PSPermissionModel> addPermissions(final UserModel sourceUser, final UserModel targetUser,
			final List<PSPermissibleAreaItemTypeModel> permissibleAreas, final PSPermissionStatus status, final boolean isRequested)
	{
		final List<PSPermissionModel> permissions = new ArrayList();
		permissibleAreas.forEach(item -> {
			permissions.add(addPermission(sourceUser, targetUser, item, status, isRequested));
		});
		return permissions;
	}

	@Override
	public List<PSPermissionModel> updatePermissionStatus(final List<String> permissionPk, final PSPermissionStatus status,
			final Date approvalDate, final Date disapprovalDate)
	{
		ServicesUtil.validateParameterNotNull(status, STATUS_CANT_BE_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put("itemPK", permissionPk);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SELECT_PERMISSIONS + WHERE_PERMISSION_ID_IN);
		query.addQueryParameters(params);

		final SearchResult<PSPermissionModel> permissions = getFlexibleSearchService().search(query);
		if (CollectionUtils.isNotEmpty(permissions.getResult()))
		{
			permissions.getResult().forEach(permissionItem -> {

				permissionItem.setPermissionStatus(status);
				if (approvalDate != null)
				{
					permissionItem.setApprovalDateTime(approvalDate);
				}
				if (disapprovalDate != null)
				{
					permissionItem.setDisApprovalDateTime(disapprovalDate);
				}
			});
			getModelService().saveAll(permissions.getResult());
		}
		return permissions.getResult();
	}

	@Override
	public boolean isPermitted(final UserModel accessRequestor, final UserModel objectOwner, final ComposedTypeModel composedType)
	{
		final List<PSPermissionModel> permissions = getPermissionsForType(objectOwner, accessRequestor,
				Collections.singletonList(PSPermissionStatus.ACTIVE), composedType, true);
		return CollectionUtils.isNotEmpty(permissions);
	}

	@Override
	public List<PSPermissionModel> getPermissionWithPermissibleAreaStatus(final UserModel sourceUser, final UserModel targetUser,
			final List<PSPermissionStatus> statuses, final boolean active)
	{
		ServicesUtil.validateParameterNotNull(sourceUser, SOURCE_USER_CANNOT_BE_NULL);
		ServicesUtil.validateParameterNotNull(targetUser, TARGET_USER_CANNOT_BE_NULL);

		String searchQuery;
		final Map<String, Object> params = new HashMap<>();
		params.put(SOURCE_USER, sourceUser);
		params.put(TARGET_USER, targetUser);
		params.put(ACTIVE, Boolean.valueOf(active));

		if (statuses != null)
		{
			params.put(ITEM_PERMISSION_STATUS, statuses);
			searchQuery = FIND_PERMISSION_FOR_USER + WHERE_PERMISSION_STATUS_IN + SUB_SELECT_FOR_ACTIVE_PERMISSIBLE_ITEM_TYPE;
		}
		else
		{
			searchQuery = FIND_PERMISSION_FOR_USER + SUB_SELECT_FOR_ACTIVE_PERMISSIBLE_ITEM_TYPE;
		}

		final FlexibleSearchQuery query = new FlexibleSearchQuery(searchQuery);
		query.addQueryParameters(params);
		final SearchResult<PSPermissionModel> permissions = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(permissions.getResult()) ? permissions.getResult() : null;
	}

	@Override
	public void updatePermissionStatus(final List<PSPermissionModel> permissions, final PSPermissionStatus status)
	{
		ServicesUtil.validateParameterNotNull(permissions, "permissions cannot be null");
		ServicesUtil.validateParameterNotNull(status, STATUS_CANT_BE_NULL);

		permissions.forEach(item -> item.setPermissionStatus(status));
		getModelService().saveAll(permissions);
	}

	@Override
	public List<AbstractPSPermissibleAreaModel> getPermissibleAreaItems(final Boolean isVisibleToUser, final Boolean isActive)
	{
		String searchQuery;
		final Map<String, Object> params = new HashMap<>();

		searchQuery = SELECT_ABSTRACT_PERMISSIBLE_ITEM;
		if (isVisibleToUser != null && isActive != null)
		{
			params.put("visibleToUser", isVisibleToUser);
			params.put(ACTIVE, isActive);
			searchQuery = searchQuery + WHERE_PERMISSIBLE_ITEM_VISIBLE_TO_USER + AND_ITEM_ACTIVE_STATUS;
		}
		else if (isVisibleToUser != null)
		{
			params.put("visibleToUser", isVisibleToUser);
			searchQuery = searchQuery + WHERE_PERMISSIBLE_ITEM_VISIBLE_TO_USER;
		}
		else
		{
			params.put(ACTIVE, isActive);
			searchQuery = searchQuery + WHERE_PERMISSIBLE_ITEM_ACTIVE_OR_INACTIVE;
		}

		final FlexibleSearchQuery query = new FlexibleSearchQuery(searchQuery);
		query.addQueryParameters(params);
		final SearchResult<AbstractPSPermissibleAreaModel> permissions = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(permissions.getResult()) ? permissions.getResult() : null;

	}

	@Override
	public List<PSPermissionModel> getGivenOrRequestedPermissionsForTargetUser(final UserModel sourceUser,
			final UserModel targetUser, final List<PSPermissionStatus> statuses, final boolean active,
			final boolean isGivenOrRequested)
	{
		ServicesUtil.validateParameterNotNull(sourceUser, SOURCE_USER_CANNOT_BE_NULL);
		ServicesUtil.validateParameterNotNull(targetUser, TARGET_USER_CANNOT_BE_NULL);

		String searchQuery;
		final Map<String, Object> params = new HashMap<>();
		if (isGivenOrRequested)
		{
			params.put(SOURCE_USER, sourceUser);
			params.put(TARGET_USER, targetUser);
			params.put(IS_REQUESTED, Boolean.FALSE);
		}
		else
		{
			params.put(SOURCE_USER, targetUser);
			params.put(TARGET_USER, sourceUser);
			params.put(IS_REQUESTED, Boolean.TRUE);
		}
		params.put(ACTIVE, Boolean.valueOf(active));

		if (statuses != null)
		{
			params.put(ITEM_PERMISSION_STATUS, statuses);
			searchQuery = FIND_GIVEN_OR_REQUESTED_PERMISSION_FOR_TARGET_USER + WHERE_PERMISSION_STATUS_IN
					+ SUB_SELECT_FOR_ACTIVE_PERMISSIBLE_ITEM_TYPE;
		}
		else
		{
			searchQuery = FIND_GIVEN_OR_REQUESTED_PERMISSION_FOR_TARGET_USER + SUB_SELECT_FOR_ACTIVE_PERMISSIBLE_ITEM_TYPE;
		}

		final FlexibleSearchQuery query = new FlexibleSearchQuery(searchQuery);
		query.addQueryParameters(params);
		final SearchResult<PSPermissionModel> permissions = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(permissions.getResult()) ? permissions.getResult() : null;
	}
}
