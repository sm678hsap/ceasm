/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.constants;

/**
 * Global class for all Relationshipservices constants. You can add global constants for your extension into this class.
 */
public final class RelationshipservicesConstants extends GeneratedRelationshipservicesConstants
{
	public static final String EXTENSIONNAME = "relationshipservices";

	private RelationshipservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public interface BusinessProcess //NOSONAR
	{
		String RELATIONSHIP_REQUEST_EMAIL_PROCESS = "relationshipRequestEmailProcess";
	}
}
