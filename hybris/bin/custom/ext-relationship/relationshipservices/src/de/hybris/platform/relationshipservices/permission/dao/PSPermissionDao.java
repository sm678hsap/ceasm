/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */

package de.hybris.platform.relationshipservices.permission.dao;

import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;

import java.util.Date;
import java.util.List;


/**
 * PSPermissionDao Interface.
 */
public interface PSPermissionDao
{

	/**
	 * Returns list of permission for given user and status
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param statuses
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> getPermissionsForUserAndStatuses(UserModel sourceUser, UserModel targetUser,
			List<PSPermissionStatus> statuses);

	/**
	 * Returns list of permission for type and statuses
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param statuses
	 * @param composedType
	 * @param active
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> getPermissionsForType(UserModel sourceUser, UserModel targetUser, List<PSPermissionStatus> statuses,
			ComposedTypeModel composedType, boolean active);

	/**
	 * Returns list of permission for type and statuses
	 *
	 * @param composedType
	 * @param active
	 * @return List<PSPermissibleAreaItemTypeModel>
	 */
	List<PSPermissibleAreaItemTypeModel> getPermissibleAreasForTypeAndStatus(ComposedTypeModel composedType, boolean active);

	/**
	 * add permission for type and statuses
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param status
	 * @param permissibleArea
	 * @param isRequested
	 * @return PSPermissionModel
	 *
	 */
	PSPermissionModel addPermission(UserModel sourceUser, UserModel targetUser, PSPermissibleAreaItemTypeModel permissibleArea,
			PSPermissionStatus status, boolean isRequested);

	/**
	 * updates list of permission with given status and dates
	 *
	 * @param permissionPk
	 * @param status
	 * @param approvalDate
	 * @param disApprovalDate
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> updatePermissionStatus(List<String> permissionPk, PSPermissionStatus status, Date approvalDate,
			Date disApprovalDate);

	/**
	 * Returns whether user is permitted to access given users item type
	 *
	 * @param accessRequestor
	 * @param objectOwner
	 * @param composedTypeForClass
	 * @return boolean
	 */
	boolean isPermitted(UserModel accessRequestor, UserModel objectOwner, ComposedTypeModel composedTypeForClass);

	/**
	 * Returns list of permission for type and statuses
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param statuses
	 * @param active
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> getPermissionWithPermissibleAreaStatus(UserModel sourceUser, UserModel targetUser,
			List<PSPermissionStatus> statuses, boolean active);

	/**
	 * Returns list of permissions for target user where permissions has been given or requested by some other user.
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param statuses
	 * @param active
	 * @param isGivenOrRequested
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> getGivenOrRequestedPermissionsForTargetUser(UserModel sourceUser, UserModel targetUser,
			List<PSPermissionStatus> statuses, boolean active, boolean isGivenOrRequested);

	/**
	 * Returns list of permissible items for type and statuses
	 *
	 * @param typeCodes
	 * @param active
	 * @return List<PSPermissibleAreaItemTypeModel>
	 */
	List<PSPermissibleAreaItemTypeModel> getPermissibleAreasForTypeAndStatus(List<ComposedTypeModel> typeCodes, boolean active);

	/**
	 * adds permission for given users ,item and status
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param permissibleAreas
	 * @param status
	 * @param isRequested
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> addPermissions(UserModel sourceUser, UserModel targetUser,
			List<PSPermissibleAreaItemTypeModel> permissibleAreas, PSPermissionStatus status, boolean isRequested);

	/**
	 * updates permission status for given users ,item and status
	 *
	 * @param permissions
	 * @param status
	 */
	void updatePermissionStatus(List<PSPermissionModel> permissions, PSPermissionStatus status);

	/**
	 * returns all permissible area item types for given active and shareable flag.
	 *
	 * @param isShareableByUser
	 * @param isActive
	 * @return List<AbstractPSPermissibleAreaModel>
	 */
	List<AbstractPSPermissibleAreaModel> getPermissibleAreaItems(Boolean isShareableByUser, Boolean isActive);

}

