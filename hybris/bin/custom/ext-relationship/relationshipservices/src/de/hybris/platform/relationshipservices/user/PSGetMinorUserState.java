/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.user;





import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSMinorUserType;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.LocalDate;
import org.joda.time.Years;


/**
 * Get minor user state.
 */

public class PSGetMinorUserState implements DynamicAttributeHandler<String, UserModel>
{

	@Override
	public String get(final UserModel user)
	{
		if (user.getDob() == null)
		{
			if (user.getIsMinor())
			{
				return PSMinorUserType.MINOR.getCode();
			}
			else
			{
				return PSMinorUserType.MAJOR.getCode();
			}
		}
		final long age = getAge(user.getDob());

		if (age < 18)
		{
			return PSMinorUserType.MINOR.getCode();
		}

		if (age >= 18 && user instanceof CustomerModel)
		{
			if ("Unidentified".equalsIgnoreCase(((CustomerModel) user).getType().getCode()))
			{
				return PSMinorUserType.CANBETRANSFERED.getCode();
			}

			if ("Registered".equalsIgnoreCase(((CustomerModel) user).getType().getCode()))
			{
				return PSMinorUserType.MAJOR.getCode();
			}
		}
		return PSMinorUserType.MAJOR.getCode();
	}

	/**
	 * Get user's age as of current date
	 *
	 * @param dob
	 *           Date of birth
	 * @return age
	 */
	private long getAge(final Date dob)
	{
		final Calendar cDob = Calendar.getInstance();
		cDob.setTime(dob);

		final LocalDate birthdate = new LocalDate(cDob.get(Calendar.YEAR), cDob.get(Calendar.MONTH) + 1,
				cDob.get(Calendar.DAY_OF_MONTH));
		final LocalDate now = new LocalDate();
		final Years age = Years.yearsBetween(birthdate, now);
		return age.getYears();
	}

	@Override
	public void set(final UserModel user, final String state)
	{
		// not required as it's read only
	}

}
