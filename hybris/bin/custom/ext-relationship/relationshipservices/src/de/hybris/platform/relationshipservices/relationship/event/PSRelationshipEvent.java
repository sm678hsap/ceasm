/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.event;

import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * PSRelationshipEvent Event.
 */
public class PSRelationshipEvent extends AbstractEvent implements ClusterAwareEvent
{
	@Override
	public boolean publish(final int sourceNodeId, final int targetNodeId)
	{
		return sourceNodeId == targetNodeId;
	}

	private PSRelationshipModel relationship;

	public PSRelationshipModel getRelationship()
	{
		return relationship;
	}

	public void setRelationship(final PSRelationshipModel relationship)
	{
		this.relationship = relationship;
	}
}
