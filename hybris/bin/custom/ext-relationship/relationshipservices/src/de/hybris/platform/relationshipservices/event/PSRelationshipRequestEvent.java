/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.event;

import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.relationshipservices.relationship.data.PSRelationshipParameter;


/**
 * RelationshipRequestEvent Event.
 */
public class PSRelationshipRequestEvent extends AbstractCommerceUserEvent
{
	private PSRelationshipParameter relationshipParameter;

	protected PSRelationshipParameter getRelationshipParameter()
	{
		return relationshipParameter;
	}

	public void setRelationshipParameter(final PSRelationshipParameter relationshipParameter)
	{
		this.relationshipParameter = relationshipParameter;
	}
}
