/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.interceptors;

import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;


/**
 * Interceptor to validate source and target users are not same in a relationship
 */

public class PSRelationshipSourceTargetUserValidateInterceptor implements PrepareInterceptor<PSRelationshipModel>
{

   @Override
   public void onPrepare(final PSRelationshipModel relationship, final InterceptorContext ctx) throws InterceptorException
   {
      if (isSourceUserEqualsTargetUser(relationship))
      {
         throw new InterceptorException("Source User cannot be same as Target User in a relationship");
      }
   }

   public boolean isSourceUserEqualsTargetUser(final PSRelationshipModel relationship)
   {
      if (relationship.getTargetEmail() != null)
      {
         return relationship.getSourceUser().getUid().equals(relationship.getTargetEmail());
      }
      else if (relationship.getTargetUser() != null)
      {
         return relationship.getSourceUser().getUid().equals(relationship.getTargetUser().getUid());
      }

      return false;
   }
}
