/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.permission.event;

import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.List;


/**
 * PSPermissionEvent Event.
 */
public class PSPermissionEvent extends AbstractEvent implements ClusterAwareEvent
{
	private List<PSPermissionModel> permissions;

	@Override
	public boolean publish(final int sourceNodeId, final int targetNodeId)
	{
		return sourceNodeId == targetNodeId;
	}

	public void setPermissions(final List<PSPermissionModel> permissions)
	{
		this.permissions = permissions;
	}

	public List<PSPermissionModel> getPermissions()
	{
		return permissions;
	}
}
