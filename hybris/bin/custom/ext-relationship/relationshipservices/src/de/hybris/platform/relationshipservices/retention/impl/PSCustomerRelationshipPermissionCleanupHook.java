package de.hybris.platform.relationshipservices.retention.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

/**
 * Class used to remove the relationship and permissions from a customer
 */
public class PSCustomerRelationshipPermissionCleanupHook extends PSRelationshipAbstractItemCleanupHook<CustomerModel>
{
    private static final Logger LOG = LoggerFactory.getLogger(PSCustomerRelationshipPermissionCleanupHook.class);

    private static final String PERMISSION_QUERY = "SELECT DISTINCT {p.pk}, {p.itemtype} FROM "
            + "{ Customer AS c JOIN PSPermission AS p ON ({c.pk} = {p.sourceUser} OR {c.pk} = {p.targetUser})} "
            + "WHERE {c.pk} = ?USER_PK";

    @Override
    public void cleanupRelatedObjects(final CustomerModel customerModel)
    {

        validateParameterNotNullStandardMessage("customerModel", customerModel);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Cleaning up relationships and permission for Customer : {}", customerModel);
        }

        // find, retrieve and remove permission records
        final FlexibleSearchQuery permissionQuery = new FlexibleSearchQuery(PERMISSION_QUERY);
        permissionQuery.addQueryParameter("USER_PK", customerModel.getPk());
        final SearchResult<PSPermissionModel> permissionSearchResult = getFlexibleSearchService().search(permissionQuery);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Found {} PSPermissionModel to be cleaned up", permissionSearchResult.getCount());
        }

        for(final PSPermissionModel permissionModel : permissionSearchResult.getResult())
        {
            getModelService().remove(permissionModel);
            this.removeAuditRecords(permissionModel);
        }


        if (LOG.isDebugEnabled())
        {
            LOG.debug("Found {} Source PSRelationshipModel to be cleaned up", customerModel.getSourceRelationships() != null  ? customerModel.getSourceRelationships().size() : 0 );
        }

        for(final PSRelationshipModel relationshipModel : customerModel.getSourceRelationships())
        {
            getModelService().remove(relationshipModel);
            this.removeAuditRecords(relationshipModel);
        }

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Found {} Target PSRelationshipModel to be cleaned up", customerModel.getTargetRelationships() != null  ? customerModel.getTargetRelationships().size() : 0 );
        }

        for(final PSRelationshipModel relationshipModel : customerModel.getTargetRelationships())
        {
            getModelService().remove(relationshipModel);
            this.removeAuditRecords(relationshipModel);
        }

    }

}