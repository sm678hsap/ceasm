/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.permission.service;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;

import java.util.List;


/**
 * PSPermissionService interface for Permission related functionality
 */
public interface PSPermissionService
{

	/**
	 * verifies whether the accessRequestor user has permissions to access objectOwner's given item type
	 *
	 * @param accessRequestor
	 *           UserModel that requests to access the Permissible Area Item
	 * @param objectOwner
	 *           UserModel who owns the Permissible Area Item
	 * @param permissibleAreaModel
	 *           String representation of permissibleArea item
	 * @throws RelationshipDoesNotExistException
	 *            which is thrown when relationship doesn't exist between 2 users
	 *
	 * @return boolean, true if permitted
	 */
	boolean isPermitted(final UserModel accessRequestor, final UserModel objectOwner, final String permissibleAreaModel)
			throws RelationshipDoesNotExistException;

	/**
	 * adds permission for given users ,item and status
	 *
	 * @param sourceUser
	 *           UserModel that requests for permission
	 * @param targetUser
	 *           UserModel whose permissions are requested
	 * @param permissibleArea
	 *           Generated model class for type PSPermissibleAreaItemType that Stores all permissions of permissible items
	 *           shared between source and target
	 * @param isRequested
	 *           true if permission is requested, false otherwise
	 * @param status
	 *           Status of permission request
	 */
	void addPermission(final UserModel sourceUser, final UserModel targetUser,
			final PSPermissibleAreaItemTypeModel permissibleArea, final PSPermissionStatus status, boolean isRequested);

	/**
	 * updates permission for given permission ids with given status
	 *
	 * @param permissionIds
	 *           List of permission ids
	 * @param status
	 *           Status of permission request
	 */
	void updatePermissionStatus(List<String> permissionIds, PSPermissionStatus status);

	/**
	 * returns permission list for given users and status
	 *
	 * @param sourceUser
	 *           UserModel that requests for permission
	 * @param targetUser
	 *           UserModel whose permissions are requested
	 * @param status
	 *           List of PSPermissionStatus
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> getPermissionsForStatus(UserModel sourceUser, UserModel targetUser, List<PSPermissionStatus> status);

	/**
	 * returns permission list for given users, status and type which are active
	 *
	 * @param sourceUser
	 *           UserModel that requests for permission
	 * @param targetUser
	 *           UserModel whose permissions are requested
	 * @param status
	 *           Status of permission request
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> getPermissionForActiveTypes(final UserModel sourceUser, final UserModel targetUser,
			PSPermissionStatus status);

	/**
	 * returns given or requested permissions to/from target user with provided PSPermissionStatus and active
	 * PSPermissibleAreaItemType.
	 *
	 * @param sourceUser
	 *           UserModel that requests for permission
	 * @param targetUser
	 *           UserModel whose permissions are requested
	 * @param status
	 *           List of PSPermissionStatus
	 * @param isGivenOrRequested
	 *           represents boolean value for the type of request
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> getGivenOrRequestedPermissionsForTargetUser(final UserModel sourceUser, final UserModel targetUser,
			List<PSPermissionStatus> status, final boolean isGivenOrRequested);

	/**
	 * approves permission for given permission ids
	 *
	 * @param permissionIds
	 *           List of permission ids
	 */
	void approvePermissionRequest(List<String> permissionIds);


	/**
	 * rejects permission for given permission ids
	 *
	 * @param permissionIds
	 *           List of permission ids
	 */
	void rejectPermissionRequest(List<String> permissionIds);

	/**
	 * revokes permission for given permission ids
	 *
	 * @param permissionIds
	 *           List of permission ids
	 */
	void revokePermissionRequest(List<String> permissionIds);

	/**
	 * adds permission for given users ,item and status
	 *
	 * @param sourceUser
	 *           UserModel that requests for permission
	 * @param targetUser
	 *           UserModel whose permissions are requested
	 * @param permissibleAreaCode
	 *           Code for permissibleAreaItemType
	 * @param status
	 *           Status of permission request
	 * @param isRequested
	 *           true if permission is requested, false otherwise
	 */
	void addPermission(UserModel sourceUser, UserModel targetUser, String permissibleAreaCode, PSPermissionStatus status,
			boolean isRequested);

	/**
	 * adds permission for given users ,item and status
	 *
	 * @param sourceUser
	 *           UserModel that requests for permission
	 * @param targetUser
	 *           UserModel whose permissions are requested
	 * @param types
	 *           List of PSPermissionStatus
	 * @param status
	 *           Status of permission request
	 * @param isRequested
	 *           true if permission is requested, false otherwise
	 */
	void addPermission(UserModel sourceUser, UserModel targetUser, List<String> types, PSPermissionStatus status,
			boolean isRequested);

	/**
	 * updates permission status for given users ,item and status
	 *
	 * @param permissions
	 *           List of PSPermissionModel
	 * @param status
	 *           Status of permission request
	 */
	void updatePermissionsStatus(List<PSPermissionModel> permissions, PSPermissionStatus status);

	/**
	 * get Permissible area item type for given status and sharable type
	 *
	 * @param isVisibleToUser
	 *           boolean value for shared resource visible to user
	 * @param isActive
	 *           boolean value for the status of the request
	 * @return List<AbstractPSPermissibleAreaModel>
	 */
	List<AbstractPSPermissibleAreaModel> getPermissibleItemTypes(Boolean isVisibleToUser, Boolean isActive);

	/**
	 * Gets existing permissible area types of each permission.
	 *
	 * @param permissionsForSourceToTargetUser
	 *           List<PSPermissionModel> which lists all permissions of permissible items shared between source and target
	 * @return existingPermissibleAreas List<PSPermissibleAreaItemTypeModel>
	 */
	List<PSPermissibleAreaItemTypeModel> getPermissibleAreasForPermissions(
			final List<PSPermissionModel> permissionsForSourceToTargetUser);

	/**
	 * Gets the permissible areas of the typeCodes.
	 *
	 * @param typeCodes
	 *           List<String> Code for permissible item type
	 * @return permissibleAreas List<PSPermissibleAreaItemTypeModel>
	 */
	List<PSPermissibleAreaItemTypeModel> getPermissibleAreasForTypeCodes(final List<String> typeCodes);

	/**
	 * Update Relationship and given or requested Permissions with provided status.
	 *
	 * @param relationshipId
	 *           relationship id
	 * @param isAcceptedOrRejected
	 *           boolean value for the status of the request
	 * @param isGivenOrRequested
	 *           represents boolean value for the type of request
	 */
	void updatePendingRequest(final String relationshipId, final boolean isAcceptedOrRejected, final boolean isGivenOrRequested);

	/**
	 * Update Relationship and given or requested Permissions with provided item type codes.
	 *
	 * @param relationshipId
	 *           relationship id
	 * @param isAcceptedOrRejected
	 *           boolean value for the status of the request
	 * @param isGivenOrRequested
	 *           boolean value for the type of the request
	 * @param grantedTypeCodes
	 *           List<String> codes for granted item type
	 * @param rejectedTypeCodes
	 *           List<String> codes for granted item type
	 */
	void updatePendingRequestForTypeCodes(final String relationshipId, final boolean isAcceptedOrRejected,
			final boolean isGivenOrRequested, final List<String> grantedTypeCodes, final List<String> rejectedTypeCodes);

	/**
	 * Changing existing permissions either add or remove a permission.
	 *
	 * @param sourceUserEmail
	 *           Email of user that requests for permission
	 * @param targetUserEmail
	 *           Email of user whose permissions are requested
	 * @param typeCodes
	 *           List<String> Code for permissible item type
	 * @param isRequested
	 *           true if permission is requested, false otherwise
	 */
	void changePermissions(final String sourceUserEmail, final String targetUserEmail, final List<String> typeCodes,
			final boolean isRequested);

	/**
	 * Retrieves given or requested permissions for current user in a relationship.
	 *
	 * @param relationship
	 *           Model object for relationship details
	 * @param isGivenOrRequested
	 *           boolean value for the type of the request
	 * @return List<PSPermissionModel>
	 */
	List<PSPermissionModel> getGivenOrRequestedPermissionsForCurrentUser(final PSRelationshipModel relationship,
			final boolean isGivenOrRequested);

	/**
	 * Approve relationship request by making status 'Active' and setting approval time
	 *
	 * @param relationshipId
	 *           relationship id
	 */
	void approveRelationRequest(final String relationshipId);
}
