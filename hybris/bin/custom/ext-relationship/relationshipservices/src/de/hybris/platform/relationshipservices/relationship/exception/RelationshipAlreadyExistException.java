/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.exception;

/**
 * Exception which is used when relationship already exist.
 */
public class RelationshipAlreadyExistException extends Exception
{
	public RelationshipAlreadyExistException(final String message)
	{
		super(message);
	}

	public RelationshipAlreadyExistException(final Throwable cause)
	{
		super(cause);
	}

	public RelationshipAlreadyExistException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

}
