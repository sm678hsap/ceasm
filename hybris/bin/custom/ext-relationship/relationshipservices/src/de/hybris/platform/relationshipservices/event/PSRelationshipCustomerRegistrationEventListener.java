/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.event;

import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Event listener to update relationships' TargetUser when a customer registers in the system. This will update all
 * relationships that don't have a target user(relationship request was sent for a user that is not in the system) and
 * the target email matches the registers user id.
 */
public class PSRelationshipCustomerRegistrationEventListener extends AbstractEventListener<RegisterEvent>
{

	private static final Logger LOG = LoggerFactory.getLogger(PSRelationshipCustomerRegistrationEventListener.class);
	private PSRelationshipService psRelationshipService;

	@Override
	protected void onEvent(final RegisterEvent registerEvent)
	{
		if (registerEvent.getCustomer() != null)
		{
			LOG.info("Updating relationships target user for newly registered user [" + registerEvent.getCustomer().getUid() + "]");
			psRelationshipService.updateTargetUserForNonRegistredUser(registerEvent.getCustomer());
		}
	}

	public PSRelationshipService getPsRelationshipService()
	{
		return psRelationshipService;
	}

	@Required
	public void setPsRelationshipService(final PSRelationshipService psRelationshipService)
	{
		this.psRelationshipService = psRelationshipService;
	}

}
