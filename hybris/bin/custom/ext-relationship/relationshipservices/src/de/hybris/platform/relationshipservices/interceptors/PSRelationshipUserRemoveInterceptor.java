/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.interceptors;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;


/**
 * PSRelationshipUserRemoveInterceptor , interceptor to throw an exception when trying to remove the user who has
 * atleast one relationship associated with it .
 */
public class PSRelationshipUserRemoveInterceptor implements RemoveInterceptor<UserModel>
{

	@Override
	public void onRemove(final UserModel userModel, final InterceptorContext ctx) throws InterceptorException
	{

		final List<PSRelationshipModel> relationships = (ArrayList<PSRelationshipModel>) CollectionUtils
				.union(userModel.getTargetRelationships(), userModel.getSourceRelationships());

		final boolean hasRelationships = relationships.stream()
				.filter(e -> e.getRelationshipStatus().equals(PSRelationshipStatus.ACTIVE)
						|| e.getRelationshipStatus().equals(PSRelationshipStatus.PENDING))
				.filter(e -> !ctx.getModelService().isRemoved(e))
				.filter(f -> !ctx.getElementsRegisteredFor(PersistenceOperation.DELETE).contains(f)).count() > 0;

		if (hasRelationships)
		{
			throw new InterceptorException("Can not remove an User(" + userModel
					+ ") while it is assigned to (at least one) existing Active/Pending Relationships");
		}

	}

}
