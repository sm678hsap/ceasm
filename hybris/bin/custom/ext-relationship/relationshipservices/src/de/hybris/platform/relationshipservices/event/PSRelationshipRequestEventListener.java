/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.event;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.relationshipservices.constants.RelationshipservicesConstants;
import de.hybris.platform.relationshipservices.model.PSRelationshipProcessModel;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Event listener for RelationshipRequestEvent.
 */

public class PSRelationshipRequestEventListener extends AbstractEventListener<PSRelationshipRequestEvent>
{

	private static final Logger LOG = LoggerFactory.getLogger(PSRelationshipRequestEventListener.class);
	private BusinessProcessService businessProcessService;
	private ModelService modelService;

	@Override
	protected void onEvent(final PSRelationshipRequestEvent relationshipRequestEvent)
	{
		final boolean isGuestUser = relationshipRequestEvent.getRelationshipParameter().getTargetUser() == null ? true : false;
		final String targetEmail = isGuestUser ? relationshipRequestEvent.getRelationshipParameter().getTargetEmail()
				: relationshipRequestEvent.getRelationshipParameter().getTargetUser().getUid();

		final PSRelationshipProcessModel businessProcessModel = (PSRelationshipProcessModel) getBusinessProcessService()
				.createProcess(
						RelationshipservicesConstants.BusinessProcess.RELATIONSHIP_REQUEST_EMAIL_PROCESS + "-" + targetEmail + "-"
								+ System.currentTimeMillis(),
						RelationshipservicesConstants.BusinessProcess.RELATIONSHIP_REQUEST_EMAIL_PROCESS);
		businessProcessModel.setGuestUser(isGuestUser);
		businessProcessModel.setTargetEmail(targetEmail);
		businessProcessModel.setSite(relationshipRequestEvent.getSite());
		businessProcessModel.setBaseStore(relationshipRequestEvent.getBaseStore());
		businessProcessModel.setCustomerDisplayName(relationshipRequestEvent.getRelationshipParameter().getFirstName() + " "
				+ relationshipRequestEvent.getRelationshipParameter().getLastName());
		businessProcessModel.setLanguage(relationshipRequestEvent.getLanguage());
		businessProcessModel.setCurrency(relationshipRequestEvent.getCurrency());
		getModelService().save(businessProcessModel);
		LOG.info("Starting business process for the relationship event");
		getBusinessProcessService().startProcess(businessProcessModel);
	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
