/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.service;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.relationship.data.PSRelationshipParameter;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;

import java.util.List;


/**
 * PSRelationshipService interface for Relationship related functionality
 */
public interface PSRelationshipService
{
	/**
	 * Add new relationship between source and target user with the given relationship status
	 *
	 * @param relationshipParameter
	 *           object holds relationship details
	 * @throws RelationshipAlreadyExistException
	 *            Exception when relationship already exist
	 *
	 */
	void addRelationship(PSRelationshipParameter relationshipParameter) throws RelationshipAlreadyExistException;

	/**
	 * Fetch all relations for users with status
	 *
	 * @param userModel
	 *           Current user
	 * @param relationshipStatus
	 *           Status of the relationship request
	 * @return List<PSRelationshipModel>
	 */
	List<PSRelationshipModel> getRelations(UserModel userModel, PSRelationshipStatus relationshipStatus);

	/**
	 * Fetch relation for source model, target model and relationship status
	 *
	 * @param sourceUser
	 *           Email of user that requests for permission.
	 * @param targetUser
	 *           Email of user whose permissions are requested
	 * @param relationshipStatus
	 *           Status of the relationship request
	 * @return PSRelationshipModel
	 */
	PSRelationshipModel getRelation(UserModel sourceUser, UserModel targetUser, PSRelationshipStatus relationshipStatus);

	/**
	 * Fetch relation for source model, target model and relationship status
	 *
	 * @param sourceUser
	 *           Email of user that requests for permission.
	 * @param targetUser
	 *           Email of user whose permissions are requested
	 * @param relationshipStatus
	 *           Status of the relationship request
	 * @return PSRelationshipModel
	 */
	PSRelationshipModel getRelation(UserModel sourceUser, String targetUser, PSRelationshipStatus relationshipStatus);

	/**
	 * Update relationship with status
	 *
	 * @param relationship
	 *           Model object for PSRelationship
	 * @param relationshipStatus
	 *           Status of the relationship request
	 */
	void updateRelation(PSRelationshipModel relationship, PSRelationshipStatus relationshipStatus);

	/**
	 * Approve relationship request by making status 'Active' and setting approval time
	 *
	 * @param relationship
	 *           Model object for PSRelationship
	 */
	void approveRelationRequest(PSRelationshipModel relationship);

	/**
	 * Reject relationship request by making status 'Rejected' and setting disapproval time
	 *
	 * @param relationship
	 *           Model object for PSRelationship
	 */
	void rejectRelationRequest(PSRelationshipModel relationship);

	/**
	 * Fetch relation for PK.
	 *
	 * @param relationshipId
	 *           relationshipId
	 * @return PSRelationshipModel
	 */
	PSRelationshipModel getRelationForPk(String relationshipId);

	/**
	 * gets customer data based on PK
	 *
	 * @param customerPK
	 *           Primary key of the customer
	 * @return CustomerData
	 */
	CustomerModel getCustomerForPK(String customerPK);

	/**
	 * Updates target user with given user model in relationship
	 *
	 * @param relationshipModel
	 *           Model object for relationship details
	 * @param targetUser
	 *           Model object of the user whose permission is requested
	 *
	 */
	void updateTargetUserInRelationship(PSRelationshipModel relationshipModel, UserModel targetUser);

	/**
	 * Updates target user with given user model in relationships that don't have a TargetUser set and Target email is
	 * same as targetUser's uid
	 *
	 * @param targetUser
	 *           Model object of the user whose permission is requested
	 */
	void updateTargetUserForNonRegistredUser(UserModel targetUser);

	/**
	 * Get the user in context, this may be currently signed in user or the one who gave currently signed in user POA
	 * holder
	 *
	 * @return UserModel
	 */
	UserModel getUserInContext();

	/**
	 * Gets List of Order for user's relationships.
	 *
	 * @param user
	 *           Current user
	 * @return List<OrderModel> of orders for user relationships
	 *
	 */
	List<OrderModel> getOrdersForUserRelationships(UserModel user);

	/**
	 * Gets list of saved carts for user's relationships.
	 *
	 * @param user
	 *           Current user
	 * @return List<CartModel> of drafts for user relationships
	 *
	 */
	List<CartModel> getDraftsForUserRelationships(UserModel user);

	/**
	 * Gets List of Addresses for user's relationships.
	 *
	 * @param user
	 *           Current user
	 * @return List<AddressModel> of addresses for user relationships
	 *
	 */
	List<AddressModel> getAddressesForUserRelationships(UserModel user);

	/**
	 * Fetch user's active and pending relationships count
	 *
	 * @param userModel
	 *           Current user
	 * @return Integer
	 */
	Integer getUserActiveAndPendingRelationshipsCount(UserModel userModel);

}
