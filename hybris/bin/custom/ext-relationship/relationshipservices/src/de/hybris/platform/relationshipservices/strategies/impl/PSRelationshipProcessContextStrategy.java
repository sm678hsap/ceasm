/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.strategies.impl;

import de.hybris.platform.acceleratorservices.process.strategies.impl.AbstractProcessContextStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Default strategy to impersonate site and initialize session context from the process model.
 */
public class PSRelationshipProcessContextStrategy extends AbstractProcessContextStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(PSRelationshipProcessContextStrategy.class);

	@Override
	public BaseSiteModel getCmsSite(final BusinessProcessModel businessProcess)
	{
		ServicesUtil.validateParameterNotNull(businessProcess, BUSINESS_PROCESS_MUST_NOT_BE_NULL_MSG);

		if (businessProcess instanceof PSRelationshipProcessModel)
		{
			return ((PSRelationshipProcessModel) businessProcess).getSite();
		}

		LOG.info("Unsupported BusinessProcess type [" + businessProcess.getClass().getSimpleName() + "] for item ["
				+ businessProcess + "]");
		return null;
	}

	@Override
	protected CustomerModel getCustomer(final BusinessProcessModel businessProcess)
	{
		return null;
	}
}
