/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.permission.service.impl;

import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.permission.dao.PSPermissionDao;
import de.hybris.platform.relationshipservices.permission.event.PSPermissionEvent;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * implementation of PSPermissionService
 */
public class DefaultPSPermissionService implements PSPermissionService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSPermissionService.class);
	private PSRelationshipService relationshipService;
	private PSPermissionDao permissionDao;
	private TypeService typeService;
	private EventService eventService;
	private UserService userService;
	private ModelService modelService;

	/**
	 * returns whether source user is POAHolder else if target user can access source user permissibleArea item
	 *
	 * @param accessRequestor
	 * @param objectOwner
	 * @param permissibleArea
	 * @throws RelationshipDoesNotExistException
	 */
	@Override
	public boolean isPermitted(final UserModel accessRequestor, final UserModel objectOwner, final String permissibleArea)
			throws RelationshipDoesNotExistException
	{
		boolean isPermitted = false;
		if (accessRequestor != null && objectOwner != null && permissibleArea != null)
		{
			final PSRelationshipModel relation = getRelationshipService().getRelation(accessRequestor, objectOwner,
					PSRelationshipStatus.ACTIVE);

			if (relation != null)
			{
				final boolean isSourcePOAHolder = isSourcePOAHolder(accessRequestor, relation);
				isPermitted = isSourcePOAHolder ? true
						: getPermissionDao().isPermitted(accessRequestor, objectOwner,
								getTypeService().getComposedTypeForCode(permissibleArea));
			}
			else
			{
				throw new RelationshipDoesNotExistException(
						String.format("Relation does not exist between given source user %s and target user %s",
								accessRequestor.getUid(), objectOwner.getUid()));
			}
		}
		return isPermitted;
	}

	/**
	 * adds permission for given users ,item and status
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param permissibleArea
	 * @param status
	 */
	@Override
	public void addPermission(final UserModel sourceUser, final UserModel targetUser,
			final PSPermissibleAreaItemTypeModel permissibleArea, final PSPermissionStatus status, final boolean isRequested)
	{
		if (sourceUser != null && targetUser != null && permissibleArea != null)
		{
			final PSPermissionModel permission = getPermissionDao().addPermission(sourceUser, targetUser, permissibleArea, status,
					isRequested);
			LOG.info("adding the permission for target user and source user ", sourceUser, targetUser);
			publishPermissionEvent(Collections.singletonList(permission));
		}
		else
		{
			LOG.debug("either source user {} , target user {} or permissible area {} is null", sourceUser, targetUser,
					permissibleArea);
		}
	}

	/**
	 * adds permission for given users ,item and status
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param permissibleAreaCode
	 * @param status
	 * @param isRequested
	 * @throws UnknownIdentifierException
	 */
	@Override
	public void addPermission(final UserModel sourceUser, final UserModel targetUser, final String permissibleAreaCode,
			final PSPermissionStatus status, final boolean isRequested) throws UnknownIdentifierException
	{
		if (sourceUser != null && targetUser != null && StringUtils.isNotEmpty(permissibleAreaCode))
		{
			final ComposedTypeModel permissibleAreaItemType = getTypeService().getComposedTypeForCode(permissibleAreaCode);

			final List<PSPermissibleAreaItemTypeModel> permissibleAreaItems = getPermissionDao()
					.getPermissibleAreasForTypeAndStatus(permissibleAreaItemType, true);

			if (CollectionUtils.isNotEmpty(permissibleAreaItems))
			{
				final PSPermissionModel permission = getPermissionDao().addPermission(sourceUser, targetUser,
						permissibleAreaItems.get(0), status == null ? PSPermissionStatus.PENDING : status, isRequested);
				publishPermissionEvent(Collections.singletonList(permission));
			}
			else
			{
				LOG.error("No permissible item found for given item code" + permissibleAreaCode);
			}
		}
	}

	private void publishPermissionEvent(final List<PSPermissionModel> permissions)
	{
		final PSPermissionEvent permissionEvent = new PSPermissionEvent();
		permissionEvent.setPermissions(permissions);
		LOG.info("Publishing permission event");
		getEventService().publishEvent(permissionEvent);
	}

	/**
	 * updates permission for given permission ids with given status
	 *
	 * @param permissionIds
	 * @param status
	 */
	@Override
	public void updatePermissionStatus(final List<String> permissionIds, final PSPermissionStatus status)
	{
		if (status != null && CollectionUtils.isNotEmpty(permissionIds))
		{
			List<PSPermissionModel> permissions = null;
			switch (status)
			{
				case ACTIVE:
					permissions = getPermissionDao().updatePermissionStatus(permissionIds, status, new Date(), null);
					break;
				case INACTIVE:
				case REJECTED:
				case CANCELLED:
					permissions = getPermissionDao().updatePermissionStatus(permissionIds, status, null, new Date());
					break;
				default:
					permissions = getPermissionDao().updatePermissionStatus(permissionIds, status, null, null);
					break;
			}
			publishPermissionEvent(permissions);
		}
	}

	/**
	 * updates permission status for given users ,item and status
	 *
	 * @param permissions
	 * @param status
	 */
	@Override
	public void updatePermissionsStatus(final List<PSPermissionModel> permissions, final PSPermissionStatus status)
	{
		if (CollectionUtils.isNotEmpty(permissions) && status != null)
		{
			getPermissionDao().updatePermissionStatus(permissions, status);
			publishPermissionEvent(permissions);
		}
	}

	/**
	 * returns permission list for given users and status
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param status
	 */
	@Override
	public List<PSPermissionModel> getPermissionsForStatus(final UserModel sourceUser, final UserModel targetUser,
			final List<PSPermissionStatus> status)
	{
		return getPermissionDao().getPermissionsForUserAndStatuses(sourceUser, targetUser, status);
	}

	/**
	 * returns permission list for given users ,status and type which are active
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param status
	 */
	@Override
	public List<PSPermissionModel> getPermissionForActiveTypes(final UserModel sourceUser, final UserModel targetUser,
			final PSPermissionStatus status)
	{
		final List<PSPermissionStatus> statuses = new ArrayList();
		statuses.add(status);
		return getPermissionDao().getPermissionWithPermissibleAreaStatus(sourceUser, targetUser, statuses, true);
	}

	/**
	 * adds permission for given users ,item and status
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param types
	 * @param status
	 * @param isRequested
	 *
	 * @throws UnknownIdentifierException
	 */
	@Override
	public void addPermission(final UserModel sourceUser, final UserModel targetUser, final List<String> types,
			final PSPermissionStatus status, final boolean isRequested) throws UnknownIdentifierException
	{
		if (CollectionUtils.isNotEmpty(types))
		{
			final List<ComposedTypeModel> composedTypes = new ArrayList();

			types.forEach(item -> composedTypes.add(getTypeService().getComposedTypeForCode(item)));
			final List<PSPermissibleAreaItemTypeModel> permissibleAreas = getPermissionDao()
					.getPermissibleAreasForTypeAndStatus(composedTypes, true);
			final List<PSPermissionModel> permissions = getPermissionDao().addPermissions(sourceUser, targetUser, permissibleAreas,
					(status != null) ? status : PSPermissionStatus.PENDING, isRequested);
			publishPermissionEvent(permissions);
		}
	}

	/**
	 * approves permission for given permission ids
	 *
	 * @param permissionIds
	 */
	@Override
	public void approvePermissionRequest(final List<String> permissionIds)
	{
		if (CollectionUtils.isNotEmpty(permissionIds))
		{
			updatePermissionStatus(permissionIds, PSPermissionStatus.ACTIVE);
		}
	}

	/**
	 * rejects permission for given permission ids
	 *
	 * @param permissionIds
	 */
	@Override
	public void rejectPermissionRequest(final List<String> permissionIds)
	{
		if (CollectionUtils.isNotEmpty(permissionIds))
		{
			updatePermissionStatus(permissionIds, PSPermissionStatus.REJECTED);
		}
	}

	/**
	 * revokes permission for given permission ids
	 *
	 * @param permissionIds
	 */
	@Override
	public void revokePermissionRequest(final List<String> permissionIds)
	{
		if (CollectionUtils.isNotEmpty(permissionIds))
		{
			updatePermissionStatus(permissionIds, PSPermissionStatus.INACTIVE);
		}
	}

	@Override
	public List<PSPermissionModel> getGivenOrRequestedPermissionsForTargetUser(final UserModel sourceUser,
			final UserModel targetUser, final List<PSPermissionStatus> status, final boolean isGivenOrRequested)
	{
		return getPermissionDao().getGivenOrRequestedPermissionsForTargetUser(sourceUser, targetUser, status, true,
				isGivenOrRequested);
	}

	@Override
	public List<PSPermissibleAreaItemTypeModel> getPermissibleAreasForPermissions(
			final List<PSPermissionModel> permissionsForSourceToTargetUser)
	{
		if (CollectionUtils.isNotEmpty(permissionsForSourceToTargetUser))
		{
			return permissionsForSourceToTargetUser.stream()
					.map(permission -> (PSPermissibleAreaItemTypeModel) permission.getPermissibleAreaItemType())
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	@Override
	public List<PSPermissibleAreaItemTypeModel> getPermissibleAreasForTypeCodes(final List<String> typeCodes)
	{

		final List<ComposedTypeModel> composedTypes = new ArrayList();

		if (CollectionUtils.isNotEmpty(typeCodes))
		{
			composedTypes.addAll(
					typeCodes.stream().map(item -> getTypeService().getComposedTypeForCode(item)).collect(Collectors.toList()));

			return getPermissionDao().getPermissibleAreasForTypeAndStatus(composedTypes, true);
		}
		return Collections.emptyList();
	}

	protected void giveMorePermissions(final UserModel sourceUser, final UserModel targetUser,
			final List<PSPermissibleAreaItemTypeModel> existingPermissibleAreas,
			final List<PSPermissibleAreaItemTypeModel> changedPermissibleAreas, final boolean isRequested)
	{

		for (final PSPermissibleAreaItemTypeModel newPermissibleArea : changedPermissibleAreas)
		{
			if (!existingPermissibleAreas.contains(newPermissibleArea))
			{
				addPermission(sourceUser, targetUser, newPermissibleArea, PSPermissionStatus.PENDING, isRequested);
			}
		}
	}

	@Override
	public void updatePendingRequest(final String relationshipId, final boolean isAcceptedOrRejected,
			final boolean isGivenOrRequested)
	{
		boolean isPendingRelationRejectRequest = false;
		final PSRelationshipModel relationshipModel = getRelationshipService().getRelationForPk(relationshipId);
		if (relationshipModel != null)
		{
			if (relationshipModel.getRelationshipStatus().equals(PSRelationshipStatus.PENDING))
			{
				final PSRelationshipStatus relationshipStatus = isAcceptedOrRejected ? PSRelationshipStatus.ACTIVE
						: PSRelationshipStatus.REJECTED;
				getRelationshipService().updateRelation(relationshipModel, relationshipStatus);
				isPendingRelationRejectRequest = !isAcceptedOrRejected;
			}
			final List<PSPermissionModel> givenPermissionsToCurrentUser = getGivenOrRequestedPermissionsForCurrentUser(
					relationshipModel, true);
			final List<PSPermissionModel> requestedPermissionsFromCurrentUser = getGivenOrRequestedPermissionsForCurrentUser(
					relationshipModel, false);
			final List<String> pendingPermissionIds = getPendingPermissionsIds(
					isGivenOrRequested ? givenPermissionsToCurrentUser : requestedPermissionsFromCurrentUser);

			if (isPendingRelationRejectRequest)
			{
				pendingPermissionIds.addAll(getPendingPermissionsIds(
						!isGivenOrRequested ? givenPermissionsToCurrentUser : requestedPermissionsFromCurrentUser));
			}
			updatePermissionStatus(pendingPermissionIds,
					isAcceptedOrRejected ? PSPermissionStatus.ACTIVE : PSPermissionStatus.REJECTED);
		}
	}

	@Override
	public void updatePendingRequestForTypeCodes(final String relationshipId, final boolean isAcceptedOrRejected,
			final boolean isGivenOrRequested, final List<String> grantedTypeCodes, final List<String> rejectedTypeCodes)
	{
		boolean isPendingRelationRejectRequest = false;
		final PSRelationshipModel relationshipModel = getRelationshipService().getRelationForPk(relationshipId);
		if (relationshipModel != null)
		{
			if (relationshipModel.getRelationshipStatus().equals(PSRelationshipStatus.PENDING))
			{
				getRelationshipService().updateRelation(relationshipModel,
						isAcceptedOrRejected ? PSRelationshipStatus.ACTIVE : PSRelationshipStatus.REJECTED);
				isPendingRelationRejectRequest = !isAcceptedOrRejected;
			}
			final List<PSPermissionModel> givenPermissionsToCurrentUser = getGivenOrRequestedPermissionsForCurrentUser(
					relationshipModel, true);
			final List<PSPermissionModel> requestedPermissionsFromCurrentUser = getGivenOrRequestedPermissionsForCurrentUser(
					relationshipModel, false);

			final List<PSPermissionModel> pendingPermissions = isGivenOrRequested ? givenPermissionsToCurrentUser
					: requestedPermissionsFromCurrentUser;

			if (CollectionUtils.isNotEmpty(grantedTypeCodes))
			{
				final List<String> grantedPermissionIds = getPermissionIdsForTypeCodes(pendingPermissions, grantedTypeCodes);
				updatePermissionStatus(grantedPermissionIds, PSPermissionStatus.ACTIVE);
			}
			if (CollectionUtils.isNotEmpty(rejectedTypeCodes) || isPendingRelationRejectRequest)
			{
				final List<String> rejectedPermissionIds = getPermissionIdsForTypeCodes(pendingPermissions, rejectedTypeCodes);
				if (isPendingRelationRejectRequest)
				{
					rejectedPermissionIds.addAll(getPendingPermissionsIds(
							!isGivenOrRequested ? givenPermissionsToCurrentUser : requestedPermissionsFromCurrentUser));
				}

				updatePermissionStatus(rejectedPermissionIds, PSPermissionStatus.REJECTED);
			}
		}
	}

	@Override
	public void changePermissions(final String sourceUserEmail, final String targetUserEmail, final List<String> typeCodes,
			final boolean isRequested)
	{
		final List<PSPermissibleAreaItemTypeModel> changedPermissibleAreas = getPermissibleAreasForTypeCodes(typeCodes);
		final UserModel sourceUser = getUserService().getUserForUID(sourceUserEmail);
		final UserModel targetUser = getUserService().getUserForUID(targetUserEmail);

		if (getUserService().isUserExisting(sourceUserEmail) && getUserService().isUserExisting(targetUserEmail))
		{
			final List<PSPermissionModel> permissionsForSourceToTargetUser = getPermissionsForStatus(sourceUser, targetUser,
					Arrays.asList(PSPermissionStatus.ACTIVE));

			final List<PSPermissibleAreaItemTypeModel> existingPermissibleAreas = getPermissibleAreasForPermissions(
					permissionsForSourceToTargetUser);
			if (permissionsForSourceToTargetUser != null)
			{
				final List<String> permissionsToBeInactive = permissionsForSourceToTargetUser.stream()
						.filter(permission -> !changedPermissibleAreas.contains(permission.getPermissibleAreaItemType())
								&& permission.getPermissionStatus() == PSPermissionStatus.ACTIVE)
						.map(item -> item.getPk().getLongValueAsString()).collect(Collectors.toCollection(ArrayList::new));

				updatePermissionStatus(permissionsToBeInactive, PSPermissionStatus.INACTIVE);
			}
			giveMorePermissions(sourceUser, targetUser, existingPermissibleAreas, changedPermissibleAreas, isRequested);
		}
		else
		{
			throw new UnknownIdentifierException("Source and or target user does not exist");
		}
	}

	protected List<String> getPermissionIdsForTypeCodes(final List<PSPermissionModel> permissions, final List<String> typeCodes)
	{
		final List<String> permissionIds = new ArrayList<>();
		for (final PSPermissionModel permission : permissions)
		{
			for (final String typeCode : typeCodes)
			{
				if (permission.getPermissibleAreaItemType() instanceof PSPermissibleAreaItemTypeModel
						&& ((PSPermissibleAreaItemTypeModel) permission.getPermissibleAreaItemType()).getShareableType() != null
						&& ((PSPermissibleAreaItemTypeModel) permission.getPermissibleAreaItemType()).getShareableType().getCode()
								.equals(typeCode))
				{
					permissionIds.add(permission.getPk().getLongValueAsString());
				}
			}
		}
		return permissionIds;
	}

	@Override
	public List<PSPermissionModel> getGivenOrRequestedPermissionsForCurrentUser(final PSRelationshipModel relationship,
			final boolean isGivenOrRequested)
	{
		if (relationship.getSourceUser() != null && relationship.getTargetUser() != null)
		{
			final UserModel targetUser = relationshipService.getUserInContext();
			final UserModel sourceUser = relationship.getSourceUser().getUid().equals(targetUser.getUid())
					? relationship.getTargetUser()
					: relationship.getSourceUser();
			final List<PSPermissionStatus> permissionStatus = Arrays.asList(PSPermissionStatus.ACTIVE, PSPermissionStatus.PENDING);

			final List<PSPermissionModel> permissions = getGivenOrRequestedPermissionsForTargetUser(sourceUser, targetUser,
					permissionStatus, isGivenOrRequested);
			return permissions;
		}
		return Collections.emptyList();
	}

	@Override
	public void approveRelationRequest(final String relationshipId)
	{
		if (StringUtils.isNotEmpty(relationshipId))
		{
			final PSRelationshipModel relationshipModel = getRelationshipService().getRelationForPk(relationshipId);
			if (relationshipModel != null)
			{
				final List<PSPermissionModel> permissionForSourceToTargetUser = getPermissionsForStatus(
						relationshipModel.getSourceUser(), relationshipModel.getTargetUser(), null);
				final List<PSPermissionModel> permissionForTargetToSourceUser = getPermissionsForStatus(
						relationshipModel.getTargetUser(), relationshipModel.getSourceUser(), null);
				final List<String> permissionIds = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(permissionForSourceToTargetUser))
				{
					permissionForSourceToTargetUser.forEach(item -> permissionIds.add(item.getPk().getLongValueAsString()));
				}

				if (CollectionUtils.isNotEmpty(permissionForTargetToSourceUser))
				{
					permissionForTargetToSourceUser.forEach(item -> permissionIds.add(item.getPk().getLongValueAsString()));
				}
				getRelationshipService().approveRelationRequest(relationshipModel);
				updatePermissionStatus(permissionIds, PSPermissionStatus.ACTIVE);
			}
		}
	}

	protected List<String> getPendingPermissionsIds(final List<PSPermissionModel> permissions)
	{
		if (CollectionUtils.isNotEmpty(permissions))
		{
			return permissions.stream().filter(permission -> PSPermissionStatus.PENDING.equals(permission.getPermissionStatus()))
					.map(p -> p.getPk().getLongValueAsString()).collect(Collectors.toList());
		}
		else
		{
			LOG.debug("Permission ids passed are empty");
		}
		return new ArrayList<>();
	}

	/**
	 * verifies if SourceUser has POA.
	 *
	 * @param sourceUser
	 * @param relation
	 * @return boolean
	 */
	private boolean isSourcePOAHolder(final UserModel sourceUser, final PSRelationshipModel relation)
	{
		if (sourceUser.getUid().equalsIgnoreCase(relation.getSourceUser().getUid()))
		{
			return relation.getIsSourcePoaHolder();
		}
		else if (sourceUser.getUid().equalsIgnoreCase(relation.getTargetUser().getUid()))
		{
			return relation.getIsTargetPoaHolder();
		}
		return false;
	}

	@Override
	public List<AbstractPSPermissibleAreaModel> getPermissibleItemTypes(final Boolean isVisibleToUser, final Boolean isActive)
	{
		return getPermissionDao().getPermissibleAreaItems(isVisibleToUser, isActive);
	}

	protected PSRelationshipService getRelationshipService()
	{
		return relationshipService;
	}

	@Required
	public void setRelationshipService(final PSRelationshipService relationshipService)
	{
		this.relationshipService = relationshipService;
	}

	protected PSPermissionDao getPermissionDao()
	{
		return permissionDao;
	}

	@Required
	public void setPermissionDao(final PSPermissionDao permissionDao)
	{
		this.permissionDao = permissionDao;
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
