/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.exception;

/**
 * Exception which is used when relationship doesn't exist between 2 users
 */
public class RelationshipDoesNotExistException extends Exception
{
	public RelationshipDoesNotExistException(final String message)
	{
		super(message);
	}

	public RelationshipDoesNotExistException(final Throwable cause)
	{
		super(cause);
	}

	public RelationshipDoesNotExistException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
