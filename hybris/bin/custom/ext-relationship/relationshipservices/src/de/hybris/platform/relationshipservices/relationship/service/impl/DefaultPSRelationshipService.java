/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.event.PSRelationshipRequestEvent;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.relationship.dao.PSRelationshipDao;
import de.hybris.platform.relationshipservices.relationship.data.PSRelationshipParameter;
import de.hybris.platform.relationshipservices.relationship.event.PSRelationshipEvent;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipAlreadyExistException;
import de.hybris.platform.relationshipservices.relationship.service.PSRelationshipService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.task.TaskService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Default Implementation of PSRelationshipService interface.
 */
public class DefaultPSRelationshipService implements PSRelationshipService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSRelationshipService.class);
	private static final String SESSION_USER_IN_CONTEXT = "userInContext";
	private static final String RELATIONSHIP_EXISTS_ERROR = "Relationship already exists between user1=%s, user2=%s, and relationshipStatus=%s";
	private PSRelationshipDao psRelationshipDao;
	private ModelService modelService;
	private EventService eventService;
	private BaseSiteService baseSiteService;
	private CommonI18NService commonI18NService;
	private BaseStoreService baseStoreService;
	private UserService userService;
	private SessionService sessionService;
	private TypeService typeService;
	private CommerceCommonI18NService commerceCommonI18NService;
	private TaskService taskService;
	private String taskRunnerBean;

	@Override
	public void addRelationship(final PSRelationshipParameter relationshipParameter) throws RelationshipAlreadyExistException
	{
		final List<PSRelationshipStatus> statuses = new ArrayList<>();
		if (relationshipParameter.getRelationshipStatus() == null)
		{
			statuses.add(PSRelationshipStatus.ACTIVE);
			statuses.add(PSRelationshipStatus.PENDING);
		}
		else
		{
			statuses.add(relationshipParameter.getRelationshipStatus());
		}

		List<PSRelationshipModel> existingRelationships;
		PSRelationshipModel relationship = null;
		if (relationshipParameter.getTargetUser() != null)
		{
			existingRelationships = getPsRelationshipDao().getRelationshipForRegisteredUsersWithStatuses(
					relationshipParameter.getSourceUser(), relationshipParameter.getTargetUser(), statuses);
			checkIfRelationshipAlreadyExists(relationshipParameter.getSourceUser(), relationshipParameter.getTargetUser(),
					relationshipParameter.getTargetEmail(), existingRelationships);
			relationship = getPsRelationshipDao().createRelationshipBetweenRegisteredUsersForStatus(
					relationshipParameter.getSourceUser(), relationshipParameter.getTargetUser(),
					relationshipParameter.getRelationshipStatus() != null ? relationshipParameter.getRelationshipStatus()
							: PSRelationshipStatus.PENDING);
			getPsRelationshipDao().setGuestUserInformation(relationship, relationshipParameter.getTitle(),
					relationshipParameter.getFirstName(), relationshipParameter.getLastName());
		}
		else
		{
			LOG.debug("relationship parameter dont have target user ");
			existingRelationships = getPsRelationshipDao().getRelationshipForRegisteredUserAndGuestUserWithStatuses(
					relationshipParameter.getSourceUser(), relationshipParameter.getTargetEmail(), statuses);
			checkIfRelationshipAlreadyExists(relationshipParameter.getSourceUser(), relationshipParameter.getTargetUser(),
					relationshipParameter.getTargetEmail(), existingRelationships);
			relationship = getPsRelationshipDao().createRelationshipBetweenRegisteredUserAndGuestUserForStatus(
					relationshipParameter.getSourceUser(), relationshipParameter.getTargetEmail(),
					relationshipParameter.getRelationshipStatus() != null ? relationshipParameter.getRelationshipStatus()
							: PSRelationshipStatus.PENDING);
			getPsRelationshipDao().setGuestUserInformation(relationship, relationshipParameter.getTitle(),
					relationshipParameter.getFirstName(), relationshipParameter.getLastName());
		}

		getEventService().publishEvent(initializeEvent(relationshipParameter));
		publishRelationshipEvent(relationship);
	}

	private void publishRelationshipEvent(final PSRelationshipModel relationshipModel)
	{
		if (relationshipModel != null)
		{
			final PSRelationshipEvent event = new PSRelationshipEvent();
			event.setRelationship(relationshipModel);
			LOG.info("publishing relationship event");
			getEventService().publishEvent(event);
		}
	}

	/**
	 * Initialize RelationshipRequestEvent
	 *
	 * @param relationshipParameter
	 */
	private PSRelationshipRequestEvent initializeEvent(final PSRelationshipParameter relationshipParameter)
	{
		final PSRelationshipRequestEvent event = new PSRelationshipRequestEvent();
		event.setRelationshipParameter(relationshipParameter);
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		event.setLanguage(getCommonI18NService().getCurrentLanguage());

		return event;
	}

	/**
	 * Checks if relationships already exists if yes then RelationshipAlreadyExistException is thrown
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param targetEmail
	 * @param existingRelationships
	 * @throws RelationshipAlreadyExistException
	 */
	private void checkIfRelationshipAlreadyExists(final UserModel sourceUser, final UserModel targetUser, final String targetEmail,
			final List<PSRelationshipModel> existingRelationships) throws RelationshipAlreadyExistException
	{
		if (CollectionUtils.isNotEmpty(existingRelationships))
		{
			throw new RelationshipAlreadyExistException(String.format(RELATIONSHIP_EXISTS_ERROR, sourceUser.getUid(),
					targetUser != null ? targetUser.getUid() : targetEmail, existingRelationships.get(0).getRelationshipStatus()));
		}
	}

	@Override
	public List<PSRelationshipModel> getRelations(final UserModel userModel, final PSRelationshipStatus relationshipStatus)
	{
		return getPsRelationshipDao().getRelations(userModel, relationshipStatus);
	}

	@Override
	public Integer getUserActiveAndPendingRelationshipsCount(final UserModel userModel)
	{
		return getPsRelationshipDao().getUserActiveAndPendingRelationshipsCount(userModel);
	}

	@Override
	public PSRelationshipModel getRelation(final UserModel sourceUser, final UserModel targetUser,
			final PSRelationshipStatus relationshipStatus)
	{
		return getPsRelationshipDao().getRelation(sourceUser, targetUser, relationshipStatus);
	}

	@Override
	public PSRelationshipModel getRelation(final UserModel sourceUser, final String targetUser,
			final PSRelationshipStatus relationshipStatus)
	{
		return getPsRelationshipDao().getRelation(sourceUser, targetUser, relationshipStatus);
	}

	@Override
	public void updateRelation(final PSRelationshipModel relationship, final PSRelationshipStatus relationshipStatus)
	{
		switch (relationshipStatus)
		{
			case ACTIVE:
				getPsRelationshipDao().approveRelationRequest(relationship);
				break;
			case REJECTED:
				getPsRelationshipDao().rejectRelationRequest(relationship);
				break;
			case CANCELLED:
				getPsRelationshipDao().cancelRelationRequest(relationship);
				break;
			default:
				getPsRelationshipDao().updateRelation(relationship, relationshipStatus);
				break;
		}
		publishRelationshipEvent(relationship);
	}

	@Override
	public void approveRelationRequest(final PSRelationshipModel relationship)
	{
		getPsRelationshipDao().approveRelationRequest(relationship);
		publishRelationshipEvent(relationship);
	}

	@Override
	public void updateTargetUserInRelationship(final PSRelationshipModel relationshipModel, final UserModel targetUser)
	{
		LOG.info("Updating target user in relationship");
		relationshipModel.setTargetUser(targetUser);
		getModelService().save(relationshipModel);
	}

	@Override
	public void rejectRelationRequest(final PSRelationshipModel relationship)
	{
		getPsRelationshipDao().rejectRelationRequest(relationship);
		publishRelationshipEvent(relationship);
	}

	@Override
	public PSRelationshipModel getRelationForPk(final String relationshipId)
	{
		return getPsRelationshipDao().getRelationForPk(relationshipId);
	}

	@Override
	public CustomerModel getCustomerForPK(final String customerPK)
	{
		validateParameterNotNull(customerPK, "Customer PK cannot be null");
		return getPsRelationshipDao().getCustomerForPk(customerPK);
	}

	@Override
	public void updateTargetUserForNonRegistredUser(final UserModel targetUser)
	{
		validateParameterNotNull(targetUser, "Customer cannot be null");
		final List<PSRelationshipModel> relationships = getPsRelationshipDao()
				.getRelationshipsWithoutTargetUserForUserUid(targetUser.getUid());

		relationships.forEach(item -> {
			LOG.debug("updating target user for relationship {} with target user {}", item.getPk(), targetUser.getUid());
			item.setTargetUser(targetUser);
			item.setTargetEmail(null);
		});
		getModelService().saveAll(relationships);
	}

	@Override
	public UserModel getUserInContext()
	{
		final String customerUid = sessionService.getAttribute(SESSION_USER_IN_CONTEXT);
		if (StringUtils.isBlank(customerUid))
		{
			LOG.debug("No context user in session. hence returning current user");
			return userService.getCurrentUser();
		}
		else
		{
			return userService.getUserForUID(customerUid);
		}
	}

	@Override
	public List<OrderModel> getOrdersForUserRelationships(final UserModel user)
	{
		return getPsRelationshipDao().getOrdersForUserRelationships(user,
				getTypeService().getComposedTypeForCode(PSServiceProductModel._TYPECODE));
	}

	@Override
	public List<CartModel> getDraftsForUserRelationships(final UserModel user)
	{
		return getPsRelationshipDao().getDraftsForUserRelationships(user,
				getTypeService().getComposedTypeForCode(PSServiceProductModel._TYPECODE));
	}

	@Override
	public List<AddressModel> getAddressesForUserRelationships(final UserModel user)
	{
		return getPsRelationshipDao().getAddressesForUserRelationships(user,
				getTypeService().getComposedTypeForCode(AddressModel._TYPECODE), getCommerceCommonI18NService().getAllCountries());
	}

	protected PSRelationshipDao getPsRelationshipDao()
	{
		return psRelationshipDao;
	}

	@Required
	public void setPsRelationshipDao(final PSRelationshipDao psRelationshipDao)
	{
		this.psRelationshipDao = psRelationshipDao;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	protected CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	public TaskService getTaskService()
	{
		return taskService;
	}

	@Required
	public void setTaskService(final TaskService taskService)
	{
		this.taskService = taskService;
	}

	public String getTaskRunnerBean()
	{
		return taskRunnerBean;
	}

	@Required
	public void setTaskRunnerBean(final String taskRunnerBean)
	{
		this.taskRunnerBean = taskRunnerBean;
	}


}
