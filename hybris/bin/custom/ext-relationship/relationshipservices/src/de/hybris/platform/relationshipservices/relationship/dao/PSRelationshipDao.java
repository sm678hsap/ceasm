/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.dao;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.jalo.PSPermissibleAreaItemType;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;

import java.util.Collection;
import java.util.List;


/**
 * PSRelationshipDao Interface.
 */
public interface PSRelationshipDao
{
	/**
	 * Get relationship status for the provided source, target user and relationship statuses.
	 *
	 * @param sourceUser
	 *           - registered user
	 * @param targetUser
	 *           - registered user
	 * @param relationshipStatuses
	 *           - various statuses
	 * @return PSRelationshipModels - For status ACTIVE/PENDING - Only 1 model will be returned, where as for other
	 *         statuses it can return multiple
	 */
	List<PSRelationshipModel> getRelationshipForRegisteredUsersWithStatuses(UserModel sourceUser, UserModel targetUser,
			List<PSRelationshipStatus> relationshipStatuses);

	/**
	 * Get relationship status for the provided source user, target email and relationship statuses.
	 *
	 * @param sourceUser
	 *           - registered user
	 * @param targetEmail
	 *           - guest user's email address
	 * @param relationshipStatuses
	 *           - various statuses
	 * @return PSRelationshipModels - For status ACTIVE/PENDING - Only 1 model will be returned, where as for other
	 *         statuses it can return multiple
	 */
	List<PSRelationshipModel> getRelationshipForRegisteredUserAndGuestUserWithStatuses(UserModel sourceUser, String targetEmail,
			List<PSRelationshipStatus> relationshipStatuses);

	/**
	 * Create relationship between 2 users for the provided status
	 *
	 * @param sourceUser
	 *           - registered user
	 * @param targetUser
	 *           - guest user's email address
	 * @param relationshipStatus
	 *           - relationship status
	 */
	PSRelationshipModel createRelationshipBetweenRegisteredUsersForStatus(UserModel sourceUser, UserModel targetUser,
			PSRelationshipStatus relationshipStatus);

	/**
	 * Create relationship between a user and guest user for the provided status
	 *
	 * @param sourceUser
	 *           - registered user
	 * @param targetEmail
	 *           - guest user's email address
	 * @param relationshipStatus
	 *           - relationship status
	 */
	PSRelationshipModel createRelationshipBetweenRegisteredUserAndGuestUserForStatus(UserModel sourceUser, String targetEmail,
			PSRelationshipStatus relationshipStatus);

	/**
	 * Fetch relations for user and relationship status
	 *
	 * @param userModel
	 * @param relationshipStatus
	 * @return PSRelationshipModels
	 */
	List<PSRelationshipModel> getRelations(UserModel userModel, PSRelationshipStatus relationshipStatus);

	/**
	 * Fetch relation for given source and target user with relationship status
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param relationshipStatus
	 * @return PSRelationshipModel
	 */
	PSRelationshipModel getRelation(UserModel sourceUser, UserModel targetUser, PSRelationshipStatus relationshipStatus);

	/**
	 * Fetch relation for given source and target user with relationship status
	 *
	 * @param sourceUser
	 * @param targetUser
	 * @param relationshipStatus
	 * @return PSRelationshipModel
	 */
	PSRelationshipModel getRelation(UserModel sourceUser, String targetUser, PSRelationshipStatus relationshipStatus);

	/**
	 * Update relationship with status
	 *
	 * @param relationship
	 * @param relationshipStatus
	 */
	void updateRelation(PSRelationshipModel relationship, PSRelationshipStatus relationshipStatus);

	/**
	 * Approve relationship request by making status 'Active' and setting approval time
	 *
	 * @param relationship
	 */
	void approveRelationRequest(PSRelationshipModel relationship);

	/**
	 * Reject relationship request by making status 'Rejected' and setting disapproval time
	 */
	void rejectRelationRequest(PSRelationshipModel relationship);

	/**
	 * Cancel relationship request by making status 'Canceled' and setting disapproval time
	 *
	 * @param relationship
	 */
	void cancelRelationRequest(final PSRelationshipModel relationship);

	/**
	 * Fetch relation for pk.
	 *
	 * @param relationshipId
	 * @return PSRelationshipModel
	 */
	PSRelationshipModel getRelationForPk(String relationshipId);

	/**
	 * Save guest user information in relationship
	 *
	 * @param relationship
	 * @param title
	 * @param firstName
	 * @param lastName
	 */
	void setGuestUserInformation(PSRelationshipModel relationship, TitleModel title, String firstName, String lastName);

	/**
	 * gets customer data based on PK
	 *
	 * @param customerPK
	 * @return CustomerData
	 */
	CustomerModel getCustomerForPk(String customerPK);

	/**
	 * Finds Relationships that do not have a TargetUser set for given userId(TargetEmail)
	 *
	 * @param userId
	 * @return List<PSRelationshipModel>
	 */
	List<PSRelationshipModel> getRelationshipsWithoutTargetUserForUserUid(String userId);

	/**
	 *
	 */
	List<CustomerModel> getPOAReceiversForUserWithStatus(UserModel user, PSRelationshipStatus relationshipStatus);

	/**
	 *
	 */
	List<CustomerModel> getPOAHoldersForUserWithStatus(UserModel user, PSRelationshipStatus relationshipStatus);

	/**
	 *
	 */
	List<CustomerModel> getUserRelationsWithPermissionToShareableType(UserModel user, PSPermissibleAreaItemType shareableType,
			PSPermissionStatus permissionStatus);

	/**
	 *
	 */
	List<OrderModel> getOrdersForUserRelationships(UserModel user, ComposedTypeModel serviceProductComposedType);

	/**
	 *
	 */
	List<CartModel> getDraftsForUserRelationships(UserModel user, ComposedTypeModel serviceProductComposedType);

	/**
	 *
	 */
	List<AddressModel> getAddressesForUserRelationships(UserModel user, ComposedTypeModel serviceProductComposedType,
			Collection<CountryModel> deliveryCountries);

	/**
	 * Fetches user's active & pending relationships count
	 *
	 * @param userModel
	 * @return Integer
	 */
	Integer getUserActiveAndPendingRelationshipsCount(UserModel userModel);
}
