/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.relationshipservices.relationship.dao.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.jalo.PSPermissibleAreaItemType;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.relationshipservices.relationship.dao.PSRelationshipDao;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;


/**
 * Default implementation of PSRelationshipDao interface.
 */
public class DefaultPSRelationshipDao extends AbstractItemDao implements PSRelationshipDao
{
	private static final String USER_NOT_NULL = "User should not be null";
	private static final String SHAREABLE_TYPE_NOT_NULL = "Shareable item type should not be null";
	private static final String RELATIONSHIP_STATUSES_NOT_NULL = "Relationship status or statuses should not be null";
	private static final String RELATIONSHIP_STATUSES = "relationshipStatuses";
	private static final String SHAREABLE_TYPE = "shareableType";
	private static final String RELATIONSHIP_STATUS = "status";
	private static final String PERMISSION_STATUS = "permissionStatus";
	private static final String TARGET_EMAIL = "targetEmail";

	private static final String USER = "user";
	private static final String SHIP_ADDRESS = "shippingAddress";
	private static final String VISIBLE_ADDRESS_BOOK = "visibleInAddressBook";
	private static final String DELIVERY_COUNTRIES = "deliveryCountries";
	private static final String ACTIVE = "active";
	private static final String SOURCE_POA_HOLDER = "isSourcePoaHolder";
	private static final String TARGET_POA_HOLDER = "isTargetPoaHolder";
	private static final String SOURCE_USER = "sourceUser";
	private static final String TARGET_USER = "targetUser";
	private static final String PK = "pk";
	private static final String ACTIVE_RELATIONSHIPS = "activeRelationships";
	private static final String PENDING_RELATIONSHIPS = "pendingRelationships";

	private static final String FETCH_RELATIONSHIPS = "SELECT {" + PSRelationshipModel.PK + "} FROM {"
			+ PSRelationshipModel._TYPECODE + "}";

	private static final String FETCH_CUSTOMERS = "SELECT {" + CustomerModel.PK + "} FROM {" + CustomerModel._TYPECODE + "}";

	private static final String FETCH_RELATIONSHIP_FOR_USERS_WITH_STATUS = FETCH_RELATIONSHIPS + " WHERE (({"
			+ PSRelationshipModel.SOURCEUSER + "} = ?sourceUser AND {" + PSRelationshipModel.TARGETUSER + "} = ?targetUser) OR ({"
			+ PSRelationshipModel.SOURCEUSER + "} = ?targetUser AND {" + PSRelationshipModel.TARGETUSER + "} = ?sourceUser))  AND {"
			+ PSRelationshipModel.RELATIONSHIPSTATUS + "} = ?relationshipStatuses";

	private static final String FETCH_RELATIONSHIP_FOR_GUEST_USER_WITH_STATUS = FETCH_RELATIONSHIPS + " WHERE {"
			+ PSRelationshipModel.SOURCEUSER + "} = ?sourceUser AND {" + PSRelationshipModel.TARGETUSER + "} IS NULL AND {"
			+ PSRelationshipModel.TARGETEMAIL + "} = ?targetEmail AND {" + PSRelationshipModel.RELATIONSHIPSTATUS
			+ "} = ?relationshipStatuses";

	private static final String FETCH_RELATIONSHIP_WITHOUT_TARGET_USER = FETCH_RELATIONSHIPS + " WHERE {"
			+ PSRelationshipModel.TARGETUSER + "} IS NULL AND {" + PSRelationshipModel.TARGETEMAIL + "} = ?targetEmail";

	private static final String FETCH_RELATIONSHIPS_FOR_USER_WITH_STATUS = FETCH_RELATIONSHIPS + " WHERE (" + "({"
			+ PSRelationshipModel.SOURCEUSER + "} = ?user) OR " + "({" + PSRelationshipModel.TARGETUSER + "} = ?user) OR " + "({"
			+ PSRelationshipModel.TARGETEMAIL + "} = ?targetEmail)) " + "AND " + "{" + PSRelationshipModel.RELATIONSHIPSTATUS
			+ "} IN (?relationshipStatuses) " + "ORDER BY {" + PSRelationshipModel.CREATIONTIME + "}";

	private static final String FETCH_ACTIVE_AND_PENDING_RELATIONSHIPS_COUNT_FOR_USER = "SELECT COUNT(uniontable.pk) FROM ( {{ "
			+ FETCH_RELATIONSHIPS + " WHERE (" + "({" + PSRelationshipModel.SOURCEUSER + "} = ?user) OR " + "({"
			+ PSRelationshipModel.TARGETUSER + "} = ?user)) " + "AND " + "{" + PSRelationshipModel.RELATIONSHIPSTATUS
			+ "}  = (?activeRelationships) }} UNION {{ " + FETCH_RELATIONSHIPS + " WHERE (" + "({" + PSRelationshipModel.SOURCEUSER
			+ "} = ?user)) AND " + "{" + PSRelationshipModel.RELATIONSHIPSTATUS + "}  = (?pendingRelationships) }}) uniontable";

	private static final String FETCH_RELATIONSHIPS_FOR_PK = FETCH_RELATIONSHIPS + " WHERE {" + PSRelationshipModel.PK + "} =?pk";
	private static final String FETCH_CUSTOMER_ID_FOR_PK = FETCH_CUSTOMERS + " WHERE {" + CustomerModel.PK + "} =?pk";

	private static final String SELECT_PERMISSIBLE_ITEM = "SELECT {" + PSPermissibleAreaItemTypeModel.PK + "} FROM {"
			+ PSPermissibleAreaItemTypeModel._TYPECODE + "}" + " WHERE {" + AbstractPSPermissibleAreaModel.ACTIVE + "} = ?active"
			+ " AND {" + PSPermissibleAreaItemTypeModel.SHAREABLETYPE + "} =?shareableType";

	private static final String FETCH_POA_RECEIVERS_FOR_USER = "SELECT ( CASE WHEN {" + PSRelationshipModel.SOURCEUSER
			+ "} = ?user" + " THEN {" + PSRelationshipModel.TARGETUSER + "} ELSE {" + PSRelationshipModel.SOURCEUSER
			+ "} END ) AS usr_rel_per FROM" + " {" + PSRelationshipModel._TYPECODE + "} WHERE ( ({" + PSRelationshipModel.SOURCEUSER
			+ "} = ?user AND {" + PSRelationshipModel.ISSOURCEPOAHOLDER + "} = ?isSourcePoaHolder)" + " OR ({" + PSRelationshipModel.TARGETUSER
			+ "} = ?user AND {" + PSRelationshipModel.ISTARGETPOAHOLDER + "} = ?isTargetPoaHolder) )" + " AND {"
			+ PSRelationshipModel.RELATIONSHIPSTATUS + "} = ?status";

	private static final String FETCH_POA_HOLDERS_FOR_USER = "SELECT ( CASE WHEN {" + PSRelationshipModel.SOURCEUSER + "} = ?user"
			+ " THEN {" + PSRelationshipModel.TARGETUSER + "} ELSE {" + PSRelationshipModel.SOURCEUSER + "} END ) AS usr_rel_per FROM"
			+ " {" + PSRelationshipModel._TYPECODE + "} WHERE ( ({" + PSRelationshipModel.SOURCEUSER + "} = ?user AND {"
			+ PSRelationshipModel.ISTARGETPOAHOLDER + "} = ?isTargetPoaHolder)" + " OR ({" + PSRelationshipModel.TARGETUSER + "} = ?user AND {"
			+ PSRelationshipModel.ISSOURCEPOAHOLDER + "} = ?isSourcePoaHolder) )" + " AND {" + PSRelationshipModel.RELATIONSHIPSTATUS
			+ "} = ?status";


	private static final String FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE = "SELECT uniontable.usr_rel_per FROM ( {{ "
			+ FETCH_POA_RECEIVERS_FOR_USER + " }} UNION ALL {{ SELECT {" + PSPermissionModel.SOURCEUSER + "} AS usr_rel_per FROM {"
			+ PSPermissionModel._TYPECODE + "} WHERE {" + PSPermissionModel.TARGETUSER + "} = ?user AND {"
			+ PSPermissionModel.PERMISSIBLEAREAITEMTYPE + "} = ( {{ " + SELECT_PERMISSIBLE_ITEM + " }} ) AND {"
			+ PSPermissionModel.PERMISSIONSTATUS + "} = ?permissionStatus }} ) uniontable";

	private static final String FETCH_ORDER_COUNT_FOR_USER_RELATIONSHIPS = "SELECT {" + OrderModel.PK + "} FROM {"
			+ OrderModel._TYPECODE + "} WHERE {" + OrderModel.USER + "} IN ( {{ "
			+ FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE + " }} )";

	protected final static String SELECT_DRAFT_CLAUSE = "SELECT {" + CartModel.PK + "} FROM {" + CartModel._TYPECODE + "} ";

	protected final static String SAVED_CARTS_CLAUSE = "{" + CartModel.SAVETIME + "} IS NOT NULL";

	protected final static String DRAFTS_USER_CLAUSE = " ( {" + CartModel.USER + "} IN ( {{ "
			+ FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE + " }} ) AND ( {" + CartModel.USERINCONTEXT + "} IS NULL OR {"
			+ CartModel.USERINCONTEXT + "} = {" + CartModel.USER + "} ) " + " OR {" + CartModel.USERINCONTEXT + "} IN ( {{ "
			+ FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE + " }} ) ) ";

	protected final static String FETCH_DRAFTS_FOR_USER_RELATIONSHIPS = SELECT_DRAFT_CLAUSE + "WHERE " + DRAFTS_USER_CLAUSE
			+ " AND " + SAVED_CARTS_CLAUSE + " ";

	private static final String FETCH_ADDRESSES_FOR_USER_RELATIONSHIPS = "SELECT {address:" + AddressModel.PK + "} FROM {"
			+ AddressModel._TYPECODE + " AS address LEFT JOIN " + CustomerModel._TYPECODE + " AS customer ON {address:"
			+ AddressModel.OWNER + "}={customer:" + CustomerModel.PK + "}} WHERE {customer:" + CustomerModel.PK + "} IN ( {{ "
			+ FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE + " }} ) AND {address:" + AddressModel.SHIPPINGADDRESS
			+ "} = ?shippingAddress AND {address:" + AddressModel.VISIBLEINADDRESSBOOK + "} = ?visibleInAddressBook AND {address:"
			+ AddressModel.COUNTRY + "} IN (?deliveryCountries)";

	@Override
	public List<AddressModel> getAddressesForUserRelationships(final UserModel user,
			final ComposedTypeModel serviceProductComposedType, final Collection<CountryModel> deliveryCountries)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(USER, user);
		params.put(SHAREABLE_TYPE, serviceProductComposedType);
		params.put(RELATIONSHIP_STATUS, PSRelationshipStatus.ACTIVE);
		params.put(PERMISSION_STATUS, PSPermissionStatus.ACTIVE);

		params.put(SHIP_ADDRESS, Boolean.TRUE);
		params.put(VISIBLE_ADDRESS_BOOK, Boolean.TRUE);
		params.put(DELIVERY_COUNTRIES, deliveryCountries);

		params.put(ACTIVE, Boolean.TRUE);
		params.put(SOURCE_POA_HOLDER, Boolean.TRUE);
		params.put(TARGET_POA_HOLDER, Boolean.TRUE);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_ADDRESSES_FOR_USER_RELATIONSHIPS);
		query.addQueryParameters(params);

		final SearchResult<AddressModel> addresses = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(addresses.getResult()) ? addresses.getResult() : null;
	}


	@Override
	public List<OrderModel> getOrdersForUserRelationships(final UserModel user, final ComposedTypeModel serviceProductComposedType)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(USER, user);
		params.put(SHAREABLE_TYPE, serviceProductComposedType);

		params.put(RELATIONSHIP_STATUS, PSRelationshipStatus.ACTIVE);
		params.put(PERMISSION_STATUS, PSPermissionStatus.ACTIVE);

		params.put(ACTIVE, Boolean.TRUE);
		params.put(SOURCE_POA_HOLDER, Boolean.TRUE);
		params.put(TARGET_POA_HOLDER, Boolean.TRUE);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_ORDER_COUNT_FOR_USER_RELATIONSHIPS);
		query.addQueryParameters(params);

		final SearchResult<OrderModel> orders = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(orders.getResult()) ? orders.getResult() : null;
	}

	@Override
	public List<CartModel> getDraftsForUserRelationships(final UserModel user, final ComposedTypeModel serviceProductComposedType)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(USER, user);
		params.put(SHAREABLE_TYPE, serviceProductComposedType);

		params.put(RELATIONSHIP_STATUS, PSRelationshipStatus.ACTIVE);
		params.put(PERMISSION_STATUS, PSPermissionStatus.ACTIVE);

		params.put(ACTIVE, Boolean.TRUE);
		params.put(SOURCE_POA_HOLDER, Boolean.TRUE);
		params.put(TARGET_POA_HOLDER, Boolean.TRUE);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_DRAFTS_FOR_USER_RELATIONSHIPS);
		query.addQueryParameters(params);

		final SearchResult<CartModel> drafts = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(drafts.getResult()) ? drafts.getResult() : null;
	}

	@Override
	public List<CustomerModel> getUserRelationsWithPermissionToShareableType(final UserModel user,
			final PSPermissibleAreaItemType shareableType, final PSPermissionStatus permissionStatus)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(shareableType, SHAREABLE_TYPE_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(USER, user);
		params.put(SHAREABLE_TYPE, shareableType);

		if (permissionStatus != null)
		{
			params.put(PERMISSION_STATUS, permissionStatus);
		}
		else
		{
			params.put(PERMISSION_STATUS, PSPermissionStatus.ACTIVE);
		}

		params.put(ACTIVE, Boolean.TRUE);
		params.put(SOURCE_POA_HOLDER, Boolean.TRUE);
		params.put(TARGET_POA_HOLDER, Boolean.TRUE);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE);
		query.addQueryParameters(params);

		final SearchResult<CustomerModel> relationships = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(relationships.getResult()) ? relationships.getResult() : null;
	}

	@Override
	public List<CustomerModel> getPOAReceiversForUserWithStatus(final UserModel user,
			final PSRelationshipStatus relationshipStatus)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(USER, user);

		if (relationshipStatus != null)
		{
			params.put(RELATIONSHIP_STATUS, relationshipStatus);
		}
		else
		{
			params.put(RELATIONSHIP_STATUS, PSRelationshipStatus.ACTIVE);
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_POA_RECEIVERS_FOR_USER);
		query.addQueryParameters(params);

		final SearchResult<CustomerModel> relationships = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(relationships.getResult()) ? relationships.getResult() : null;
	}

	@Override
	public List<CustomerModel> getPOAHoldersForUserWithStatus(final UserModel user, final PSRelationshipStatus relationshipStatus)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(USER, user);

		if (relationshipStatus != null)
		{
			params.put(RELATIONSHIP_STATUS, relationshipStatus);
		}
		else
		{
			params.put(RELATIONSHIP_STATUS, PSRelationshipStatus.ACTIVE);
		}

		params.put(SOURCE_POA_HOLDER, Boolean.TRUE);
		params.put(TARGET_POA_HOLDER, Boolean.TRUE);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_POA_HOLDERS_FOR_USER);
		query.addQueryParameters(params);

		final SearchResult<CustomerModel> relationships = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(relationships.getResult()) ? relationships.getResult() : null;
	}


	@Override
	public List<PSRelationshipModel> getRelationshipForRegisteredUsersWithStatuses(final UserModel sourceUser,
			final UserModel targetUser, final List<PSRelationshipStatus> relationshipStatuses)
	{
		ServicesUtil.validateParameterNotNull(sourceUser, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(targetUser, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(relationshipStatuses, RELATIONSHIP_STATUSES_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(SOURCE_USER, sourceUser);
		params.put(TARGET_USER, targetUser);
		params.put(RELATIONSHIP_STATUSES, relationshipStatuses);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_RELATIONSHIP_FOR_USERS_WITH_STATUS);
		query.addQueryParameters(params);

		final SearchResult<PSRelationshipModel> relationships = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(relationships.getResult()) ? relationships.getResult() : null;
	}

	@Override
	public List<PSRelationshipModel> getRelationshipForRegisteredUserAndGuestUserWithStatuses(final UserModel sourceUser,
			final String targetEmail, final List<PSRelationshipStatus> relationshipStatuses)
	{
		ServicesUtil.validateParameterNotNull(sourceUser, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(targetEmail, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(relationshipStatuses, RELATIONSHIP_STATUSES_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(SOURCE_USER, sourceUser);
		params.put(TARGET_EMAIL, targetEmail);
		params.put(RELATIONSHIP_STATUSES, relationshipStatuses);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_RELATIONSHIP_FOR_GUEST_USER_WITH_STATUS);
		query.addQueryParameters(params);

		final SearchResult<PSRelationshipModel> relationships = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(relationships.getResult()) ? relationships.getResult() : null;
	}

	@Override
	public PSRelationshipModel createRelationshipBetweenRegisteredUsersForStatus(final UserModel sourceUser,
			final UserModel targetUser, final PSRelationshipStatus relationshipStatus)
	{
		ServicesUtil.validateParameterNotNull(sourceUser, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(targetUser, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(relationshipStatus, RELATIONSHIP_STATUSES_NOT_NULL);

		return createRelationshipModel(sourceUser, targetUser, null, relationshipStatus);
	}

	@Override
	public PSRelationshipModel createRelationshipBetweenRegisteredUserAndGuestUserForStatus(final UserModel sourceUser,
			final String targetEmail, final PSRelationshipStatus relationshipStatus)
	{
		ServicesUtil.validateParameterNotNull(sourceUser, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(targetEmail, USER_NOT_NULL);
		ServicesUtil.validateParameterNotNull(relationshipStatus, RELATIONSHIP_STATUSES_NOT_NULL);

		return createRelationshipModel(sourceUser, null, targetEmail, relationshipStatus);
	}

	@Override
	public List<PSRelationshipModel> getRelations(final UserModel userModel, final PSRelationshipStatus relationshipStatus)
	{
		ServicesUtil.validateParameterNotNull(userModel, USER_NOT_NULL);
		final List<PSRelationshipStatus> statuses = new ArrayList<>();
		if (relationshipStatus != null)
		{
			statuses.add(relationshipStatus);
		}
		else
		{
			statuses.add(PSRelationshipStatus.PENDING);
			statuses.add(PSRelationshipStatus.ACTIVE);
		}

		final Map<String, Object> params = new HashMap<>();
		params.put(USER, userModel);
		params.put(TARGET_EMAIL, userModel.getUid());
		params.put(RELATIONSHIP_STATUSES, statuses);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_RELATIONSHIPS_FOR_USER_WITH_STATUS);
		query.addQueryParameters(params);

		final SearchResult<PSRelationshipModel> relationships = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(relationships.getResult()) ? relationships.getResult() : null;
	}

	@Override
	public PSRelationshipModel getRelation(final UserModel sourceUser, final UserModel targetUser,
			final PSRelationshipStatus relationshipStatus)
	{
		final List<PSRelationshipModel> relations = getRelationshipForRegisteredUsersWithStatuses(sourceUser, targetUser,
				Collections.singletonList(relationshipStatus));
		return CollectionUtils.isEmpty(relations) ? null : relations.get(0);
	}

	@Override
	public PSRelationshipModel getRelation(final UserModel sourceUser, final String targetUser,
			final PSRelationshipStatus relationshipStatus)
	{
		final List<PSRelationshipModel> relations = getRelationshipForRegisteredUserAndGuestUserWithStatuses(sourceUser, targetUser,
				Collections.singletonList(relationshipStatus));
		return CollectionUtils.isEmpty(relations) ? null : relations.get(0);
	}

	/**
	 * Update relationship with status
	 *
	 * @param relationship
	 * @param relationshipStatus
	 */
	@Override
	public void updateRelation(final PSRelationshipModel relationship, final PSRelationshipStatus relationshipStatus)
	{
		if (relationship != null)
		{
			relationship.setRelationshipStatus(relationshipStatus);
			getModelService().save(relationship);
		}
	}

	/**
	 * Approve relationship request by making status 'Active' and setting approval time
	 *
	 * @param relationship
	 */
	@Override
	public void approveRelationRequest(final PSRelationshipModel relationship)
	{
		if (relationship != null)
		{
			relationship.setRelationshipStatus(PSRelationshipStatus.ACTIVE);
			relationship.setApprovalDateTime(new Date());
			getModelService().save(relationship);
		}
	}

	/**
	 * Reject relationship request by making status 'Rejected' and setting disapproval time
	 */
	@Override
	public void rejectRelationRequest(final PSRelationshipModel relationship)
	{
		if (relationship != null)
		{
			relationship.setRelationshipStatus(PSRelationshipStatus.REJECTED);
			relationship.setDisApprovalDateTime(new Date());
			getModelService().save(relationship);
		}
	}

	@Override
	public void cancelRelationRequest(final PSRelationshipModel relationship)
	{
		if (relationship != null)
		{
			relationship.setRelationshipStatus(PSRelationshipStatus.CANCELLED);
			relationship.setDisApprovalDateTime(new Date());
			getModelService().save(relationship);
		}
	}

	@Override
	public PSRelationshipModel getRelationForPk(final String relationshipId)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(PK, relationshipId);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_RELATIONSHIPS_FOR_PK);
		query.addQueryParameters(params);

		return getFlexibleSearchService().searchUnique(query);
	}

	@Override
	public void setGuestUserInformation(final PSRelationshipModel relationship, final TitleModel title, final String firstName,
			final String lastName)
	{
		if (relationship != null)
		{
			relationship.setTitle(title);
			relationship.setFirstName(firstName);
			relationship.setLastName(lastName);
			getModelService().save(relationship);
		}
	}

	/**
	 * Create relationship model
	 *
	 * @param sourceUser
	 * @param targetEmail
	 * @param relationshipStatus
	 */
	private PSRelationshipModel createRelationshipModel(final UserModel sourceUser, final UserModel targetUser,
			final String targetEmail, final PSRelationshipStatus relationshipStatus)
	{
		final PSRelationshipModel relationshipModel = getModelService().create(PSRelationshipModel.class);
		relationshipModel.setSourceUser(sourceUser);
		relationshipModel.setTargetUser(targetUser);
		relationshipModel.setTargetEmail(targetEmail);
		relationshipModel.setRelationshipStatus(relationshipStatus);
		relationshipModel.setRequestDateTime(new Date());
		getModelService().save(relationshipModel);
		return relationshipModel;
	}

	@Override
	public CustomerModel getCustomerForPk(final String customerPK)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(PK, customerPK);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_CUSTOMER_ID_FOR_PK);
		query.addQueryParameters(params);

		return getFlexibleSearchService().searchUnique(query);
	}

	@Override
	public List<PSRelationshipModel> getRelationshipsWithoutTargetUserForUserUid(final String userId)
	{
		ServicesUtil.validateParameterNotNull(userId, USER_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(TARGET_EMAIL, userId);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_RELATIONSHIP_WITHOUT_TARGET_USER);
		query.addQueryParameters(params);

		final SearchResult<PSRelationshipModel> relationships = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(relationships.getResult()) ? relationships.getResult() : Collections.emptyList();
	}

	@Override
	public Integer getUserActiveAndPendingRelationshipsCount(final UserModel userModel)
	{
		ServicesUtil.validateParameterNotNull(userModel, USER_NOT_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(USER, userModel);
		params.put(ACTIVE_RELATIONSHIPS, PSRelationshipStatus.ACTIVE);
		params.put(PENDING_RELATIONSHIPS, PSRelationshipStatus.PENDING);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_ACTIVE_AND_PENDING_RELATIONSHIPS_COUNT_FOR_USER);
		query.addQueryParameters(params);
		query.setResultClassList(Arrays.asList(Integer.class));

		final SearchResult<Integer> result = getFlexibleSearchService().search(query);

		return result.getResult().get(0);
	}
}
