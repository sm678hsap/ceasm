/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorsolrlangsupport.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.publicsectorsolrlangsupport.constants.PublicsectorsolrlangsupportConstants;

import java.util.ArrayList;
import java.util.List;


@SystemSetup(extension = PublicsectorsolrlangsupportConstants.EXTENSIONNAME)
public class PSSolrLangSupportSystemSetup extends AbstractSystemSetup
{
	public static final String PUBLICSECTOR = "publicsector";

	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));

		return params;
	}


	/**
	 * This method will be called during the system initialization.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		importImpexFile(context, "/publicsectorsolrlangsupport/import/coredata/store/store.impex", true);
		importImpexFile(context, "/publicsectorsolrlangsupport/import/coredata/store/solr.impex", true);

		if (this.getBooleanSystemSetupParameter(context, ACTIVATE_SOLR_CRON_JOBS))
		{
			getSetupSolrIndexerService().executeSolrIndexerCronJob(String.format("%sIndex", PUBLICSECTOR), true);
			getSetupSolrIndexerService().activateSolrIndexerCronJobs(String.format("%sIndex", PUBLICSECTOR));
		}
	}
}
