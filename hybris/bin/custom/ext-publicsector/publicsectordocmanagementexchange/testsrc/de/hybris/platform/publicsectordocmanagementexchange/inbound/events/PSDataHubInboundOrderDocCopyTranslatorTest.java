/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectordocmanagementexchange.inbound.events;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagementexchange.constants.PSDataHubInboundDocumentConstants;
import de.hybris.platform.publicsectordocmanagementexchange.inbound.PSDataHubInboundDocumentHelper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;


@SuppressWarnings("javadoc")
@UnitTest
public class PSDataHubInboundOrderDocCopyTranslatorTest
{

	@InjectMocks
	private PSDataHubInboundOrderDocCopyTranslator classUnderTest;
	@Mock
	private Item processedItem;

	@Mock
	private PSDataHubInboundDocumentHelper documentInboundService;

	@Before
	public void setUp()
	{
		classUnderTest = new PSDataHubInboundOrderDocCopyTranslator();
		processedItem = org.mockito.Mockito.mock(Item.class);
		documentInboundService = org.mockito.Mockito.mock(PSDataHubInboundDocumentHelper.class);
		classUnderTest.setInboundHelper(documentInboundService);
	}

	@Test
	public void testCopyOrderDocumentFromDataHub() throws ImpExException, JaloSecurityException
	{
		Mockito.when(processedItem.getAttribute(PSDataHubInboundDocumentConstants.DOCUMENT_ID)).thenReturn("DOC01");
		final OrderModel order = mock(OrderModel.class);
		Mockito.when(order.getCode()).thenReturn("ORD01");
		final PSDocumentModel document = mock(PSDocumentModel.class);
		Mockito.when(document.getFileName()).thenReturn("BirthCertificate.pdf");
		Mockito.when(document.getFilePath()).thenReturn("/a/b");

		classUnderTest.performImport("ORD01:BirthCertificate.pdf:/a/b:false:true", processedItem);
		org.mockito.Mockito.verify(documentInboundService).copyOrderDocumentFromHub("ORD01:BirthCertificate.pdf:/a/b:false:true",
				"DOC01");
	}
}
