/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagementexchange.inbound.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;
import de.hybris.platform.publicsectordocmanagementexchange.inbound.PSDataHubInboundDocumentHelper;

import java.io.IOException;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation class for {@link PSDataHubInboundDocumentHelper}
 */
public class DefaultPSDataHubInboundDocumentHelper extends AbstractPSDataHubInboundDocumentHelper
		implements PSDataHubInboundDocumentHelper
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSDataHubInboundDocumentHelper.class);

	private PSDocumentManagementService psDocumentManagementService;

	@Override
	public void copyOrderDocumentFromHub(final String orderDocumentInfo, final String documentId)
	{
		if (orderDocumentInfo != null)
		{
			final String[] tokens = orderDocumentInfo.split(":");
			if (tokens.length != 5)
			{
				LOG.warn("Error reading line, expected attributes " + tokens);
			}

			// get the order id
			final String orderId = tokens[0];
			// get the source file name
			final String sourceFileName = tokens[1];
			//get the source file path
			final String sourceFilePath = tokens[2];
			//get the document file type
			final Boolean documentThumbnail = Boolean.valueOf(tokens[3]);
			//get if the path is absolute
			final Boolean isAbsolutePath = Boolean.valueOf(tokens[4]);

			if (documentId != null)
			{
				final PSDocumentModel psDocument = getPsDocumentManagementService().findDocumentByDocId(documentId);
				if (psDocument != null)
				{
					final OrderModel order = getPsDocumentManagementService().findOrderbyCode(orderId);
					psDocument.setOrder(order);
					psDocument.setFileName(sourceFileName);
					psDocument.setFilePath(sourceFilePath);
					psDocument.setCustomer((CustomerModel) order.getUser());
					getModelService().save(psDocument);
				}
			}
			try
			{
				if (BooleanUtils.isTrue(documentThumbnail))
				{
					getPsDocumentManagementService().copyOrderDocumentThumbnail(orderId, sourceFileName, sourceFilePath,
							isAbsolutePath);
				}
				else
				{
					getPsDocumentManagementService().copyOrderDocument(orderId, sourceFileName, sourceFilePath, isAbsolutePath);
				}
			}
			catch (final IOException e)
			{
				LOG.error("Exception occurred during file copy due to : {}", e);
			}
		}
	}

	protected PSDocumentManagementService getPsDocumentManagementService()
	{
		return psDocumentManagementService;
	}

	@Required
	public void setPsDocumentManagementService(final PSDocumentManagementService psDocumentManagementService)
	{
		this.psDocumentManagementService = psDocumentManagementService;
	}
}
