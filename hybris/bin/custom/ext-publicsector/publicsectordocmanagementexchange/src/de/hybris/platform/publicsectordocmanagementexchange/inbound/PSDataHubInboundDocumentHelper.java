/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagementexchange.inbound;

/**
 * Interface class for Datahub Inbound Document Helper.
 */
public interface PSDataHubInboundDocumentHelper
{
	/**
	 * Copy Order Document from Datahub to Hybris.
	 * 
	 * @param orderDocumentInfo
	 *           String The document's related order, file name, and file path.
	 * @param documentId
	 *           String The document id.
	 */
	public void copyOrderDocumentFromHub(final String orderDocumentInfo, final String documentId);
}
