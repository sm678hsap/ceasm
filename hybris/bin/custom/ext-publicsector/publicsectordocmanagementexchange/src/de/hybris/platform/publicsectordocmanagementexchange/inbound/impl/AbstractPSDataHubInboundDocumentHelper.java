/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectordocmanagementexchange.inbound.impl;

import de.hybris.platform.servicelayer.model.ModelService;


/**
 * Abstract root class for inbound processing of order related notifications from Data Hub
 */
public abstract class AbstractPSDataHubInboundDocumentHelper
{

	private ModelService modelService;

	protected ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
