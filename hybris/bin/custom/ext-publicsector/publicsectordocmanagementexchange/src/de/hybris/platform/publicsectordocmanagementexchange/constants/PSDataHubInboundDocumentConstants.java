/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagementexchange.constants;

/**
 * Contants class for DataHub Inbound Document.
 */
public class PSDataHubInboundDocumentConstants
{
	public static final String DOCUMENT_ID = "docId";

	private PSDataHubInboundDocumentConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
