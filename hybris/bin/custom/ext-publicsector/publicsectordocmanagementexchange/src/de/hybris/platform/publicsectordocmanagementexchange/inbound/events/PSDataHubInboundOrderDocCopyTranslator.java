/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagementexchange.inbound.events;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.publicsectordocmanagementexchange.constants.PSDataHubInboundDocumentConstants;
import de.hybris.platform.publicsectordocmanagementexchange.inbound.PSDataHubInboundDocumentHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * DataHubInboundOrderDocCopyTranslator class which extends {@link PSDataHubInboundDocumentHelper}
 */
public class PSDataHubInboundOrderDocCopyTranslator extends PSDataHubTranslator<PSDataHubInboundDocumentHelper>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSDataHubInboundOrderDocCopyTranslator.class);
	@SuppressWarnings("javadoc")
	public static final String HELPER_BEAN = "psDataHubInboundDocumentHelper";

	@SuppressWarnings("javadoc")
	public PSDataHubInboundOrderDocCopyTranslator()
	{
		super(HELPER_BEAN);
	}

	@Override
	public void performImport(final String documentInfo, final Item processedItem) throws ImpExException

	{
		final String documentId = getDocumentId(processedItem);
		getInboundHelper().copyOrderDocumentFromHub(documentInfo, documentId);
	}

	/**
	 * Gets document id of the processedItem.
	 */
	private String getDocumentId(final Item processedItem) throws ImpExException
	{
		String documentId = null;
		try
		{
			documentId = processedItem.getAttribute(PSDataHubInboundDocumentConstants.DOCUMENT_ID).toString();
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("error in getting document ID of the processed items", e);
			throw new ImpExException(e);
		}
		return documentId;
	}
}
