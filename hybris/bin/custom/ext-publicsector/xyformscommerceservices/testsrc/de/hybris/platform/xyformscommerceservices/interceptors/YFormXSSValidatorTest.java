

package de.hybris.platform.xyformscommerceservices.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.testframework.HybrisJUnit4Test;
import de.hybris.platform.xyformsservices.model.YFormDataModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;


@UnitTest
public class YFormXSSValidatorTest extends HybrisJUnit4Test
{
	private static final String VALID_CONTENT = "<comment>valid</comment>";
	private static final String INVALID_CONTENT_SCRIPT = "<comment>&lt;script&gt;</comment>";

	@InjectMocks
	private YFormXSSValidator yformXssValidator;

	@Before
	public void setup()
	{
		this.yformXssValidator = new YFormXSSValidator();
		MockitoAnnotations.initMocks(this);

	}

	@Test(expected = InterceptorException.class)
	public void testIfValidateThrowsInterceptorException() throws InterceptorException
	{
		final YFormDataModel data = new YFormDataModel();
		data.setContent(INVALID_CONTENT_SCRIPT);
		final InterceptorContext interceptor = null;
		yformXssValidator.onValidate(data, interceptor);
	}

	@Test
	public void testIfValidateNotThrowsInterceptorException() throws InterceptorException
	{
		final YFormDataModel data = new YFormDataModel();
		data.setContent(VALID_CONTENT);
		final InterceptorContext interceptor = null;
		yformXssValidator.onValidate(data, interceptor);

	}
}
