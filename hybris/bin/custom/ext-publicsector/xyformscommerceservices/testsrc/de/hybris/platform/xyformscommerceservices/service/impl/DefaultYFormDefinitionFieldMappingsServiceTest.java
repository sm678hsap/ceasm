/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommerceservices.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.xyformscommerceservices.dao.YFormDefinitionFieldMappingsDao;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import junit.framework.Assert;


/**
 * Interface class for YFormDefinitionFieldMappings service
 */
@UnitTest
public class DefaultYFormDefinitionFieldMappingsServiceTest
{

	@InjectMocks
	private DefaultYFormDefinitionFieldMappingsService service;

	@Mock
	private YFormDefinitionFieldMappingsDao formDefinitionFieldMappingsDao;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		this.service = new DefaultYFormDefinitionFieldMappingsService();

	}

	@Test
	public void testGetAllYFormsMappings()
	{

		this.service.setFormDefinitionFieldMappingsDao(formDefinitionFieldMappingsDao);

		Mockito.when(formDefinitionFieldMappingsDao.getAllYFormsMappings())
				.thenReturn(new ArrayList<YFormDefinitionFieldMappingsModel>());

		final List<YFormDefinitionFieldMappingsModel> returnList = this.service.getAllYFormsMappings();

		Assert.assertNotNull(returnList);

	}

	@Test
	public void testGetMappingsForYForm()
	{
		this.service.setFormDefinitionFieldMappingsDao(formDefinitionFieldMappingsDao);

		final String appId = "TestApplicationId";
		final String formId = "TestFormId";
		Mockito.when(formDefinitionFieldMappingsDao.getMappingsForYForm(appId, formId))
				.thenReturn(new YFormDefinitionFieldMappingsModel());

		final YFormDefinitionFieldMappingsModel returnModel = this.service.getMappingsForYForm(appId, formId);

		Assert.assertNotNull(returnModel);
	}

}
