package de.hybris.platform.xyformscommerceservices.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.Utilities;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.form.YFormService;
import de.hybris.platform.xyformsservices.model.YFormDefinitionModel;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


@UnitTest
public class YFormXmlParserTest
{
	private static final Logger LOG = LoggerFactory.getLogger(YFormXmlParserTest.class);
	private static final String YFORM_DEFINITION_MOCK_XML = "resources" + File.separator + "xyformscommerceservices" +File.separator + "test" + File.separator +"TestYFormDefinitionMock.xml";
	private static final String YFORM_DATA_MOCK_XML = "resources"+ File.separator + "xyformscommerceservices"+ File.separator + "test" + File.separator + "TestYFormDataMock.xml";
	private static final String FORM1 = "form-1";

	@InjectMocks
	private YFormXmlParser formParser;

	@Mock
	private DocumentBuilderFactory builderFactory;

	@Mock
	private DocumentBuilder builder;

	@Mock
	private Document document;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private YFormService yformService;

	@Before
	public void setup()
	{
		formParser = new YFormXmlParser();
		MockitoAnnotations.initMocks(this);
		this.formParser.setCommonI18NService(commonI18NService);
	}

	@Test
	public void shouldReturnValidDocumentGivenValidXML() throws SAXException, IOException, ParserConfigurationException // NOSONAR
	{
		Mockito.when(builderFactory.newDocumentBuilder()).thenReturn(builder);
		Mockito.when(builder.parse((InputSource) Matchers.isNotNull())).thenReturn(document);
		final String xmlContent = "<test><item>something</item><test>";
		final Document parsedDocument = formParser.parseContent(xmlContent);
		assertEquals(document, parsedDocument);
	}

	@Test
	public void shouldReturnNullGivenEmptyXML()
	{
		final String xmlContent = "";
		final Document parsedDocument = formParser.parseContent(xmlContent);
		assertNull(parsedDocument);
	}

	@Test
	public void shouldReturnNullGivenXMLIsNull()
	{
		final String xmlContent = null;
		final Document parsedDocument = formParser.parseContent(xmlContent);
		assertNull(parsedDocument);
	}

	@Test
	public void shouldReturnNullWhenDocumentBuilderFactoryIsNull()
	{
		formParser.setBuilderFactory(null);
		final String xmlContent = "<test><item>something</item><test>";
		final Document parsedDocument = formParser.parseContent(xmlContent);
		assertNull(parsedDocument);
	}

	@Test
	public void shouldReturnValidMapForValidYFormDataAndYFormDefinition()
	{
		final String yFormDataContent = getTestDataFileContent(YFORM_DATA_MOCK_XML);

		final String yFormDefinitionDataContent = getTestDataFileContent(YFORM_DEFINITION_MOCK_XML);
		formParser.init();
		final LanguageModel language = new LanguageModel();
		language.setIsocode("en");
		Mockito.when(commonI18NService.getCurrentLanguage()).thenReturn(language);
		final Map<String, String> yFormDataMap = formParser.getYFormDataInMap(yFormDataContent, yFormDefinitionDataContent, FORM1);

		Assert.assertEquals("failed to get data for input field", yFormDataMap.get("form-1\\section-1\\control-1"), "Sample Text");
		Assert.assertEquals("failed to get data for text Area", yFormDataMap.get("form-1\\section-1\\control-3"),
				"Sample Text Area");
		Assert.assertEquals("failed to get data for email address text field", yFormDataMap.get("form-1\\section-1\\control-4"),
				"Sample@sample.com");
		Assert.assertEquals("failed to get data for us phone number text field", yFormDataMap.get("form-1\\section-1\\control-5"),
				"55555555");
		Assert.assertEquals("failed to get data for us  number text field", yFormDataMap.get("form-1\\section-1\\control-6"),
				"55555555");
		Assert.assertEquals("failed to get data for currency text field", yFormDataMap.get("form-1\\section-1\\control-7"),
				"1250.00");
		Assert.assertEquals("failed to get data for time field", yFormDataMap.get("form-1\\section-1\\control-10"), "10:30");
		Assert.assertEquals("failed to get data for date time drop field", yFormDataMap.get("form-1\\section-1\\control-12"),
				"2016-05-11T10:30");
		Assert.assertEquals("failed to get data for date drop field", yFormDataMap.get("form-1\\section-1\\control-13"),
				"2016-05-11");
		Assert.assertEquals("failed to get data for  dropdown field", yFormDataMap.get("form-1\\section-1\\control-14"),
				"Third choice");
		Assert.assertEquals("failed to get data for radio field", yFormDataMap.get("form-1\\section-1\\control-15"),
				"First choice");
		Assert.assertEquals("failed to get data for checkbox field", yFormDataMap.get("form-1\\section-1\\control-16"),
				"First choice,Third choice");
		Assert.assertEquals("failed to get data for scrollable list field", yFormDataMap.get("form-1\\section-1\\control-17"),
				"Second choice");
		Assert.assertEquals("failed to get data for boolean checkbox field", yFormDataMap.get("form-1\\section-1\\control-18"),
				"false");
		Assert.assertEquals("failed to get data for scrollable checkbox field", yFormDataMap.get("form-1\\section-1\\control-19"),
				"Second choice,First choice");
		Assert.assertEquals("failed to get data for dynamic dropdown", yFormDataMap.get("form-1\\section-1\\control-20"), "AU");
		Assert.assertEquals("failed to get data for Autocomplete", yFormDataMap.get("form-1\\section-1\\control-21"),
				"Autocomplete");
		Assert.assertEquals("failed to get file name for file attachment",
				yFormDataMap.get("form-1\\section-1\\control-24\\filename"), "sample.pdf");

		Assert.assertEquals("failed to get file size for file attachment", yFormDataMap.get("form-1\\section-1\\control-24\\size"),
				"199 KB");
		Assert.assertEquals("failed to get data for date text field", yFormDataMap.get("form-1\\section-1\\control-9"),
				"2016-05-11");
		Assert.assertEquals("failed to get indexed data for repeated grid input",
				yFormDataMap.get("form-1\\section-1\\grid-28\\control-30[1]"), "Repeated Grid Text Field");
		Assert.assertEquals("failed to get indexed data for repeated grid dropdown",
				yFormDataMap.get("form-1\\section-1\\grid-28\\control-31[1]"), "First choice");
		Assert.assertEquals("failed to get indexed data for repeated grid input",
				yFormDataMap.get("form-1\\section-1\\grid-28\\control-30[2]"), "Repeated Grid Text Field 2");
		Assert.assertEquals("failed to get indexed data for repeated grid dropdown",
				yFormDataMap.get("form-1\\section-1\\grid-28\\control-31[2]"), "Second choice");
	}

	@Test
	public void shouldReturnNullForInvalidYFormDefinition()
	{
		final String yFormDataContent = getTestDataFileContent(YFORM_DATA_MOCK_XML);
		final String yFormDefinitionDataContent = null;
		formParser.init();
		final LanguageModel language = new LanguageModel();
		language.setIsocode("en");
		Mockito.when(commonI18NService.getCurrentLanguage()).thenReturn(language);
		final Map<String, String> yFromDataMap = formParser.getYFormDataInMap(yFormDataContent, yFormDefinitionDataContent, FORM1);
		Assert.assertNull(yFromDataMap);
	}

	@Test
	public void shouldReturnNullForInvalidYFormData()
	{
		final String yFormDataContent = null;
		final String yFormDefinitionDataContent = getTestDataFileContent(YFORM_DEFINITION_MOCK_XML);
		formParser.init();
		final LanguageModel language = new LanguageModel();
		language.setIsocode("en");
		Mockito.when(commonI18NService.getCurrentLanguage()).thenReturn(language);
		final Map<String, String> yFromDataMap = formParser.getYFormDataInMap(yFormDataContent, yFormDefinitionDataContent, FORM1);
		Assert.assertNull(yFromDataMap);
	}

	@Test
	public void shouldReturnLanguageSpecificLabelsForGivenLanguages()
	{
		final String yFormDataContent = getTestDataFileContent(YFORM_DATA_MOCK_XML);
		final String yFormDefinitionDataContent = getTestDataFileContent(YFORM_DEFINITION_MOCK_XML);
		formParser.init();
		final LanguageModel language = new LanguageModel();
		language.setIsocode("de");
		Mockito.when(commonI18NService.getCurrentLanguage()).thenReturn(language);
		Map<String, String> yFormDataMap = formParser.getYFormDataInMap(yFormDataContent, yFormDefinitionDataContent, FORM1);
		Assert.assertEquals("failed to get data for  dropdown field", yFormDataMap.get("form-1\\section-1\\control-14"),
				"[DE] Third choice");
		language.setIsocode("fr");
		Mockito.when(commonI18NService.getCurrentLanguage()).thenReturn(language);
		yFormDataMap = formParser.getYFormDataInMap(yFormDataContent, yFormDefinitionDataContent, FORM1);
		Assert.assertEquals("failed able to get data for  dropdown field", yFormDataMap.get("form-1\\section-1\\control-14"),
				"[FR] Third choice");
	}

	@Test
	public void shouldReturnFirstAvailableLanguageSpecificLabelsIfCurrentLanguageIsNotAvailable()
	{
		final String yFormDataContent = getTestDataFileContent(YFORM_DATA_MOCK_XML);
		final String yFormDefinitionDataContent = getTestDataFileContent(YFORM_DEFINITION_MOCK_XML);
		formParser.init();
		final LanguageModel language = new LanguageModel();
		language.setIsocode("au");
		Mockito.when(commonI18NService.getCurrentLanguage()).thenReturn(language);
		final Map<String, String> yFormDataMap = formParser.getYFormDataInMap(yFormDataContent, yFormDefinitionDataContent, FORM1);
		Assert.assertEquals("failed to get data for  dropdown field", yFormDataMap.get("form-1\\section-1\\control-14"),
				"[DE] Third choice");
	}

	private String getTestDataFileContent(final String filename)
	{
		final File extensionDirectory = Utilities.getExtensionInfo("xyformscommerceservices").getExtensionDirectory();
		String filePath;
		String fileContent = null;
		File file;
		try
		{
			filePath = extensionDirectory.getCanonicalPath() + File.separator + filename;
			file = new File(filePath);
			fileContent = FileUtils.readFileToString(file);
			if (!file.exists())
			{
				Assert.fail("Test files does not exist");
			}

		}
		catch (final IOException e)
		{
			Assert.fail("Testfail does not exist");
			LOG.error("Exception Occurred : ", e);
		}
		return fileContent;
	}

	@Test
	public void testGetXPathsForYFormDefinitionWithCorrectXML() throws YFormServiceException, ParserConfigurationException
	{
		final String testXml = "<xf:instance id=\"fr-form-instance\" xxf:exclude-result-prefixes=\"#all\" xxf:index=\"id\"><form><section-report-graffiti><address-line1/><damaged-item/><label-address-header/><address-line2/><address-city/><address-postcode/><comment/><file-upload-control/><file-upload-label2/><update-me/><label-required-field-hint/></section-report-graffiti><section-update-me><is-user-logged-in>false</is-user-logged-in><label-logged-in-user/><label-unidentified-user/><user-name/><user-email/></section-update-me></form></xf:instance>";
		final String applicationId = "testApplicationId";
		final String formId = "testFormId";

		final YFormDefinitionModel yFormDefinitionModel = new YFormDefinitionModel();
		yFormDefinitionModel.setContent(testXml);
		yFormDefinitionModel.setApplicationId(applicationId);
		yFormDefinitionModel.setFormId(formId);

		formParser.setYformService(yformService);
		formParser.setBuilderFactory(DocumentBuilderFactory.newInstance());
		formParser.setXpath(XPathFactory.newInstance().newXPath());

		Mockito.when(yformService.getYFormDefinition(applicationId, formId)).thenReturn(yFormDefinitionModel);

		final Set<String> returnSet = formParser.getXPathsForYFormDefinition(applicationId, formId);

		Assert.assertTrue(!returnSet.isEmpty());

	}

	@Test
	public void testGetXPathsForYFormDefinitionWithEmptyXML() throws YFormServiceException
	{
		final String testXml = "";
		final String applicationId = "testApplicationId";
		final String formId = "testFormId";

		final YFormDefinitionModel yFormDefinitionModel = new YFormDefinitionModel();
		yFormDefinitionModel.setContent(testXml);
		yFormDefinitionModel.setApplicationId(applicationId);
		yFormDefinitionModel.setFormId(formId);

		formParser.setYformService(yformService);
		formParser.setBuilderFactory(DocumentBuilderFactory.newInstance());
		formParser.setXpath(XPathFactory.newInstance().newXPath());

		Mockito.when(yformService.getYFormDefinition(applicationId, formId)).thenReturn(yFormDefinitionModel);

		final Set<String> returnSet = formParser.getXPathsForYFormDefinition(applicationId, formId);

		Assert.assertTrue(returnSet.isEmpty());

	}
}
