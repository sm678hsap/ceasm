/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommerceservices.service.impl;

import de.hybris.platform.xyformscommerceservices.dao.YFormDefinitionFieldMappingsDao;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;
import de.hybris.platform.xyformscommerceservices.service.YFormDefinitionFieldMappingsService;

import java.util.List;


/**
 * Interface class for YFormDefinitionFieldMappings service
 */
public class DefaultYFormDefinitionFieldMappingsService implements YFormDefinitionFieldMappingsService
{

	private YFormDefinitionFieldMappingsDao formDefinitionFieldMappingsDao;

	@Override
	public List<YFormDefinitionFieldMappingsModel> getAllYFormsMappings()
	{
		return getFormDefinitionFieldMappingsDao().getAllYFormsMappings();
	}

	@Override
	public YFormDefinitionFieldMappingsModel getMappingsForYForm(final String applicationId, final String formId)
	{
		if (applicationId != null && formId != null)
		{
			return getFormDefinitionFieldMappingsDao().getMappingsForYForm(applicationId, formId);
		}
		return null;
	}

	public YFormDefinitionFieldMappingsDao getFormDefinitionFieldMappingsDao()
	{
		return formDefinitionFieldMappingsDao;
	}

	public void setFormDefinitionFieldMappingsDao(final YFormDefinitionFieldMappingsDao formDefinitionFieldMappingsDao)
	{
		this.formDefinitionFieldMappingsDao = formDefinitionFieldMappingsDao;
	}

}
