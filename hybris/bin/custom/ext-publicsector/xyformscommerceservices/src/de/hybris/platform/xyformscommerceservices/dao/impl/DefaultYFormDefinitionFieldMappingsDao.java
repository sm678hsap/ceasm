/*
 * [y] hybris Platform

 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommerceservices.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.xyformscommerceservices.dao.YFormDefinitionFieldMappingsDao;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Implementation class for YFormDefinitionFieldMappings DAO.
 */
public class DefaultYFormDefinitionFieldMappingsDao extends AbstractItemDao implements YFormDefinitionFieldMappingsDao
{
	protected static final String SELECTCLAUSE = "SELECT {" + YFormDefinitionFieldMappingsModel.PK + "} FROM {"
			+ YFormDefinitionFieldMappingsModel._TYPECODE + "! AS p} ";

	protected static final String YFORMFIELDMAPPINGS_QUERY_BY_FORM_AND_APP_ID = SELECTCLAUSE + "WHERE  { p:"
			+ YFormDefinitionFieldMappingsModel.APPLICATIONID + "} = ?applicationId AND { p:"
			+ YFormDefinitionFieldMappingsModel.FORMID + "} = ?formId";

	@Override
	public YFormDefinitionFieldMappingsModel getMappingsForYForm(final String applicationId, final String formId)
	{
		ServicesUtil.validateParameterNotNull(applicationId, "Application Id must not be null");
		ServicesUtil.validateParameterNotNull(formId, "Form Id must not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put("applicationId", applicationId);
		params.put("formId", formId);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(YFORMFIELDMAPPINGS_QUERY_BY_FORM_AND_APP_ID, params);
		final SearchResult<YFormDefinitionFieldMappingsModel> result = getFlexibleSearchService().search(fsq);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

	@Override
	public List<YFormDefinitionFieldMappingsModel> getAllYFormsMappings()
	{

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(SELECTCLAUSE);
		final SearchResult<YFormDefinitionFieldMappingsModel> result = getFlexibleSearchService().search(fsq);
		return result.getCount() > 0 ? result.getResult() : null;

	}

}
