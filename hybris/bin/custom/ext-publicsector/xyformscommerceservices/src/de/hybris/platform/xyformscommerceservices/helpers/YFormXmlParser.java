/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommerceservices.helpers;

import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.form.YFormService;
import de.hybris.platform.xyformsservices.model.YFormDefinitionModel;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * This is a helper class for the yForm used to parse the XML content of a object into a map of key/value.
 */
public class YFormXmlParser
{
	private static final Logger LOG = LoggerFactory.getLogger(YFormXmlParser.class);
	private static final String REPEATED_INDEX = "repeatedIndex";
	private static final String FILEUPLOAD_CONTROL_SIZE_ATTRIBUTE = "size";
	private static final String FILEUPLOAD_CONTROL_NAME_ATTRIBUTE = "filename";

	private static final String XPATH_EXPRESSION_TO_GET_SELECTED_VALUE_LABEL = "string(//resource[@lang='%s']/%s/item/value[text()='%s']/preceding-sibling::label)";
	private static final String XPATH_EXPRESSION_TO_CHECK_IF_ITS_DROPDOWN = "count(//*[local-name()='dropdown-select1' and @id='%s-control'] )";
	private static final String XPATH_EXPRESSION_TO_CHECK_IF_ITS_CHECKBOX = "count(//*[local-name()='select1' and @id='%s-control'] )";
	private static final String XPATH_EXPRESSION_TO_CHECK_IF_ITS_MULTI_CHECKBOX = "count(//*[local-name()='select' and @id='%s-control'] )";
	private static final String XPATH_EXPRESSION_TO_CHECK_IF_ITS_BOX_MULTI_CHECKBOX = "count(//*[local-name()='box-select' and @id='%s-control'] )";
	private static final String XPATH_EXPRESSION_TO_CHECK_IF_ITS_UPLOAD = "count(//*[local-name()='upload' and @id='%s-control'] )";
	private static final String XPATH_EXPRESSION_TO_CHECK_IF_ITS_ATTACHMENT = "count(//*[local-name()='attachment' and @id='%s-control'] )";
	private static final String XPATH_EXPRESSION_TO_CHECK_IF_MULTIPLE_ELEMENT_EXIST_WITH_SAME_NAME = "count(//%s)";
	private static final String XPATH_EXPRESSION_TO_CHECK_IF_GIVEN_LANGUAGE_LOCALIZATION_AVAILABLE = "count(//resource[@lang='%s'])";
	private static final String XPATH_EXPRESSION_TO_GET_FIRST_AVAILABLE_LANGUAGE = "string(//resource[1]/@lang)";
	private static final String XPATH_EXPRESSION_TO_GET_INSTANCE_FORM = "//instance/form";

	private DocumentBuilderFactory builderFactory;
	private XPath xpath;
	private CommonI18NService commonI18NService;
	private YFormService yformService;

	/**
	 * This method is called once the bean has been constructed and ensures that the members are correctly set up before the
	 * <parse> call is made.
	 */
	@PostConstruct
	public void init()
	{
		try
		{
			LOG.info("Initializing form XML parser.");
			DocumentBuilderFactory secureBuilderFactory = DocumentBuilderFactory.newInstance();
			secureBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			secureBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			this.setBuilderFactory(secureBuilderFactory);
			this.setXpath(XPathFactory.newInstance().newXPath());
		}
		catch (final ParserConfigurationException e)
		{
			LOG.error("Unable to set builder factory.", e);
		}
	}

	/**
	 * This method accepts a string which contains XML content and parses it to return a standard DOM Document which can
	 * then be used in a standard <EmbeddedFormXml> bean.
	 * 
	 * @param xmlContent
	 *           a string containing xml content
	 * @return an XML Dom <Document> which can be consumed by an <EmbeddedFormXml> bean. If parsing fails, or an empty
	 *         document is passed in, OR if the initialization has failed to produce a valid <DocumentBuilder>,then a NULL
	 *         is returned instead
	 */
	public Document parseContent(final String xmlContent)
	{
		if (xmlContent != null && xmlContent.length() > 0 && getBuilderFactory() != null)
		{
			DocumentBuilder documentBuilder;
			try
			{
				documentBuilder = getBuilderFactory().newDocumentBuilder();
				return documentBuilder.parse(new InputSource(new StringReader(xmlContent)));
			}
			catch (SAXException | IOException | ParserConfigurationException e)
			{
				LOG.error("Unable to parse xml content for : " + xmlContent, e);
			}
		}
		return null;
	}

	/**
	 * returns yForm data xml in key/value format.
	 * 
	 * @param yFormDataXml
	 * @param yFormDefinitionXml
	 * @param yFormId
	 * @return HashMap
	 */
	public Map<String, String> getYFormDataInMap(final String yFormDataXml, final String yFormDefinitionXml, final String yFormId)
	{
		final Document yFormData = parseContent(yFormDataXml);
		final Document yFormDefinition = parseContent(yFormDefinitionXml);
		if (yFormData != null && yFormDefinition != null)
		{
			final Map<String, String> yFormDataMap = new HashMap<>();
			final Node user = yFormData.getFirstChild();
			childNodes(user, "", yFormDefinition, yFormData, yFormDataMap, yFormId);
			return yFormDataMap;
		}
		return null;
	}

	/**
	 * Uses the supplied xpath expression to return a string value for the supplied expression where the expression refers
	 * only to a single element in the DOM. Please, note that the NOSONAR comment will avoid Sonar from raising a false
	 * positive for this line as a potential Xpath Injection. After analysis, the method is used only internally in the
	 * classes, hence, marking it as private as well.
	 * 
	 * @param xpathExpression
	 *           expression to use to evaluate the single value
	 * @param document
	 * @return the single value required
	 */
	private String evaluateXpathExpression(final String xpathExpression, final Document document)
	{
		String result = null;
		try
		{
			result = getXpath().compile(xpathExpression).evaluate(document); //NOSONAR
		}
		catch (final XPathExpressionException e)
		{
			LOG.error("Unable to resolve xpath to a single item for : " + xpathExpression, e);
		}
		return (result == null) ? "" : StringEscapeUtils.escapeXml(result);
	}

	private void childNodes(final Node currentNode, final String currentNodeXmlPath, final Document yFormDefinition,
			final Document yFormData, final Map<String, String> yFormDataMap, final String yFormId)
	{
		if (currentNode.hasChildNodes())
		{
			addIndexIfMultipleElementExistsWithSameName(currentNode, yFormData);
			final NodeList childNodes = currentNode.getChildNodes();

			for (int i = 0; i < childNodes.getLength(); i++)
			{
				final Node childNode = childNodes.item(i);
				updateYFormDataMapOnFieldType(currentNodeXmlPath, childNode, yFormDefinition, yFormDataMap, yFormId);

				if (childNode.hasChildNodes())
				{
					final String xmlTreePath = StringUtils.isNotEmpty(currentNodeXmlPath)
							? currentNodeXmlPath + "\\" + childNode.getNodeName()
							: childNode.getNodeName();
					childNodes(childNode, xmlTreePath, yFormDefinition, yFormData, yFormDataMap, yFormId);
				}
			}
		}
	}

	/**
	 * For the current child node in yForm xml, update yForm data in map based on the field type that current node represent.
	 *
	 * @param currentNodeXmlPath
	 * @param childNode
	 * @param yFormDefinition
	 * @param yFormDataMap
	 * @param yFormId
	 */
	private void updateYFormDataMapOnFieldType(final String currentNodeXmlPath, final Node childNode,
			final Document yFormDefinition, final Map<String, String> yFormDataMap, final String yFormId)
	{
		final String textContent = childNode.getTextContent();
		if (childNode.getNodeType() == Node.TEXT_NODE)
		{
			String xmlPath = (childNode.getParentNode().getUserData(REPEATED_INDEX) != null)
					? currentNodeXmlPath + "[" + childNode.getParentNode().getUserData(REPEATED_INDEX) + "]"
					: currentNodeXmlPath;
			xmlPath = yFormId + "\\" + xmlPath;

			final String nodeName = childNode.getParentNode().getNodeName();

			if (isDropDown(nodeName, yFormDefinition) || (isCheckBox(nodeName, yFormDefinition)))
			{
				yFormDataMap.put(xmlPath, getSelectedValueLabel(nodeName, textContent, yFormDefinition));
			}
			else if (isMultiCheckBox(nodeName, yFormDefinition) || isBoxedMultiCheckBox(nodeName, yFormDefinition))
			{
				yFormDataMap.put(xmlPath, getSelectedValuesLabel(nodeName, textContent, yFormDefinition));
			}
			else if (isFileUpload(nodeName, yFormDefinition))
			{
				putFileInformationOnMap(childNode.getParentNode(), yFormDataMap, xmlPath);
			}
			else
			{
				yFormDataMap.put(xmlPath, textContent);
			}
		}
	}

	private void putFileInformationOnMap(final Node node, final Map<String, String> yFormDataMap, final String xmlPath)
	{
		if (node.getNodeType() == Node.ELEMENT_NODE)
		{
			final Element element = (Element) node;
			yFormDataMap.put(xmlPath + "\\" + FILEUPLOAD_CONTROL_NAME_ATTRIBUTE,
					element.getAttribute(FILEUPLOAD_CONTROL_NAME_ATTRIBUTE));
			final String fileSize = FileUtils
					.byteCountToDisplaySize(Long.parseLong(element.getAttribute(FILEUPLOAD_CONTROL_SIZE_ATTRIBUTE)));
			yFormDataMap.put(xmlPath + "\\" + FILEUPLOAD_CONTROL_SIZE_ATTRIBUTE, fileSize);
		}
	}

	private void addIndexIfMultipleElementExistsWithSameName(final Node node, final Document yFormData)
	{
		if (node.getNodeType() == Node.ELEMENT_NODE)
		{
			if (isElementRepeated(node.getNodeName(), yFormData)
					&& (node.hasChildNodes() && node.getFirstChild().getNodeType() != Node.TEXT_NODE))
			{
				String index = "0";
				if (node.getPreviousSibling() != null && node.getNodeName().equals(node.getPreviousSibling().getNodeName())
						&& node.getPreviousSibling().getUserData(REPEATED_INDEX) != null)
				{
					final int previousRepeatedNodeIndex = Integer
							.parseInt((String) node.getPreviousSibling().getUserData(REPEATED_INDEX)) + 1;
					index = String.valueOf(previousRepeatedNodeIndex);
				}
				node.setUserData(REPEATED_INDEX, index, null);
			}
			else if (node.getParentNode() != null && node.getParentNode().getUserData(REPEATED_INDEX) != null)
			{
				node.setUserData(REPEATED_INDEX, node.getParentNode().getUserData(REPEATED_INDEX), null);
			}
		}
	}

	private String getSelectedValuesLabel(final String nodeName, final String selectedOption,
			final Document yFormDefinitiondocument)
	{

		final String language = getApplicableLanguageForLabel(yFormDefinitiondocument);
		String labelsForSelectedValue = "";
		final String[] selectedValuesArray = selectedOption.split(" ");

		for (int selectedValuesArrayIndex = 0; selectedValuesArrayIndex < selectedValuesArray.length; selectedValuesArrayIndex++)
		{
			final String getSelectedLabelXPathExpression = String.format(XPATH_EXPRESSION_TO_GET_SELECTED_VALUE_LABEL, language,
					nodeName, selectedValuesArray[selectedValuesArrayIndex]);
			labelsForSelectedValue += evaluateXpathExpression(getSelectedLabelXPathExpression, yFormDefinitiondocument);
			if (selectedValuesArrayIndex != selectedValuesArray.length - 1)
			{
				labelsForSelectedValue += ",";
			}
		}
		return labelsForSelectedValue;
	}

	private String getSelectedValueLabel(final String nodeName, final String selectedOption,
			final Document yFormDefinitionDocument)
	{
		final String language = getApplicableLanguageForLabel(yFormDefinitionDocument);
		final String getSelectedLabelXPathExpression = String.format(XPATH_EXPRESSION_TO_GET_SELECTED_VALUE_LABEL, language,
				nodeName, selectedOption);
		return evaluateXpathExpression(getSelectedLabelXPathExpression, yFormDefinitionDocument);
	}

	private boolean isElementRepeated(final String nodeName, final Document document)
	{
		final String checkCountXPathExpression = String.format(XPATH_EXPRESSION_TO_CHECK_IF_MULTIPLE_ELEMENT_EXIST_WITH_SAME_NAME,
				nodeName);
		final String totalCount = evaluateXpathExpression(checkCountXPathExpression, document);

		if (StringUtils.isNotBlank(totalCount))
		{
			return (Integer.parseInt(totalCount) > 1) ? true : false;
		}
		return false;
	}

	private boolean isBoxedMultiCheckBox(final String nodeName, final Document yFormDataDocument)
	{
		final String checkCountXPathExpression = String.format(XPATH_EXPRESSION_TO_CHECK_IF_ITS_BOX_MULTI_CHECKBOX, nodeName);
		return checkIfElementExists(checkCountXPathExpression, yFormDataDocument);
	}

	private boolean isMultiCheckBox(final String nodeName, final Document yFormDataDocument)
	{
		final String checkCountXPathExpression = String.format(XPATH_EXPRESSION_TO_CHECK_IF_ITS_MULTI_CHECKBOX, nodeName);
		return checkIfElementExists(checkCountXPathExpression, yFormDataDocument);
	}

	private boolean isCheckBox(final String nodeName, final Document yFormDataDocument)
	{
		final String checkCountXPathExpression = String.format(XPATH_EXPRESSION_TO_CHECK_IF_ITS_CHECKBOX, nodeName);
		return checkIfElementExists(checkCountXPathExpression, yFormDataDocument);
	}

	private boolean isDropDown(final String nodeName, final Document yFormDataDocument)
	{
		final String checkCountXPathExpression = String.format(XPATH_EXPRESSION_TO_CHECK_IF_ITS_DROPDOWN, nodeName);
		return checkIfElementExists(checkCountXPathExpression, yFormDataDocument);
	}

	private boolean isFileUpload(final String nodeName, final Document yFormDataDocument)
	{
		String checkCountXPathExpression = String.format(XPATH_EXPRESSION_TO_CHECK_IF_ITS_UPLOAD, nodeName);
		if (!checkIfElementExists(checkCountXPathExpression, yFormDataDocument))
		{
			checkCountXPathExpression = String.format(XPATH_EXPRESSION_TO_CHECK_IF_ITS_ATTACHMENT, nodeName);
			return checkIfElementExists(checkCountXPathExpression, yFormDataDocument);
		}
		else
		{
			return true;
		}
	}

	private String getApplicableLanguageForLabel(final Document yFormDefinitionDocument)
	{
		final String currentLanguage = getCommonI18NService().getCurrentLanguage().getIsocode();
		final String applicableLanguage = String.format(XPATH_EXPRESSION_TO_CHECK_IF_GIVEN_LANGUAGE_LOCALIZATION_AVAILABLE,
				currentLanguage);
		if (!checkIfElementExists(applicableLanguage, yFormDefinitionDocument))
		{
			return evaluateXpathExpression(XPATH_EXPRESSION_TO_GET_FIRST_AVAILABLE_LANGUAGE, yFormDefinitionDocument);
		}
		else
		{
			return currentLanguage;
		}
	}

	/**
	 * For a given application id and form id a set of XPath is returned.
	 * 
	 * @param applicationId
	 * @param formId
	 * @return Set<String>
	 */
	public Set<String> getXPathsForYFormDefinition(final String applicationId, final String formId)
	{
		final Set<String> entries = new HashSet<>();
		NodeList nodeList = null;
		try
		{
			final YFormDefinitionModel yFormDefinition = getYformService().getYFormDefinition(applicationId, formId);
			final Document yFormDefinitionContent = parseContent(yFormDefinition.getContent());

			nodeList = (NodeList) getXpath().compile(XPATH_EXPRESSION_TO_GET_INSTANCE_FORM).evaluate(yFormDefinitionContent,
					XPathConstants.NODESET);
			if (nodeList != null)
			{
				for (int i = 0; i < nodeList.getLength(); i++)
				{
					final Node child = nodeList.item(i);
					generateXPath(entries, "/" + child.getNodeName(), child);
				}
			}
		}
		catch (final XPathExpressionException | YFormServiceException e)
		{
			LOG.error("Error occurred while generating XPath for YForm : " + applicationId + ":" + formId, e);
		}
		return entries;
	}

	private static void generateXPath(final Set<String> entries, final String parentXPath, final Node child)
	{
		final NodeList childrens = child.getChildNodes();
		if (childrens != null)
		{
			for (int i = 0; i < childrens.getLength(); i++)
			{
				final Node children = childrens.item(i);
				if ((!children.hasChildNodes() || children.getFirstChild().getNextSibling() == null)
						&& children.getNodeType() == Node.ELEMENT_NODE)
				{
					entries.add(parentXPath + "/" + children.getNodeName());
				}
				else
				{
					generateXPath(entries, parentXPath + "/" + children.getNodeName(), children);
				}
			}
		}
	}

	private boolean checkIfElementExists(final String xpathExpression, final Document document)
	{
		return "0".equals(evaluateXpathExpression(xpathExpression, document)) ? false : true;
	}

	public DocumentBuilderFactory getBuilderFactory()
	{
		return builderFactory;
	}

	public void setBuilderFactory(final DocumentBuilderFactory builderFactory)
	{
		this.builderFactory = builderFactory;
	}

	public XPath getXpath()
	{
		return xpath;
	}

	public void setXpath(final XPath xpath)
	{
		this.xpath = xpath;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected YFormService getYformService()
	{
		return yformService;
	}

	@Required
	public void setYformService(final YFormService yformService)
	{
		this.yformService = yformService;
	}

}
