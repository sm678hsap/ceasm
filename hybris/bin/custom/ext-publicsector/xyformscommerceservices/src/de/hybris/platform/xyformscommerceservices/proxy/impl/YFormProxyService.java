/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */

package de.hybris.platform.xyformscommerceservices.proxy.impl;

import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.xyformsservices.proxy.impl.DefaultProxyService;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation for proxy service.
 */
public class YFormProxyService extends DefaultProxyService
{
	private static final Logger LOG = LoggerFactory.getLogger(YFormProxyService.class);

	protected CommonI18NService commonI18NService;

	/**
	 * Method to add the selected session language in the Orbeon URL. So the Orbeon forms will get loaded with
	 * appropriate localized values/labels.
	 */
	@Override
	public String rewriteURL(final String url, final boolean embeddable) throws MalformedURLException
	{
		LOG.debug("Got URL [" + url + "]");
		final URL u = new URL(url);

		final int index = u.getPath().indexOf(ORBEON_PREFIX);
		if (index < 0)
		{
			throw new MalformedURLException(ORBEON_PREFIX + " is not part of the URL");
		}

		// Take the URI part of the URL and remove the application context from
		// it.
		final String path = u.getPath().substring(index);
		LOG.debug("Call Proxy Service with path [" + path + "]");

		// We are assuming that orbeon is:
		// - Deployed as /orbeon
		// - /orbeon is part of the request uri.
		// - We don't need to go through an Apache Server

		URIBuilder builder = null;
		try
		{
			builder = new URIBuilder(this.orbeonAddress + path);
		}
		catch (final URISyntaxException e)
		{
			LOG.error("Error occurred while building the URL : " + e);
			throw new MalformedURLException(e.getMessage());
		}

		// if the original URl had a QueryString...
		builder.setQuery(u.getQuery());

		// If the form should be embeddable
		if (embeddable)
		{
			builder.addParameter("orbeon-embeddable", "true");
			builder.addParameter("fr-language", getCommonI18NService().getCurrentLanguage().getIsocode());
		}

		final String newURL = builder.toString();

		LOG.debug("Rewritten URL [" + newURL + "]");
		return newURL;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

}
