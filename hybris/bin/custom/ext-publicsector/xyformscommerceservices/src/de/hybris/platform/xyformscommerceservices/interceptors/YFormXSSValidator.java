/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */

package de.hybris.platform.xyformscommerceservices.interceptors;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.util.config.ConfigIntf;
import de.hybris.platform.xyformsservices.model.YFormDataModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Class of {@link YFormXSSFilterValidator}
 */
public class YFormXSSValidator implements ValidateInterceptor<YFormDataModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(YFormXSSValidator.class);

	/*
	 *
	 * @param yFormDataModel
	 *
	 * @param interceptorContext
	 *
	 * Method is used to check if yform data contains any malicious script code and return exception if it does
	 */
	@Override
	public void onValidate(final YFormDataModel yFormDataModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (StringUtils.isNotEmpty(yFormDataModel.getContent())
				&& hasXSSCharacters(yFormDataModel.getContent(), compilePatterns(getPatternDefinitions())))
		{
			throw new InterceptorException("given yformdata contains scripts");
		}
	}

	protected Map<String, String> getPatternDefinitions()
	{
		final Map<String, String> rules = new LinkedHashMap<>(getConfig().getParametersMatching("xss\\.filter\\.rule\\..*(?i)"));
		rules.putAll(getConfig().getParametersMatching("xyformscommerceservices\\.(xss\\.filter\\.rule\\..*(?i))", true));
		return rules;
	}

	private List<Pattern> compilePatterns(final Map<String, String> rules)
	{
		final List<Pattern> ret = new ArrayList<>();
		final Iterator<Entry<String, String>> rulesIterator = rules.entrySet().iterator();

		while (rulesIterator.hasNext())
		{
			final Entry<String, String> entry = rulesIterator.next();
			final String expr = entry.getValue();
			if (StringUtils.isNotBlank(expr))
			{
				try
				{
					ret.add(Pattern.compile(expr));
				}
				catch (final IllegalArgumentException ex)
				{
					LOG.error("error parsing xss rule " + entry, ex);
				}
			}
		}

		return ret;
	}

	private boolean hasXSSCharacters(final String value, final List<Pattern> xssPatternDefinitions)
	{
		return xssPatternDefinitions.stream().anyMatch(match -> match.matcher(value).find());
	}

	protected ConfigIntf getConfig()
	{
		return Registry.getCurrentTenant().getConfig();
	}
}
