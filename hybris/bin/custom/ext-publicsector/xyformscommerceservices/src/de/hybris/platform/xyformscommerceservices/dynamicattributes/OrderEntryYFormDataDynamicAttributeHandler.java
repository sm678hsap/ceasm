/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommerceservices.dynamicattributes;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.xyformsservices.form.YFormService;
import de.hybris.platform.xyformsservices.model.YFormDataModel;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * OrderEntryYFormDataDynamicAttributeHandler class to retrieve the YFormData by refId
 */
public class OrderEntryYFormDataDynamicAttributeHandler
		implements DynamicAttributeHandler<List<YFormDataModel>, AbstractOrderEntryModel>
{

	private YFormService yformService;

	@Override
	public List<YFormDataModel> get(final AbstractOrderEntryModel orderEntry)
	{

		final String yFormDataRefId = buildYFormDataRefId(orderEntry.getOrder().getCode(), orderEntry.getEntryNumber());

		return getYformService().getYFormDataByRefId(yFormDataRefId);
	}

	@Override
	public void set(final AbstractOrderEntryModel orderEntry, final List<YFormDataModel> yFormDataList)
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Builds up a refId for YFormData
	 *
	 * @param orderCode
	 * @param orderEntryNumber
	 * @return String YFormData refId
	 */
	protected String buildYFormDataRefId(final String orderCode, final Integer orderEntryNumber)
	{
		final StringBuilder refIdBuilder = new StringBuilder();

		refIdBuilder.append(orderCode);
		refIdBuilder.append('_');
		refIdBuilder.append(orderEntryNumber);

		return refIdBuilder.toString();
	}

	protected YFormService getYformService()
	{
		return yformService;
	}

	@Required
	public void setYformService(final YFormService yformService)
	{
		this.yformService = yformService;
	}

}
