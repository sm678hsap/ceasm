/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommerceservices.service;

import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;

import java.util.List;


/**
 * Interface class for YFormDefinitionFieldMappings service.
 */
public interface YFormDefinitionFieldMappingsService
{

	/**
	 * Get Fields mappings for a specific yForm
	 *
	 * @param applicationId
	 *            applicationId of the yform
	 * @param formId
	 *            formId of the yform
	 * @return YFormDefinitionFieldMappingsModel
	 */
	YFormDefinitionFieldMappingsModel getMappingsForYForm(final String applicationId, final String formId);

	/**
	 * Get all mappings for all YForms
	 *
	 * @return List<YFormDefinitionFieldMappingsModel>
	 */
	List<YFormDefinitionFieldMappingsModel> getAllYFormsMappings();

}
