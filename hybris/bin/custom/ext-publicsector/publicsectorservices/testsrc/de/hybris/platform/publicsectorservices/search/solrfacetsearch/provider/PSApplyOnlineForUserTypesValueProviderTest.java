/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.PropertyFieldValueProviderTestBase;
import de.hybris.platform.publicsectorservices.enums.PSApplyOnlineCustomerType;
import de.hybris.platform.publicsectorservices.model.PSApplyOnlineModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider.impl.PSApplyOnlineForUserTypesValueProvider;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit test for {@link PSApplyOnlineForUserTypesValueProvider}
 */
@UnitTest
public class PSApplyOnlineForUserTypesValueProviderTest extends PropertyFieldValueProviderTestBase
{
	private static final String TEST_PRODUCT_APPLY_ONLINE_PROP = "applyOnline";

	@InjectMocks
	private PSApplyOnlineForUserTypesValueProvider psApplyOnlineForUserTypesValueProvider;

	@Mock
	private IndexedProperty indexedProperty;

	@Mock
	private PSServiceProductModel psServiceProductModel;

	@Mock
	private PSApplyOnlineModel psApplyOnlineModel;

	@Mock
	private PSApplyOnlineCustomerType psApplyOnlineCustomerType;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		psApplyOnlineForUserTypesValueProvider = new PSApplyOnlineForUserTypesValueProvider();

		configure();
	}

	@Override
	protected String getPropertyName()
	{
		return TEST_PRODUCT_APPLY_ONLINE_PROP;
	}

	@Override
	protected void configure()
	{
		setPropertyFieldValueProvider(psApplyOnlineForUserTypesValueProvider);
		configureBase();

		((PSApplyOnlineForUserTypesValueProvider) getPropertyFieldValueProvider()).setI18nService(i18nService);
		((PSApplyOnlineForUserTypesValueProvider) getPropertyFieldValueProvider()).setLocaleService(localeService);
		((PSApplyOnlineForUserTypesValueProvider) getPropertyFieldValueProvider()).setFieldNameProvider(fieldNameProvider);
		((PSApplyOnlineForUserTypesValueProvider) getPropertyFieldValueProvider()).setModelService(modelService);

		Assert.assertTrue(getPropertyFieldValueProvider() instanceof FieldValueProvider);
	}

	@Test
	public void testWhenNoServiceProduct() throws FieldValueProviderException
	{
		Mockito.when(psApplyOnlineModel.getCustomerType()).thenReturn(psApplyOnlineCustomerType);
		final Collection<FieldValue> result = psApplyOnlineForUserTypesValueProvider.getFieldValues(indexConfig, indexedProperty,
				psServiceProductModel);

		Assert.assertNotNull(result);
		Assert.assertTrue(result.isEmpty());
	}

	@Test
	public void testGetFieldValues() throws FieldValueProviderException
	{
		final Collection<PSApplyOnlineModel> applyOnlineList = new ArrayList<PSApplyOnlineModel>();
		Mockito.when(psApplyOnlineModel.getCustomerType()).thenReturn(psApplyOnlineCustomerType);
		applyOnlineList.add(psApplyOnlineModel);

		Mockito.when(psServiceProductModel.getApplyOnline()).thenReturn(applyOnlineList);
		final Collection<FieldValue> result = psApplyOnlineForUserTypesValueProvider.getFieldValues(indexConfig, indexedProperty,
				psServiceProductModel);

		Assert.assertNotNull(result);
		Assert.assertFalse(result.isEmpty());
		Assert.assertEquals(1, result.size());
	}
}
