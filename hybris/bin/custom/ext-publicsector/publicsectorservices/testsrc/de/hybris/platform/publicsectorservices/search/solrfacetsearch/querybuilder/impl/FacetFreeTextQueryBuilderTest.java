/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorservices.search.solrfacetsearch.querybuilder.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of DefaultPSCartDaoTest.
 *
 */
@UnitTest
public class FacetFreeTextQueryBuilderTest
{

	@InjectMocks
	private FacetFreeTextQueryBuilder facetFreeTextQueryBuilder;



	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void testAddFreeTextQuery()
	{
		final SearchQuery searchQuery = Mockito.mock(SearchQuery.class);
		final IndexedProperty indexedProperty = Mockito.mock(IndexedProperty.class);

		Mockito.when(indexedProperty.getType()).thenReturn("text");
		facetFreeTextQueryBuilder.addFreeTextQuery(searchQuery, indexedProperty, "value", 1);
		Mockito.verify(searchQuery, Mockito.atLeast(1)).addRawQuery(Mockito.any(RawQuery.class));

		Mockito.when(indexedProperty.getType()).thenReturn("select");
		facetFreeTextQueryBuilder.addFreeTextQuery(searchQuery, indexedProperty, "value", 1);
		Mockito.verify(searchQuery, Mockito.atLeast(1)).addRawQuery(Mockito.any(RawQuery.class));

	}

}
