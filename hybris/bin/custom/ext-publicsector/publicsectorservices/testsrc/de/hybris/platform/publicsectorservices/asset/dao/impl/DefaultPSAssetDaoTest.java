/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.asset.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.asset.exception.PSAssetNotFoundException;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of DefaultPSCartDaoTest.
 *
 */
@UnitTest
public class DefaultPSAssetDaoTest
{

	@InjectMocks
	private DefaultPSAssetDao publicSectorAssetDao;

	@Mock
	private ModelService modelService;

	@Mock
	private FlexibleSearchService flexibleSearchService;


	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		publicSectorAssetDao.setModelService(modelService);
		publicSectorAssetDao.setFlexibleSearchService(flexibleSearchService);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetAssetsForUserNull()
	{
		Assert.assertNull(publicSectorAssetDao.getAssetsForUser(null));
	}

	@Test
	public void testGetAssetsForUser()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(PSAssetModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(publicSectorAssetDao.getAssetsForUser(user));

	}

	@Test
	public void testGetAssetByCode() throws PSAssetNotFoundException
	{
		final String assetCode = "TestAsset";
		final Integer count = new Integer(1);
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(PSAssetModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);
		Mockito.when(new Integer(searchResult.getCount())).thenReturn(count);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(publicSectorAssetDao.getAssetByCode(assetCode));

	}

	@Test
	public void testGetAssetAttributesByAssetCode() throws PSAssetNotFoundException
	{
		final String assetCode = "TestAsset";

		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(PSAssetAttributeModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(publicSectorAssetDao.getAssetAttributesByAssetCode(assetCode));

	}

	@Test
	public void testGetAssetAttributesMapping() throws PSAssetNotFoundException
	{
		final String assetCode = "TestAsset";
		final List<String> assetAttributeNames = new ArrayList<>();
		assetAttributeNames.add("TestName");

		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(PSAssetAttributeMappingModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(publicSectorAssetDao.getAssetAttributesMapping(assetCode, assetAttributeNames));

	}



}
