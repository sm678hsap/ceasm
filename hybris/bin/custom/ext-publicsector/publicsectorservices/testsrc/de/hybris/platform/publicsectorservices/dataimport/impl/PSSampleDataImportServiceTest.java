/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.dataimport.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit testing for {@link PSSampleDataImportService}
 */
@UnitTest
public class PSSampleDataImportServiceTest
{
	private static final String TEST_EXTENSION_NAME = "testExtension";
	private static final String TEST_PRODUCT_CATALOG_NAME = "test";
	private static final String TEST_CONTENT_CATALOG_NAME = "test";
	private static final String TEST_STORE_NAME = "test";

	@InjectMocks
	private PSSampleDataImportService psSampleDataImportService;

	@Mock
	private SetupImpexService setupImpexService;

	@Mock
	private AbstractSystemSetup setup;

	@Mock
	private SystemSetupContext ctx;

	@Mock
	private ValidationService validation;

	@Mock
	private SetupSyncJobService setupSyncJobService;

	@Mock
	private SetupSolrIndexerService setupSolrIndexerService;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private EventService eventService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		psSampleDataImportService = Mockito.spy(new PSSampleDataImportService());

		Mockito.doReturn(setupImpexService).when(psSampleDataImportService).getSetupImpexService();
		Mockito.doReturn(setupSyncJobService).when(psSampleDataImportService).getSetupSyncJobService();
		Mockito.doReturn(setupSolrIndexerService).when(psSampleDataImportService).getSetupSolrIndexerService();
		Mockito.doReturn(catalogVersionService).when(psSampleDataImportService).getCatalogVersionService();
		Mockito.doReturn(eventService).when(psSampleDataImportService).getEventService();

		Mockito.doReturn(TEST_EXTENSION_NAME).when(ctx).getExtensionName();

		Mockito.doReturn(Boolean.TRUE).when(psSampleDataImportService)
				.isExtensionLoaded(PSSampleDataImportService.CMS_COCKPIT_EXTENSION_NAME);
		Mockito.doReturn(Boolean.TRUE).when(psSampleDataImportService)
				.isExtensionLoaded(PSSampleDataImportService.PRODUCT_COCKPIT_EXTENSION_NAME);
		Mockito.doReturn(Boolean.TRUE).when(psSampleDataImportService)
				.isExtensionLoaded(PSSampleDataImportService.CS_COCKPIT_EXTENSION_NAME);
		Mockito.doReturn(Boolean.TRUE).when(psSampleDataImportService)
				.isExtensionLoaded(PSSampleDataImportService.REPORT_COCKPIT_EXTENSION_NAME);
		Mockito.doReturn(Boolean.TRUE).when(psSampleDataImportService)
				.isExtensionLoaded(PSSampleDataImportService.MCC_EXTENSION_NAME);
		Mockito.doReturn(Boolean.TRUE).when(psSampleDataImportService)
				.isExtensionLoaded(PSSampleDataImportService.BTG_EXTENSION_NAME);

		Mockito.doReturn(Boolean.TRUE).when(setup).getBooleanSystemSetupParameter(ctx,
				PSSampleDataImportService.IMPORT_SAMPLE_DATA);
		Mockito.doReturn(Boolean.TRUE).when(setup).getBooleanSystemSetupParameter(ctx,
				PSSampleDataImportService.ACTIVATE_SOLR_CRON_JOBS);
	}

	@Test
	public void testImportExecution()
	{
		final InOrder order = Mockito.inOrder(setupImpexService, setupSyncJobService, setupSolrIndexerService, validation);

		final ImportData sampleImportData = new ImportData();
		sampleImportData.setProductCatalogName(TEST_PRODUCT_CATALOG_NAME);
		sampleImportData.setContentCatalogNames(Arrays.asList(TEST_CONTENT_CATALOG_NAME));
		sampleImportData.setStoreNames(Arrays.asList(TEST_STORE_NAME));

		psSampleDataImportService.execute(setup, ctx, Arrays.asList(sampleImportData));

		verifyImportCommonData(order);
		verifyImportProductCatalog(order);
		verifyImportContentCatalog(order);
		verifyImportAllData(order);
	}

	private void verifyImportProductCatalog(final InOrder order)
	{
		// Load Bundle Templates
		order.verify(setupImpexService)
				.importImpexFile(String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/bundletemplates.impex",
						TEST_EXTENSION_NAME, TEST_PRODUCT_CATALOG_NAME), false);
		order.verify(setupImpexService).importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/bundletemplates-products.impex",
						TEST_EXTENSION_NAME, TEST_PRODUCT_CATALOG_NAME),
				false);
		order.verify(setupImpexService).importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/bundletemplates-selectioncriteria.impex",
						TEST_EXTENSION_NAME, TEST_PRODUCT_CATALOG_NAME),
				false);
		order.verify(setupImpexService).importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/bundletemplates-sampledata.impex",
						TEST_EXTENSION_NAME, TEST_PRODUCT_CATALOG_NAME),
				false);

		// Load datahub data classification
		order.verify(setupImpexService).importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/datahub-data-classification.impex",
						TEST_EXTENSION_NAME, TEST_PRODUCT_CATALOG_NAME),
				false);
	}

	private void verifyImportCommonData(final InOrder order)
	{
		order.verify(setupImpexService).importImpexFile("/publicsectorstore/import/sampledata/customer/customers.impex", true);
		order.verify(setupImpexService).importImpexFile(
				"/publicsectorstore/import/sampledata/productCatalogs/publicsectorProductCatalog/catalog-sync.impex", true);
		order.verify(setupImpexService).importImpexFile(
				"/publicsectorstore/import/sampledata/productCatalogs/publicsectorProductCatalog/yform-template-content.impex", true);
		order.verify(setupImpexService)
				.importImpexFile("/publicsectorstore/import/cockpits/productcockpit/productcockpit-access-rights.impex", true);
		order.verify(setupImpexService)
				.importImpexFile("/publicsectorstore/import/cockpits/cmscockpit/cmscockpit-access-rights.impex", true);
	}

	private void verifyImportContentCatalog(final InOrder order)
	{
		order.verify(setupImpexService)
				.importImpexFile(String.format("/%s/import/sampledata/contentCatalogs/%sContentCatalog/forms-content.impex",
						TEST_EXTENSION_NAME, TEST_CONTENT_CATALOG_NAME), true);
	}

	private void verifyImportAllData(final InOrder order)
	{
		order.verify(setupImpexService)
				.importImpexFile(String.format("/%s/import/sampledata/orders/orders.impex", TEST_EXTENSION_NAME), true);
	}
}
