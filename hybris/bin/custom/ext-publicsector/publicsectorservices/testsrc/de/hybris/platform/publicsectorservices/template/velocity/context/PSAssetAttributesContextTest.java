/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.template.velocity.context;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;
import de.hybris.platform.core.Registry;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * UnitTest class for DefaultPSAssetService
 *
 */
@UnitTest
public class PSAssetAttributesContextTest
{

	@InjectMocks
	private PSAssetAttributesContext psAssetAttributesContext;

	@Mock
	private BaseSiteModel baseSite;


	@BeforeClass
	public static void tenantStuff()
	{
		Registry.setCurrentTenantByID("junit");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		psAssetAttributesContext = new PSAssetAttributesContext();
	}

	@Test
	public void testInit()
	{
		final List<PSAssetAttributeMappingData> assetAttributeMappingDataList = new ArrayList<>();
		final PSAssetAttributeMappingData assetAttributeMappingData = new PSAssetAttributeMappingData();
		assetAttributeMappingData.setAssetType("TestAssetType");
		assetAttributeMappingData.setAttributeName("TestAttributeName");
		assetAttributeMappingData.setAttributeDisplayName("Test Attribute Display Name");

		final List<PSAssetAttributeData> psAssetAttributeDataList = new ArrayList<>();
		final PSAssetAttributeData psAssetAttributeData = new PSAssetAttributeData();
		psAssetAttributeData.setValue("Test Attribute Description");
		psAssetAttributeData.setName("TestAttributeName");

		psAssetAttributesContext.init(baseSite, psAssetAttributeDataList, assetAttributeMappingDataList);

		Assert.assertEquals(Boolean.TRUE, new Boolean(psAssetAttributesContext.internalContainsKey("assetType")));
		Assert.assertEquals(Boolean.TRUE, new Boolean(psAssetAttributesContext.internalContainsKey("assetAttributes")));

	}

}
