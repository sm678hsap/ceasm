/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.order.impl;

import static org.mockito.Mockito.doNothing;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.order.CartService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.xyformscommerceservices.strategies.YFormUserStrategy;

import java.util.Arrays;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of DefaultPublicSectorCartServiceTest.
 *
 */
@UnitTest
public class DefaultPSCartServiceTest
{

	@InjectMocks
	private DefaultPSCartService publicSectorCartService;

	@Mock
	private ModelService modelService;
	@Mock
	private SessionService sessionService;
	@Mock
	private CartService cartService;
	@Mock
	private CartFactory cartFactory;
	@Mock
	private CommerceCartService commerceCartService;
	@Mock
	private YFormUserStrategy defaultYFormUserStrategy;
	@Mock
	private TimeService timeService;
	@Mock
	private CommerceSaveCartStrategy commerceSaveCartStrategy;
	@Mock
	private EventService eventService;
	@Mock
	private UserService userService;
	@Mock
	private BaseStoreService baseStoreService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private BaseSiteService baseSiteService;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		publicSectorCartService = new DefaultPSCartService();
		publicSectorCartService.setModelService(modelService);
		publicSectorCartService.setCartService(cartService);
		publicSectorCartService.setCommerceCartService(commerceCartService);
		publicSectorCartService.setUserStrategy(defaultYFormUserStrategy);
		publicSectorCartService.setTimeService(timeService);
		publicSectorCartService.setEventService(eventService);
		publicSectorCartService.setCommerceSaveCartStrategy(commerceSaveCartStrategy);
		publicSectorCartService.setBaseStoreService(baseStoreService);
		publicSectorCartService.setCommonI18NService(commonI18NService);
		publicSectorCartService.setBaseSiteService(baseSiteService);
		publicSectorCartService.setUserService(userService);
	}

	@Test
	public void testUpdateCartOnYFormVersionUpdate()
	{
		final CartModel cartModel = new CartModel();
		final AddressModel addressModel = new AddressModel();
		final CountryModel countryModel = new CountryModel();
		final ZoneDeliveryModeModel zoneDeliveryModeModel = new ZoneDeliveryModeModel();
		final CreditCardPaymentInfoModel paymentInfoModel = new CreditCardPaymentInfoModel();
		final PSServiceProductModel serviceProduct = new PSServiceProductModel();

		final CartEntryModel entryModel1 = new CartEntryModel();
		final CartEntryModel entryModel2 = new CartEntryModel();

		entryModel1.setProduct(serviceProduct);

		paymentInfoModel.setSubscriptionId("subsId");
		addressModel.setCountry(countryModel);

		cartModel.setEntries(Arrays.<AbstractOrderEntryModel> asList(entryModel1, entryModel2));
		cartModel.setDeliveryAddress(addressModel);
		cartModel.setPaymentAddress(addressModel);
		cartModel.setDeliveryMode(zoneDeliveryModeModel);
		cartModel.setPaymentInfo(paymentInfoModel);

		Mockito.when(sessionService.getAttribute("cart")).thenReturn("cart");
		Mockito.when(cartFactory.createCart()).thenReturn(cartModel);

		doNothing().when(modelService).save(Mockito.any());
		doNothing().when(modelService).refresh(Mockito.any());

		publicSectorCartService.updateCartOnYFormVersionUpdate();

		Assert.assertNull(modelService.getSource(addressModel));
		Assert.assertNull(modelService.getSource(zoneDeliveryModeModel));
		Assert.assertNull(modelService.getSource(paymentInfoModel));

	}

	@Test
	public void testSetDraftCartDetails() throws CommerceSaveCartException
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.valueOf(false));
		publicSectorCartService.setDraftCartDetails("");
		Mockito.verify(commerceSaveCartStrategy, Mockito.atLeast(0)).saveCart(Mockito.any());

		final CartModel cart = new CartModel();
		cart.setCode("draft");
		final UserModel user = new UserModel();
		user.setUid("");
		cart.setUser(user);
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.valueOf(true));
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		Mockito.when(timeService.getCurrentTime()).thenReturn(new Date());

		Mockito.when(defaultYFormUserStrategy.getCurrentUserForCheckout()).thenReturn(Mockito.mock(CustomerModel.class));
		Mockito.when(userService.getCurrentUser()).thenReturn(Mockito.mock(UserModel.class));
		Mockito.doNothing().when(eventService).publishEvent(Mockito.any());
		Mockito.when(commerceSaveCartStrategy.saveCart(Mockito.any())).thenReturn(null);
		Mockito.when(userService.getCurrentUser()).thenReturn(new CustomerModel());
		publicSectorCartService.setDraftCartDetails("");
		Mockito.verify(commerceSaveCartStrategy, Mockito.atLeast(1)).saveCart(Mockito.any());
	}
}
