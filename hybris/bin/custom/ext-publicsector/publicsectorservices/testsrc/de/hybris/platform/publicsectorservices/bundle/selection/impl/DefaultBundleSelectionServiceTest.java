/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.bundle.selection.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundleservices.bundle.impl.DefaultBundleTemplateService;
import de.hybris.platform.configurablebundleservices.daos.BundleRuleDao;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.DisableProductBundleRuleModel;
import de.hybris.platform.configurablebundleservices.model.PickExactlyNBundleSelectionCriteriaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductAddOnModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit testing for Service Class {@link DefaultBundleSelectionService}
 */
@UnitTest
public class DefaultBundleSelectionServiceTest
{
	private static final String TEST_SERVICE_PRODUCT_CODE = "apply_dummy";
	private static final String TEST_SERVICE_PRODUCT_CODE1 = "psService1";
	private static final String TEST_BUNDLETEMPLATE_ID1 = "bundleTemplate1";
	private static final String TEST_SERVICE_ADDON_CODE2 = "psService2";
	private static final String TEST_BUNDLETEMPLATE_ID2 = "bundleTemplate2";
	private static final String TEST_PARENT_BUNDLETEMPLATE = "parentBundle";

	public class DefaultBundleSelectionServiceForTest extends DefaultBundleSelectionService
	{
		protected ModelService modelService;

		public ModelService getModelService()
		{
			return modelService;
		}

		public void setModelService(final ModelService modelService)
		{
			this.modelService = modelService;
		}
	}

	@InjectMocks
	private DefaultBundleSelectionServiceForTest defaultBundleSelectionService;

	@Mock
	private DefaultBundleTemplateService bundleTemplateService;

	@Mock
	private CartService cartService;

	@Mock
	private ProductService productService;
	@Mock
	private ModelService modelService;

	@Mock
	private BundleRuleDao disableProductBundleRuleDao;

	@Mock
	private Converter<ProductModel, ProductData> productConverter;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		defaultBundleSelectionService = new DefaultBundleSelectionServiceForTest();
		defaultBundleSelectionService.setBundleTemplateService(bundleTemplateService);
		defaultBundleSelectionService.setCartService(cartService);
		defaultBundleSelectionService.setProductService(productService);
		defaultBundleSelectionService.setDisableProductBundleRuleDao(disableProductBundleRuleDao);
		defaultBundleSelectionService.setModelService(modelService);
	}

	@Test
	public void testHasBundleAddOns()
	{
		final ProductModel serviceProduct = mock(ProductModel.class);
		Mockito.when(serviceProduct.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE);
		Mockito.when(productService.getProductForCode(TEST_SERVICE_PRODUCT_CODE)).thenReturn(serviceProduct);

		final List<ProductModel> serviceProducts = new ArrayList<ProductModel>();
		serviceProducts.add(serviceProduct);

		final BundleTemplateModel serviceProductBundleTemplate = mock(BundleTemplateModel.class);
		Mockito.when(serviceProductBundleTemplate.getProducts()).thenReturn(serviceProducts);

		final List<BundleTemplateModel> serviceProductTemplates = new ArrayList<BundleTemplateModel>();
		serviceProductTemplates.add(serviceProductBundleTemplate);

		Mockito.when(serviceProduct.getBundleTemplates()).thenReturn(serviceProductTemplates);

		Assert.assertTrue(defaultBundleSelectionService.hasBundleAddons(TEST_SERVICE_PRODUCT_CODE).booleanValue());
	}

	@Test
	public void testNoBundleAddons()
	{
		final ProductModel serviceProduct = mock(ProductModel.class);
		Mockito.when(serviceProduct.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE);
		Mockito.when(productService.getProductForCode(TEST_SERVICE_PRODUCT_CODE)).thenReturn(serviceProduct);

		Assert.assertFalse(defaultBundleSelectionService.hasBundleAddons(serviceProduct.getCode()).booleanValue());

		final List<BundleTemplateModel> serviceProductTemplates = new ArrayList();
		Mockito.when(serviceProduct.getBundleTemplates()).thenReturn(serviceProductTemplates);
		Assert.assertFalse(defaultBundleSelectionService.hasBundleAddons(serviceProduct.getCode()).booleanValue());
	}

	@Test
	public void testGetDisabledProductsMethod()
	{
		final ProductModel serviceProduct = mock(ProductModel.class);
		Mockito.when(serviceProduct.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE);
		Mockito.when(productService.getProductForCode(TEST_SERVICE_PRODUCT_CODE)).thenReturn(serviceProduct);
		final List<BundleTemplateModel> serviceProductTemplates = new ArrayList();
		serviceProductTemplates.add(mock(BundleTemplateModel.class));
		Mockito.when(serviceProduct.getBundleTemplates()).thenReturn(serviceProductTemplates);
		Mockito.when(bundleTemplateService.getBundleTemplatesByProduct(serviceProduct)).thenReturn(serviceProductTemplates);

		final ProductModel productModel1 = mock(ProductModel.class);
		Mockito.when(productModel1.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE);
		final ProductModel productModel2 = mock(ProductModel.class);
		Mockito.when(productModel2.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE1);

		final List<ProductModel> productModelList = new ArrayList<>();
		productModelList.add(productModel1);
		productModelList.add(productModel2);

		final DisableProductBundleRuleModel disableProductModel1 = mock(DisableProductBundleRuleModel.class);
		Mockito.doReturn(productModelList).when(disableProductModel1).getTargetProducts();
		final DisableProductBundleRuleModel disableProductModel2 = mock(DisableProductBundleRuleModel.class);
		final List<DisableProductBundleRuleModel> disableBundleRuleModelList = new ArrayList<>();
		disableBundleRuleModelList.add(disableProductModel1);
		disableBundleRuleModelList.add(disableProductModel2);
		Mockito.when(disableProductBundleRuleDao.findBundleRulesByProductAndRootTemplate(Mockito.any(), Mockito.any()))
				.thenReturn(disableBundleRuleModelList);

		Assert.assertEquals(CollectionUtils.size(defaultBundleSelectionService.getDisabledProducts(TEST_SERVICE_PRODUCT_CODE)), 1);
	}

	@Test
	public void testCartCleanupBeforeAddMethod() throws CommerceCartModificationException
	{
		final ProductModel serviceProduct = mock(PSServiceProductModel.class);
		Mockito.when(serviceProduct.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE);

		final ProductModel serviceProductAddOn = mock(PSServiceProductAddOnModel.class);
		Mockito.when(serviceProduct.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE1);

		final List<BundleTemplateModel> serviceProductTemplates = new ArrayList();
		final BundleTemplateModel bundleTemplate = mock(BundleTemplateModel.class);

		serviceProductTemplates.add(bundleTemplate);
		final List<ProductModel> requiredProductsList = new ArrayList();
		requiredProductsList.add(serviceProductAddOn);
		requiredProductsList.add(serviceProduct);
		Mockito.when(bundleTemplate.getProducts()).thenReturn(requiredProductsList);
		Mockito.when(bundleTemplate.getRequiredBundleTemplates()).thenReturn(serviceProductTemplates);

		Mockito.when(serviceProduct.getBundleTemplates()).thenReturn(serviceProductTemplates);
		Mockito.when(bundleTemplateService.getBundleTemplatesByProduct(serviceProduct)).thenReturn(serviceProductTemplates);

		final CartModel cart = Mockito.mock(CartModel.class);
		final List<AbstractOrderEntryModel> entries = new ArrayList();
		final AbstractOrderEntryModel orderEntry = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(orderEntry.getProduct()).thenReturn(serviceProduct);
		Mockito.when(orderEntry.getEntryNumber()).thenReturn(Integer.valueOf(1));
		entries.add(orderEntry);
		final AbstractOrderEntryModel orderEntry2 = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(orderEntry2.getEntryNumber()).thenReturn(Integer.valueOf(0));
		entries.add(orderEntry2);
		final AbstractOrderEntryModel orderEntry3 = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(orderEntry3.getEntryNumber()).thenReturn(null);
		entries.add(orderEntry3);
		final AbstractOrderEntryModel orderEntry4 = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(orderEntry4.getProduct()).thenReturn(serviceProductAddOn);
		Mockito.when(orderEntry4.getEntryNumber()).thenReturn(Integer.valueOf(1));
		entries.add(orderEntry4);

		Mockito.when(cart.getEntries()).thenReturn(entries);

		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		Mockito.doNothing().when(modelService).saveAll(Mockito.anyList());
		Mockito.doNothing().when(modelService).removeAll(Mockito.anyList());
		Mockito.doNothing().when(modelService).refresh(cart);

		defaultBundleSelectionService.cartCleanupBeforeAdd(TEST_SERVICE_PRODUCT_CODE);
	}


	@Test
	public void testIsEligibleToAddBundleAddon()
	{
		final ProductModel serviceProductAddOn = mock(PSServiceProductAddOnModel.class);
		Mockito.when(serviceProductAddOn.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE1);

		final CartModel cart = Mockito.mock(CartModel.class);
		final List<AbstractOrderEntryModel> entries = new ArrayList();
		Mockito.when(cart.getEntries()).thenReturn(entries);

		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		Assert.assertTrue(defaultBundleSelectionService.isEligibleToAddBundleAddon(TEST_SERVICE_PRODUCT_CODE1));
		final AbstractOrderEntryModel orderEntry = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(orderEntry.getProduct()).thenReturn(serviceProductAddOn);
		Mockito.when(orderEntry.getEntryNumber()).thenReturn(Integer.valueOf(1));
		entries.add(orderEntry);
		final AbstractOrderEntryModel orderEntry2 = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(orderEntry2.getEntryNumber()).thenReturn(Integer.valueOf(0));
		entries.add(orderEntry2);
		final AbstractOrderEntryModel orderEntry3 = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(orderEntry3.getEntryNumber()).thenReturn(null);
		entries.add(orderEntry3);
		final AbstractOrderEntryModel orderEntry4 = Mockito.mock(AbstractOrderEntryModel.class);
		Mockito.when(orderEntry4.getProduct()).thenReturn(serviceProductAddOn);
		Mockito.when(orderEntry4.getEntryNumber()).thenReturn(Integer.valueOf(1));
		entries.add(orderEntry4);

		Mockito.when(cart.getEntries()).thenReturn(entries);

		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		Assert.assertFalse(defaultBundleSelectionService.isEligibleToAddBundleAddon(TEST_SERVICE_PRODUCT_CODE1));

	}

	@Test
	public void getParentBundleTemplateByProductTest()
	{
		final ProductModel serviceProduct = mock(ProductModel.class);
		Mockito.when(serviceProduct.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE);
		Mockito.when(productService.getProductForCode(TEST_SERVICE_PRODUCT_CODE)).thenReturn(serviceProduct);

		final List<ProductModel> serviceProducts = new ArrayList<ProductModel>();
		serviceProducts.add(serviceProduct);

		final BundleTemplateModel serviceProductBundleTemplate = mock(BundleTemplateModel.class);
		Mockito.when(serviceProductBundleTemplate.getProducts()).thenReturn(serviceProducts);

		final List<BundleTemplateModel> serviceProductTemplates = new ArrayList<BundleTemplateModel>();
		serviceProductTemplates.add(serviceProductBundleTemplate);

		final BundleTemplateModel parentBundleTemplate = mock(BundleTemplateModel.class);
		Mockito.when(parentBundleTemplate.getChildTemplates()).thenReturn(serviceProductTemplates);

		Mockito.when(serviceProductBundleTemplate.getParentTemplate()).thenReturn(parentBundleTemplate);

		Mockito.when(bundleTemplateService.getBundleTemplatesByProduct(serviceProduct)).thenReturn(serviceProductTemplates);

		assertEquals(parentBundleTemplate,
				defaultBundleSelectionService.getParentBundleTemplateByProduct(TEST_SERVICE_PRODUCT_CODE));

	}

	@Test
	public void getNullParentBundleTemplateByProductTest()
	{
		final BundleTemplateModel parentBundleTemplate = defaultBundleSelectionService.getParentBundleTemplateByProduct(null);
		Assert.assertNull(parentBundleTemplate);
	}

	@Test
	public void getChildBundlesNotInCartTest()
	{
		//Create a service product1
		final PSServiceProductModel serviceProduct1 = mock(PSServiceProductModel.class);
		Mockito.when(serviceProduct1.getCode()).thenReturn(TEST_SERVICE_PRODUCT_CODE1);
		Mockito.when(productService.getProductForCode(TEST_SERVICE_PRODUCT_CODE1)).thenReturn(serviceProduct1);

		final List<ProductModel> serviceProducts = new ArrayList<ProductModel>();
		serviceProducts.add(serviceProduct1);

		//create a bundle with this product
		final BundleTemplateModel serviceProductBundleTemplate1 = mock(BundleTemplateModel.class);
		Mockito.when(serviceProductBundleTemplate1.getId()).thenReturn(TEST_BUNDLETEMPLATE_ID1);
		Mockito.when(serviceProductBundleTemplate1.getProducts()).thenReturn(serviceProducts);

		//Create a service add on
		final PSServiceProductAddOnModel serviceProductAddon2 = mock(PSServiceProductAddOnModel.class);
		Mockito.when(serviceProductAddon2.getCode()).thenReturn(TEST_SERVICE_ADDON_CODE2);
		Mockito.when(productService.getProductForCode(TEST_SERVICE_ADDON_CODE2)).thenReturn(serviceProductAddon2);

		final List<ProductModel> serviceProductAddons = new ArrayList<ProductModel>();
		serviceProductAddons.add(serviceProductAddon2);

		//create a bundle with this addon
		final BundleTemplateModel serviceProductAddOnBundleTemplate2 = mock(BundleTemplateModel.class);
		Mockito.when(serviceProductAddOnBundleTemplate2.getId()).thenReturn(TEST_BUNDLETEMPLATE_ID2);
		Mockito.when(serviceProductAddOnBundleTemplate2.getProducts()).thenReturn(serviceProductAddons);

		//child bundles list
		final List<BundleTemplateModel> childBundles = new ArrayList<BundleTemplateModel>();
		childBundles.add(serviceProductBundleTemplate1);
		childBundles.add(serviceProductAddOnBundleTemplate2);

		//create a parent bundle
		final BundleTemplateModel parentBundle = mock(BundleTemplateModel.class);
		Mockito.when(parentBundle.getId()).thenReturn(TEST_PARENT_BUNDLETEMPLATE);
		Mockito.when(parentBundle.getChildTemplates()).thenReturn(childBundles);

		Mockito.when(serviceProductBundleTemplate1.getParentTemplate()).thenReturn(parentBundle);
		Mockito.when(serviceProductAddOnBundleTemplate2.getParentTemplate()).thenReturn(parentBundle);

		final PickExactlyNBundleSelectionCriteriaModel pickexactly1 = mock(PickExactlyNBundleSelectionCriteriaModel.class);
		Mockito.when((pickexactly1).getN()).thenReturn(new Integer(1));
		Mockito.when(serviceProductBundleTemplate1.getBundleSelectionCriteria()).thenReturn(pickexactly1);
		Mockito.when(serviceProductAddOnBundleTemplate2.getBundleSelectionCriteria()).thenReturn(pickexactly1);

		final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
		Mockito.when(entry.getProduct()).thenReturn(serviceProduct1);

		final List<AbstractOrderEntryModel> entries = new ArrayList<AbstractOrderEntryModel>();
		entries.add(entry);

		final CartModel cartModel = new CartModel();
		cartModel.setEntries(entries);

		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);

		Mockito.when(productService.getProductForCode(TEST_SERVICE_PRODUCT_CODE1)).thenReturn(serviceProduct1);
		Mockito.when(bundleTemplateService.getBundleTemplatesByProduct(serviceProduct1))
				.thenReturn(Arrays.<BundleTemplateModel> asList(serviceProductBundleTemplate1));

		final List<BundleTemplateModel> bundlesNotInCart = defaultBundleSelectionService
				.getChildBundlesNotInCart(TEST_SERVICE_PRODUCT_CODE1);

		Assert.assertEquals(1, bundlesNotInCart.size());
	}

	@Test
	public void getNullChildBundlesNotInCartTest()
	{
		final List<BundleTemplateModel> bundlesNotInCart = defaultBundleSelectionService.getChildBundlesNotInCart(null);
		Assert.assertNotNull(bundlesNotInCart);
		Assert.assertEquals(0, bundlesNotInCart.size());
	}
}
