/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.strategies.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.delivery.dao.CountryZoneDeliveryModeDao;
import de.hybris.platform.commerceservices.delivery.dao.PickupDeliveryModeDao;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.product.PSProductService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * This is test class for PublicSectorDeliveryModeLookupStrategy.
 */
@UnitTest
public class PSDeliveryModeLookupStrategyTest
{
	@InjectMocks
	private PSDeliveryModeLookupStrategy strategy;

	@Mock
	private CountryZoneDeliveryModeDao countryZoneDeliveryModeDao;

	@Mock
	private AbstractOrderModel abstractOrderModel;

	@Mock
	private PickupDeliveryModeDao pickupDeliveryModeDao;

	@Mock
	private PSServiceProductModel productModel;

	@Mock
	private PSProductService publicSectorProductService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		strategy = new PSDeliveryModeLookupStrategy();
		strategy.setCountryZoneDeliveryModeDao(countryZoneDeliveryModeDao);
		strategy.setPickupDeliveryModeDao(pickupDeliveryModeDao);
		strategy.setPsProductService(publicSectorProductService);
	}

	@Test
	public void testGetDeliveryModesForOrderFromServiceProduct()
	{
		final Collection<DeliveryModeModel> deliveryModels = new ArrayList<DeliveryModeModel>();
		final AddressModel addressModel = mock(AddressModel.class);
		final CountryModel deliveryCountry = mock(CountryModel.class);
		final CurrencyModel currency = mock(CurrencyModel.class);
		final DeliveryModeModel deliveryModeModel1 = mock(DeliveryModeModel.class);
		final AbstractOrderEntryModel entryModel = mock(AbstractOrderEntryModel.class);

		deliveryModels.add(deliveryModeModel1);

		given(publicSectorProductService.getServiceProductFromSessionCart()).willReturn(productModel);
		given(productModel.getDeliveryModes()).willReturn(new HashSet<DeliveryModeModel>(deliveryModels));
		given(abstractOrderModel.getNet()).willReturn(Boolean.FALSE);
		given(abstractOrderModel.getDeliveryAddress()).willReturn(addressModel);
		given(addressModel.getCountry()).willReturn(deliveryCountry);
		given(abstractOrderModel.getCurrency()).willReturn(currency);
		given(entryModel.getProduct()).willReturn(productModel);
		given(abstractOrderModel.getEntries()).willReturn(Collections.singletonList(entryModel));
		given(entryModel.getDeliveryPointOfService()).willReturn(null);
		given(countryZoneDeliveryModeDao.findDeliveryModes((AbstractOrderModel) Mockito.anyObject())).willReturn(deliveryModels);

		final List<DeliveryModeModel> result = strategy.getSelectableDeliveryModesForOrder(abstractOrderModel);

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals(deliveryModeModel1, result.get(0));
	}

	@Test
	public void testGetDeliveryModesForOrderFromBundleProduct()
	{
		final Collection<DeliveryModeModel> deliveryModels = new ArrayList<DeliveryModeModel>();
		final AddressModel addressModel = mock(AddressModel.class);
		final CountryModel deliveryCountry = mock(CountryModel.class);
		final CurrencyModel currency = mock(CurrencyModel.class);
		final DeliveryModeModel deliveryModeModel1 = mock(DeliveryModeModel.class);
		final AbstractOrderEntryModel entryModel = mock(AbstractOrderEntryModel.class);
		final PSServiceProductModel serviceProductModel = mock(PSServiceProductModel.class);
		final AbstractOrderEntryModel serviceEntryModel = mock(AbstractOrderEntryModel.class);

		deliveryModels.add(deliveryModeModel1);

		given(serviceProductModel.getDeliveryModes()).willReturn(null);
		given(publicSectorProductService.getServiceProductFromSessionCart()).willReturn(serviceProductModel);
		given(productModel.getCode()).willReturn("bundle");
		given(serviceProductModel.getCode()).willReturn("serviceProduct");
		given(productModel.getDeliveryModes()).willReturn(new HashSet<DeliveryModeModel>(deliveryModels));
		given(abstractOrderModel.getNet()).willReturn(Boolean.FALSE);
		given(abstractOrderModel.getDeliveryAddress()).willReturn(addressModel);
		given(addressModel.getCountry()).willReturn(deliveryCountry);
		given(abstractOrderModel.getCurrency()).willReturn(currency);
		given(entryModel.getProduct()).willReturn(productModel);
		given(serviceEntryModel.getProduct()).willReturn(serviceProductModel);
		given(abstractOrderModel.getEntries()).willReturn(Collections.singletonList(entryModel));
		given(entryModel.getDeliveryPointOfService()).willReturn(null);
		given(countryZoneDeliveryModeDao.findDeliveryModes((AbstractOrderModel) Mockito.anyObject())).willReturn(deliveryModels);

		final List<DeliveryModeModel> result = strategy.getSelectableDeliveryModesForOrder(abstractOrderModel);

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals(deliveryModeModel1, result.get(0));
	}

	@Test
	public void testGetDeliveryModesForPickUpOnlyOrder()
	{
		final AbstractOrderEntryModel entryModel = mock(AbstractOrderEntryModel.class);
		final PointOfServiceModel pos1 = mock(PointOfServiceModel.class);
		final List<DeliveryModeModel> deliveryModels = new ArrayList<DeliveryModeModel>();
		final DeliveryModeModel deliveryModeModel1 = mock(DeliveryModeModel.class);

		deliveryModels.add(deliveryModeModel1);

		given(entryModel.getProduct()).willReturn(productModel);
		given(abstractOrderModel.getEntries()).willReturn(Collections.singletonList(entryModel));
		given(entryModel.getDeliveryPointOfService()).willReturn(pos1);
		given(pickupDeliveryModeDao.findPickupDeliveryModesForAbstractOrder(abstractOrderModel)).willReturn(deliveryModels);

	}
}
