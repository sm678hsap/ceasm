/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for PSRelationshipGetUserStrategyTest
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSRelationshipYFormUserStrategyTest
{

	@Mock
	private CartService cartService;

	@Mock
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	@InjectMocks
	private final PSRelationshipYFormUserStrategy rsDefaultGetUserStrategy = new PSRelationshipYFormUserStrategy();

	private CartModel cart;

	private CustomerModel user;

	private CustomerModel userInContext;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		cart = new CartModel();
		user = new CustomerModel();
		userInContext = new CustomerModel();

		cart.setUser(user);
		rsDefaultGetUserStrategy.setCartService(cartService);
		rsDefaultGetUserStrategy.setCheckoutCustomerStrategy(checkoutCustomerStrategy);

	}

	@Test
	public void testGetCurrentUserForCheckout()
	{

		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		Mockito.when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(user);

		Assert.assertEquals(rsDefaultGetUserStrategy.getCurrentUserForCheckout(), user);

	}


	@Test
	public void testGetCurrentUserForCheckoutForUserInContext()
	{
		cart.setUserInContext(userInContext);

		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.valueOf(true));
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);

		Assert.assertEquals(rsDefaultGetUserStrategy.getCurrentUserForCheckout(), userInContext);

	}


}
