/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.publicsectorservices.enums.PSDepartment;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class PSDepartmentFacetValueDisplayNameProviderTest
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSDepartmentFacetValueDisplayNameProviderTest
{

	private static final String TEST_FACET_VALUE = "test";
	private static final String LOCALIZED_FACET_VALUE = "loc-test";
	private static final String LANGUAGE_CODE = "es";

	@InjectMocks
	private final PSDepartmentFacetValueDisplayNameProvider provider = new PSDepartmentFacetValueDisplayNameProvider();

	@Mock
	private SearchQuery searchQuery;

	@Mock
	private IndexedProperty indexedProperty;

	@Mock
	private EnumerationService enumerationService;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private I18NService i18NService;

	private Locale locale;

	@Mock
	private PSDepartment psDepartmentEnum;

	@Mock
	private LanguageModel language;

	@Test
	public void testGetDisplayNameWhenFacetValueIsNull()
	{
		Assert.assertEquals(StringUtils.EMPTY, provider.getDisplayName(searchQuery, indexedProperty, null));
	}

	@Test
	public void testGetDisplayNameWhenFacetValueExistsAndLocalized()
	{
		Mockito.when(enumerationService.getEnumerationValue(PSDepartment.class, TEST_FACET_VALUE)).thenReturn(psDepartmentEnum);
		Mockito.when(i18NService.getCurrentLocale()).thenReturn(locale);
		Mockito.when(enumerationService.getEnumerationName(psDepartmentEnum, locale)).thenReturn(LOCALIZED_FACET_VALUE);

		Assert.assertEquals(LOCALIZED_FACET_VALUE, provider.getDisplayName(searchQuery, indexedProperty, TEST_FACET_VALUE));
	}

	@Test
	public void testGetDisplayNameWhenFacetValueExistsAndNotLocalized()
	{
		Mockito.when(enumerationService.getEnumerationValue(PSDepartment.class, TEST_FACET_VALUE)).thenReturn(psDepartmentEnum);
		Mockito.when(i18NService.getCurrentLocale()).thenReturn(locale);
		Mockito.when(enumerationService.getEnumerationName(psDepartmentEnum, locale)).thenReturn(null);

		Assert.assertEquals(TEST_FACET_VALUE, provider.getDisplayName(searchQuery, indexedProperty, TEST_FACET_VALUE));
	}

	@Test
	public void testGetDisplayNameWhenFacetValueExistsAndLocalizedWithNullCurrentLocale()
	{
		Mockito.when(enumerationService.getEnumerationValue(PSDepartment.class, TEST_FACET_VALUE)).thenReturn(psDepartmentEnum);
		Mockito.when(i18NService.getCurrentLocale()).thenReturn(null);
		Mockito.when(commonI18NService.getLanguage(LANGUAGE_CODE)).thenReturn(language);
		Mockito.when(commonI18NService.getLocaleForLanguage(language)).thenReturn(locale);
		Mockito.when(enumerationService.getEnumerationName(psDepartmentEnum, locale)).thenReturn(LOCALIZED_FACET_VALUE);

		Assert.assertEquals(LOCALIZED_FACET_VALUE, provider.getDisplayName(searchQuery, indexedProperty, TEST_FACET_VALUE));
	}

}
