/**
 *
 */
package de.hybris.platform.publicsectorservices.template.impl;

import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.publicsectorservices.template.context.impl.DefaultPSYFormTemplateContextFactory;
import de.hybris.platform.publicservices.template.impl.DefaultPSYFormTemplateService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.xyformscommerceservices.helpers.YFormXmlParser;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.configuration.Configuration;
import org.apache.velocity.VelocityContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * DefaultPublicSectorYFormTemplateServiceTest
 */
@UnitTest
public class DefaultPSYFormTemplateServiceTest
{
	private static final String RENDERER = "_renderer";
	private static final String SAMPLE_GRAFFITI = "sampleGraffiti";
	private static final String SAMPLE_FORMID = "sampleFormId";

	private static final String xmlContent = "<form><section-report-graffiti>"
			+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
			+ "<address-line2/><address-city/><address-postcode/><comment/>"
			+ "<update-me/><label-required-field-hint/></section-report-graffiti>"
			+ "<section-update-me><is-user-logged-in>false</is-user-logged-in>" + "<label-logged-in-user/><label-unidentified-user/>"
			+ "<user-name/><user-email/></section-update-me></form>";

	@InjectMocks
	private DefaultPSYFormTemplateService publicSectorTemplateService;

	@Mock
	private RendererService rendererService;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;

	@Mock
	private DefaultPSYFormTemplateContextFactory publicSectorTemplateContextFactory;

	@Mock
	private YFormXmlParser yFormXmlParser;

	@Mock
	private StringWriter renderedBody;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		publicSectorTemplateService = new DefaultPSYFormTemplateService();
		publicSectorTemplateService.setRendererService(rendererService);
		publicSectorTemplateService.setConfigurationService(configurationService);
		publicSectorTemplateService.setyFormXmlParser(yFormXmlParser);
		publicSectorTemplateService.setPsYFormTemplateContextFactory(publicSectorTemplateContextFactory);

		Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
		Mockito.when(configuration.getString("checkout.reviewpage.custom.template.renderer.format"))
				.thenReturn("{productCode}_renderer");
	}

	@Test
	public void shouldReturnFalseIfNoTemplateIsPresentWithProductCode()
	{
		final String productCode = "sampleProduct";
		Mockito.when(rendererService.getRendererTemplateForCode(productCode)).thenThrow(UnknownIdentifierException.class);
		Assert.assertFalse(publicSectorTemplateService.isServiceHasTemplate(productCode));
	}

	@Test
	public void shouldReturnTrueIfTemplateIsPresentWithProductCode()
	{
		final String productCode = "sampleProduct";
		final RendererTemplateModel rendererTemplateModel = Mockito.mock(RendererTemplateModel.class);
		rendererTemplateModel.setCode(productCode);

		Mockito.when(rendererService.getRendererTemplateForCode(productCode + RENDERER)).thenReturn(rendererTemplateModel);
		Assert.assertTrue(publicSectorTemplateService.isServiceHasTemplate(productCode));
	}

	@Test
	public void shouldReturnHTMLForValidProductWithTemplate()
	{
		final ProductData product = Mockito.mock(ProductData.class);
		final YFormDefinitionData yformDefinitionData = Mockito.mock(YFormDefinitionData.class);
		yformDefinitionData.setFormId(SAMPLE_FORMID);

		Mockito.when(product.getAllYFormDefinitionList()).thenReturn(Arrays.asList(yformDefinitionData));
		Mockito.when(yformDefinitionData.getContent()).thenReturn(xmlContent);
		final YFormDataData yFormData1 = Mockito.mock(YFormDataData.class);
		Mockito.when(yFormData1.getContent()).thenReturn(xmlContent);
		Mockito.when(yFormData1.getFormDefinition()).thenReturn(yformDefinitionData);

		Mockito.when(product.getAllYFormDefinitionList()).thenReturn(Arrays.asList(yformDefinitionData));
		Mockito.when(yformDefinitionData.getContent()).thenReturn(xmlContent);
		final YFormDataData yFormData2 = Mockito.mock(YFormDataData.class);
		Mockito.when(yFormData2.getContent()).thenReturn(xmlContent);
		Mockito.when(yFormData2.getFormDefinition()).thenReturn(yformDefinitionData);

		final HashMap<String, String> map = new HashMap<String, String>();
		map.put("/sampleFormId/section-report-graffiti/damaged-item", "BUS");
		Mockito.when(yFormXmlParser.getYFormDataInMap(yFormData1.getContent(), yformDefinitionData.getContent(), SAMPLE_FORMID))
				.thenReturn(map);

		final RendererTemplateModel rendererTemplateModel = Mockito.mock(RendererTemplateModel.class);
		Mockito.when(product.getCode()).thenReturn(SAMPLE_GRAFFITI);
		rendererTemplateModel.setCode(product.getCode());
		Mockito.when(rendererService.getRendererTemplateForCode(product.getCode() + RENDERER)).thenReturn(rendererTemplateModel);

		final VelocityContext velocity = Mockito.mock(VelocityContext.class);
		Mockito.when(publicSectorTemplateContextFactory.create(map, rendererTemplateModel)).thenReturn(velocity);
		Mockito.doNothing().when(rendererService).render(rendererTemplateModel, velocity, renderedBody);
		publicSectorTemplateService.getTemplateHTMLForService(product, Arrays.asList(yFormData1, yFormData2));
		verify(rendererService, times(1)).render(any(RendererTemplateModel.class), any(VelocityContext.class),
				any(StringWriter.class));
	}

	@Test
	public void shouldReturnNullForProductWithInvalidTemplate()
	{
		final ProductData product = Mockito.mock(ProductData.class);
		final YFormDataData yFormData = Mockito.mock(YFormDataData.class);
		Mockito.when(yFormData.getContent()).thenReturn(xmlContent);

		final RendererTemplateModel rendererTemplateModel = Mockito.mock(RendererTemplateModel.class);
		Mockito.when(product.getCode()).thenReturn(SAMPLE_GRAFFITI);
		rendererTemplateModel.setCode(product.getCode());
		Mockito.when(rendererService.getRendererTemplateForCode(product.getCode() + RENDERER)).thenReturn(null);

		Assert.assertNull(publicSectorTemplateService.getTemplateHTMLForService(product, Arrays.asList(yFormData)));

	}

	@Test
	public void shouldReturnNullIfProductHasNoYFormDefinition()
	{
		final ProductData product = Mockito.mock(ProductData.class);
		final YFormDefinitionData yformDefinitionData = Mockito.mock(YFormDefinitionData.class);
		yformDefinitionData.setFormId(SAMPLE_FORMID);
		yformDefinitionData.setContent("");

		Mockito.when(product.getAllYFormDefinitionList()).thenReturn(Arrays.asList(yformDefinitionData));
		final YFormDataData yFormData = Mockito.mock(YFormDataData.class);
		Mockito.when(yFormData.getContent()).thenReturn(xmlContent);

		final RendererTemplateModel rendererTemplateModel = Mockito.mock(RendererTemplateModel.class);
		Mockito.when(product.getCode()).thenReturn(SAMPLE_GRAFFITI);
		rendererTemplateModel.setCode(product.getCode());
		Assert.assertNull(publicSectorTemplateService.getTemplateHTMLForService(product, Arrays.asList(yFormData)));

	}

	@Test
	public void shouldReturnNullForInvalidYFormData()
	{

		final ProductData product = Mockito.mock(ProductData.class);
		final YFormDefinitionData yformDefinitionData = Mockito.mock(YFormDefinitionData.class);
		yformDefinitionData.setFormId(SAMPLE_FORMID);

		yformDefinitionData.setContent(xmlContent);
		Mockito.when(product.getAllYFormDefinitionList()).thenReturn(Arrays.asList(yformDefinitionData));
		final YFormDataData yFormData = Mockito.mock(YFormDataData.class);
		Mockito.when(yFormData.getContent()).thenReturn(null);

		final RendererTemplateModel rendererTemplateModel = Mockito.mock(RendererTemplateModel.class);
		Mockito.when(product.getCode()).thenReturn(SAMPLE_GRAFFITI);
		rendererTemplateModel.setCode(product.getCode());
		Mockito.when(rendererService.getRendererTemplateForCode(product.getCode() + RENDERER)).thenReturn(rendererTemplateModel);
		Assert.assertThat(publicSectorTemplateService.getTemplateHTMLForService(product, Arrays.asList(yFormData)),
				isEmptyString());
	}

}
