/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.asset.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.asset.dao.impl.DefaultPSAssetDao;
import de.hybris.platform.publicsectorservices.asset.exception.PSAssetNotFoundException;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * UnitTest class for DefaultPSAssetService
 *
 */
@UnitTest
public class DefaultPSAssetServiceTest
{

	@InjectMocks
	private DefaultPSAssetService publicSectorAssetService;

	@Mock
	private DefaultPSAssetDao psAssetDao;

	private static final String ASSET_CODE = "1";

	@BeforeClass
	public static void tenantStuff()
	{
		Registry.setCurrentTenantByID("junit");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		publicSectorAssetService = new DefaultPSAssetService();
		publicSectorAssetService.setPsAssetDao(psAssetDao);
	}

	@Test
	public void testGetAssetsForUser()
	{
		final UserModel user = new UserModel();
		user.setUid("TestUser");

		final List<PSAssetModel> assetList = new ArrayList<>();
		Mockito.when(psAssetDao.getAssetsForUser(user)).thenReturn(assetList);

		Assert.assertNotNull(psAssetDao.getAssetsForUser(user));

	}

	@Test
	public void testGetAssetByCode() throws PSAssetNotFoundException
	{
		final String assetCode = "TestAsset";
		final PSAssetModel assetModel = new PSAssetModel();

		Mockito.when(psAssetDao.getAssetByCode(assetCode)).thenReturn(assetModel);

		Assert.assertEquals(publicSectorAssetService.getAssetByCode(assetCode), assetModel);
	}


	@Test
	public void testGetAssetAttributesByCode()
	{
		final String assetCode = "TestAsset";
		final List<PSAssetAttributeModel> assetAttrbutesModels = new ArrayList<>();

		Mockito.when(psAssetDao.getAssetAttributesByAssetCode(assetCode)).thenReturn(assetAttrbutesModels);

		Assert.assertEquals(publicSectorAssetService.getAssetAttributesByCode(assetCode), assetAttrbutesModels);

	}

	@Test
	public void testGetAssetAttributesMapping()
	{
		final String assetCode = "TestAsset";
		final List<PSAssetAttributeMappingModel> assetAttrbutesMappingModels = new ArrayList<>();

		final List<String> attributeNames = new ArrayList<>();
		attributeNames.add("TestName");

		Mockito.when(psAssetDao.getAssetAttributesMapping(assetCode, attributeNames)).thenReturn(assetAttrbutesMappingModels);

		Assert.assertEquals(publicSectorAssetService.getAssetAttributesMapping(assetCode, attributeNames),
				assetAttrbutesMappingModels);

	}
}
