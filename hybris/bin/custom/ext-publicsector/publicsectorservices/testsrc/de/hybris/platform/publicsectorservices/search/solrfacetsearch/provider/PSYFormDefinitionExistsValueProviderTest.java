/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.PropertyFieldValueProviderTestBase;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider.impl.PSYFormDefinitionExistsValueProvider;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider.FieldType;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.xyformsservices.model.YFormDefinitionModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit test for {@link PSYFormDefinitionExistsValueProvider}
 */
@UnitTest
public class PSYFormDefinitionExistsValueProviderTest extends PropertyFieldValueProviderTestBase
{
	private static final String TEST_PRODUCT_YFORM_DEFINITION_PROP = "allYFormDefinitionsList";
	private static final String INDEXED_PROP_EXPORT_ID = "exportId";
	private static final String INDEXED_PROP_TYPE = "propType";

	@InjectMocks
	private PSYFormDefinitionExistsValueProvider psYFormDefinitionExistsValueProvider;

	@Mock
	private FieldNameProvider fieldNameProvider;

	@Mock
	private PSServiceProductModel psServiceProductModel;

	@Mock
	private YFormDefinitionModel yFormDefinitionModel;

	@Mock
	private IndexedProperty indexedProperty;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		psYFormDefinitionExistsValueProvider = new PSYFormDefinitionExistsValueProvider();

		configure();
	}


	@Override
	protected String getPropertyName()
	{
		return TEST_PRODUCT_YFORM_DEFINITION_PROP;
	}

	@Override
	protected void configure()
	{
		setPropertyFieldValueProvider(psYFormDefinitionExistsValueProvider);
		configureBase();

		((PSYFormDefinitionExistsValueProvider) getPropertyFieldValueProvider()).setI18nService(i18nService);
		((PSYFormDefinitionExistsValueProvider) getPropertyFieldValueProvider()).setLocaleService(localeService);
		((PSYFormDefinitionExistsValueProvider) getPropertyFieldValueProvider()).setFieldNameProvider(fieldNameProvider);
		((PSYFormDefinitionExistsValueProvider) getPropertyFieldValueProvider()).setModelService(modelService);

		Assert.assertTrue(getPropertyFieldValueProvider() instanceof FieldValueProvider);
	}

	@Test
	public void testWhenNoYFormDefinitions() throws FieldValueProviderException
	{
		final Collection<FieldValue> result = psYFormDefinitionExistsValueProvider.getFieldValues(indexConfig, indexedProperty,
				psServiceProductModel);

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		if (result.iterator().hasNext())
		{
			Assert.assertFalse(((Boolean) result.iterator().next().getValue()).booleanValue());
			Assert.assertNull(result.iterator().next().getFieldName());
		}
	}

	@Test
	public void testGetYFormDefFieldValues() throws FieldValueProviderException
	{
		final List<YFormDefinitionModel> yformDefinitionList = new ArrayList<YFormDefinitionModel>();
		yformDefinitionList.add(yFormDefinitionModel);

		Mockito.when(psServiceProductModel.getAllYFormDefinitionList()).thenReturn(yformDefinitionList);

		Mockito.when(indexedProperty.getExportId()).thenReturn(INDEXED_PROP_EXPORT_ID);
		Mockito.when(indexedProperty.getType()).thenReturn(INDEXED_PROP_TYPE);
		Mockito.when(fieldNameProvider.getFieldName(indexedProperty, null, FieldType.INDEX))
				.thenReturn(TEST_PRODUCT_YFORM_DEFINITION_PROP);

		final Collection<FieldValue> result = psYFormDefinitionExistsValueProvider.getFieldValues(indexConfig, indexedProperty,
				psServiceProductModel);

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		if (result.iterator().hasNext())
		{
			Assert.assertTrue(((Boolean) result.iterator().next().getValue()).booleanValue());
			Assert.assertEquals(TEST_PRODUCT_YFORM_DEFINITION_PROP, result.iterator().next().getFieldName());
		}
	}
}
