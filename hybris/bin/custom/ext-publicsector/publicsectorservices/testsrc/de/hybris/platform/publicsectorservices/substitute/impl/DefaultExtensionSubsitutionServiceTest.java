/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.substitute.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;



/**
 * The class of DefaultPSCartDaoTest.
 *
 */
@UnitTest
public class DefaultExtensionSubsitutionServiceTest
{
	private static final String EXTENSION_NAME = "publicsectorservice";
	private static final String SUB_EXTENSION_NAME = "sub";

	@InjectMocks
	private DefaultExtensionSubsitutionService extensionService;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void testGetSubstitutedExtension()
	{
		final Map<String, String> extensionSubstitutions = Mockito.mock(HashMap.class);
		extensionSubstitutions.put(EXTENSION_NAME, null);
		extensionService.setExtensionSubstitutionMap(extensionSubstitutions);
		Assert.assertEquals(extensionService.getSubstitutedExtension(EXTENSION_NAME), EXTENSION_NAME);

		extensionSubstitutions.put(EXTENSION_NAME, "");
		Assert.assertEquals(extensionService.getSubstitutedExtension(EXTENSION_NAME), EXTENSION_NAME);

		Mockito.when(extensionSubstitutions.get(EXTENSION_NAME)).thenReturn(SUB_EXTENSION_NAME);
		Assert.assertEquals(extensionService.getSubstitutedExtension(EXTENSION_NAME), SUB_EXTENSION_NAME);
	}



}
