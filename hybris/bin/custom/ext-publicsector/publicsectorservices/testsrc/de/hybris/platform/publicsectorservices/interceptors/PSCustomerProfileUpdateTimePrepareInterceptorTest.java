/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorservices.interceptors;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ItemModelContext;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Unit test of {@link PSCustomerProfileUpdateTimePrepareInterceptor}
 */
@UnitTest
public class PSCustomerProfileUpdateTimePrepareInterceptorTest
{
	private static final String TEST_USER_UID = "testUid";
	private static final String TEST_USER_NAME = "testName";
	private static final String TEST_USER_UID_ORIGINAL = "testUid1";
	private static final String TEST_USER_NAME_ORIGINAL = "testName1";

	@InjectMocks
	private PSCustomerProfileUpdateTimePrepareInterceptor preparerInterceptor;

	@Mock
	private TimeService timeService;

	@Mock
	private ModelService mockModelService;

	@Mock
	private InterceptorContext mockInterceptorContext;

	@Mock
	private ItemModelContext customerContext;

	private CustomerModel customerModel;
	private AddressModel defaultDeliveryAddress;
	private TitleModel titleModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		preparerInterceptor = new PSCustomerProfileUpdateTimePrepareInterceptor()
		{
			@Override
			protected ItemModelContext getModelContext()
			{
				return customerContext;
			}
		};

		preparerInterceptor.setTimeService(timeService);

		customerModel = new CustomerModel();
		defaultDeliveryAddress = new AddressModel();
		titleModel = new TitleModel();

		when(timeService.getCurrentTime()).thenReturn(new Date());
	}

	@Test
	public void shouldUpdateProfileUpdateTimeOnProfileChanged() throws InterceptorException
	{
		// given
		customerModel.setType(CustomerType.REGISTERED);
		customerModel.setTitle(titleModel);
		customerModel.setName(TEST_USER_NAME);
		customerModel.setUid(TEST_USER_UID);
		customerModel.setDefaultShipmentAddress(defaultDeliveryAddress);

		recordOrderIsNew(false);
		recordInterceptorContextModifyReturns(true);
		recordInterceptorContextOriginalValueReturns();

		// when
		preparerInterceptor.onPrepare(customerModel, mockInterceptorContext);

		// then
		Assert.assertNotNull(customerModel.getProfileUpdateTime());
	}

	@Test
	public void shouldNotUpdateProfileUpdateTimeWhenProfileNotModified() throws InterceptorException
	{
		customerModel.setType(CustomerType.REGISTERED);
		customerModel.setTitle(titleModel);
		customerModel.setName(TEST_USER_NAME);
		customerModel.setUid(TEST_USER_UID);
		customerModel.setDefaultShipmentAddress(defaultDeliveryAddress);

		recordOrderIsNew(false);
		recordInterceptorContextModifyReturns(false);

		preparerInterceptor.onPrepare(customerModel, mockInterceptorContext);

		Assert.assertNull(customerModel.getProfileUpdateTime());
	}

	@Test
	public void shouldNotUpdateProfileUpdateTimeForGuestUser() throws InterceptorException
	{
		customerModel.setType(CustomerType.GUEST);
		customerModel.setTitle(titleModel);
		customerModel.setName(TEST_USER_NAME);
		customerModel.setUid(TEST_USER_UID);
		customerModel.setDefaultShipmentAddress(defaultDeliveryAddress);

		recordOrderIsNew(false);
		recordInterceptorContextModifyReturns(true);

		preparerInterceptor.onPrepare(customerModel, mockInterceptorContext);

		Assert.assertNull(customerModel.getProfileUpdateTime());
	}

	@Test
	public void shouldUpdateProfileUpdateTimeForNewRegisterdUser() throws InterceptorException
	{
		customerModel.setType(CustomerType.REGISTERED);

		recordOrderIsNew(true);

		preparerInterceptor.onPrepare(customerModel, mockInterceptorContext);

		Assert.assertNotNull(customerModel.getProfileUpdateTime());
	}

	private void recordOrderIsNew(final boolean isNew)
	{
		when(Boolean.valueOf(mockInterceptorContext.isNew(customerModel))).thenReturn(Boolean.valueOf(isNew));
	}

	private void recordInterceptorContextModifyReturns(final boolean returnValue)
	{
		when(Boolean.valueOf(mockInterceptorContext.isModified(customerModel, CustomerModel.UID)))
				.thenReturn(Boolean.valueOf(returnValue));
		when(Boolean.valueOf(mockInterceptorContext.isModified(customerModel, CustomerModel.TITLE)))
				.thenReturn(Boolean.valueOf(returnValue));
		when(Boolean.valueOf(mockInterceptorContext.isModified(customerModel, CustomerModel.NAME)))
				.thenReturn(Boolean.valueOf(returnValue));
		when(Boolean.valueOf(mockInterceptorContext.isModified(customerModel, CustomerModel.DEFAULTSHIPMENTADDRESS)))
				.thenReturn(Boolean.valueOf(returnValue));
	}

	private void recordInterceptorContextOriginalValueReturns()
	{
		when(customerContext.getOriginalValue(CustomerModel.UID)).thenReturn(TEST_USER_UID_ORIGINAL);
		when(customerContext.getOriginalValue(CustomerModel.TITLE)).thenReturn(customerModel.getTitle());
		when(customerContext.getOriginalValue(CustomerModel.NAME)).thenReturn(TEST_USER_NAME_ORIGINAL);
		when(customerContext.getOriginalValue(CustomerModel.DEFAULTSHIPMENTADDRESS))
				.thenReturn(customerModel.getDefaultShipmentAddress());
	}

}
