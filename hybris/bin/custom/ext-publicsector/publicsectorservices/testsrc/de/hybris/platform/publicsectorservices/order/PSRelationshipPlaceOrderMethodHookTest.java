/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.order;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.xyformscommerceservices.strategies.YFormUserStrategy;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class for PSRelationshipPlaceOrderMethodHook class to check all values will be passed and set correctly
 *
 */
@UnitTest
public class PSRelationshipPlaceOrderMethodHookTest
{
	@InjectMocks
	private PSRelationshipPlaceOrderMethodHook orderMethodHook;

	@Mock
	private ModelService modelService;

	@Mock
	private YFormUserStrategy userStrategy;

	@Mock
	private UserService userService;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		orderMethodHook.setModelService(modelService);
		orderMethodHook.setUserService(userService);
		orderMethodHook.setUserStrategy(userStrategy);
	}

	@Test
	public void testAfterAndBeforPlaceOrder() throws InvalidCartException
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(Mockito.mock(UserModel.class));

		Mockito.when(userStrategy.getCurrentUserForCheckout()).thenReturn(Mockito.mock(CustomerModel.class));
		final CommerceOrderResult orderResult = Mockito.mock(CommerceOrderResult.class);
		Mockito.when(orderResult.getOrder()).thenReturn(Mockito.mock(OrderModel.class));
		orderMethodHook.afterPlaceOrder(null, orderResult);

		//Before Place order
		final CommerceCheckoutParameter checkoutParameter = Mockito.mock(CommerceCheckoutParameter.class);
		Mockito.when(checkoutParameter.getCart()).thenReturn(Mockito.mock(CartModel.class));

		orderMethodHook.beforePlaceOrder(checkoutParameter);
		Mockito.verify(modelService, Mockito.atLeast(2)).save(Mockito.any());
	}
}
