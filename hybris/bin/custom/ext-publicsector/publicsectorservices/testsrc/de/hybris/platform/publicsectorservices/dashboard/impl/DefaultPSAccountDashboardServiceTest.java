/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.dashboard.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.dashboard.PSAccountDashboardService;
import de.hybris.platform.publicsectorservices.dashboard.dao.PSAccountDashboardDao;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for DefaultPSRelationshipService
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSAccountDashboardServiceTest
{
	List<PSRelationshipModel> existingRelationships = new ArrayList<>();

	@InjectMocks
	private final PSAccountDashboardService psRelationshipService = new DefaultPSAccountDashboardService();

	@Mock
	private PSAccountDashboardDao psDashboardDao;

	@Mock
	private UserModel sourceUser;

	@Mock
	private TypeService typeService;

	@Mock
	private CommerceCommonI18NService commerceCommonI18NService;


	@Test
	public void testGetBillsForUserRelationshipsByStatus()
	{

		final List<BillPaymentStatus> statuses = new ArrayList<>();

		final List<PSBillPaymentModel> bills = new ArrayList<>();
		Mockito.when(psDashboardDao.getBillsForUserRelationshipsByStatus(sourceUser, new ComposedTypeModel(), statuses))
				.thenReturn(bills);
		Mockito.when(typeService.getComposedTypeForCode(PSServiceProductModel._TYPECODE)).thenReturn(new ComposedTypeModel());

		Assert.assertNotNull(psRelationshipService.getBillsForUserRelationshipsByStatus(sourceUser, statuses));
	}
}
