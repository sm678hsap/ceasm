/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.helper.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.File;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;


/**
 * Unit test for {@link DefaultPSFilesHelper}
 *
 */
@UnitTest
public class DefaultPSFilesHelperTest
{
	private static final String LOCATION = File.separator + "a" + File.separator + "b";
	@InjectMocks
	private DefaultPSFilesHelper defaultPSFilesHelper;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		defaultPSFilesHelper = new DefaultPSFilesHelper();
	}

	@Test
	public void testGetNullImageAsEncodedString() throws IOException
	{
		Assert.assertNull(defaultPSFilesHelper.getImageAsEncodedString(LOCATION));
	}

}
