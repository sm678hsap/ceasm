/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.template.context.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.publicsectorservices.template.velocity.context.ReviewPageYFormContext;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.testframework.HybrisJUnit4Test;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;


/**
 * DefaultPublicSectorTemplateContextFactoryTest
 */
@UnitTest
public class DefaultPSYFormTemplateContextFactoryTest extends HybrisJUnit4Test
{
	@InjectMocks
	private DefaultPSYFormTemplateContextFactory psYFormTemplateContextFactory;

	@Mock
	BaseSiteService baseSiteService;

	@Mock
	RendererTemplateModel rendererTemplate;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		psYFormTemplateContextFactory.setBaseSiteService(baseSiteService);
		final BaseSiteModel baseSiteModel = new BaseSiteModel();
		baseSiteModel.setUid("en");
		Mockito.when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
	}

	@Test
	public void shouldReturnValidContextForValidTemplate()
	{
		Mockito.when(rendererTemplate.getContextClass()).thenReturn(ReviewPageYFormContext.class.getName());

		final HashMap<String, String> map = new HashMap<>();
		map.put("/section/control-1", "Hello");
		Assert.assertNotNull(psYFormTemplateContextFactory.create(map, rendererTemplate));
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowRuntimeExceptionTemplateIsNull()
	{
		final HashMap<String, String> map = new HashMap<>();
		map.put("/section/control-1", "Hello");
		psYFormTemplateContextFactory.create(map, null);
	}

	@Test(expected = NoSuchBeanDefinitionException.class)
	public void shouldThrowRuntimeExceptionIfContextBeanIsNotAvailable()
	{
		Mockito.when(rendererTemplate.getContextClass()).thenReturn(DefaultPSYFormTemplateContextFactoryTest.class.getName());
		final HashMap<String, String> map = new HashMap<>();
		Mockito.when(Registry.getApplicationContext().getBean(DefaultPSYFormTemplateContextFactoryTest.class)).thenReturn(null);
		psYFormTemplateContextFactory.create(map, rendererTemplate);
	}

}
