/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.event;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.publicsectorservices.model.PSDraftProcessModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;



/**
 * The class of DraftConfirmationEmailEventListenerTest.
 *
 */
@UnitTest
public class DraftConfirmationEmailEventListenerTest
{

	@InjectMocks
	private DraftConfirmationEmailEventListener draftListener;

	@Mock
	private ModelService modelService;

	@Mock
	private BusinessProcessService businessProcessService;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		draftListener.setBusinessProcessService(businessProcessService);
		draftListener.setModelService(modelService);
	}

	@Test
	public void testOnEvent()
	{
		final PSDraftProcessModel psDraft = Mockito.mock(PSDraftProcessModel.class);

		final DraftConfirmationEmailEvent draftConfirmation = Mockito.mock(DraftConfirmationEmailEvent.class);
		final CartModel cart = Mockito.mock(CartModel.class);
		final CartEntryModel cartEntry = Mockito.mock(CartEntryModel.class);
		Mockito.when(cartEntry.getProduct()).thenReturn(Mockito.mock(PSServiceProductModel.class));
		final List<AbstractOrderEntryModel> cartEntries = Collections.singletonList(cartEntry);
		Mockito.when(cart.getEntries()).thenReturn(cartEntries);
		Mockito.when(draftConfirmation.getCartModel()).thenReturn(cart);
		Mockito.when(businessProcessService.createProcess(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Mockito.mock(PSDraftProcessModel.class));

		Mockito.doNothing().when(modelService).save(psDraft);
		Mockito.doNothing().when(businessProcessService).startProcess(psDraft);

		draftListener.onEvent(draftConfirmation);

		Mockito.verify(modelService).save(Mockito.any(PSDraftProcessModel.class));

	}



}
