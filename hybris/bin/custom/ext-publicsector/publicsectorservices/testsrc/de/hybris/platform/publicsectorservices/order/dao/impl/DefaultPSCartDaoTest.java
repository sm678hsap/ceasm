/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.order.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of DefaultPSCartDaoTest.
 *
 */
@UnitTest
public class DefaultPSCartDaoTest
{

	@InjectMocks
	private DefaultPSCartDao publicSectorCartDao;

	@Mock
	private ModelService modelService;

	@Mock
	private FlexibleSearchService flexibleSearchService;


	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		publicSectorCartDao.setModelService(modelService);
		publicSectorCartDao.setFlexibleSearchService(flexibleSearchService);
	}

	@Test
	public void testGetCartForCodeAndSite()
	{
		Assert.assertNull(publicSectorCartDao.getCartForCodeAndSite(null, Mockito.mock(BaseSiteModel.class)));
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list1 = new ArrayList();
		Mockito.when(searchResult.getResult()).thenReturn(list1);
		Assert.assertNull(publicSectorCartDao.getCartForCodeAndSite(null, Mockito.mock(BaseSiteModel.class)));
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(CartModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);
		Assert.assertNotNull(publicSectorCartDao.getCartForCodeAndSite("Code1", Mockito.mock(BaseSiteModel.class)));

	}



}
