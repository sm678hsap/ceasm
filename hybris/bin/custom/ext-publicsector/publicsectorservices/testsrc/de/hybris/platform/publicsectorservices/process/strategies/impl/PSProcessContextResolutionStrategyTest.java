/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.process.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.publicsectorservices.model.PSDraftProcessModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;



/**
 * The class of PSProcessContextResolutionStrategyTest.
 *
 */
@UnitTest
public class PSProcessContextResolutionStrategyTest
{
	private static final String HYBRIS_SITE = "hybrisSite";
	private static final String PUBLIC_SECTOR_SITE = "publicSector";

	@InjectMocks
	private PSProcessContextResolutionStrategy processContextResolutionStrategy;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetCMSSite()
	{
		processContextResolutionStrategy.getCmsSite(null);
		final PSDraftProcessModel psDraft = Mockito.mock(PSDraftProcessModel.class);
		final BaseSiteModel baseSite = Mockito.mock(BaseSiteModel.class);
		Mockito.when(baseSite.getName()).thenReturn(PUBLIC_SECTOR_SITE);
		Mockito.when(psDraft.getSite()).thenReturn(baseSite);
		Assert.assertEquals(processContextResolutionStrategy.getCmsSite(psDraft).getName(), PUBLIC_SECTOR_SITE);

		final StoreFrontProcessModel businessProcessModel = Mockito.mock(StoreFrontProcessModel.class);
		Mockito.when(baseSite.getName()).thenReturn(HYBRIS_SITE);
		Mockito.when(businessProcessModel.getSite()).thenReturn(baseSite);
		Assert.assertEquals(processContextResolutionStrategy.getCmsSite(businessProcessModel).getName(), HYBRIS_SITE);
	}



}
