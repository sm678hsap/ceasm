/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.encoder.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.publicsectorservices.encoder.PSEncoder;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;


/**
 * Unit test DefaultPublicSectorEncoderTest
 */
@UnitTest
public class DefaultPSEncoderTest
{
	private static final String SAMPLE_PATH = "c:/test/test.pdf";
	private static final String SAMPLE_ENCRYPTED_DATA = "jFdFmGRu/xUDA9tuJXijP7vbRMG4LxS9";

	@InjectMocks
	private final PSEncoder publicSectorEncoder = new DefaultPSEncoder();

	@Test
	public void testEncrypt()
	{
		final String encryptedData = publicSectorEncoder.encrypt(SAMPLE_PATH);
		Assert.assertEquals(SAMPLE_ENCRYPTED_DATA, encryptedData);
	}

	@Test
	public void testDecrypt()
	{
		final String decryptedData = publicSectorEncoder.decrypt(SAMPLE_ENCRYPTED_DATA);
		Assert.assertEquals(SAMPLE_PATH, decryptedData);
	}

}
