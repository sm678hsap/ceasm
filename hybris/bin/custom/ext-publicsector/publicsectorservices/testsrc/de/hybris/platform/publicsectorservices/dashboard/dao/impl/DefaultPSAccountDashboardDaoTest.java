/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.dashboard.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of DefaultPSCartDaoTest.
 *
 */
@UnitTest
public class DefaultPSAccountDashboardDaoTest
{

	@InjectMocks
	private DefaultPSAccountDashboardDao publicSectorAssetDao;

	@Mock
	private ModelService modelService;

	@Mock
	private FlexibleSearchService flexibleSearchService;


	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		publicSectorAssetDao.setModelService(modelService);
		publicSectorAssetDao.setFlexibleSearchService(flexibleSearchService);
	}


	@Test
	public void testGetBillsForUserRelationshipsByStatus()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(PSBillPaymentModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		final List<BillPaymentStatus> statuses = new ArrayList<>();
		statuses.add(BillPaymentStatus.PAID);

		Assert.assertNotNull(publicSectorAssetDao.getBillsForUserRelationshipsByStatus(user, new ComposedTypeModel(), statuses));

	}

}
