/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.product.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;



/**
 * Test suite for {@link DefaultPSProductService}
 *
 */
@UnitTest
public class DefaultPSProductServiceTest
{
	@InjectMocks
	private DefaultPSProductService publicSectorProductService;

	@Mock
	private CartService cartService;
	@Mock
	private CustomerAccountService customerAccountService;
	@Mock
	private BaseStoreService baseStoreService;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		publicSectorProductService = new DefaultPSProductService();
		publicSectorProductService.setCartService(cartService);
		publicSectorProductService.setCustomerAccountService(customerAccountService);
		publicSectorProductService.setBaseStoreService(baseStoreService);
	}

	@Test
	public void verifyGetServiceProductFromOrder()
	{
		final AbstractOrderModel orderModel = new AbstractOrderModel();
		final AbstractOrderEntryModel entryModel1 = new AbstractOrderEntryModel();
		final AbstractOrderEntryModel entryModel2 = new AbstractOrderEntryModel();
		final PSServiceProductModel serviceProduct = new PSServiceProductModel();

		entryModel1.setProduct(serviceProduct);
		orderModel.setEntries(Arrays.<AbstractOrderEntryModel> asList(entryModel1, entryModel2));

		final PSServiceProductModel result = publicSectorProductService.getServiceProductFromOrder(orderModel);

		Assert.assertNotNull(result);
		Assert.assertEquals(serviceProduct, result);
	}

	@Test
	public void verifyGetServiceProductFromOrderForEmptyOrder()
	{
		final AbstractOrderModel orderModel = new AbstractOrderModel();
		orderModel.setEntries(Collections.<AbstractOrderEntryModel> emptyList());

		final PSServiceProductModel result = publicSectorProductService.getServiceProductFromOrder(orderModel);

		Assert.assertNull(result);
	}

	@Test
	public void verifyGetServiceProductFromSessionCart()
	{
		final CartModel cartModel = new CartModel();
		final CartEntryModel entryModel1 = new CartEntryModel();
		final CartEntryModel entryModel2 = new CartEntryModel();
		final PSServiceProductModel serviceProduct = new PSServiceProductModel();

		entryModel1.setProduct(serviceProduct);
		cartModel.setEntries(Arrays.<AbstractOrderEntryModel> asList(entryModel1, entryModel2));

		given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
		given(cartService.getSessionCart()).willReturn(cartModel);

		final PSServiceProductModel result = publicSectorProductService.getServiceProductFromSessionCart();

		Assert.assertNotNull(result);
		Assert.assertEquals(serviceProduct, result);
	}

	@Test
	public void verifyGetServiceProductForOrderCode()
	{
		final String orderCode = "sample-order";
		final OrderModel orderModel = new OrderModel();
		final AbstractOrderEntryModel entryModel1 = new AbstractOrderEntryModel();
		final AbstractOrderEntryModel entryModel2 = new AbstractOrderEntryModel();
		final PSServiceProductModel serviceProduct = new PSServiceProductModel();

		entryModel1.setProduct(serviceProduct);
		orderModel.setEntries(Arrays.<AbstractOrderEntryModel> asList(entryModel1, entryModel2));

		final BaseStoreModel baseStoreModel = new BaseStoreModel();
		baseStoreModel.setUid("baseStoreModel");

		given(publicSectorProductService.getBaseStoreService().getCurrentBaseStore()).willReturn(baseStoreModel);
		given(publicSectorProductService.getCustomerAccountService().getOrderForCode(orderCode, baseStoreModel))
				.willReturn(orderModel);

		final PSServiceProductModel result = publicSectorProductService.getServiceProductForOrderCode(orderCode);

		Assert.assertNotNull(result);
		Assert.assertEquals(serviceProduct, result);
	}

}
