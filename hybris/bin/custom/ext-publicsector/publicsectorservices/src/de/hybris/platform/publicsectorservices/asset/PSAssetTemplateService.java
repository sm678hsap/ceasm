/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.asset;

import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;

import java.util.List;


/**
 * Interface PSAssetTemplateService
 */
public interface PSAssetTemplateService
{

	/**
	 * Creates velocity context from asset attributes(assetType,assetAttributesData) & assetAttributeMappingData
	 *
	 * @param assetType
	 *           String represents the type of the asset
	 * @param assetAttributesData
	 *           list of PSAssetAttributeData
	 * @param assetAttributeMappingsData
	 *           list of PSAssetAttributeMappingData
	 * @return velocity context as a String
	 */

	String generateAssetAttributesTemplate(String assetType, List<PSAssetAttributeData> assetAttributesData,
			List<PSAssetAttributeMappingData> assetAttributeMappingsData);
}
