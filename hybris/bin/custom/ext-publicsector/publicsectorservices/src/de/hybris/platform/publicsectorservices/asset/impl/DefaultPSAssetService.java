/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.asset.impl;


import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.asset.PSAssetService;
import de.hybris.platform.publicsectorservices.asset.dao.PSAssetDao;
import de.hybris.platform.publicsectorservices.asset.exception.PSAssetNotFoundException;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Public sector asset service
 * <p>
 * DefaultPSAssetService
 */
public class DefaultPSAssetService implements PSAssetService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSAssetService.class);
	private PSAssetDao psAssetDao;

	/**
	 * gets assets for given user
	 */
	@Override
	public List<PSAssetModel> getAssetsForUser(final UserModel user)
	{
		if (user != null)
		{
			return getPsAssetDao().getAssetsForUser(user);
		}

		return Collections.emptyList();
	}

	@Override
	public PSAssetModel getAssetByCode(final String assetCode) throws PSAssetNotFoundException
	{
		PSAssetModel asset = null;
		try
		{
			asset = getPsAssetDao().getAssetByCode(assetCode);
		}
		catch (final PSAssetNotFoundException e)
		{
			LOG.error("PSAssetNotFoundException for assetCode [" + assetCode + "]", e.getMessage());
			throw e;
		}
		return asset;
	}

	@Override
	public List<PSAssetAttributeModel> getAssetAttributesByCode(final String assetCode)
	{
		return getPsAssetDao().getAssetAttributesByAssetCode(assetCode);
	}

	@Override
	public List<PSAssetAttributeMappingModel> getAssetAttributesMapping(final String assetType,
			final List<String> assetAttributeNames)
	{
		return getPsAssetDao().getAssetAttributesMapping(assetType, assetAttributeNames);
	}

	protected PSAssetDao getPsAssetDao()
	{
		return psAssetDao;
	}

	@Required
	public void setPsAssetDao(final PSAssetDao psAssetDao)
	{
		this.psAssetDao = psAssetDao;
	}

}
