/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.order.impl;


import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartStrategy;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.publicsectorservices.event.DraftConfirmationEmailEvent;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.order.PSCartService;
import de.hybris.platform.publicsectorservices.order.dao.PSCartDao;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.xyformscommerceservices.strategies.YFormUserStrategy;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Public sector cart service
 * <p>
 * DefaultPublicSectorCartService
 */
public class DefaultPSCartService implements PSCartService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSCartService.class);
	private CommerceSaveCartStrategy commerceSaveCartStrategy;
	private CommerceCartService commerceCartService;
	private PSCartDao psCartDao;
	private BaseSiteService baseSiteService;
	private ModelService modelService;
	private CartService cartService;
	private EventService eventService;
	private BaseStoreService baseStoreService;
	private CommonI18NService commonI18NService;
	private TimeService timeService;
	private YFormUserStrategy userStrategy;
	private UserService userService;

	/**
	 * Sets returnURL and lastAccessDate in cart model for draft carts
	 */
	@Override
	public void setDraftCartDetails(final String returnURL)
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();

			sessionCart.setReturnURL(returnURL);
			sessionCart.setLastAccessDate(getTimeService().getCurrentTime());
			sessionCart.setUser(getUserStrategy().getCurrentUserForCheckout());

			if (sessionCart.getLockedBy() == null)
			{
				sessionCart.setLockedBy(getUserService().getCurrentUser());
			}

			final CommerceSaveCartParameter parameter = new CommerceSaveCartParameter();
			parameter.setCart(sessionCart);
			parameter.setEnableHooks(true);
			parameter
					.setName(new StringBuilder(sessionCart.getCode()).append("-").append(sessionCart.getUser().getUid()).toString());
			parameter.setDescription("Saving cart for user");

			try
			{
				commerceSaveCartStrategy.saveCart(parameter);
				//initiate draft confirmation email
				LOG.info("updating the cart with draft details and publishing the event");
				getEventService().publishEvent(
						initializeEvent(new DraftConfirmationEmailEvent(sessionCart), (CustomerModel) sessionCart.getUser()));
			}
			catch (final CommerceSaveCartException e)
			{
				LOG.error("Error while saving cart for user: {}, \n {}", sessionCart.getCode(), e);
			}
		}
		else
		{
			LOG.warn("No session cart exists!");
		}
	}

	/**
	 * To update the cart when YForm version changed for saved YFormData in cart entries. This method remove all the
	 * bundle entries. And set Delivery Address, Payment Address, Delivery Mode, Payment Info to Null.
	 */
	@Override
	public void updateCartOnYFormVersionUpdate()
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			final List<CartEntryModel> entries = (List) sessionCart.getEntries();
			final Collection<CartEntryModel> toRemove = new LinkedList<>();
			for (final CartEntryModel cartEntry : entries)
			{
				if (cartEntry.getProduct() instanceof PSServiceProductModel)
				{
					getModelService().removeAll(cartEntry.getYFormData());
				}
				else
				{
					toRemove.add(cartEntry);
				}
			}
			sessionCart.setDeliveryAddress(null);
			sessionCart.setPaymentAddress(null);
			sessionCart.setPaymentInfo(null);
			sessionCart.setDeliveryMode(null);

			getModelService().removeAll(toRemove);
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);
			recalculateCart(sessionCart);
		}
	}

	/**
	 * Returns the cart for given cart code and user email
	 *
	 * @param code
	 * @param email
	 * @return CartModel/null
	 */
	@Override
	public CartModel getCartForGuestUserEmailAndCartCode(final String code, final String email)
	{
		ServicesUtil.validateParameterNotNull(code, "Cart Code Cannot be null");
		ServicesUtil.validateParameterNotNull(email, "Email Cannot be null");
		final CartModel cart = getPsCartDao().getCartForCodeAndSite(code, getBaseSiteService().getCurrentBaseSite());
		if (cart != null)
		{
			final CustomerModel customer = (CustomerModel) cart.getUser();
			if ((CustomerType.GUEST.equals(customer.getType()))
					&& (StringUtils.substringAfter(customer.getUid(), "|").equals(email)))
			{
				return cart;
			}
		}
		LOG.debug("Cart does not exists for given code {} and email {}", code, email);
		return null;
	}

	/**
	 * returns whether the given cart is valid for given user
	 *
	 * @param code
	 * @param user
	 * @return boolean
	 */
	@Override
	public boolean isCartAssociatedWithUser(final String code, final UserModel user)
	{
		return (getCommerceCartService().getCartForCodeAndUser(code, user) != null) ? true : false;
	}

	/**
	 * removes the given cart and associated yform data if valid
	 *
	 * @param cartCode
	 * @param user
	 */
	@Override
	public void removeCart(final String cartCode, final UserModel user)
	{
		ServicesUtil.validateParameterNotNull(cartCode, "Cart Code Cannot be null");
		final CartModel cart = getCommerceCartService().getCartForCodeAndUser(cartCode, user);
		if (cart != null)
		{
			final List<AbstractOrderEntryModel> entries = cart.getEntries();
			for (final AbstractOrderEntryModel cartEntry : entries)
			{
				if (cartEntry.getProduct() instanceof PSServiceProductModel && cartEntry.getYFormData() != null)
				{
					getModelService().removeAll(cartEntry.getYFormData());
				}
			}
			if (getCartService().hasSessionCart() && getCartService().getSessionCart().getCode().equals(cart.getCode()))
			{
				getCartService().removeSessionCart();
			}
			else
			{
				getModelService().remove(cart);
			}
		}
		else
		{
			LOG.debug("Cart does not exists for given code {} ", cartCode);
		}
	}

	/**
	 * returns whether the given cart is saved draft or not
	 *
	 * @param cart
	 * 
	 * @return boolean
	 */
	@Override
	public boolean isCartSavedDraft(final CartModel cart)
	{
		return cart != null && cart.getReturnURL() != null;
	}

	/**
	 * initializes an {@link AbstractCommerceUserEvent}
	 *
	 * @param event
	 * @param customerModel
	 * @return the event
	 */
	protected AbstractCommerceUserEvent initializeEvent(final DraftConfirmationEmailEvent event, final CustomerModel customerModel)
	{
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setCustomer(customerModel);
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		event.setCartModel(getCartService().getSessionCart());
		return event;
	}

	@Override
	public void recalculateCart(final CartModel cart)
	{
		try
		{
			final CommerceCartParameter parameter = new CommerceCartParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cart);
			getCommerceCartService().recalculateCart(parameter);
		}
		catch (final CalculationException ex)
		{
			LOG.error("Failed to recalculate cart [ {} ] {}", cart.getCode(), ex);
		}
	}

	@Override
	public SearchPageData<CartModel> getSavedDraftsForSiteAndUser(final PageableData pageableData, final BaseSiteModel baseSite,
			final UserModel user, final List<OrderStatus> orderStatus)
	{
		ServicesUtil.validateParameterNotNull(user, "User Cannot be null");
		return getPsCartDao().getSavedDraftsForSiteAndUser(pageableData, baseSite, user, orderStatus);
	}


	protected CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	@Required
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	@Required
	public void setCommerceSaveCartStrategy(final CommerceSaveCartStrategy commerceSaveCartStrategy)
	{
		this.commerceSaveCartStrategy = commerceSaveCartStrategy;
	}

	protected PSCartDao getPsCartDao()
	{
		return psCartDao;
	}

	@Required
	public void setPsCartDao(final PSCartDao psCartDao)
	{
		this.psCartDao = psCartDao;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected YFormUserStrategy getUserStrategy()
	{
		return userStrategy;
	}

	@Required
	public void setUserStrategy(final YFormUserStrategy userStrategy)
	{
		this.userStrategy = userStrategy;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
