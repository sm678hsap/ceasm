/*
 * [y] hybris Platform

 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.dashboard.dao.impl;


import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.dashboard.dao.PSAccountDashboardDao;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation class for PSAccountDashboard DAO.
 */
public class DefaultPSAccountDashboardDao extends AbstractItemDao implements PSAccountDashboardDao
{

	private static final String USER = "user";
	private static final String SHAREABLE_TYPE = "shareableType";
	private static final String STATUS = "status";
	private static final String PERMISSION_STATUS = "permissionStatus";
	private static final String BILL_STATUS = "billStatuses";

	private static final String ACTIVE = "active";
	private static final String SOURCE_POA_HOLDER = "isSourcePoaHolder";
	private static final String TARGET_POA_HOLDER = "isTargetPoaHolder";

	private static final String USER_NOT_NULL = "User should not be null";

	private static final String SELECT_PERMISSIBLE_ITEM = "SELECT {" + PSPermissibleAreaItemTypeModel.PK + "} FROM {"
			+ PSPermissibleAreaItemTypeModel._TYPECODE + "}" + " WHERE {" + AbstractPSPermissibleAreaModel.ACTIVE + "} = ?active"
			+ " AND {" + PSPermissibleAreaItemTypeModel.SHAREABLETYPE + "} =?shareableType";

	private static final String FETCH_POA_RECEIVERS_FOR_USER = "SELECT ( CASE WHEN {" + PSRelationshipModel.SOURCEUSER
			+ "} = ?user" + " THEN {" + PSRelationshipModel.TARGETUSER + "} ELSE {" + PSRelationshipModel.SOURCEUSER
			+ "} END ) AS usr_rel_per FROM" + " {" + PSRelationshipModel._TYPECODE + "} WHERE ( ({" + PSRelationshipModel.SOURCEUSER
			+ "} = ?user AND {" + PSRelationshipModel.ISSOURCEPOAHOLDER + "} = ?isSourcePoaHolder)" + " OR ({" + PSRelationshipModel.TARGETUSER
			+ "} = ?user AND {" + PSRelationshipModel.ISTARGETPOAHOLDER + "} = ?isTargetPoaHolder) )" + " AND {"
			+ PSRelationshipModel.RELATIONSHIPSTATUS + "} = ?status";

	private static final String FETCH_ACTIVE_RELATIONSHIPS_FOR_USER = "SELECT sampletable.usr_rel_per FROM ( {{ SELECT ( CASE WHEN {"
			+ PSRelationshipModel.SOURCEUSER + "} = ?user" + " THEN {" + PSRelationshipModel.TARGETUSER + "} ELSE {"
			+ PSRelationshipModel.SOURCEUSER + "} END ) AS usr_rel_per FROM" + " {" + PSRelationshipModel._TYPECODE + "}WHERE (" + "({"
			+ PSRelationshipModel.SOURCEUSER + "} = ?user) OR " + "({" + PSRelationshipModel.TARGETUSER + "} = ?user)) " + " AND "
			+ "{" + PSRelationshipModel.RELATIONSHIPSTATUS + "} IN (?status) }} ) sampletable";

	private static final String FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE = "SELECT uniontable.usr_rel_per FROM ( {{ "
			+ FETCH_POA_RECEIVERS_FOR_USER + " }} UNION ALL {{ SELECT {" + PSPermissionModel.SOURCEUSER + "} AS usr_rel_per FROM {"
			+ PSPermissionModel._TYPECODE + "} WHERE {" + PSPermissionModel.TARGETUSER + "} = ?user AND {"
			+ PSPermissionModel.SOURCEUSER + "} IN ( {{ " + FETCH_ACTIVE_RELATIONSHIPS_FOR_USER + " }} )  AND {"
			+ PSPermissionModel.PERMISSIBLEAREAITEMTYPE + "} = ( {{ " + SELECT_PERMISSIBLE_ITEM + " }} ) " + " AND " + "{"
			+ PSPermissionModel.PERMISSIONSTATUS + "} = (?permissionStatus) }} ) uniontable";

	protected static final String FETCH_BILLS_STATUS_QUERY_BY_CUSTOMER = "SELECT {" + PSBillPaymentModel.PK + "} " + "FROM {"
			+ PSBillPaymentModel._TYPECODE + "! AS p " + "JOIN " + CustomerModel._TYPECODE + " AS c ON {p:"
			+ PSBillPaymentModel.CUSTOMER + "}={c:" + CustomerModel.PK + "}" + "JOIN " + EnumerationValueModel._TYPECODE
			+ " AS status ON {p:" + PSBillPaymentModel.BILLPAYMENTSTATUS + "}={status:" + EnumerationValueModel.PK + "}} "
			+ "WHERE {c:" + CustomerModel.PK + "} IN ( {{ " + FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE + " }} ) AND {"
			+ PSBillPaymentModel.BILLPAYMENTSTATUS + "} IN (?billStatuses)";




	private PagedFlexibleSearchService pagedFlexibleSearchService;


	@Override
	public List<PSBillPaymentModel> getBillsForUserRelationshipsByStatus(final UserModel user,
			final ComposedTypeModel serviceProductComposedType, final List<BillPaymentStatus> statuses)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();

		params.put(USER, user);
		params.put(SHAREABLE_TYPE, serviceProductComposedType);

		params.put(STATUS, PSRelationshipStatus.ACTIVE);
		params.put(PERMISSION_STATUS, PSPermissionStatus.ACTIVE);
		params.put(BILL_STATUS, statuses);

		params.put(ACTIVE, Boolean.TRUE);
		params.put(SOURCE_POA_HOLDER, Boolean.TRUE);
		params.put(TARGET_POA_HOLDER, Boolean.TRUE);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FETCH_BILLS_STATUS_QUERY_BY_CUSTOMER);
		query.addQueryParameters(params);

		final SearchResult<PSBillPaymentModel> bills = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(bills.getResult()) ? bills.getResult() : null;
	}


	protected PagedFlexibleSearchService getPagedFlexibleSearchService()
	{
		return pagedFlexibleSearchService;
	}

	@Required
	public void setPagedFlexibleSearchService(final PagedFlexibleSearchService pagedFlexibleSearchService)
	{
		this.pagedFlexibleSearchService = pagedFlexibleSearchService;
	}
}
