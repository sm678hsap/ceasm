/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.template.context.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.publicsectorservices.template.context.PSAssetAttributesContextFactory;
import de.hybris.platform.publicsectorservices.template.velocity.context.PSAssetAttributesContext;
import de.hybris.platform.site.BaseSiteService;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;


/**
 * ContextFactory class for asset attributes.
 *
 */
public class DefaultPSAssetAttributesContextFactory implements PSAssetAttributesContextFactory
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSAssetAttributesContextFactory.class);
	private BaseSiteService baseSiteService;

	@Override
	public VelocityContext create(final RendererTemplateModel renderTemplate, final List<PSAssetAttributeData> assetAttributeData,
			final List<PSAssetAttributeMappingData> assetAttributeMappingData)
	{
		final BaseSiteModel baseSite = getBaseSiteService().getCurrentBaseSite();

		final PSAssetAttributesContext context = resolveContext(renderTemplate);
		context.init(baseSite, assetAttributeData, assetAttributeMappingData);
		return context;
	}

	protected <T extends VelocityContext> T resolveContext(final RendererTemplateModel renderTemplate)
	{
		try
		{
			final Class<T> contextClass = (Class<T>) Class.forName(renderTemplate.getContextClass());
			final Map<String, T> context = getApplicationContext().getBeansOfType(contextClass);
			if (MapUtils.isNotEmpty(context))
			{
				LOG.debug("found context for given class", renderTemplate.getContextClass());
				return context.entrySet().iterator().next().getValue();
			}
			else
			{
				throw new IllegalStateException("Cannot find bean in application context for context class [" + contextClass + "]");
			}
		}
		catch (final ClassNotFoundException e)
		{
			LOG.error("Context class not found", e);
			throw new IllegalStateException("Cannot find context class", e);
		}
	}

	protected ApplicationContext getApplicationContext()
	{
		return Registry.getApplicationContext();
	}

	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

}
