/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.template.context;

import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;

import java.util.List;

import org.apache.velocity.VelocityContext;


/**
 * Interface PSAssetAttributeContextFactory
 */
public interface PSAssetAttributesContextFactory
{
	VelocityContext create(RendererTemplateModel renderTemplate, List<PSAssetAttributeData> assetAttributeData,
			List<PSAssetAttributeMappingData> assetAttributeMappingData);
}
