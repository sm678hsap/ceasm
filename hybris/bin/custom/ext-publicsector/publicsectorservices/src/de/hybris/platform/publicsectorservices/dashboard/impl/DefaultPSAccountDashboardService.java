/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.dashboard.impl;


import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.dashboard.PSAccountDashboardService;
import de.hybris.platform.publicsectorservices.dashboard.dao.PSAccountDashboardDao;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * PS Account Dashboard service
 * <p>
 * DefaultPSAccountDashboardService
 */
public class DefaultPSAccountDashboardService implements PSAccountDashboardService
{
	private PSAccountDashboardDao psAccountDashboardDao;
	private TypeService typeService;

	@Override
	public List<PSBillPaymentModel> getBillsForUserRelationshipsByStatus(final UserModel user,
			final List<BillPaymentStatus> statuses)
	{
		return getPsAccountDashboardDao().getBillsForUserRelationshipsByStatus(user,
				getTypeService().getComposedTypeForCode(PSBillPaymentModel._TYPECODE), statuses);
	}


	protected PSAccountDashboardDao getPsAccountDashboardDao()
	{
		return psAccountDashboardDao;
	}

	@Required
	public void setPsAccountDashboardDao(final PSAccountDashboardDao psAccountDashboardDao)
	{
		this.psAccountDashboardDao = psAccountDashboardDao;
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}


}
