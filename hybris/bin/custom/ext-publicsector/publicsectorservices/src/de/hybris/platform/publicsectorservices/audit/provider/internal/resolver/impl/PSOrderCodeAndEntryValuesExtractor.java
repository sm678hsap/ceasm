/**
 *
 */
package de.hybris.platform.publicsectorservices.audit.provider.internal.resolver.impl;

import de.hybris.platform.audit.provider.internal.resolver.AuditRecordInternalProvider;
import de.hybris.platform.audit.provider.internal.resolver.VirtualReferenceValuesExtractor;
import de.hybris.platform.audit.provider.internal.resolver.impl.AuditTypeContext;
import de.hybris.platform.core.PK;
import de.hybris.platform.persistence.audit.internal.AuditRecordInternal;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Extracts refId and fetches the data based on it
 *
 */
public class PSOrderCodeAndEntryValuesExtractor implements VirtualReferenceValuesExtractor
{
	@Override
	public <AUDITRECORD extends AuditRecordInternal> List<AUDITRECORD> extractValues(
			final AuditRecordInternalProvider<AUDITRECORD> provider, final AuditTypeContext<AUDITRECORD> ctx)
	{
		final Set<AUDITRECORD> orderEntries = ctx.getPayloadsForBasePKs();


		final Set<Object> values = new HashSet<>();
		for (final AUDITRECORD entree : orderEntries)
		{
			final String entryNumber = entree.getAttribute("entrynumber").toString();



			final String orderPKStr = entree.getAttribute("order").toString();

			final PK orderPK = PK.parse(orderPKStr);

			final Set<AUDITRECORD> orders = ctx.getPayloads(orderPK);

			for (final AUDITRECORD order : orders)
			{
				values.add(order.getAttribute("code") + "_" + entryNumber);
			}
		}

		return provider.queryRecords(values);
	}
}
