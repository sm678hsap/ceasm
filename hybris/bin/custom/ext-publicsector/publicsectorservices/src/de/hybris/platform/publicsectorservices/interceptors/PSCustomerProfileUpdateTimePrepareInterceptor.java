/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorservices.interceptors;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ItemModelContext;
import de.hybris.platform.servicelayer.model.ModelContextUtils;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * PrepareInterceptor for CustomerModel to set profileUpdateTime when customer profile(Personal Details, Email Address
 * or Primary Address) gets modified.
 */
public class PSCustomerProfileUpdateTimePrepareInterceptor implements PrepareInterceptor<CustomerModel>
{
	private ItemModelContext customerContext;
	private TimeService timeService;

	@Override
	public void onPrepare(final CustomerModel customerModel, final InterceptorContext ctx) throws InterceptorException
	{
		if (CustomerType.REGISTERED.equals(customerModel.getType()))
		{
			if (ctx.isNew(customerModel))
			{
				setProfileUpdateTime(customerModel);
			}
			else
			{
				setModelContext(customerModel);
				if (isUIDModified(customerModel, ctx) || isTitleModified(customerModel, ctx) || isNameModified(customerModel, ctx)
						|| isDefaultShipmentAddressModified(customerModel, ctx))
				{
					setProfileUpdateTime(customerModel);
				}
			}
		}
	}

	private void setProfileUpdateTime(final CustomerModel customerModel)
	{
		final Date currentDate = getTimeService().getCurrentTime();
		customerModel.setProfileUpdateTime(currentDate);
	}

	private boolean isUIDModified(final CustomerModel customerModel, final InterceptorContext ctx)
	{
		if (ctx.isModified(customerModel, CustomerModel.UID))
		{
			return !StringUtils.equals(getModelContext().getOriginalValue(CustomerModel.UID), customerModel.getUid());
		}
		return false;
	}

	private boolean isTitleModified(final CustomerModel customerModel, final InterceptorContext ctx)
	{
		if (ctx.isModified(customerModel, CustomerModel.TITLE))
		{
			return !Objects.equals(getModelContext().getOriginalValue(CustomerModel.TITLE), customerModel.getTitle());
		}
		return false;
	}

	private boolean isNameModified(final CustomerModel customerModel, final InterceptorContext ctx)
	{
		if (ctx.isModified(customerModel, CustomerModel.NAME))
		{
			return !StringUtils.equals(getModelContext().getOriginalValue(CustomerModel.NAME), customerModel.getName());
		}
		return false;
	}

	private boolean isDefaultShipmentAddressModified(final CustomerModel customerModel, final InterceptorContext ctx)
	{
		if (ctx.isModified(customerModel, CustomerModel.DEFAULTSHIPMENTADDRESS))
		{
			final AddressModel oldDefaultShipmentAddress = getModelContext().getOriginalValue(CustomerModel.DEFAULTSHIPMENTADDRESS);
			final AddressModel currentDefaultShipmentAddress = customerModel.getDefaultShipmentAddress();

			// requires Date comparison when same default shipment address is modified
			final Date lastProfileUpdateTime = customerModel.getProfileUpdateTime();
			final Date addressModifiedTime = currentDefaultShipmentAddress != null ? currentDefaultShipmentAddress.getModifiedtime()
					: null;

			return !Objects.equals(oldDefaultShipmentAddress, currentDefaultShipmentAddress) || (lastProfileUpdateTime != null
					&& addressModifiedTime != null && addressModifiedTime.after(lastProfileUpdateTime));
		}
		return false;
	}

	protected ItemModelContext getModelContext()
	{
		return customerContext;
	}

	private void setModelContext(final CustomerModel customerModel)
	{
		customerContext = ModelContextUtils.getItemModelContext(customerModel);
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

}
