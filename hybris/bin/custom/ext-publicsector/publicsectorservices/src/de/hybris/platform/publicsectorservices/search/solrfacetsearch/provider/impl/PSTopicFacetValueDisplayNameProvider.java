/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.publicsectorservices.enums.PSTopic;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import org.apache.commons.lang3.StringUtils;


/**
 * Topic Facet Value display name provider to provide display name according to the language in the session.
 */
public class PSTopicFacetValueDisplayNameProvider extends AbstractPSFacetValueDisplayNameProvider
{

	@Override
	public String getDisplayName(final SearchQuery query, final IndexedProperty indexedProperty, final String facetValue)
	{
		if (facetValue == null)
		{
			return "";
		}

		final HybrisEnumValue topicEnumValue = getEnumerationService().getEnumerationValue(PSTopic.class, facetValue);

		String topicName = getEnumerationService().getEnumerationName(topicEnumValue, getQueryLocale(query));
		if (StringUtils.isEmpty(topicName))
		{
			topicName = facetValue;
		}

		return topicName;
	}

}
