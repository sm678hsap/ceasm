/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.template.velocity.context;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;

import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.tools.generic.EscapeTool;


/**
 * Abstract YForm Context
 */
public abstract class AbstractYFormContext extends VelocityContext
{
	public static final String BASE_SITE = "baseSite";
	public static final String YFORM_DATA = "formData";
	public static final String ESCAPE_TOOL = "esc";

	public void init(final BaseSiteModel baseSite, final Map<String, String> yFormDataMap)
	{
		put(BASE_SITE, baseSite);
		put(YFORM_DATA, yFormDataMap);
		put(ESCAPE_TOOL, new EscapeTool());
	}
}
