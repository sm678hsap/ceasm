/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.retention.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.commerceservices.consent.dao.ConsentDao;
import de.hybris.platform.commerceservices.model.consent.ConsentModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AbstractContactInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.customerreview.model.CustomerReviewModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * This Hook removes customer related objects such as addresses, payment methods, carts and contact infos.
 *
 */
public class PSServicesCustomerCleanupHook extends PSAbstractItemCleanupHook<CustomerModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PSServicesCustomerCleanupHook.class);

	private static final String CUSTOMER_PROCESSES_QUERY = "SELECT {" + StoreFrontCustomerProcessModel.PK + "} FROM {"
			+ StoreFrontCustomerProcessModel._TYPECODE + "} " + "WHERE {" + StoreFrontCustomerProcessModel.CUSTOMER + "} = ?user";

	private ConsentDao consentDao;

	@Override
	public void cleanupRelatedObjects(final CustomerModel customerModel)
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Cleaning up customer related objects for: {}", customerModel);
		}

		// remove addresses and addresses audit records
		for (final AddressModel address : customerModel.getAddresses())
		{
			getModelService().remove(address);
			removeAuditRecords(address);
		}

		// remove carts and carts audit records
		for (final CartModel cart : customerModel.getCarts())
		{
			getModelService().remove(cart);
			removeAuditRecords(cart);
		}

		// remove contact infos and contact infos audit records
		for (final AbstractContactInfoModel contactInfo : customerModel.getContactInfos())
		{
			getModelService().remove(contactInfo);
			removeAuditRecords(contactInfo);
		}

		// remove comments and comments audit records
		for (final CommentModel comment : customerModel.getComments())
		{
			getModelService().remove(comment);
			removeAuditRecords(comment);
		}

		// remove product reviews and reviews audit records
		for (final CustomerReviewModel review : customerModel.getCustomerReviews())
		{
			getModelService().remove(review);
			removeAuditRecords(review);
		}

		// remove consents and consents audit records
		for (final ConsentModel consent : getConsentDao().findAllConsentsByCustomer(customerModel))
		{
			getModelService().remove(consent);
			removeAuditRecords(consent);
		}

		final FlexibleSearchQuery customerProcessesQuery = new FlexibleSearchQuery(CUSTOMER_PROCESSES_QUERY);
		customerProcessesQuery.addQueryParameter("user", customerModel);
		final SearchResult<StoreFrontCustomerProcessModel> customerProcessSearchResult = getFlexibleSearchService()
				.search(customerProcessesQuery);

		for (final StoreFrontCustomerProcessModel customerProcess : customerProcessSearchResult.getResult())
		{
			getModelService().remove(customerProcess);
			removeAuditRecords(customerProcess);
		}
	}

	protected ConsentDao getConsentDao()
	{
		return consentDao;
	}

	@Required
	public void setConsentDao(final ConsentDao consentDao)
	{
		this.consentDao = consentDao;
	}

}
