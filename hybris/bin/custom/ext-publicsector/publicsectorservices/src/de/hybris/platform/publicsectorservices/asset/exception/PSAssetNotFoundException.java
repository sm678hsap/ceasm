/**
 *
 */
package de.hybris.platform.publicsectorservices.asset.exception;

/**
 * Exception thrown when as asset is not found for a given asset code.
 */
public class PSAssetNotFoundException extends Exception
{
	/**
	 * Constructs a PSAssetNotFoundException with null as its detailed message.
	 *
	 */
	public PSAssetNotFoundException()
	{

	}

	/**
	 * Constructs a PSAssetNotFoundException with the specified detail message.
	 *
	 * @param message
	 */
	public PSAssetNotFoundException(final String message)
	{
		super(message);
	}

}
