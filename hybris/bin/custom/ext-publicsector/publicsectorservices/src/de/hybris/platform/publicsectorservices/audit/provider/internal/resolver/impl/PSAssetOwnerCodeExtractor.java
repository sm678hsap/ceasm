/**
 *
 */
package de.hybris.platform.publicsectorservices.audit.provider.internal.resolver.impl;

import de.hybris.platform.audit.provider.internal.resolver.AuditRecordInternalProvider;
import de.hybris.platform.audit.provider.internal.resolver.VirtualReferenceValuesExtractor;
import de.hybris.platform.audit.provider.internal.resolver.impl.AuditTypeContext;
import de.hybris.platform.persistence.audit.internal.AuditRecordInternal;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Extracts assetcode from PSAssetOwner and fetches the data based on it
 *
 */

public class PSAssetOwnerCodeExtractor implements VirtualReferenceValuesExtractor
{
	@Override
	public <AUDITRECORD extends AuditRecordInternal> List<AUDITRECORD> extractValues(
			final AuditRecordInternalProvider<AUDITRECORD> provider, final AuditTypeContext<AUDITRECORD> ctx)
	{
		final Set<AUDITRECORD> assetOwnerData = ctx.getPayloadsForBasePKs();


		final Set<Object> values = new HashSet<>();
		for (final AUDITRECORD entree : assetOwnerData)
		{
			final String assetCode = entree.getAttribute("assetcode").toString();

			values.add(assetCode);
		}

		return provider.queryRecords(values);
	}
}