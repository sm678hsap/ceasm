/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorservices.order;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;


/**
 * This is an interface for the Public Sector Cart service methods.
 */
public interface PSCartService
{

	/**
	 * Sets returnURL and lastAccessDate in cart model for draft carts
	 *
	 * @param returnURL
	 */
	void setDraftCartDetails(String returnURL);

	/**
	 * To update the cart when YForm version changed for saved YFormData in cart entries. This method remove all the bundle
	 * entries. And set Delivery Address, Payment Address, Delivery Mode, Payment Info to Null.
	 */
	void updateCartOnYFormVersionUpdate();

	/**
	 * Gets cartModel for the cart associated with email and draft number
	 *
	 * @param code
	 *           code associated to a saved cart
	 * @param email
	 *           Guest user's email
	 * @return CartModel
	 */
	CartModel getCartForGuestUserEmailAndCartCode(String code, String email);

	/**
	 * returns whether the given cart is valid for given user
	 *
	 *
	 * @param code
	 *           code associated to a saved cart
	 * @param user
	 *           current user's model object
	 * @return boolean value based on the user has a cart or not
	 */
	boolean isCartAssociatedWithUser(String code, UserModel user);

	/**
	 * verifies and removes the cart associated with user
	 *
	 * @param cartCode
	 *           draftCode
	 * @param user
	 *           current user's model object
	 */
	void removeCart(String cartCode, final UserModel user);

	/**
	 * returns whether the given cart is saved draft or not
	 *
	 * @param cart
	 *           Model object for type Cart
	 * @return boolean value if the given cart is saved draft or not
	 */
	boolean isCartSavedDraft(final CartModel cart);

	/**
	 * Recalculate the cart model
	 *
	 * @param cart
	 *           Model object for type Cart
	 */
	void recalculateCart(final CartModel cart);

	/**
	 * Gets Saved drafts for given user and site. This will load all user drafts including ones being used by a relationship
	 * user and excluding drafts where the user maybe an owner but placing the order on behalf of someone else.
	 *
	 * @param pageableData
	 *           PageableData data object for page details
	 * @param baseSite
	 *           Model object for type BaseSite
	 * @param user
	 *           current user's model object
	 * @param orderStatus
	 *           List<OrderStatus> of status applicable to orders
	 * @return SearchPageData<CartModel>
	 *
	 */
	SearchPageData<CartModel> getSavedDraftsForSiteAndUser(final PageableData pageableData, final BaseSiteModel baseSite,
			final UserModel user, final List<OrderStatus> orderStatus);
}
