/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorservices.asset;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.asset.exception.PSAssetNotFoundException;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;

import java.util.List;


/**
 * This is an interface for the Public Sector asset service methods.
 */
public interface PSAssetService
{

	/**
	 * gets the assets list for given user
	 *
	 * @param user
	 *           UserModel for user details
	 * @return PSAssetModel
	 */
	List<PSAssetModel> getAssetsForUser(UserModel user);

	/**
	 * Get the details of an asset.
	 *
	 * @param assetCode
	 *           code of the asset
	 * @return PSAssetModel
	 * @throws PSAssetNotFoundException
	 *            if asset is not found for the assetCode
	 */
	PSAssetModel getAssetByCode(String assetCode) throws PSAssetNotFoundException;

	/**
	 * Get all asset attributes.
	 *
	 * @param assetCode
	 *           code of the asset
	 * @return List<PSAssetAttributeModel>
	 */
	List<PSAssetAttributeModel> getAssetAttributesByCode(String assetCode);

	/**
	 * Get display names for all attributes of an asset.
	 *
	 * @param assetType
	 *           type of the asset
	 * @param assetAttributeNames
	 *           List<String> of PSAssetAttributeData asset attribute names
	 * @return List<PSAssetAttributeMappingModel> Returns list of PSAssetAttributeMappingModel
	 */
	List<PSAssetAttributeMappingModel> getAssetAttributesMapping(String assetType, List<String> assetAttributeNames);
}
