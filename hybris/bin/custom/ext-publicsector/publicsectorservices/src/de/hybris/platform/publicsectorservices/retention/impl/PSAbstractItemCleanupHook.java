package de.hybris.platform.publicsectorservices.retention.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.persistence.audit.AuditScopeInvalidator;
import de.hybris.platform.persistence.audit.gateway.WriteAuditGateway;
import de.hybris.platform.retention.hook.ItemCleanupHook;
import de.hybris.platform.servicelayer.model.AbstractItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Required;

/**
 * Abstract class to encapsulate common methods that will be used by Public Sector Services
 * @param <T>
 */
public abstract class PSAbstractItemCleanupHook<T extends ItemModel>  implements ItemCleanupHook<T> {

    private FlexibleSearchService flexibleSearchService;
    private ModelService modelService;
    private WriteAuditGateway writeAuditGateway;
    private AuditScopeInvalidator auditScopeInvalidator;

    protected void removeAuditRecords(AbstractItemModel item) {
        this.writeAuditGateway.removeAuditRecordsForType(item.getItemtype(), item.getPk());
        this.auditScopeInvalidator.clearCurrentAuditForPK(item.getPk());
    }

    protected ModelService getModelService()
    {
        return modelService;
    }

    @Required
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    protected FlexibleSearchService getFlexibleSearchService()
    {
        return flexibleSearchService;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }

    protected WriteAuditGateway getWriteAuditGateway()
    {
        return this.writeAuditGateway;
    }

    @Required
    public void setWriteAuditGateway(WriteAuditGateway writeAuditGateway) {
        this.writeAuditGateway = writeAuditGateway;
    }

    protected AuditScopeInvalidator getAuditScopeInvalidator()
    {
        return this.auditScopeInvalidator;
    }

    @Required
    public void setAuditScopeInvalidator(AuditScopeInvalidator auditScopeInvalidator) {
        this.auditScopeInvalidator = auditScopeInvalidator;
    }

}
