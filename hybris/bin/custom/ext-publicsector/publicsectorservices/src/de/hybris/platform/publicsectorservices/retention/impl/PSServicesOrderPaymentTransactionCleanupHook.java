package de.hybris.platform.publicsectorservices.retention.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Cleanup Hook to be injected in the orders cleanup action to remove Payment Transactions and their entries
 */
public class PSServicesOrderPaymentTransactionCleanupHook extends PSAbstractItemCleanupHook<OrderModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(PSServicesOrderPaymentTransactionCleanupHook.class);

	@Override
	public void cleanupRelatedObjects(final OrderModel orderModel)
	{

		validateParameterNotNullStandardMessage("orderModel", orderModel);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Cleaning up payment transactions and their entries for Order : {}", orderModel);
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Found {} PaymentTransactionModel to be cleaned up",
					orderModel.getPaymentTransactions() != null ? orderModel.getPaymentTransactions().size() : 0);
		}

		for (final PaymentTransactionModel ptModel : orderModel.getPaymentTransactions())
		{

			cleanupPaymentTransactionEntries(ptModel);

			getModelService().remove(ptModel);
			removeAuditRecords(ptModel);

		}

	}

	private void cleanupPaymentTransactionEntries( PaymentTransactionModel ptModel) {

		validateParameterNotNullStandardMessage("paymentTransactiontModel", ptModel);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Found {} PaymentTransactionEntryModel to be cleaned up",
					ptModel.getEntries() != null ? ptModel.getEntries().size() : 0);
		}

		for (final PaymentTransactionEntryModel ptEntryModel : ptModel.getEntries())
		{
			getModelService().remove(ptEntryModel);
			removeAuditRecords(ptEntryModel);
		}


	}

}
