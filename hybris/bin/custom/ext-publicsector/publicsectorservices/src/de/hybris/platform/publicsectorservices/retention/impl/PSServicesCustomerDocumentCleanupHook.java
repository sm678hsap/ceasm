/**
 *
 */
package de.hybris.platform.publicsectorservices.retention.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentTagModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Class used to remove the documents related info from a customer
 *
 */
public class PSServicesCustomerDocumentCleanupHook extends PSAbstractItemCleanupHook<CustomerModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(PSServicesCustomerDocumentCleanupHook.class);

	@Override
	public void cleanupRelatedObjects(final CustomerModel customerModel)
	{

		validateParameterNotNullStandardMessage("customerModel", customerModel);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Cleaning up document related objects for Customer : {}", customerModel);
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Found {} PSDocumentModel to be cleaned up",
					customerModel.getPsDocument() != null ? customerModel.getPsDocument().size() : 0);
		}

		for (final PSDocumentModel docModel : customerModel.getPsDocument())
		{

			if (LOG.isDebugEnabled())
			{
				LOG.debug("Found {} PSDocumentTagModel to be cleaned up",
						docModel.getPsDocumentTag() != null ? docModel.getPsDocumentTag().size() : 0);
			}

			for (final PSDocumentTagModel docTagModel : docModel.getPsDocumentTag())
			{
				getModelService().remove(docTagModel);
				this.removeAuditRecords(docTagModel);
			}

			getModelService().remove(docModel);
			this.removeAuditRecords(docModel);

		}

	}
}
