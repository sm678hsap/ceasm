/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.template.velocity.context;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;

import java.util.Map;


/**
 * Review Page Context
 */
public class ReviewPageYFormContext extends AbstractYFormContext
{

	@Override
	public void init(final BaseSiteModel baseSite, final Map<String, String> yFormDataMap)
	{
		super.init(baseSite, yFormDataMap);

	}

}
