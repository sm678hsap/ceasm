/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.encoder;

/**
 * PublicSectorEncoder class to encode and decode data
 */
public interface PSEncoder
{
	/**
	 * Method to encrypt data using lock password
	 *
	 * @deprecated since 3.1, use {@link PSEncoder#encrypt(String)} instead. To be removed in next releases.
     * @param data String to be decrypted
     * @param lockPassword String used as encryption key. Not required anymore, hence deprecating the method
	 * @return encrypted string
	 */
	@Deprecated
	String encrypt(String data, String lockPassword);

	/**
	 * Encrypt document's file path.
	 *
	 * @param data
	 *           Document's file path
	 * @return encrypted file path
	 */
	String encrypt(String data);

	/**
	 * Method to decrypt data using unlock password
	 *
	 * @deprecated since 3.1, use {@link PSEncoder#decrypt(String)} instead. To be removed in next releases.
     * @param data String to be decrypted
     * @param unlockPassword String used as decryption key. Not required anymore, hence deprecating the method
	 * @return decrypted string
	 */
	@Deprecated
	String decrypt(String data, String unlockPassword);

	/**
	 * Decrypt document's file path.
	 *
	 * @param data
	 *           Encrypted file path of a document
	 * @return decrypted file path
	 */
	String decrypt(String data);
}
