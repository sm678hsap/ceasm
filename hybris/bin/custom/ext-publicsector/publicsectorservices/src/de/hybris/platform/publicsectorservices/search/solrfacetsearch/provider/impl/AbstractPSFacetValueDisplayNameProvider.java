/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider.impl;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider.PSFacetValueDisplayNameProvider;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Required;


/**
 * Abstract implementation of PublicSectorFacetValueDisplayNameProvider and FacetDisplayNameProvider
 */
public abstract class AbstractPSFacetValueDisplayNameProvider implements PSFacetValueDisplayNameProvider, FacetDisplayNameProvider
{
	private I18NService i18nService;
	private CommonI18NService commonI18NService;
	private EnumerationService enumerationService;

	@Override
	public abstract String getDisplayName(SearchQuery paramSearchQuery, IndexedProperty paramIndexedProperty, String paramString);

	@Override
	public Locale getQueryLocale(final SearchQuery query)
	{
		Locale queryLocale = null;
		if (query == null || query.getLanguage() == null || query.getLanguage().isEmpty())
		{
			queryLocale = getI18nService().getCurrentLocale();
		}

		if (queryLocale == null && query != null)
		{
			queryLocale = getCommonI18NService().getLocaleForLanguage(getCommonI18NService().getLanguage(query.getLanguage()));
		}
		return queryLocale;
	}
   
	/**
	 * @deprecated this is deprecated and replaced by {@link FacetValueDisplayNameProvider#getDisplayName(SearchQuery, IndexedProperty, String)} method.
	 */
	@SuppressWarnings("deprecation")
	@Deprecated
	@Override
	public final String getDisplayName(final SearchQuery query, final String name)
	{
		throw new IllegalStateException(
				"Do not call the FacetDisplayNameProvider#getDisplayName method call the FacetValueDisplayNameProvider#getDisplayName method instead.");
	}

	protected final I18NService getI18nService()
	{
		return i18nService;
	}

	@Required
	public final void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}

	protected final CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public final void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected final EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public final void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}
}
