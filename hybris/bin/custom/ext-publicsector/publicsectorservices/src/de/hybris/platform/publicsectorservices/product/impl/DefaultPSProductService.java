/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.product.impl;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.product.PSProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * Default implementation of the {@link PSProductService}.
 */
public class DefaultPSProductService implements PSProductService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSProductService.class);

	private CartService cartService;
	private CustomerAccountService customerAccountService;
	private BaseStoreService baseStoreService;

	/**
	 * Returns the ServiceProduct from the specified AbstractOrderModel. As AbstractOrderModel contains order entries
	 * with ServiceProduct and ServiceProductAddon.
	 *
	 * @param orderModel
	 * @return PSServiceProductModel
	 */
	@Override
	public PSServiceProductModel getServiceProductFromOrder(final AbstractOrderModel orderModel)
	{
		Assert.notNull(orderModel, "orderModel should not be null");

		final Optional<AbstractOrderEntryModel> orderEntry = orderModel.getEntries().stream()
				.filter(e -> e.getProduct() instanceof PSServiceProductModel).findFirst();

		return orderEntry.isPresent() ? (PSServiceProductModel) orderEntry.get().getProduct() : null;
	}

	/**
	 * Returns the ServiceProduct from the current session cart.
	 *
	 * @return PSServiceProductModel
	 */
	@Override
	public PSServiceProductModel getServiceProductFromSessionCart()
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel cartModel = getCartService().getSessionCart();
			return getServiceProductFromOrder(cartModel);
		}
		LOG.debug("no cart available for the session");
		return null;
	}

	/**
	 * Returns the ServiceProduct from the order with given orderCode.
	 *
	 * @param orderCode
	 * @return PSServiceProductModel
	 */
	@Override
	public PSServiceProductModel getServiceProductForOrderCode(final String orderCode)
	{
		final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
		final OrderModel orderModel = getCustomerAccountService().getOrderForCode(orderCode, baseStoreModel);
		if (orderModel == null)
		{
			throw new UnknownIdentifierException("Order with orderCode " + orderCode + " not found in current BaseStore");
		}
		return getServiceProductFromOrder(orderModel);
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

}
