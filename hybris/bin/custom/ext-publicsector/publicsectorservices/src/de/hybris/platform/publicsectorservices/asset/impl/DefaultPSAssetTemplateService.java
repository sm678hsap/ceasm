/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.asset.impl;

import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.publicsectorservices.asset.PSAssetTemplateService;
import de.hybris.platform.publicsectorservices.template.context.PSAssetAttributesContextFactory;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.StringWriter;
import java.util.List;

import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Service class to create a RendererTemplate for the asset type, create velocity context with asset attribute mapping
 * data. Returns the velocity template as a String.
 */
public class DefaultPSAssetTemplateService implements PSAssetTemplateService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSAssetTemplateService.class);
	private static final String ASSET_ATTRIBUTES_TEMPLATE_PREFIX = "asset_details_attributes_";

	private RendererService rendererService;
	private ConfigurationService configurationService;
	private PSAssetAttributesContextFactory assetAttributesContextFactory;

	@Override
	public String generateAssetAttributesTemplate(final String assetType, final List<PSAssetAttributeData> assetAttributesData,
			final List<PSAssetAttributeMappingData> assetAttributeMappingsData)
	{
		final RendererTemplateModel template = getTemplateModel(assetType);
		final StringWriter renderedBody = new StringWriter();

		final VelocityContext assetAttributesContext = getAssetAttributesContextFactory().create(template, assetAttributesData,
				assetAttributeMappingsData);
		getRendererService().render(template, assetAttributesContext, renderedBody);
		return renderedBody.getBuffer().toString();
	}

	protected RendererTemplateModel getTemplateModel(final String assetType)
	{
		final String templateName = ASSET_ATTRIBUTES_TEMPLATE_PREFIX + assetType;
		RendererTemplateModel templateModel = null;
		try
		{
			templateModel = getRendererService().getRendererTemplateForCode(templateName);
		}
		catch (final Exception e)
		{
			LOG.error("No velocity template found for asset type: {}", assetType, e);
			templateModel = getRendererService().getRendererTemplateForCode(ASSET_ATTRIBUTES_TEMPLATE_PREFIX + "generic_template");
		}

		return templateModel;
	}

	public RendererService getRendererService()
	{
		return rendererService;
	}

	@Required
	public void setRendererService(final RendererService rendererService)
	{
		this.rendererService = rendererService;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public PSAssetAttributesContextFactory getAssetAttributesContextFactory()
	{
		return assetAttributesContextFactory;
	}

	@Required
	public void setAssetAttributesContextFactory(final PSAssetAttributesContextFactory assetAttributesContextFactory)
	{
		this.assetAttributesContextFactory = assetAttributesContextFactory;
	}

}
