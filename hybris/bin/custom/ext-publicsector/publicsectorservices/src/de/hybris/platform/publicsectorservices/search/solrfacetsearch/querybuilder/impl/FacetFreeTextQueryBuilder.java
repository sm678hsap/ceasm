/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.querybuilder.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.querybuilder.impl.DefaultFreeTextQueryBuilder;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;


/**
 * This is FacetFreeTextQueryBuilder, this also builds query that will query the facet indexed properties.
 */
public class FacetFreeTextQueryBuilder extends DefaultFreeTextQueryBuilder
{

	@Override
	protected void addFreeTextQuery(final SearchQuery searchQuery, final IndexedProperty indexedProperty, final String value,
			final double boost)
	{
		final String field = indexedProperty.getName();
		if ("text".equalsIgnoreCase(indexedProperty.getType()))
		{
			addFreeTextQuery(searchQuery, field, value.toLowerCase(), "", boost);
			addFreeTextQuery(searchQuery, field, value.toLowerCase(), "*", boost / 2.0d);
			addFreeTextQuery(searchQuery, field, value.toLowerCase(), "~", boost / 4.0d);
		}
		else
		{
			addFreeTextQuery(searchQuery, field, value.toLowerCase(), "", boost);
			addFreeTextQuery(searchQuery, field, value.toLowerCase(), "*", boost / 2.0d);
		}
	}
}
