/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.event;


import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.publicsectorservices.model.PSDraftProcessModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * <DraftConfirmationEmailEventListener> Listener for <DraftConfirmationEmailEvent>
 */
public class DraftConfirmationEmailEventListener extends AbstractEventListener<DraftConfirmationEmailEvent>
{
	private static final Logger LOG = LoggerFactory.getLogger(DraftConfirmationEmailEventListener.class);

	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	@Override
	protected void onEvent(final DraftConfirmationEmailEvent event)
	{
		final PSDraftProcessModel psDraftProcessModel = (PSDraftProcessModel) getBusinessProcessService().createProcess(
				"draftConfirmationEmailProcess-" + event.getCartModel().getCode() + "-" + System.currentTimeMillis(),
				"draftConfirmationEmailProcess");
		psDraftProcessModel.setSite(event.getSite());
		psDraftProcessModel.setCustomer(event.getCustomer());
		psDraftProcessModel.setStore(event.getBaseStore());
		psDraftProcessModel.setCurrency(event.getCurrency());
		psDraftProcessModel.setLanguage(event.getLanguage());
		psDraftProcessModel.setServiceProductName(getServiceProductName(event.getCartModel()));
		getModelService().save(psDraftProcessModel);
		LOG.info("Starting save draft business service for customer {} and for cart {}", event.getCustomer().getUid(),
				event.getCartModel().getCode());
		getBusinessProcessService().startProcess(psDraftProcessModel);
	}

	private String getServiceProductName(final CartModel cart)
	{

		final List<AbstractOrderEntryModel> entries = cart.getEntries();

		if (CollectionUtils.isNotEmpty(entries))
		{
			return entries.stream().filter(p -> p.getProduct() instanceof PSServiceProductModel).findFirst()
					.map(p -> p.getProduct().getName()).orElse(null);
		}
		return null;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

}
