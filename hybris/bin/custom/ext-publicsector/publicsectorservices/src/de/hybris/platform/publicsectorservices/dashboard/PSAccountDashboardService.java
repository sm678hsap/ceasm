/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorservices.dashboard;

import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;


/**
 * This is an interface for the Public Sector Cart service methods.
 */
public interface PSAccountDashboardService
{

	/**
	 * Gets user's relationships bills for bill payment status (e.g. Unpaid, Part-paid, Paid)
	 *
	 * @param user
	 *           current user
	 * @param statuses
	 *           List of BillPaymentStatus
	 * @return list of user relationship bills
	 */
	List<PSBillPaymentModel> getBillsForUserRelationshipsByStatus(UserModel user, List<BillPaymentStatus> statuses);


}
