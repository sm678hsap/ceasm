/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.encoder.impl;

import de.hybris.platform.publicsectorservices.encoder.PSEncoder;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Default implementation of PublicSectorEncoder
 */
public class DefaultPSEncoder implements PSEncoder
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSEncoder.class);

	private static final String UTF8 = "UTF-8";

	private static final String ENCRYPT_FAIL_MSG = "Encryption failed.";
	private static final String DECRYPT_FAIL_MSG = "Decryption failed.";

	private static final String CONFIG_PARAM_ENCODER_ALGORITHM = "publicsectorservices.encoder.algorithm";
	private static final String CONFIG_PARAM_ENCODER_AES_KEY = "publicsectorservices.encoder.aes.key";
	private static final String CONFIG_PARAM_ENCRYPTION_CIPHER = "publicsectorservices.encoder.encryption.cipher";
	private static final String CONFIG_PARAM_INITIALIZATION_VECTOR = "publicsectorservices.encoder.initialization.vector";

	private static final String ALGORITHM;
	private static final String AES_KEY;
	private static final String ENCRYPTION_CIPHER;
	private static final String INITIALIZATION_VECTOR;
	private static final SecretKeySpec mSecretKeySpec;
	private static final IvParameterSpec mIvParameterSpec;

	static
	{
		ALGORITHM = Config.getParameter(CONFIG_PARAM_ENCODER_ALGORITHM);
		AES_KEY = Config.getParameter(CONFIG_PARAM_ENCODER_AES_KEY);

		ENCRYPTION_CIPHER = Config.getParameter(CONFIG_PARAM_ENCRYPTION_CIPHER);
		INITIALIZATION_VECTOR = Config.getParameter(CONFIG_PARAM_INITIALIZATION_VECTOR);

		mSecretKeySpec = new SecretKeySpec(AES_KEY.getBytes(), ALGORITHM);
		mIvParameterSpec = new IvParameterSpec(INITIALIZATION_VECTOR.getBytes());
	}

	/**
	 * @deprecated since 3.1, use {@link DefaultPSEncoder#encrypt(String)} instead. To be removed in next releases.
	 * @param data String to be decrypted
	 * @param lockPassword String used as encryption key. Not required anymore, hence deprecating the method
     * @return encrypted string
	 */
	@Deprecated
	@Override
	public String encrypt(final String data, final String lockPassword)
	{
		String encryptedData = null;
		try
		{
			final Cipher cipherInstance = Cipher.getInstance(ENCRYPTION_CIPHER);
			cipherInstance.init(Cipher.ENCRYPT_MODE, mSecretKeySpec, mIvParameterSpec);
			encryptedData = base64Encode(cipherInstance.doFinal(data.getBytes(UTF8)));
		}
		catch (UnsupportedEncodingException | GeneralSecurityException e)
		{
			LOG.error(ENCRYPT_FAIL_MSG, e);
		}

		return encryptedData;
	}

	@Override
	public String encrypt(final String data)
	{
		String encryptedData = null;
		try
		{
			final Cipher cipherInstance = Cipher.getInstance(ENCRYPTION_CIPHER);
			cipherInstance.init(Cipher.ENCRYPT_MODE, mSecretKeySpec, mIvParameterSpec);
			encryptedData = base64Encode(cipherInstance.doFinal(data.getBytes(UTF8)));
		}
		catch (UnsupportedEncodingException | GeneralSecurityException e)
		{
			LOG.error(ENCRYPT_FAIL_MSG, e);
		}

		return encryptedData;
	}

	private static String base64Encode(final byte[] bytes)
	{
		return Base64.getEncoder().encodeToString(bytes);
	}

	/**
	 * @deprecated since 3.1, use {@link DefaultPSEncoder#decrypt(String)} instead. To be removed in next releases.
     * @param data String to be decrypted
     * @param unlockPassword String used as decryption key. Not required anymore, hence deprecating the method
     * @return decrypted string
	 */
	@Deprecated
	@Override
	public String decrypt(final String data, final String unlockPassword)
	{
		String decryptedData = null;
		try
		{
			final Cipher cipherInstance = Cipher.getInstance(ENCRYPTION_CIPHER);
			cipherInstance.init(Cipher.DECRYPT_MODE, mSecretKeySpec, mIvParameterSpec);
			decryptedData = new String(cipherInstance.doFinal(base64Decode(data)), UTF8);
		}
		catch (IOException | GeneralSecurityException e)
		{
			LOG.error(DECRYPT_FAIL_MSG, e);
		}

		return decryptedData;
	}

	@Override
	public String decrypt(final String data)
	{
		String decryptedData = null;
		try
		{
			final Cipher cipherInstance = Cipher.getInstance(ENCRYPTION_CIPHER);
			cipherInstance.init(Cipher.DECRYPT_MODE, mSecretKeySpec, mIvParameterSpec);
			decryptedData = new String(cipherInstance.doFinal(base64Decode(data)), UTF8);
		}
		catch (IOException | GeneralSecurityException e)
		{
			LOG.error(DECRYPT_FAIL_MSG, e);
		}

		return decryptedData;
	}

	private static byte[] base64Decode(final String data)
	{
		return Base64.getDecoder().decode(data);
	}
}
