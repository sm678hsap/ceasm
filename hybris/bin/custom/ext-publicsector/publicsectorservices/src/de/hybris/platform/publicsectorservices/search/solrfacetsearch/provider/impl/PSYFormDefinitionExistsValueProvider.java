/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider.impl;

import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider.FieldType;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;



/**
 * This is a value provider class which checks if yform definition exists for a product and product can be applied
 * online and adds a flag for it appropriately.
 */
public class PSYFormDefinitionExistsValueProvider extends AbstractPropertyFieldValueProvider
		implements FieldValueProvider, Serializable
{

	private FieldNameProvider fieldNameProvider;

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof PSServiceProductModel)
		{
			final PSServiceProductModel serviceProduct = (PSServiceProductModel) model;

			final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

			if (CollectionUtils.isNotEmpty(serviceProduct.getAllYFormDefinitionList()))
			{
				fieldValues.add(createNewFieldValue(indexedProperty, Boolean.TRUE));
			}
			else
			{
				fieldValues.add(createNewFieldValue(indexedProperty, Boolean.FALSE));
			}
			return fieldValues;
		}
		return CollectionUtils.EMPTY_COLLECTION;
	}

	private FieldValue createNewFieldValue(final IndexedProperty indexedProperty, final Boolean value)
	{
		return new FieldValue(fieldNameProvider.getFieldName(indexedProperty, null, FieldType.INDEX), value);
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}
}
