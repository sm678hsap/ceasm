/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.strategies.impl;

import de.hybris.platform.commerceservices.strategies.impl.DefaultDeliveryModeLookupStrategy;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.publicsectorservices.product.PSProductService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * This is a Public Sector Delivery Mode Lookup Strategy extended {@link DefaultDeliveryModeLookupStrategy } to retrieve
 * delivery modes set on the Service Product if available else on the bundle products.
 *
 */
public class PSDeliveryModeLookupStrategy extends DefaultDeliveryModeLookupStrategy
{
	private PSProductService psProductService;

	/**
	 * This method overrides the strategy on finding delivery modes by order instead it will go over the list of delivery
	 * modes set on the product itself. The delivery mode is checked against the zone delivery modes applicable to the
	 * order. It will return the list of common delivery modes which exist on both product and zone delivery.
	 *
	 * It will check for service product and returns if available else it will check the bundle products for delivery
	 * modes
	 *
	 * @param abstractOrderModel
	 * @return List<DeliveryModeModel>
	 */
	@Override
	public List<DeliveryModeModel> getSelectableDeliveryModesForOrder(final AbstractOrderModel abstractOrderModel)
	{
		if (isPickUpOnlyOrder(abstractOrderModel))
		{
			return new ArrayList<>(getPickupDeliveryModeDao().findPickupDeliveryModesForAbstractOrder(abstractOrderModel));
		}
		else
		{
			final List<DeliveryModeModel> productDeliveryModes = new ArrayList<>();
			final ProductModel serviceProduct = getPsProductService().getServiceProductFromSessionCart();

			if (serviceProduct != null && CollectionUtils.isNotEmpty(serviceProduct.getDeliveryModes()))
			{
				addApplicableDeliveryModesForProduct(abstractOrderModel, productDeliveryModes, serviceProduct);
			}
			if (CollectionUtils.isEmpty(productDeliveryModes))
			{
				addApplicableDeliveryModesForServiceAddon(abstractOrderModel, productDeliveryModes, serviceProduct);
			}
			return productDeliveryModes;
		}
	}

	/**
	 * @param abstractOrderModel
	 * @param productDeliveryModes
	 * @param serviceProduct
	 */
	private void addApplicableDeliveryModesForServiceAddon(final AbstractOrderModel abstractOrderModel,
			final List<DeliveryModeModel> productDeliveryModes, final ProductModel serviceProduct)
	{
		for (final AbstractOrderEntryModel orderEntry : abstractOrderModel.getEntries())
		{
			final ProductModel product = orderEntry.getProduct();
			final boolean isProductsNotNull = serviceProduct != null && product != null;
			if (isProductsNotNull && !serviceProduct.getCode().equals(product.getCode()) && productDeliveryModes != null)
			{
				addApplicableDeliveryModesForProduct(abstractOrderModel, productDeliveryModes, product);
			}
			if (CollectionUtils.isNotEmpty(productDeliveryModes))
			{
				break;
			}
		}
	}

	/**
	 * @param abstractOrderModel
	 * @param productDeliveryModes
	 * @param product
	 */
	private void addApplicableDeliveryModesForProduct(final AbstractOrderModel abstractOrderModel,
			final List<DeliveryModeModel> productDeliveryModes, final ProductModel product)
	{
		final AddressModel deliveryAddress = abstractOrderModel.getDeliveryAddress();
		final CurrencyModel currency = abstractOrderModel.getCurrency();
		if (currency != null && deliveryAddress != null && deliveryAddress.getCountry() != null)
		{
			addApplicableZoneDeliveryModesForProduct(abstractOrderModel, productDeliveryModes, product);
		}
		else
		{
			if (CollectionUtils.isNotEmpty(product.getDeliveryModes()) && productDeliveryModes != null)
			{
				productDeliveryModes.addAll(product.getDeliveryModes());
			}
		}
	}

	/**
	 * @param abstractOrderModel
	 * @param productDeliveryModes
	 * @param product
	 */
	private void addApplicableZoneDeliveryModesForProduct(final AbstractOrderModel abstractOrderModel,
			final List<DeliveryModeModel> productDeliveryModes, final ProductModel product)
	{
		final List<DeliveryModeModel> zoneDeliveryModes = (List<DeliveryModeModel>) getCountryZoneDeliveryModeDao()
				.findDeliveryModes(abstractOrderModel);
		for (final DeliveryModeModel productDeliveryMode : product.getDeliveryModes())
		{
			if (zoneDeliveryModes.contains(productDeliveryMode) && !productDeliveryModes.contains(productDeliveryMode))
			{
				productDeliveryModes.add(productDeliveryMode);
			}
		}
	}

	protected PSProductService getPsProductService()
	{
		return psProductService;
	}

	@Required
	public void setPsProductService(final PSProductService psProductService)
	{
		this.psProductService = psProductService;
	}
}
