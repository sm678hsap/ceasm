package de.hybris.platform.publicsectorservices.retention.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.user.CustomerModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;
import de.hybris.platform.retention.hook.ItemCleanupHook;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import java.util.List;

/**
 * Class used to remove the assets and its related info from a customer
 */
public class PSServicesCustomerAssetsCleanupHook extends PSAbstractItemCleanupHook<CustomerModel>
{
    private static final Logger LOG = LoggerFactory.getLogger(PSServicesCustomerAssetsCleanupHook.class);

    private static final String ASSET_QUERY = "SELECT DISTINCT {psa.pk}, {psa.itemtype} FROM {PSAsset AS psa JOIN PSAssetOwner AS pso ON {psa.code} = {pso.assetCode} JOIN Customer AS c ON {pso.uid} = ?CUSTOMER_ID}";

    private List<ItemCleanupHook> itemCleanupHooks;

    @Override
    public void cleanupRelatedObjects(final CustomerModel customerModel)
    {

        validateParameterNotNullStandardMessage("customerModel", customerModel);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Cleaning up asset related objects for Customer : {}", customerModel);
        }

        // find, retrieve and remove asset records
        final FlexibleSearchQuery assetQuery = new FlexibleSearchQuery(ASSET_QUERY);
        assetQuery.addQueryParameter("CUSTOMER_ID", customerModel.getUid());
        final SearchResult<PSAssetModel> assetSearchResult = getFlexibleSearchService().search(assetQuery);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Found {} PSAssetModel to be cleaned up", assetSearchResult.getCount());
        }

        for(final PSAssetModel assetModel : assetSearchResult.getResult())
        {

            for ( ItemCleanupHook itemCleanupHook : getItemCleanupHooks() ) {
                itemCleanupHook.cleanupRelatedObjects(assetModel);
            }

            getModelService().remove(assetModel);
            this.removeAuditRecords(assetModel);

        }

    }

    protected List<ItemCleanupHook> getItemCleanupHooks() {
        return this.itemCleanupHooks;
    }

    @Required
    public void setItemCleanupHooks(List<ItemCleanupHook> itemCleanupHooks) {
        this.itemCleanupHooks = itemCleanupHooks;
    }

}