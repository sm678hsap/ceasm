/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.helper.impl;

import de.hybris.platform.publicsectorservices.helper.PSFilesHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Default implementation of PublicSectorFilesHelper
 */
public class DefaultPSFilesHelper implements PSFilesHelper
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSFilesHelper.class);

	@Override
	public String getImageAsEncodedString(final String fileLocation) throws IOException
	{
		final File actualFile = new File(fileLocation);
		if (actualFile.exists())
		{
			try (final FileInputStream fileInputStream = new FileInputStream(actualFile);
					final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream())
			{
				int b;
				final byte[] buffer = new byte[1024];
				while ((b = fileInputStream.read(buffer)) != -1)
				{
					byteArrayOutputStream.write(buffer, 0, b);
				}

				return Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
			}
			catch (final IOException e)
			{
				LOG.error("Exception occured while reading Image file {}", e);
			}
		}
		LOG.debug("file does not exists in the location {}", fileLocation);
		return null;
	}


}
