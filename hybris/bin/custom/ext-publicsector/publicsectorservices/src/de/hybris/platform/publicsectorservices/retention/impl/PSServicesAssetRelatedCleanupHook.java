package de.hybris.platform.publicsectorservices.retention.impl;

import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;
import de.hybris.platform.publicsectorservices.model.PSAssetOwnerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

/**
 * Class used to remove the assets and its related info from a customer
 */
public class PSServicesAssetRelatedCleanupHook extends PSAbstractItemCleanupHook<PSAssetModel>
{
    private static final Logger LOG = LoggerFactory.getLogger(PSServicesAssetRelatedCleanupHook.class);

    private static final String ASSET_OWNER_QUERY = "SELECT {pso:PK} FROM {PSAssetOwner AS pso} WHERE {pso:assetcode} = ?asset";

    private static final String ASSET_ATT_QUERY = "SELECT {psa:PK} FROM {PSAssetAttribute AS psa} WHERE {psa:assetcode} = ?asset";

    @Override
    public void cleanupRelatedObjects(final PSAssetModel assetModel)
    {
        validateParameterNotNullStandardMessage("assetModel", assetModel);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Cleaning up asset related objects for: {}", assetModel);
        }

        // find, retrieve and remove asset owner information
        final FlexibleSearchQuery assetOwnerQuery = new FlexibleSearchQuery(ASSET_OWNER_QUERY);
        assetOwnerQuery.addQueryParameter("asset", assetModel.getCode());
        final SearchResult<PSAssetOwnerModel> assetOwnerSearchResult = getFlexibleSearchService().search(assetOwnerQuery);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Found {} PSAssetOwnerModel to be cleaned up", assetOwnerSearchResult.getCount());
        }

        for(final PSAssetOwnerModel assetOwnerModel : assetOwnerSearchResult.getResult())
        {
            getModelService().remove(assetOwnerModel);
            this.removeAuditRecords(assetOwnerModel);
        }

        // find, retrieve and remove asset attribute information
        final FlexibleSearchQuery assetAttQuery = new FlexibleSearchQuery(ASSET_ATT_QUERY);
        assetAttQuery.addQueryParameter("asset", assetModel.getCode());
        final SearchResult<PSAssetAttributeModel> assetAttSearchResult = getFlexibleSearchService().search(assetAttQuery);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Found {} PSAssetAttributeModel to be cleaned up", assetAttSearchResult.getCount());
        }

        for(final PSAssetAttributeModel assetAttModel : assetAttSearchResult.getResult())
        {
            getModelService().remove(assetAttModel);
            removeAuditRecords(assetAttModel);
        }

    }

}