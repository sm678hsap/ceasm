/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.product;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;


/**
 * Service to read and update {@link PSServiceProductModel}s.
 *
 * @spring.bean publicSectorProductService
 */
public interface PSProductService
{

	/**
	 * Returns the ServiceProduct from the specified AbstractOrderModel. As AbstractOrderModel contains order entries
	 * with ServiceProduct and ServiceProductAddon.
	 *
	 * @param orderModel
	 *           represents AbstractOrderModel which contains order entries with ServiceProduct and ServiceProductAddon.
	 * @return PSServiceProductModel model object for type PSServiceProduct
	 */
	PSServiceProductModel getServiceProductFromOrder(AbstractOrderModel orderModel);

	/**
	 * Returns the ServiceProduct from the current session cart.
	 *
	 * @return PSServiceProductModel
	 */
	PSServiceProductModel getServiceProductFromSessionCart();

	/**
	 * Returns the ServiceProduct from the order with given orderCode.
	 *
	 * @param orderCode
	 *           orderCode
	 * @return PSServiceProductModel represents Generated model class for type PSServiceProduct
	 */
	PSServiceProductModel getServiceProductForOrderCode(String orderCode);

}
