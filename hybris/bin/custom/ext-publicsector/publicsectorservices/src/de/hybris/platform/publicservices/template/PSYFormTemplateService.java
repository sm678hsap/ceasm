/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicservices.template;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.xyformsfacades.data.YFormDataData;

import java.util.List;


/**
 * The interface for the PublicSectorYFormTemplateService
 *
 */
public interface PSYFormTemplateService
{

	/**
	 * Checks if the given service has custom defined template
	 *
	 * @param productCode
	 *           is string representation of productCode
	 * @return boolean value based on given service has custom defined template or not
	 */
	boolean isServiceHasTemplate(String productCode);

	/**
	 * Returns the template html as string for the given service
	 *
	 * @param product
	 *           - productData
	 * @param yFormDatas
	 *           - List<YFormDataData>
	 * @returns String
	 */
	String getTemplateHTMLForService(ProductData product, List<YFormDataData> yFormDatas);

}
