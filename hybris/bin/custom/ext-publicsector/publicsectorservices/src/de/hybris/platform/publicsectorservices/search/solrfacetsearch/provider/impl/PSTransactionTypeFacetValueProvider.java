/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider.impl;

import de.hybris.platform.publicsectorservices.enums.PSTransactionType;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider.FieldType;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * This is a provider to add data for the transaction.
 */
public class PSTransactionTypeFacetValueProvider extends AbstractPropertyFieldValueProvider
		implements FieldValueProvider, Serializable
{

	private transient FieldNameProvider fieldNameProvider;

	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof PSServiceProductModel)
		{
			final PSServiceProductModel serviceProduct = (PSServiceProductModel) model;

			final Collection<FieldValue> fieldValues = new ArrayList<>();

			if (CollectionUtils.isNotEmpty(serviceProduct.getTransactionTypes()))
			{
				for (final PSTransactionType applyOnline : serviceProduct.getTransactionTypes())
				{
					fieldValues.add(createNewFieldValue(indexedProperty, applyOnline.getCode()));
				}
				return fieldValues;
			}
		}
		return CollectionUtils.EMPTY_COLLECTION;
	}

	private FieldValue createNewFieldValue(final IndexedProperty indexedProperty, final String value)
	{
		return new FieldValue(getFieldNameProvider().getFieldName(indexedProperty, null, FieldType.INDEX), value);
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

}
