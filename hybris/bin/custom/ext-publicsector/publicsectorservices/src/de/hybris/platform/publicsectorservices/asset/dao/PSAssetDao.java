/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.asset.dao;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.asset.exception.PSAssetNotFoundException;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;

import java.util.List;


/**
 * Interface PSAssetDao
 */
public interface PSAssetDao
{
	/**
	 * gets the assets list for given user
	 *
	 * @param user
	 * @return List<PSAssetModel>
	 */
	List<PSAssetModel> getAssetsForUser(UserModel user);

	/**
	 * Get the asset details by asset code.
	 *
	 * @param assetCode
	 * @return PSAssetModel
	 */
	PSAssetModel getAssetByCode(String assetCode) throws PSAssetNotFoundException;

	/**
	 * Get attributes and display name for an asset.
	 *
	 * @param assetCode
	 * @return List of all attributes (meta-data) related to an asset.
	 */
	List<PSAssetAttributeModel> getAssetAttributesByAssetCode(String assetCode);

	/**
	 * Get display name of all attributes for an asset.
	 *
	 * @param assetType
	 * @param assetAttributeNames
	 * @return List of all attributes display name.
	 */
	List<PSAssetAttributeMappingModel> getAssetAttributesMapping(String assetType, List<String> assetAttributeNames);
}
