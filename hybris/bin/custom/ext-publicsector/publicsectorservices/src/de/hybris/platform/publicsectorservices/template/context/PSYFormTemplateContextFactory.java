/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorservices.template.context;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;

import java.util.Map;

import org.apache.velocity.VelocityContext;


/**
 * PublicSectorYFormTemplateContextFactory interface
 */
public interface PSYFormTemplateContextFactory
{

	/**
	 * Returns the velocity context for yForm which provide context data that can then be used for rendering the html
	 *
	 * @param yformData
	 * @param renderTemplate
	 * @return VelocityContext
	 */
	VelocityContext create(Map<String, String> yformData, RendererTemplateModel renderTemplate);
}
