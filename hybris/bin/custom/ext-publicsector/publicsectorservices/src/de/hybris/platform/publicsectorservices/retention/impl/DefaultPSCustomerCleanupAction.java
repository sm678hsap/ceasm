package de.hybris.platform.publicsectorservices.retention.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processing.model.AbstractRetentionRuleModel;
import de.hybris.platform.retention.ItemToCleanup;
import de.hybris.platform.retention.impl.AbstractExtensibleRemoveCleanupAction;
import de.hybris.platform.retention.job.AfterRetentionCleanupJobPerformable;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * Extended Customer Cleanup Action for Public Sector. Required to check whether the customer has orders, carts or any payment transaction paid by the customer.
 * Other required methods and attributes are available in the parent class.
 *
 */
public class DefaultPSCustomerCleanupAction extends AbstractExtensibleRemoveCleanupAction
{

	private static final String CUSTOMER_QUERY_PARAM = "customer";

	private static final String CUSTOMER_REFERENCE_ORDER = "SELECT {o:PK} FROM {Order AS o} WHERE {o:placedby} = ?customer";

	private static final String CUSTOMER_REFERENCE_CART = "SELECT {c:PK} FROM {Cart AS c} WHERE {c:savedby} = ?customer";

	private static final String CUSTOMER_REFERENCE_BILLS = "SELECT {pt:PK} FROM {PaymentTransaction AS pt} WHERE {pt:paidby} = ?customer";

	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSCustomerCleanupAction.class);

	private FlexibleSearchService flexibleSearchService;

	@Override
	public void cleanup(final AfterRetentionCleanupJobPerformable retentionJob, final AbstractRetentionRuleModel rule,
			final ItemToCleanup item)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("item to cleanup", item);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Cleaning up item and its audit records: {}", item.getPk());
		}

		final Object customer = getModelService().get(item.getPk());
		if (!(customer instanceof CustomerModel))
		{
			throw new IllegalStateException("Not instance of CustomerModel:" + item.getPk());
		}

		// find, retrieve and customer reference orders
		final FlexibleSearchQuery customerReferenceOrderQuery = new FlexibleSearchQuery(CUSTOMER_REFERENCE_ORDER);
		customerReferenceOrderQuery.addQueryParameter(CUSTOMER_QUERY_PARAM, customer);
		final SearchResult<OrderModel> customerReferenceOrderSearchResult = getFlexibleSearchService()
				.search(customerReferenceOrderQuery);
		final boolean isCustomerReferenceOrderExists = (customerReferenceOrderSearchResult.getCount() > 0);

		// find, retrieve and customer reference bills
		final FlexibleSearchQuery customerReferenceBillQuery = new FlexibleSearchQuery(CUSTOMER_REFERENCE_BILLS);
		customerReferenceBillQuery.addQueryParameter(CUSTOMER_QUERY_PARAM, customer);
		final SearchResult<PaymentTransactionModel> customerReferenceBillSearchResult = getFlexibleSearchService()
				.search(customerReferenceBillQuery);
		final boolean isCustomerReferenceBillExists = (customerReferenceBillSearchResult.getCount() > 0);

		// find, retrieve and customer reference carts
		final FlexibleSearchQuery customerReferenceCartQuery = new FlexibleSearchQuery(CUSTOMER_REFERENCE_CART);
		customerReferenceCartQuery.addQueryParameter(CUSTOMER_QUERY_PARAM, customer);
		final SearchResult<PaymentTransactionModel> customerReferenceCartSearchResult = getFlexibleSearchService()
				.search(customerReferenceCartQuery);
		final boolean isCustomerReferenceCartExists = (customerReferenceCartSearchResult.getCount() > 0);

		final CustomerModel customerModel = (CustomerModel) customer;
		cleanupRelatedObjects(customerModel);

		if (!isCustomerReferenceBillExists)
		{
			// remove payment methods and payment methods audit records
			for (final PaymentInfoModel paymentInfo : customerModel.getPaymentInfos())
			{
				//create an item cleanup builder and an item cleanup objects to pass to the removeAuditRecords method
				ItemToCleanup.Builder itemCleanupBuilder = ItemToCleanup.builder().withPK(paymentInfo.getPk());
				itemCleanupBuilder = itemCleanupBuilder.withItemType(paymentInfo.getItemtype());
				ItemToCleanup paymentInfoCleanup = itemCleanupBuilder.build();
				getModelService().remove(paymentInfoCleanup.getPk());
				removeAuditRecords(paymentInfoCleanup);
			}
		}

		if (!isCustomerReferenceOrderExists && !isCustomerReferenceBillExists && !isCustomerReferenceCartExists)
		{
			this.getModelService().remove(item.getPk());
			this.removeAuditRecords(item);
		}
	}

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

}