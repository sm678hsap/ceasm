/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.asset.dao.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.asset.dao.PSAssetDao;
import de.hybris.platform.publicsectorservices.asset.exception.PSAssetNotFoundException;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;
import de.hybris.platform.publicsectorservices.model.PSAssetOwnerModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;


/**
 * Implementation of {@link PSAssetDao}
 */
public class DefaultPSAssetDao extends AbstractItemDao implements PSAssetDao
{

	private static final String USER_NOT_NULL = "User should not be null";

	private static final String ASSET_CODE_NOT_NULL = "Asset code should not be null";

	protected static final String FIND_ASSETS_FOR_USER = "SELECT {" + PSAssetModel.PK + "} FROM {" + PSAssetModel._TYPECODE
			+ "} , {" + PSAssetOwnerModel._TYPECODE + "}  WHERE {" + PSAssetModel._TYPECODE + "." + PSAssetModel.CODE + "} = {"
			+ PSAssetOwnerModel._TYPECODE + "." + PSAssetOwnerModel.ASSETCODE + "} AND {" + PSAssetOwnerModel._TYPECODE + "."
			+ PSAssetOwnerModel.UID + "} = ?user AND {" + PSAssetOwnerModel._TYPECODE + "." + PSAssetOwnerModel.STARTDATE
			+ "} <= ?currentDate AND ({" + PSAssetOwnerModel._TYPECODE + "." + PSAssetOwnerModel.ENDDATE + "} >= ?currentDate OR {"
			+ PSAssetOwnerModel._TYPECODE + "." + PSAssetOwnerModel.ENDDATE + "} IS NULL)";

	protected static final String ASSET_BY_CODE = "SELECT {" + PSAssetModel.PK + "} FROM {" + PSAssetModel._TYPECODE + "} WHERE {"
			+ PSAssetModel._TYPECODE + "." + PSAssetModel.CODE + "} = ?" + PSAssetModel.CODE;

	protected static final String ASSET_ATTRIBUTES_BY_CODE = "SELECT {aa." + PSAssetAttributeModel.PK + "} FROM " + "{"
			+ PSAssetAttributeModel._TYPECODE + " AS aa } WHERE {aa." + PSAssetAttributeModel.ASSETCODE + "} = ?"
			+ PSAssetAttributeModel.ASSETCODE;

	protected static final String ASSET_ATTRIBUTES_MAPPING = "SELECT {aam." + PSAssetAttributeMappingModel.PK + "} FROM {"
			+ PSAssetAttributeMappingModel._TYPECODE + " AS aam} WHERE {aam." + PSAssetAttributeMappingModel.ASSETTYPECODE + "} = ?"
			+ PSAssetAttributeMappingModel.ASSETTYPECODE + " AND {aam." + PSAssetAttributeMappingModel.ATTRIBUTENAME
			+ "} IN (?assetAttributeNames)";

	@Override
	public List<PSAssetModel> getAssetsForUser(final UserModel user)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put("user", user.getUid());

		final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		params.put("currentDate", df.format(new Date()));

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ASSETS_FOR_USER);
		query.addQueryParameters(params);

		final SearchResult<PSAssetModel> assets = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(assets.getResult()) ? assets.getResult() : null;
	}

	@Override
	public PSAssetModel getAssetByCode(final String assetCode) throws PSAssetNotFoundException
	{
		ServicesUtil.validateParameterNotNull(assetCode, ASSET_CODE_NOT_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(ASSET_BY_CODE);
		query.addQueryParameter(PSAssetModel.CODE, assetCode);

		final SearchResult<PSAssetModel> asset = getFlexibleSearchService().search(query);
		if (asset.getCount() == 0)
		{
			throw new PSAssetNotFoundException(String.format("Asset with code [%s] not found", assetCode));
		}
		return asset.getResult().get(0);
	}

	@Override
	public List<PSAssetAttributeModel> getAssetAttributesByAssetCode(final String assetCode)
	{
		ServicesUtil.validateParameterNotNull(assetCode, ASSET_CODE_NOT_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(ASSET_ATTRIBUTES_BY_CODE);
		query.addQueryParameter(PSAssetAttributeModel.ASSETCODE, assetCode);

		final SearchResult<PSAssetAttributeModel> assetAttributes = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(assetAttributes.getResult()) ? assetAttributes.getResult() : null;
	}

	@Override
	public List<PSAssetAttributeMappingModel> getAssetAttributesMapping(final String assetType,
			final List<String> assetAttributeNames)
	{
		if (CollectionUtils.isEmpty(assetAttributeNames))
		{
			return Collections.emptyList();
		}
		final Map<String, Object> params = new HashMap();
		params.put(PSAssetAttributeMappingModel.ASSETTYPECODE, assetType);
		params.put("assetAttributeNames", assetAttributeNames);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(ASSET_ATTRIBUTES_MAPPING);
		query.addQueryParameters(params);

		final SearchResult<PSAssetAttributeMappingModel> assetAttributesMapping = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(assetAttributesMapping.getResult()) ? assetAttributesMapping.getResult() : null;
	}

}
