/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.process.strategies.impl;

import de.hybris.platform.acceleratorservices.process.strategies.impl.DefaultProcessContextResolutionStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.publicsectorservices.model.PSDraftProcessModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;


/**
 * Default strategy to impersonate site and initialize session context from the process model.
 */
public class PSProcessContextResolutionStrategy extends DefaultProcessContextResolutionStrategy
{

	private static final String BUSINESS_PROCESS_MUST_NOT_BE_NULL_MSG = "businessProcess must not be null";

	@Override
	public BaseSiteModel getCmsSite(final BusinessProcessModel businessProcess)
	{
		ServicesUtil.validateParameterNotNull(businessProcess, BUSINESS_PROCESS_MUST_NOT_BE_NULL_MSG);
		if (businessProcess instanceof PSDraftProcessModel)
		{
			return ((PSDraftProcessModel) businessProcess).getSite();
		}

		if (businessProcess instanceof PSRelationshipProcessModel)
		{
			return ((PSRelationshipProcessModel) businessProcess).getSite();
		}

		return super.getCmsSite(businessProcess);

	}
}
