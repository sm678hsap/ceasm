/**
 *
 */
package de.hybris.platform.publicsectorservices.audit.provider.internal.resolver.impl;

import de.hybris.platform.audit.provider.internal.resolver.AuditRecordInternalProvider;
import de.hybris.platform.audit.provider.internal.resolver.VirtualReferenceValuesExtractor;
import de.hybris.platform.audit.provider.internal.resolver.impl.AuditTypeContext;
import de.hybris.platform.persistence.audit.internal.AuditRecordInternal;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Extracts uid from User and fetches the data based on it
 *
 */
public class PSUserEmailIdExtractor implements VirtualReferenceValuesExtractor
{
	@Override
	public <AUDITRECORD extends AuditRecordInternal> List<AUDITRECORD> extractValues(
			final AuditRecordInternalProvider<AUDITRECORD> provider, final AuditTypeContext<AUDITRECORD> ctx)
	{
		final Set<AUDITRECORD> userData = ctx.getPayloadsForBasePKs();


		final Set<Object> values = new HashSet<>();
		for (final AUDITRECORD entree : userData)
		{
			final String userEmailId = entree.getAttribute("uid").toString();

			values.add(userEmailId);
		}

		return provider.queryRecords(values);
	}
}

