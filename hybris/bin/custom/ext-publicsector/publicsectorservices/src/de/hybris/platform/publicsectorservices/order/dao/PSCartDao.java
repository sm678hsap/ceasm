/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.order.dao;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;


/**
 * PublicSectorCartDao Interface
 */
public interface PSCartDao
{
	/**
	 * gets the cart for the given site and cart Code
	 *
	 * @param cartCode
	 * @param site
	 * @return CartModel
	 */
	CartModel getCartForCodeAndSite(final String cartCode, final BaseSiteModel site);

	/**
	 * Gets Saved drafts for given user and site. This will load all user drafts including ones being used by a
	 * relationship user and excluding drafts where the user maybe an owner but placing the order on behalf of someone
	 * else.
	 *
	 * @param pageableData
	 * @param baseSite
	 * @param user
	 * @param orderStatus
	 * @return SearchPageData<CartModel>
	 *
	 */
	SearchPageData<CartModel> getSavedDraftsForSiteAndUser(final PageableData pageableData, final BaseSiteModel baseSite,
			final UserModel user, final List<OrderStatus> orderStatus);
}
