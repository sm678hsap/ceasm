/*
 * [y] hybris Platform

 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.dashboard.dao;

import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;


/**
 * Interface class for PSAccountDashboard DAO.
 */
public interface PSAccountDashboardDao
{
	/**
	 * @param user
	 * @param serviceProductComposedType
	 * @param statuses
	 * @return List<PSBillPaymentModel>
	 */
	List<PSBillPaymentModel> getBillsForUserRelationshipsByStatus(UserModel user, ComposedTypeModel serviceProductComposedType,
			List<BillPaymentStatus> statuses);
}
