/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.strategies.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.xyformscommerceservices.strategies.impl.DefaultYFormUserStrategy;

import org.springframework.beans.factory.annotation.Required;


/**
 *
 * RelationshipGetUserStrategy extends {@link DefaultYFormUserStrategy}
 *
 */
public class PSRelationshipYFormUserStrategy extends DefaultYFormUserStrategy
{

	private CartService cartService;

	@Override
	public CustomerModel getCurrentUserForCheckout()
	{

		final CartModel cart = getCartService().hasSessionCart() ? getCartService().getSessionCart() : null;
		if (cart != null && cart.getUserInContext() != null)
		{
			return (CustomerModel) cart.getUserInContext();
		}
		else
		{
			return super.getCurrentUserForCheckout();
		}
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
