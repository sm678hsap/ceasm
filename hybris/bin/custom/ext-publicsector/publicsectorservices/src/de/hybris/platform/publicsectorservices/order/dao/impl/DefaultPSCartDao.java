/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.order.dao.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.dao.impl.DefaultCommerceCartDao;
import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.order.dao.PSCartDao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * implementation of {@link PSCartDao}
 */
public class DefaultPSCartDao extends DefaultCommerceCartDao implements PSCartDao
{
	protected final static String SAVED_CARTS_CLAUSE = "{" + CartModel.SAVETIME + "} IS NOT NULL";

	protected static final String FIND_CART_FOR_CODE_AND_CMS_SITE = SELECTCLAUSE + "WHERE {" + CartModel.CODE + "} = ?code  AND {"
			+ CartModel.SITE + "} = ?site " + ORDERBYCLAUSE;

	protected final static String DRAFTS_USER_CLAUSE = " ( {" + CartModel.USER + "} = ?user AND ( {" + CartModel.USERINCONTEXT
			+ "} IS NULL OR {" + CartModel.USERINCONTEXT + "} = ?user ) " + " OR {" + CartModel.USERINCONTEXT + "} = ?user ) ";

	protected final static String FIND_SAVED_CARTS_FOR_USER_AND_SITE = SELECTCLAUSE + "WHERE " + DRAFTS_USER_CLAUSE + " AND {"
			+ CartModel.SITE + "} = ?site AND " + SAVED_CARTS_CLAUSE + " ";

	protected final static String FIND_SAVED_CARTS_FOR_USER = SELECTCLAUSE + "WHERE " + DRAFTS_USER_CLAUSE + " AND "
			+ SAVED_CARTS_CLAUSE + " ";

	protected final static String FIND_SAVED_CARTS_FOR_SITE_AND_USER_WITH_STATUS = "SELECT {" + CartModel.PK + "} FROM {"
			+ CartModel._TYPECODE + "}, {" + OrderStatus._TYPECODE + "} " + "WHERE {" + CartModel._TYPECODE + "." + CartModel.STATUS
			+ "} = {" + OrderStatus._TYPECODE + ".pk} AND " + DRAFTS_USER_CLAUSE + " AND {" + CartModel.SITE + "} = ?site AND "
			+ SAVED_CARTS_CLAUSE + " AND {OrderStatus.CODE} in (?orderStatus) ";

	protected final static String FIND_SAVED_CARTS_FOR_USER_WITH_STATUS = "SELECT {" + CartModel.PK + "} FROM {"
			+ CartModel._TYPECODE + "}, {" + OrderStatus._TYPECODE + "} " + "WHERE {" + CartModel._TYPECODE + "." + CartModel.STATUS
			+ "} = {" + OrderStatus._TYPECODE + ".pk} AND " + DRAFTS_USER_CLAUSE + " AND " + SAVED_CARTS_CLAUSE
			+ " AND {OrderStatus.CODE} in (?orderStatus) ";

	protected static final String DATE_MODIFIED_SORT_CRITERIA = "{" + CartModel.MODIFIEDTIME + "} DESC";

	protected static final String SORT_SAVED_CARTS_BY_CODE = " ORDER BY {" + CartModel.CODE + "}, " + DATE_MODIFIED_SORT_CRITERIA;

	protected static final String SORT_SAVED_CARTS_BY_NAME = " ORDER BY {" + CartModel.NAME + "}, " + DATE_MODIFIED_SORT_CRITERIA;

	protected static final String SORT_SAVED_CARTS_BY_DATE_SAVED = " ORDER BY {" + CartModel.SAVETIME + "} DESC";

	protected static final String SORT_SAVED_CARTS_BY_TOTAL = " ORDER BY {" + CartModel.TOTALPRICE + "}, "
			+ DATE_MODIFIED_SORT_CRITERIA;

	protected static final String SORT_CODE_BY_DATE_MODIFIED = "byDateModified";
	protected static final String SORT_CODE_BY_DATE_SAVED = "byDateSaved";
	protected static final String SORT_CODE_BY_NAME = "byName";
	protected static final String SORT_CODE_BY_CODE = "byCode";
	protected static final String SORT_CODE_BY_TOTAL = "byTotal";
	protected static final String ORDER_STATUS_CODE_SEPERATOR = ",";

	private PagedFlexibleSearchService pagedFlexibleSearchService;
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSCartDao.class);

	@Override
	public CartModel getCartForCodeAndSite(final String cartCode, final BaseSiteModel site)
	{
		if (cartCode != null)
		{
			final Map<String, Object> params = new HashMap<>();
			params.put("code", cartCode);
			params.put("site", site);
			final List<CartModel> cartList = doSearch(FIND_CART_FOR_CODE_AND_CMS_SITE, params, CartModel.class);
			if (!CollectionUtils.isEmpty(cartList))
			{
				return cartList.get(0);
			}
			else
			{
				LOG.debug("No cart found for the given code {} and base site {}", cartCode, site.getUid());
			}
		}
		return null;
	}

	@Override
	public SearchPageData<CartModel> getSavedDraftsForSiteAndUser(final PageableData pageableData, final BaseSiteModel baseSite,
			final UserModel user, final List<OrderStatus> orderStatus)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("user", user);

		final String orderStatusList = formatOrderStatusList(orderStatus);
		final String query;
		if (baseSite != null)
		{
			params.put("site", baseSite);
			if (StringUtils.isNotBlank(orderStatusList))
			{

				params.put("orderStatus", orderStatusList);
				query = FIND_SAVED_CARTS_FOR_SITE_AND_USER_WITH_STATUS;
			}
			else
			{
				query = FIND_SAVED_CARTS_FOR_USER_AND_SITE;
			}
		}
		else
		{
			if (StringUtils.isNotBlank(orderStatusList))
			{
				params.put("orderStatus", orderStatusList);
				query = FIND_SAVED_CARTS_FOR_USER_WITH_STATUS;
			}
			else
			{
				query = FIND_SAVED_CARTS_FOR_USER;
			}
		}

		final List<SortQueryData> sortQueries = Arrays.asList(
				createSortQueryData(SORT_CODE_BY_DATE_SAVED, query + SORT_SAVED_CARTS_BY_DATE_SAVED),
				createSortQueryData(SORT_CODE_BY_DATE_MODIFIED, query + ORDERBYCLAUSE),
				createSortQueryData(SORT_CODE_BY_NAME, query + SORT_SAVED_CARTS_BY_NAME),
				createSortQueryData(SORT_CODE_BY_CODE, query + SORT_SAVED_CARTS_BY_CODE),
				createSortQueryData(SORT_CODE_BY_TOTAL, query + SORT_SAVED_CARTS_BY_TOTAL));

		return getPagedFlexibleSearchService().search(sortQueries, SORT_CODE_BY_DATE_MODIFIED, params, pageableData);
	}

	protected SortQueryData createSortQueryData(final String sortCode, final String query)
	{
		final SortQueryData result = new SortQueryData();
		result.setSortCode(sortCode);
		result.setQuery(query);
		return result;
	}

	/**
	 * Format given list of OrderStatus to comma-separated string of order status ids in brackets
	 *
	 */
	protected String formatOrderStatusList(final List<OrderStatus> orderStatus)
	{
		if (CollectionUtils.isNotEmpty(orderStatus))
		{
			final StringBuilder orderStatusIdList = new StringBuilder();
			for (final OrderStatus status : orderStatus)
			{
				if (status != null && StringUtils.isNotBlank(status.getCode()))
				{
					orderStatusIdList.append(status.getCode()).append(ORDER_STATUS_CODE_SEPERATOR);
				}
			}
			if (orderStatusIdList.length() > 0)
			{
				orderStatusIdList.deleteCharAt(orderStatusIdList.length() - 1);
			}
			return orderStatusIdList.toString();
		}
		return null;
	}

	protected PagedFlexibleSearchService getPagedFlexibleSearchService()
	{
		return pagedFlexibleSearchService;
	}

	@Required
	public void setPagedFlexibleSearchService(final PagedFlexibleSearchService pagedFlexibleSearchService)
	{
		this.pagedFlexibleSearchService = pagedFlexibleSearchService;
	}
}
