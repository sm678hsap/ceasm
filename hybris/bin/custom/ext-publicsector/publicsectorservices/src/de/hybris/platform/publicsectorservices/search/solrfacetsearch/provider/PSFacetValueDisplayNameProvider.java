/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.search.solrfacetsearch.provider;

import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import java.util.Locale;


/**
 * Public sector facet value display name provider interface to get display name for facets
 */
public interface PSFacetValueDisplayNameProvider extends FacetValueDisplayNameProvider
{

	/**
	 * This returns the current locale of the search query
	 *
	 * @param query
	 * @return Locale
	 */
	Locale getQueryLocale(SearchQuery query);

}
