/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorservices.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.platform.publicsectorservices.constants.PublicsectorservicesConstants;

import org.apache.log4j.Logger;


@SuppressWarnings("PMD")
public class PublicsectorservicesManager extends GeneratedPublicsectorservicesManager
{
	@SuppressWarnings("unused")
	private final static Logger log = Logger.getLogger(PublicsectorservicesManager.class.getName());

	public static final PublicsectorservicesManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (PublicsectorservicesManager) em.getExtension(PublicsectorservicesConstants.EXTENSIONNAME);
	}

}
