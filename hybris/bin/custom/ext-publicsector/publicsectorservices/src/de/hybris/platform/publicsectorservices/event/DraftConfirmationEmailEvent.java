/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.event;

import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.order.CartModel;



/**
 * Draft confirmation event, implementation of {@link AbstractCommerceUserEvent}
 */
public class DraftConfirmationEmailEvent extends AbstractCommerceUserEvent

{
	private CartModel cartModel;

	public DraftConfirmationEmailEvent(final CartModel cartModel)
	{
		super();
		this.cartModel = cartModel;

	}

	protected CartModel getCartModel()
	{
		return cartModel;
	}

	public void setCartModel(CartModel cartModel)
	{
		this.cartModel = cartModel;
	}
}
