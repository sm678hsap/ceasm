/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorservices.order;

import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.xyformscommerceservices.strategies.YFormUserStrategy;

import org.springframework.beans.factory.annotation.Required;


/**
 * PSRelationshipPlaceOrderMethodHook implementation of {@link CommercePlaceOrderMethodHook}
 *
 */
public class PSRelationshipPlaceOrderMethodHook implements CommercePlaceOrderMethodHook
{
	private YFormUserStrategy userStrategy;
	private ModelService modelService;
	private UserService userService;

	@Override
	public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel)
			throws InvalidCartException
	{
		final OrderModel orderModelObj = orderModel.getOrder();
		orderModelObj.setPlacedBy(getUserService().getCurrentUser());
		modelService.save(orderModelObj);
		modelService.refresh(orderModelObj);
	}

	@Override
	public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
	{
		final CartModel cartModel = parameter.getCart();
		cartModel.setUser(getUserStrategy().getCurrentUserForCheckout());
		getModelService().save(cartModel);
		getModelService().refresh(cartModel);
	}

	@Override
	public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
			throws InvalidCartException
	{
		//NOOP
	}

	public YFormUserStrategy getUserStrategy()
	{
		return userStrategy;
	}

	@Required
	public void setUserStrategy(final YFormUserStrategy userStrategy)
	{
		this.userStrategy = userStrategy;
	}

	public ModelService getModelService()
	{
		return modelService;
	}


	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}




}
