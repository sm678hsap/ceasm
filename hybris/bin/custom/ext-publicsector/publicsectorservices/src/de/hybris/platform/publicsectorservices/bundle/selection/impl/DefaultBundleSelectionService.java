/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.bundle.selection.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.daos.BundleRuleDao;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.DisableProductBundleRuleModel;
import de.hybris.platform.configurablebundleservices.model.PickExactlyNBundleSelectionCriteriaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.publicsectorservices.bundle.selection.BundleSelectionService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductAddOnModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * The default bundle selection service which implements {@link BundleSelectionService}
 */
public class DefaultBundleSelectionService implements BundleSelectionService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultBundleSelectionService.class);

	private BundleTemplateService bundleTemplateService;
	private ProductService productService;
	private CartService cartService;
	private BundleRuleDao disableProductBundleRuleDao;

	@Override
	public BundleTemplateModel getParentBundleTemplateByProduct(final String productCode)
	{
		final ProductModel product = getProductService().getProductForCode(productCode);
		final List<BundleTemplateModel> bundleTemplates = getBundleTemplateService().getBundleTemplatesByProduct(product);
		BundleTemplateModel parentBundleTemplate = null;

		for (final BundleTemplateModel bundleTemplate : bundleTemplates)
		{
			parentBundleTemplate = bundleTemplate.getParentTemplate();
			break;
		}
		return parentBundleTemplate;
	}

	@Override
	public Boolean hasBundleAddons(final String productCode)
	{
		final ProductModel product = getProductService().getProductForCode(productCode);

		if (product != null && CollectionUtils.isNotEmpty(product.getBundleTemplates()))
		{
			return Boolean.TRUE;
		}
		LOG.debug("Product has no addons " + productCode);
		return Boolean.FALSE;
	}

	@Override
	public List<ProductModel> getDisabledProducts(final String productCode)
	{
		final ProductModel product = getProductService().getProductForCode(productCode);
		final BundleTemplateModel bundleModel = getParentBundleTemplateByProduct(productCode);
		final List<DisableProductBundleRuleModel> disableRules = getDisableProductBundleRuleDao()
				.findBundleRulesByProductAndRootTemplate(product, bundleModel);

		final List<ProductModel> listOfDisabledProducts = new ArrayList<>();
		for (final DisableProductBundleRuleModel disableProductModel : disableRules)
		{
			if (CollectionUtils.isNotEmpty(disableProductModel.getTargetProducts()))
			{
				listOfDisabledProducts.addAll(disableProductModel.getTargetProducts().stream()
						.filter(e -> !e.getCode().equalsIgnoreCase(productCode)).collect(Collectors.toList()));
			}
		}
		return listOfDisabledProducts;
	}

	@Override
	public void cartCleanupBeforeAdd(final String productCode) throws CommerceCartModificationException
	{
		final ProductModel product = getProductService().getProductForCode(productCode);
		final List<BundleTemplateModel> bundleTemplates = getBundleTemplateService().getBundleTemplatesByProduct(product);
		final CartModel cartModel = getCartService().getSessionCart();

		cleanupServiceProducts(productCode, cartModel);
		cleanupBundleAddons(bundleTemplates, cartModel);
	}

	@Override
	public boolean isEligibleToAddBundleAddon(final String productCode)
	{
		final CartModel cart = getCartService().getSessionCart();

		if (CollectionUtils.isNotEmpty(cart.getEntries()))
		{
			return cart.getEntries().stream().filter(e -> e.getProduct() instanceof PSServiceProductAddOnModel)
					.filter(e -> e.getProduct().getCode().equals(productCode)).count() == 0;
		}
		return true;
	}

	/**
	 * Deletes an addon from cart if it is not a product of a required bundle template.
	 *
	 * @param bundleTemplates
	 * @param cartModel
	 * @throws CommerceCartModificationException
	 */
	protected void cleanupBundleAddons(final List<BundleTemplateModel> bundleTemplates, final CartModel cartModel)
			throws CommerceCartModificationException
	{
		deleteAddOnEntryOnUnRequiredBundle(bundleTemplates, cartModel);
		deleteAddonEntryOnSameBundle(bundleTemplates, cartModel);
		deleteCartAddOnEntriesWithoutARequiredBundle(bundleTemplates, cartModel);
		deleteCartAddOnEntriesWithoutABundle(bundleTemplates, cartModel);
	}

	/**
	 * Deletes an addon if its not in the required bundle
	 *
	 * @param bundleTemplates
	 * @param cartModel
	 */
	protected void deleteAddOnEntryOnUnRequiredBundle(final List<BundleTemplateModel> bundleTemplates, final CartModel cartModel)
	{
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			if (entry.getProduct() instanceof PSServiceProductAddOnModel)
			{
				final PSServiceProductAddOnModel serviceProductAddon = (PSServiceProductAddOnModel) entry.getProduct();

				if (!isRequiredBundleHasAddon(bundleTemplates, serviceProductAddon))
				{
					LOG.trace("deleting the entry with the product" + entry.getProduct().getCode());
					deleteCartEntries(cartModel, entry);
				}
			}
		}
	}

	/**
	 * Checks if the cart entry is part of a required bundle.
	 *
	 * @param bundleTemplates
	 * @param serviceProductAddon
	 * @return boolean
	 */
	protected boolean isRequiredBundleHasAddon(final List<BundleTemplateModel> bundleTemplates,
			final PSServiceProductAddOnModel serviceProductAddon)
	{

		for (final BundleTemplateModel bundleTemplate : bundleTemplates)
		{
			for (final BundleTemplateModel requiredBundle : bundleTemplate.getRequiredBundleTemplates())
			{
				if (requiredBundle.getProducts().contains(serviceProductAddon))
				{
					LOG.debug("service product addon is in required bundle " + serviceProductAddon);
					return true;
				}
			}
		}
		LOG.debug("service product is not in required bundle " + serviceProductAddon);
		return false;
	}

	/**
	 * Deletes an addon entry if it is in the same bundle template with the new addon to be added.
	 *
	 * @param bundleTemplates
	 * @param cartModel
	 */
	protected void deleteAddonEntryOnSameBundle(final List<BundleTemplateModel> bundleTemplates, final CartModel cartModel)
	{
		for (final BundleTemplateModel bundleTemplate : bundleTemplates)
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				if (getEntryOnSameBundle(bundleTemplate, entry) != null)
				{
					deleteCartEntries(cartModel, getEntryOnSameBundle(bundleTemplate, entry));
				}
			}
		}
	}

	/**
	 * Gets the entry on the same bundle.
	 *
	 * @param bundleTemplate
	 * @param entry
	 * @return AbstractOrderEntryModel
	 */
	protected AbstractOrderEntryModel getEntryOnSameBundle(final BundleTemplateModel bundleTemplate,
			final AbstractOrderEntryModel entry)
	{
		if (entry.getProduct() instanceof PSServiceProductAddOnModel)
		{
			final PSServiceProductAddOnModel serviceProductAddon = (PSServiceProductAddOnModel) entry.getProduct();
			if (bundleTemplate.getProducts().contains(serviceProductAddon))
			{
				return entry;
			}
		}
		return null;
	}

	/**
	 * Deletes cart addon entries without a bundle template associated to it.
	 *
	 * @param bundleTemplates
	 * @param cartModel
	 */
	protected void deleteCartAddOnEntriesWithoutABundle(final List<BundleTemplateModel> bundleTemplates, final CartModel cartModel)
	{
		//clean bundle addons if there is no bundle products
		if (CollectionUtils.isEmpty(bundleTemplates))
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				if (entry.getProduct() instanceof PSServiceProductAddOnModel)
				{
					deleteCartEntries(cartModel, entry);
				}
			}
		}
	}

	/**
	 * Deletes cart addon entries if there's no required bundles.
	 *
	 * @param bundleTemplates
	 * @param cartModel
	 */
	protected void deleteCartAddOnEntriesWithoutARequiredBundle(final List<BundleTemplateModel> bundleTemplates,
			final CartModel cartModel)
	{
		List<AbstractOrderEntryModel> entries = new ArrayList<>();
		for (final BundleTemplateModel bundleTemplate : bundleTemplates)
		{
			if (CollectionUtils.isEmpty(bundleTemplate.getRequiredBundleTemplates()))
			{
				entries = getAddOnsInCart(cartModel);
			}
		}
		for (final AbstractOrderEntryModel entry : entries)
		{
			deleteCartEntries(cartModel, entry);
		}
	}

	/**
	 * Get addons from cart
	 *
	 * @param cartModel
	 * @return List<AbstractOrderEntryModel>
	 */
	protected List<AbstractOrderEntryModel> getAddOnsInCart(final CartModel cartModel)
	{
		final List<AbstractOrderEntryModel> entries = new ArrayList<>();
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			if (entry.getProduct() instanceof PSServiceProductAddOnModel)
			{
				LOG.trace("found add on in cart {}", entry.getProduct().getCode());
				entries.add(entry);
			}
		}
		return entries;
	}

	/**
	 * Deletes a service product cart entry if the entry to be added is a PSServiceProductModel.
	 *
	 * @param productCode
	 * @param cartModel
	 * @throws CommerceCartModificationException
	 */
	protected void cleanupServiceProducts(final String productCode, final CartModel cartModel)
			throws CommerceCartModificationException
	{
		if (getProductService().getProductForCode(productCode) instanceof PSServiceProductModel)
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				if (entry.getProduct() instanceof PSServiceProductModel)
				{
					LOG.debug("deleting service product in cart {}", entry.getProduct().getCode());
					deleteCartEntries(cartModel, entry);
				}
			}
		}
	}

	/**
	 * As for Citizen Engagement accelerator, a cart can have only one entry. This method deletes one entry from cart at
	 * a time. For implementations with more than one entry in the cart, this method needs to be called once for each
	 * deletion of an entry from cart.
	 *
	 * @param cartModel
	 * @param entry
	 */
	protected void deleteCartEntries(final CartModel cartModel, final AbstractOrderEntryModel entry)
	{
		ServicesUtil.validateParameterNotNull(entry, "Cart entry cannot be null.");
		final Map<Integer, Long> quantityMap = new HashMap<>();
		quantityMap.put(entry.getEntryNumber(), Long.valueOf(0L));
		cartService.updateQuantities(cartModel, quantityMap);
	}

	@Override
	public List<BundleTemplateModel> getChildBundlesNotInCart(final String productCode)
	{
		List<BundleTemplateModel> childBundlesNotInCart = new ArrayList<>();
		if (getCartService().hasSessionCart())
		{
			final CartModel cartModel = getCartService().getSessionCart();
			addChildBundleNotInCart(childBundlesNotInCart, cartModel, getChildBundleTemplates(productCode));
			childBundlesNotInCart = excludeDisabledProductsFromCart(childBundlesNotInCart, cartModel);
		}
		else
		{
			LOG.warn("No current session cart available");
		}
		return childBundlesNotInCart;
	}

	/**
	 * @param childBundlesNotInCart
	 * @param cartModel
	 * @param childBundles
	 */
	private void addChildBundleNotInCart(final List<BundleTemplateModel> childBundlesNotInCart, final CartModel cartModel,
			final List<BundleTemplateModel> childBundles)
	{
		int maxItemsAllowed;
		for (final BundleTemplateModel childBundle : childBundles)
		{
			final BundleSelectionCriteriaModel selectionCriteria = childBundle.getBundleSelectionCriteria();
			if (selectionCriteria instanceof PickExactlyNBundleSelectionCriteriaModel)
			{
				maxItemsAllowed = ((PickExactlyNBundleSelectionCriteriaModel) selectionCriteria).getN().intValue();
				int childProductCount = 0;
				for (final ProductModel childProduct : childBundle.getProducts())
				{
					childProductCount += cartModel.getEntries().stream().filter(s -> s.getProduct().equals(childProduct)).count();
				}
				if (childProductCount > maxItemsAllowed)
				{
					LOG.debug("You have added more than the maximum allowed addons to cart from this bundle template: "
							+ childBundle.getName());
				}

				if (childProductCount == 0)
				{
					childBundlesNotInCart.add(childBundle);
				}
			}
		}
	}

	/**
	 * Retrieves required bundles which are not in cart and excludes bundles with disabled products in cart.
	 *
	 * @param childBundles
	 * @param cartModel
	 * @return List<BundleTemplateModel>
	 */
	protected List<BundleTemplateModel> excludeDisabledProductsFromCart(final List<BundleTemplateModel> childBundles,
			final CartModel cartModel)
	{
		final List<BundleTemplateModel> bundlesWithDisabledProduct = getBundleWithDisabledProductsForCartEntries(
				cartModel.getEntries());

		return childBundles.stream().filter(s -> !bundlesWithDisabledProduct.contains(s)).collect(Collectors.toList());

	}

	protected List<BundleTemplateModel> getBundleWithDisabledProductsForCartEntries(
			final List<AbstractOrderEntryModel> orderEntries)
	{
		final List<BundleTemplateModel> bundlesWithDisabledProductList = new ArrayList<>();
		for (final AbstractOrderEntryModel entry : orderEntries)
		{
			final List<BundleTemplateModel> bundleWithDisabledProduct = getBundleWithDisabledProducts(entry.getProduct().getCode());
			if (CollectionUtils.isNotEmpty(bundleWithDisabledProduct))
			{
				bundlesWithDisabledProductList.addAll(bundleWithDisabledProduct);
			}
		}
		return bundlesWithDisabledProductList;
	}

	@Override
	public List<BundleTemplateModel> getChildBundleTemplates(final String productCode)
	{
		final BundleTemplateModel parentBundle = getParentBundleTemplateByProduct(productCode);
		List<BundleTemplateModel> childBundlesModel = new ArrayList<>();
		if (parentBundle != null)
		{
			LOG.trace("product have child templates" + productCode);
			childBundlesModel = parentBundle.getChildTemplates();
		}
		else
		{
			LOG.debug("product dont have child templates {}", productCode);
		}
		return childBundlesModel;
	}

	@Override
	public List<BundleTemplateModel> getBundleWithDisabledProducts(final String productCode)
	{
		final ProductModel product = getProductService().getProductForCode(productCode);

		final BundleTemplateModel bundleModel = getParentBundleTemplateByProduct(productCode);
		final List<DisableProductBundleRuleModel> disableRules = getDisableProductBundleRuleDao()
				.findBundleRulesByProductAndRootTemplate(product, bundleModel);

		return disableRules.stream().filter(disableProductModel -> disableProductModel.getConditionalProducts().contains(product))
				.map(bundleTemplate -> bundleTemplate.getBundleTemplate()).collect(Collectors.toList());

	}

	protected BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	@Required
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected BundleRuleDao getDisableProductBundleRuleDao()
	{
		return disableProductBundleRuleDao;
	}

	@Required
	public void setDisableProductBundleRuleDao(final BundleRuleDao disableProductBundleRuleDao)
	{
		this.disableProductBundleRuleDao = disableProductBundleRuleDao;
	}
}
