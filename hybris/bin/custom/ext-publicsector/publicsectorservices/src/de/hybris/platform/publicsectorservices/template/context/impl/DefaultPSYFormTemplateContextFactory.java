/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.template.context.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.publicsectorservices.template.context.PSYFormTemplateContextFactory;
import de.hybris.platform.publicsectorservices.template.velocity.context.AbstractYFormContext;
import de.hybris.platform.site.BaseSiteService;

import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;


/**
 * Default YFormData Template Context Factory used to create velocity context for rendering yFormData
 */
public class DefaultPSYFormTemplateContextFactory implements PSYFormTemplateContextFactory
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSYFormTemplateContextFactory.class);
	BaseSiteService baseSiteService;

	/**
	 * Returns the velocity context for yForm which provide context data that can then be used for rendering the html
	 *
	 * @param yformData
	 * @param renderTemplate
	 * @return VelocityContext
	 */
	@Override
	public VelocityContext create(final Map<String, String> yformData, final RendererTemplateModel renderTemplate)
	{
		final BaseSiteModel baseSite = baseSiteService.getCurrentBaseSite();

		final AbstractYFormContext context = resolveContext(renderTemplate);
		context.init(baseSite, yformData);
		return context;
	}

	/**
	 * resolve the context class name and return the class for given renderer template model
	 *
	 * @param renderTemplate
	 * @return VelocityContext
	 */
	protected <T extends VelocityContext> T resolveContext(final RendererTemplateModel renderTemplate)
	{
		try
		{
			final Class<T> contextClass = (Class<T>) Class.forName(renderTemplate.getContextClass());
			final T context = getApplicationContext().getBean(contextClass);
			if (context != null)
			{
				return context;
			}
			else
			{
				LOG.error("Failed to find bean for the context class provided {}", renderTemplate.getContextClass());
				throw new IllegalStateException("Cannot find bean in application context for class [" + contextClass + "]");
			}
		}
		catch (final ClassNotFoundException e)
		{
			LOG.error("Failed to create yForm  context", e);
			throw new IllegalStateException("Cannot find yForm context class", e);
		}
	}

	protected ApplicationContext getApplicationContext()
	{
		return Registry.getApplicationContext();
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}
}
