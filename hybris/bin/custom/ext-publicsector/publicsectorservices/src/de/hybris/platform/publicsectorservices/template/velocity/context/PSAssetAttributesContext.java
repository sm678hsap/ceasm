/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorservices.template.velocity.context;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeSortedMetadata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.tools.generic.EscapeTool;


/**
 * Context class for asset attributes
 */
public class PSAssetAttributesContext extends VelocityContext
{
	private static final String DEFAULT_ASSET_TYPE_TEXT = "Asset";
	private static final String ASSET_TYPE = "assetType";
	private static final String ASSET_ATTRIBUTES = "assetAttributes";
	private static final String ATTRIBUTE_DISPLAY_ORDER = "attributeDisplayOrder";
	private static final String ESCAPE_TOOL = "esc";

	/**
	 * Initialize Context
	 *
	 * @param baseSite
	 * @param assetAttributeData
	 * @param assetAttributeMappingData
	 */
	public void init(final BaseSiteModel baseSite, final List<PSAssetAttributeData> assetAttributeData,
			final List<PSAssetAttributeMappingData> assetAttributeMappingData)
	{
		put(ASSET_TYPE, fetchAssetType(assetAttributeMappingData));
		put(ASSET_ATTRIBUTES, fetchAssetAttributes(assetAttributeData, assetAttributeMappingData));
		put(ESCAPE_TOOL, new EscapeTool());

	}

	private String fetchAssetType(final List<PSAssetAttributeMappingData> assetAttributeMappingData)
	{
		return CollectionUtils.isNotEmpty(assetAttributeMappingData) ? assetAttributeMappingData.get(0).getAssetType()
				: DEFAULT_ASSET_TYPE_TEXT;
	}

	private List<PSAssetAttributeSortedMetadata> fetchAssetAttributes(final List<PSAssetAttributeData> assetAttributeData,
			final List<PSAssetAttributeMappingData> assetAttributeMappingData)
	{
		final List<PSAssetAttributeSortedMetadata> assetAttributesMetadataList = new ArrayList();
		for (final PSAssetAttributeData attribute : assetAttributeData)
		{

			final PSAssetAttributeSortedMetadata assetMetadata = new PSAssetAttributeSortedMetadata();
			setAssetMetadata(assetMetadata, attribute.getName(), assetAttributeMappingData);
			assetMetadata.setAttributeValue(attribute.getValue());

			assetAttributesMetadataList.add(assetMetadata);
		}

		Collections.sort(assetAttributesMetadataList, new BeanComparator(ATTRIBUTE_DISPLAY_ORDER));

		return assetAttributesMetadataList;
	}

	private void setAssetMetadata(final PSAssetAttributeSortedMetadata assetMetadata, final String attributeName,
			final List<PSAssetAttributeMappingData> assetAttributeMappingData)
	{
		int maxDisplayOrder = 0;
		for (final PSAssetAttributeMappingData attributeMapping : assetAttributeMappingData)
		{
			if (Integer.parseInt(attributeMapping.getAttributeDisplayOrder()) > maxDisplayOrder)
			{
				maxDisplayOrder = Integer.parseInt(attributeMapping.getAttributeDisplayOrder());
			}
			if (attributeMapping.getAttributeName().equals(attributeName))
			{
				assetMetadata.setAttributeDisplayName(attributeMapping.getAttributeDisplayName());
				assetMetadata.setAttributeDisplayOrder(Integer.valueOf(attributeMapping.getAttributeDisplayOrder()));
				return;
			}
		}
		assetMetadata.setAttributeDisplayName(attributeName);
		assetMetadata.setAttributeDisplayOrder(Integer.valueOf(maxDisplayOrder + 1));
		return;
	}

}
