/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicservices.template.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.publicsectorservices.template.context.PSYFormTemplateContextFactory;
import de.hybris.platform.publicservices.template.PSYFormTemplateService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.xyformscommerceservices.helpers.YFormXmlParser;
import de.hybris.platform.xyformsfacades.data.YFormDataData;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * DefaultPublicSectorYFormTemplateService
 *
 * Default implementation of PublicSectorYFormTemplateService
 *
 */
public class DefaultPSYFormTemplateService implements PSYFormTemplateService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSYFormTemplateService.class);
	private static final String TEMPLATE_RENDERER_CONVENTION = "checkout.reviewpage.custom.template.renderer.format";

	private RendererService rendererService;
	private ConfigurationService configurationService;
	private PSYFormTemplateContextFactory psYFormTemplateContextFactory;
	private YFormXmlParser yFormXmlParser;

	/**
	 * Returns the template html as string for the given service
	 *
	 * @param product
	 *           - productData
	 * @param yFormDatas
	 *           - List<YFormDataData>
	 * @returns String
	 */
	@Override
	public String getTemplateHTMLForService(final ProductData product, final List<YFormDataData> yFormDatas)
	{
		final RendererTemplateModel template = getRendererService().getRendererTemplateForCode(getTemplateName(product.getCode()));

		if (template != null)
		{
			final Map<String, String> yFormDataMap = new HashMap<>();
			for (final YFormDataData yFormData : yFormDatas)
			{
				final boolean isYformDefinitionNotEmpty = yFormData.getFormDefinition() != null
						&& StringUtils.isNotEmpty(yFormData.getFormDefinition().getContent());
				final boolean isYformDataNotEmpty = StringUtils.isNotEmpty(yFormData.getContent());
				if (isYformDefinitionNotEmpty && isYformDataNotEmpty)
				{
					yFormDataMap.putAll(getyFormXmlParser().getYFormDataInMap(yFormData.getContent(),
							yFormData.getFormDefinition().getContent(), yFormData.getFormDefinition().getFormId()));
				}
			}
			final VelocityContext context = getPsYFormTemplateContextFactory().create(yFormDataMap, template);
			final StringWriter renderedBody = new StringWriter();
			getRendererService().render(template, context, renderedBody);

			return renderedBody.getBuffer().toString();
		}
		LOG.debug("Template not found for the given product code {}", product.getCode());
		return null;
	}

	/**
	 * Checks if the given service has custom defined template by verifying if there exists a renderer template for
	 * product following the product template convention
	 *
	 * @param productCode
	 * @return boolean
	 */
	@Override
	public boolean isServiceHasTemplate(final String productCode)
	{
		boolean isAvailable = false;
		try
		{
			final String templateName = getTemplateName(productCode);
			if (getRendererService().getRendererTemplateForCode(templateName) != null)
			{
				LOG.info("found template for the service with code {}", productCode);
				isAvailable = true;
			}
		}
		catch (final Exception e) // NOSONAR
		{
			LOG.info("Unable to get renderer template for service : {} ", productCode);
		}
		return isAvailable;
	}

	protected String getTemplateName(final String productCode)
	{
		final String templateFormat = getConfigurationService().getConfiguration().getString(TEMPLATE_RENDERER_CONVENTION);
		return templateFormat.replace("{productCode}", productCode);
	}

	protected RendererService getRendererService()
	{
		return rendererService;
	}

	@Required
	public void setRendererService(final RendererService rendererService)
	{
		this.rendererService = rendererService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected YFormXmlParser getyFormXmlParser()
	{
		return yFormXmlParser;
	}

	@Required
	public void setyFormXmlParser(final YFormXmlParser yFormXmlParser) // NOSONAR
	{
		this.yFormXmlParser = yFormXmlParser;
	}

	protected PSYFormTemplateContextFactory getPsYFormTemplateContextFactory()
	{
		return psYFormTemplateContextFactory;
	}

	@Required
	public void setPsYFormTemplateContextFactory(final PSYFormTemplateContextFactory psYFormTemplateContextFactory)
	{
		this.psYFormTemplateContextFactory = psYFormTemplateContextFactory;
	}
}
