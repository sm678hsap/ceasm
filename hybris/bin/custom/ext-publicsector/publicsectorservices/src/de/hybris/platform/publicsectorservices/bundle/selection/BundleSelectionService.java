/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorservices.bundle.selection;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


/**
 * This is an interface for the Bundle Selection Service methods.
 *
 */
public interface BundleSelectionService
{
	/**
	 * Retrieves the bundle parent template of a chidle bundle.
	 *
	 * @param productCode
	 *           the service product code of the cart.
	 *
	 * @return the parent template id
	 *
	 */
	BundleTemplateModel getParentBundleTemplateByProduct(final String productCode);

	/**
	 * Checks if the service product in the cart has a bundle with addons on it.
	 *
	 * @param productCode
	 *           the service product code of the cart.
	 * @return True or False
	 */
	Boolean hasBundleAddons(final String productCode);

	/**
	 * Get Disable products
	 *
	 * @param productCode
	 *           the service product code
	 * @return List of product model
	 */
	List<ProductModel> getDisabledProducts(final String productCode);

	/**
	 * Cleanup the cart before adding service product
	 *
	 * @param productCode
	 *           the service product code
	 * @throws CommerceCartModificationException
	 *            if the cart cannot be modified
	 */
	void cartCleanupBeforeAdd(final String productCode) throws CommerceCartModificationException;

	/**
	 * Check eligibility to add bundle addon
	 *
	 * @param productCode
	 *           the service product code
	 * @return true if bundle can add
	 */
	boolean isEligibleToAddBundleAddon(final String productCode);

	/**
	 * Gets the child bundle templates of the Service Product.
	 *
	 * @param productCode
	 *           of the Service Product.
	 * @return List<BundleTemplateModel>
	 */
	List<BundleTemplateModel> getChildBundleTemplates(final String productCode);

	/**
	 * Checks if Bundle Selection Criteria in Cart is met and list out all child bundle templates whose services product
	 * addons are not in cart.
	 *
	 * @param productCode
	 *           of the Service Product.
	 * @return List<BundleTemplateModel> child bundle templates which are not added to cart.
	 *
	 */
	List<BundleTemplateModel> getChildBundlesNotInCart(final String productCode);

	/**
	 * Get Bundle with disabled product/s.
	 *
	 * @param productCode
	 *           the service product code
	 * @return List of BundleTemplateModel
	 */
	List<BundleTemplateModel> getBundleWithDisabledProducts(final String productCode);
}
