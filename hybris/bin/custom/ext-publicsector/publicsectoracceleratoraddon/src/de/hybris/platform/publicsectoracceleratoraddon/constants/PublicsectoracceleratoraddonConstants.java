/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectoracceleratoraddon.constants;

/**
 * Global class for all Publicsectoracceleratoraddon constants. You can add global constants for your extension into
 * this class.
 */
@SuppressWarnings("deprecation")
public final class PublicsectoracceleratoraddonConstants extends GeneratedPublicsectoracceleratoraddonConstants
{
	public static final String EXTENSIONNAME = "publicsectoracceleratoraddon";

	public static final String CART_RESTORATION = "cart_restoration";

	public static final String CART_RESTORATION_ERROR_STATUS = "restorationError";

	public static final String CART_RESTORATION_SHOW_MESSAGE = "showRestoration";

	public static final String REDIRECT_WITH_DRAFT = "publicsectoracceleratoraddon.redirect.customer_and_draft";

	public static final String REDIRECT_WITH_CART = "publicsectoracceleratoraddon.redirect.customer_and_cart";

	public static final String REDIRECT_WITH_ORDER = "publicsectoracceleratoraddon.redirect.order";

	public static final String REDIRECT_CUSTOMER_ONLY = "publicsectoracceleratoraddon.redirect.customer_only";

	private PublicsectoracceleratoraddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
