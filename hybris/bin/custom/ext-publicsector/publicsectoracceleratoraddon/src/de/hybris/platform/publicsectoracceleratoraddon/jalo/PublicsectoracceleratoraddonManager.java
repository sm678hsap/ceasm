/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 */
package de.hybris.platform.publicsectoracceleratoraddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.platform.publicsectoracceleratoraddon.constants.PublicsectoracceleratoraddonConstants;

import org.apache.log4j.Logger;


@SuppressWarnings("PMD")
public class PublicsectoracceleratoraddonManager extends GeneratedPublicsectoracceleratoraddonManager
{
	@SuppressWarnings("unused")
	private final static Logger log = Logger.getLogger(PublicsectoracceleratoraddonManager.class.getName());

	public static final PublicsectoracceleratoraddonManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (PublicsectoracceleratoraddonManager) em.getExtension(PublicsectoracceleratoraddonConstants.EXTENSIONNAME);
	}

}
