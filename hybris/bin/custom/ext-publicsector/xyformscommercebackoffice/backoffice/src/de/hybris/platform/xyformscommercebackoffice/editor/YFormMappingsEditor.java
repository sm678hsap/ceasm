/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommercebackoffice.editor;

import de.hybris.platform.xyformscommercefacades.form.XYFormFacade;
import de.hybris.platform.xyformscommerceservices.helpers.YFormXmlParser;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.ListModelMap;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listfoot;
import org.zkoss.zul.Listfooter;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Popup;

import com.hybris.cockpitng.components.Editor;
import com.hybris.cockpitng.core.util.Validate;
import com.hybris.cockpitng.editors.EditorContext;
import com.hybris.cockpitng.editors.EditorListener;
import com.hybris.cockpitng.editors.impl.AbstractPaginableEditorRenderer;
import com.hybris.cockpitng.labels.LabelService;


/**
 * Editor for yfrom mappings.
 *
 * @param <K>
 * @param <V>
 */
public class YFormMappingsEditor<K, V> extends AbstractPaginableEditorRenderer<Map<K, V>>
{

	private static final Logger LOG = LoggerFactory.getLogger(YFormMappingsEditor.class);

	public static final Pattern MAP_EDITORS_PATTERN = Pattern.compile("^(.*),(.*)$");

	protected static final String YE_MAP_EDITOR_BTN_CONTAINER = "ye-map-editor-btn-container";
	protected static final String YE_MAP_INLINE_EDITOR_KEY = "ye-map-inline-editor-key";
	protected static final String YE_MAP_FOOTER = "ye-map-footer";

	protected static final String SAVE_EDITOR_IMG = "/icons/ok.png";
	protected static final String CLEAR_EDITOR_IMG = "/icons/cancel.png";
	protected static final String KEY_VALUE_SEPARATOR_IMG = "/icons/icon_arrow_small_default.png";

	protected static final String MODE_CTX_PARAM = "mode";

	private static final String KEY_VALUE_SEPARATOR_IMG_CLASS = "ye-map-separator";
	private static final String KEY_LABEL_CLASS = "ye-map-key";
	private static final String VALUE_LABEL_CLASS = "ye-map-value";
	private static final String EDITOR_TYPE_SEPARATOR = ", ";
	private static final String REMOVE_BUTTON_CLASS = "ye-remove-button";

	private static final String LABEL_SERVICE = "labelService";

	private static final int MAP_VALUE_IDX = 1;
	private static final int MAP_KEY_IDX = 0;

	private LabelService labelService;

	protected Listbox editorView;

	@Resource
	private XYFormFacade xyFormFacade;

	@Resource
	private YFormXmlParser yFormXmlParser;

	protected enum InlineEditorMode
	{
		CREATE, EDIT
	}


	@Override
	public void render(final Component parent, final EditorContext<Map<K, V>> context, final EditorListener<Map<K, V>> listener)
	{
		Validate.notNull("All parameters are mandatory", parent, context, listener);
		final Div container = new Div();
		container.setSclass(YE_LIST_CONTAINER);

		final Div content = new Div();
		content.setSclass(YE_LIST_CONTENT);
		container.appendChild(content);

		editorView = new Listbox();
		editorView.setSclass(YE_LIST_LISTBOX);
		configurePaging(editorView, context);
		editorView.setItemRenderer(createItemRenderer(context, listener));
		editorView.addEventListener(ON_UPDATE_EVENT, event -> context.removeParameter(ITEM_INDEX_TO_CUT));
		editorView.setEmptyMessage(getL10nDecorator(context, "noValue", "title.no.value"));
		final Map<K, V> initMap = new LinkedHashMap<>();
		if (context.getInitialValue() != null)
		{
			initMap.putAll(context.getInitialValue());
		}
		final ListModelMap<K, V> listModel = new ListModelMap<>();
		listModel.putAll(initMap);
		editorView.setModel(listModel);
		if (context.isEditable())
		{
			final Listfoot listfoot = new Listfoot();
			listfoot.setSclass(YE_MAP_FOOTER);
			listfoot.appendChild(createFooter(listener, context));
			editorView.appendChild(listfoot);
		}
		content.appendChild(editorView);
		container.setParent(parent);
	}

	protected Listfooter createFooter(final EditorListener<Map<K, V>> listener, final EditorContext<Map<K, V>> context)
	{
		final Listfooter footer = new Listfooter();
		footer.appendChild(createFooterContent(context, listener, footer));
		return footer;
	}

	private Div createFooterContent(final EditorContext<Map<K, V>> context, final EditorListener<Map<K, V>> listener,
			final Listfooter footer)
	{
		final Div footerContent = new Div();
		final Image addImage = new Image(context.getResourceUrl("/icons/icon_func_mapeditor_add_default.png"));
		final Label addLabel = new Label(getL10nDecorator(context, "buttonAddLabel", "button.label.add"));
		footerContent.addEventListener(Events.ON_CLICK, event -> {
			footer.getChildren().clear();
			context.setParameter(MODE_CTX_PARAM, InlineEditorMode.CREATE);
			final Div inlineEditor = createInlineEditor(context, listener, null, null);
			footer.appendChild(inlineEditor);
			Events.postEvent(ON_INIT_EVENT, inlineEditor, null);
		});
		footerContent.appendChild(addImage);
		footerContent.appendChild(addLabel);
		return footerContent;
	}

	private void swapValues(final Listbox listbox, final EditorListener<Map<K, V>> listener, final Listitem swappableListitemYang,
			final Listitem swappableListitemYing)
	{
		final Map<K, V> currentContent = getCurrentContent(listbox);
		final V fromValue = currentContent.put(((Map.Entry<K, V>) swappableListitemYang.getValue()).getKey(),
				((Map.Entry<K, V>) swappableListitemYing.getValue()).getValue());
		currentContent.put(((Map.Entry<K, V>) swappableListitemYing.getValue()).getKey(), fromValue);
		updateCurrentValue(listbox, listener, currentContent);
	}


	/**
	 * Defines {@link ListitemRenderer} for the items in the list.
	 *
	 * @param context
	 *           defines editor's environment
	 * @param listener
	 *           reacts on editor's events
	 * @return {@link ListitemRenderer}
	 */
	protected ListitemRenderer<Map.Entry<K, V>> createItemRenderer(final EditorContext<Map<K, V>> context,
			final EditorListener<Map<K, V>> listener)
	{
		return new ListitemRenderer<Map.Entry<K, V>>()
		{

			@Override
			public void render(final Listitem listitem, final Map.Entry<K, V> data, final int idx)
			{
				listitem.setValue(data);
				final Listcell listcell = renderItem(data);
				listitem.appendChild(listcell);
				if (context.isEditable())
				{
					listitem.setDraggable(TRUE);
					listitem.setDroppable(TRUE);
					listitem.addEventListener(Events.ON_DROP, event -> onDrop((DropEvent) event, listitem));
					listcell.addEventListener(Events.ON_DOUBLE_CLICK, event -> showInlineEditor(listitem, data));
				}
			}

			protected Listcell renderItem(final Map.Entry<K, V> item)
			{
				final Listcell container = new Listcell();
				if (item != null)
				{
					final Label keyLabel = new Label();
					keyLabel.setSclass(KEY_LABEL_CLASS);
					keyLabel.setValue(item.getKey() != null ? getLabel(item.getKey()) : StringUtils.EMPTY);
					container.appendChild(keyLabel);

					final Image arrowImage = new Image(context.getResourceUrl(KEY_VALUE_SEPARATOR_IMG));
					arrowImage.setSclass(KEY_VALUE_SEPARATOR_IMG_CLASS);
					container.appendChild(arrowImage);

					final Label valueLabel = new Label();
					valueLabel.setSclass(VALUE_LABEL_CLASS);
					valueLabel.setValue(item.getValue() != null ? getLabel(item.getValue()) : StringUtils.EMPTY);
					container.appendChild(valueLabel);

					final Div removeImage = new Div();
					removeImage.setSclass(REMOVE_BUTTON_CLASS);
					removeImage.addEventListener(Events.ON_CLICK, this::onRemove);
					removeImage.setVisible(context.isEditable());
					container.appendChild(removeImage);
				}
				return container;
			}

			protected String getLabel(final Object key)
			{
				return StringUtils.defaultIfEmpty(getLabelService().getObjectLabel(key), ObjectUtils.toString(key));
			}

			protected void onRemove(final Event event)
			{
				if (selectionNotEmpty(editorView))
				{
					final boolean confirmDelete = BooleanUtils.toBoolean((String) context.getParameter(CONFIRM_DELETE_PARAM));
					if (confirmDelete)
					{
						Messagebox.show(getL10nDecorator(context, "deletePopupMessageLabel", "delete.popup.message"),
								getL10nDecorator(context, "deletePopupTitleLabel", "delete.popup.title"), new Messagebox.Button[]
						{ Messagebox.Button.CANCEL, Messagebox.Button.YES }, Messagebox.QUESTION, messageBoxEvent -> {
							final Messagebox.Button selection = messageBoxEvent.getButton();
							if (Messagebox.Button.YES.equals(selection))
							{
								deleteValueAtIndex(editorView.getSelectedIndex(), editorView, listener);
							}
						});
					}
					else
					{
						deleteValueAtIndex(editorView.getSelectedIndex(), editorView, listener);
					}
				}
			}

			protected void onDrop(final DropEvent event, final Listitem listitem)
			{
				final Component dragged = event.getDragged();
				if (dragged instanceof Listitem)
				{
					final Listitem draggedListItem = (Listitem) dragged;
					swapValues(listitem.getListbox(), listener, draggedListItem, listitem);
				}
			}

			private void showInlineEditor(final Listitem listItem, final Map.Entry<K, V> data)
			{
				listItem.getChildren().clear();
				context.setParameter(MODE_CTX_PARAM, InlineEditorMode.EDIT);
				final Listcell inlineEditorCell = new Listcell();
				inlineEditorCell.setSclass(YE_LIST_LINE_EDITOR);
				inlineEditorCell.appendChild(createInlineEditor(context, listener, listItem, data));
				listItem.appendChild(inlineEditorCell);
				listItem.setContext((Popup) null);
				Events.postEvent(ON_INIT_EVENT, inlineEditorCell, null);
			}

		};
	}

	/**
	 * Creates inline editor for list item.
	 *
	 * @param context
	 *           - editor context
	 * @param listener
	 *           - editor listener
	 * @param listItem
	 *           - list item where the editor should be rendered
	 * @param data
	 *           - initial data for the editor
	 * @return {@link Div} with the inline editor
	 */
	protected Div createInlineEditor(final EditorContext<Map<K, V>> context, final EditorListener<Map<K, V>> listener,
			final Listitem listItem, final Map.Entry<K, V> data)
	{
		final InlineEditorMode mode = context.getParameterAs(MODE_CTX_PARAM);

		final Div inlineEditorContainerKey = new Div();
		inlineEditorContainerKey.setSclass(YE_LIST_INLINE);
		final Div inlineEditorContainerValue = new Div();
		inlineEditorContainerValue.setSclass(YE_LIST_INLINE);
		final Div buttonContainer = new Div();
		buttonContainer.setSclass(YE_MAP_EDITOR_BTN_CONTAINER);

		final Combobox inlineEditorValue = getInlineEditorValue(context, listener);

		final HtmlBasedComponent inlineEditorKey;
		final EventListener<Event> saveInlineValueListener;
		switch (mode)
		{
			case EDIT:
			{
				inlineEditorKey = new Label(data.getKey() != null ? data.getKey().toString() : "");
				saveInlineValueListener = createSaveInlineValueListener(data.getKey(), null, inlineEditorValue, listener, context);
				inlineEditorValue.setValue((String) data.getValue());
				break;
			}
			case CREATE:
			default:
			{
				inlineEditorKey = getInlineEditorKey(context, listener);
				saveInlineValueListener = createSaveInlineValueListener(null, (Combobox) inlineEditorKey, inlineEditorValue, listener,
						context);
				break;
			}
		}

		inlineEditorKey.setSclass(YE_MAP_INLINE_EDITOR_KEY);
		inlineEditorContainerKey.appendChild(inlineEditorKey);
		inlineEditorValue.setSclass(YE_LIST_INLINE_EDITOR);
		inlineEditorValue.addEventListener(Editor.ON_EDITOR_EVENT, saveInlineValueListener);
		inlineEditorContainerValue.appendChild(inlineEditorValue);

		final Image cancelButton = new Image(context.getResourceUrl(CLEAR_EDITOR_IMG));
		cancelButton.addEventListener(Events.ON_CLICK, event -> revert(mode, listener, context));
		buttonContainer.appendChild(cancelButton);
		final Image saveButton = new Image(context.getResourceUrl(SAVE_EDITOR_IMG));
		saveButton.addEventListener(Events.ON_CLICK, saveInlineValueListener);
		buttonContainer.appendChild(saveButton);

		final Div editor = new Div();
		editor.setSclass(YE_LIST_LINE_EDITOR);
		editor.appendChild(inlineEditorContainerKey);
		editor.appendChild(inlineEditorContainerValue);
		editor.appendChild(buttonContainer);
		editor.addEventListener(ON_INIT_EVENT, event -> {
			switch (mode)
			{
				case EDIT:
					inlineEditorValue.focus();
					break;
				case CREATE:
				default:
					inlineEditorKey.focus();
					break;
			}
		});

		return editor;
	}

	private EventListener<Event> createSaveInlineValueListener(final K key, final Combobox inlineEditorKey,
			final Combobox inlineEditorValue, final EditorListener<Map<K, V>> listener, final EditorContext<Map<K, V>> context)
	{
		return event -> {
			final String data = ObjectUtils.toString(event.getData());
			if (StringUtils.isEmpty(data) || EditorListener.ENTER_PRESSED.equals(data))
			{
				final K newKey = key != null ? key : inlineEditorKey != null ? (K) inlineEditorKey.getValue() : null;
				final V value = (V) inlineEditorValue.getValue();
				editValue(newKey, value, editorView, listener);
				revertFooter(context, listener);
			}
			if (EditorListener.ESCAPE_PRESSED.equals(data))
			{
				revert(context.getParameterAs(MODE_CTX_PARAM), listener, context);
			}
		};
	}

	private void revert(final InlineEditorMode mode, final EditorListener<Map<K, V>> listener,
			final EditorContext<Map<K, V>> context)
	{
		switch (mode)
		{
			case EDIT:
				revertValue(editorView, listener);
				break;
			case CREATE:
				revertFooter(context, listener);
				break;
			default:
				break;
		}
	}

	private void revertValue(final Listbox listbox, final EditorListener<Map<K, V>> listener)
	{
		final Map<K, V> currentValue = getCurrentContent(listbox);
		updateCurrentValue(listbox, listener, currentValue);
	}

	private void updateCurrentValue(final Listbox listbox, final EditorListener<Map<K, V>> listener, final Map<K, V> newValue)
	{
		final ListModelMap<K, V> mapModel = new ListModelMap<>();
		mapModel.putAll(newValue);
		listbox.setModel(mapModel);
		listener.onValueChanged(newValue);
		Events.postEvent(ON_UPDATE_EVENT, listbox, null);
	}

	private void revertFooter(final EditorContext<Map<K, V>> context, final EditorListener<Map<K, V>> listener)
	{
		final Listfooter listfooter = (Listfooter) editorView.getListfoot().getFirstChild();
		listfooter.getChildren().clear();
		listfooter.appendChild(createFooterContent(context, listener, listfooter));
	}

	@Override
	protected Popup createAddPopup(final Listbox listbox, final EditorListener<Map<K, V>> listener,
			final EditorContext<Map<K, V>> context)
	{
		// not implemented, uses inline editor instead
		return null;
	}

	protected EditorContext<Map<K, V>> prepareNestedContext(final EditorContext<Map<K, V>> context, final String editorType,
			final String valueType)
	{
		final EditorContext<Map<K, V>> editorContext = new EditorContext<>(context.getInitialValue(), context.getDefinition(),
				context.getParameters(), context.getLabels(), context.getReadableLocales(), context.getWritableLocales());
		editorContext.setParameter(Editor.VALUE_EDITOR, editorType);
		editorContext.setValueType(valueType);
		/* editorContext.setClearValueSupported(context.isClearValueSupported()); // has been removed from EditorContext class in Hybris 6.7 */
		editorContext.setEditable(context.isEditable());
		editorContext.setOrdered(context.isOrdered());
		editorContext.setOptional(context.isOptional());
		return editorContext;
	}

	protected Pair<String, String> extractDefaultEditors(final EditorContext<Map<K, V>> context)
	{
		final String valueEditor = context.getParameterAs(Editor.VALUE_EDITOR);
		if (StringUtils.isNotBlank(valueEditor))
		{
			final String embeddedEditor = extractEmbeddedEditor(valueEditor);
			if (StringUtils.isNotBlank(embeddedEditor))
			{
				final Matcher matcher = MAP_EDITORS_PATTERN.matcher(embeddedEditor);
				if (matcher.matches())
				{
					return new ImmutablePair<>(matcher.group(1).trim(), matcher.group(2).trim());
				}
			}
		}
		return new ImmutablePair<>(null, null);
	}

	private void editValue(final K key, final V value, final Listbox listbox, final EditorListener<Map<K, V>> listener)
	{
		final Map<K, V> currentValue = getCurrentContent(listbox);
		currentValue.put(key, value);
		updateCurrentValue(listbox, listener, currentValue);
	}

	private void deleteValueAtIndex(final int idx, final Listbox listbox, final EditorListener<Map<K, V>> listener)
	{
		final Map<K, V> currentValue = getCurrentContent(listbox);
		currentValue.remove(((Map.Entry<K, V>) listbox.getItemAtIndex(idx).getValue()).getKey());
		updateCurrentValue(listbox, listener, currentValue);
	}

	private ListModel<Map.Entry<K, V>> getListModel(final Listbox listbox)
	{
		return listbox.getListModel();
	}

	private Map<K, V> getCurrentContent(final Listbox listbox)
	{
		final ListModelMap<K, V> listModel = (ListModelMap<K, V>) getListModel(listbox);
		if (listModel == null)
		{
			return new HashMap<>();
		}
		return listModel.getInnerMap();
	}

	/**
	 * @return the labelService
	 */
	protected LabelService getLabelService()
	{
		if (labelService == null)
		{
			this.labelService = SpringUtil.getApplicationContext().getBean(LABEL_SERVICE, LabelService.class);
		}
		return labelService;
	}

	/**
	 *
	 * Creates inline editor for XPaths of YFormDefinition.
	 *
	 * @param context
	 * @param listener
	 * @return Combobox
	 */
	private Combobox getInlineEditorKey(final EditorContext<Map<K, V>> context, final EditorListener<Map<K, V>> listener)
	{
		final Combobox box = new Combobox();
		final YFormDefinitionFieldMappingsModel mappingModel = ((YFormDefinitionFieldMappingsModel) context
				.getParameter("parentObject"));

		final Set<String> xPaths = getyFormXmlParser().getXPathsForYFormDefinition(mappingModel.getApplicationId(),
				mappingModel.getFormId());

		final ListModelList<Object> model = new ListModelList<>(xPaths);
		box.setModel(model);
		box.setReadonly(true);
		box.setAutodrop(true);
		box.setDisabled(!context.isEditable());

		return box;
	}

	/**
	 * Creates inline editor for fields of YFormMappingsData.
	 *
	 * @param context
	 * @param listener
	 * @return Combobox
	 */
	private Combobox getInlineEditorValue(final EditorContext<Map<K, V>> context, final EditorListener<Map<K, V>> listener)
	{
		final Combobox box = new Combobox();
		List<String> allValues = new ArrayList<>();
		try
		{
			allValues = getXyFormFacade().getMappingFields();
		}
		catch (final IllegalAccessException e)
		{
			LOG.error("Error occurred while getting YFormMappingsData fields : " + e);
		}

		final ListModelList<Object> model = new ListModelList<>(allValues);
		box.setModel(model);
		box.setReadonly(true);
		box.setAutodrop(true);
		box.setDisabled(!context.isEditable());

		return box;
	}

	protected XYFormFacade getXyFormFacade()
	{
		return xyFormFacade;
	}

	public void setXyFormFacade(final XYFormFacade xyFormFacade)
	{
		this.xyFormFacade = xyFormFacade;
	}

	protected YFormXmlParser getyFormXmlParser()
	{
		return yFormXmlParser;
	}

	public void setyFormXmlParser(final YFormXmlParser yFormXmlParser) // NOSONAR
	{
		this.yFormXmlParser = yFormXmlParser;
	}

}
