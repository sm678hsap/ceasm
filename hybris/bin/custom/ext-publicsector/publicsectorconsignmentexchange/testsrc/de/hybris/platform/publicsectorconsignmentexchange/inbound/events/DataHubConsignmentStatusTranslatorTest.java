package de.hybris.platform.publicsectorconsignmentexchange.inbound.events;

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.publicsectorconsignmentexchange.constants.DataHubInboundConstants;
import de.hybris.platform.publicsectorconsignmentexchange.inbound.DataHubInboundConsignmentHelper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;


@SuppressWarnings("javadoc")
@UnitTest
public class DataHubConsignmentStatusTranslatorTest
{
	private static final String CONSIGNMENT_CODE = "c101010";
	private static final String CONSIGNMENT_STATUS = "CANCELLED";

	@InjectMocks
	private DataHubConsignmentStatusTranslator classUnderTest;
	@Mock
	private Item processedItem;

	@Mock
	private DataHubInboundConsignmentHelper consignmentInboundService;

	@Before
	public void setUp()
	{
		classUnderTest = new DataHubConsignmentStatusTranslator();
		processedItem = Mockito.mock(Item.class);
		consignmentInboundService = Mockito.mock(DataHubInboundConsignmentHelper.class);
		classUnderTest.setInboundHelper(consignmentInboundService);
	}

	/**
	 * Test on updating consignment status.
	 *
	 * @throws JaloInvalidParameterException
	 * @throws JaloBusinessException
	 */
	@Test
	public void testUpdateConsignmentStatus() throws JaloInvalidParameterException, JaloBusinessException
	{
		Mockito.when(processedItem.getAttribute(DataHubInboundConstants.CONSIGNMENT_CODE)).thenReturn(CONSIGNMENT_CODE);
		final ConsignmentModel consignment = mock(ConsignmentModel.class);
		Mockito.when(consignment.getCode()).thenReturn("CONSIGNMENT_CODE");
		final ConsignmentStatus status = mock(ConsignmentStatus.class);
		Mockito.when(status.getCode()).thenReturn(CONSIGNMENT_STATUS);
		Mockito.when(consignment.getStatus()).thenReturn(status);

		classUnderTest.performImport("CANCELLED", processedItem);
		Mockito.verify(consignmentInboundService).updateConsignmentStatus(CONSIGNMENT_CODE, CONSIGNMENT_STATUS);
	}

	/**
	 * Test for updating consignment status to null.
	 *
	 * @throws ImpExException
	 * @throws JaloInvalidParameterException
	 * @throws JaloSecurityException
	 */
	@Test
	public void testUpdateConsignmentStatusToNull() throws ImpExException, JaloInvalidParameterException, JaloSecurityException
	{
		Mockito.when(processedItem.getAttribute(DataHubInboundConstants.CONSIGNMENT_CODE)).thenReturn(CONSIGNMENT_CODE);
		Mockito.doNothing().when(consignmentInboundService).updateConsignmentStatus(CONSIGNMENT_CODE, null);
		classUnderTest.performImport(null, processedItem);
		org.mockito.Mockito.verifyZeroInteractions(consignmentInboundService);
	}
}
