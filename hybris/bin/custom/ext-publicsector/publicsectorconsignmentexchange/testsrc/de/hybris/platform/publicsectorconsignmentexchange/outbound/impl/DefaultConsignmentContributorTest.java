/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.outbound.impl;

import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.publicsectorconsignmentexchange.constants.PSConsignmentCsvColumns;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;


@SuppressWarnings("javadoc")
@UnitTest
public class DefaultConsignmentContributorTest
{

	private DefaultConsignmentContributor publicSectorConsignmentContributor;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		publicSectorConsignmentContributor = new DefaultConsignmentContributor();
	}

	@Test
	public void testGetColumns()
	{
		final Set<String> columns = publicSectorConsignmentContributor.getColumns();

		assertTrue(columns.contains(PSConsignmentCsvColumns.UID));
		assertTrue(columns.contains(PSConsignmentCsvColumns.NAME));
		assertTrue(columns.contains(PSConsignmentCsvColumns.CREATED));
		assertTrue(columns.contains(PSConsignmentCsvColumns.CURRENCY));
		assertTrue(columns.contains(PSConsignmentCsvColumns.ORDER_ID));
		assertTrue(columns.contains(PSConsignmentCsvColumns.CONSIGNMENT_ID));
		assertTrue(columns.contains(PSConsignmentCsvColumns.TAXCODE));
		assertTrue(columns.contains(PSConsignmentCsvColumns.DELIVERY_METHOD));
		assertTrue(columns.contains(PSConsignmentCsvColumns.CONSIGNMENT_STATUS));

		assertTrue(columns.contains(PSConsignmentCsvColumns.SERVICE_REQUEST));
		assertTrue(columns.contains(PSConsignmentCsvColumns.PRICE));
		assertTrue(columns.contains(PSConsignmentCsvColumns.YFORMDATA));
	}
}
