/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.outbound.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.publicsectorconsignmentexchange.constants.PSConsignmentCsvColumns;
import de.hybris.platform.publicsectorconsignmentexchange.outbound.RawItemContributor;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.xyformsservices.model.YFormDataModel;


/**
 * This a Public Sector Consignment Contributor class which creates consignment rows and implements
 * {@link RawItemContributor}
 */
public class DefaultConsignmentContributor implements RawItemContributor<ConsignmentModel>
{
	private final Set<String> columns = new HashSet<>(Arrays.asList(PSConsignmentCsvColumns.CONSIGNMENT_STATUS,
			PSConsignmentCsvColumns.UID, PSConsignmentCsvColumns.NAME, PSConsignmentCsvColumns.CREATED,
			PSConsignmentCsvColumns.CURRENCY, PSConsignmentCsvColumns.ORDER_ID, PSConsignmentCsvColumns.CONSIGNMENT_ID,
			PSConsignmentCsvColumns.TAXCODE, PSConsignmentCsvColumns.DELIVERY_METHOD, PSConsignmentCsvColumns.ENTRY_NUMBER,
			PSConsignmentCsvColumns.SERVICE_REQUEST, PSConsignmentCsvColumns.PRICE, PSConsignmentCsvColumns.YFORMDATA,
			PSConsignmentCsvColumns.BILLING_LINE1, PSConsignmentCsvColumns.BILLING_LINE2, PSConsignmentCsvColumns.BILLING_TOWN,
			PSConsignmentCsvColumns.BILLING_COUNTRY, PSConsignmentCsvColumns.BILLING_POSTCODE,
			PSConsignmentCsvColumns.ADDRESS_BILLING_TYPE, PSConsignmentCsvColumns.SHIPPING_LINE1,
			PSConsignmentCsvColumns.SHIPPING_LINE2, PSConsignmentCsvColumns.SHIPPING_TOWN, PSConsignmentCsvColumns.SHIPPING_COUNTRY,
			PSConsignmentCsvColumns.SHIPPING_POSTCODE, PSConsignmentCsvColumns.ADDRESS_SHIPPING_TYPE));

	@Override
	public Set<String> getColumns()
	{
		return columns;
	}

	@Override
	public List<Map<String, Object>> createRows(final ConsignmentModel consignment)
	{
		final Map<String, Object> row = new HashMap<>();
		final AbstractOrderModel order = consignment.getOrder();
		final PaymentInfoModel payment = consignment.getOrder().getPaymentInfo();
		final AddressModel shippingAddress = consignment.getShippingAddress();

		final List<Map<String, Object>> result = new ArrayList<>();

		if (order != null)
		{
			final UserModel user = order.getUser();
			if (user != null)
			{
				row.put(PSConsignmentCsvColumns.UID, user.getUid());
				if (user instanceof CustomerModel)
				{
					final CustomerModel customer = (CustomerModel) user;
					row.put(PSConsignmentCsvColumns.NAME, customer.getName());
				}
			}
			row.put(PSConsignmentCsvColumns.CREATED, order.getCreationtime());
			row.put(PSConsignmentCsvColumns.CURRENCY, order.getCurrency().getIsocode());
			row.put(PSConsignmentCsvColumns.ORDER_ID, order.getCode());
			final StringBuilder taxCode = setTaxCodeRow(order);
			row.put(PSConsignmentCsvColumns.TAXCODE, taxCode);
		}
		else
		{  
			row.put(PSConsignmentCsvColumns.TAXCODE, StringUtils.EMPTY);
		}
		row.put(PSConsignmentCsvColumns.CONSIGNMENT_ID, consignment.getCode());
		row.put(PSConsignmentCsvColumns.CONSIGNMENT_STATUS, consignment.getStatus().toString());

		final DeliveryModeModel deliveryMode = consignment.getDeliveryMode();
		row.put(PSConsignmentCsvColumns.DELIVERY_METHOD, deliveryMode != null ? deliveryMode.getCode() : "");

		setConsignmentServiceProductEntryContributor(consignment, row);
		setConsignmentAddressContributor(row, payment, shippingAddress);
		result.add(row);

		return result;
	}

	/**
	 * Sets Consignment address details.
	 *
	 * @param row
	 * @param payment
	 * @param shippingAddress
	 */
	private void setConsignmentAddressContributor(final Map<String, Object> row, final PaymentInfoModel payment,
			final AddressModel shippingAddress)
	{
		if (payment != null)
		{
			final AddressModel billingAddress = payment.getBillingAddress();
			if (billingAddress != null)
			{
				row.put(PSConsignmentCsvColumns.BILLING_LINE1, billingAddress.getLine1());
				row.put(PSConsignmentCsvColumns.BILLING_LINE2, billingAddress.getLine2());
				row.put(PSConsignmentCsvColumns.BILLING_TOWN, billingAddress.getTown());
				row.put(PSConsignmentCsvColumns.BILLING_COUNTRY, billingAddress.getCountry().getIsocode());
				row.put(PSConsignmentCsvColumns.BILLING_POSTCODE, billingAddress.getPostalcode());
				row.put(PSConsignmentCsvColumns.ADDRESS_BILLING_TYPE, "BILL_TO");
			}
		}
		if (shippingAddress != null)
		{
			row.put(PSConsignmentCsvColumns.SHIPPING_LINE1, shippingAddress.getLine1());
			row.put(PSConsignmentCsvColumns.SHIPPING_LINE2, shippingAddress.getLine2());
			row.put(PSConsignmentCsvColumns.SHIPPING_TOWN, shippingAddress.getTown());
			row.put(PSConsignmentCsvColumns.SHIPPING_COUNTRY, shippingAddress.getCountry().getIsocode());
			row.put(PSConsignmentCsvColumns.SHIPPING_POSTCODE, shippingAddress.getPostalcode());
			row.put(PSConsignmentCsvColumns.ADDRESS_SHIPPING_TYPE, "SHIPP_TO");
		}
	}

	/**
	 * Sets service product yforms details.
	 *
	 * @param consignment
	 * @param row
	 */
	private void setConsignmentServiceProductEntryContributor(final ConsignmentModel consignment, final Map<String, Object> row)
	{
		for (final ConsignmentEntryModel entry : consignment.getConsignmentEntries())
		{
			final AbstractOrderEntryModel orderEntry = entry.getOrderEntry();
			if (orderEntry != null)
			{
				final ProductModel serviceProduct = orderEntry.getProduct();
				if (serviceProduct instanceof PSServiceProductModel)
				{
					row.put(PSConsignmentCsvColumns.ENTRY_NUMBER, orderEntry.getEntryNumber().toString());
					row.put(PSConsignmentCsvColumns.SERVICE_REQUEST, serviceProduct.getCode());
					row.put(PSConsignmentCsvColumns.PRICE, orderEntry.getTotalPrice());
					break;
				}
			}
		}
		final StringBuilder yFormData = setYFormDataRow(new ArrayList(consignment.getConsignmentEntries()));
		row.put(PSConsignmentCsvColumns.YFORMDATA, yFormData);
	}

	/**
	 * Builds the tax code string values of the request.
	 *
	 * @param order
	 * @return StringBuilder
	 */
	private StringBuilder setTaxCodeRow(final AbstractOrderModel order)
	{
		final StringBuilder taxCodesStrBuilder = new StringBuilder();

		final Collection<TaxValue> taxValues = order.getTotalTaxValues();
		int index = 1;
		for (final TaxValue taxValue : taxValues)
		{
			taxCodesStrBuilder.append(taxValue.getCode());
			if (taxValues.size() > 1 && index != taxValues.size())
			{
				taxCodesStrBuilder.append("|");
			}
			index++;
		}
		return taxCodesStrBuilder;
	}

	/**
	 * Builds the yFormData Content of the Form.
	 *
	 * @param consignmentEntries
	 * @return StringBuilder
	 */
	private StringBuilder setYFormDataRow(final List<ConsignmentEntryModel> consignmentEntries)
	{
		final StringBuilder yFormDataStrBuilder = new StringBuilder();
		for(final ConsignmentEntryModel consignmentEntryModel: consignmentEntries)
		{
			final AbstractOrderEntryModel orderEntry = consignmentEntryModel.getOrderEntry();
			final List<YFormDataModel> yFormDataList = orderEntry.getYFormData();
			int index = 1;
			for (final YFormDataModel yFormData : yFormDataList)
			{
				yFormDataStrBuilder.append(yFormData.getContent());
				if (orderEntry.getYFormData().size() > 1 && index != yFormDataList.size())
				{
					yFormDataStrBuilder.append("|");
				}
				index++;
			}
		}
		return yFormDataStrBuilder;
	}
}
