/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.inbound.events;


import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.publicsectorconsignmentexchange.constants.DataHubInboundConstants;
import de.hybris.platform.publicsectorconsignmentexchange.inbound.DataHubInboundConsignmentHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class includes the translator for consignment ID update
 */
public class DataHubConsignmentExternalIDTranslator extends DataHubTranslator<DataHubInboundConsignmentHelper>
{
	private static final Logger LOG = LoggerFactory.getLogger(DataHubConsignmentExternalIDTranslator.class);
	
	public static final String HELPER_BEAN = "psDataHubInboundConsignmentHelper";

	public DataHubConsignmentExternalIDTranslator()
	{
		super(HELPER_BEAN);
	}

	@Override
	public void performImport(final String consignmentExternalRef, final Item processedItem) throws ImpExException
	{
		if (consignmentExternalRef != null && processedItem != null)
		{
			final String consignmentCode = getConsignmentCode(processedItem);
			getInboundHelper().updateConsignmentExternalIdFromHub(consignmentCode, consignmentExternalRef);
		}
	}

	private String getConsignmentCode(final Item processedItem) throws ImpExException
	{
		String consignmentCode = null;
		try
		{
			consignmentCode = processedItem.getAttribute(DataHubInboundConstants.CONSIGNMENT_CODE).toString();
		}
		catch (final JaloSecurityException e)
		{
			LOG.error("Error in getting consignment code", e);
			throw new ImpExException(e);
		}
		return consignmentCode;
	}
}
