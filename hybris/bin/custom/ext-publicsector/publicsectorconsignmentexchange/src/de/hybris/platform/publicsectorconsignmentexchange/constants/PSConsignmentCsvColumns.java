/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.constants;

/**
 * The Public Sector Consignment CSV Columns.
 */
public class PSConsignmentCsvColumns
{
	public static final String UID = "uid";

	public static final String NAME = "name";

	public static final String CREATED = "created";

	public static final String CURRENCY = "currency";

	public static final String ORDER_ID = "orderId";

	public static final String CONSIGNMENT_ID = "consignmentId";

	public static final String CONSIGNMENT_STATUS = "consignmentStatus";

	public static final String TAXCODE = "taxCode";

	public static final String DELIVERY_METHOD = "deliveryMethod";

	public static final String PRICE = "price";

	public static final String SERVICE_REQUEST = "serviceRequest";

	public static final String YFORMDATA = "yFormData";

	public static final String ENTRY_NUMBER = "entryNumber";

	public static final String BILLING_LINE1 = "billingLine1";

	public static final String BILLING_LINE2 = "billingLine2";

	public static final String BILLING_TOWN = "billingTown";

	public static final String BILLING_COUNTRY = "billingCountry";

	public static final String BILLING_POSTCODE = "billingPostCode";

	public static final String ADDRESS_BILLING_TYPE = "billingAddressType";

	//Shipping address
	public static final String SHIPPING_LINE1 = "shippingLine1";

	public static final String SHIPPING_LINE2 = "shippingLine2";

	public static final String SHIPPING_TOWN = "shippingTown";

	public static final String SHIPPING_COUNTRY = "shippingCountry";

	public static final String SHIPPING_POSTCODE = "shippingPostCode";

	public static final String ADDRESS_SHIPPING_TYPE = "shippingAddressType";


	private PSConsignmentCsvColumns()
	{
		//empty to avoid instantiating this class
	}
}
