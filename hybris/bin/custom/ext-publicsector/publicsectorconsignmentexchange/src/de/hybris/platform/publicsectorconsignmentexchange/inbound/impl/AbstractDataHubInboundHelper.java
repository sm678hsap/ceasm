/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.inbound.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Abstract root class for inbound processing of order related notifications from Data Hub
 */
public abstract class AbstractDataHubInboundHelper
{
    private static final Logger LOG = LoggerFactory.getLogger(AbstractDataHubInboundHelper.class);
	protected static final String CONSIGNMENT_BY_CODE = "SELECT {" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ "} WHERE  {" + ConsignmentModel.CODE + "} = ?consignmentCode";
	private FlexibleSearchService flexibleSearchService;
	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	/**
	 * Retrieves consignment using consignment code.
	 *
	 * @param consignmentCode
	 * @return
	 */
	protected ConsignmentModel readConsignment(final String consignmentCode)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(CONSIGNMENT_BY_CODE);
		flexibleSearchQuery.addQueryParameter("consignmentCode", consignmentCode);

		final ConsignmentModel consignment = (ConsignmentModel) getFlexibleSearchService().searchUnique(flexibleSearchQuery);
		if (consignment == null)
		{
			final String msg = "Error while Consignment processing. Consignment does not exist for consignment code : "
					+ consignmentCode;
			LOG.error(msg);
			throw new IllegalArgumentException(msg);
		}
		return consignment;
	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
