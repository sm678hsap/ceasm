/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.outbound.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.publicsectorconsignmentexchange.outbound.RawItemBuilder;
import de.hybris.platform.publicsectorconsignmentexchange.outbound.SendToDataHubHelper;
import de.hybris.platform.publicsectorconsignmentexchange.outbound.SendToDataHubResult;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.hybris.datahub.core.rest.DataHubCommunicationException;
import com.hybris.datahub.core.rest.DataHubOutboundException;
import com.hybris.datahub.core.services.DataHubOutboundService;


/**
 * Helper for creating a raw items and sending it to the Data Hub
 *
 * @param <T>
 */
public class AbstractSendToDataHubHelper<T extends AbstractItemModel> implements SendToDataHubHelper<T>
{

	private static final Logger LOG = LoggerFactory.getLogger(AbstractSendToDataHubHelper.class);
	private static final String DEFAULT_FEED = "DEFAULT_FEED";
	
	private DataHubOutboundService dataHubOutboundService;
	private String rawItemType;
	private RawItemBuilder<T> rawItemBuilder;
	
	@Override
	public SendToDataHubResult createAndSendRawItem(final T model)
	{
		try
		{
			getDataHubOutboundService().sendToDataHub(getFeed(model), getRawItemType(),
					getRawItemBuilder().rowsAsNameValuePairs(model));
		}
		//sendToDataHub throws DataHubOutboundException which is parent class to DataHubCommunicationException
		catch (final DataHubOutboundException exception)
		{
			LOG.error("Datahub exception while sending consignment to datahub",exception);
			return new DefaultSendToDataHubResult(SendToDataHubResult.SENDING_FAILED_CODE, exception.getMessage());
		}
		catch (final Exception exception)
		{
			LOG.error("Error while sending consignment to datahub",exception);
			return new DefaultSendToDataHubResult(SendToDataHubResult.SENDING_FAILED_CODE, exception.getMessage());
		}
		return DefaultSendToDataHubResult.OKAY;
	}

	/**
	 * Get datahub feed based on service request
	 *
	 * @param model
	 * @return
	 */
	public String getFeed(final T model)
	{
		if (model instanceof ConsignmentModel)
		{
			for (final AbstractOrderEntryModel entry : ((ConsignmentModel) model).getOrder().getEntries())
			{
				if (entry.getProduct() instanceof PSServiceProductModel)
				{
					final PSServiceProductModel serviceProduct = (PSServiceProductModel) entry.getProduct();
					if (serviceProduct.getDataClassification() != null)
					{
						return ((PSServiceProductModel) entry.getProduct()).getDataClassification().getFeed();
					}
				}
			}
		}
		return DEFAULT_FEED;
	}
	
	protected RawItemBuilder<T> getRawItemBuilder()
	{
		return rawItemBuilder;
	}

	@Required
	public void setRawItemBuilder(final RawItemBuilder<T> csvBuilder)
	{
		this.rawItemBuilder = csvBuilder;
	}

	protected DataHubOutboundService getDataHubOutboundService()
	{
		return dataHubOutboundService;
	}

	@Required
	public void setDataHubOutboundService(final DataHubOutboundService dataHubOutboundService)
	{
		this.dataHubOutboundService = dataHubOutboundService;
	}
	
	public String getRawItemType()
	{
		return rawItemType;
	}

	@Required
	public void setRawItemType(final String rawItem)
	{
		this.rawItemType = rawItem;
	}
}
