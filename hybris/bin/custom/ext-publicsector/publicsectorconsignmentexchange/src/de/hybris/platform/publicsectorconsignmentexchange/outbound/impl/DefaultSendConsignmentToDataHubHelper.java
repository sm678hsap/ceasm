/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.outbound.impl;


import de.hybris.platform.ordersplitting.model.ConsignmentModel;


/**
 * Class definition as a workaround for Spring since it cannot handle parameterized types
 */
public class DefaultSendConsignmentToDataHubHelper extends AbstractSendToDataHubHelper<ConsignmentModel>
{

}
