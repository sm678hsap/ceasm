/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.inbound;

/**
 * This is a Helper class for Inbound Order.
 */
public interface DataHubInboundConsignmentHelper
{
	/**
	 * Update consignment status
	 *
	 * @param consignmentRef
	 *           String
	 */
	public void updateConsignmentStatus(final String consignmentRef, final String consignmentExternalStatus);

	/**
	 * Update consignment External ID from datahub
	 * 
	 * @param consignmentRef
	 * @param consignmentExternalRef
	 */
	public void updateConsignmentExternalIdFromHub(final String consignmentRef, final String consignmentExternalRef);
}
