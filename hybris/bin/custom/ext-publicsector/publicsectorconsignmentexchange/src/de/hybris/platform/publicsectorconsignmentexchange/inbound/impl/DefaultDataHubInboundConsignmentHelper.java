/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.inbound.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.publicsectorconsignmentexchange.constants.DataHubInboundConstants;
import de.hybris.platform.publicsectorconsignmentexchange.inbound.DataHubInboundConsignmentHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Default DataHub Inbound Helper for Consignment and implements {@link DataHubInboundConsignmentHelper}
 */
public class DefaultDataHubInboundConsignmentHelper extends AbstractDataHubInboundHelper
		implements DataHubInboundConsignmentHelper
{
        private static final Logger LOG = LoggerFactory.getLogger(DefaultDataHubInboundConsignmentHelper.class);
	private EnumerationService enumerationService;

	@Override
	public void updateConsignmentStatus(final String consignmentRef, final String consignmentExternalStatus)
	{
		final ConsignmentModel consignment = readConsignment(consignmentRef);
		final ConsignmentStatus status = getEnumerationService().getEnumerationValue(ConsignmentStatus.class,
				consignmentExternalStatus);

		if (getEnumerationService().getEnumerationValues(ConsignmentStatus.class).contains(status))
		{
			consignment.setStatus(status);
			getModelService().save(consignment);
		}

		final String eventName = DataHubInboundConstants.CONSIGNMENT_COMPLETION_EVENTNAME_PREFIX + consignmentRef;
		LOG.info("business process for {} consignment has started ",consignmentRef);
		getBusinessProcessService().triggerEvent(eventName);
	}

	@Override
	public void updateConsignmentExternalIdFromHub(final String consignmentRef, final String consignmentExternalRef)
	{
		final ConsignmentModel consignment = readConsignment(consignmentRef);
		consignment.setPsConsignmentExternalRef(consignmentExternalRef);
		getModelService().save(consignment);

	}

	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

}
