/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorconsignmentexchange.constants;

/**
 * Constants for Data Hub Inbound
 */
public class DataHubInboundConstants
{
	public static final String CONSIGNMENT_EXTERNAL_REFERENCE = "psConsignmentExternalRef";

	public static final String CONSIGNMENT_STATUS = "status";

	public static final String CONSIGNMENT_CODE = "code";

	public static final String CONSIGNMENT_EXTERNAL_STATUS = "psConsignmentExternalStatus";

	public static final String CONSIGNMENT_COMPLETION_EVENTNAME_PREFIX = "ERPConsignmentConfirmationEvent_";

	private DataHubInboundConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
