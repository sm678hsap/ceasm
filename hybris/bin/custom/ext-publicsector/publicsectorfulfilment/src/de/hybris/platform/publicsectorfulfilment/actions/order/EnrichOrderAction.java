/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.publicsectorfulfilment.constants.PublicsectorfulfilmentConstants;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;


/**
 * This action is used to enrich the existing order for services where few data are not mandatory but required by hybris
 * order process
 */
public class EnrichOrderAction extends AbstractProceduralAction<OrderProcessModel>
{
	private DeliveryModeService deliveryModeService;
	private CalculationService calculationService;
	private CommonI18NService commonI18NService;

	@Override
	public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception
	{
		final OrderModel order = process.getOrder();
		boolean isOrderUpdated = false;

		if (order.getDeliveryMode() == null)
		{
			isOrderUpdated = true;
			setNotApplicableDeliveryModeInOrder(order);
		}

		if (order.getDeliveryAddress() == null)
		{
			isOrderUpdated = true;
			final AddressModel sampleAddress = createNotApplicableAddress(order);
			order.setDeliveryAddress(sampleAddress);
		}

		if (order.getPaymentInfo() == null)
		{
			isOrderUpdated = true;
			final InvoicePaymentInfoModel paymentInfo = createNotApplicableInvoicePaymentInfo(order);
			order.setPaymentInfo(paymentInfo);
		}

		if (isOrderUpdated)
		{
			save(order);
			getCalculationService().calculate(order);
		}
	}

	/**
	 * Method to create not-applicable invoice payment info for order
	 *
	 * @param order
	 * @return InvoicePaymentInfoModel
	 */
	private InvoicePaymentInfoModel createNotApplicableInvoicePaymentInfo(final OrderModel order)
	{
		final InvoicePaymentInfoModel paymentInfo = getModelService().create(InvoicePaymentInfoModel.class);
		paymentInfo.setOwner(order);
		paymentInfo.setUser(order.getUser());
		paymentInfo.setCode(PublicsectorfulfilmentConstants.NOT_APPLICABLE);
		save(paymentInfo);
		return paymentInfo;
	}

	/**
	 * Method to create not-applicable address for order
	 *
	 * @param order
	 * @return AddressModel
	 */
	private AddressModel createNotApplicableAddress(final OrderModel order)
	{
		final AddressModel sampleAddress = getModelService().create(AddressModel.class);
		sampleAddress.setOwner(order);
		sampleAddress.setVisibleInAddressBook(false);
		sampleAddress.setShippingAddress(true);
		sampleAddress.setStreetnumber(PublicsectorfulfilmentConstants.NOT_APPLICABLE);
		sampleAddress.setEmail(PublicsectorfulfilmentConstants.NOT_APPLICABLE);
		sampleAddress.setLastname(PublicsectorfulfilmentConstants.NOT_APPLICABLE);
		sampleAddress.setCountry(getCommonI18NService().getCountry(PublicsectorfulfilmentConstants.DEFAULT_COUNTRY_CODE));
		save(sampleAddress);
		return sampleAddress;
	}

	/**
	 * Method to set not-applicable delivery mode in order
	 *
	 * @param order
	 */
	private void setNotApplicableDeliveryModeInOrder(final OrderModel order)
	{
		final DeliveryModeModel deliveryMode = getDeliveryModeService()
				.getDeliveryModeForCode(PublicsectorfulfilmentConstants.NOT_APPLICABLE);
		order.setDeliveryMode(deliveryMode);
	}

	/**
	 * @return the deliveryModeService
	 */
	protected DeliveryModeService getDeliveryModeService()
	{
		return deliveryModeService;
	}

	/**
	 * @param deliveryModeService
	 *           the deliveryModeService to set
	 */
	@Required
	public void setDeliveryModeService(final DeliveryModeService deliveryModeService)
	{
		this.deliveryModeService = deliveryModeService;
	}

	/**
	 * @return the calculationService
	 */
	protected CalculationService getCalculationService()
	{
		return calculationService;
	}

	/**
	 * @param calculationService
	 *           the calculationService to set
	 */
	@Required
	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}

	/**
	 * @return the commonI18NService
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

}
