/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.order;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


public class SendOrderPlacedNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SendOrderCompletedNotificationAction.class);
	private EventService eventService;

	@Override
	public void executeAction(final OrderProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass());
		final OrderModel order = process.getOrder();

		if (isUserUnidentified(order))
		{
			LOG.info("Skipped sending order placed notification as user is unidentified");
		}
		else
		{
			getEventService().publishEvent(new OrderPlacedEvent(process));
		}
	}

	/**
	 * Method to check if order contains user who is unidentified
	 *
	 * @param order
	 * @return true if user is unidentified
	 */
	private boolean isUserUnidentified(final OrderModel order)
	{
		if (order != null && order.getUser() != null && order.getUser() instanceof CustomerModel)
		{
			final CustomerModel user = (CustomerModel) order.getUser();
			return CustomerType.UNIDENTIFIED.equals(user.getType());
		}
		return true;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}
}
