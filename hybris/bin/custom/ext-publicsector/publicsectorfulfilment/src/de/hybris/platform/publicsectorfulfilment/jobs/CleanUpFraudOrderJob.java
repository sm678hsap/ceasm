/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.publicsectorfulfilment.constants.PublicsectorfulfilmentConstants;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * CronJob periodically send CleanUpEvent for <b>order-process</b> processes which are in action <b>waitForCleanUp</b>
 */
public class CleanUpFraudOrderJob extends AbstractJobPerformable<CronJobModel>
{
	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(CleanUpFraudOrderJob.class);
	private BusinessProcessService businessProcessService;

	protected static final String PROCESS_BY_DEFINITION_AND_ACTION = "select {bp." + BusinessProcessModel.PK + "} " + "from {"
			+ BusinessProcessModel._TYPECODE + " AS bp  JOIN " + ProcessTaskModel._TYPECODE + " AS pt ON {bp."
			+ BusinessProcessModel.PK + "} = {pt." + ProcessTaskModel.PROCESS + "} } " + "WHERE {bp."
			+ BusinessProcessModel.PROCESSDEFINITIONNAME + "} = ?processDefinitionName and {pt." + ProcessTaskModel.ACTION
			+ "} = ?processCurrentAction";

	@Override
	public boolean isAbortable()
	{
		return true;
	}

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		final String processDefinitionName = PublicsectorfulfilmentConstants.ORDER_PROCESS_NAME;
		final String processCurrentAction = "waitForCleanUp";
		final List<BusinessProcessModel> processes = getAllProcessByDefinitionAndCurrentAction(processDefinitionName,
				processCurrentAction);

		final String eventNameSuffix = "_CleanUpEvent";
		for (final BusinessProcessModel bpm : processes)
		{
			// ${process.code}_CleanUpEvent
			final String eventName = bpm.getCode() + eventNameSuffix;
			businessProcessService.triggerEvent(eventName);
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	protected List<BusinessProcessModel> getAllProcessByDefinitionAndCurrentAction(final String processDefinitionName,
			final String processCurrentAction)
	{
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(PROCESS_BY_DEFINITION_AND_ACTION);
		searchQuery.addQueryParameter("processDefinitionName", processDefinitionName);
		searchQuery.addQueryParameter("processCurrentAction", processCurrentAction);
		final SearchResult<BusinessProcessModel> processes = flexibleSearchService.search(searchQuery);
		return processes.getResult();
	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}
}
