/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.order;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class GetPaymentDetailsAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(GetPaymentDetailsAction.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.processengine.spring.Action#execute(de.hybris.platform
	 * .processengine.model.BusinessProcessModel )
	 */
	@Override
	public void executeAction(final OrderProcessModel process)
	{
		LOG.info("Process:  {} in step {} ", process.getCode(), getClass());
	}
}
