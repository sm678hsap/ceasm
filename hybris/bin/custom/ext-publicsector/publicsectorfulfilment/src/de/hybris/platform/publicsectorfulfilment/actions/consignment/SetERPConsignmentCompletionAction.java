/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class checks for consignment status after receiving status information from ERP System.
 */
public class SetERPConsignmentCompletionAction extends AbstractSimpleDecisionAction<ConsignmentProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SetERPConsignmentCompletionAction.class);

	@Override
	public Transition executeAction(final ConsignmentProcessModel process)
	{
		final ConsignmentModel consignment = process.getConsignment();
		if (consignment != null && consignment.getStatus() != null)
		{
			return Transition.OK;
		}
		LOG.warn("Consignment or consignment status is null. ");
		return Transition.NOK;
	}
}
