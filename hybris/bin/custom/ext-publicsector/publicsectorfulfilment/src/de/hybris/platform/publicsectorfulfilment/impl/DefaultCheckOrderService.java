/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.publicsectorfulfilment.CheckOrderService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This is the implementation class of {@link CheckOrderService}
 */
public class DefaultCheckOrderService implements CheckOrderService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCheckOrderService.class);

	@Override
	public boolean check(final OrderModel order)
	{
		if (!order.getCalculated().booleanValue())
		{
			// Order must be calculated
			LOG.debug("Order is not calculated for order {}", order.getCode());
			return false;
		}
		if (order.getEntries().isEmpty())
		{
			// Order must have some lines
			LOG.debug("Order has no entries {}", order.getCode());
			return false;
		}
		else if (order.getPaymentInfo() == null)
		{
			// Order must have some payment info to use in the process
			LOG.debug("Order has no payment info {}", order.getCode());
			return false;
		}
		else
		{
			// Order delivery options must be valid
			return checkDeliveryOptions(order);
		}
	}

	protected boolean checkDeliveryOptions(final OrderModel order)
	{

		if (order.getDeliveryAddress() == null)
		{
			for (final AbstractOrderEntryModel entry : order.getEntries())
			{
				if (entry.getDeliveryPointOfService() == null && entry.getDeliveryAddress() == null)
				{
					// Order and Entry have no delivery address and some entries
					// are not for pickup
					LOG.debug("Order has no delivery {}", order.getCode());
					return false;
				}
			}
		}

		return true;
	}
}
