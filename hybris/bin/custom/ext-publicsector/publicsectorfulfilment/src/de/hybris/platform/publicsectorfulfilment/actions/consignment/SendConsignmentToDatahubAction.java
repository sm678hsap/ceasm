/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.publicsectorconsignmentexchange.outbound.SendToDataHubHelper;
import de.hybris.platform.publicsectorconsignmentexchange.outbound.SendToDataHubResult;
import de.hybris.platform.task.RetryLaterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * This is the business process action to send consignment details to datahub
 */
public class SendConsignmentToDatahubAction extends AbstractSimpleDecisionAction<ConsignmentProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(SendConsignmentToDatahubAction.class);

	public static final String ERROR_END_MESSAGE = "Sending to ERP went wrong.";

	static final int DEFAULT_MAX_RETRIES = 10;
	static final int DEFAULT_RETRY_DELAY = 60 * 1000; // value in ms

	private int maxRetries = DEFAULT_MAX_RETRIES;
	private int retryDelay = DEFAULT_RETRY_DELAY;

	private SendToDataHubHelper<ConsignmentModel> sendConsignmentToDataHubHelper;

	@Override
	public Transition executeAction(final ConsignmentProcessModel consignmentProcessModel) throws RetryLaterException
	{
		final ConsignmentModel consignment = consignmentProcessModel.getConsignment();
		final SendToDataHubResult result = sendConsignmentToDataHubHelper.createAndSendRawItem(consignment);
		if (result.isSuccess())
		{
			setConsignmentDatahubExportStatus(consignment, ExportStatus.EXPORTED);
			setConsignmentStatus(consignment, ConsignmentStatus.READY);
			resetEndMessage(consignmentProcessModel);
			return Transition.OK;
		}
		else
		{
			LOG.debug("Unable to send the consignment. {}", consignment);
			setConsignmentDatahubExportStatus(consignment, ExportStatus.NOTEXPORTED);
			handleRetry(consignmentProcessModel);
			return Transition.NOK;
		}

	}

	/**
	 * Sets consignment's status.
	 *
	 * @param consignment
	 * @param status
	 */
	protected void setConsignmentStatus(final ConsignmentModel consignment, final ConsignmentStatus status)
	{
		consignment.setStatus(status);
		save(consignment);
	}

	/**
	 * Sets consignment's datahub export status.
	 *
	 * @param consignment
	 * @param status
	 */
	protected void setConsignmentDatahubExportStatus(final ConsignmentModel consignment, final ExportStatus exportStatus)
	{
		consignment.setDatahubExportStatus(exportStatus);
		save(consignment);
	}

	protected void resetEndMessage(final ConsignmentProcessModel process)
	{
		final String endMessage = process.getEndMessage();
		if (ERROR_END_MESSAGE.equals(endMessage))
		{
			process.setEndMessage("");
			modelService.save(process);
		}
	}

	protected void handleRetry(final ConsignmentProcessModel process) throws RetryLaterException
	{
		if (process.getSendConsignmentRetryCount() < getMaxRetries())
		{
			final ConsignmentModel consignment = process.getConsignment();
			process.setSendConsignmentRetryCount(process.getSendConsignmentRetryCount() + 1);
			modelService.save(process);
			final RetryLaterException ex = new RetryLaterException(
					"Sending to backend failed for consignment " + consignment.getCode());
			ex.setDelay(getRetryDelay());
			ex.setRollBack(false);
			throw ex;
		}
	}

	/**
	 * Get Max Retries
	 *
	 * @return
	 */
	public int getMaxRetries()
	{
		return maxRetries;
	}

	/**
	 * Set the Max Retries
	 *
	 * @param maxRetries
	 */
	public void setMaxRetries(final int maxRetries)
	{
		this.maxRetries = maxRetries;
	}

	/**
	 * Get the Retry Delay
	 *
	 * @return
	 */
	public int getRetryDelay()
	{
		return retryDelay;
	}

	/**
	 * Set the Retry Delay
	 *
	 * @param retryDelay
	 */
	public void setRetryDelay(final int retryDelay)
	{
		this.retryDelay = retryDelay;
	}

	/**
	 * Return the sendConsignmentToDataHubHelper
	 *
	 * @return
	 */
	public SendToDataHubHelper<ConsignmentModel> getSendConsignmentToDataHubHelper()
	{
		return sendConsignmentToDataHubHelper;
	}

	/**
	 * Set the sendConsignmentToDataHubHelper
	 *
	 * @param sendConsignmentToDataHubHelper
	 */
	@Required
	public void setSendConsignmentToDataHubHelper(final SendToDataHubHelper<ConsignmentModel> sendConsignmentToDataHubHelper)
	{
		this.sendConsignmentToDataHubHelper = sendConsignmentToDataHubHelper;
	}
}
