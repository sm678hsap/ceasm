/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * CancelConsignmentActionTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CancelConsignmentActionTest {

	@InjectMocks
	private CancelConsignmentAction cancelConsignmentAction;

	@Mock
	private ConsignmentProcessModel consignmentProcessModel;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private ModelService modelService;

	@Test
	public void testExecuteAction() {
		given(consignmentProcessModel.getConsignment()).willReturn(consignmentModel);
		cancelConsignmentAction.setModelService(modelService);
		cancelConsignmentAction.executeAction(consignmentProcessModel);
	}

}
