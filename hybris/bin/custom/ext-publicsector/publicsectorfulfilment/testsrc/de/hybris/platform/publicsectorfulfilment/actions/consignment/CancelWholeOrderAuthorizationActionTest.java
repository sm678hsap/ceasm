/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.publicsectorfulfilment.actions.order.CancelWholeOrderAuthorizationAction;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * CancelWholeOrderAuthorizationAction unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CancelWholeOrderAuthorizationActionTest {

	private static final String PAYMENT_TRANSACTION = "paymentTransaction";
	@InjectMocks
	private CancelWholeOrderAuthorizationAction cancelWholeOrderAuthorizationAction;

	@Mock
	private OrderProcessModel orderProcessModel;

	@Mock
	private PaymentTransactionModel paymentTransactionModel;

	@Mock
	private PaymentTransactionEntryModel paymentTransactionEntryModel;

	@Mock
	private OrderModel orderModel;

	@Mock
	private PaymentService paymentService;

	@Before
	public void setUp() {
		cancelWholeOrderAuthorizationAction = new CancelWholeOrderAuthorizationAction();
		cancelWholeOrderAuthorizationAction.setPaymentService(paymentService);
	}

	@Test
	public void testExecuteAction() {
		given(orderProcessModel.getOrder()).willReturn(orderModel);
		final List<PaymentTransactionModel> list = new ArrayList<>();
		list.add(paymentTransactionModel);
		given(orderModel.getPaymentTransactions()).willReturn(list);
		final List<PaymentTransactionEntryModel> result = new ArrayList<>();
		result.add(paymentTransactionEntryModel);
		given(paymentTransactionModel.getEntries()).willReturn(result);
		given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(PAYMENT_TRANSACTION);

	}

}
