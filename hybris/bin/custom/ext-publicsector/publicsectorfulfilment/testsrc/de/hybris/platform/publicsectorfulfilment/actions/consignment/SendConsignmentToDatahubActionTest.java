/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.processengine.helpers.ProcessParameterHelper;
import de.hybris.platform.publicsectorconsignmentexchange.outbound.SendToDataHubHelper;
import de.hybris.platform.publicsectorconsignmentexchange.outbound.SendToDataHubResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit Test for {@link SendConsignmentToDatahubAction}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendConsignmentToDatahubActionTest
{

	private static final int DEFAULT_MAX_RETRIES = 10;
	private static final int DEFAULT_RETRY_DELAY = 60 * 1000; // value in ms
	private static final String CONSIGNMENT_CODE = "00000001test";


	@InjectMocks
	private SendConsignmentToDatahubAction action;

	@Mock
	private SendToDataHubHelper<ConsignmentModel> sendConsignmentToDataHubHelper;

	@Mock
	private ModelService modelService;

	@Mock
	private ProcessParameterHelper processParameterHelper;

	@Mock
	private ConsignmentModel consignment;

	@Mock
	private EventService eventService;

	@Mock
	private SendToDataHubResult result;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		action = new SendConsignmentToDatahubAction();
		action.setSendConsignmentToDataHubHelper(sendConsignmentToDataHubHelper);
		action.setModelService(modelService);
		action.setMaxRetries(DEFAULT_MAX_RETRIES);
		action.setRetryDelay(DEFAULT_RETRY_DELAY);
		action.setProcessParameterHelper(processParameterHelper);
		consignment.setCode(CONSIGNMENT_CODE);
	}

	@Test
	public void testActionOkForSendingConsignmentToDatahub()
	{
		Mockito.when(result.isSuccess()).thenReturn(true);
		Mockito.when(sendConsignmentToDataHubHelper.createAndSendRawItem(consignment)).thenReturn(result);

		final ConsignmentProcessModel consignmentProcessModel = new ConsignmentProcessModel();
		consignmentProcessModel.setConsignment(consignment);
		final Transition transition = action.executeAction(consignmentProcessModel);

		Assert.assertEquals(Transition.OK, transition);
	}

	@Test
	public void testActionNotOkForSendingConsignmentToDatahub()
	{
		Mockito.when(sendConsignmentToDataHubHelper.createAndSendRawItem(consignment)).thenReturn(result);
		Mockito.when(consignment.getCode()).thenReturn(CONSIGNMENT_CODE);

		final ConsignmentProcessModel consignmentProcessModel = new ConsignmentProcessModel();
		consignmentProcessModel.setConsignment(consignment);
		consignmentProcessModel.setSendConsignmentRetryCount(20);

		final Transition transition = action.executeAction(consignmentProcessModel);

		Assert.assertEquals(Transition.NOK, transition);
	}
}
