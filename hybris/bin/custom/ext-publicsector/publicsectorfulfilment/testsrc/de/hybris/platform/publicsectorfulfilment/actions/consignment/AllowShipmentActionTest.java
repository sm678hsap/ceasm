/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.warehouse.Process2WarehouseAdapter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * AllowShipmentActionTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AllowShipmentActionTest {

	@InjectMocks
	private AllowShipmentAction allowShipmentAction;

	@Mock
	private Process2WarehouseAdapter process2WarehouseAdapter;

	@Mock
	private ConsignmentProcessModel consignmentProcessModel;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private AbstractOrderModel abstractOrderModel;

	@Mock
	private OrderStatus orderStatus;

	@Before
	public void setUp() {
		allowShipmentAction = new AllowShipmentAction();
		allowShipmentAction.setProcess2WarehouseAdapter(process2WarehouseAdapter);
	}

	@Test
	public void testExecute() {
		given(consignmentProcessModel.getConsignment()).willReturn(consignmentModel);
		given(abstractOrderModel.getStatus()).willReturn(OrderStatus.CANCELLED);
		given(consignmentModel.getOrder()).willReturn(abstractOrderModel);
		allowShipmentAction.execute(consignmentProcessModel);
	}

}
