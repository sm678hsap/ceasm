/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * ConfirmConsignmentPickupActionTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConfirmConsignmentPickupActionTest {

	@InjectMocks
	private ConfirmConsignmentPickupAction confirmConsignmentPickupAction;

	@Mock
	private ConsignmentProcessModel consignmentProcessModel;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private ModelService modelService;

	@Test
	public void testExecute() {
		given(consignmentProcessModel.getConsignment()).willReturn(consignmentModel);
		confirmConsignmentPickupAction.setModelService(modelService);
		confirmConsignmentPickupAction.execute(consignmentProcessModel);
	}
}
