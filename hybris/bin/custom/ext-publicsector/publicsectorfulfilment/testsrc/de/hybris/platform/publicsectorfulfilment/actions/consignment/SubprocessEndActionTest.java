/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * SubprocessEndActionTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubprocessEndActionTest {

	private static final String CONSIGNMENT = "consignment";

	private static final String ORDER_PROCESS = "orderProcess";

	@InjectMocks
	private SubprocessEndAction subprocessEndAction;

	@Mock
	private ConsignmentProcessModel consignmentProcessModel;

	@Mock
	private BusinessProcessService businessProcessService;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private OrderProcessModel orderProcessModel;

	@Before
	public void setUp() {
		subprocessEndAction = new SubprocessEndAction();
		subprocessEndAction.setBusinessProcessService(businessProcessService);
		consignmentProcessModel.setParentProcess(orderProcessModel);
	}

	@Test
	public void testExecuteAction() {
		given(orderProcessModel.getCode()).willReturn(ORDER_PROCESS);
		given(consignmentProcessModel.getParentProcess()).willReturn(orderProcessModel);
		given(consignmentProcessModel.getCode()).willReturn(CONSIGNMENT);
		subprocessEndAction.executeAction(consignmentProcessModel);
	}

}
