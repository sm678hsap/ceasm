/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions;

import de.hybris.platform.core.Registry;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.publicsectorfulfilment.constants.PublicsectorfulfilmentConstants;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class SplitOrder extends TestActionTemp
{
	private static final Logger LOG = LoggerFactory.getLogger(SplitOrder.class);

	private int subprocessCount = 1;

	public void setSubprocessCount(final int subprocessCount)
	{
		this.subprocessCount = subprocessCount;
	}

	@Override
	public String execute(final BusinessProcessModel process) throws Exception // NOPMD
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass());

		final BusinessProcessParameterModel warehouseCounter = new BusinessProcessParameterModel();
		warehouseCounter.setName(PublicsectorfulfilmentConstants.CONSIGNMENT_COUNTER);
		warehouseCounter.setProcess(process);
		warehouseCounter.setValue(Integer.valueOf(subprocessCount));
		save(warehouseCounter);

		final BusinessProcessParameterModel params = new BusinessProcessParameterModel();
		params.setName(PublicsectorfulfilmentConstants.PARENT_PROCESS);
		params.setValue(process.getCode());

		for (int i = 0; i < subprocessCount; i++)
		{
			final ConsignmentProcessModel consProcess = modelService.create(ConsignmentProcessModel.class);
			consProcess.setParentProcess((OrderProcessModel) process);
			consProcess.setCode(process.getCode() + "_" + i);
			consProcess.setProcessDefinitionName("consignment-process-test");
			params.setProcess(consProcess);
			consProcess.setContextParameters(Arrays.asList(params));
			consProcess.setState(ProcessState.CREATED);
			modelService.save(consProcess);
			getBusinessProcessService().startProcess(consProcess);
			LOG.info("Subprocess: " + process.getCode() + "_" + i + " started");
		}

		return "OK";
	}

	/**
	 * @return the businessProcessService
	 */
	@Override
	public BusinessProcessService getBusinessProcessService()
	{
		return (BusinessProcessService) Registry.getApplicationContext().getBean("businessProcessService");
	}

}
