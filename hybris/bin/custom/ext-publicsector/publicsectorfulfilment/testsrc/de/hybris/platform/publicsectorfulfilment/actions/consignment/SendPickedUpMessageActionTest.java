/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehouse.Process2WarehouseAdapter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * SendPickedUpMessageActionTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendPickedUpMessageActionTest {

	private static final String CONSIGNMENT = "consignment";

	@InjectMocks
	private SendPickedUpMessageAction sendPickedUpMessageAction;

	@Mock
	private ConsignmentProcessModel consignmentProcessModel;

	@Mock
	private Process2WarehouseAdapter process2WarehouseAdapter;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private ModelService modelService;

	@Mock
	private EventService eventService;

	@Before
	public void setUp() {
		sendPickedUpMessageAction = new SendPickedUpMessageAction();
		sendPickedUpMessageAction.setEventService(eventService);
	}

	@Test
	public void testExecuteAction() {
		given(consignmentProcessModel.getCode()).willReturn(CONSIGNMENT);
		sendPickedUpMessageAction.executeAction(consignmentProcessModel);
	}

}
