/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class SubprocessesCompleted extends TestActionTemp<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SubprocessesCompleted.class);

	@Override
	public String execute(final OrderProcessModel process) throws Exception// NOPMD
	{
		for (final ConsignmentProcessModel subProcess : process.getConsignmentProcesses())
		{
			modelService.refresh(subProcess);
			if (!subProcess.getDone())
			{

				LOG.info(
						"Process: " + process.getCode() + " found  subprocess " + subProcess.getCode() + " incomplete -> wait again!");
				return "NOK";
			}
		}
		return "OK";
	}

}
