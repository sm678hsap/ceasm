/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.warehouse.Process2WarehouseAdapter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * SetERPConsignmentCompletionActionTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SetERPConsignmentCompletionActionTest {

	@InjectMocks
	private SetERPConsignmentCompletionAction setERPConsignmentCompletionAction;

	@Mock
	private ConsignmentProcessModel consignmentProcessModel;

	@Mock
	private Process2WarehouseAdapter process2WarehouseAdapter;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private ConsignmentStatus consignmentStatus;

	@Test
	public void testExecuteAction() {
		given(consignmentProcessModel.getConsignment()).willReturn(consignmentModel);
		given(consignmentModel.getStatus()).willReturn(consignmentStatus);
		setERPConsignmentCompletionAction.executeAction(consignmentProcessModel);
	}
}
