/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.WarehouseConsignmentState;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * ReceiveConsignmentStatusActionTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ReceiveConsignmentStatusActionTest {

	@InjectMocks
	private ReceiveConsignmentStatusAction receiveConsignmentStatusAction;

	@Mock
	private ConsignmentProcessModel consignmentProcessModel;

	@Mock
	private ModelService modelService;

	@Mock
	private WarehouseConsignmentState warehouseConsignmentState;

	@Test
	public void testExecute() {
		given(consignmentProcessModel.getWarehouseConsignmentState()).willReturn(warehouseConsignmentState);
		receiveConsignmentStatusAction.setModelService(modelService);
		receiveConsignmentStatusAction.execute(consignmentProcessModel);
	}

}
