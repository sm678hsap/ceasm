/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.order;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.publicsectorfulfilment.constants.PublicsectorfulfilmentConstants;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for EnrichOrderAction class
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EnrichOrderActionTest
{

	@InjectMocks
	private final EnrichOrderAction action = new EnrichOrderAction();

	@Mock
	private DeliveryModeService deliveryModeService;

	@Mock
	private CalculationService calculationService;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private ModelService modelService;

	@Mock
	private OrderProcessModel process;

	@Mock
	private DeliveryModeModel deliveryMode;

	@Mock
	private CountryModel country;

	private AddressModel addressModel;

	private InvoicePaymentInfoModel paymentInfoModel;

	private OrderModel order;

	/**
	 * Setup class
	 */
	@Before
	public void setUp()
	{
		order = new OrderModel();
		addressModel = new AddressModel();
		paymentInfoModel = new InvoicePaymentInfoModel();
		Mockito.when(process.getOrder()).thenReturn(order);
		Mockito.when(deliveryModeService.getDeliveryModeForCode(PublicsectorfulfilmentConstants.NOT_APPLICABLE))
				.thenReturn(deliveryMode);
		Mockito.when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.when(modelService.create(InvoicePaymentInfoModel.class)).thenReturn(paymentInfoModel);
		Mockito.when(commonI18NService.getCountry(PublicsectorfulfilmentConstants.DEFAULT_COUNTRY_CODE)).thenReturn(country);
		Mockito.when(country.getIsocode()).thenReturn(PublicsectorfulfilmentConstants.DEFAULT_COUNTRY_CODE);
	}

	/**
	 * Test Order where delivery mode is missing
	 *
	 * @throws RetryLaterException
	 * @throws Exception
	 */
	@Test
	public void testActionForOrderWithoutDeliveryMode() throws RetryLaterException, Exception // NOSONAR
	{
		action.executeAction(process);
		Assert.assertNotNull(order.getDeliveryMode());
		Assert.assertEquals(order.getDeliveryMode(), deliveryMode);
	}

	/**
	 * Test Order where delivery address is missing
	 *
	 * @throws RetryLaterException
	 * @throws Exception
	 */
	@Test
	public void testActionForOrderWithoutDeliveryAddress() throws RetryLaterException, Exception // NOSONAR
	{
		action.executeAction(process);
		Assert.assertNotNull(order.getDeliveryAddress());
		Assert.assertEquals(order.getDeliveryAddress().getCountry().getIsocode(),
				PublicsectorfulfilmentConstants.DEFAULT_COUNTRY_CODE);
	}

	/**
	 * Test Order where payment info is missing
	 *
	 * @throws RetryLaterException
	 * @throws Exception
	 */
	@Test
	public void testActionForOrderWithoutPaymentInfo() throws RetryLaterException, Exception // NOSONAR
	{
		action.executeAction(process);
		Assert.assertNotNull(order.getPaymentInfo());
		Assert.assertEquals(order.getPaymentInfo().getCode(), PublicsectorfulfilmentConstants.NOT_APPLICABLE);
	}

	/**
	 * Test Order where delivery mode, delivery address and payment info is missing
	 *
	 * @throws RetryLaterException
	 * @throws Exception
	 */
	@Test
	public void testActionForOrderWithoutVitalDetails() throws RetryLaterException, Exception // NOSONAR
	{
		action.executeAction(process);

		Assert.assertNotNull(order.getDeliveryMode());
		Assert.assertEquals(order.getDeliveryMode(), deliveryMode);

		Assert.assertNotNull(order.getDeliveryAddress());
		Assert.assertEquals(order.getDeliveryAddress().getCountry().getIsocode(),
				PublicsectorfulfilmentConstants.DEFAULT_COUNTRY_CODE);

		Assert.assertNotNull(order.getPaymentInfo());
		Assert.assertEquals(order.getPaymentInfo().getCode(), PublicsectorfulfilmentConstants.NOT_APPLICABLE);
	}

}
