/**
 *
 */
package de.hybris.platform.publicsectorfulfilment.actions.consignment;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehouse.Process2WarehouseAdapter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * SendConsignmentToWarehouseActionTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendConsignmentToWarehouseActionTest {

	@InjectMocks
	private SendConsignmentToWarehouseAction sendConsignmentToWarehouseAction;

	@Mock
	private ConsignmentProcessModel consignmentProcessModel;

	@Mock
	private Process2WarehouseAdapter process2WarehouseAdapter;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private ModelService modelService;

	@Before
	public void setUp() {
		sendConsignmentToWarehouseAction = new SendConsignmentToWarehouseAction();
		sendConsignmentToWarehouseAction.setProcess2WarehouseAdapter(process2WarehouseAdapter);
		sendConsignmentToWarehouseAction.setModelService(modelService);
	}

	@Test
	public void testExecuteAction() {
		given(consignmentProcessModel.getConsignment()).willReturn(consignmentModel);
		sendConsignmentToWarehouseAction.executeAction(consignmentProcessModel);
	}

}
