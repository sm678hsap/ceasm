/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.template.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.publicsectorfacades.strategies.PSYFormsStrategy;
import de.hybris.platform.publicservices.template.PSYFormTemplateService;
import de.hybris.platform.xyformscommercefacades.strategies.YFormsStrategy;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsservices.enums.YFormDataActionEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * This is unit test class for {@link DefaultPSYFormTemplateFacade}
 */
@UnitTest
public class DefaultPSYFormTemplateFacadeUnitTest
{
	@InjectMocks
	private DefaultPSYFormTemplateFacade psYformTemplateFacade;

	@Mock
	private PSYFormsStrategy psYFormsStrategy;

	@Mock
	private YFormsStrategy yformsStratgey;

	@Mock
	private PSYFormTemplateService psYFormTemplateService;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		psYformTemplateFacade = new DefaultPSYFormTemplateFacade();
		psYformTemplateFacade.setPsYFormsStrategy(psYFormsStrategy);
		psYformTemplateFacade.setPsYFormTemplateService(psYFormTemplateService);
		psYformTemplateFacade.setyFormsStrategy(yformsStratgey);
	}

	@Test
	public void testGetYFormHTMLForServiceForNullOrder()
	{
		given(psYFormsStrategy.getYFormOrderEntry(null)).willReturn(null);
		final List<String> yFormHTML = new ArrayList<>();
		Assert.assertEquals(psYformTemplateFacade.getYFormHTMLForService(null), yFormHTML);
	}

	@Test
	public void testGetYFormHTMLForServiceWithoutTemplate()
	{
		final OrderEntryData orderEntry = new OrderEntryData();
		final ProductData product = new ProductData();
		product.setCode("test");
		orderEntry.setProduct(product);
		final OrderData order = new OrderData();
		order.setEntries(Arrays.asList(orderEntry));
		given(psYFormsStrategy.getYFormOrderEntryByProduct(order, product.getCode())).willReturn(orderEntry);
		given(Boolean.valueOf(psYFormTemplateService.isServiceHasTemplate("test"))).willReturn(Boolean.FALSE);
		given(yformsStratgey.getFormsInlineHtml(YFormDataActionEnum.VIEW, order)).willReturn(Arrays.asList("<html></html>"));
		final List<String> html = psYformTemplateFacade.getYFormHTMLForService(order);
		final List<String> expectedHtml = new ArrayList<String>();
		expectedHtml.add("<html></html>");
		Assert.assertEquals(html, expectedHtml);
	}

	@Test
	public void testGetYFormHTMLForServiceWithTemplateAndWithoutFormData()
	{
		final OrderEntryData orderEntry = new OrderEntryData();

		final ProductData product = new ProductData();
		product.setCode("test");
		orderEntry.setProduct(product);
		final Collection<YFormDataData> yFormDataList = new ArrayList<>();
		final OrderData order = new OrderData();
		orderEntry.setFormDataData(yFormDataList);
		order.setEntries(Arrays.asList(orderEntry));
		given(psYFormsStrategy.getYFormOrderEntryByProduct(order, product.getCode())).willReturn(orderEntry);
		given(Boolean.valueOf(psYFormTemplateService.isServiceHasTemplate("test"))).willReturn(Boolean.TRUE);
		final List<String> html = psYformTemplateFacade.getYFormHTMLForService(order);
		final List<String> expectedHtml = new ArrayList<String>();
		Assert.assertEquals(html, expectedHtml);
	}

	@Test
	public void testGetYFormHTMLForServiceWithTemplateAndFormData()
	{
		final OrderEntryData orderEntry = new OrderEntryData();
		final ProductData product = new ProductData();
		product.setCode("test");
		orderEntry.setProduct(product);
		final YFormDataData yformdatadata = new YFormDataData();
		yformdatadata.setContent("sampleContent");
		final Collection<YFormDataData> yFormDataList = Arrays.asList(yformdatadata);
		orderEntry.setFormDataData(yFormDataList);
		final OrderData order = new OrderData();
		order.setEntries(Arrays.asList(orderEntry));
		given(psYFormsStrategy.getYFormOrderEntryByProduct(order, product.getCode())).willReturn(orderEntry);
		given(Boolean.valueOf(psYFormTemplateService.isServiceHasTemplate("test"))).willReturn(Boolean.TRUE);
		given(psYFormTemplateService.getTemplateHTMLForService(product, (List<YFormDataData>) yFormDataList))
				.willReturn("<h1>sample template</h2>");
		final List<String> html = psYformTemplateFacade.getYFormHTMLForService(order);
		final List<String> expectedHtml = new ArrayList<String>();
		expectedHtml.add("<h1>sample template</h2>");
		Assert.assertEquals(html, expectedHtml);
	}
}
