package de.hybris.platform.publicsectorfacades.orders.converters.populators;
/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */


import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectorfacades.order.converters.populators.PSOrderHistoryPopulator;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of PSOrderHistoryPopulatorUnitTest. Unit test for {@link PSOrderHistoryPopulator}
 */
@UnitTest
public class PSOrderHistoryPopulatorUnitTest
{
	private static final String TEST_DELIVERY_MODE = "sampleStandard";

	private static final String TEST_PRODUCT_CODE = "1234";

	@InjectMocks
	private PSOrderHistoryPopulator psOrderHistoryPopulator;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		psOrderHistoryPopulator = new PSOrderHistoryPopulator();
	}

	@Test
	public void testPopulate()
	{
		final OrderModel orderModel = new OrderModel();
		final DeliveryModeModel deliveryModel = Mockito.mock(DeliveryModeModel.class);
		Mockito.when(deliveryModel.getCode()).thenReturn(TEST_DELIVERY_MODE);
		orderModel.setDeliveryMode(deliveryModel);
		final PSDocumentModel psDocument = Mockito.mock(PSDocumentModel.class);
		final Collection<PSDocumentModel> psDocuments = Arrays.asList(psDocument);
		orderModel.setPsDocument(psDocuments);
		final PSServiceProductModel serviceProduct = Mockito.mock(PSServiceProductModel.class);
		Mockito.when(serviceProduct.getName()).thenReturn(TEST_PRODUCT_CODE);
		final OrderEntryModel orderEntryModel = new OrderEntryModel();
		orderEntryModel.setProduct(serviceProduct);
		orderModel.setEntries(Arrays.asList(orderEntryModel));

		final OrderHistoryData target = new OrderHistoryData();
		psOrderHistoryPopulator.populate(orderModel, target);

		assertEquals(target.getDeliveryMode().getCode(), TEST_DELIVERY_MODE);
		assertEquals(Boolean.valueOf(target.isHasDocument()), Boolean.TRUE);
		assertEquals(target.getServiceRequest(), TEST_PRODUCT_CODE);
	}
}
