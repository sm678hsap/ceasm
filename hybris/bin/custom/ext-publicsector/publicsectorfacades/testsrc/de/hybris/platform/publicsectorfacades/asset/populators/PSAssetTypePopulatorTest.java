/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.publicsectorfacades.asset.populators;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.asset.data.PSAssetTypeData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.publicsectorservices.model.PSAssetTypeModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Arrays;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test class for PSAssetTypePopulator
 */
@UnitTest
public class PSAssetTypePopulatorTest
{
	private static final String ASSET_TYPE_CODE = "car";
	private static final String ASSET_TYPE_NAME = "Car";
	private static final String ASSET_TYPE_MEDIA_URL = "mediaUrl";
	private static final String ASSET_TYPE_MEDIA_FORMAT_NAME = "mediaFormatName";
	private static final String ISO_CODE = "AU";

	private AbstractPopulatingConverter<PSAssetTypeModel, PSAssetTypeData> assetTypeConverter;
	private final PSAssetTypePopulator assetTypePopulator = new PSAssetTypePopulator();

	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private PSAssetTypeModel assetTypeModel;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		assetTypePopulator.setCommonI18NService(commonI18NService);

		assetTypeConverter = new AbstractPopulatingConverter<PSAssetTypeModel, PSAssetTypeData>();
		assetTypeConverter.setPopulators(Arrays.asList(assetTypePopulator));
	}

	@Test
	public void testConvert()
	{
		final LanguageModel langModel = mock(LanguageModel.class);
		when(langModel.getIsocode()).thenReturn(ISO_CODE);

		final Locale locale = new Locale(ISO_CODE);
		when(commonI18NService.getCurrentLanguage()).thenReturn(langModel);
		when(commonI18NService.getLocaleForIsoCode(ISO_CODE)).thenReturn(locale);

		given(assetTypeModel.getCode()).willReturn(ASSET_TYPE_CODE);
		given(assetTypeModel.getName(locale)).willReturn(ASSET_TYPE_NAME);

		final MediaModel media = mockMediaModel();
		given(assetTypeModel.getIcon(locale)).willReturn(media);

		final PSAssetTypeData assetTypeData = new PSAssetTypeData();
		assetTypeConverter.populate(assetTypeModel, assetTypeData);
		Assert.assertEquals(ASSET_TYPE_CODE, assetTypeData.getCode());
		Assert.assertEquals(ASSET_TYPE_NAME, assetTypeData.getName());
		Assert.assertEquals(ASSET_TYPE_MEDIA_URL, assetTypeData.getIconMediaURL());
	}

	private MediaModel mockMediaModel()
	{
		final MediaModel mediaModel = mock(MediaModel.class);
		when(mediaModel.getURL()).thenReturn(ASSET_TYPE_MEDIA_URL);

		final MediaFormatModel mediaFormat = mock(MediaFormatModel.class);
		when(mediaFormat.getName()).thenReturn(ASSET_TYPE_MEDIA_FORMAT_NAME);

		when(mediaModel.getMediaFormat()).thenReturn(mediaFormat);

		return mediaModel;
	}
}
