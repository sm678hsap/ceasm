/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartParameterData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.publicsectorfacades.strategies.PSYFormsStrategy;
import de.hybris.platform.publicsectorservices.order.PSCartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;

import java.util.Collections;
import java.util.Date;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * DefaultPSCartFacadeTest unit test class
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSCartFacadeTest
{

	private static final String DRAFT_LOCK_PERIOD = "draft.locked.period";
	private static final String TEST_SAVE_DRAFT_URL = "test_url";
	private static final String BILLING_TO_COUNRTY = "sample_billing_counrty";
	private static final String BILLING_TO_STATE = "sample_billing_state";
	private static final String SAMPLE_TITLE = "Mr";
	private static final String SAMPLE_RETURN_URL = "sample_return_url";
	private static final String SAMPLE_CART_CODE = "cartCode";
	private static final String SAMPLE_DRAFT_NUMBER = "draftNumber";
	private static final String SAMPLE_EMAIL = "sampleEmail";
	private static final String SAMPLE_COUNTRY_ISOCODE = "in";
	private static final int CART_LOCKED_PERIOD = 3600;


	@InjectMocks
	private final DefaultPSCartFacade psCartFacade = new DefaultPSCartFacade();

	@Mock
	private PSCartService psCartService;

	@Mock
	private ModelService modelService;

	@Mock
	private CommonI18NService commoni18nService;

	@Mock
	private UserService userService;

	@Mock
	private CartService cartService;

	@Mock
	private SessionService sessionService;

	@Mock
	private CartFacade cartFacade;

	@Mock
	private SaveCartFacade saveCartFacade;

	@Mock
	private CountryModel country;

	@Mock
	private CartModel cart;

	@Mock
	private AddressModel billingAddress;

	@Mock
	private UserModel user;

	@Mock
	private CartData cartData;

	@Mock
	private PSYFormsStrategy psYFormsStrategy;

	@Mock
	private OrderEntryData orderEntryData;

	@Mock
	private YFormFacade yFormFacade;

	@Mock
	private CommerceCartService commerceCartService;

	@Mock
	private Date date;

	@Mock
	private TimeService timeService;

	@Mock
	private Converter<CommerceCartRestoration, CartRestorationData> cartRestorationConverter;

	@Mock
	private CommerceSaveCartService commerceSaveCartService;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;

	@Mock
	private FlexibleSearchService flexibleSearchService;

	@Before
	public void setUp()
	{
		psCartFacade.setCartRestorationConverter(cartRestorationConverter);
		psCartFacade.setCommerceSaveCartService(commerceSaveCartService);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		configuration.setProperty(DRAFT_LOCK_PERIOD, 3600);
	}

	@Test
	public void testSetDraftCartDetails()
	{
		psCartFacade.setDraftCartDetails(TEST_SAVE_DRAFT_URL);
		Mockito.verify(psCartService).setDraftCartDetails(TEST_SAVE_DRAFT_URL);
	}

	@Test
	public void testGetNewBillingAddress()
	{
		psCartFacade.getNewBillingAddress();
		Mockito.verify(modelService).create(AddressModel.class);
	}

	@Test
	public void testGetI18NCountry()
	{
		psCartFacade.getI18NCountry(BILLING_TO_COUNRTY);
		Mockito.verify(commoni18nService).getCountry(BILLING_TO_COUNRTY);
	}

	@Test
	public void testGetI18NRegion()
	{
		Mockito.when(country.getIsocode()).thenReturn(SAMPLE_COUNTRY_ISOCODE);
		psCartFacade.getI18NRegion(country, BILLING_TO_STATE);
		Mockito.verify(commoni18nService).getRegion(country, SAMPLE_COUNTRY_ISOCODE + "-" + BILLING_TO_STATE);
	}

	@Test
	public void testGetTitleForCode()
	{
		psCartFacade.getTitleForCode(SAMPLE_TITLE);
		Mockito.verify(userService).getTitleForCode(SAMPLE_TITLE);
	}

	@Test
	public void testSaveCreditCardDetailsWhenSessionCartExists()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);

		psCartFacade.saveCreditCardDetails(null, billingAddress);

		Mockito.verify(billingAddress).setOwner(cart);
		Mockito.verify(modelService).save(billingAddress);
		Mockito.verify(modelService).refresh(billingAddress);
	}

	@Test
	public void testSaveCreditCardDetailsWhenSessionCartDoesntExists()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.FALSE);

		psCartFacade.saveCreditCardDetails(null, billingAddress);

		Mockito.verify(cartService).hasSessionCart();
	}

	@Test
	public void testSavePaymentDetailsToCartWhenSessionCartExists()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);

		psCartFacade.savePaymentDetailsToCart(billingAddress);

		Mockito.verify(cart).setPaymentAddress(billingAddress);
		Mockito.verify(modelService).save(cart);
	}

	@Test
	public void testSavePaymentDetailsToCartWhenSessionCartDoesntExists()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.FALSE);

		psCartFacade.savePaymentDetailsToCart(billingAddress);

		Mockito.verifyZeroInteractions(cart);
		Mockito.verify(cartService).hasSessionCart();
	}

	@Test
	public void testRemoveExistingSessionCartWhenSessionCartExistsWithNoReturnURL()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);

		psCartFacade.removeExistingSessionCart();

		Mockito.verify(cartService).removeSessionCart();
		Mockito.verify(cartService).hasSessionCart();
		Mockito.verify(cartService).hasSessionCart();
	}

	@Test
	public void testRemoveExistingSessionCartWhenSessionCartExistsWithReturnURL()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		Mockito.when(cart.getReturnURL()).thenReturn(SAMPLE_RETURN_URL);
		psCartFacade.removeExistingSessionCart();

		Mockito.verify(sessionService).removeAttribute(DefaultPSCartFacade.SESSION_CART_PARAMETER_NAME);
	}

	@Test
	public void testRemoveExistingSessionCartWhenSessionCartDoesntExists()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.FALSE);

		psCartFacade.removeExistingSessionCart();

		Mockito.verify(cartService).hasSessionCart();
	}

	@Test
	public void testRestoreCart() throws CommerceSaveCartException
	{
		psCartFacade.restoreCart(SAMPLE_CART_CODE);

		Mockito.verify(saveCartFacade).restoreSavedCart(Mockito.any(CommerceSaveCartParameterData.class));
		Mockito.verify(cartFacade).getSessionCart();
	}

	@Test
	public void testIsCartAssociatedWithCurrentUserWhenCartIsAssociatedWithUser() throws CommerceSaveCartException
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(Boolean.valueOf(psCartService.isCartAssociatedWithUser(SAMPLE_CART_CODE, user))).thenReturn(Boolean.TRUE);

		Assert.assertTrue(psCartFacade.isCartAssociatedWithCurrentUser(SAMPLE_CART_CODE));
	}

	@Test
	public void testIsCartAssociatedWithCurrentUserWhenCartIsNotAssociatedWithUser() throws CommerceSaveCartException
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(Boolean.valueOf(psCartService.isCartAssociatedWithUser(SAMPLE_CART_CODE, user))).thenReturn(Boolean.FALSE);

		Assert.assertFalse(psCartFacade.isCartAssociatedWithCurrentUser(SAMPLE_CART_CODE));
	}

	@Test
	public void testIsYFormVersionUpdatedForCartWhenYFormIsUpdated() throws CommerceSaveCartException, YFormServiceException
	{
		Mockito.when(Boolean.valueOf(cartFacade.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartFacade.getSessionCart()).thenReturn(cartData);
		Mockito.when(psYFormsStrategy.getYFormOrderEntry(cartData)).thenReturn(orderEntryData);
		mockYFormRelatedData(1, 2);

		Assert.assertTrue(psCartFacade.isYFormVersionUpdatedForCart());
	}

	@Test
	public void testIsYFormVersionUpdatedForCartWhenYFormIsNotUpdated() throws CommerceSaveCartException, YFormServiceException
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartFacade.getSessionCart()).thenReturn(cartData);
		Mockito.when(psYFormsStrategy.getYFormOrderEntry(cartData)).thenReturn(orderEntryData);
		mockYFormRelatedData(1, 1);

		Assert.assertFalse(psCartFacade.isYFormVersionUpdatedForCart());
	}

	@Test
	public void testIsYFormVersionUpdatedForCartWhenNoYFormDataExists() throws CommerceSaveCartException, YFormServiceException
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartFacade.getSessionCart()).thenReturn(cartData);
		Mockito.when(psYFormsStrategy.getYFormOrderEntry(cartData)).thenReturn(orderEntryData);
		mockYFormRelatedData(1, 1);
		Mockito.when(orderEntryData.getFormDataData()).thenReturn(null);

		Assert.assertFalse(psCartFacade.isYFormVersionUpdatedForCart());
	}

	@Test
	public void testIsYFormVersionUpdatedForCartWhenOrderEntryExists() throws CommerceSaveCartException, YFormServiceException
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartFacade.getSessionCart()).thenReturn(cartData);
		Mockito.when(psYFormsStrategy.getYFormOrderEntry(cartData)).thenReturn(null);

		Assert.assertFalse(psCartFacade.isYFormVersionUpdatedForCart());
	}

	@Test
	public void testIsYFormVersionUpdatedForCartWhenNoSessionCart() throws CommerceSaveCartException, YFormServiceException
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.FALSE);

		Assert.assertFalse(psCartFacade.isYFormVersionUpdatedForCart());
	}

	private void mockYFormRelatedData(final int yFormVersion, final int latestYformVersion) throws YFormServiceException
	{
		final YFormDataData yFormDataData = Mockito.mock(YFormDataData.class);
		final YFormDefinitionData yForm = Mockito.mock(YFormDefinitionData.class);
		final YFormDefinitionData latestYForm = Mockito.mock(YFormDefinitionData.class);
		Mockito.when(orderEntryData.getFormDataData()).thenReturn(Collections.singletonList(yFormDataData));
		Mockito.when(yFormDataData.getFormDefinition()).thenReturn(yForm);
		Mockito.when(yFormFacade.getYFormDefinition(yForm.getApplicationId(), yForm.getFormId())).thenReturn(latestYForm);
		Mockito.when(Integer.valueOf(yForm.getVersion())).thenReturn(Integer.valueOf(yFormVersion));
		Mockito.when(Integer.valueOf(yForm.getVersion())).thenReturn(Integer.valueOf(latestYformVersion));
	}

	@Test
	public void testUpdateCartOnYFormVersionUpdate() throws CommerceSaveCartException, YFormServiceException
	{
		psCartFacade.updateCartOnYFormVersionUpdate();

		Mockito.verify(psCartService).updateCartOnYFormVersionUpdate();
	}

	@Test
	public void testRestoreCartForDraftNumberAndEmailAddressWhenCartExists()
			throws CommerceSaveCartException, YFormServiceException, CommerceCartRestorationException
	{
		Mockito.when(psCartService.getCartForGuestUserEmailAndCartCode(SAMPLE_DRAFT_NUMBER, SAMPLE_EMAIL)).thenReturn(cart);
		Mockito.when(cart.getUser()).thenReturn(user);
		Mockito.when(userService.getUserForUID(Mockito.anyString())).thenReturn(user);

		psCartFacade.restoreCartForDraftNumberAndEmailAddress(SAMPLE_DRAFT_NUMBER, SAMPLE_EMAIL);

		Mockito.verify(commerceCartService).restoreCart(Mockito.any(CommerceCartParameter.class));
		Mockito.verify(cartService).changeCurrentCartUser(user);
		Mockito.verify(commoni18nService).setCurrentCurrency(Mockito.any(CurrencyModel.class));
	}

	@Test
	public void testRestoreCartForDraftNumberAndEmailAddressWhenNoSuchCartExist()
			throws CommerceSaveCartException, YFormServiceException, CommerceCartRestorationException
	{
		Mockito.when(psCartService.getCartForGuestUserEmailAndCartCode(SAMPLE_DRAFT_NUMBER, SAMPLE_EMAIL)).thenReturn(null);

		Assert.assertNull(psCartFacade.restoreCartForDraftNumberAndEmailAddress(SAMPLE_DRAFT_NUMBER, SAMPLE_EMAIL));
	}

	@Test
	public void testRestoreAnonymousCartAndTakeOwnership()
			throws CommerceSaveCartException, YFormServiceException, CommerceCartRestorationException
	{
		Mockito.when(cart.getUser()).thenReturn(user);
		Mockito.when(userService.getUserForUID(Mockito.anyString())).thenReturn(user);

		psCartFacade.restoreAnonymousCartAndTakeOwnership(cart);

		Mockito.verify(commerceCartService).restoreCart(Mockito.any(CommerceCartParameter.class));
		Mockito.verify(cartService).changeCurrentCartUser(user);
		Mockito.verify(commoni18nService).setCurrentCurrency(Mockito.any(CurrencyModel.class));
	}

	@Test
	public void testRemoveCart()
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(userService.getUserForUID(Mockito.anyString())).thenReturn(user);

		psCartFacade.removeCart(SAMPLE_CART_CODE);

		Mockito.verify(psCartService).removeCart(SAMPLE_CART_CODE, user);
	}

	@Test
	public void testUpdateLastAccessTimeInCartForCurrentUser()
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(commerceCartService.getCartForCodeAndUser(SAMPLE_CART_CODE, user)).thenReturn(cart);
		Mockito.when(timeService.getCurrentTime()).thenReturn(date);

		psCartFacade.updateLastAccessTimeInCartForCurrentUser(SAMPLE_CART_CODE);

		Mockito.verify(cart).setLastAccessDate(date);
		Mockito.verify(modelService).save(cart);
	}

	@Test
	public void testUpdateReturnURLAndLastAccessTimeInCartForCurrentUser()
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(commerceCartService.getCartForCodeAndUser(SAMPLE_CART_CODE, user)).thenReturn(cart);
		Mockito.when(timeService.getCurrentTime()).thenReturn(date);

		psCartFacade.updateReturnURLAndLastAccessTimeInCartForCurrentUser(SAMPLE_CART_CODE, SAMPLE_RETURN_URL);

		Mockito.verify(cart).setReturnURL(SAMPLE_RETURN_URL);
		Mockito.verify(cart).setLastAccessDate(date);
		Mockito.verify(modelService).save(cart);
	}

	@Test
	public void testUpdateLastAccessTimeInCartForGuestUser()
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(psCartService.getCartForGuestUserEmailAndCartCode(SAMPLE_CART_CODE, SAMPLE_EMAIL)).thenReturn(cart);
		Mockito.when(timeService.getCurrentTime()).thenReturn(date);

		psCartFacade.updateLastAccessTimeInCartForGuestUser(SAMPLE_CART_CODE, SAMPLE_EMAIL);

		Mockito.verify(cart).setLastAccessDate(date);
		Mockito.verify(modelService).save(cart);
	}

	@Test
	public void testIsCurrentCartSavedDraftWhenSessionCartExists()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		Mockito.when(Boolean.valueOf(psCartService.isCartSavedDraft(cart))).thenReturn(Boolean.TRUE);

		Assert.assertTrue(psCartFacade.isCurrentCartSavedDraft());
	}

	@Test
	public void testIsCurrentCartSavedDraftWhenSessionCartDoesntExists()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.FALSE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cart);
		Mockito.when(Boolean.valueOf(psCartService.isCartSavedDraft(cart))).thenReturn(Boolean.TRUE);

		Assert.assertFalse(psCartFacade.isCurrentCartSavedDraft());
	}

	@Test
	public void testIsCustomerProfileUpdatedAfterDraftSavedWhenCustomerProfileUpdated()
	{
		final Date draftLastAccessTime = new Date(System.currentTimeMillis() - 1000L * 3600L * 24); //one day old
		final Date customerProfileUpdateTime = new Date(System.currentTimeMillis());

		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		Mockito.when(Boolean.valueOf(cartFacade.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartFacade.getSessionCart()).thenReturn(cartData);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(cartData.getLastAccessTime()).thenReturn(draftLastAccessTime);
		Mockito.when(customerModel.getProfileUpdateTime()).thenReturn(customerProfileUpdateTime);

		Assert.assertTrue(psCartFacade.isCustomerProfileUpdatedAfterDraftSaved());
	}

	@Test
	public void testIsCustomerProfileUpdatedAfterDraftSavedWhenCustomerProfileNotUpdated()
	{
		final Date draftLastAccessTime = new Date(System.currentTimeMillis() - 1000L * 3600L * 24); //one day old
		final Date customerProfileUpdateTime = new Date(System.currentTimeMillis() - 5000L * 3600L * 24); //five day old

		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		Mockito.when(Boolean.valueOf(cartFacade.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartFacade.getSessionCart()).thenReturn(cartData);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(cartData.getLastAccessTime()).thenReturn(draftLastAccessTime);
		Mockito.when(customerModel.getProfileUpdateTime()).thenReturn(customerProfileUpdateTime);

		Assert.assertFalse(psCartFacade.isCustomerProfileUpdatedAfterDraftSaved());
	}

	@Test
	public void testGetCartForCodeAndUser() throws CommerceSaveCartException
	{
		final String cartCode = "TestCart";
		final CartModel cartForCodeAndUser = new CartModel();
		cartForCodeAndUser.setCode(cartCode);

		final String customerId = "TestCustomer";
		final UserModel user = new UserModel();
		user.setUid(customerId);

		final CartData cart = new CartData();

		final CommerceCartRestoration cartRestoration = new CommerceCartRestoration();

		Mockito.when(userService.getUserForUID(customerId)).thenReturn(user);
		Mockito.when(commerceCartService.getCartForCodeAndUser(cartCode, user)).thenReturn(cartForCodeAndUser);
		Mockito.when(commerceSaveCartService.restoreSavedCart(Mockito.any(CommerceSaveCartParameter.class)))
				.thenReturn(cartRestoration);

		Mockito.when(cartFacade.getSessionCart()).thenReturn(cart);

		psCartFacade.getCartForCodeAndUser(cartCode, customerId);

	}

	@Test
	public void testSetCurrentUserOwnerForRelationshipCart() throws CommerceSaveCartException
	{
		final String sessionCustomerId = "SessionCustomer";
		final UserModel sessionUser = new UserModel();
		sessionUser.setUid(sessionCustomerId);

		final String customerId = "TestCustomer";
		final UserModel user = new UserModel();
		user.setUid(customerId);

		final String cartCode = "TestCart";
		final CartModel cartForCodeAndUser = new CartModel();
		cartForCodeAndUser.setCode(cartCode);
		cartForCodeAndUser.setUser(user);


		final CartData cart = new CartData();

		Mockito.when(cartService.hasSessionCart()).thenReturn(Boolean.TRUE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cartForCodeAndUser);
		Mockito.when(userService.getCurrentUser()).thenReturn(sessionUser);

		Mockito.when(cartFacade.getSessionCart()).thenReturn(cart);

		psCartFacade.setCurrentUserOwnerForRelationshipCart();

		Mockito.verify(modelService).save(cartForCodeAndUser);
		Mockito.verify(modelService).refresh(cartForCodeAndUser);

		Assert.assertEquals(user, cartForCodeAndUser.getUserInContext());

	}

	@Test
	public void testRemoveCartForUser()
	{
		final String customerId = "TestUser";
		Mockito.when(userService.getUserForUID(customerId)).thenReturn(user);

		psCartFacade.removeCartForUser(customerId, SAMPLE_CART_CODE);

		Mockito.verify(psCartService).removeCart(SAMPLE_CART_CODE, user);
	}


	@Test
	public void testCartLockedByCartOwnerAccessedByPoaUserTrue() throws CommerceSaveCartException
	{
		final String poaCustomerId = "PoaCustomer";
		final UserModel poaUser = new UserModel();
		poaUser.setUid(poaCustomerId);

		final String draftOwnerId = "DraftOwner";
		final UserModel cartOwner = new UserModel();
		cartOwner.setUid(draftOwnerId);

		final CustomerModel lockedByCustomer = new CustomerModel();
		lockedByCustomer.setUid(draftOwnerId);

		final Date currentDate = new Date();

		final Date addSecondsToDate = DateUtils.addSeconds(currentDate, -1800);
		final CartModel cart = new CartModel();
		cart.setCode("123456");
		cart.setModifiedtime(addSecondsToDate);
		cart.setLockedBy(lockedByCustomer);

		Mockito.when(cartService.hasSessionCart()).thenReturn(Boolean.TRUE);
		Mockito.when(userService.getCurrentUser()).thenReturn(poaUser);
		Mockito.when(configurationService.getConfiguration().getInt(DRAFT_LOCK_PERIOD, 3600)).thenReturn(CART_LOCKED_PERIOD);
		Mockito.when(timeService.getCurrentTime()).thenReturn(currentDate);
		Mockito.when(flexibleSearchService.getModelByExample(any())).thenReturn(cart);

		Assert.assertTrue(psCartFacade.isDraftLocked("123456"));
	}

	@Test
	public void testCartLockedByPoaUserAccessedByCartOwnerTrue() throws CommerceSaveCartException
	{
		final String poaCustomerId = "POACustomer";
		final UserModel poaUser = new UserModel();
		poaUser.setUid(poaCustomerId);

		final String draftOwnerId = "DraftOwner";
		final UserModel cartOwner = new UserModel();
		cartOwner.setUid(draftOwnerId);

		final CustomerModel lockedByCustomer = new CustomerModel();
		lockedByCustomer.setUid(poaCustomerId);

		final Date currentDate = new Date();
		final Date addSecondsToDate = DateUtils.addSeconds(currentDate, -1800);
		final CartModel cart = new CartModel();
		cart.setCode("123456");
		cart.setModifiedtime(addSecondsToDate);
		cart.setLockedBy(lockedByCustomer);

		Mockito.when(cartService.hasSessionCart()).thenReturn(Boolean.TRUE);
		Mockito.when(userService.getCurrentUser()).thenReturn(cartOwner);
		Mockito.when(configurationService.getConfiguration().getInt(DRAFT_LOCK_PERIOD, 3600)).thenReturn(CART_LOCKED_PERIOD);
		Mockito.when(timeService.getCurrentTime()).thenReturn(currentDate);
		Mockito.when(flexibleSearchService.getModelByExample(any())).thenReturn(cart);

		Assert.assertTrue(psCartFacade.isDraftLocked("123456"));
	}

	@Test
	public void testCartLockedByCartOwnerAccessedByPoaUserAfterLockingPeriodFalse() throws CommerceSaveCartException
	{
		final String poaCustomerId = "PoaCustomer";
		final UserModel poaUser = new UserModel();
		poaUser.setUid(poaCustomerId);

		final String draftOwnerId = "DraftOwner";
		final UserModel cartOwner = new UserModel();
		cartOwner.setUid(draftOwnerId);

		final CustomerModel lockedByCustomer = new CustomerModel();
		lockedByCustomer.setUid(draftOwnerId);

		final Date currentDate = new Date();

		final Date addSecondsToDate = DateUtils.addSeconds(currentDate, -1800);
		final CartModel cart = new CartModel();
		cart.setCode("123456");
		cart.setModifiedtime(addSecondsToDate);
		cart.setLockedBy(lockedByCustomer);

		final Date addSecondsToCurrentDate = DateUtils.addSeconds(currentDate, 3700);

		Mockito.when(cartService.hasSessionCart()).thenReturn(Boolean.TRUE);
		Mockito.when(userService.getCurrentUser()).thenReturn(poaUser);
		Mockito.when(configurationService.getConfiguration().getInt(DRAFT_LOCK_PERIOD, 3600)).thenReturn(CART_LOCKED_PERIOD);
		Mockito.when(timeService.getCurrentTime()).thenReturn(addSecondsToCurrentDate);
		Mockito.when(flexibleSearchService.getModelByExample(any())).thenReturn(cart);

		Assert.assertFalse(psCartFacade.isDraftLocked("123456"));
	}

}
