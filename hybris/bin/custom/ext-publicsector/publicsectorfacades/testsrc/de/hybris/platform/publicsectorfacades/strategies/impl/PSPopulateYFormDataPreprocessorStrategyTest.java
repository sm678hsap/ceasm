/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.xyformscommercefacades.data.YFormDefinitionFieldMappingsData;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;
import de.hybris.platform.xyformscommercefacades.form.data.FormDetailData;
import de.hybris.platform.xyformscommercefacades.form.impl.DefaultXYFormFacade;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;
import de.hybris.platform.xyformscommerceservices.service.YFormDefinitionFieldMappingsService;
import de.hybris.platform.xyformscommerceservices.strategies.YFormUserStrategy;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.form.YFormService;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class of PSPopulateYFormDataPreprocessorStrategy.
 */
@UnitTest
public class PSPopulateYFormDataPreprocessorStrategyTest
{

	@Mock
	private UserService userService;

	@Mock
	private Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> yformDefinitionFieldsConverter;

	@Mock
	private Converter<CustomerModel, YFormMappingsData> yFormMappingsConverter;

	@Mock
	private YFormService yformService;

	@Mock
	private DefaultXYFormFacade defaultXYFormFacade;

	@Mock
	private YFormUserStrategy defaultGetUserStrategy;

	@Mock
	private YFormDefinitionFieldMappingsService yFormDefinitionFieldMappingsService;

	@InjectMocks
	private PSPopulateYFormDataPreprocessorStrategy yFormDataPreprocessorStrategy;


	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		yFormDataPreprocessorStrategy = new PSPopulateYFormDataPreprocessorStrategy();
	}

	/**
	 * Applies the actual transformation to a formData
	 *
	 * @throws de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException
	 * @throws YFormServiceException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testTransformForMinor() throws YFormProcessorException, IllegalAccessException, YFormServiceException
	{

		getyFormDataPreprocessorStrategy().setDefaultXYFormFacade(defaultXYFormFacade);
		getyFormDataPreprocessorStrategy().setUserService(userService);
		getyFormDataPreprocessorStrategy().setyFormDefinitionFieldMappingsService(yFormDefinitionFieldMappingsService);
		getyFormDataPreprocessorStrategy().setYformDefinitionFieldsConverter(yformDefinitionFieldsConverter);
		getyFormDataPreprocessorStrategy().setYformService(yformService);
		getyFormDataPreprocessorStrategy().setyFormMappingsConverter(yFormMappingsConverter);
		getyFormDataPreprocessorStrategy().setUserStrategy(defaultGetUserStrategy);

		final String xmlContent = "<form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>"
				+ "<section-update-me><is-user-logged-in>false</is-user-logged-in>"
				+ "<label-logged-in-user/><label-unidentified-user/>" + "<user-name/><user-email/></section-update-me></form>";

		final FormDetailData formDetailData = new FormDetailData();
		formDetailData.setApplicationId("TestApplicationId");
		formDetailData.setFormDataId("TestFormId");

		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		final Map<String, Object> params = new HashMap<>();
		params.put("formDetailData", formDetailData);

		Mockito.when(defaultGetUserStrategy.getCurrentUserForCheckout()).thenReturn(customerModel);
		Mockito.when(customerModel.getType()).thenReturn(CustomerType.UNIDENTIFIED);
		Mockito.when(customerModel.getIsMinor()).thenReturn(true);

		final YFormMappingsData yFormMappingsData = new YFormMappingsData();
		Mockito.when(getyFormMappingsConverter().convert(customerModel)).thenReturn(yFormMappingsData);

		final Map<String, String> yformMappingsDataMap = new HashMap<>();
		yformMappingsDataMap.put("name", "TEST_USER");
		yformMappingsDataMap.put("email", "test_user@test.com");
		yformMappingsDataMap.put("isUserLoggedIn", "true");

		Mockito.when(getDefaultXYFormFacade().getMappingsData(yFormMappingsData)).thenReturn(yformMappingsDataMap);

		final YFormDefinitionFieldMappingsModel yFormDefinitionFieldMappingsModel = new YFormDefinitionFieldMappingsModel();
		Mockito.when(getyFormDefinitionFieldMappingsService().getMappingsForYForm(formDetailData.getApplicationId(),
				formDetailData.getFormId())).thenReturn(yFormDefinitionFieldMappingsModel);

		final YFormDefinitionFieldMappingsData yFormDefinitionFieldMappingsData = new YFormDefinitionFieldMappingsData();

		final Map<String, String> map = new HashMap<>();
		map.put("/form/section-update-me/user-name", "name");
		map.put("/form/section-update-me/user-email", "email");
		map.put("/form/section-update-me/is-user-logged-in", "isUserLoggedIn");
		yFormDefinitionFieldMappingsData.setYFormFieldMappings(map);

		Mockito.when(getYformDefinitionFieldsConverter().convert(yFormDefinitionFieldMappingsModel))
				.thenReturn(yFormDefinitionFieldMappingsData);

		final String resultXml = getyFormDataPreprocessorStrategy().transform(xmlContent, params);


		final String expectedXmlContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>" + "<section-update-me><is-user-logged-in>"
				+ "true" + "</is-user-logged-in><label-logged-in-user/><label-unidentified-user/>" + "<user-name>" + "TEST_USER"
				+ "</user-name><user-email>" + "test_user@test.com" + "</user-email></section-update-me></form>";

		Assert.assertEquals(expectedXmlContent, resultXml);

	}

	@Test
	public void testTransformForRegistered() throws YFormProcessorException, IllegalAccessException, YFormServiceException
	{

		getyFormDataPreprocessorStrategy().setDefaultXYFormFacade(defaultXYFormFacade);
		getyFormDataPreprocessorStrategy().setUserService(userService);
		getyFormDataPreprocessorStrategy().setyFormDefinitionFieldMappingsService(yFormDefinitionFieldMappingsService);
		getyFormDataPreprocessorStrategy().setYformDefinitionFieldsConverter(yformDefinitionFieldsConverter);
		getyFormDataPreprocessorStrategy().setYformService(yformService);
		getyFormDataPreprocessorStrategy().setyFormMappingsConverter(yFormMappingsConverter);
		getyFormDataPreprocessorStrategy().setUserStrategy(defaultGetUserStrategy);

		final String xmlContent = "<form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>"
				+ "<section-update-me><is-user-logged-in>false</is-user-logged-in>"
				+ "<label-logged-in-user/><label-unidentified-user/>" + "<user-name/><user-email/></section-update-me></form>";

		final FormDetailData formDetailData = new FormDetailData();
		formDetailData.setApplicationId("TestApplicationId");
		formDetailData.setFormDataId("TestFormId");

		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		final Map<String, Object> params = new HashMap<>();
		params.put("formDetailData", formDetailData);

		Mockito.when(defaultGetUserStrategy.getCurrentUserForCheckout()).thenReturn(customerModel);
		Mockito.when(customerModel.getType()).thenReturn(CustomerType.REGISTERED);
		Mockito.when(customerModel.getIsMinor()).thenReturn(false);

		final YFormMappingsData yFormMappingsData = new YFormMappingsData();
		Mockito.when(getyFormMappingsConverter().convert(customerModel)).thenReturn(yFormMappingsData);

		final Map<String, String> yformMappingsDataMap = new HashMap<>();
		yformMappingsDataMap.put("name", "TEST_USER");
		yformMappingsDataMap.put("email", "test_user@test.com");
		yformMappingsDataMap.put("isUserLoggedIn", "true");

		Mockito.when(getDefaultXYFormFacade().getMappingsData(yFormMappingsData)).thenReturn(yformMappingsDataMap);

		final YFormDefinitionFieldMappingsModel yFormDefinitionFieldMappingsModel = new YFormDefinitionFieldMappingsModel();
		Mockito.when(getyFormDefinitionFieldMappingsService().getMappingsForYForm(formDetailData.getApplicationId(),
				formDetailData.getFormId())).thenReturn(yFormDefinitionFieldMappingsModel);

		final YFormDefinitionFieldMappingsData yFormDefinitionFieldMappingsData = new YFormDefinitionFieldMappingsData();

		final Map<String, String> map = new HashMap<>();
		map.put("/form/section-update-me/user-name", "name");
		map.put("/form/section-update-me/user-email", "email");
		map.put("/form/section-update-me/is-user-logged-in", "isUserLoggedIn");
		yFormDefinitionFieldMappingsData.setYFormFieldMappings(map);

		Mockito.when(getYformDefinitionFieldsConverter().convert(yFormDefinitionFieldMappingsModel))
				.thenReturn(yFormDefinitionFieldMappingsData);

		final String resultXml = getyFormDataPreprocessorStrategy().transform(xmlContent, params);


		final String expectedXmlContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>" + "<section-update-me><is-user-logged-in>"
				+ "true" + "</is-user-logged-in><label-logged-in-user/><label-unidentified-user/>" + "<user-name>" + "TEST_USER"
				+ "</user-name><user-email>" + "test_user@test.com" + "</user-email></section-update-me></form>";

		Assert.assertEquals(expectedXmlContent, resultXml);

	}

	protected UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> getYformDefinitionFieldsConverter()
	{
		return yformDefinitionFieldsConverter;
	}

	public void setYformDefinitionFieldsConverter(
			final Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> yformDefinitionFieldsConverter)
	{
		this.yformDefinitionFieldsConverter = yformDefinitionFieldsConverter;
	}

	protected Converter<CustomerModel, YFormMappingsData> getyFormMappingsConverter()
	{
		return yFormMappingsConverter;
	}

	public void setyFormMappingsConverter(final Converter<CustomerModel, YFormMappingsData> yFormMappingsConverter)
	{
		this.yFormMappingsConverter = yFormMappingsConverter;
	}

	protected YFormService getYformService()
	{
		return yformService;
	}

	public void setYformService(final YFormService yformService)
	{
		this.yformService = yformService;
	}

	protected DefaultXYFormFacade getDefaultXYFormFacade()
	{
		return defaultXYFormFacade;
	}

	public void setDefaultXYFormFacade(final DefaultXYFormFacade defaultXYFormFacade)
	{
		this.defaultXYFormFacade = defaultXYFormFacade;
	}

	protected YFormDefinitionFieldMappingsService getyFormDefinitionFieldMappingsService()
	{
		return yFormDefinitionFieldMappingsService;
	}

	public void setyFormDefinitionFieldMappingsService(
			final YFormDefinitionFieldMappingsService yFormDefinitionFieldMappingsService)
	{
		this.yFormDefinitionFieldMappingsService = yFormDefinitionFieldMappingsService;
	}

	protected PSPopulateYFormDataPreprocessorStrategy getyFormDataPreprocessorStrategy()
	{
		return yFormDataPreprocessorStrategy;
	}

	public void setyFormDataPreprocessorStrategy(final PSPopulateYFormDataPreprocessorStrategy yFormDataPreprocessorStrategy)
	{
		this.yFormDataPreprocessorStrategy = yFormDataPreprocessorStrategy;
	}

}
