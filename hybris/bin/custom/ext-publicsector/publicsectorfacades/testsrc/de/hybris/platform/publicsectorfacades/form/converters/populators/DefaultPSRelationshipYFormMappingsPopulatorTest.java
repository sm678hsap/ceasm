/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.form.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationship.data.PSRelationshipData;
import de.hybris.platform.relationshipfacades.permission.PSPermissionFacade;
import de.hybris.platform.relationshipfacades.relationship.PSRelationshipFacade;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;
import de.hybris.platform.xyformscommercefacades.form.XYFormFacade;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class of {@link DefaultPSRelationshipYFormMappingsPopulator}
 */
@UnitTest
public class DefaultPSRelationshipYFormMappingsPopulatorTest
{

	@Mock
	private UserFacade userFacade;

	@Mock
	private UserService userService;

	@Mock
	private XYFormFacade xyFormFacade;

	@Mock
	private PSRelationshipFacade psRelationshipFacade;

	@Mock
	private PSPermissionFacade psPermissionFacade;

	@InjectMocks
	private DefaultPSRelationshipYFormMappingsPopulator populator;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		populator = new DefaultPSRelationshipYFormMappingsPopulator();
		populator.setXyFormFacade(xyFormFacade);
		populator.setUserFacade(userFacade);
		populator.setUserService(userService);
		populator.setPsRelationshipFacade(psRelationshipFacade);
		populator.setPsPermissionFacade(psPermissionFacade);
	}

	@Test
	public void testPopulate() throws RelationshipDoesNotExistException
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final YFormMappingsData yFormMappingsData = new YFormMappingsData();
		final TitleModel titleModel = new TitleModel();
		titleModel.setCode("TestTitle");

		final AddressData addressData = getTestAddressData();

		Mockito.when(customerModel.getContactEmail()).thenReturn("customer@customer.com");
		Mockito.when(customerModel.getName()).thenReturn("Test User");
		Mockito.when(customerModel.getTitle()).thenReturn(titleModel);
		Mockito.when(customerModel.getUid()).thenReturn("customer@customer.com");

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(Boolean.valueOf(psPermissionFacade.isPermitted(Mockito.any(), Mockito.any(), Mockito.anyString())))
				.thenReturn(Boolean.TRUE);
		Mockito.when(xyFormFacade.getDefaultAddressForUser(customerModel)).thenReturn(addressData);
		Mockito.when(Boolean.valueOf(userFacade.isAnonymousUser())).thenReturn(Boolean.TRUE);

		populator.populate(customerModel, yFormMappingsData);

		Assert.assertEquals("Test", yFormMappingsData.getFirstName());
		Assert.assertEquals("User", yFormMappingsData.getLastName());

		Assert.assertEquals("Test Address line 1", yFormMappingsData.getAddressLine1());
		Assert.assertEquals("Test Address line 2", yFormMappingsData.getAddressLine2());
		Assert.assertEquals("TestPostalCode", yFormMappingsData.getPostcode());
		Assert.assertEquals("TestTown", yFormMappingsData.getCity());
		Assert.assertEquals("TestPhone", yFormMappingsData.getPhone());
		Assert.assertEquals("TestCountry", yFormMappingsData.getCountry());
		Assert.assertEquals("TestTitle", yFormMappingsData.getTitle());
		Assert.assertEquals("customer@customer.com", yFormMappingsData.getEmail());
	}

	@Test
	public void testPopulateWhenUserIsMinor() throws RelationshipDoesNotExistException
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final YFormMappingsData yFormMappingsData = new YFormMappingsData();
		mockDataForPopulateMinorUser(customerModel);
		Mockito.when(customerModel.getIsMinor()).thenReturn(Boolean.TRUE);
		populator.populate(customerModel, yFormMappingsData);
		Assert.assertNull(yFormMappingsData.getEmail());
	}

	@Test
	public void testPopulateWhenUserIsNotMinor() throws RelationshipDoesNotExistException
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final YFormMappingsData yFormMappingsData = new YFormMappingsData();
		mockDataForPopulateMinorUser(customerModel);
		Mockito.when(customerModel.getIsMinor()).thenReturn(Boolean.FALSE);
		populator.populate(customerModel, yFormMappingsData);
		Assert.assertEquals("customer@customer.com", yFormMappingsData.getEmail());
	}

	private void mockDataForPopulateMinorUser(final CustomerModel customerModel) throws RelationshipDoesNotExistException
	{
		final TitleModel titleModel = new TitleModel();
		titleModel.setCode("TestTitle");

		Mockito.when(customerModel.getContactEmail()).thenReturn("customer@customer.com");
		Mockito.when(customerModel.getName()).thenReturn("Test User");
		Mockito.when(customerModel.getTitle()).thenReturn(titleModel);
		Mockito.when(customerModel.getUid()).thenReturn("customer@customer.com");

		final UserModel currentUser = Mockito.mock(UserModel.class);
		Mockito.when(currentUser.getUid()).thenReturn("currentuser@user.com");
		Mockito.when(userService.getCurrentUser()).thenReturn(currentUser);
		Mockito.when(psRelationshipFacade.getActiveRelation(Mockito.any(), Mockito.any()))
				.thenReturn(Mockito.mock(PSRelationshipData.class));
		Mockito.when(Boolean.valueOf(psPermissionFacade.isPermitted(Mockito.any(), Mockito.any(), Mockito.anyString())))
				.thenReturn(Boolean.TRUE);
	}

	private AddressData getTestAddressData()
	{
		final AddressData addressData = new AddressData();
		final CountryData countryData = new CountryData();

		countryData.setIsocode("TestCountry");
		addressData.setLine1("Test Address line 1");
		addressData.setLine2("Test Address line 2");
		addressData.setCountry(countryData);
		addressData.setPostalCode("TestPostalCode");
		addressData.setTown("TestTown");
		addressData.setPhone("TestPhone");

		return addressData;
	}
}
