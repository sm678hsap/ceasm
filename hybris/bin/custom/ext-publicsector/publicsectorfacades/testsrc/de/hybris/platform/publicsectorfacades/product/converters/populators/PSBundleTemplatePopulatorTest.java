package de.hybris.platform.publicsectorfacades.product.converters.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * PSBundleTemplatePopulatorTest unit test
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSBundleTemplatePopulatorTest
{
	private static final String ID = "id";

	private static final String HEADER_MESSAGE = "headerMessage";

	private static final String CHECKOUT_STEP_LABEL = "checkoutStep";

	@InjectMocks
	private PSBundleTemplatePopulator psBundleTemplatePopulator;

	@Mock
	private Converter<ProductModel, ProductData> psBasicServiceConverter;

	@Mock
	private BundleTemplateModel bundleTemplateModel;

	@Mock
	private BundleTemplateData bundleTemplateData;

	@Before
	public void setUp()
	{
		psBundleTemplatePopulator = new PSBundleTemplatePopulator();
		psBundleTemplatePopulator.setPsBasicServiceConverter(psBasicServiceConverter);
	}

	@Test
	public void testPopulate()
	{
		given(bundleTemplateModel.getId()).willReturn(ID);
		given(bundleTemplateModel.getHeaderMessage()).willReturn(HEADER_MESSAGE);
		given(bundleTemplateModel.getCheckoutStepLabel()).willReturn(CHECKOUT_STEP_LABEL);
		psBundleTemplatePopulator.populateProducts(bundleTemplateModel, bundleTemplateData);
	}

}
