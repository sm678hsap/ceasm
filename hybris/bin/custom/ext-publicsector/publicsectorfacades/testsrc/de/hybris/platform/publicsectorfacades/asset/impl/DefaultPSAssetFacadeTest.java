/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.asset.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;
import de.hybris.platform.commercefacades.asset.data.PSAssetData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.asset.PSAssetService;
import de.hybris.platform.publicsectorservices.asset.PSAssetTemplateService;
import de.hybris.platform.publicsectorservices.asset.exception.PSAssetNotFoundException;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * DefaultPSCartFacadeTest unit test class
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSAssetFacadeTest
{

	@InjectMocks
	private final DefaultPSAssetFacade psAssetFacade = new DefaultPSAssetFacade();

	@Mock
	private PSAssetService psAssetService;

	@Mock
	private UserService userService;

	@Mock
	private UserModel user;

	@Mock
	private Converter<PSAssetModel, PSAssetData> assetConverter;

	@Mock
	private Converter<PSAssetAttributeModel, PSAssetAttributeData> assetAttributeConverter;

	@Mock
	private Converter<PSAssetAttributeMappingModel, PSAssetAttributeMappingData> assetAttributeMappingConverter;

	@Mock
	private PSAssetTemplateService assetTemplateService;

	@Before
	public void setUp()
	{
		psAssetFacade.setAssetConverter(assetConverter);
		psAssetFacade.setUserService(userService);
		psAssetFacade.setAssetAttributeConverter(assetAttributeConverter);
		psAssetFacade.setAssetAttributeMappingConverter(assetAttributeMappingConverter);
		psAssetFacade.setAssetTemplateService(assetTemplateService);
	}

	@Test
	public void testGetAssetsForUser()
	{
		final String userId = "TestUser";
		final UserModel testUser = new UserModel();
		user.setUid("TestUser");

		final List<PSAssetModel> assetModels = new ArrayList<>();
		assetModels.add(Mockito.mock(PSAssetModel.class));

		final List<PSAssetData> assetsData = new ArrayList<>();
		assetsData.add(Mockito.mock(PSAssetData.class));

		Mockito.when(userService.getUserForUID(userId)).thenReturn(testUser);
		Mockito.when(psAssetService.getAssetsForUser(testUser)).thenReturn(assetModels);

		Mockito.when(assetConverter.convertAll(Mockito.anyList())).thenReturn(assetsData);

		psAssetFacade.getAssetsForUser(userId);
	}

	@Test
	public void testGetAssetDetailsByCode() throws PSAssetNotFoundException
	{
		final String assetCode = "TestAsset";
		final PSAssetModel assetModel = new PSAssetModel();
		assetModel.setCode(assetCode);

		final PSAssetData assetData = new PSAssetData();
		assetData.setCode(assetCode);

		Mockito.when(psAssetService.getAssetByCode(assetCode)).thenReturn(assetModel);
		Mockito.when(assetConverter.convert(assetModel)).thenReturn(assetData);

		final PSAssetData returnAssetData = psAssetFacade.getAssetDetailsByCode(assetCode);

		Assert.assertEquals(assetCode, returnAssetData.getCode());

	}


	@Test
	public void testGetAssetAttributeDetails() throws PSAssetNotFoundException
	{
		final String assetCode = "TestAsset";
		final String assetType = "Car";
		final String assetName = "Manza";
		final String returnTemplateName = "TestTemplate";

		final PSAssetAttributeModel assetAttributeModel = new PSAssetAttributeModel();
		assetAttributeModel.setAssetCode(assetCode);

		final List<PSAssetAttributeModel> assetAttributes = new ArrayList<>();
		assetAttributes.add(assetAttributeModel);


		final PSAssetAttributeData assetAttributesDataObj = new PSAssetAttributeData();
		assetAttributesDataObj.setAssetCode(assetCode);
		assetAttributesDataObj.setName(assetName);


		final List<PSAssetAttributeData> assetAttributesData = new ArrayList<>();
		assetAttributesData.add(assetAttributesDataObj);


		Mockito.when(psAssetService.getAssetAttributesByCode(assetCode)).thenReturn(assetAttributes);
		Mockito.when(assetAttributeConverter.convertAll(assetAttributes)).thenReturn(assetAttributesData);


		final List<String> assetAttributeNames = new ArrayList<>();
		assetAttributeNames.add(assetName);

		//Mockito.when(psAssetFacade.getAssetAttributeNames(assetAttributesData)).thenReturn(assetAttributeNames);


		final PSAssetAttributeMappingModel assetAttributeMappingModel = new PSAssetAttributeMappingModel();

		final List<PSAssetAttributeMappingModel> assetAttributeMappings = new ArrayList<>();
		assetAttributeMappings.add(assetAttributeMappingModel);

		final List<PSAssetAttributeMappingData> assetAttributeMappingsData = new ArrayList<>();

		Mockito.when(psAssetService.getAssetAttributesMapping(assetType, assetAttributeNames)).thenReturn(assetAttributeMappings);

		Mockito.when(assetAttributeMappingConverter.convertAll(assetAttributeMappings)).thenReturn(assetAttributeMappingsData);


		Mockito.when(
				assetTemplateService.generateAssetAttributesTemplate(assetType, assetAttributesData, assetAttributeMappingsData))
				.thenReturn(returnTemplateName);


		final String assetAttributesTemplate = psAssetFacade.getAssetAttributeDetails(assetCode, assetType);
		Assert.assertEquals(returnTemplateName, assetAttributesTemplate);

	}

}
