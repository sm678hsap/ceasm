/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.product.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.commercefacades.converter.impl.DefaultConfigurablePopulator;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.converters.populator.ProductCategoriesPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductClassificationPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductDescriptionPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductFeatureListPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPromotionsPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductReviewsPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductStockPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductSummaryPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.product.CommerceProductService;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.customerreview.CustomerReviewService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.product.PSProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of DefaultPSProductFacadeUnitTest.
 */

@UnitTest
public class DefaultPSProductFacadeUnitTest
{

	private static final String TEST_LANG_CODE = "en";
	private static final String TEST_PRODUCT_CODE = "1234";
	@InjectMocks
	private final ProductSummaryPopulator productSummaryPopulator = new ProductSummaryPopulator();
	@InjectMocks
	private final ProductDescriptionPopulator productDescriptionPopulator = new ProductDescriptionPopulator();
	@InjectMocks
	private final ProductGalleryImagesPopulator productGalleryImagesPopulator = new ProductGalleryImagesPopulator();
	@InjectMocks
	private final ProductStockPopulator productStockPopulator = new ProductStockPopulator();
	@InjectMocks
	private final ProductCategoriesPopulator productCategoriesPopulator = new ProductCategoriesPopulator();
	@InjectMocks
	private final ProductPromotionsPopulator productPromotionsPopulator = new ProductPromotionsPopulator();
	@InjectMocks
	private final ProductReviewsPopulator productReviewsPopulator = new ProductReviewsPopulator();
	@InjectMocks
	private final ProductClassificationPopulator productClassificationPopulator = new ProductClassificationPopulator();
	@InjectMocks
	private final DefaultPriceDataFactory priceDataFactory = new DefaultPriceDataFactory();
	@InjectMocks
	private final ProductPricePopulator productPricePopulator = new ProductPricePopulator();
	@InjectMocks
	private DefaultPSProductFacade psProductFacade;
	@Mock
	private PromotionsService promotionsService;
	@Mock
	private ClassificationService classificationService;
	@Mock
	private CustomerReviewService customerReviewService;
	@Mock
	private UserService userService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private ModelService modelService;
	@Mock
	private CommercePriceService commercePriceService;
	@Mock
	private CommerceProductService commerceProductService;
	@Mock
	private CommerceStockService commerceStockService;
	@Mock
	private BaseStoreService baseStoreService;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private CommerceCommonI18NService commerceCommonI18NService;
	@Mock
	private TimeService timeService;
	@Mock
	private ProductReferenceData productReferenceData;
	@Mock
	private ProductService productService;
	@Mock
	private ProductModel productModel;
	@Mock
	private PSProductService psProductService;

	@Before
	public void setup()
	{

		MockitoAnnotations.initMocks(this);
		psProductFacade = new DefaultPSProductFacade();

		productPricePopulator.setPriceDataFactory(priceDataFactory);

		given(timeService.getCurrentTime()).willReturn(new Date());

		psProductFacade.setProductService(productService);

		final DefaultConfigurablePopulator<ProductModel, ProductData, ProductOption> defaultConfigurablePopulator = new DefaultConfigurablePopulator<ProductModel, ProductData, ProductOption>();
		final LinkedHashMap<ProductOption, Populator<ProductModel, ProductData>> populators = new LinkedHashMap<ProductOption, Populator<ProductModel, ProductData>>();
		defaultConfigurablePopulator.setPopulators(populators);

		populators.put(ProductOption.PRICE, productPricePopulator);
		populators.put(ProductOption.GALLERY, productGalleryImagesPopulator);
		populators.put(ProductOption.SUMMARY, productSummaryPopulator);
		populators.put(ProductOption.DESCRIPTION, productDescriptionPopulator);
		populators.put(ProductOption.CATEGORIES, productCategoriesPopulator);
		populators.put(ProductOption.PROMOTIONS, productPromotionsPopulator);
		populators.put(ProductOption.STOCK, productStockPopulator);
		populators.put(ProductOption.REVIEW, productReviewsPopulator);
		populators.put(ProductOption.CLASSIFICATION, productClassificationPopulator);

		final AbstractPopulatingConverter<ProductModel, ProductData> productConverter = mock(AbstractPopulatingConverter.class);

		final ProductFeatureListPopulator productFeatureListPopulator = mock(ProductFeatureListPopulator.class);

		final LanguageModel enModel = mock(LanguageModel.class);

		productClassificationPopulator.setProductFeatureListPopulator(productFeatureListPopulator);

		given(enModel.getIsocode()).willReturn(TEST_LANG_CODE);
		given(commonI18NService.getCurrentLanguage()).willReturn(enModel);
		given(productConverter.convert(productModel)).willReturn(new ProductData());
		given(productService.getProductForCode(TEST_PRODUCT_CODE)).willReturn(productModel);
		given(productModel.getCode()).willReturn(TEST_PRODUCT_CODE);
	}

	@Test
	public void testUserTypeAssignedToServiceProduct()
	{

		final String productCode = "productCode";

		final PSServiceProductModel serviceProduct = new PSServiceProductModel();
		serviceProduct.setCode(productCode);
		serviceProduct.setCustomerTypes(Arrays.asList(CustomerType.UNIDENTIFIED, CustomerType.GUEST, CustomerType.REGISTERED));

		Mockito.when(productService.getProductForCode(productCode)).thenReturn(serviceProduct);

		psProductFacade.setProductService(productService);

		Assert.assertTrue(psProductFacade.isUnidentifiedUserInService(serviceProduct.getCode()));
		Assert.assertTrue(psProductFacade.isGuestUserInService(serviceProduct.getCode()));
		Assert.assertTrue(psProductFacade.isRegisteredUserInService(serviceProduct.getCode()));
		Assert.assertTrue(psProductFacade.isServiceProduct(serviceProduct.getCode()));

		final Converter<ProductModel, ProductData> productConverter = mock(Converter.class);


		Mockito.when(psProductService.getServiceProductFromSessionCart()).thenReturn(serviceProduct);
		Mockito.when(productConverter.convert(serviceProduct)).thenReturn(new ProductData());
		Mockito.when(psProductService.getServiceProductForOrderCode(productCode)).thenReturn(serviceProduct);

		psProductFacade.setPsProductService(psProductService);
		psProductFacade.setProductConverter(productConverter);

		Assert.assertNotNull(psProductFacade.getServiceProductFromSessionCart());
		Assert.assertNotNull(psProductFacade.getServiceProductForOrderCode(serviceProduct.getCode()));

		Assert.assertTrue(
				psProductFacade.isServiceApplicableForGiveCustomerType(serviceProduct, CustomerType.UNIDENTIFIED.toString()));
		Assert.assertTrue(psProductFacade.isServiceApplicableForGiveCustomerType(serviceProduct, CustomerType.GUEST.toString()));
		Assert.assertTrue(
				psProductFacade.isServiceApplicableForGiveCustomerType(serviceProduct, CustomerType.REGISTERED.toString()));
	}
}
