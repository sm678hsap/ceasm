/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.customer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.dashboard.PSAccountDashboardService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of DefaultPublicSectorCustomerFacadeUnitTest.
 */
@UnitTest
public class DefaultPSCustomerFacadeUnitTest
{

	@InjectMocks
	private DefaultPSCustomerFacade publicSectorCustomerFacade;

	@Mock
	private CustomerFacade customerFacade;
	@Mock
	private CustomerAccountService customerAccountService;
	@Mock
	private ModelService modelService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private Converter<UserModel, CustomerData> customerConverter;

	private CurrencyData defaultCurrencyData;

	private LanguageData defaultLanguageData;

	@Mock
	private PSAccountDashboardService psAccountDashboardService;

	@Mock
	private UserService userService;

	@Mock
	private Converter<PSBillPaymentModel, PSBillPaymentData> psBillConverter;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		publicSectorCustomerFacade = new DefaultPSCustomerFacade();
		publicSectorCustomerFacade.setCustomerFacade(customerFacade);
		publicSectorCustomerFacade.setModelService(modelService);
		publicSectorCustomerFacade.setCustomerAccountService(customerAccountService);
		publicSectorCustomerFacade.setCustomerConverter(customerConverter);

		publicSectorCustomerFacade.setCommonI18NService(commonI18NService);
		publicSectorCustomerFacade.setPsAccountDashboardService(psAccountDashboardService);
		publicSectorCustomerFacade.setUserService(userService);
		publicSectorCustomerFacade.setPsBillConverter(psBillConverter);

		defaultCurrencyData = new CurrencyData();
		defaultCurrencyData.setIsocode("GBP");

		defaultLanguageData = new LanguageData();
		defaultLanguageData.setIsocode("en");
	}


	@Test
	public void testRegisterUnidentifiedUser() throws DuplicateUidException
	{
		final CustomerData unidentifiedCustomerData = new CustomerData();
		unidentifiedCustomerData.setCurrency(defaultCurrencyData);
		unidentifiedCustomerData.setLanguage(defaultLanguageData);
		final CustomerModel unidentifiedCustomer = new CustomerModel();
		Mockito.when(modelService.create(CustomerModel.class)).thenReturn(unidentifiedCustomer);
		Mockito.when(customerConverter.convert(unidentifiedCustomer)).thenReturn(unidentifiedCustomerData);
		publicSectorCustomerFacade.createUnidentifiedUserForAnonymousCheckout("Unidentified");

		Assert.assertEquals(CustomerType.UNIDENTIFIED, unidentifiedCustomer.getType());
	}


	@Test
	public void testGetBillsForUserRelationshipsByStatus()
	{
		final String userId = "TestUser";
		final UserModel testUser = new UserModel();
		testUser.setUid("TestUser");

		final List<BillPaymentStatus> statuses = new ArrayList<>();
		statuses.add(BillPaymentStatus.PAID);

		final List<PSBillPaymentModel> billModels = new ArrayList<>();
		billModels.add(Mockito.mock(PSBillPaymentModel.class));

		final List<PSBillPaymentData> billsData = new ArrayList<>();
		billsData.add(Mockito.mock(PSBillPaymentData.class));

		Mockito.when(userService.getUserForUID(userId)).thenReturn(testUser);
		Mockito.when(psAccountDashboardService.getBillsForUserRelationshipsByStatus(testUser, statuses)).thenReturn(billModels);

		Mockito.when(psBillConverter.convertAll(Mockito.anyList())).thenReturn(billsData);

		publicSectorCustomerFacade.getBillsForUserRelationshipsByStatus(userId, statuses);
	}


}
