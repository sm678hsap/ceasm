/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.strategies.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.publicsectorfacades.product.PSProductFacade;
import de.hybris.platform.xyformscommercefacades.strategies.YFormsStrategy;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;



/**
 * Test suite for {@link DefaultPSYFormsStrategy}
 *
 */
@UnitTest
public class DefaultPSYFormsStrategyUnitTest
{
	private static final String CART = "cart";
	private static final String PRODUCT_CODE1 = "productCode1";
	private static final String PRODUCT_CODE2 = "productCode2";

	@InjectMocks
	private DefaultPSYFormsStrategy psYFormsStrategy;

	@Mock
	private YFormsStrategy yFormsStrategy;

	@Mock
	private PSProductFacade psProductFacade;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		psYFormsStrategy = new DefaultPSYFormsStrategy();
		psYFormsStrategy.setyFormsStrategy(yFormsStrategy);
		psYFormsStrategy.setPsProductFacade(psProductFacade);
	}

	@Test
	public void testGetYFormOrderEntry()
	{
		final OrderEntryData orderEntryData1 = getOrderEntryData(1, 1, PRODUCT_CODE1);
		final OrderEntryData orderEntryData2 = getOrderEntryData(1, 1, PRODUCT_CODE2);

		final CartData cartData = new CartData();
		cartData.setCode(CART);
		cartData.setEntries(Arrays.asList(orderEntryData1, orderEntryData2));

		// considering both order entries contains product which has YForms assigned. Only one of them contains the ServiceProduct
		given(psYFormsStrategy.getyFormsStrategy().getYFormOrderEntries(cartData))
				.willReturn(Arrays.asList(orderEntryData1, orderEntryData2));
		given(Boolean.valueOf(psYFormsStrategy.getPsProductFacade().isServiceProduct(PRODUCT_CODE1))).willReturn(Boolean.TRUE);
		given(Boolean.valueOf(psYFormsStrategy.getPsProductFacade().isServiceProduct(PRODUCT_CODE2))).willReturn(Boolean.FALSE);

		final OrderEntryData yFormOrderEntryData = psYFormsStrategy.getYFormOrderEntry(cartData);

		Assert.assertNotNull(yFormOrderEntryData);
		Assert.assertEquals(orderEntryData1, yFormOrderEntryData);
	}

	@Test
	public void testGetYFormOrderEntryForEmptyCart()
	{
		final CartData cartData = new CartData();
		cartData.setCode(CART);

		final OrderEntryData yFormOrderEntryData = psYFormsStrategy.getYFormOrderEntry(cartData);

		Assert.assertNull(yFormOrderEntryData);
	}

	protected OrderEntryData getOrderEntryData(final int entryNumber, final long qty, final String productCode)
	{
		final OrderEntryData orderEntryData = new OrderEntryData();
		orderEntryData.setEntryNumber(Integer.valueOf(entryNumber));
		orderEntryData.setQuantity(Long.valueOf(qty));
		orderEntryData.setProduct(new ProductData());
		orderEntryData.getProduct().setCode(productCode);

		return orderEntryData;
	}

}
