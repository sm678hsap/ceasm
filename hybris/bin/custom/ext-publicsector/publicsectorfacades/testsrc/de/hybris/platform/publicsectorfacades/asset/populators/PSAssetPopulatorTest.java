/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.asset.populators;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.asset.data.PSAssetData;
import de.hybris.platform.commercefacades.asset.data.PSAssetTypeData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;
import de.hybris.platform.publicsectorservices.model.PSAssetTypeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test class for PSAssetPopulator
 *
 */
@UnitTest
public class PSAssetPopulatorTest
{
	private static final String ASSET_CODE = "1";
	private static final String ASSET_ID = "1";
	private static final String ASSET_NAME = "2018 Subaru WRX Carbon Black";

	private AbstractPopulatingConverter<PSAssetModel, PSAssetData> assetConverter;
	private final PSAssetPopulator assetPopulator = new PSAssetPopulator();

	@Mock
	private Converter<PSAssetTypeModel, PSAssetTypeData> assetTypeConverter;

	@Mock
	private PSAssetModel assetModel;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		assetPopulator.setAssetTypeConverter(assetTypeConverter);

		assetConverter = new AbstractPopulatingConverter<PSAssetModel, PSAssetData>();
		assetConverter.setPopulators(Arrays.asList(assetPopulator));
	}

	@Test
	public void test()
	{
		given(assetModel.getCode()).willReturn(ASSET_CODE);
		given(assetModel.getAssetId()).willReturn(ASSET_ID);
		given(assetModel.getName()).willReturn(ASSET_NAME);

		final PSAssetTypeModel assetTypeModel = mock(PSAssetTypeModel.class);
		final PSAssetTypeData assetTypeData = mock(PSAssetTypeData.class);
		given(assetTypeConverter.convert(assetTypeModel)).willReturn(assetTypeData);

		final PSAssetData assetData = new PSAssetData();
		assetConverter.populate(assetModel, assetData);
		Assert.assertEquals(ASSET_CODE, assetData.getCode());
		Assert.assertEquals(ASSET_ID, assetData.getAssetId());
		Assert.assertEquals(ASSET_NAME, assetData.getAssetName());
	}

}
