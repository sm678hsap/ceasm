/**
 *
 */
package de.hybris.platform.publicsectorfacades.docmanagement.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.publicsectordocmanagement.data.PSDocumentData;
import de.hybris.platform.publicsectordocmanagement.data.PSDocumentTagData;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentTagModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentThumbnailModel;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;
import de.hybris.platform.publicsectorservices.helper.PSFilesHelper;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test suite for {@link PSDocumentPopulator}
 *
 */

@UnitTest
public class PSDocumentPopulatorTest

{
	private static final String DOC_NAME = "DOC";
	private static final String DOC_ID = "DOC1";

	private static final String FILE_NAME = "FILE_NAME";
	private static final String FILE_PATH = "FILE_PATH";

	private static String URL = "http://www.test.com/c";

	private static String URL_DOC = "http://www.test.com/doc";

	private static String URL_DOC_RETURN = "http://www.test.com/doc/return";

	private static final String MIME_TYPE = "Mime_type";

	private static final String UID = "12345";

	@InjectMocks
	private PSDocumentPopulator psDocumentPopulator;

	@Mock
	private Converter<PSDocumentTagModel, PSDocumentTagData> documentTagConverter;

	@Mock
	private PSDocumentTagData psDocumentTagData;


	@Mock
	private PSDocumentThumbnailModel psDocumentThumbnailModel;


	@Mock
	private PSFilesHelper psFilesHelper;
	@Mock
	private PSDocumentManagementService psDocumentManagementService;
	@Mock
	private PSDocumentModel source;
	@Mock
	private PSDocumentData psDocumentData;

	@Mock
	private CustomerModel customerModel;


	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		psDocumentPopulator.setDocumentTagConverter(documentTagConverter);
		psDocumentPopulator.setPsDocumentManagementService(psDocumentManagementService);
		psDocumentPopulator.setPsFilesHelper(psFilesHelper);

	}

	@Test
	public void test() throws UnsupportedEncodingException
	{
		given(source.getDocId()).willReturn(DOC_ID);
		given(source.getDocName()).willReturn(DOC_NAME);
		given(source.getExpiresOn()).willReturn(new Date());
		given(source.getIssuedOn()).willReturn(new Date());
		given(source.getFileName()).willReturn(FILE_NAME);
		given(source.getFilePath()).willReturn(FILE_PATH);
		final PSDocumentTagModel psDocument = new PSDocumentTagModel();
		final Collection<PSDocumentTagModel> psDocumentTagModel = new ArrayList();
		psDocumentTagModel.add(psDocument);
		given(source.getPsDocumentTag()).willReturn(psDocumentTagModel);
		given(source.getDocumentThumbnail()).willReturn(psDocumentThumbnailModel);
		given(psDocumentThumbnailModel.getMimeType()).willReturn(MIME_TYPE);
		given(source.getSecurePathURLId()).willReturn(URL);
		given(psDocumentManagementService.getDownloadUrlForDocument(URL_DOC)).willReturn(URL_DOC_RETURN);
		given(customerModel.getUid()).willReturn(UID);
		given(source.getCustomer()).willReturn(customerModel);
		given(source.getSecurePathURLId()).willReturn(URL);
		given(source.getMimeType()).willReturn(MIME_TYPE);
		psDocumentPopulator.populate(source, psDocumentData);

	}

}