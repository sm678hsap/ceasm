/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceSaveCartService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.order.PSCartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSSaveCartFacadeUnitTest
{
	private UserModel userModel;
	private BaseSiteModel baseSiteModel;

	private DefaultPSSaveCartFacade saveCartFacade;
	private CommerceSaveCartService saveCartService;
	private BaseSiteService baseSiteService;
	private UserService userService;

	private PSCartService psCartService;

	@Mock
	private Converter<CartModel, CartData> cartConverter;

	@Before
	public void setUp()
	{
		userModel = mock(UserModel.class);
		baseSiteModel = mock(BaseSiteModel.class);

		saveCartFacade = new DefaultPSSaveCartFacade();

		baseSiteService = mock(BaseSiteService.class);
		saveCartFacade.setBaseSiteService(baseSiteService);

		userService = mock(UserService.class);
		saveCartFacade.setUserService(userService);

		saveCartService = mock(CommerceSaveCartService.class);
		saveCartFacade.setCommerceSaveCartService(saveCartService);

		psCartService = mock(PSCartService.class);
		saveCartFacade.setPsCartService(psCartService);

		saveCartFacade.setCartConverter(cartConverter);
	}

	@Test
	public void testGetSavedCartsForGivenUser()
	{
		final String customerId = "TestUser";
		final UserModel user = new UserModel();
		user.setUid(customerId);

		final List<OrderStatus> orderStatus = new ArrayList<>();
		orderStatus.add(OrderStatus.CREATED);

		final PageableData pageableData = new PageableData();

		final List<CartModel> carts = new ArrayList<>();
		final CartModel cart1 = new CartModel();
		final CartModel cart2 = new CartModel();
		final CartModel cart3 = new CartModel();

		carts.add(cart1);
		carts.add(cart2);
		carts.add(cart3);

		final SearchPageData<CartModel> savedCartModels = new SearchPageData<>();
		savedCartModels.setResults(carts);

		when(userService.getUserForUID(customerId)).thenReturn(user);
		when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

		when(psCartService.getSavedDraftsForSiteAndUser(pageableData, baseSiteModel, user, orderStatus))
				.thenReturn(savedCartModels);

		saveCartFacade.getSavedCartsForGivenUser(pageableData, orderStatus, customerId);

	}
}
