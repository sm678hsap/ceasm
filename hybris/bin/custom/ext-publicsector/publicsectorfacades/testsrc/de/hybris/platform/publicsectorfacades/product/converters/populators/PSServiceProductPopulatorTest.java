/**
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.service.data.PSApplyByEmailData;
import de.hybris.platform.commercefacades.service.data.PSApplyByPhoneData;
import de.hybris.platform.commercefacades.service.data.PSApplyOnlineData;
import de.hybris.platform.commercefacades.service.data.PSConsentDeclarationData;
import de.hybris.platform.commercefacades.service.data.PSKeyFactsData;
import de.hybris.platform.commercefacades.service.data.PSRelatedLinksData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.publicsectorservices.model.PSApplyByEmailModel;
import de.hybris.platform.publicsectorservices.model.PSApplyByPhoneModel;
import de.hybris.platform.publicsectorservices.model.PSApplyOnlineModel;
import de.hybris.platform.publicsectorservices.model.PSConsentDeclarationModel;
import de.hybris.platform.publicsectorservices.model.PSKeyFactsModel;
import de.hybris.platform.publicsectorservices.model.PSRelatedLinksModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * PSServiceProductPopulatorTest unit test class
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSServiceProductPopulatorTest
{
	private static final String CART_LABEL_TEXT = "cartLabelText";
	@InjectMocks
	private PSServiceProductPopulator psServiceProductPopulator;
	@Mock
	private Converter<PSApplyOnlineModel, PSApplyOnlineData> applyOnlineCMSConverter;

	@Mock
	private Converter<PSApplyByPhoneModel, PSApplyByPhoneData> applyByPhoneCMSConverter;

	@Mock
	private Converter<PSApplyByEmailModel, PSApplyByEmailData> applyByEmailCMSConverter;

	@Mock
	private Converter<PSRelatedLinksModel, PSRelatedLinksData> relatedLinksCMSConverter;

	@Mock
	private Converter<PSKeyFactsModel, PSKeyFactsData> keyFactsCMSConverter;

	@Mock
	private Converter<PSConsentDeclarationModel, PSConsentDeclarationData> consentDeclarationConverter;

	@Mock
	private PSServiceProductModel psServiceProductModel;

	@Mock
	private PSApplyOnlineModel psApplyOnlineModel;

	@Mock
	private PSApplyByEmailModel psApplyByEmailModel;

	@Mock
	private PSApplyByPhoneModel psApplyByPhoneModel;

	@Mock
	private PSRelatedLinksModel psRelatedLinksModel;

	@Mock
	private PSKeyFactsModel psKeyFactsModel;

	@Mock
	private PSConsentDeclarationModel psConsentDeclarationModel;

	@Mock
	private CustomerType customerType;

	@Mock
	private PSApplyOnlineData psApplyOnlineData;

	@Mock
	private PSApplyByEmailData psApplyByEmailData;

	@Mock
	private PSApplyByPhoneData psApplyByPhoneData;

	@Mock
	private PSRelatedLinksData psRelatedLinksData;

	@Mock
	private PSKeyFactsData psKeyFactsData;

	@Mock
	private PSConsentDeclarationData psConsentDeclarationData;




	@Before
	public void setUp()
	{
		psServiceProductPopulator = new PSServiceProductPopulator();
		psServiceProductPopulator.setApplyOnlineCMSConverter(applyOnlineCMSConverter);
		psServiceProductPopulator.setApplyByPhoneCMSConverter(applyByPhoneCMSConverter);
		psServiceProductPopulator.setApplyByEmailCMSConverter(applyByEmailCMSConverter);
		psServiceProductPopulator.setRelatedLinksCMSConverter(relatedLinksCMSConverter);
		psServiceProductPopulator.setKeyFactsCMSConverter(keyFactsCMSConverter);
		psServiceProductPopulator.setConsentDeclarationConverter(consentDeclarationConverter);
	}

	@Test
	public void test()
	{
		final Collection<PSApplyOnlineModel> psApplyOnline = new ArrayList<>();
		psApplyOnline.add(psApplyOnlineModel);
		given(psServiceProductModel.getApplyOnline()).willReturn(psApplyOnline);
		final List<PSApplyOnlineData> psApplyDta = new ArrayList<>();
		psApplyDta.add(psApplyOnlineData);
		given(applyOnlineCMSConverter.convertAll(psApplyOnline)).willReturn(psApplyDta);
		given(psServiceProductModel.getApplyByEmail()).willReturn(psApplyByEmailModel);
		given(applyByEmailCMSConverter.convert(psApplyByEmailModel)).willReturn(psApplyByEmailData);
		given(psServiceProductModel.getApplyByPhone()).willReturn(psApplyByPhoneModel);
		given(applyByPhoneCMSConverter.convert(psApplyByPhoneModel)).willReturn(psApplyByPhoneData);
		given(psServiceProductModel.getRelatedLinks()).willReturn(psRelatedLinksModel);
		given(relatedLinksCMSConverter.convert(psRelatedLinksModel)).willReturn(psRelatedLinksData);
		given(psServiceProductModel.getKeyFacts()).willReturn(psKeyFactsModel);
		given(keyFactsCMSConverter.convert(psKeyFactsModel)).willReturn(psKeyFactsData);
		final Collection<PSConsentDeclarationModel> psConsentDeclarationModels = new ArrayList<>();
		psConsentDeclarationModels.add(psConsentDeclarationModel);
		given(psServiceProductModel.getConsentDeclaration()).willReturn(psConsentDeclarationModels);
		final List<PSConsentDeclarationData> psConsent = new ArrayList<>();
		psConsent.add(psConsentDeclarationData);
		given(consentDeclarationConverter.convertAll(psConsentDeclarationModels)).willReturn(psConsent);
		given(psServiceProductModel.getAddToCartLabelText()).willReturn(CART_LABEL_TEXT);
		final Collection<CustomerType> customer = new ArrayList<>();
		customer.add(customerType);
		given(psServiceProductModel.getCustomerTypes()).willReturn(customer);
	}
}
