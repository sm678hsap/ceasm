/**
 *
 */
package de.hybris.platform.publicsectorfacades.order.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorfacades.order.PSOrderFacade;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Default implementation of {@link PSOrderFacade}.
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSOrderFacadeTest
{

	private static final String USER_ID = "userId";

	private static final String CUSTOMER_ID = "customerId";

	@InjectMocks
	private DefaultPSOrderFacade defaultPSOrderFacade;

	@Mock
	private UserService userService;

	@Mock
	private CustomerAccountService customerAccountService;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private Converter<OrderModel, OrderHistoryData> orderHistoryConverter;

	@Mock
	private UserModel userModel;

	@Mock
	private BaseStoreModel baseStoreModel;

	@Mock
	private CustomerModel customerModel;

	@Mock
	private OrderStatus orderStatus;

	@Mock
	private OrderModel orderModel;

	@Mock
	private Converter<OrderModel, OrderHistoryData> converter;

	@Mock
	private PageableData pageData;

	@Before
	public void setUp()
	{
		defaultPSOrderFacade = new DefaultPSOrderFacade();
		defaultPSOrderFacade.setCustomerAccountService(customerAccountService);
		defaultPSOrderFacade.setBaseStoreService(baseStoreService);
		defaultPSOrderFacade.setUserService(userService);
		defaultPSOrderFacade.setOrderHistoryConverter(converter);
	}

	@Test
	public void testGetOrderHistoryForStatuses()
	{
		given(userService.getUserForUID(USER_ID)).willReturn(customerModel);
		given(baseStoreService.getCurrentBaseStore()).willReturn(baseStoreModel);
		final OrderStatus[] status = new OrderStatus[]
		{ OrderStatus.CANCELLED };
		final List<OrderModel> order = new ArrayList<>();
		order.add(orderModel);
		given(customerAccountService.getOrderList(customerModel, baseStoreModel, status)).willReturn(order);
		defaultPSOrderFacade.getOrderHistoryForStatuses(USER_ID, status);
	}

	@Test
	public void testGetPagedOrderHistoryForStatuses()
	{
		given(userService.getUserForUID(CUSTOMER_ID)).willReturn(customerModel);
		given(baseStoreService.getCurrentBaseStore()).willReturn(baseStoreModel);
		final OrderStatus[] status = new OrderStatus[]
		{ OrderStatus.CANCELLED };
		final SearchPageData<OrderModel> searchpage = new SearchPageData<>();
		given(customerAccountService.getOrderList(customerModel, baseStoreModel, status, pageData)).willReturn(searchpage);
		defaultPSOrderFacade.getPagedOrderHistoryForStatuses(CUSTOMER_ID, pageData, status);
	}

}
