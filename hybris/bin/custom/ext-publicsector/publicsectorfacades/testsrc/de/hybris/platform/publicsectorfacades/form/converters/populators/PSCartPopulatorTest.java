/**
 *
 */
package de.hybris.platform.publicsectorfacades.form.converters.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorfacades.bundle.selection.BundleSelectionFacade;
import de.hybris.platform.publicsectorfacades.order.PSCheckoutFacade;
import de.hybris.platform.publicsectorfacades.order.converters.populators.PSCartPopulator;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * @author ndokku
 *
 */
public class PSCartPopulatorTest
{

	private static final String CODE = "1234";
	private static final String CODE1 = "234576";
	private static final String EMAIL = "test.kumar82c@gmail.com";
	private static final String ISO = "AU";
	private static final String URL = "test/au";
	private static final double EPS = 0.001;



	@InjectMocks
	private PSCartPopulator psCartPopulator;
	@Mock
	private Converter<AddressModel, AddressData> addressConverter;
	@Mock
	private PSCheckoutFacade psCheckoutFacade;
	@Mock
	private BundleSelectionFacade bundleSelectionFacade;
	@Mock
	private Converter<UserModel, CustomerData> customerConverter;
	@Mock
	private CartModel source;
	@Mock
	private CartData cartData;
	@Mock
	private UserModel userModel;
	@Mock
	private AddressModel addressModel;
	@Mock
	private DeliveryModeModel deliveryModeModel;
	@Mock
	private AbstractOrderEntryModel entry;
	@Mock
	private ProductModel productModel;
	@Mock
	private PSServiceProductModel serviceProduct;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		psCartPopulator.setAddressConverter(addressConverter);
		psCartPopulator.setBundleSelectionFacade(bundleSelectionFacade);
		psCartPopulator.setCustomerConverter(customerConverter);
		psCartPopulator.setPsCheckoutFacade(psCheckoutFacade);




	}

	@Test
	public void cartPopulatorTest() throws UnsupportedEncodingException
	{
		given(source.getReturnURL()).willReturn(URL);
		given(source.getLastAccessDate()).willReturn(new Date());
		given(source.getLockedBy()).willReturn(userModel);
		given(source.getPaymentAddress()).willReturn(addressModel);
		given(psCheckoutFacade.isServiceRequestApplicableForDelivery(source)).willReturn(true);
		given(source.getDeliveryMode()).willReturn(deliveryModeModel);
		final List<AbstractOrderEntryModel> list1 = new ArrayList();
		list1.add(entry);
		given(source.getEntries()).willReturn(list1);
		given(entry.getProduct()).willReturn(serviceProduct);
		given(entry.getBasePrice()).willReturn(EPS);
		given(serviceProduct.getCode()).willReturn(CODE);
		psCartPopulator.populate(source, cartData);


	}

}

