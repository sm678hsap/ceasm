/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package de.hybris.platform.publicsectorfacades.order.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.order.PSCartService;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;


/**
 * The class of DefaultPSCheckoutFacadeTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSCheckoutFacadeTest
{

	private static final String CART_CODE = "cartCode";
	private static final Integer ORDER_ENTRY_NUMBER = Integer.valueOf(123);

	@InjectMocks
	private final DefaultPSCheckoutFacade psCheckoutFacade = new DefaultPSCheckoutFacade();

	@Mock
	private CheckoutFacade checkoutFacade;

	@Mock
	private CartService cartService;

	@Mock
	private CartModel cartModel;

	@Mock
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

	@Mock
	private ModelService modelService;

	@Mock
	private PSCartService psCartService;

	@Mock
	private CustomerAccountService customerAccountService;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private UserService userService;

	@Mock
	private AbstractPopulatingConverter<OrderModel, OrderData> orderConverter;

	@Mock
	private CheckoutCustomerStrategy defaultCheckoutCustomerStrategy;

	@Mock
	private PSPermissionService psPermissionService;

	private OrderModel orderModel;

	private ProductModel productModel;

	private UserModel user;

	private OrderEntryData orderEntryData;

	private CartData cartData;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		psCheckoutFacade.setBaseStoreService(baseStoreService);
		psCheckoutFacade.setUserService(userService);
		psCheckoutFacade.setCheckoutCustomerStrategy(defaultCheckoutCustomerStrategy);
		psCheckoutFacade.setPsPermissionService(psPermissionService);

		psCheckoutFacade.setCustomerAccountService(customerAccountService);
		psCheckoutFacade.setOrderConverter(orderConverter);

		user = new UserModel();
		user.setUid("TestUser");

		orderModel = new OrderModel();
		orderModel.setUser(user);
		orderModel.setCode("order");
		final AbstractOrderEntryModel entryModel = new AbstractOrderEntryModel();
		final ProductModel productModel1 = new ProductModel();
		entryModel.setProduct(productModel1);
		final List<AbstractOrderEntryModel> entryModelList = new ArrayList<AbstractOrderEntryModel>();
		entryModelList.add(entryModel);
		orderModel.setEntries(entryModelList);

		productModel = Mockito.mock(ProductModel.class);
	}

	private void addMockCartEntryToCart()
	{
		final ProductData productData = new ProductData();
		final String productCode = "productCode";
		productData.setCode(productCode);

		orderEntryData = new OrderEntryData();
		orderEntryData.setProduct(productData);
		orderEntryData.setEntryNumber(ORDER_ENTRY_NUMBER);

		cartData = new CartData();
		cartData.setEntries(Lists.<OrderEntryData> newArrayList(orderEntryData));
		cartData.setCode(CART_CODE);

	}

	@Test
	public void shouldReturnFalseIfPaymentNotNeededForService()
	{
		addMockCartEntryToCart();
		orderEntryData.setBasePrice(null);
		Mockito.when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
		Assert.assertFalse(psCheckoutFacade.isPaymentNeeded());
	}


	@Test
	public void shouldReturnTrueIfServiceNeedsPayment()
	{
		addMockCartEntryToCart();

		final PriceData price = new PriceData();
		price.setValue(BigDecimal.TEN);

		cartData.setTotalPrice(price);
		// given
		Mockito.when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
		Assert.assertTrue(psCheckoutFacade.isPaymentNeeded());
	}

	@Test
	public void shouldReturnFalseIfCartHasNoEntries()
	{
		cartData = new CartData();
		cartData.setEntries(new ArrayList<OrderEntryData>());
		Mockito.when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);

		Assert.assertFalse(psCheckoutFacade.isPaymentNeeded());
	}

	@Test
	public void testGetServiceProductOrderEntryFromCartWhenSessionCartExists()
	{
		final CartEntryModel cartEntry = Mockito.mock(CartEntryModel.class);
		final PSServiceProductModel serviceProduct = Mockito.mock(PSServiceProductModel.class);
		final OrderEntryData expectedOrderEntryData = new OrderEntryData();
		Mockito.when(cartEntry.getProduct()).thenReturn(serviceProduct);
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.TRUE);
		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);
		Mockito.when(cartModel.getEntries()).thenReturn(Collections.singletonList(cartEntry));
		Mockito.when(orderEntryConverter.convert(cartEntry)).thenReturn(expectedOrderEntryData);

		final OrderEntryData actualOrderEntryData = psCheckoutFacade.getServiceProductOrderEntryFromCart();
		Assert.assertNotNull(actualOrderEntryData);
		Assert.assertEquals(expectedOrderEntryData, actualOrderEntryData);
	}

	@Test
	public void testGetServiceProductOrderEntryFromCartWhenSessionCartDoesntExists()
	{
		Mockito.when(Boolean.valueOf(cartService.hasSessionCart())).thenReturn(Boolean.FALSE);

		Assert.assertNull(psCheckoutFacade.getServiceProductOrderEntryFromCart());
	}

	@Test
	public void testRemovePaymentInfoIfNotNeededWhenSessionCartIsNotNullAndPaymentIsRequired()
	{
		cartData = Mockito.mock(CartData.class);
		final PriceData priceData = new PriceData();
		priceData.setValue(BigDecimal.valueOf(2.0));
		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);
		Mockito.when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
		Mockito.when(cartData.getTotalPrice()).thenReturn(priceData);

		psCheckoutFacade.removePaymentInfoIfNotNeeded();
		Mockito.verifyZeroInteractions(cartModel);
		Mockito.verifyZeroInteractions(modelService);
	}

	@Test
	public void testRemovePaymentInfoIfNotNeededWhenSessionCartIsNotNullAndPaymentIsNotRequired()
	{
		cartData = Mockito.mock(CartData.class);
		final PriceData priceData = new PriceData();
		priceData.setValue(BigDecimal.ZERO);
		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);
		Mockito.when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
		Mockito.when(cartData.getTotalPrice()).thenReturn(priceData);

		psCheckoutFacade.removePaymentInfoIfNotNeeded();
		Mockito.verify(cartModel).setPaymentInfo(null);
		Mockito.verify(modelService).save(cartModel);
	}

	@Test
	public void testRemovePaymentInfoIfNotNeededWhenSessionCartIsNull()
	{
		Mockito.when(cartService.getSessionCart()).thenReturn(null);

		psCheckoutFacade.removePaymentInfoIfNotNeeded();
		Mockito.verifyZeroInteractions(cartModel);
		Mockito.verifyZeroInteractions(modelService);
	}

	@Test
	public void testRemoveDeliveryInfoIfNotNeededWhenCartIsDeliverable()
	{
		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);
		final CartEntryModel cartEntry = Mockito.mock(CartEntryModel.class);
		final ProductModel product = Mockito.mock(PSServiceProductModel.class);
		Mockito.when(cartModel.getEntries()).thenReturn(Collections.singletonList(cartEntry));
		Mockito.when(cartEntry.getProduct()).thenReturn(product);
		Mockito.when(product.getDeliveryModes()).thenReturn(Collections.singleton(new ZoneDeliveryModeModel()));

		psCheckoutFacade.removeDeliveryInfoIfNotNeeded();
		Mockito.verifyZeroInteractions(modelService);
	}

	@Test
	public void testRemoveDeliveryInfoIfNotNeededWhenCartIsNotDeliverable()
	{
		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);
		final CartEntryModel cartEntry = Mockito.mock(CartEntryModel.class);
		final ProductModel product = Mockito.mock(PSServiceProductModel.class);
		Mockito.when(cartModel.getEntries()).thenReturn(Collections.singletonList(cartEntry));
		Mockito.when(cartEntry.getProduct()).thenReturn(product);
		Mockito.when(product.getDeliveryModes()).thenReturn(null);

		psCheckoutFacade.removeDeliveryInfoIfNotNeeded();
		Mockito.verify(cartModel).setDeliveryAddress(null);
		Mockito.verify(cartModel).setDeliveryMode(null);
		Mockito.verify(modelService).save(cartModel);
	}

	@Test
	public void testRemoveDeliveryInfoIfNotNeededWhenSessionCartIsNull()
	{
		Mockito.when(cartService.getSessionCart()).thenReturn(null);
		final CartEntryModel cartEntry = Mockito.mock(CartEntryModel.class);
		final ProductModel product = Mockito.mock(PSServiceProductModel.class);
		Mockito.when(cartModel.getEntries()).thenReturn(Collections.singletonList(cartEntry));
		Mockito.when(cartEntry.getProduct()).thenReturn(product);
		Mockito.when(product.getDeliveryModes()).thenReturn(null);

		psCheckoutFacade.removeDeliveryInfoIfNotNeeded();
		Mockito.verifyZeroInteractions(cartModel);
		Mockito.verifyZeroInteractions(modelService);
	}

	@Test
	public void testRecalculateCartWhenSessionCartIsNotNull()
	{
		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);

		psCheckoutFacade.recalculateCart();
		Mockito.verify(psCartService).recalculateCart(cartModel);
	}

	@Test
	public void testGetOrderDetailsForCode() throws RelationshipDoesNotExistException
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		given(customerAccountService.getOrderForCode(Mockito.anyString(), Mockito.any(BaseStoreModel.class)))
				.willReturn(orderModel);
		given(productModel.getPicture()).willReturn(null);
		psCheckoutFacade.getOrderDetailsForCode("1234");
		verify(orderConverter).convert(orderModel);

	}

	@Test(expected = UnknownIdentifierException.class)
	public void testGetOrderDetailsForCodeNoPermission() throws RelationshipDoesNotExistException
	{
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		given(customerAccountService.getOrderForCode(Mockito.anyString(), Mockito.any(BaseStoreModel.class)))
				.willReturn(orderModel);
		given(productModel.getPicture()).willReturn(null);

		final UserModel newUser = new UserModel();
		newUser.setUid("NewUser");
		orderModel.setUser(newUser);
		given(psPermissionService.isPermitted(Mockito.any(UserModel.class), Mockito.any(UserModel.class), Mockito.anyString()))
				.willReturn(false);

		psCheckoutFacade.getOrderDetailsForCode("1234");
	}

}
