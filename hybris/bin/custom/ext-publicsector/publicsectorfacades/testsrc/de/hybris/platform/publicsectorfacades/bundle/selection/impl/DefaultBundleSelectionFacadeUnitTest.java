/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.bundle.selection.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.bundle.impl.DefaultBundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.publicsectorservices.bundle.selection.BundleSelectionService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;

import java.util.ArrayList;
import java.util.List;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * The class of DefaultBundleSelectionFacadeUnitTest.
 */
@UnitTest
public class DefaultBundleSelectionFacadeUnitTest
{

	private static final String TEST_PRODUCT_CODE = "1234";

	@InjectMocks
	private DefaultBundleSelectionFacade defaultBundleSelectionFacade;

	@Mock
	private BundleSelectionService bundleSelectionService;
	@Mock
	private ProductService productService;
	@Mock
	private DefaultBundleTemplateService bundleTemplateService;
	@Mock
	private CartService cartService;
	@Mock
	private Converter<BundleTemplateModel, BundleTemplateData> psBundleTemplateConverter;


	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);

		defaultBundleSelectionFacade = new DefaultBundleSelectionFacade();
		defaultBundleSelectionFacade.setBundleSelectionService(bundleSelectionService);
		defaultBundleSelectionFacade.setProductService(productService);
		defaultBundleSelectionFacade.setBundleTemplateService(bundleTemplateService);
		defaultBundleSelectionFacade.setPsBundleTemplateConverter(psBundleTemplateConverter);

		bundleTemplateService = new DefaultBundleTemplateService();
	}

	@Test
	public void testBundleAddons()
	{
		final ProductModel product = new ProductModel();

		product.setCode(TEST_PRODUCT_CODE);
		Mockito.when(productService.getProductForCode(TEST_PRODUCT_CODE)).thenReturn(product);

		// no products assigned to template
		final BundleTemplateModel bundleTemplate = mock(BundleTemplateModel.class);
		final BundleTemplateModel parentBundleTemplate = mock(BundleTemplateModel.class);
		bundleTemplate.setParentTemplate(parentBundleTemplate);

		Assert.assertNull(defaultBundleSelectionFacade.getBundleParentTemplateByProduct(product.getCode()));

		// products assigned to template
		final List<ProductModel> products = new ArrayList<>();
		products.add(product);
		given(bundleTemplate.getProducts()).willReturn(products);
		boolean containsProduct = bundleTemplateService.containsComponenentProductsOfType(bundleTemplate, getClazz());
		assertFalse(containsProduct);

		// add service product assigned to template
		products.clear();
		final PSServiceProductModel sProduct = new PSServiceProductModel();
		sProduct.setCode(TEST_PRODUCT_CODE);
		products.add(sProduct);
		containsProduct = bundleTemplateService.containsComponenentProductsOfType(bundleTemplate, getClazz());
		assertTrue(containsProduct);

		// 1 child template that has products of type ProductModel
		final BundleTemplateModel productTemplate = mock(BundleTemplateModel.class);
		final List<BundleTemplateModel> childTemplates = new ArrayList<>();
		childTemplates.add(productTemplate);
		given(bundleTemplate.getChildTemplates()).willReturn(childTemplates);
		final ProductModel serviceProductAddon = new ProductModel();
		final List<ProductModel> serviceAddons = new ArrayList<>();
		serviceAddons.add(serviceProductAddon);
		childTemplates.get(0).setProducts(serviceAddons);
	}

	private Class getClazz()
	{
		return PSServiceProductModel.class;
	}
}
