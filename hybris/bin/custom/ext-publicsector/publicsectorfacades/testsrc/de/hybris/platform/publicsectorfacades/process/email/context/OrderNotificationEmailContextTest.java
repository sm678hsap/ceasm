/**
 *
 */
package de.hybris.platform.publicsectorfacades.process.email.context;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Default implementation of {@link OrderNotificationEmailContext}
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderNotificationEmailContextTest
{

	private static final String EXPIRY_CC_MONTH = "expiryCCMonth";
	private static final String EXPIRY_CC_YEAR = "expiryCCYear";

	@InjectMocks
	private OrderNotificationEmailContext orderNotificationEmailContext;

	@Mock
	private Converter<OrderModel, OrderData> orderConverter;

	@Mock
	private OrderData orderData;

	@Mock
	private OrderProcessModel orderProcessModel;

	@Mock
	private OrderModel orderModel;

	@Mock
	private CCPaymentInfoData ccPayment;

	@Mock
	private EmailPageModel emailPageModel;

	@Test
	public void testInit()
	{
		given(orderProcessModel.getOrder()).willReturn(orderModel);
		given(orderConverter.convert(orderModel)).willReturn(orderData);
		orderNotificationEmailContext.setOrderConverter(orderConverter);
		given(orderData.getPaymentInfo()).willReturn(new CCPaymentInfoData());
		given(ccPayment.getExpiryMonth()).willReturn(EXPIRY_CC_MONTH);
		given(ccPayment.getExpiryYear()).willReturn(EXPIRY_CC_YEAR);
		given(orderData.getPaymentInfo()).willReturn(ccPayment);
		orderNotificationEmailContext.init(orderProcessModel, emailPageModel);
	}
}
