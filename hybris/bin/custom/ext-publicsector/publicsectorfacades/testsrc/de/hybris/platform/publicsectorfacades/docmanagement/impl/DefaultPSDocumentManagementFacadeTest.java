/**
 *
 */
package de.hybris.platform.publicsectorfacades.docmanagement.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectordocmanagement.data.PSDocumentData;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * DefaultPSDocumentManagementFacadeTest unit test class.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPSDocumentManagementFacadeTest
{

	private static final String CUSTOMER_UID = "custID";

	@InjectMocks
	private final DefaultPSDocumentManagementFacade psDocumentManagementFacade = new DefaultPSDocumentManagementFacade();

	@Mock
	private UserService userService;

	@Mock
	private PSDocumentManagementService psDocumentManagementService;

	@Mock
	private CustomerModel customer;

	@Mock
	private Converter<PSDocumentModel, PSDocumentData> documentConverter;

	@Test
	public void testGetDocumentsForCustomer()
	{
		Mockito.when(userService.getUserForUID(CUSTOMER_UID)).thenReturn(customer);
		psDocumentManagementFacade.getDocumentsForCustomer(CUSTOMER_UID);

		Mockito.verify(psDocumentManagementService).findActivePSDocumentByCustomer(customer);
	}

	@Test
	public void testGetExpiredDocumentsForCustomer()
	{
		Mockito.when(userService.getUserForUID(CUSTOMER_UID)).thenReturn(customer);
		psDocumentManagementFacade.getExpiredDocumentsForCustomer(CUSTOMER_UID);

		Mockito.verify(psDocumentManagementService).findExpiredPSDocumentByCustomer(customer);
	}

	@Test
	public void testGetDocumentsForUserRelationshipsByStatus()
	{
		final String userId = "TestUser";
		final UserModel testUser = new UserModel();
		testUser.setUid("TestUser");


		final List<PSDocumentModel> docModels = new ArrayList<>();
		docModels.add(Mockito.mock(PSDocumentModel.class));

		final List<PSDocumentData> docData = new ArrayList<>();
		docData.add(Mockito.mock(PSDocumentData.class));

		Mockito.when(userService.getUserForUID(userId)).thenReturn(testUser);
		Mockito.when(psDocumentManagementService.getDocumentsForUserRelationshipsByStatus(testUser, false)).thenReturn(docModels);

		Mockito.when(documentConverter.convertAll(Mockito.anyList())).thenReturn(docData);

		psDocumentManagementFacade.getDocumentsForUserRelationshipsByStatus(userId, false);
	}


}
