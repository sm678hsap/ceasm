/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.product.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.publicsectorfacades.constants.PublicsectorfacadesConstants;
import de.hybris.platform.publicsectorfacades.product.PSProductFacade;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.product.PSProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * This is the implementation class of {@link PSProductFacade}
 */
public class DefaultPSProductFacade implements PSProductFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSProductFacade.class);
	private ProductService productService;
	private PSProductService psProductService;
	private Converter<ProductModel, ProductData> productConverter;

	@Override
	public boolean isUnidentifiedUserInService(final String code)
	{
		final ProductModel product = getProductService().getProductForCode(code);
		return isServiceApplicableForGiveCustomerType(product, PublicsectorfacadesConstants.UNIDENTIFIED_USER);
	}

	@Override
	public boolean isGuestUserInService(final String code)
	{
		final ProductModel product = getProductService().getProductForCode(code);
		return isServiceApplicableForGiveCustomerType(product, PublicsectorfacadesConstants.GUEST_USER);
	}

	@Override
	public boolean isRegisteredUserInService(final String code)
	{
		final ProductModel product = getProductService().getProductForCode(code);
		return isServiceApplicableForGiveCustomerType(product, PublicsectorfacadesConstants.REGISTERED_USER);
	}

	@Override
	public boolean isServiceProduct(final String code)
	{
		final ProductModel product = getProductService().getProductForCode(code);
		LOG.debug("Given product code {} is service product ", code);
		return product instanceof PSServiceProductModel;
	}

	@Override
	public ProductData getServiceProductFromSessionCart()
	{
		final PSServiceProductModel serviceProduct = getPsProductService().getServiceProductFromSessionCart();
		if (serviceProduct != null)
		{
			return getProductConverter().convert(serviceProduct);
		}
		return null;
	}

	@Override
	public ProductData getServiceProductForOrderCode(final String orderCode)
	{
		final PSServiceProductModel serviceProduct = getPsProductService().getServiceProductForOrderCode(orderCode);
		if (serviceProduct != null)
		{
			return getProductConverter().convert(serviceProduct);
		}
		return null;
	}

	protected boolean isServiceApplicableForGiveCustomerType(final ProductModel product, final String customerType)
	{
		if (product instanceof PSServiceProductModel)
		{
			final PSServiceProductModel psServiceProductModel = (PSServiceProductModel) product;

			return psServiceProductModel.getCustomerTypes().stream()
					.filter(serviceCustomerType -> serviceCustomerType.getCode().equals(customerType)).findFirst().isPresent();
		}
		LOG.debug("given service is not applicable for given customer type");
		return false;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected PSProductService getPsProductService()
	{
		return psProductService;
	}

	@Required
	public void setPsProductService(final PSProductService psProductService)
	{
		this.psProductService = psProductService;
	}

	protected Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	@Required
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

}
