/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorfacades.search.converters.populator;

import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.publicsectorservices.enums.PSApplyOnlineCustomerType;
import de.hybris.platform.publicsectorservices.enums.PSDepartment;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * This is PSSearchResultProductPopulator.
 */
public class PSSearchResultProductPopulator implements Populator<SearchResultValueData, ProductData>
{
	private static final String ADD_TO_CART_LABEL = "addToCartLabelText";
	private static final String DEPARTMENTS = "departments";
	private static final String SHORT_DESCRIPTION = "shortDescription";
	private static final String SERVICE_ICON = "serviceIcon";
	private static final String YFORM_DEFINITION_EXISTS = "yFormDefinitionExists";
	private static final String APPLY_ONLINE_FOR_USER_TYPES = "applyOnlineForUserTypes";

	private EnumerationService enumerationService;
	private CommonI18NService commonI18NService;
	private ImageFormatMapping imageFormatMapping;
	private UserService userService;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target) throws ConversionException
	{
		if (source != null && MapUtils.isNotEmpty(source.getValues()))
		{
			target.setAddToCartLabelText((String) source.getValues().get(ADD_TO_CART_LABEL));
			target.setShortDescription((String) source.getValues().get(SHORT_DESCRIPTION));

			final boolean yFormDefinitionExists = source.getValues().get(YFORM_DEFINITION_EXISTS) != null
					? ((Boolean) source.getValues().get(YFORM_DEFINITION_EXISTS)).booleanValue() : false;
			target.setYFormDefinitionExists(yFormDefinitionExists);

			populateServiceApplicabilityForCustomerType(source, target);
			populateDepartments(source, target);
			populateImages(source, target);
		}
	}

	/**
	 * Method to populate image data
	 *
	 * @param source
	 * @param target
	 */
	private void populateImages(final SearchResultValueData source, final ProductData target)
	{
		final List<ImageData> images = createImageData(source);
		if (CollectionUtils.isNotEmpty(images))
		{
			target.setImages(images);
		}
	}

	/**
	 * Method to populate departments
	 *
	 * @param source
	 * @param target
	 */
	private void populateDepartments(final SearchResultValueData source, final ProductData target)
	{
		final List<String> departments = (List<String>) source.getValues().get(DEPARTMENTS);
		if (CollectionUtils.isNotEmpty(departments))
		{
			final List<String> localizedDepartments = new ArrayList<>();
			final String isoCode = commonI18NService.getCurrentLanguage().getIsocode();
			departments.forEach(department -> {
				final HybrisEnumValue departmentEnumValue = getEnumerationService().getEnumerationValue(PSDepartment.class,
						department);
				final String localizedEnumValue = enumerationService.getEnumerationName(departmentEnumValue, new Locale(isoCode));
				localizedDepartments.add(StringUtils.isNotEmpty(localizedEnumValue) ? localizedEnumValue : null);
			});
			target.setDepartments(localizedDepartments);
		}
	}

	/**
	 * Method to check if a service is applicable for the customer type and if it applied for by the user
	 *
	 * @param source
	 * @param target
	 */
	private void populateServiceApplicabilityForCustomerType(final SearchResultValueData source, final ProductData target)
	{
		final List<String> applyOnlineForUserTypes = (List<String>) source.getValues().get(APPLY_ONLINE_FOR_USER_TYPES);

		if (CollectionUtils.isNotEmpty(applyOnlineForUserTypes))
		{
			final CustomerModel customer = (CustomerModel) getUserService().getCurrentUser();
			if (customer != null && applyOnlineForUserTypes.contains(PSApplyOnlineCustomerType.NOT_SIGNED_IN.getCode())
					|| (!getUserService().isAnonymousUser(customer)
							&& applyOnlineForUserTypes.contains(PSApplyOnlineCustomerType.SIGNED_IN.getCode())))
			{
				target.setServiceApplicableForCustomerType(true);
			}
		}
	}

	/**
	 * Method to create Image Data for image format 'serviceIcon'.
	 *
	 * @param source
	 * @return List<ImageData>
	 */
	private List<ImageData> createImageData(final SearchResultValueData source)
	{
		final List<ImageData> result = new ArrayList<>();

		final String mediaFormatQualifier = "img-" + getImageFormatMapping().getMediaFormatQualifierForImageFormat(SERVICE_ICON);
		final String imgValue = (String) source.getValues().get(mediaFormatQualifier);
		if (imgValue != null && !imgValue.isEmpty())
		{
			final ImageData imageData = new ImageData();
			imageData.setImageType(ImageDataType.PRIMARY);
			imageData.setFormat(SERVICE_ICON);
			imageData.setUrl(imgValue);

			result.add(imageData);
		}
		return result;
	}

	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected ImageFormatMapping getImageFormatMapping()
	{
		return imageFormatMapping;
	}

	@Required
	public void setImageFormatMapping(final ImageFormatMapping imageFormatMapping)
	{
		this.imageFormatMapping = imageFormatMapping;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
