/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.asset.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Required;


/**
 * Populates PSAssetAttributeMappingData {@link PSAssetAttributeMappingPopulator}
 */
public class PSAssetAttributeMappingPopulator implements Populator<PSAssetAttributeMappingModel, PSAssetAttributeMappingData>
{
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final PSAssetAttributeMappingModel source, final PSAssetAttributeMappingData target)
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		final String isoCode = getCommonI18NService().getCurrentLanguage().getIsocode();

		target.setAttributeName(source.getAttributeName());
		target.setAttributeDisplayName(source.getAttributeDisplayName(new Locale(isoCode)));
		target.setAttributeDisplayOrder(source.getAttributeDisplayOrder());
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
