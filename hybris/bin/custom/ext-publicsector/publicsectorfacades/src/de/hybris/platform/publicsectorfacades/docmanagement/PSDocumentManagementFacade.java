/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorfacades.docmanagement;

import de.hybris.platform.publicsectordocmanagement.data.PSDocumentData;

import java.util.List;


/**
 *
 * PublicSectorDocumentFacade
 */
public interface PSDocumentManagementFacade
{

	/**
	 * returns list of documents for the given customer
	 *
	 * @param customerUid
	 *           customer uid
	 * @return List<PSDocumentData> of documents specific to customer uid
	 */
	List<PSDocumentData> getDocumentsForCustomer(String customerUid);

	/**
	 * returns list of expired documents for the given customer
	 *
	 * @param customerUid
	 *           customer uid
	 * @return List<PSDocumentData> of expired documents specific to customer uid
	 *
	 */
	List<PSDocumentData> getExpiredDocumentsForCustomer(String customerUid);

	/**
	 * Find document by secured path url
	 *
	 * @param pathUrlId
	 *           Path Url Id
	 * @return Document data
	 *
	 */
	PSDocumentData findDocumentByDocumentSecurePathUrlId(String pathUrlId);

	/**
	 * Decrypt document url by document id
	 *
	 * @param pathUrl
	 *           Path Url Id
	 * @param documentId
	 *           document Id
	 * @return Decrypted document url
	 */
	String decryptDocumentSecurePathUrlById(String pathUrl, String documentId);

	/**
	 * Retrieve List<PSDocumentData> of documents for user relationships by status
	 *
	 * @param userId
	 *           the user id
	 * @param expired
	 *           boolean checks if document is expired
	 * @return List<PSDocumentData> of documents for user relationships by status
	 */
	List<PSDocumentData> getDocumentsForUserRelationshipsByStatus(String userId, boolean expired);

	/**
	 * Retrieve users relationships documents
	 *
	 * @param userId
	 *           the user id
	 * @return List<PSDocumentData> of documents for user relationships
	 */
	List<PSDocumentData> getDocumentsForUserRelationships(String userId);

}
