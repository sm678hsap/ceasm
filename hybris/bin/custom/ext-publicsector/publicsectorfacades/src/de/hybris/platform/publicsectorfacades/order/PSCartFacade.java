/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;

import java.util.List;


/**
 * PSCartFacade
 */
public interface PSCartFacade
{
	/**
	 * Sets returnURL and lastAccessDate in cart model for draft carts
	 *
	 * @param returnURL
	 */
	void setDraftCartDetails(String returnURL);

	/**
	 * Sets credit card details in cart model for draft carts
	 *
	 * @param creditCardModel
	 *           CreditCardPaymentInfoModel for credit card details
	 * @param address
	 *           AddressModel for address details
	 */
	void saveCreditCardDetails(CreditCardPaymentInfoModel creditCardModel, AddressModel address);

	/**
	 * Find title for code
	 *
	 * @param code
	 *           billTo_titleCode attribute from SopPaymentDetailsForm object
	 * @return TitleModel
	 */
	TitleModel getTitleForCode(String code);

	/**
	 * Find billing region
	 *
	 * @param country
	 *           CountryModel for country details
	 * @param billToState
	 *           billTo_state attribute from SopPaymentDetailsForm object
	 * @return RegionModel for region details
	 */
	RegionModel getI18NRegion(CountryModel country, String billToState);

	/**
	 * Find billing country
	 *
	 * @param billToCountry
	 *           billTo_country attribute from SopPaymentDetailsForm object
	 * @return CountryModel for country details
	 */
	CountryModel getI18NCountry(String billToCountry);

	/**
	 * Create new billing address
	 *
	 * @return AddressModel
	 */
	AddressModel getNewBillingAddress();

	/**
	 * Sets payment details in cart model for draft carts
	 *
	 * @param billingAddress
	 *           AddressModel for address details
	 */
	void savePaymentDetailsToCart(AddressModel billingAddress);

	/**
	 * Removes existing cart from session if not empty before adding new products. Public sector does not allow to merge
	 * carts.
	 */
	void removeExistingSessionCart();

	/**
	 * Get saved carts for the current user.
	 *
	 * @return List<CartData>
	 */
	List<CartData> getSavedCartsForCurrentUser();

	/**
	 * Restore cart with code for current user and the site.
	 *
	 * @param code
	 *           code for saved cart
	 * @return CartData
	 * @throws CommerceSaveCartException
	 *            Exception thrown if the cart cannot be saved
	 *
	 */
	CartData restoreCart(String code) throws CommerceSaveCartException;

	/**
	 * To verify if the YForm version has been updated for the saved YFormData.
	 *
	 * @return boolean
	 */
	boolean isYFormVersionUpdatedForCart();

	/**
	 * To update cart on YForm version changed for the saved YFormData.
	 */
	void updateCartOnYFormVersionUpdate();

	/**
	 * Restore cart if applicable and return true else return false
	 *
	 * @param draftNumber
	 *           draft number
	 * @param emailAddress
	 *           email address
	 * @return CartData
	 * @throws CommerceCartRestorationException
	 *            Exception thrown if the cart cannot be restored
	 */

	CartData restoreCartForDraftNumberAndEmailAddress(String draftNumber, String emailAddress)
			throws CommerceCartRestorationException;

	/**
	 * Restores the anonymous user's cart to the session and sets its user to current user.
	 *
	 * @param cart
	 *           Cart Model
	 * @return the cart restoration data that includes details of any items that could not be restored in part or in
	 *         full.
	 * @throws CommerceCartRestorationException
	 *            Exception thrown if the cart cannot be restored
	 */
	CartRestorationData restoreAnonymousCartAndTakeOwnership(CartModel cart) throws CommerceCartRestorationException;

	/**
	 * Removes the given cart
	 *
	 * @param cartCode
	 *           cart code
	 */
	void removeCart(String cartCode);

	/**
	 * returns whether the given cart is associated with current user
	 *
	 * @param code
	 *           code for saved cart
	 * @return boolean
	 */
	boolean isCartAssociatedWithCurrentUser(final String code);

	/**
	 * Updates last access time in the given cart with code and for current user
	 *
	 * @param code
	 *           code for saved cart
	 */
	void updateLastAccessTimeInCartForCurrentUser(final String code);

	/**
	 * Updates return URL and last access time for the given cart with code and for current user.
	 *
	 * @param code
	 *           code for saved cart
	 * @param returnURL
	 *           return url attribute from CartData object
	 */
	void updateReturnURLAndLastAccessTimeInCartForCurrentUser(final String code, String returnURL);

	/**
	 * Updates last access time in the given cart with code and for guest user
	 *
	 * @param code
	 *           code for saved cart
	 * @param guestEmailAddress
	 *           _email attribute from RetrieveDraftForm object
	 */
	void updateLastAccessTimeInCartForGuestUser(final String code, final String guestEmailAddress);


	/**
	 * Returns whether the given cart is a saved draft or not
	 *
	 * @return boolean
	 */
	boolean isCurrentCartSavedDraft();

	/**
	 * To verify if Customer Profile(Personal Details, Email Address or Primary Address) has been updated for saved
	 * Draft.
	 *
	 * @return boolean
	 */
	boolean isCustomerProfileUpdatedAfterDraftSaved();

	/**
	 * To get cart for user and code
	 *
	 * @param code
	 *           code for saved cart
	 * @param customerId
	 *           customer id
	 * @return CartData
	 * @throws CommerceSaveCartException
	 *            Exception thrown if the cart cannot be saved
	 */
	CartData getCartForCodeAndUser(final String code, final String customerId) throws CommerceSaveCartException;

	/**
	 * Sets the current user as cart owner and relationship user in userInContext
	 *
	 * @return CartData
	 */
	CartData setCurrentUserOwnerForRelationshipCart();

	/**
	 * Removes the given cart for given user
	 *
	 * @param customerId
	 *           customer id
	 * @param cartCode
	 *           cart code
	 */
	void removeCartForUser(String customerId, String cartCode);

	/**
	 * Verifies if the draft(cart) is locked by another user
	 *
	 * @param draftCode
	 *           code for saved cart
	 * @return boolean
	 */
	boolean isDraftLocked(final String draftCode);

	/**
	 * Sets lockedBy on draft (cart)
	 *
	 * @return CartData
	 */
	CartData setLockOnDraft();

	/**
	 * Get difference in minutes in which draft(craft) will be unlocked
	 *
	 * @param draftCode
	 *           code for saved cart
	 * @return Long
	 */
	Long getDifferenceInMinutes(final String draftCode);

	/**
	 * Gets the service product from current cart and update the session with newly created cart
	 *
	 * @throws CommerceCartModificationException
	 *            Exception thrown if the cart cannot be modified
	 */
	void rebuildSessionCartFromCurrentCart() throws CommerceCartModificationException;

	/**
	 * Update the user in cart with user in context value of the cart
	 */
	void updateUserInCartWithContextUser();
}
