/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.publicsectorservices.model.PSDraftProcessModel;

import org.apache.velocity.tools.generic.EscapeTool;


/**
 * Velocity context for a draft confirmation email.
 */
public class DraftConfirmationEmailContext extends AbstractEmailContext<PSDraftProcessModel>
{
	private static final String SERVICE_PRODUCT_NAME = "serviceProductName";
	private static final String IS_GUEST_USER = "isGuestUser";
	private static final String ESCAPE_TOOL = "esc";

	private CustomerData customerData;

	@Override
	public void init(final PSDraftProcessModel psDraftProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(psDraftProcessModel, emailPageModel);

		put(IS_GUEST_USER, isGuestUser(getCustomer(psDraftProcessModel)));
		put(SERVICE_PRODUCT_NAME, psDraftProcessModel.getServiceProductName());
		put(ESCAPE_TOOL, new EscapeTool());
	}

	@Override
	protected BaseSiteModel getSite(final PSDraftProcessModel psDraftProcessModel)
	{
		return psDraftProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final PSDraftProcessModel psDraftProcessModel)
	{
		return psDraftProcessModel.getCustomer();
	}

	@Override
	protected LanguageModel getEmailLanguage(final PSDraftProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}

	protected Boolean isGuestUser(final CustomerModel customer)
	{
		if (CustomerType.GUEST.equals(customer.getType()))
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public CustomerData getCustomer()
	{
		return customerData;
	}
}
