/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order.converters.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorfacades.bundle.selection.BundleSelectionFacade;
import de.hybris.platform.publicsectorfacades.order.PSCheckoutFacade;
import de.hybris.platform.publicsectorservices.model.PSServiceProductAddOnModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;


/**
 * The class of PSCartPopulator.
 *
 */
public class PSCartPopulator implements Populator<CartModel, CartData>
{
	private Converter<AddressModel, AddressData> addressConverter;
	private PSCheckoutFacade psCheckoutFacade;
	private BundleSelectionFacade bundleSelectionFacade;
	private Converter<UserModel, CustomerData> customerConverter;

	@Override
	public void populate(final CartModel source, final CartData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setReturnURL(source.getReturnURL());
		target.setLastAccessTime(source.getLastAccessDate());

		addPaymentAddress(source, target);
		setShowDeliveryModeAsTBA(source, target);
		setShowSubtotalAsTBA(source, target);
		if (null != source.getLockedBy())
		{
			target.setLockedBy(getCustomerConverter().convert(source.getLockedBy()));
		}

	}

	protected void addPaymentAddress(final CartModel source, final CartData target)
	{
		if (source.getPaymentAddress() != null)
		{
			target.setPaymentAddress(getAddressConverter().convert(source.getPaymentAddress()));
		}
	}

	protected void setShowDeliveryModeAsTBA(final CartModel source, final CartData target)
	{
		if (getPsCheckoutFacade().isServiceRequestApplicableForDelivery(source) && source.getDeliveryMode() == null)
		{
			target.setShowDeliveryModeAsTBA(true);
		}
	}

	protected void setShowSubtotalAsTBA(final CartModel source, final CartData target)
	{
		PSServiceProductModel serviceProduct = null;
		double basePrice = 0.0;
		boolean isBundleSelected = false;
		for (final AbstractOrderEntryModel entry : source.getEntries())
		{
			if (entry.getProduct() instanceof PSServiceProductModel)
			{
				serviceProduct = (PSServiceProductModel) entry.getProduct();
				basePrice = entry.getBasePrice() != null ? entry.getBasePrice().doubleValue() : 0.0d;
			}
			if (entry.getProduct() instanceof PSServiceProductAddOnModel)
			{
				isBundleSelected = true;
			}
		}

		if (serviceProduct != null && Double.compare(basePrice, 0) == 0
				&& getBundleSelectionFacade().hasBundleAddons(serviceProduct.getCode()).booleanValue() && !isBundleSelected)
		{
			target.setShowSubtotalAsTBA(true);
		}
	}

	protected Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}

	protected BundleSelectionFacade getBundleSelectionFacade()
	{
		return bundleSelectionFacade;
	}

	@Required
	public void setBundleSelectionFacade(final BundleSelectionFacade bundleSelectionFacade)
	{
		this.bundleSelectionFacade = bundleSelectionFacade;
	}

	protected PSCheckoutFacade getPsCheckoutFacade()
	{
		return psCheckoutFacade;
	}

	@Required
	public void setPsCheckoutFacade(final PSCheckoutFacade psCheckoutFacade)
	{
		this.psCheckoutFacade = psCheckoutFacade;
	}

	public Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}
}
