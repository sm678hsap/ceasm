/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.asset.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.commercefacades.asset.data.PSAssetData;
import de.hybris.platform.commercefacades.asset.data.PSAssetTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;
import de.hybris.platform.publicsectorservices.model.PSAssetTypeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * Populates PSAssetData {@link PSAssetPopulator}
 */
public class PSAssetPopulator implements Populator<PSAssetModel, PSAssetData>
{
	private Converter<PSAssetTypeModel, PSAssetTypeData> assetTypeConverter;
	private Converter<PSBillPaymentModel, PSBillPaymentData> assetBillsConverter;
	private UserService userService;

	@Override
	public void populate(final PSAssetModel source, final PSAssetData target)
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setAssetId(source.getAssetId());
		target.setAssetName(source.getName());
		target.setAssetType(getAssetTypeConverter().convert(source.getAssetType()));
		target.setAssetBills(sortAssetBills(getAssetBillsConverter().convertAll(source.getPsBillPayments())));


	}

	private List<PSBillPaymentData> sortAssetBills(final List<PSBillPaymentData> assetBillsData)

	{
		final List<PSBillPaymentData> billsList = new ArrayList<>();
		final List<PSBillPaymentData> paidBillsList = new ArrayList<>();

		for (final PSBillPaymentData bill : assetBillsData)
		{
			if (bill.getCustomer() != null && bill.getCustomer().getUid().equals(getUserService().getCurrentUser().getUid()))
			{

				if (bill.getBillPaymentStatus().equals(BillPaymentStatus.PAID))
				{
					//add paid bills to a separate list of paid bills
					paidBillsList.add(bill);
				}
				else
				{
					//add unpaid and part-paid bills into the list
					billsList.add(bill);
				}

			}
		}

		//sort unpaid and part-paid bills by due date
		billsSortedByDueDate(billsList);

		//sort paid bills by due date
		billsSortedByDueDate(paidBillsList);

		//add paid bills in the end of the bills list
		billsList.addAll(paidBillsList);

		return billsList;
	}

	private void billsSortedByDueDate(final List<PSBillPaymentData> assetBillsData)
	{
		Collections.sort(assetBillsData, (bill1, bill2) -> bill1.getBillDueDate().compareTo(bill2.getBillDueDate()));
	}

	protected Converter<PSAssetTypeModel, PSAssetTypeData> getAssetTypeConverter()
	{
		return assetTypeConverter;
	}

	@Required
	public void setAssetTypeConverter(final Converter<PSAssetTypeModel, PSAssetTypeData> assetTypeConverter)
	{
		this.assetTypeConverter = assetTypeConverter;
	}

	protected Converter<PSBillPaymentModel, PSBillPaymentData> getAssetBillsConverter()
	{
		return assetBillsConverter;
	}

	@Required
	public void setAssetBillsConverter(final Converter<PSBillPaymentModel, PSBillPaymentData> assetBillsConverter)
	{
		this.assetBillsConverter = assetBillsConverter;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
