/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorfacades.strategies;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;


/**
 * PSYFormsStrategy
 */
public interface PSYFormsStrategy
{
	/**
	 * Method to return the OrderEntryData for which YFormDefinitions has been assigned from orderData.
	 *
	 * @param orderData
	 * @return OrderEntryData
	 */
	OrderEntryData getYFormOrderEntry(AbstractOrderData orderData);

	/**
	 * Method to return the OrderEntryData for which YFormDefinitions has been assigned from orderData.
	 * @param orderData
	 * @param productCode
	 * @return OrderEntryData
	 */
	OrderEntryData getYFormOrderEntryByProduct(final AbstractOrderData orderData, final String productCode);

}
