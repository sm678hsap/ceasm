/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.CartModel;


/**
 * PublicSectorCheckoutFacade
 */
public interface PSCheckoutFacade
{

	/**
	 * Checks if Payment is needed for the checkout.
	 *
	 * @return boolean
	 */
	boolean isPaymentNeeded();

	/**
	 * To get OrderEntryData from cart which contains ServiceProductModel.
	 *
	 * @return OrderEntryData
	 */
	OrderEntryData getServiceProductOrderEntryFromCart();

	/**
	 * Removes payment info from the session cart
	 */
	void removePaymentInfoIfNotNeeded();

	/**
	 * Removes delivery info from cart if cart is not deliverable
	 */
	void removeDeliveryInfoIfNotNeeded();

	/**
	 * Recalculate the session cart
	 */
	void recalculateCart();

	/**
	 * returns whether the service request is applicable for delivery
	 *
	 * @param cartModel
	 *           CartModel for cart details
	 * @return Boolean
	 */
	boolean isServiceRequestApplicableForDelivery(final CartModel cartModel);

	/**
	 * return order details for given order
	 *
	 * @param code
	 *           the code
	 * @return OrderData
	 */
	OrderData getOrderDetailsForCode(String code);

}
