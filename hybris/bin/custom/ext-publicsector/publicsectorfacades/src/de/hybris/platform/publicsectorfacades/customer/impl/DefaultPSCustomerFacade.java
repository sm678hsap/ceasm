/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.customer.impl;

import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.billservices.model.PSBillPaymentModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorfacades.customer.PSCustomerFacade;
import de.hybris.platform.publicsectorservices.dashboard.PSAccountDashboardService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * This is the implementation class of {@link PSCustomerFacade}
 */
public class DefaultPSCustomerFacade implements PSCustomerFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSCustomerFacade.class);
	private ModelService modelService;
	private CommonI18NService commonI18NService;
	private CustomerAccountService customerAccountService;
	private CustomerFacade customerFacade;
	private Converter<UserModel, CustomerData> customerConverter;
	private PSAccountDashboardService psAccountDashboardService;
	private UserService userService;
	private Converter<PSBillPaymentModel, PSBillPaymentData> psBillConverter;

	@Override
	public void createUnidentifiedUserForAnonymousCheckout(final String name) throws DuplicateUidException
	{
		final CustomerModel unidentifiedCustomer = getModelService().create(CustomerModel.class);
		final String guid = getCustomerFacade().generateGUID();

		//takes care of localizing the name based on the site language
		unidentifiedCustomer.setUid(guid);
		unidentifiedCustomer.setName(name);
		unidentifiedCustomer.setType(CustomerType.valueOf(CustomerType.UNIDENTIFIED.getCode()));
		unidentifiedCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		unidentifiedCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		LOG.info("creating guest user with name and uid {} {}", name, guid);
		getCustomerAccountService().registerGuestForAnonymousCheckout(unidentifiedCustomer, guid);
		getCustomerFacade().updateCartWithGuestForAnonymousCheckout(getCustomerConverter().convert(unidentifiedCustomer));
	}


	@Override
	public List<PSBillPaymentData> getBillsForUserRelationshipsByStatus(final String userId,
			final List<BillPaymentStatus> statuses)
	{
		if (StringUtils.isNotEmpty(userId))
		{
			final UserModel user = getUserService().getUserForUID(userId);
			final List<PSBillPaymentModel> bills = getPsAccountDashboardService().getBillsForUserRelationshipsByStatus(user,
					statuses);
			return getPsBillConverter().convertAll(bills);
		}
		return Collections.emptyList();
	}




	protected Converter<PSBillPaymentModel, PSBillPaymentData> getPsBillConverter()
	{
		return psBillConverter;
	}

	@Required
	public void setPsBillConverter(final Converter<PSBillPaymentModel, PSBillPaymentData> psBillConverter)
	{
		this.psBillConverter = psBillConverter;
	}


	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	protected PSAccountDashboardService getPsAccountDashboardService()
	{
		return psAccountDashboardService;
	}

	@Required
	public void setPsAccountDashboardService(final PSAccountDashboardService psAccountDashboardService)
	{
		this.psAccountDashboardService = psAccountDashboardService;
	}


	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	@Required
	public void setCustomerFacade(final CustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}
}
