/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.customer;

import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.util.List;


/**
 * This will serve as the interface for Publicsector customer services.
 */
public interface PSCustomerFacade
{

	/**
	 * Creates a new unidentified customer for anonymousCheckout and sets the name.
	 *
	 * @param name
	 *           the name or the uid for anonymous checkout
	 * @throws DuplicateUidException
	 *            Exception is thrown when an attempt to store an UID that is already assigned
	 */
	void createUnidentifiedUserForAnonymousCheckout(String name) throws DuplicateUidException;

	/**
	 * Retrieve List of bills for user relationships specific to bill status
	 *
	 * @param userId
	 *           user id
	 * @param statuses
	 *           List<BillPaymentStatus> of bill statuses applicable to bills
	 * @return List<PSBillPaymentData> of bills for user relationships specific to bill status
	 */
	List<PSBillPaymentData> getBillsForUserRelationshipsByStatus(String userId, List<BillPaymentStatus> statuses);

}
