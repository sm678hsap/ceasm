/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.service.data.PSApplyByEmailData;
import de.hybris.platform.commercefacades.service.data.PSApplyByPhoneData;
import de.hybris.platform.commercefacades.service.data.PSApplyOnlineData;
import de.hybris.platform.commercefacades.service.data.PSConsentDeclarationData;
import de.hybris.platform.commercefacades.service.data.PSKeyFactsData;
import de.hybris.platform.commercefacades.service.data.PSRelatedLinksData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.publicsectorservices.model.PSApplyByEmailModel;
import de.hybris.platform.publicsectorservices.model.PSApplyByPhoneModel;
import de.hybris.platform.publicsectorservices.model.PSApplyOnlineModel;
import de.hybris.platform.publicsectorservices.model.PSConsentDeclarationModel;
import de.hybris.platform.publicsectorservices.model.PSKeyFactsModel;
import de.hybris.platform.publicsectorservices.model.PSRelatedLinksModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * Populates {@link ProductData}
 */
public class PSServiceProductPopulator implements Populator<ProductModel, ProductData>
{
	private Converter<PSApplyOnlineModel, PSApplyOnlineData> applyOnlineCMSConverter;
	private Converter<PSApplyByPhoneModel, PSApplyByPhoneData> applyByPhoneCMSConverter;
	private Converter<PSApplyByEmailModel, PSApplyByEmailData> applyByEmailCMSConverter;
	private Converter<PSRelatedLinksModel, PSRelatedLinksData> relatedLinksCMSConverter;
	private Converter<PSKeyFactsModel, PSKeyFactsData> keyFactsCMSConverter;
	private Converter<PSConsentDeclarationModel, PSConsentDeclarationData> consentDeclarationConverter;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		final ProductModel baseProduct = getBaseProduct(source);

		if (baseProduct instanceof PSServiceProductModel)
		{
			final PSServiceProductModel serviceProductModel = (PSServiceProductModel) baseProduct;

			final List<PSApplyOnlineData> psApplyOnlineData = Converters.convertAll(serviceProductModel.getApplyOnline(),
					getApplyOnlineCMSConverter());
			final PSApplyByEmailData applyByEmailData = getApplyByEmailCMSConverter().convert(serviceProductModel.getApplyByEmail());
			final PSApplyByPhoneData applyByPhoneData = getApplyByPhoneCMSConverter().convert(serviceProductModel.getApplyByPhone());
			final PSRelatedLinksData relatedLinksData = getRelatedLinksCMSConverter().convert(serviceProductModel.getRelatedLinks());
			final PSKeyFactsData keyFactsData = getKeyFactsCMSConverter().convert(serviceProductModel.getKeyFacts());
			final List<PSConsentDeclarationData> consentDeclarationData = Converters
					.convertAll(serviceProductModel.getConsentDeclaration(), getConsentDeclarationConverter());

			target.setApplyByEmailCMS(applyByEmailData);
			target.setApplyByPhoneCMS(applyByPhoneData);
			target.setApplyOnlineCMS(psApplyOnlineData);
			target.setRelatedLinks(relatedLinksData);
			target.setKeyFacts(keyFactsData);
			target.setAddToCartLabelText(serviceProductModel.getAddToCartLabelText());
			target.setConsentDeclarations(consentDeclarationData);

			target.setCustomerTypes(new ArrayList(serviceProductModel.getCustomerTypes()));
		}
	}

	/**
	 * Get the base product model
	 *
	 * @param productModel
	 * @return ProductModel
	 */
	protected ProductModel getBaseProduct(final ProductModel productModel)
	{
		ProductModel currentProduct = productModel;
		while (currentProduct instanceof VariantProductModel)
		{
			final VariantProductModel variant = (VariantProductModel) currentProduct;
			currentProduct = variant.getBaseProduct();
		}
		return currentProduct;
	}

	protected Converter<PSApplyOnlineModel, PSApplyOnlineData> getApplyOnlineCMSConverter()
	{
		return applyOnlineCMSConverter;
	}

	@Required
	public void setApplyOnlineCMSConverter(final Converter<PSApplyOnlineModel, PSApplyOnlineData> serviceApplyOnlineCMSConverter)
	{
		this.applyOnlineCMSConverter = serviceApplyOnlineCMSConverter;
	}

	protected Converter<PSApplyByPhoneModel, PSApplyByPhoneData> getApplyByPhoneCMSConverter()
	{
		return applyByPhoneCMSConverter;
	}

	@Required
	public void setApplyByPhoneCMSConverter(
			final Converter<PSApplyByPhoneModel, PSApplyByPhoneData> serviceApplyByPhoneCMSConverter)
	{
		this.applyByPhoneCMSConverter = serviceApplyByPhoneCMSConverter;
	}

	protected Converter<PSApplyByEmailModel, PSApplyByEmailData> getApplyByEmailCMSConverter()
	{
		return applyByEmailCMSConverter;
	}

	@Required
	public void setApplyByEmailCMSConverter(
			final Converter<PSApplyByEmailModel, PSApplyByEmailData> serviceApplyByEmailCMSConverter)
	{
		this.applyByEmailCMSConverter = serviceApplyByEmailCMSConverter;
	}

	protected Converter<PSRelatedLinksModel, PSRelatedLinksData> getRelatedLinksCMSConverter()
	{
		return relatedLinksCMSConverter;
	}

	@Required
	public void setRelatedLinksCMSConverter(
			final Converter<PSRelatedLinksModel, PSRelatedLinksData> serviceRelatedLinksCMSConverter)
	{
		this.relatedLinksCMSConverter = serviceRelatedLinksCMSConverter;
	}

	protected Converter<PSKeyFactsModel, PSKeyFactsData> getKeyFactsCMSConverter()
	{
		return keyFactsCMSConverter;
	}

	@Required
	public void setKeyFactsCMSConverter(final Converter<PSKeyFactsModel, PSKeyFactsData> servicekeyFactsCMSConverter)
	{
		this.keyFactsCMSConverter = servicekeyFactsCMSConverter;
	}

	protected Converter<PSConsentDeclarationModel, PSConsentDeclarationData> getConsentDeclarationConverter()
	{
		return consentDeclarationConverter;
	}

	@Required
	public void setConsentDeclarationConverter(
			final Converter<PSConsentDeclarationModel, PSConsentDeclarationData> consentDeclarationConverter)
	{
		this.consentDeclarationConverter = consentDeclarationConverter;
	}

}
