/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.configurablebundlefacades.converters.populator.BundleTemplatePopulator;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * This Populator extends {@link BundleTemplatePopulator}.
 */
public class PSBundleTemplatePopulator implements Populator<BundleTemplateModel, BundleTemplateData>
{
	private Converter<ProductModel, ProductData> psBasicServiceConverter;

	@Override
	public void populate(final BundleTemplateModel source, final BundleTemplateData target)
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setId(source.getId());
		target.setHeaderMessage(source.getHeaderMessage());
		target.setCheckoutStepLabel(source.getCheckoutStepLabel());
		populateProducts(source, target);
	}

	/**
	 * Populates the products of the bundle template.
	 *
	 * @param source
	 *           the BundleTemplateModel
	 * @param target
	 *           the BundleTemplateData
	 */
	protected void populateProducts(final BundleTemplateModel source, final BundleTemplateData target)
	{
		if (CollectionUtils.isNotEmpty(source.getProducts()))
		{
			target.setProducts(Converters.convertAll(source.getProducts(),getPsBasicServiceConverter()));
		}
	}

	protected Converter<ProductModel, ProductData> getPsBasicServiceConverter()
	{
		return psBasicServiceConverter;
	}

	@Required
	public void setPsBasicServiceConverter(final Converter<ProductModel, ProductData> psBasicServiceConverter)
	{
		this.psBasicServiceConverter = psBasicServiceConverter;
	}

}
