/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
/**
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators.relatedlinks;

import de.hybris.platform.commercefacades.service.data.PSLinkData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSLinkModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * PSLinkPopulator
 *
 * returns the service related links data for the product
 */
public class PSLinkPopulator<SOURCE extends PSLinkModel, TARGET extends PSLinkData> implements Populator<SOURCE, TARGET>
{
	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param PSServiceLinkModel
	 *           the source object
	 * @param PSLinkData
	 *           the target to fill
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		if (source != null)
		{
			target.setLinkName(source.getLinkName());
			target.setUrl(source.getUrl());
		}
	}

}
