/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.service.data.PSApplyOnlineData;
import de.hybris.platform.publicsectorservices.model.PSApplyOnlineModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * PSApplyOnlineCMSPopulator
 *
 * returns the service apply online data for the product
 */
public class PSApplyOnlineCMSPopulator extends AbstractPSServiceApplicationPopulator<PSApplyOnlineModel, PSApplyOnlineData>
{

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param source
	 *           PSApplyOnlineModel the source object
	 * @param target
	 *           PSApplyOnlineData the target to fill
	 */
	@Override
	public void populate(final PSApplyOnlineModel source, final PSApplyOnlineData target) throws ConversionException
	{
		validateParameterNotNull(target, "Parameter target cannot be null");

		super.populate(source, target);
		if (source != null)
		{
			target.setCustomerType(source.getCustomerType());
		}
	}
}
