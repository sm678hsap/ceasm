/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Velocity context for a order notification email.
 */
public class OrderNotificationEmailContext extends AbstractEmailContext<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(OrderNotificationEmailContext.class);
	private static final String SERVICE_PRODUCT_NAME = "serviceProductName";
	private static final String EXPIRY_CC_MONTH = "expiryCCMonth";
	private static final String EXPIRY_CC_YEAR = "expiryCCYear";

	private Converter<OrderModel, OrderData> orderConverter;
	private OrderData orderData;
	private String expiryMonth;
	private String expiryYear;


	@Override
	public void init(final OrderProcessModel orderProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(orderProcessModel, emailPageModel);
		orderData = getOrderConverter().convert(orderProcessModel.getOrder());

		if (orderData.getPaymentInfo() != null)
		{
			expiryMonth = orderData.getPaymentInfo().getExpiryMonth();
			expiryYear = orderData.getPaymentInfo().getExpiryYear();

			put(EXPIRY_CC_MONTH, (expiryMonth.length() == 1) ? "0" + expiryMonth : expiryMonth);
			put(EXPIRY_CC_YEAR, (expiryYear.length() == 4) ? expiryYear.substring(2, expiryYear.length()) : expiryYear);
		}
		put(SERVICE_PRODUCT_NAME, getServiceProductName(orderProcessModel.getOrder()));
	}

	private String getServiceProductName(final OrderModel order)
	{
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		for (final AbstractOrderEntryModel orderEntry : entries)
		{
			if (orderEntry.getProduct() instanceof PSServiceProductModel)
			{
				return orderEntry.getProduct().getName();
			}
		}
		LOG.debug("Service product not found in the given order {}", order);
		return null;
	}

	@Override
	protected BaseSiteModel getSite(final OrderProcessModel orderProcessModel)
	{
		return orderProcessModel.getOrder().getSite();
	}

	@Override
	protected CustomerModel getCustomer(final OrderProcessModel orderProcessModel)
	{
		return (CustomerModel) orderProcessModel.getOrder().getUser();
	}

	@Override
	protected LanguageModel getEmailLanguage(final OrderProcessModel orderProcessModel)
	{
		return orderProcessModel.getOrder().getLanguage();
	}

	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}

	@Required
	public void setOrderConverter(final Converter<OrderModel, OrderData> orderConverter)
	{
		this.orderConverter = orderConverter;
	}

	public OrderData getOrder()
	{
		return orderData;
	}
}
