/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.asset;

import de.hybris.platform.commercefacades.asset.data.PSAssetData;

import java.util.List;


/**
 * PSAssetFacade
 */
public interface PSAssetFacade
{
	/**
	 * Gets Assets for given user
	 *
	 * @param userId
	 *           user id
	 * @return List<PSAssetData> of assets specific to user id
	 */
	List<PSAssetData> getAssetsForUser(String userId);

	/**
	 * Get asset details by code.
	 *
	 * @param assetCode
	 *           asset code
	 * @return AssetData data object for asset details specific to asset code
	 */
	PSAssetData getAssetDetailsByCode(String assetCode);

	/**
	 * Get all attributes related to the asset.
	 *
	 * @param assetCode
	 *           asset code
	 * @param assetType
	 *           asset type
	 * @return assetAttributesTemplate
	 */
	String getAssetAttributeDetails(String assetCode, String assetType);
}
