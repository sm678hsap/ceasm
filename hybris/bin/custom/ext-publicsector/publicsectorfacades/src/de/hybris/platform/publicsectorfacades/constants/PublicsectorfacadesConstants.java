/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.constants;

/**
 * Global class for all Publicsectorfacades constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class PublicsectorfacadesConstants extends GeneratedPublicsectorfacadesConstants
{
	public static final String EXTENSIONNAME = "publicsectorfacades";

	public static final String UNIDENTIFIED_USER = "UNIDENTIFIED";
	public static final String GUEST_USER = "GUEST";
	public static final String REGISTERED_USER = "REGISTERED";

	private PublicsectorfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
