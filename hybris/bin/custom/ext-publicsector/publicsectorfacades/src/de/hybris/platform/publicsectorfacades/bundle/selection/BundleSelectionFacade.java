/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.bundle.selection;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;

import java.util.List;
import java.util.Map;


/**
 * This will serve as the interface for Bundle selection services.
 */
public interface BundleSelectionFacade
{
	/**
	 * Retrieves the bundle parent template of a child bundle.
	 *
	 * @param productCode
	 *           the service product code of the cart.
	 * @return the parent template id
	 */
	String getBundleParentTemplateByProduct(final String productCode);

	/**
	 * Checks if the service product in the cart has a bundle with addons on it.
	 *
	 * @param productCode
	 *           the service product code of the cart.
	 * @return True or False
	 */
	Boolean hasBundleAddons(final String productCode);

	/**
	 * Retrieves the bundle template model by product code
	 *
	 * @param productCode
	 *           the product code
	 * @return the Bundle Template Data
	 */
	BundleTemplateData getBundleTemplateByProductCode(final String productCode);

	/**
	 * Retrieves the child templates of the parent template.
	 *
	 * @param productCode
	 *           the service product code of the cart.
	 * @return List of BundleTemplateData
	 */
	List<BundleTemplateData> getChildrenTemplates(final String productCode);

	/**
	 * Retrieves bundle template by bundle code.
	 *
	 * @param bundleCode
	 *           the bundle code
	 * @return BundleTemplateData
	 */
	BundleTemplateData getBundleTemplateByCode(final String bundleCode);

	/**
	 * Retrieves bundle template status by bundle code.
	 *
	 * @param productCode
	 *           the product code
	 * @return BundleTemplateData
	 */
	String getBundleTemplateStatusByCode(final String productCode);

	/**
	 * Return the disabled products for a given product
	 *
	 * @param productCode
	 *           the product code
	 * @return list of disabled product models
	 */
	List<ProductData> returnDisabledProducts(final String productCode);

	/**
	 * Returns the bundle information for a given cart
	 *
	 * @param cartData
	 *           AbstractOrderData data object for cart details
	 * @return map of bundle product and respective bundle template data
	 */
	Map<ProductData, BundleTemplateData> getSelectedBundlesInformationForCart(AbstractOrderData cartData);

	/**
	 * Checks if Bundle Selection Criteria in Cart is met and list out all child bundle templates whose services product
	 * addons are not in cart.
	 *
	 * @param productCode
	 *           of the Service Product.
	 * @return List<Integer> the child bundle sequence indexes which were not added to cart.
	 *
	 */
	List<Integer> getChildBundlesNotInCart(final String productCode);

	/**
	 * Cleanup the cart before adding service product
	 *
	 * @param productCode
	 *           the service product code
	 * @throws CommerceCartModificationException
	 *            if the cart cannot be modified
	 */
	void cartCleanupBeforeAdd(final String productCode) throws CommerceCartModificationException;

	/**
	 * Check eligibility to add bundle addon
	 *
	 * @param productCode
	 *           the service product code
	 * @return true if bundle can add
	 */
	boolean isEligibleToAddBundleAddon(final String productCode);

}
