/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorfacades.order.impl;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.publicsectorfacades.order.PSCheckoutFacade;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.publicsectorservices.order.PSCartService;
import de.hybris.platform.relationshipservices.permission.service.PSPermissionService;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation of public sector checkout facade
 *
 */
public class DefaultPSCheckoutFacade implements PSCheckoutFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSCheckoutFacade.class);
	private static final String ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE = "Order with guid %s not found for current user in current BaseStore";
	private static final String SERVICE_REQUEST_PERMISSIBLE_AREA = "PSServiceProduct";

	private CheckoutFacade checkoutFacade;
	private CartService cartService;
	private ModelService modelService;
	private PSCartService psCartService;
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;
	private BaseStoreService baseStoreService;
	private Converter<OrderModel, OrderData> orderConverter;
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	private UserService userService;
	private CustomerAccountService customerAccountService;
	private PSPermissionService psPermissionService;

	/**
	 * checks if the checkout needs payment info or not based on service price
	 *
	 * @return boolean
	 */
	@Override
	public boolean isPaymentNeeded()
	{
		//assuming cart will contain only one entry & will return new cart even if no cart exists
		final CartData sessionCart = getCheckoutFacade().getCheckoutCart();
		if (sessionCart.getTotalPrice() != null && sessionCart.getTotalPrice().getValue().compareTo(BigDecimal.ZERO) > 0)
		{
			return true;
		}
		return false;
	}

	/**
	 * To get OrderEntryData from cart which contains ServiceProductModel.
	 *
	 * @return OrderEntryData
	 */
	@Override
	public OrderEntryData getServiceProductOrderEntryFromCart()
	{
		if (getCartService().hasSessionCart() && getCartService().getSessionCart() != null
				&& CollectionUtils.isNotEmpty(getCartService().getSessionCart().getEntries()))
		{
			final CartModel cartModel = getCartService().getSessionCart();
			final Optional<AbstractOrderEntryModel> entry = cartModel.getEntries().stream()
					.filter(e -> e.getProduct() instanceof PSServiceProductModel).findFirst();
			if (entry.isPresent())
			{
				return getOrderEntryConverter().convert(entry.get());
			}
		}
		LOG.debug("No Service product found in cart");
		return null;
	}

	@Override
	public void removePaymentInfoIfNotNeeded()
	{
		final CartModel cartModel = getCartService().getSessionCart();
		if (cartModel != null && !isPaymentNeeded())
		{
			cartModel.setPaymentInfo(null);
			getModelService().save(cartModel);
		}
	}

	@Override
	public void removeDeliveryInfoIfNotNeeded()
	{
		final CartModel cartModel = getCartService().getSessionCart();
		if (cartModel != null && !isServiceRequestApplicableForDelivery(cartModel))
		{
			cartModel.setDeliveryAddress(null);
			cartModel.setDeliveryMode(null);
			getModelService().save(cartModel);
		}
	}

	/**
	 * Checks if cart entries are deliverable
	 *
	 * @param cartModel
	 * @return true, if cart contains entries that has delivery modes
	 */
	@Override
	public boolean isServiceRequestApplicableForDelivery(final CartModel cartModel)
	{
		if (cartModel != null && CollectionUtils.isNotEmpty(cartModel.getEntries()))
		{
			return cartModel.getEntries().stream().filter(e -> CollectionUtils.isNotEmpty(e.getProduct().getDeliveryModes()))
					.findFirst().isPresent();
		}
		LOG.debug("Service request not applicable for delivery");
		return false;
	}

	@Override
	public void recalculateCart()
	{
		final CartModel cartModel = getCartService().getSessionCart();
		if (cartModel != null)
		{
			getPsCartService().recalculateCart(cartModel);
		}
	}

	@Override
	public OrderData getOrderDetailsForCode(final String code)
	{
		final UserModel currentUser = getUserService().getCurrentUser();

		OrderModel orderModel;
		if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			orderModel = getCustomerAccountService().getOrderDetailsForGUID(code, getBaseStoreService().getCurrentBaseStore());
		}
		else
		{
			try
			{
				orderModel = getCustomerAccountService().getOrderForCode(code, getBaseStoreService().getCurrentBaseStore());

				if (!orderModel.getUser().getUid().equalsIgnoreCase(currentUser.getUid())
						&& !getPsPermissionService().isPermitted(currentUser, orderModel.getUser(), SERVICE_REQUEST_PERMISSIBLE_AREA))
				{
					LOG.debug("current user {} dont have access to {} order", currentUser.getUid(), orderModel.getUser());
					orderModel = null;
				}
			}
			catch (final ModelNotFoundException e)
			{
				LOG.error("Model does not exists {}", e);
				throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, code));
			}
			catch (final RelationshipDoesNotExistException e)
			{
				LOG.error("Relationship does not exists {}", e);
				orderModel = null;
			}
		}

		if (orderModel == null)
		{
			throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, code));
		}
		return getOrderConverter().convert(orderModel);
	}

	protected CheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	@Required
	public void setCheckoutFacade(final CheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected Converter<AbstractOrderEntryModel, OrderEntryData> getOrderEntryConverter()
	{
		return orderEntryConverter;
	}

	@Required
	public void setOrderEntryConverter(final Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter)
	{
		this.orderEntryConverter = orderEntryConverter;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected PSCartService getPsCartService()
	{
		return psCartService;
	}

	@Required
	public void setPsCartService(final PSCartService psCartService)
	{
		this.psCartService = psCartService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}

	@Required
	public void setOrderConverter(final Converter<OrderModel, OrderData> orderConverter)
	{
		this.orderConverter = orderConverter;
	}

	protected CheckoutCustomerStrategy getCheckoutCustomerStrategy()
	{
		return checkoutCustomerStrategy;
	}

	@Required
	public void setCheckoutCustomerStrategy(final CheckoutCustomerStrategy checkoutCustomerStrategy)
	{
		this.checkoutCustomerStrategy = checkoutCustomerStrategy;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected PSPermissionService getPsPermissionService()
	{
		return psPermissionService;
	}

	@Required
	public void setPsPermissionService(final PSPermissionService psPermissionService)
	{
		this.psPermissionService = psPermissionService;
	}
}
