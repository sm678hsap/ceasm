/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators;

import de.hybris.platform.commercefacades.service.data.PSConsentDeclarationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSConsentDeclarationModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * PSConsentDeclarationPopulator
 *
 * returns the service consent declaration data for the product
 */
public class PSConsentDeclarationPopulator<SOURCE extends PSConsentDeclarationModel, TARGET extends PSConsentDeclarationData>
		implements Populator<SOURCE, TARGET>
{

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param PSConsentDeclarationModel
	 *           the source object
	 * @param PSConsentDeclarationData
	 *           the target to fill
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		if (source != null)
		{
			target.setText(source.getText());
		}
	}
}
