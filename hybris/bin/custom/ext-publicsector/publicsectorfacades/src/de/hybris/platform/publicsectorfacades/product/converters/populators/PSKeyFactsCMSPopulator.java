/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators;

import de.hybris.platform.commercefacades.service.data.PSKeyFactsData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSKeyFactsModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * PSKeyFactsCMSPopulator
 *
 * returns the service key facts data for the product
 */
public class PSKeyFactsCMSPopulator<SOURCE extends PSKeyFactsModel, TARGET extends PSKeyFactsData>
		implements Populator<SOURCE, TARGET>
{

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param PSKeyFactsModel
	 *           the source object
	 * @param PSKeyFactsData
	 *           the target to fill
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		if (source != null)
		{
			target.setDescription(source.getDescription());
			target.setTitle(source.getTitle());
		}
	}
}
