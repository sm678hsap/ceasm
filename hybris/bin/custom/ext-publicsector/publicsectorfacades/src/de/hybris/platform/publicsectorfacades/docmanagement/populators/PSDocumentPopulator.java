/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.docmanagement.populators;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectordocmanagement.data.PSDocumentData;
import de.hybris.platform.publicsectordocmanagement.data.PSDocumentTagData;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentTagModel;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;
import de.hybris.platform.publicsectorservices.helper.PSFilesHelper;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * PSDocumentPopulator to populate PSDocumentData
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class PSDocumentPopulator<SOURCE extends PSDocumentModel, TARGET extends PSDocumentData>
		implements Populator<SOURCE, TARGET>
{

	private static final Logger LOG = LoggerFactory.getLogger(PSDocumentPopulator.class);
	private Converter<PSDocumentTagModel, PSDocumentTagData> documentTagConverter;
	private PSFilesHelper psFilesHelper;
	private PSDocumentManagementService psDocumentManagementService;

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param source
	 *           - PSDocumentModel the source object
	 * @param target
	 *           - PSDocumentData the target to fill
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException {
		if (source != null)
		{
			target.setDocId(source.getDocId());
			target.setName(source.getDocName());
			target.setExpiresOn(source.getExpiresOn());
			target.setIssuedOn(source.getIssuedOn());
			target.setFileName(source.getFileName());
			target.setFilePath(source.getFilePath());
			target.setDocumentTags(Converters.convertAll(source.getPsDocumentTag(), getDocumentTagConverter()));
			addFileType(source, target);
			target.setFileSize(source.getFileSize());
			try
			{
				if (source.getDocumentThumbnail() != null)
				{
					target.setEncodedThumbnail(getPsFilesHelper().getImageAsEncodedString(
							source.getDocumentThumbnail().getFilePath() + "/" + source.getDocumentThumbnail().getFileName()));
					target.setThumbnailMimeType(source.getDocumentThumbnail().getMimeType());
				}
			}
			catch (final IOException e)
			{
				LOG.error("Exception occurred during file handling due to : ", e);
			}
			try
			{
				target.setDownloadUrl(getPsDocumentManagementService().getDownloadUrlForDocument(source.getSecurePathURLId()));
			}
			catch (final UnsupportedEncodingException e)
			{
				LOG.error("Encoding of secure path was un-successful! {}", e);
			}

			target.setCustomerUid(source.getCustomer().getUid());
			target.setSecurePathURLId(source.getSecurePathURLId());
			target.setMimeType(source.getMimeType());
		}
	}

	private void addFileType(final SOURCE source, final TARGET target)
	{
		final String fileName = source.getFileName();
		final int lastIndex = fileName.lastIndexOf('.');
		if (lastIndex < fileName.length() - 1)
		{
			target.setFileType(fileName.substring(lastIndex + 1).toUpperCase());
		}
		else
		{
			target.setFileType(StringUtils.EMPTY);
		}
	}

	protected Converter<PSDocumentTagModel, PSDocumentTagData> getDocumentTagConverter()
	{
		return documentTagConverter;
	}

	@Required
	public void setDocumentTagConverter(final Converter<PSDocumentTagModel, PSDocumentTagData> documentTagConverter)
	{
		this.documentTagConverter = documentTagConverter;
	}

	protected PSFilesHelper getPsFilesHelper()
	{
		return psFilesHelper;
	}

	@Required
	public void setPsFilesHelper(final PSFilesHelper psFilesHelper)
	{
		this.psFilesHelper = psFilesHelper;
	}

	protected PSDocumentManagementService getPsDocumentManagementService()
	{
		return psDocumentManagementService;
	}

	@Required
	public void setPsDocumentManagementService(final PSDocumentManagementService psDocumentManagementService)
	{
		this.psDocumentManagementService = psDocumentManagementService;
	}
}
