/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.asset.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.asset.data.PSAssetTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSAssetTypeModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Required;


/**
 * Populates PSAssetTypeData {@link PSAssetTypePopulator}
 */
public class PSAssetTypePopulator implements Populator<PSAssetTypeModel, PSAssetTypeData>
{
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final PSAssetTypeModel source, final PSAssetTypeData target)
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		final Locale locale = getLocale();
		target.setCode(source.getCode());
		target.setName(source.getName(locale));
		target.setIconMediaURL(source.getIcon(locale) != null ? source.getIcon(locale).getURL() : "");
	}

	private Locale getLocale()
	{
		final String isoCode = getCommonI18NService().getCurrentLanguage().getIsocode();
		return getCommonI18NService().getLocaleForIsoCode(isoCode);
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

}
