/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.docmanagement.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectordocmanagement.data.PSDocumentTagData;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentTagModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * PSDocumentTagPopulator to populate PSDocumentTagData
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class PSDocumentTagPopulator<SOURCE extends PSDocumentTagModel, TARGET extends PSDocumentTagData>
		implements Populator<SOURCE, TARGET>
{

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param source
	 *           - PSDocumentModel the source object
	 * @param target
	 *           - PSDocumentData the target to fill
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		if (source != null)
		{
			target.setDocTagId(source.getDocTagId());
			target.setTagName(source.getDocTagName());
		}
	}

}
