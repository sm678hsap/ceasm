/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorfacades.docmanagement.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectordocmanagement.data.PSDocumentData;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;
import de.hybris.platform.publicsectorfacades.docmanagement.PSDocumentManagementFacade;
import de.hybris.platform.publicsectorservices.encoder.PSEncoder;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Default implementation of PSDocumentManagementFacade
 *
 */
public class DefaultPSDocumentManagementFacade implements PSDocumentManagementFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSDocumentManagementFacade.class);
	private PSDocumentManagementService psDocumentManagementService;
	private Converter<PSDocumentModel, PSDocumentData> documentTagConverter;
	private UserService userService;
	private PSEncoder psEncoder;

	/**
	 * returns list of documents for the given customer
	 *
	 * @param customerUid
	 * @return PSDocumentData
	 */
	@Override
	public List<PSDocumentData> getDocumentsForCustomer(final String customerUid)
	{
		final CustomerModel customer = (CustomerModel) getUserService().getUserForUID(customerUid);
		final List<PSDocumentModel> documents = getPsDocumentManagementService().findActivePSDocumentByCustomer(customer);
		return Converters.convertAll(documents, getDocumentConverter());
	}

	/**
	 * returns list of expired documents for the given customer
	 *
	 * @param customerUid
	 * @return PSDocumentData
	 */
	@Override
	public List<PSDocumentData> getExpiredDocumentsForCustomer(final String customerUid)
	{
		final CustomerModel customer = (CustomerModel) getUserService().getUserForUID(customerUid);
		final List<PSDocumentModel> documents = getPsDocumentManagementService().findExpiredPSDocumentByCustomer(customer);
		return Converters.convertAll(documents, getDocumentConverter());
	}

	@Override
	public PSDocumentData findDocumentByDocumentSecurePathUrlId(final String pathUrlId)
	{
		final PSDocumentModel documentModel = getPsDocumentManagementService().findDocumentByDocumentSecurePathUrlId(pathUrlId);
		if (documentModel != null)
		{
			return getDocumentConverter().convert(documentModel);
		}
		LOG.debug("no document found for the given path url id {}", pathUrlId);
		return null;
	}

	@Override
	public String decryptDocumentSecurePathUrlById(final String pathUrl, final String documentId)
	{
		return psEncoder.decrypt(pathUrl);
	}

	@Override
	public List<PSDocumentData> getDocumentsForUserRelationshipsByStatus(final String userId, final boolean expired)
	{
		if (StringUtils.isNotEmpty(userId))
		{
			final UserModel user = getUserService().getUserForUID(userId);
			final List<PSDocumentModel> documents = getPsDocumentManagementService().getDocumentsForUserRelationshipsByStatus(user,
					expired);
			return getDocumentConverter().convertAll(documents);
		}
		return Collections.emptyList();
	}

	@Override
	public List<PSDocumentData> getDocumentsForUserRelationships(final String userId)
	{
		if (StringUtils.isNotEmpty(userId))
		{
			final UserModel user = getUserService().getUserForUID(userId);
			final List<PSDocumentModel> documents = getPsDocumentManagementService().getDocumentsForUserRelationships(user);
			return getDocumentConverter().convertAll(documents);
		}
		return Collections.emptyList();
	}


	protected PSDocumentManagementService getPsDocumentManagementService()
	{
		return psDocumentManagementService;
	}

	@Required
	public void setPsDocumentManagementService(final PSDocumentManagementService psDocumentManagementService)
	{
		this.psDocumentManagementService = psDocumentManagementService;
	}

	protected Converter<PSDocumentModel, PSDocumentData> getDocumentConverter()
	{
		return documentTagConverter;
	}

	@Required
	public void setDocumentConverter(final Converter<PSDocumentModel, PSDocumentData> documentTagConverter)
	{
		this.documentTagConverter = documentTagConverter;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public PSEncoder getPsEncoder()
	{
		return psEncoder;
	}

	@Required
	public void setPsEncoder(final PSEncoder psEncoder)
	{
		this.psEncoder = psEncoder;
	}
}
