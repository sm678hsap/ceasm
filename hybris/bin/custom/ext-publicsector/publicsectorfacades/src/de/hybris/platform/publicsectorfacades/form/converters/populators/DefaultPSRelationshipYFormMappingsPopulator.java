/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.form.converters.populators;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.relationshipfacades.permission.PSPermissionFacade;
import de.hybris.platform.relationshipfacades.relationship.PSRelationshipFacade;
import de.hybris.platform.relationshipservices.relationship.exception.RelationshipDoesNotExistException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;
import de.hybris.platform.xyformscommercefacades.populators.DefaultYFormMappingsPopulator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Class DefaultPSRelationshipYFormMappingsPopulator to populate YFormMappingsData from CustomerModel
 */
public class DefaultPSRelationshipYFormMappingsPopulator extends DefaultYFormMappingsPopulator
{

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPSRelationshipYFormMappingsPopulator.class);
	private static final String ADDRESS_BOOK_PERMISSIBLE_AREA = "Address";
	private UserService userService;
	private PSRelationshipFacade psRelationshipFacade;
	private PSPermissionFacade psPermissionFacade;

	@Override
	public void populate(final CustomerModel customerModel, final YFormMappingsData yFormMappingsData)
	{
		super.populate(customerModel, yFormMappingsData);
		try
		{
			final UserModel currentUser = getUserService().getCurrentUser();
			final boolean userInContextDifferentFromCurrentUser = !StringUtils.equalsIgnoreCase(currentUser.getUid(),
					customerModel.getUid());

			if (userInContextDifferentFromCurrentUser)
			{
				if (!getPsPermissionFacade().isPermitted(customerModel.getUid(), currentUser.getUid(), ADDRESS_BOOK_PERMISSIBLE_AREA))
				{
					LOGGER.debug("{} dont have permission to access {} address book", customerModel.getUid(), currentUser.getUid());
					yFormMappingsData.setAddressLine1(null);
					yFormMappingsData.setAddressLine2(null);
					yFormMappingsData.setCity(null);
					yFormMappingsData.setPostcode(null);
					yFormMappingsData.setPhone(null);
					yFormMappingsData.setCountry(null);
				}
				if (Boolean.TRUE.equals(customerModel.getIsMinor()))
				{
					LOGGER.debug("Given customer {} is minor, not populating the email", customerModel.getIsMinor());
					yFormMappingsData.setEmail(null);
				}
			}
		}
		catch (final RelationshipDoesNotExistException e)
		{
			LOGGER.error("Relationship does not exists {}", e);
		}
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected PSRelationshipFacade getPsRelationshipFacade()
	{
		return psRelationshipFacade;
	}

	@Required
	public void setPsRelationshipFacade(final PSRelationshipFacade psRelationshipFacade)
	{
		this.psRelationshipFacade = psRelationshipFacade;
	}

	protected PSPermissionFacade getPsPermissionFacade()
	{
		return psPermissionFacade;
	}

	@Required
	public void setPsPermissionFacade(final PSPermissionFacade psPermissionFacade)
	{
		this.psPermissionFacade = psPermissionFacade;
	}



}
