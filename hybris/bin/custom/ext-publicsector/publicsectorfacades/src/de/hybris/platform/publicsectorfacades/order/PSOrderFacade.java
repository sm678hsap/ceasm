/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order;

import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;

import java.util.List;


/**
 * PSOrderFacade
 */
public interface PSOrderFacade
{
	/**
	 * Returns the order history of the given user for given statuses.
	 *
	 * @param customerId
	 *           customer id
	 * @param statuses
	 *           array of order statuses to filter the results
	 * @return The order history of the current user.
	 */
	List<OrderHistoryData> getOrderHistoryForStatuses(final String customerId, final OrderStatus... statuses);

	/**
	 * Returns the order history of the given user for given statuses.
	 *
	 * @param customerId
	 *           customer id
	 * @param pageableData
	 *           paging information
	 * @param statuses
	 *           array of order statuses to filter the results
	 * @return The order history of the current user.
	 */
	SearchPageData<OrderHistoryData> getPagedOrderHistoryForStatuses(final String customerId, final PageableData pageableData,
			final OrderStatus... statuses);


}
