/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
/**
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators;

import de.hybris.platform.commercefacades.service.data.PSLinkData;
import de.hybris.platform.commercefacades.service.data.PSRelatedLinksData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSLinkModel;
import de.hybris.platform.publicsectorservices.model.PSRelatedLinksModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;


/**
 * PSRelatedLinksCMSPopulator
 *
 * returns the service related links data for the product
 */
public class PSRelatedLinksCMSPopulator<SOURCE extends PSRelatedLinksModel, TARGET extends PSRelatedLinksData>
		implements Populator<SOURCE, TARGET>
{

	private Converter<PSLinkModel, PSLinkData> linkConverter;

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param RelatedLinksModel
	 *           the source object
	 * @param ServiceRelatedLinksData
	 *           the target to fill
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		if (source != null)
		{
			target.setLinks(Converters.convertAll(source.getServiceLinks(), getLinkConverter()));
			target.setTitle(source.getTitle());
		}
	}

	protected Converter<PSLinkModel, PSLinkData> getLinkConverter()
	{
		return linkConverter;
	}

	@Required
	public void setLinkConverter(final Converter<PSLinkModel, PSLinkData> serviceLinkConverter)
	{
		this.linkConverter = serviceLinkConverter;
	}

}
