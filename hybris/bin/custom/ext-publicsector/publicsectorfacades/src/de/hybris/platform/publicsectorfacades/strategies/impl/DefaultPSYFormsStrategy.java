/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectorfacades.strategies.impl;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.publicsectorfacades.product.PSProductFacade;
import de.hybris.platform.publicsectorfacades.strategies.PSYFormsStrategy;
import de.hybris.platform.xyformscommercefacades.strategies.YFormsStrategy;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * This is the implementation class of {@link PSYFormsStrategy}
 */
public class DefaultPSYFormsStrategy implements PSYFormsStrategy
{
	private YFormsStrategy yFormsStrategy;
	private PSProductFacade psProductFacade;

	@Override
	public OrderEntryData getYFormOrderEntry(final AbstractOrderData orderData)
	{
		if (orderData != null)
		{
			final List<OrderEntryData> yFormOrderEntryDataList = getyFormsStrategy().getYFormOrderEntries(orderData);
			return yFormOrderEntryDataList.stream()
					.filter(orderEntryData -> getPsProductFacade().isServiceProduct(orderEntryData.getProduct().getCode())).findFirst()
					.orElse(null);
		}
		return null;
	}

	@Override
	public OrderEntryData getYFormOrderEntryByProduct(final AbstractOrderData orderData, final String productCode)
	{
		if (orderData != null)
		{
			final List<OrderEntryData> yFormOrderEntryDataList = getyFormsStrategy().getYFormOrderEntries(orderData);
			return yFormOrderEntryDataList.stream()
					.filter(orderEntryData -> productCode.equalsIgnoreCase(orderEntryData.getProduct().getCode())).findFirst()
					.orElse(null);
		}
		return null;
	}

	protected YFormsStrategy getyFormsStrategy()
	{
		return yFormsStrategy;
	}

	@Required
	public void setyFormsStrategy(final YFormsStrategy yFormsStrategy) // NOSONAR
	{
		this.yFormsStrategy = yFormsStrategy;
	}

	protected PSProductFacade getPsProductFacade()
	{
		return psProductFacade;
	}

	@Required
	public void setPsProductFacade(final PSProductFacade psProductFacade)
	{
		this.psProductFacade = psProductFacade;
	}
}
