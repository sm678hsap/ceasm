/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.bundle.selection.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.publicsectorfacades.bundle.selection.BundleSelectionFacade;
import de.hybris.platform.publicsectorfacades.strategies.impl.DefaultPSYFormsStrategy;
import de.hybris.platform.publicsectorservices.bundle.selection.BundleSelectionService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductAddOnModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * This is the implementation class of {@link BundleSelectionFacade}
 */
public class DefaultBundleSelectionFacade implements BundleSelectionFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultBundleSelectionFacade.class);
	private BundleSelectionService bundleSelectionService;
	private ProductService productService;
	private BundleTemplateService bundleTemplateService;
	private Converter<BundleTemplateModel, BundleTemplateData> psBundleTemplateConverter;
	private Converter<ProductModel, ProductData> productConverter;
	private DefaultPSYFormsStrategy psYFormsStrategy;

	@Override
	public String getBundleParentTemplateByProduct(final String productCode)
	{
		final BundleTemplateModel parentBundleTemplate = getBundleSelectionService().getParentBundleTemplateByProduct(productCode);
		if (parentBundleTemplate != null)
		{
			return parentBundleTemplate.getId();
		}
		return null;
	}

	@Override
	public Boolean hasBundleAddons(final String productCode)
	{
		validateParameterNotNull(productCode, "product code cannot be null");
		return getBundleSelectionService().hasBundleAddons(productCode);
	}

	@Override
	public List<BundleTemplateData> getChildrenTemplates(final String productCode)
	{
		final List<BundleTemplateData> childBundlesData = new ArrayList<>();
		final List<BundleTemplateModel> childBundlesModel = getBundleSelectionService().getChildBundleTemplates(productCode);
		final List<BundleTemplateModel> bundleWithDisabledProducts = getBundleSelectionService()
				.getBundleWithDisabledProducts(productCode);
		if (childBundlesModel != null && !childBundlesModel.isEmpty())
		{
			for (final BundleTemplateModel bundleModel : childBundlesModel)
			{
				if (!bundleWithDisabledProducts.contains(bundleModel) && (CollectionUtils.isNotEmpty(bundleModel.getProducts())
						&& bundleModel.getProducts().iterator().next() instanceof PSServiceProductAddOnModel))
				{
					final BundleTemplateData bundleData = new BundleTemplateData();
					getPsBundleTemplateConverter().convert(bundleModel, bundleData);
					childBundlesData.add(bundleData);
				}
			}
		}
		return childBundlesData;
	}

	@Override
	public BundleTemplateData getBundleTemplateByProductCode(final String productCode)
	{
		validateParameterNotNull(productCode, "Parameter productCode cannot be null.");

		final BundleTemplateData bundleData = new BundleTemplateData();
		final BundleTemplateModel bundleModel = getBundleSelectionService().getParentBundleTemplateByProduct(productCode);
		if (bundleModel != null)
		{
			getPsBundleTemplateConverter().convert(bundleModel, bundleData);
		}
		else
		{
			LOG.debug("No bundle template found for product : {}", productCode);
		}
		return bundleData;
	}

	@Override
	public BundleTemplateData getBundleTemplateByCode(final String bundleCode)
	{
		validateParameterNotNull(bundleCode, "bundle code cannot be null");

		final BundleTemplateModel bundleModel = getBundleTemplateService().getBundleTemplateForCode(bundleCode);
		final BundleTemplateData bundleData = new BundleTemplateData();
		if (bundleModel != null)
		{
			getPsBundleTemplateConverter().convert(bundleModel, bundleData);
		}
		return bundleData;
	}

	@Override
	public String getBundleTemplateStatusByCode(final String productCode)
	{
		final BundleTemplateModel bundleModel = getBundleSelectionService().getParentBundleTemplateByProduct(productCode);
		if (bundleModel != null)
		{
			return bundleModel.getStatus().getStatus().toString();
		}
		else
		{
			LOG.debug("No bundle template found for product {}", productCode);
		}
		return null;
	}

	@Override
	public List<ProductData> returnDisabledProducts(final String productCode)
	{
		final List<ProductModel> disabledProductModels = getBundleSelectionService().getDisabledProducts(productCode);
		final List<ProductData> disabledProductsData = new ArrayList<>();
		for (final ProductModel productModel : disabledProductModels)
		{
			disabledProductsData.add(getProductConverter().convert(productModel));
		}
		return disabledProductsData;
	}

	@Override
	public Map<ProductData, BundleTemplateData> getSelectedBundlesInformationForCart(final AbstractOrderData cart)
	{
		final OrderEntryData orderEntryData = getPsYFormsStrategy().getYFormOrderEntry(cart);

		final Map<ProductData, BundleTemplateData> bundleInformationMap = new LinkedHashMap<>();

		if (orderEntryData != null && orderEntryData.getProduct() != null
				&& StringUtils.isNotEmpty(orderEntryData.getProduct().getCode()))
		{
			final String serviceProductCode = orderEntryData.getProduct().getCode();
			LOG.debug("found the service product code {} for cart", serviceProductCode, cart.getCode());

			//select only cart products that are different from the service product code
			List<ProductData> selectedCartProducts = cart.getEntries().stream()
					.filter(c -> !serviceProductCode.equals(c.getProduct().getCode())).map(c -> c.getProduct()).collect(Collectors.toList());

			for (ProductData product : selectedCartProducts) {
				prepareProductToBundleMap( product , bundleInformationMap);
			}
		}

		return bundleInformationMap;
	}

	/**
	 * Retrieve bundle templates for the given product if the product is an instance of PSServiceProductAddOnModel
	 * Get the first bundle template data found for the product and add to the given map
	 *
	 * @param product given product (DTO) from cart
	 * @param bundleInformationMap map for product and bundle template data
	 */
	private void prepareProductToBundleMap( final ProductData product , final Map<ProductData, BundleTemplateData> bundleInformationMap) {

		if ( product != null && bundleInformationMap != null ) {

			final ProductModel productModel = getProductService().getProductForCode(product.getCode());

			if ( productModel instanceof PSServiceProductAddOnModel) {

				final List<BundleTemplateModel> bundleModel = getBundleTemplateService().getBundleTemplatesByProduct(productModel);

				if (CollectionUtils.isNotEmpty(bundleModel)) {
					final BundleTemplateData bundleData = new BundleTemplateData();
					getPsBundleTemplateConverter().convert(bundleModel.get(0), bundleData);
					bundleInformationMap.put(product, bundleData);
				}
			}
		}
	}

	@Override
	public List<Integer> getChildBundlesNotInCart(final String productCode)
	{
		final List<BundleTemplateModel> childBundlesNotInCart = getBundleSelectionService().getChildBundlesNotInCart(productCode);
		final List<BundleTemplateModel> childBundles = getBundleSelectionService().getChildBundleTemplates(productCode);
		final List<Integer> childBundlesNotInCartIndexes = new ArrayList<>();

		for (int i = 0; i < childBundles.size(); i++)
		{
			if (childBundlesNotInCart.contains(childBundles.get(i)))
			{
				childBundlesNotInCartIndexes.add(Integer.valueOf(i));
			}
		}
		return childBundlesNotInCartIndexes;
	}

	@Override
	public void cartCleanupBeforeAdd(final String productCode) throws CommerceCartModificationException
	{
		getBundleSelectionService().cartCleanupBeforeAdd(productCode);
	}

	@Override
	public boolean isEligibleToAddBundleAddon(final String productCode)
	{
		return getBundleSelectionService().isEligibleToAddBundleAddon(productCode);
	}

	protected BundleSelectionService getBundleSelectionService()
	{
		return bundleSelectionService;
	}

	@Required
	public void setBundleSelectionService(final BundleSelectionService bundleSelectionService)
	{
		this.bundleSelectionService = bundleSelectionService;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	@Required
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	protected Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	@Required
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	protected Converter<BundleTemplateModel, BundleTemplateData> getPsBundleTemplateConverter()
	{
		return psBundleTemplateConverter;
	}

	@Required
	public void setPsBundleTemplateConverter(final Converter<BundleTemplateModel, BundleTemplateData> psBundleTemplateConverter)
	{
		this.psBundleTemplateConverter = psBundleTemplateConverter;
	}

	protected DefaultPSYFormsStrategy getPsYFormsStrategy()
	{
		return psYFormsStrategy;
	}

	@Required
	public void setPsYFormsStrategy(final DefaultPSYFormsStrategy psYFormsStrategy)
	{
		this.psYFormsStrategy = psYFormsStrategy;
	}
}
