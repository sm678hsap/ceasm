/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order.converters.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.publicsectorservices.constants.PublicsectorservicesConstants;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.collections.CollectionUtils;


/**
 * The class of PSOrderPopulator.
 */
public class PSOrderPopulator implements Populator<OrderModel, OrderData>
{

	@Override
	public void populate(final OrderModel source, final OrderData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		checkForUnidentifiedCustomer(source, target);
		if (CollectionUtils.isNotEmpty(source.getPsDocument()))
		{
			target.setHasDocument(true);
		}

		if (notApplicableDeliveryModeExists(source))
		{
			target.setDeliveryMode(null);
		}

		if (notApplicableDeliveryAddressExists(source))
		{
			target.setDeliveryAddress(null);
		}

		if (notApplicablePaymentInfoExists(source))
		{
			target.setPaymentInfo(null);
		}
	}

	/**
	 * Method to check if delivery mode is not applicable for the order
	 *
	 * @param source
	 * @return true, if delivery mode's code is not-applicable
	 */
	private boolean notApplicableDeliveryModeExists(final OrderModel source)
	{
		final DeliveryModeModel deliveryMode = source.getDeliveryMode();
		return deliveryMode != null && PublicsectorservicesConstants.NOT_APPLICABLE.equals(deliveryMode.getCode());
	}

	/**
	 * Method to check if payment info is not applicable for the order
	 *
	 * @param source
	 * @return true, if not applicable for the order
	 */
	private boolean notApplicablePaymentInfoExists(final OrderModel source)
	{
		final PaymentInfoModel paymentInfo = source.getPaymentInfo();
		return paymentInfo != null && PublicsectorservicesConstants.NOT_APPLICABLE.equals(paymentInfo.getCode());
	}

	/**
	 * Method to check if order contains not applicable delivery address
	 *
	 * @param source
	 * @return true, if not applicable delivery address exists
	 */
	private boolean notApplicableDeliveryAddressExists(final OrderModel source)
	{
		final AddressModel deliveryAddress = source.getDeliveryAddress();
		if (null != deliveryAddress)
		{
			final CountryModel countryModel = deliveryAddress.getCountry();
			return null != countryModel &&
					PublicsectorservicesConstants.NOT_APPLICABLE.equals(deliveryAddress.getEmail())
					&& PublicsectorservicesConstants.DEFAULT_COUNTRY_CODE.equals(countryModel.getIsocode());
		}
		return false;
	}

	protected void checkForUnidentifiedCustomer(final OrderModel source, final OrderData target)
	{
		if (CustomerType.UNIDENTIFIED.equals(((CustomerModel) source.getUser()).getType()))
		{
			target.setUnidentifiedCustomer(true);
		}
	}


}
