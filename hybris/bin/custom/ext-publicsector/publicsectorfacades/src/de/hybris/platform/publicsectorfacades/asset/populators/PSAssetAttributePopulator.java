/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.asset.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;


/**
 * Populates PSAssetAttributeData {@link PSAssetAttributePopulator}
 */
public class PSAssetAttributePopulator implements Populator<PSAssetAttributeModel, PSAssetAttributeData>
{

	@Override
	public void populate(final PSAssetAttributeModel source, final PSAssetAttributeData target)
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setAssetCode(source.getAssetCode());
		target.setName(source.getName());
		target.setValue(source.getValue());
	}

}
