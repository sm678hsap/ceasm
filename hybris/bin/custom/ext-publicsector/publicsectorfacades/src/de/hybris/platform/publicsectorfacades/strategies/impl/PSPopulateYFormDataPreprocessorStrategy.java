/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectorfacades.strategies.impl;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.xyformscommercefacades.form.data.FormDetailData;
import de.hybris.platform.xyformscommercefacades.strategies.impl.PopulateYFormDataPreprocessorStrategy;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import de.hybris.platform.xyformsservices.enums.YFormDataActionEnum;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * PS implementation of PopulateYFormDataPreprocessorStrategy. Added to cover changes required for the relationship's
 * Minor user logic.
 */
public class PSPopulateYFormDataPreprocessorStrategy extends PopulateYFormDataPreprocessorStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(PSPopulateYFormDataPreprocessorStrategy.class);

	/**
	 * Applies the actual transformation to a formData
	 *
	 * @param xmlContent
	 * @param params
	 * @throws de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException
	 */
	@SuppressWarnings("boxing")
	@Override
	protected String transform(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		final String xmlString = super.transform(xmlContent, params);

		if (!validation(params))
		{
			return xmlString;
		}

		if (YFormDataActionEnum.VIEW.equals(params.get(PSPopulateYFormDataPreprocessorStrategy.FORM_ACTION)))
		{
			LOG.debug("yform data action is view ");
			return xmlString;
		}

		final CustomerModel customerModel = getUserStrategy().getCurrentUserForCheckout();

		if (!CustomerType.REGISTERED.equals(customerModel.getType()) && !customerModel.getIsMinor())
		{
			LOG.debug("Customer type for customer {} ", customerModel.getType());
			return xmlString;
		}

		final FormDetailData data = (FormDetailData) params.get(PSPopulateYFormDataPreprocessorStrategy.FORM_DETAIL_DATA);

		try
		{
			final Map<String, String> dataMap = prepareYFormData(data.getApplicationId(), data.getFormId(), customerModel);

			if (dataMap != null)
			{
				params.putAll(dataMap);
			}

		}
		catch (final IllegalAccessException | YFormServiceException e)
		{
			LOG.error(e.getMessage(), e);
		}

		return updateXmlContent(xmlString, params);
	}
}
