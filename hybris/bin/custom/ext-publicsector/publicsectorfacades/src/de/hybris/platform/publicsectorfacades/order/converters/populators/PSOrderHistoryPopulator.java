/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectorservices.constants.PublicsectorservicesConstants;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Converter implementation for {@link de.hybris.platform.core.model.order.OrderModel} as source and
 * {@link de.hybris.platform.commercefacades.order.data.OrderHistoryData} as target type.
 */
public class PSOrderHistoryPopulator implements Populator<OrderModel, OrderHistoryData>
{

	private Converter<UserModel, CustomerData> customerConverter;

	@Override
	public void populate(final OrderModel source, final OrderHistoryData target)
	{
		if (source.getDeliveryMode() != null
				&& !StringUtils.equals(source.getDeliveryMode().getCode(), PublicsectorservicesConstants.NOT_APPLICABLE))
		{
			target.setDeliveryMode(source.getDeliveryMode());
		}

		if (CollectionUtils.isNotEmpty(source.getPsDocument()))
		{
			target.setHasDocument(true);
		}


		source.getEntries().stream().filter(e -> e.getProduct() instanceof PSServiceProductModel).findFirst()
				.ifPresent(e -> target.setServiceRequest(e.getProduct().getName()));

		if (source.getPlacedBy() != null)
		{
			target.setPlacedBy(getCustomerConverter().convert(source.getPlacedBy()));
		}
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}
}
