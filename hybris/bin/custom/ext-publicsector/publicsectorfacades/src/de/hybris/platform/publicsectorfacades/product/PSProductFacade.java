/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.product;

import de.hybris.platform.commercefacades.product.data.ProductData;


/**
 * This will serve as the interface for Publicsector product services.
 */
public interface PSProductFacade
{

	/**
	 * Returns true if Unidentified user in service product
	 *
	 * @param code
	 *           product code
	 * @return boolean
	 */
	boolean isUnidentifiedUserInService(String code);

	/**
	 * Returns true if Guest user in service product
	 *
	 * @param code
	 *           product code
	 * @return boolean
	 */
	boolean isGuestUserInService(String code);

	/**
	 * Returns true if registered user in service product
	 *
	 * @param code
	 *           product code
	 * @return boolean
	 */
	boolean isRegisteredUserInService(String code);

	/**
	 * Returns true if Product is an instance of ServiceProductModel
	 *
	 * @param code
	 *           product code
	 * @return boolean
	 */
	boolean isServiceProduct(String code);

	/**
	 * Returns the ServiceProduct from the current session cart.
	 *
	 * @return ProductData
	 */
	ProductData getServiceProductFromSessionCart();

	/**
	 * Returns the ServiceProduct from the order with given orderCode.
	 *
	 * @param orderCode
	 *           order code
	 * @return ProductData
	 */
	ProductData getServiceProductForOrderCode(String orderCode);

}
