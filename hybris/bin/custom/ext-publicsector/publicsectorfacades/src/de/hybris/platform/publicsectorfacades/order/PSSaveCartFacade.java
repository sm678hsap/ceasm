/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;

import java.util.List;


/**
 * Save Cart facade interface. Service is responsible for saved cart related functionality such as saving a cart,
 * retrieving saved cart(s), restoring a saved cart etc.
 */
public interface PSSaveCartFacade
{

	/**
	 * Get the saved carts for given user
	 *
	 * @param pageableData
	 *           PageableData data object for page details
	 * @param orderStatus
	 *           List<OrderStatus> of status applicable to orders
	 * @param customerId
	 *           customer id
	 * @return the number of saved user carts
	 */

	SearchPageData<CartData> getSavedCartsForGivenUser(final PageableData pageableData, final List<OrderStatus> orderStatus,
			final String customerId);
}
