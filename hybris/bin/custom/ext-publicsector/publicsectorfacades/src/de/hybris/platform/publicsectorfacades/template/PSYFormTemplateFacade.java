/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.template;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.xyformsfacades.data.YFormDataData;

import java.util.List;


/**
 * PublicSectorTemplateFacade interface
 *
 */
public interface PSYFormTemplateFacade
{
	/**
	 * Checks if this service has template
	 *
	 * @param productCode
	 *           product code
	 * @return true/false
	 */
	boolean isServiceHasTemplate(String productCode);

	/**
	 * Returns the template html for the given service
	 *
	 * @param product
	 *           ProductData data object for product details
	 * @param yFormData
	 *           List<YFormDataData> of yformData
	 * @returns string
	 */
	String getTemplateHTMLForService(ProductData product, List<YFormDataData> yFormData);

	/**
	 * Return template html if template is present or else return readonly yform data html for all the YForms.
	 *
	 * @param orderData
	 *           AbstractOrderData data object for order data details
	 * @return List<String>
	 */

	List<String> getYFormHTMLForService(AbstractOrderData orderData);
}
