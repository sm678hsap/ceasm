/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order.impl;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartParameterData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.publicsectorfacades.order.PSCartFacade;
import de.hybris.platform.publicsectorfacades.strategies.PSYFormsStrategy;
import de.hybris.platform.publicsectorservices.order.PSCartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Implementation of public sector cart facade
 */
public class DefaultPSCartFacade implements PSCartFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSCartFacade.class);
	private static final String DRAFT_LOCK_PERIOD = "draft.locked.period";

	public static final String SESSION_CART_PARAMETER_NAME = "cart";
	public static final long SECOND_MILLIS = 1000;
	public static final long MINUTE_MILLIS = SECOND_MILLIS * 60;

	private ModelService modelService;
	private CommonI18NService commonI18NService;
	private SessionService sessionService;
	private SaveCartFacade saveCartFacade;
	private FlexibleSearchService flexibleSearchService;
	private YFormFacade yFormFacade;
	private CartService cartService;
	private BaseSiteService baseSiteService;
	private UserService userService;
	private CommerceCartService commerceCartService;
	private Converter<CommerceCartRestoration, CartRestorationData> cartRestorationConverter;
	private Converter<CartModel, CartData> cartConverter;
	private CartFacade cartFacade;
	private PSCartService psCartService;
	private TimeService timeService;
	private PSYFormsStrategy psYFormsStrategy;
	private CommerceSaveCartService commerceSaveCartService;
	private ConfigurationService configurationService;


	/**
	 * Sets returnURL and lastAccessDate in cart model for draft carts
	 */
	@Override
	public void setDraftCartDetails(final String returnURL)
	{
		getPsCartService().setDraftCartDetails(returnURL);
	}

	@Override
	public AddressModel getNewBillingAddress()
	{
		return getModelService().create(AddressModel.class);
	}

	@Override
	public CountryModel getI18NCountry(final String billToCountry)
	{
		return getCommonI18NService().getCountry(billToCountry);
	}

	@Override
	public RegionModel getI18NRegion(final CountryModel country, final String billToState)
	{
		return getCommonI18NService().getRegion(country, country.getIsocode() + "-" + billToState);
	}

	@Override
	public TitleModel getTitleForCode(final String code)
	{
		return getUserService().getTitleForCode(code);
	}

	@Override
	public void saveCreditCardDetails(final CreditCardPaymentInfoModel cardPaymentInfoModel, final AddressModel billingAddress)
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			billingAddress.setOwner(sessionCart);
			getModelService().save(billingAddress);
			getModelService().refresh(billingAddress);
		}
		else
		{
			LOG.warn("No session cart exists. Unable to save the creditcard payment info");
		}
	}

	@Override
	public void savePaymentDetailsToCart(final AddressModel billingAddress)
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			sessionCart.setPaymentAddress(billingAddress);
			getModelService().save(sessionCart);
		}
		else
		{
			LOG.warn("No session cart exists. Unable to save the  payment info");
		}
	}

	@Override
	public void removeExistingSessionCart()
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			if (sessionCart != null && sessionCart.getReturnURL() == null)
			{
				getCartService().removeSessionCart();
			}
			else
			{
				getSessionService().removeAttribute(SESSION_CART_PARAMETER_NAME);
			}
		}
		else
		{
			LOG.debug("No session cart exists. Unable to save the  payment info");
		}
	}

	/**
	 * Get saved carts for the current user.
	 */
	@Override
	public List<CartData> getSavedCartsForCurrentUser()
	{
		/*
		 * This is temporary code to get the list of Saved Carts. It will be removed as part of HPS-658.
		 */
		final CartModel cartModel = new CartModel();
		cartModel.setSite(getBaseSiteService().getCurrentBaseSite());
		cartModel.setUser(getUserService().getCurrentUser());
		final List<CartModel> savedCartModels = getFlexibleSearchService().getModelsByExample(cartModel);
		return Converters.convertAll(savedCartModels, getCartConverter());
	}

	/**
	 * Restore cart with code for current user and the site.
	 */
	@Override
	public CartData restoreCart(final String code) throws CommerceSaveCartException
	{
		restoreSavedCartForCode(code);
		return getCartFacade().getSessionCart();
	}

	/**
	 * returns whether the given cart is associated with current user
	 *
	 * @param code
	 * @return boolean
	 */
	@Override
	public boolean isCartAssociatedWithCurrentUser(final String code)
	{
		return getPsCartService().isCartAssociatedWithUser(code, getUserService().getCurrentUser());
	}

	/**
	 * @param code
	 * @throws CommerceSaveCartException
	 */
	protected void restoreSavedCartForCode(final String code) throws CommerceSaveCartException
	{
		final CommerceSaveCartParameterData saveCartParameterData = new CommerceSaveCartParameterData();
		saveCartParameterData.setCartId(code);
		getSaveCartFacade().restoreSavedCart(saveCartParameterData);
	}

	/**
	 * To verify if the YForm version has been updated for the saved YFormData.
	 *
	 * @return boolean
	 */
	@Override
	public boolean isYFormVersionUpdatedForCart()
	{
		boolean isYFormVersionUpdated = false;
		if (getCartFacade().hasSessionCart())
		{
			final CartData cartData = getCartFacade().getSessionCart();

			final OrderEntryData orderEntryData = getPsYFormsStrategy().getYFormOrderEntry(cartData);
			if (orderEntryData != null)
			{
				final List<YFormDataData> formDataData = (List<YFormDataData>) orderEntryData.getFormDataData();
				isYFormVersionUpdated = compareYFormVersions(formDataData);
			}
		}
		return isYFormVersionUpdated;
	}

	/**
	 * To update cart on YForm version changed for the saved YFormData.
	 */
	@Override
	public void updateCartOnYFormVersionUpdate()
	{
		getPsCartService().updateCartOnYFormVersionUpdate();
	}

	/**
	 * restores cart if cart and associated guest user email address matches with given email address
	 *
	 * @param draftNumber
	 * @param emailAddress
	 * @return CartData
	 * @throws CommerceCartRestorationException
	 */
	@Override
	public CartData restoreCartForDraftNumberAndEmailAddress(final String draftNumber, final String emailAddress)
			throws CommerceCartRestorationException
	{
		final CartModel cart = getPsCartService().getCartForGuestUserEmailAndCartCode(draftNumber, emailAddress);
		if (cart != null)
		{
			restoreAnonymousCartAndTakeOwnership(cart);
			return getCartFacade().getSessionCart();
		}
		else
		{
			LOG.debug("Cart not found for the draft number {} and email address {}", draftNumber, emailAddress);
		}
		return null;
	}

	@Override
	public CartRestorationData restoreAnonymousCartAndTakeOwnership(final CartModel cart) throws CommerceCartRestorationException
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cart);
		parameter.setUser(cart.getUser());
		final CommerceCartRestoration commerceCartRestoration = getCommerceCartService().restoreCart(parameter);
		getCartService().changeCurrentCartUser(getUserService().getUserForUID(cart.getUser().getUid()));
		getCommonI18NService().setCurrentCurrency(cart.getCurrency());
		return getCartRestorationConverter().convert(commerceCartRestoration);
	}

	/**
	 * Remove cart with code for current user and the site.
	 *
	 * @param cartCode
	 */
	@Override
	public void removeCart(final String cartCode)
	{
		getPsCartService().removeCart(cartCode, getUserService().getCurrentUser());
	}

	/**
	 * Remove cart with code for current user and the site.
	 *
	 * @param cartCode
	 */
	@Override
	public void removeCartForUser(final String customerId, final String cartCode)
	{
		getPsCartService().removeCart(cartCode, getUserService().getUserForUID(customerId));
	}

	@Override
	public void updateLastAccessTimeInCartForCurrentUser(final String code)
	{
		final CartModel cartForCurrentUserWithCode = getCommerceCartService().getCartForCodeAndUser(code,
				getUserService().getCurrentUser());
		updateLastAccessTimeInCart(cartForCurrentUserWithCode);
	}

	@Override
	public void updateReturnURLAndLastAccessTimeInCartForCurrentUser(final String code, final String returnURL)
	{
		final CartModel cartModel = getCommerceCartService().getCartForCodeAndUser(code, getUserService().getCurrentUser());
		if (cartModel != null)
		{
			cartModel.setReturnURL(returnURL);
			cartModel.setLastAccessDate(getTimeService().getCurrentTime());
			getModelService().save(cartModel);
		}
		else
		{
			LOG.debug("Cart not found for the cart {} ", code);
		}
	}

	@Override
	public void updateLastAccessTimeInCartForGuestUser(final String code, final String guestEmailAddress)
	{
		final CartModel cartForGuestUserWithCode = getPsCartService().getCartForGuestUserEmailAndCartCode(code, guestEmailAddress);
		updateLastAccessTimeInCart(cartForGuestUserWithCode);
	}

	/**
	 * Returns whether the given cart is a saved draft or not
	 *
	 * @return boolean
	 */
	@Override
	public boolean isCurrentCartSavedDraft()
	{
		if (getCartService().hasSessionCart())
		{
			return getPsCartService().isCartSavedDraft(getCartService().getSessionCart());
		}
		return false;
	}

	@Override
	public boolean isCustomerProfileUpdatedAfterDraftSaved()
	{
		boolean isCustomerProfileUpdated = false;
		if (getCartFacade().hasSessionCart())
		{
			final CartData cartData = getCartFacade().getSessionCart();
			final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
			final Date customerProfileUpdateTime = customerModel.getProfileUpdateTime();
			final Date draftLastAccessTime = cartData.getLastAccessTime();

			if (customerProfileUpdateTime != null && draftLastAccessTime != null
					&& customerProfileUpdateTime.after(draftLastAccessTime))
			{
				LOG.info("Customer has updated the profile after draft last access time");
				isCustomerProfileUpdated = true;
			}
		}
		return isCustomerProfileUpdated;
	}

	/**
	 * Method to update last access time in cart
	 *
	 * @param cart
	 */
	private void updateLastAccessTimeInCart(final CartModel cart)
	{
		if (cart != null)
		{
			cart.setLastAccessDate(getTimeService().getCurrentTime());
			getModelService().save(cart);
		}
	}

	private boolean compareYFormVersions(final List<YFormDataData> formDataData)
	{
		boolean isYFormVersionsEqual = false;
		try
		{
			if (CollectionUtils.isNotEmpty(formDataData))
			{
				for (final YFormDataData formData : formDataData)
				{
					final YFormDefinitionData yForm = formData.getFormDefinition();
					final int oldFormVersion = yForm.getVersion();
					final YFormDefinitionData latestYForm = getYFormFacade().getYFormDefinition(yForm.getApplicationId(),
							yForm.getFormId());
					final int currentFormVersion = latestYForm.getVersion();
					isYFormVersionsEqual = oldFormVersion != currentFormVersion ? true : false;
				}
			}
		}
		catch (final YFormServiceException e)
		{
			LOG.error("Error occurred while getting YForm : ", e);
		}
		return isYFormVersionsEqual;
	}

	@Override
	public CartData getCartForCodeAndUser(final String code, final String customerId) throws CommerceSaveCartException
	{
		final UserModel user = userService.getUserForUID(customerId);
		final CommerceSaveCartParameter parameter = new CommerceSaveCartParameter();
		final CartModel cartForCodeAndUser = getCommerceCartService().getCartForCodeAndUser(code, user);

		if (null == cartForCodeAndUser)
		{
			throw new CommerceSaveCartException("Cannot find a cart for code [" + code + "]");
		}

		parameter.setCart(cartForCodeAndUser);
		parameter.setEnableHooks(false);

		getCartRestorationConverter().convert(getCommerceSaveCartService().restoreSavedCart(parameter));
		return getCartFacade().getSessionCart();
	}

	@Override
	public CartData setCurrentUserOwnerForRelationshipCart()
	{
		if (getCartService().hasSessionCart()
				&& !getCartService().getSessionCart().getUser().equals(getUserService().getCurrentUser()))
		{
			final CartModel sessionCart = getCartService().getSessionCart();

			sessionCart.setUserInContext(sessionCart.getUser());
			sessionCart.setUser(getUserService().getCurrentUser());
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);
		}
		return getCartFacade().getSessionCart();
	}

	@Override
	public void updateUserInCartWithContextUser()
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel cartModel = getCartService().getSessionCart();
			final UserModel user = cartModel.getUserInContext();
			if (user != null && !cartModel.getUser().getUid().equals(user.getUid()))
			{
				cartModel.setUser(user);
				getModelService().save(cartModel);
				getModelService().refresh(cartModel);
			}
		}
	}

	@Override
	public void rebuildSessionCartFromCurrentCart() throws CommerceCartModificationException
	{
		final OrderEntryData serviceProductOrderEntry = getPsYFormsStrategy().getYFormOrderEntry(getCartFacade().getSessionCart());
		if (serviceProductOrderEntry != null)
		{
			final String productCode = serviceProductOrderEntry.getProduct().getCode();
			getSessionService().removeAttribute(SESSION_CART_PARAMETER_NAME);
			getCartFacade().addToCart(productCode, 1);
		}
	}

	@Override
	public CartData setLockOnDraft()
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			sessionCart.setLockedBy(getUserService().getCurrentUser());
			getModelService().save(sessionCart);
			getModelService().refresh(sessionCart);
			LOG.info("Updating locked by with current user {} in cart {}", getUserService().getCurrentUser().getUid(),
					sessionCart.getCode());
		}
		return getCartFacade().getSessionCart();
	}

	@Override
	public boolean isDraftLocked(final String draftCode)
	{
		final UserModel loggedInUser = getUserService().getCurrentUser();
		final CartModel cart = getCartByCode(draftCode);

		// If its first attempt by someone
		if (cart == null || cart.getLockedBy() == null || loggedInUser.getUid().equalsIgnoreCase(cart.getLockedBy().getUid()))
		{
			return false;
		}
		final Date currentDate = getTimeService().getCurrentTime();
		final Date elapsedTime = getCartLockedTime(cart.getModifiedtime());

		return currentDate.before(elapsedTime);
	}

	private Date getCartLockedTime(final Date lastModifiedTime)
	{
		final int lockedSeconds = getConfigurationService().getConfiguration().getInt(DRAFT_LOCK_PERIOD, 3600);
		return DateUtils.addSeconds(lastModifiedTime, lockedSeconds);
	}

	@Override
	public Long getDifferenceInMinutes(final String draftCode)
	{
		final CartModel cart = getCartByCode(draftCode);
		if (cart != null)
		{
			final long cartlockedFor = getCartLockedTime(cart.getModifiedtime()).getTime();
			return Long.valueOf((cartlockedFor / MINUTE_MILLIS) - (new Date().getTime() / MINUTE_MILLIS));
		}
		return Long.valueOf(0);
	}

	private CartModel getCartByCode(final String draftCode)
	{
		final CartModel cartModel = new CartModel();
		cartModel.setCode(draftCode);
		final CartModel cartModelByExample = getFlexibleSearchService().getModelByExample(cartModel);
		return cartModelByExample;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected SaveCartFacade getSaveCartFacade()
	{
		return saveCartFacade;
	}

	@Required
	public void setSaveCartFacade(final SaveCartFacade saveCartFacade)
	{
		this.saveCartFacade = saveCartFacade;
	}

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	protected YFormFacade getYFormFacade()
	{
		return yFormFacade;
	}

	@Required
	public void setYFormFacade(final YFormFacade yFormFacade)
	{
		this.yFormFacade = yFormFacade;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	@Required
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	protected Converter<CommerceCartRestoration, CartRestorationData> getCartRestorationConverter()
	{
		return cartRestorationConverter;
	}

	@Required
	public void setCartRestorationConverter(final Converter<CommerceCartRestoration, CartRestorationData> cartRestorationConverter)
	{
		this.cartRestorationConverter = cartRestorationConverter;
	}

	protected Converter<CartModel, CartData> getCartConverter()
	{
		return cartConverter;
	}

	@Required
	public void setCartConverter(final Converter<CartModel, CartData> cartConverter)
	{
		this.cartConverter = cartConverter;
	}

	protected CartFacade getCartFacade()
	{
		return cartFacade;
	}

	@Required
	public void setCartFacade(final CartFacade cartFacade)
	{
		this.cartFacade = cartFacade;
	}

	protected PSCartService getPsCartService()
	{
		return psCartService;
	}

	@Required
	public void setPsCartService(final PSCartService psCartService)
	{
		this.psCartService = psCartService;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected PSYFormsStrategy getPsYFormsStrategy()
	{
		return psYFormsStrategy;
	}

	@Required
	public void setPsYFormsStrategy(final PSYFormsStrategy psYFormsStrategy)
	{
		this.psYFormsStrategy = psYFormsStrategy;
	}

	protected CommerceSaveCartService getCommerceSaveCartService()
	{
		return commerceSaveCartService;
	}

	@Required
	public void setCommerceSaveCartService(final CommerceSaveCartService commerceSaveCartService)
	{
		this.commerceSaveCartService = commerceSaveCartService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
