/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order.impl;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.publicsectorfacades.order.PSSaveCartFacade;

import java.util.List;


/**
 * Default implementation of the interface {@link PSSaveCartFacade}
 */
public class DefaultPSSaveCartFacade extends DefaultPSCartFacade implements PSSaveCartFacade
{
	@Override
	public SearchPageData<CartData> getSavedCartsForGivenUser(final PageableData pageableData, final List<OrderStatus> orderStatus,
			final String customerId)
	{
		final SearchPageData<CartData> result = new SearchPageData<>();
		final SearchPageData<CartModel> savedCartModels = getPsCartService().getSavedDraftsForSiteAndUser(pageableData,
				getBaseSiteService().getCurrentBaseSite(), getUserService().getUserForUID(customerId), orderStatus);

		result.setPagination(savedCartModels.getPagination());
		result.setSorts(savedCartModels.getSorts());

		final List<CartData> savedCartDatas = Converters.convertAll(savedCartModels.getResults(), getCartConverter());
		result.setResults(savedCartDatas);

		return result;
	}
}
