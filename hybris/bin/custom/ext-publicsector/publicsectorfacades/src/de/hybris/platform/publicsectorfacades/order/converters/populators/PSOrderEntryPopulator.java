/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;

import reactor.util.Assert;


/**
 * The class of PSOrderEntryPopulator.
 */
public class PSOrderEntryPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source != null && source.getProduct() instanceof PSServiceProductModel)
		{
			target.setIsServiceRequest(true);
		}

	}

}
