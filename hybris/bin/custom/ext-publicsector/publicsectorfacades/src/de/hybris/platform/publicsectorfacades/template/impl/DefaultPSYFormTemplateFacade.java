/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.template.impl;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.publicsectorfacades.strategies.PSYFormsStrategy;
import de.hybris.platform.publicsectorfacades.template.PSYFormTemplateFacade;
import de.hybris.platform.publicservices.template.PSYFormTemplateService;
import de.hybris.platform.xyformscommercefacades.strategies.YFormsStrategy;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsservices.enums.YFormDataActionEnum;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * DefaultPSYFormTemplateFacade
 *
 */
public class DefaultPSYFormTemplateFacade implements PSYFormTemplateFacade
{
	private PSYFormTemplateService psYFormTemplateService;
	private YFormsStrategy yFormsStrategy;
	private PSYFormsStrategy psYFormsStrategy;

	/**
	 * Returns the template html for the given service
	 *
	 * @param product
	 * @param yFormData
	 * @returns string
	 */
	@Override
	public String getTemplateHTMLForService(final ProductData product, final List<YFormDataData> yFormData)
	{
		return getPsYFormTemplateService().getTemplateHTMLForService(product, yFormData);
	}

	/**
	 * Checks if this service has template
	 *
	 * @param productCode
	 * @return true/false
	 */
	@Override
	public boolean isServiceHasTemplate(final String productCode)
	{
		return getPsYFormTemplateService().isServiceHasTemplate(productCode);
	}

	/**
	 * Return template html if template is present or else return readonly yform data html
	 *
	 * @param orderData
	 * @return List<String>
	 */
	@Override
	public List<String> getYFormHTMLForService(final AbstractOrderData orderData)
	{
		List<String> yFormHTML = new ArrayList<>();
		if (orderData != null)
		{
			for (final OrderEntryData orderEntryData : orderData.getEntries())
			{
				final String templateHTML = getTemplateHTML(orderEntryData, orderData);
				if (StringUtils.isNotEmpty(templateHTML))
				{
					yFormHTML.add(templateHTML);
				}
			}

			if (yFormHTML.isEmpty())
			{
				yFormHTML = getyFormsStrategy().getFormsInlineHtml(YFormDataActionEnum.VIEW, orderData);
			}
		}

		return yFormHTML;
	}

	protected String getTemplateHTML(final OrderEntryData orderEntryData, final AbstractOrderData orderData)
	{
		String templateHTML = null;
		if (orderEntryData != null)
		{
			final ProductData product = orderEntryData.getProduct();
			final OrderEntryData yFormOrderEntry = getPsYFormsStrategy().getYFormOrderEntryByProduct(orderData, product.getCode());

			if (yFormOrderEntry != null && isServiceHasTemplate(yFormOrderEntry.getProduct().getCode()))
			{
				final List<YFormDataData> yFormDataList = (List<YFormDataData>) yFormOrderEntry.getFormDataData();
				if (CollectionUtils.isNotEmpty(yFormDataList))
				{
					templateHTML = getTemplateHTMLForService(product, yFormDataList);
				}
			}
		}
		return templateHTML;
	}

	protected PSYFormsStrategy getPsYFormsStrategy()
	{
		return psYFormsStrategy;
	}

	@Required
	public void setPsYFormsStrategy(final PSYFormsStrategy psYFormsStrategy)
	{
		this.psYFormsStrategy = psYFormsStrategy;
	}

	protected YFormsStrategy getyFormsStrategy()
	{
		return yFormsStrategy;
	}

	@Required
	public void setyFormsStrategy(final YFormsStrategy yFormsStrategy) // NOSONAR
	{
		this.yFormsStrategy = yFormsStrategy;
	}

	protected PSYFormTemplateService getPsYFormTemplateService()
	{
		return psYFormTemplateService;
	}

	@Required
	public void setPsYFormTemplateService(final PSYFormTemplateService psYFormTemplateService)
	{
		this.psYFormTemplateService = psYFormTemplateService;
	}
}
