/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.product.converters.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.service.data.AbstractPSServiceApplicationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.publicsectorservices.model.PSProductInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Populate DTO {@link AbstractPSServiceApplicationData} with data from {@link PSProductInfoModel}.
 *
 * @param <SOURCE>
 *           source class
 * @param <TARGET>
 *           target class
 */
public class AbstractPSServiceApplicationPopulator<SOURCE extends PSProductInfoModel, TARGET extends AbstractPSServiceApplicationData>
		implements Populator<SOURCE, TARGET>
{

	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		validateParameterNotNull(target, "Parameter target cannot be null");

		if (source != null)
		{
			target.setDescription(source.getDescription());
			target.setTitle(source.getTitle());
		}
	}
}
