/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectorfacades.asset.impl;

import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeData;
import de.hybris.platform.commercefacades.asset.data.PSAssetAttributeMappingData;
import de.hybris.platform.commercefacades.asset.data.PSAssetData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.publicsectorfacades.asset.PSAssetFacade;
import de.hybris.platform.publicsectorservices.asset.PSAssetService;
import de.hybris.platform.publicsectorservices.asset.PSAssetTemplateService;
import de.hybris.platform.publicsectorservices.asset.exception.PSAssetNotFoundException;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeMappingModel;
import de.hybris.platform.publicsectorservices.model.PSAssetAttributeModel;
import de.hybris.platform.publicsectorservices.model.PSAssetModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;



/**
 * Implementation of public sector asset facade
 */
public class DefaultPSAssetFacade implements PSAssetFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSAssetFacade.class);

	private PSAssetService assetService;
	private Converter<PSAssetModel, PSAssetData> assetConverter;
	private Converter<PSAssetAttributeModel, PSAssetAttributeData> assetAttributeConverter;
	private Converter<PSAssetAttributeMappingModel, PSAssetAttributeMappingData> assetAttributeMappingConverter;
	private UserService userService;
	private PSAssetTemplateService assetTemplateService;

	/**
	 * Sets returnURL and lastAccessDate in cart model for draft carts
	 */
	@Override
	public List<PSAssetData> getAssetsForUser(final String userId)
	{

		if (!StringUtils.isEmpty(userId))
		{
			final List<PSAssetModel> assetModels = getAssetService().getAssetsForUser(getUserService().getUserForUID(userId));
			return Converters.convertAll(assetModels, getAssetConverter());
		}

		return Collections.emptyList();
	}

	@Override
	public PSAssetData getAssetDetailsByCode(final String assetCode)
	{
		PSAssetData assetData = null;
		try
		{
			final PSAssetModel assetModel = getAssetService().getAssetByCode(assetCode);
			assetData = getAssetConverter().convert(assetModel);
		}
		catch (final PSAssetNotFoundException e)
		{
			LOG.error("No asset details found for assetCode: {}", assetCode, e);
		}
		return assetData;
	}

	@Override
	public String getAssetAttributeDetails(final String assetCode, final String assetType)
	{
		final List<PSAssetAttributeModel> assetAttributes = getAssetService().getAssetAttributesByCode(assetCode);

		final List<PSAssetAttributeData> assetAttributesData = getAssetAttributeConverter().convertAll(assetAttributes);

		final List<String> assetAttributeNames = assetAttributesData.stream().map(PSAssetAttributeData::getName)
				.collect(Collectors.toList());

		final List<PSAssetAttributeMappingModel> assetAttributeMappings = getAssetService().getAssetAttributesMapping(assetType,
				assetAttributeNames);

		final List<PSAssetAttributeMappingData> assetAttributeMappingsData = getAssetAttributeMappingConverter()
				.convertAll(assetAttributeMappings);

		final String assetAttributesTemplate = getAssetTemplateService().generateAssetAttributesTemplate(assetType,
				assetAttributesData, assetAttributeMappingsData);

		return assetAttributesTemplate;
	}

	protected PSAssetService getAssetService()
	{
		return assetService;
	}

	@Required
	public void setAssetService(final PSAssetService assetService)
	{
		this.assetService = assetService;
	}


	protected Converter<PSAssetModel, PSAssetData> getAssetConverter()
	{
		return assetConverter;
	}

	@Required
	public void setAssetAttributeConverter(final Converter<PSAssetAttributeModel, PSAssetAttributeData> assetAttributeConverter)
	{
		this.assetAttributeConverter = assetAttributeConverter;
	}

	public Converter<PSAssetAttributeModel, PSAssetAttributeData> getAssetAttributeConverter()
	{
		return assetAttributeConverter;
	}

	@Required
	public void setAssetConverter(final Converter<PSAssetModel, PSAssetData> assetConverter)
	{
		this.assetConverter = assetConverter;
	}

	public Converter<PSAssetAttributeMappingModel, PSAssetAttributeMappingData> getAssetAttributeMappingConverter()
	{
		return assetAttributeMappingConverter;
	}

	@Required
	public void setAssetAttributeMappingConverter(
			final Converter<PSAssetAttributeMappingModel, PSAssetAttributeMappingData> assetAttributeMappingConverter)
	{
		this.assetAttributeMappingConverter = assetAttributeMappingConverter;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public PSAssetTemplateService getAssetTemplateService()
	{
		return assetTemplateService;
	}

	@Required
	public void setAssetTemplateService(final PSAssetTemplateService assetTemplateService)
	{
		this.assetTemplateService = assetTemplateService;
	}

}
