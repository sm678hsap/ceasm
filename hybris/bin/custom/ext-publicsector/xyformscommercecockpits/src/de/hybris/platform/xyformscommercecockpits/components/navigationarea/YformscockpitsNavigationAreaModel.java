/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package de.hybris.platform.xyformscommercecockpits.components.navigationarea;

import de.hybris.platform.cockpit.components.navigationarea.DefaultNavigationAreaModel;
import de.hybris.platform.cockpit.session.impl.AbstractUINavigationArea;

import de.hybris.platform.xyformscommercecockpits.session.impl.YformscockpitsNavigationArea;


/**
 * Yformscockpits navigation area model.
 */
public class YformscockpitsNavigationAreaModel extends DefaultNavigationAreaModel
{
	public YformscockpitsNavigationAreaModel()
	{
		super();
	}

	public YformscockpitsNavigationAreaModel(final AbstractUINavigationArea area)
	{
		super(area);
	}

	@Override
	public YformscockpitsNavigationArea getNavigationArea()
	{
		return (YformscockpitsNavigationArea) super.getNavigationArea();
	}
}
