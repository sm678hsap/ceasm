/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommercefacades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.xyformscommercefacades.data.YFormDefinitionFieldMappingsData;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;


/**
 * The class of OrderEntryFormDataPopulator.
 */
public class DefaultYFormDefinitionFieldMappingsPopulator
		implements Populator<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData>
{

	@Override
	public void populate(final YFormDefinitionFieldMappingsModel mappingsModel,
			final YFormDefinitionFieldMappingsData mappingsData)
	{
		validateParameterNotNull(mappingsModel, "YFormDefinitionFieldMappingsModel cannot be nulll");
		validateParameterNotNull(mappingsData, "YFormDefinitionFieldMappingsData cannot be null");

		mappingsData.setApplicationId(mappingsModel.getApplicationId());
		mappingsData.setYFormId(mappingsModel.getFormId());
		mappingsData.setYFormFieldMappings(mappingsModel.getFormToUserMappings());

	}

}
