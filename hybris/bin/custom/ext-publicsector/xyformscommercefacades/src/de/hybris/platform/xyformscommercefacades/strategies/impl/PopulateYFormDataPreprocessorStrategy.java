/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.strategies.impl;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.xyformscommercefacades.data.YFormDefinitionFieldMappingsData;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;
import de.hybris.platform.xyformscommercefacades.form.data.FormDetailData;
import de.hybris.platform.xyformscommercefacades.form.impl.DefaultXYFormFacade;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;
import de.hybris.platform.xyformscommerceservices.service.YFormDefinitionFieldMappingsService;
import de.hybris.platform.xyformscommerceservices.strategies.YFormUserStrategy;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import de.hybris.platform.xyformsservices.enums.YFormDataActionEnum;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.form.YFormService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * The class of PopulateYFormDataPreprocessorStrategy.
 */
public class PopulateYFormDataPreprocessorStrategy extends DefaultYFormDataPreprocessorStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(PopulateYFormDataPreprocessorStrategy.class);

	public static final String FORM_DETAIL_DATA = "formDetailData";

	private UserService userService;
	private Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> yformDefinitionFieldsConverter;
	private Converter<CustomerModel, YFormMappingsData> yFormMappingsConverter;
	private YFormService yformService;
	private DefaultXYFormFacade defaultXYFormFacade;
	private YFormDefinitionFieldMappingsService yFormDefinitionFieldMappingsService;
	private YFormUserStrategy userStrategy;

	/**
	 * Applies the actual transformation to a formData
	 *
	 * @param xmlContent
	 * @param params
	 * @throws de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException
	 */
	@Override
	protected String transform(final String xmlContent, final Map<String, Object> params) throws YFormProcessorException
	{
		final String xmlString = super.transform(xmlContent, params);

		if (!validation(params))
		{
			return xmlString;
		}

		if (YFormDataActionEnum.VIEW.equals(params.get(PopulateYFormDataPreprocessorStrategy.FORM_ACTION)))
		{
			return xmlString;
		}

		final CustomerModel customerModel = getUserStrategy().getCurrentUserForCheckout();

		if (!CustomerType.REGISTERED.equals(customerModel.getType()))
		{
			return xmlString;
		}

		final FormDetailData data = (FormDetailData) params.get(PopulateYFormDataPreprocessorStrategy.FORM_DETAIL_DATA);

		try
		{
			final Map<String, String> dataMap = prepareYFormData(data.getApplicationId(), data.getFormId(), customerModel);

			if (dataMap != null)
			{
				params.putAll(dataMap);
			}

		}
		catch (final IllegalAccessException | YFormServiceException e)
		{
			LOG.error(e.getMessage(), e);
		}

		return updateXmlContent(xmlString, params);
	}

	protected boolean validation(final Map<String, Object> params)
	{
		return params.containsKey(FORM_DETAIL_DATA) && params.get(FORM_DETAIL_DATA) instanceof FormDetailData;
	}

	/**
	 * Prepares the yform mappings data
	 *
	 * @param applicationId
	 * @param yFormId
	 * @param customerModel
	 * @throws YFormServiceException,
	 *            IllegalAccessException
	 */
	protected Map<String, String> prepareYFormData(final String applicationId, final String yFormId,
			final CustomerModel customerModel) throws YFormServiceException, IllegalAccessException
	{

		Map<String, String> yFormPrefilData = null;

		final YFormMappingsData yFormMappingsData = getyFormMappingsConverter().convert(customerModel);

		final Map<String, String> yformMappingsData = getDefaultXYFormFacade().getMappingsData(yFormMappingsData);

		final YFormDefinitionFieldMappingsModel yFormDefinitionFieldMappingsModel = getyFormDefinitionFieldMappingsService()
				.getMappingsForYForm(applicationId, yFormId);

		if (yFormDefinitionFieldMappingsModel != null)
		{
			final YFormDefinitionFieldMappingsData yFormDefinitionFieldMappingsData = getYformDefinitionFieldsConverter()
					.convert(yFormDefinitionFieldMappingsModel);

			if (!yFormDefinitionFieldMappingsData.getYFormFieldMappings().isEmpty())
			{

				yFormPrefilData = new HashMap<>();

				final Iterator it = yFormDefinitionFieldMappingsData.getYFormFieldMappings().entrySet().iterator();
				while (it.hasNext())
				{
					final Map.Entry yFormCurrentFieldMapping = (Map.Entry) it.next();
					yFormPrefilData.put(yFormCurrentFieldMapping.getKey().toString(),
							yformMappingsData.get(yFormCurrentFieldMapping.getValue()));
				}

			}
		}

		return yFormPrefilData;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected YFormService getYformService()
	{
		return yformService;
	}

	@Required
	public void setYformService(final YFormService yformService)
	{
		this.yformService = yformService;
	}

	protected Converter<CustomerModel, YFormMappingsData> getyFormMappingsConverter()
	{
		return yFormMappingsConverter;
	}

	@Required
	public void setyFormMappingsConverter(final Converter<CustomerModel, YFormMappingsData> yFormMappingsConverter)
	{
		this.yFormMappingsConverter = yFormMappingsConverter;
	}

	protected DefaultXYFormFacade getDefaultXYFormFacade()
	{
		return defaultXYFormFacade;
	}

	@Required
	public void setDefaultXYFormFacade(final DefaultXYFormFacade defaultXYFormFacade)
	{
		this.defaultXYFormFacade = defaultXYFormFacade;
	}

	protected YFormDefinitionFieldMappingsService getyFormDefinitionFieldMappingsService()
	{
		return yFormDefinitionFieldMappingsService;
	}

	@Required
	public void setyFormDefinitionFieldMappingsService(
			final YFormDefinitionFieldMappingsService yFormDefinitionFieldMappingsService)
	{
		this.yFormDefinitionFieldMappingsService = yFormDefinitionFieldMappingsService;
	}

	protected Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> getYformDefinitionFieldsConverter()
	{
		return yformDefinitionFieldsConverter;
	}

	@Required
	public void setYformDefinitionFieldsConverter(
			final Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> yformDefinitionFieldsConverter)
	{
		this.yformDefinitionFieldsConverter = yformDefinitionFieldsConverter;
	}

	protected YFormUserStrategy getUserStrategy()
	{
		return userStrategy;
	}

	@Required
	public void setUserStrategy(final YFormUserStrategy userStrategy)
	{
		this.userStrategy = userStrategy;
	}

}
