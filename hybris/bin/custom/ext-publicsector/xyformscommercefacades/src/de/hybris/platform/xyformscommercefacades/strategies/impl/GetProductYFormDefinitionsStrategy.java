/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.strategies.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.strategy.GetYFormDefinitionsForItemStrategy;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.model.YFormDefinitionModel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Strategy used to get yForm Definitions associated to a Product.
 */
public class GetProductYFormDefinitionsStrategy implements GetYFormDefinitionsForItemStrategy<String>
{
	private ProductService productService;

	private Converter<YFormDefinitionModel, YFormDefinitionData> yformDefinitionConverter;

	/**
	 * Returns a List of YFormDefinitionData that relate to the product
	 *
	 * @return List<YFormDefinitionData>
	 * @throws YFormServiceException
	 */
	@Override
	public List<YFormDefinitionData> execute(final String code) throws YFormServiceException
	{
		final List<YFormDefinitionData> yFormDefinitionDataList = new ArrayList<YFormDefinitionData>();

		ProductModel product = null;
		try
		{
			product = getProductService().getProductForCode(code);
		}
		catch (final ModelNotFoundException e)
		{
			throw new YFormServiceException(e);
		}
		catch (final UnknownIdentifierException e)
		{
			throw new YFormServiceException(e);
		}


		final List<YFormDefinitionModel> allYFormDefinitions = new LinkedList<>();
		allYFormDefinitions.addAll(product.getAllYFormDefinitionList());
		if (CollectionUtils.isNotEmpty(allYFormDefinitions))
		{
			for (final YFormDefinitionModel yFormDefinitionModel : allYFormDefinitions)
			{
				yFormDefinitionDataList.add(getYformDefinitionConverter().convert(yFormDefinitionModel));
			}
		}

		return CollectionUtils.isNotEmpty(yFormDefinitionDataList) ? yFormDefinitionDataList : null;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected Converter<YFormDefinitionModel, YFormDefinitionData> getYformDefinitionConverter()
	{
		return yformDefinitionConverter;
	}

	@Required
	public void setYformDefinitionConverter(final Converter<YFormDefinitionModel, YFormDefinitionData> yformDefinitionConverter)
	{
		this.yformDefinitionConverter = yformDefinitionConverter;
	}

}
