/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.util;

import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformscommercefacades.form.data.FormDetailData;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Lists;


/**
 * DefaultYFormsHelper
 */
public class DefaultYFormsHelper implements YFormsHelper
{

	private YFormFacade yFormFacade;

	/**
	 * Builds up a refId for YFormData
	 *
	 * @param orderCode
	 * @param orderEntryNumber
	 * @return String YFormData refId
	 */
	@Override
	public String buildYFormDataRefId(final String orderCode, final Integer orderEntryNumber)
	{
		final StringBuilder refIdBuilder = new StringBuilder();

		refIdBuilder.append(orderCode);
		refIdBuilder.append('_');
		refIdBuilder.append(orderEntryNumber);

		return refIdBuilder.toString();
	}

	/**
	 * Transforms the inputs into a FormDetailData object
	 *
	 * @param yFormDefinitionData
	 * @param orderCode
	 * @param orderEntryNumber
	 * @return FormDetailData
	 */
	@Override
	public FormDetailData createFormDetailData(final YFormDefinitionData yFormDefinitionData, final String orderCode,
			final Integer orderEntryNumber)
	{
		final FormDetailData formDetailData = new FormDetailData();
		formDetailData.setApplicationId(yFormDefinitionData.getApplicationId());
		formDetailData.setFormId(yFormDefinitionData.getFormId());
		formDetailData.setRefId(buildYFormDataRefId(orderCode, orderEntryNumber));
		formDetailData.setOrderEntryNumber(orderEntryNumber);
		return formDetailData;
	}

	/**
	 * Transforms the inputs into a FormDetailData objects
	 *
	 * @param yFormDefinitionDataList
	 * @param orderCode
	 * @param orderEntryNumber
	 * @return List<FormDetailData>
	 */
	@Override
	public List<FormDetailData> createFormDetailData(final List<YFormDefinitionData> yFormDefinitionDataList,
			final String orderCode, final Integer orderEntryNumber)
	{
		final List<FormDetailData> formDetailDataList = Lists.newArrayList();
		if (CollectionUtils.isNotEmpty(yFormDefinitionDataList))
		{
			for (final YFormDefinitionData yFormDefinitionData : yFormDefinitionDataList)
			{
				formDetailDataList.add(createFormDetailData(yFormDefinitionData, orderCode, orderEntryNumber));
			}
		}

		return formDetailDataList;
	}

	protected YFormFacade getYFormFacade()
	{
		return yFormFacade;
	}

	@Required
	public void setyFormFacade(final YFormFacade yFormFacade)
	{
		this.yFormFacade = yFormFacade;
	}

}
