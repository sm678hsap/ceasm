/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.strategies.impl;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.xyformscommercefacades.form.data.FormDetailData;
import de.hybris.platform.xyformscommercefacades.strategies.YFormsStrategy;
import de.hybris.platform.xyformscommercefacades.util.YFormsHelper;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.EmptyYFormPreprocessorStrategy;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.ReferenceIdTransformerYFormPreprocessorStrategy;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormPreprocessorStrategy;
import de.hybris.platform.xyformsservices.enums.YFormDataActionEnum;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * DefaultYFormsStrategy
 */
/**
 *
 */
public class DefaultYFormsStrategy implements YFormsStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultYFormsStrategy.class);

	private YFormFacade yFormFacade;
	private YFormsHelper yFormsHelper;
	private EmptyYFormPreprocessorStrategy emptyYFormPreprocessorStrategy;
	private DefaultYFormDataPreprocessorStrategy referenceIdTransformerYFormPreprocessorStrategy;
	private GetProductYFormDefinitionsStrategy getProductYFormDefinitionsStrategy;

	/**
	 * Method to return List<OrderEntryData> for which YFormDefinitions have been assigned from orderData.
	 *
	 * @param orderData
	 * @return List<OrderEntryData>
	 */
	@Override
	public List<OrderEntryData> getYFormOrderEntries(final AbstractOrderData orderData)
	{
		final List<OrderEntryData> yFormOrderEntryData = new ArrayList<>();
		if (orderData.getEntries() != null)
		{
			for (final OrderEntryData orderEntryData : orderData.getEntries())
			{
				if (CollectionUtils.isNotEmpty(getFormDefinitionsForOrderEntry(orderEntryData)))
				{
					yFormOrderEntryData.add(orderEntryData);
				}
			}
		}
		return yFormOrderEntryData;
	}

	/**
	 * Get List<YFormDefinitionData> by OrderEntryData. Returns a List of YFormDefinitionData that belongs to the product
	 * held by the OrderEntryData.
	 *
	 * @param orderEntryData
	 * @return List<YFormDefinitionData>
	 */
	@Override
	public List<YFormDefinitionData> getFormDefinitionsForOrderEntry(final OrderEntryData orderEntryData)
	{
		List<YFormDefinitionData> yFormDefinitions = null;

		final String productCode = orderEntryData.getProduct().getCode();
		try
		{
			yFormDefinitions = getGetProductYFormDefinitionsStrategy().execute(productCode);
		}
		catch (final YFormServiceException e)
		{
			LOG.error("Could not retrieve YFormDefinitions for Product[code=\"" + productCode + "\"]", e);
		}
		return yFormDefinitions;
	}

	/**
	 * Get List<FormDetailData> For orderData/cartData. <br/>
	 *
	 * @param orderData
	 * @return List<FormDetailData>
	 */
	@Override
	public List<FormDetailData> getFormDetailDataForOrder(final AbstractOrderData orderData)
	{
		final List<FormDetailData> formDetailDataList = new ArrayList<>();
		for (final OrderEntryData orderEntryData : orderData.getEntries())
		{
			if (CollectionUtils.isNotEmpty(getFormDefinitionsForOrderEntry(orderEntryData)))
			{
				formDetailDataList.addAll(getFormDetailDataForOrderEntry(orderData, orderEntryData));
			}
		}
		return formDetailDataList;
	}

	/**
	 * Get List<FormDetailData> For OrderEntry.
	 *
	 * @param orderData
	 * @param orderEntryData
	 * @return List<FormDetailData>
	 */
	@Override
	public List<FormDetailData> getFormDetailDataForOrderEntry(final AbstractOrderData orderData,
			final OrderEntryData orderEntryData)
	{
		final List<YFormDefinitionData> yFormDefinitions = getFormDefinitionsForOrderEntry(orderEntryData);
		return getyFormsHelper().createFormDetailData(yFormDefinitions, orderData.getCode(), orderEntryData.getEntryNumber());
	}

	/**
	 * Get all the YForms HTML as a List<String> for orderData/cartData. <br/>
	 *
	 * @param formMode
	 * @param orderData
	 * @return List<String>
	 */
	@Override
	public List<String> getFormsInlineHtml(final YFormDataActionEnum formMode, final AbstractOrderData orderData)
	{
		final List<FormDetailData> forms = getFormDetailDataForOrder(orderData);
		return getYFormHtmlListForFormDataList(formMode, forms);
	}

	/**
	 * Get all the YForms HTML as a List<String> for CartEntry/OrderEntry. <br/>
	 *
	 * @param formMode
	 * @param orderData
	 * @param orderEntryData
	 * @return List<String>
	 */
	@Override
	public List<String> getFormsInlineHtmlForOrderEntry(final YFormDataActionEnum formMode, final AbstractOrderData orderData,
			final OrderEntryData orderEntryData)
	{
		final List<FormDetailData> forms = getFormDetailDataForOrderEntry(orderData, orderEntryData);
		return getYFormHtmlListForFormDataList(formMode, forms);
	}

	/**
	 * Get all the YForms HTML as a List<String> for FormDetailData & read mode. <br/>
	 *
	 * @param formMode
	 * @param forms
	 * @return List<String>
	 */
	protected List<String> getYFormHtmlListForFormDataList(final YFormDataActionEnum formMode, final List<FormDetailData> forms)
	{
		final List<String> formsHtml = new ArrayList<>();
		for (final FormDetailData data : forms)
		{
			String embeddedFormHtml;

			// This strategy does not apply any execute any extra code.
			YFormDataActionEnum action = formMode;
			String formDataId = null;
			final Map<String, Object> params = new HashMap<>();

			YFormDataData yFormData = null;
			try
			{
				yFormData = getYFormFacade().getYFormData(data.getApplicationId(), data.getFormId(), data.getRefId());
				formDataId = yFormData.getId();
			}
			catch (final YFormServiceException e)
			{

				action = YFormDataActionEnum.NEW;
				formDataId = getYFormFacade().getNewFormDataId();
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Exception occurred while getting the YFormData. : " + e);
				}
			}

			try
			{
				final YFormPreprocessorStrategy strategy = referenceIdTransformerYFormPreprocessorStrategy;
				params.put(DefaultYFormDataPreprocessorStrategy.FORM_ACTION, action);
				params.put(DefaultYFormDataPreprocessorStrategy.FORM_DETAIL_DATA, data);
				params.put(ReferenceIdTransformerYFormPreprocessorStrategy.REFERENCE_ID, data.getRefId());
				embeddedFormHtml = getYFormFacade().getInlineFormHtml(data.getApplicationId(), data.getFormId(), action, formDataId,
						strategy, params);
			}
			catch (final YFormServiceException e)
			{
				embeddedFormHtml = "";
				LOG.error(e.getMessage(), e);
			}
			formsHtml.add(embeddedFormHtml);
		}
		return formsHtml;
	}

	protected YFormFacade getYFormFacade()
	{
		return yFormFacade;
	}

	@Required
	public void setYFormFacade(final YFormFacade yFormFacade)
	{
		this.yFormFacade = yFormFacade;
	}

	protected EmptyYFormPreprocessorStrategy getEmptyYFormPreprocessorStrategy()
	{
		return emptyYFormPreprocessorStrategy;
	}

	@Required
	public void setEmptyYFormPreprocessorStrategy(final EmptyYFormPreprocessorStrategy emptyYFormPreprocessorStrategy)
	{
		this.emptyYFormPreprocessorStrategy = emptyYFormPreprocessorStrategy;
	}

	protected DefaultYFormDataPreprocessorStrategy getReferenceIdTransformerYFormPreprocessorStrategy()
	{
		return referenceIdTransformerYFormPreprocessorStrategy;
	}

	@Required
	public void setReferenceIdTransformerYFormPreprocessorStrategy(
			final DefaultYFormDataPreprocessorStrategy referenceIdTransformerYFormPreprocessorStrategy)
	{
		this.referenceIdTransformerYFormPreprocessorStrategy = referenceIdTransformerYFormPreprocessorStrategy;
	}

	protected YFormsHelper getyFormsHelper()
	{
		return yFormsHelper;
	}

	@Required
	public void setyFormsHelper(final YFormsHelper yFormsHelper)
	{
		this.yFormsHelper = yFormsHelper;
	}

	protected GetProductYFormDefinitionsStrategy getGetProductYFormDefinitionsStrategy()
	{
		return getProductYFormDefinitionsStrategy;
	}

	@Required
	public void setGetProductYFormDefinitionsStrategy(final GetProductYFormDefinitionsStrategy getProductYFormDefinitionsStrategy)
	{
		this.getProductYFormDefinitionsStrategy = getProductYFormDefinitionsStrategy;
	}

}
