/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 */
package de.hybris.platform.xyformscommercefacades.form;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;

import java.util.List;
import java.util.Map;


/**
 * Default Form Facade to handle yForm definitions and yForm data.
 */
public interface XYFormFacade
{

	/**
	 * Method to get yForm mappings data where key is column name and value is column data
	 *
	 * @param mappingData
	 *            YFormMappingsData data object for yform mapping details
	 * @return Map<String, String>
	 * @throws IllegalAccessException 
	 *            	if this {@code Field} object is enforcing Java language access control and the underlying field is inaccessible.
	 */
	Map<String, String> getMappingsData(final YFormMappingsData mappingData) throws IllegalAccessException;

	/**
	 * Method to get the fields list of YFormMappingsData
	 *
	 * @param mappingData
	 * @return List<String>
	 */
	List<String> getMappingFields() throws IllegalAccessException;

	/**
	 * returns default address for user in context if present else default return current user default address
	 *
	 * @param currentCustomer
	 *           Model object for current customer
	 * @return AddresssData
	 */
	AddressData getDefaultAddressForUser(CustomerModel currentCustomer);
}
