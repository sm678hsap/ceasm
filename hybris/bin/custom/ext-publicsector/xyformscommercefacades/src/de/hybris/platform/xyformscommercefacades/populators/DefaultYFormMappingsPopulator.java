/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommercefacades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;
import de.hybris.platform.xyformscommercefacades.form.XYFormFacade;

import org.springframework.beans.factory.annotation.Required;


/**
 * Class DefaultYFormMappingsPopulator to populate YFormMappingsData from CustomerModel
 */
public class DefaultYFormMappingsPopulator implements Populator<CustomerModel, YFormMappingsData>
{

	private XYFormFacade xyFormFacade;
	private UserFacade userFacade;

	@Override
	public void populate(final CustomerModel customerModel, final YFormMappingsData yFormMappingsData)
	{
		validateParameterNotNull(customerModel, "CustomerModel cannot be nulll");
		validateParameterNotNull(yFormMappingsData, "YFormMappingsDTO cannot be null");

		yFormMappingsData.setEmail(customerModel.getContactEmail());
		yFormMappingsData.setFullName(customerModel.getName());
		if (customerModel.getName() != null)
		{
			final String[] name = customerModel.getName().split(" ");
			if (name != null)
			{
				setCustomerNames(yFormMappingsData, name);
			}
		}

		if (customerModel.getTitle() != null)
		{
			yFormMappingsData.setTitle(customerModel.getTitle().getCode());
		}

		final AddressData address = getXyFormFacade().getDefaultAddressForUser(customerModel);
		if (address != null)
		{
			yFormMappingsData.setAddressLine1(address.getLine1());
			yFormMappingsData.setAddressLine2(address.getLine2());
			yFormMappingsData.setCity(address.getTown());
			yFormMappingsData.setPostcode(address.getPostalCode());
			yFormMappingsData.setPhone(address.getPhone());

			if (address.getCountry() != null)
			{
				yFormMappingsData.setCountry(address.getCountry().getIsocode());
			}
		}
		yFormMappingsData.setIsUserLoggedIn(!userFacade.isAnonymousUser());

	}

	private void setCustomerNames(final YFormMappingsData yFormMappingsData, final String[] name)
	{
		if (name.length > 1)
		{
			yFormMappingsData.setFirstName(name[0]);
			for (int i = 1; i <= name.length - 2; i++)
			{
				yFormMappingsData.setFirstName(yFormMappingsData.getFirstName() + " " + name[i]);
			}
			yFormMappingsData.setLastName(name[name.length - 1]);
		}
		else if (name.length == 1)
		{
			yFormMappingsData.setFirstName(name[0]);
		}
	}

	protected XYFormFacade getXyFormFacade()
	{
		return xyFormFacade;
	}

	@Required
	public void setXyFormFacade(final XYFormFacade xyFormFacade)
	{
		this.xyFormFacade = xyFormFacade;
	}

	protected UserFacade getUserFacade()
	{
		return userFacade;
	}

	@Required
	public void setUserFacade(final UserFacade userFacade)
	{
		this.userFacade = userFacade;
	}

}
