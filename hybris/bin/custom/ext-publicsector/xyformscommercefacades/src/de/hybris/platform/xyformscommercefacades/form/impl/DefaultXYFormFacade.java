/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 */
package de.hybris.platform.xyformscommercefacades.form.impl;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;
import de.hybris.platform.xyformscommercefacades.form.XYFormFacade;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Default Form Facade to handle xyForm definitions and yForm data.
 */
public class DefaultXYFormFacade implements XYFormFacade
{

	private CustomerAccountService customerAccountService;
	private Converter<AddressModel, AddressData> addressConverter;

	@Override
	public Map<String, String> getMappingsData(final YFormMappingsData mappingData) throws IllegalAccessException
	{

		final Field[] fields = mappingData.getClass().getDeclaredFields();

		final Map<String, String> dataMap = new HashMap<>();

		for (final Field field : fields)
		{
			field.setAccessible(setAccessSpecifier());
			if (field.get(mappingData) != null)
			{
				dataMap.put(field.getName(), field.get(mappingData).toString());
			}
			else
			{
				dataMap.put(field.getName(), "");
			}
		}

		return dataMap;
	}

	private boolean setAccessSpecifier()
	{
		return true;
	}

	@Override
	public List<String> getMappingFields() throws IllegalAccessException
	{

		final Field[] fields = YFormMappingsData.class.getDeclaredFields();

		final List<String> fieldsList = new ArrayList<>();

		for (final Field field : fields)
		{
			field.setAccessible(setAccessSpecifier());
			fieldsList.add(field.getName());
		}

		return fieldsList;
	}

	@Override
	public AddressData getDefaultAddressForUser(final CustomerModel currentCustomer)
	{
		AddressData defaultAddressData = null;

		final AddressModel defaultAddress = getCustomerAccountService().getDefaultAddress(currentCustomer);
		if (defaultAddress != null)
		{
			defaultAddressData = getAddressConverter().convert(defaultAddress);
		}
		else
		{
			final List<AddressModel> addresses = getCustomerAccountService().getAddressBookEntries(currentCustomer);
			if (CollectionUtils.isNotEmpty(addresses))
			{
				defaultAddressData = getAddressConverter().convert(addresses.get(0));
			}
		}
		return defaultAddressData;
	}

	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	protected Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}
}
