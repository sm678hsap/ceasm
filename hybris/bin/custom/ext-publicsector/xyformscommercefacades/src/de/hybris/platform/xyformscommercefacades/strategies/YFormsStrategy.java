/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.strategies;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsservices.enums.YFormDataActionEnum;
import de.hybris.platform.xyformscommercefacades.form.data.FormDetailData;

import java.util.List;


/**
 * YFormsStrategy
 */
public interface YFormsStrategy
{

	/**
	 * Method to return List<OrderEntryData> for which YFormDefinitions have been assigned from orderData.
	 *
	 * @param orderData
	 * @return List<OrderEntryData>
	 */
	List<OrderEntryData> getYFormOrderEntries(AbstractOrderData orderData);

	/**
	 * Get List<YFormDefinitionData> by OrderEntryData. Returns a List of YFormDefinitionData that belongs to the product
	 * held by the OrderEntryData.
	 *
	 * @param orderEntryData
	 * @return List<YFormDefinitionData>
	 */
	List<YFormDefinitionData> getFormDefinitionsForOrderEntry(final OrderEntryData orderEntryData);

	/**
	 * Get List<FormDetailData> For orderData/cartData. <br/>
	 *
	 * @param orderData
	 * @return List<FormDetailData>
	 */
	List<FormDetailData> getFormDetailDataForOrder(AbstractOrderData orderData);

	/**
	 * Get List<FormDetailData> For OrderEntry.
	 *
	 * @param orderData
	 * @param orderEntryData
	 * @return List<FormDetailData>
	 */
	List<FormDetailData> getFormDetailDataForOrderEntry(final AbstractOrderData orderData, OrderEntryData orderEntryData);

	/**
	 * Get all the YForms HTML as a List<String> for orderData/cartData. <br/>
	 *
	 * @param formMode
	 * @param orderData
	 * @return List<String>
	 */
	List<String> getFormsInlineHtml(YFormDataActionEnum formMode, AbstractOrderData orderData);

	/**
	 * Get all the YForms HTML as a List<String> for CartEntry/OrderEntry. <br/>
	 *
	 * @param formMode
	 * @param orderData
	 * @param orderEntryData
	 * @return List<String>
	 */
	List<String> getFormsInlineHtmlForOrderEntry(final YFormDataActionEnum formMode, AbstractOrderData orderData,
			final OrderEntryData orderEntryData);

}
