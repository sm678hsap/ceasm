/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsservices.model.YFormDefinitionModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Populates {@link ProductData}
 */
public class YFormsProductPopulator implements Populator<ProductModel, ProductData>
{
	private Converter<YFormDefinitionModel, YFormDefinitionData> yformDefinitionConverter;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		if (source.getAllYFormDefinitionList() != null && CollectionUtils.isNotEmpty(source.getAllYFormDefinitionList()))
		{
			final List<YFormDefinitionData> yFormDefinitionDataList = new ArrayList<YFormDefinitionData>();
			for (final YFormDefinitionModel yFormDefinitionModel : source.getAllYFormDefinitionList())
			{
				yFormDefinitionDataList.add(getYformDefinitionConverter().convert(yFormDefinitionModel));
			}
			target.setAllYFormDefinitionList(yFormDefinitionDataList);
		}
	}

	protected Converter<YFormDefinitionModel, YFormDefinitionData> getYformDefinitionConverter()
	{
		return yformDefinitionConverter;
	}

	@Required
	public void setYformDefinitionConverter(final Converter<YFormDefinitionModel, YFormDefinitionData> yformDefinitionConverter)
	{
		this.yformDefinitionConverter = yformDefinitionConverter;
	}

}
