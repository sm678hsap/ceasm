/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommercefacades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.xyformscommercefacades.strategies.impl.GetProductYFormDefinitionsStrategy;
import de.hybris.platform.xyformscommercefacades.util.YFormsHelper;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformsservices.enums.YFormDataTypeEnum;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Lists;


/**
 * The class of OrderEntryFormDataPopulator.
 */
public class OrderEntryFormDataPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	private static final Logger LOG = LoggerFactory.getLogger(OrderEntryFormDataPopulator.class);

	private GetProductYFormDefinitionsStrategy getProductYFormDefinitionsStrategy;
	private YFormsHelper yFormsHelper;
	private YFormFacade yFormFacade;

	@Override
	public void populate(final AbstractOrderEntryModel orderEntryModel, final OrderEntryData orderEntryData)
	{
		validateParameterNotNull(orderEntryModel, "OrderEntryModel cannot be nulll");
		validateParameterNotNull(orderEntryData, "OrderEntryData cannot be null");

		final List<YFormDataData> yFormDataDatas = Lists.newArrayList();

		final boolean isValidOrderEntry = orderEntryModel.getOrder() != null && orderEntryModel.getOrder().getCode() != null
				&& orderEntryData.getEntryNumber() != null;

		if (isValidOrderEntry)
		{
			try
			{
				final List<YFormDefinitionData> yFormDefinitions = getGetProductYFormDefinitionsStrategy()
						.execute(orderEntryData.getProduct().getCode());

				if (CollectionUtils.isNotEmpty(yFormDefinitions))
				{
					addAllOrderEntriesYformDataToList(orderEntryModel, orderEntryData, yFormDataDatas, yFormDefinitions);
				}
			}
			catch (final YFormServiceException e)
			{
				LOG.error(e.getMessage(), e);
			}
		}

		if (CollectionUtils.isNotEmpty(yFormDataDatas))
		{
			orderEntryData.setFormDataData(yFormDataDatas);
		}
	}

	/**
	 * adds all yform definition data to list
	 */
	private void addAllOrderEntriesYformDataToList(final AbstractOrderEntryModel orderEntryModel,
			final OrderEntryData orderEntryData, final List<YFormDataData> yFormDataDatas,
			final List<YFormDefinitionData> yFormDefinitions) throws YFormServiceException
	{
		for (final YFormDefinitionData yFormDefinition : yFormDefinitions)
		{
			final YFormDataData yFormData = getyFormFacade().getYFormData(yFormDefinition.getApplicationId(),
					yFormDefinition.getFormId(),
					getyFormsHelper().buildYFormDataRefId(orderEntryModel.getOrder().getCode(), orderEntryData.getEntryNumber()),
					YFormDataTypeEnum.DATA);

			addYFormDataToList(yFormDataDatas, yFormData);
		}
	}

	/**
	 * only add the yFormDraftData into the formDataData list if it is exists.
	 */
	private void addYFormDataToList(final List<YFormDataData> yFormDataDatas, final YFormDataData yFormData)
	{
		if (yFormData != null)
		{
			yFormDataDatas.add(yFormData);
			final YFormDataData yFormDraftData = retrieveDraftYFormData(yFormData);
			if (yFormDraftData != null)
			{

				yFormDataDatas.add(yFormDraftData);
			}
		}
	}

	private YFormDataData retrieveDraftYFormData(final YFormDataData yFormData)
	{
		YFormDataData yFormDraftData = null;
		try
		{
			yFormDraftData = getyFormFacade().getYFormData(yFormData.getId(), YFormDataTypeEnum.DRAFT);
		}
		catch (final YFormServiceException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("YFormServiceException occurred retrieving draft for id: {}", yFormData.getId(), e);
			}
		}
		return yFormDraftData;
	}

	protected GetProductYFormDefinitionsStrategy getGetProductYFormDefinitionsStrategy()
	{
		return getProductYFormDefinitionsStrategy;
	}

	@Required
	public void setGetProductYFormDefinitionsStrategy(final GetProductYFormDefinitionsStrategy getProductYFormDefinitionsStrategy)
	{
		this.getProductYFormDefinitionsStrategy = getProductYFormDefinitionsStrategy;
	}

	protected YFormsHelper getyFormsHelper()
	{
		return yFormsHelper;
	}

	@Required
	public void setyFormsHelper(final YFormsHelper yFormsHelper) // NOSONAR
	{
		this.yFormsHelper = yFormsHelper;
	}

	protected YFormFacade getyFormFacade()
	{
		return yFormFacade;
	}

	@Required
	public void setyFormFacade(final YFormFacade yFormFacade) // NOSONAR
	{
		this.yFormFacade = yFormFacade;
	}
}
