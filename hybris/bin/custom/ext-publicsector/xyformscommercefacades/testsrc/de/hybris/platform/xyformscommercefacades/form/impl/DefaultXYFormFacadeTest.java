/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.form.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@UnitTest
public class DefaultXYFormFacadeTest
{

	@InjectMocks
	private DefaultXYFormFacade defaultXYFormFacade;

	@Mock
	private CustomerAccountService customerAccountService;

	@Mock
	private Converter<AddressModel, AddressData> addressConverter;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		defaultXYFormFacade = new DefaultXYFormFacade();
		defaultXYFormFacade.setCustomerAccountService(customerAccountService);
		defaultXYFormFacade.setAddressConverter(addressConverter);
	}

	/**
	 * Method to get test getMappingsData()
	 *
	 * @throws IllegalAccessException
	 *
	 */
	@Test
	public void testGetMappingsData() throws IllegalAccessException
	{

		final YFormMappingsData yFormMappingsData = new YFormMappingsData();
		yFormMappingsData.setFirstName("TestUser");

		final Map<String, String> returnMap = defaultXYFormFacade.getMappingsData(yFormMappingsData);

		Assert.assertEquals(returnMap.get("firstName"), "TestUser");
	}

	/**
	 * Method to test GetMappingsFields
	 *
	 * @throws IllegalAccessException
	 *
	 */
	@Test
	public void testGetMappingsFields() throws IllegalAccessException
	{
		final List<String> list = defaultXYFormFacade.getMappingFields();

		Assert.assertNotNull(list);

	}

	@Test
	public void testGetDefaultAddressForUser()
	{
		final AddressData address = new AddressData();
		address.setEmail("testUser@test.com");
		Mockito.when(customerAccountService.getDefaultAddress(Mockito.any())).thenReturn(Mockito.mock(AddressModel.class));
		Mockito.when(addressConverter.convert(Mockito.any(AddressModel.class))).thenReturn(address);
		Assert.assertNotNull(defaultXYFormFacade.getDefaultAddressForUser(new CustomerModel()));
		Assert.assertEquals(defaultXYFormFacade.getDefaultAddressForUser(new CustomerModel()).getEmail(), "testUser@test.com");


		Mockito.when(customerAccountService.getDefaultAddress(Mockito.any())).thenReturn(null);
		Mockito.when(customerAccountService.getAddressBookEntries(Mockito.any(CustomerModel.class)))
				.thenReturn(Collections.singletonList(Mockito.any(AddressModel.class)));
		Assert.assertNotNull(defaultXYFormFacade.getDefaultAddressForUser(new CustomerModel()));
		Assert.assertEquals(defaultXYFormFacade.getDefaultAddressForUser(new CustomerModel()).getEmail(), "testUser@test.com");

		Mockito.when(customerAccountService.getAddressBookEntries(Mockito.any(CustomerModel.class))).thenReturn(null);
		Assert.assertNull(defaultXYFormFacade.getDefaultAddressForUser(new CustomerModel()));

	}
}
