/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommercefacades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;
import de.hybris.platform.xyformscommercefacades.form.XYFormFacade;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import junit.framework.Assert;


/**
 * Test class of OrderEntryFormDataPopulator.
 */
@UnitTest
public class DefaultYFormMappingsPopulatorTest
{

	@Mock
	private UserFacade userFacade;

	@Mock
	private XYFormFacade xyFormFacade;

	@InjectMocks
	private DefaultYFormMappingsPopulator populator;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		populator = new DefaultYFormMappingsPopulator();
		populator.setXyFormFacade(xyFormFacade);
	}

	@Test
	public void testPopulate()
	{

		final CustomerModel customerModel = new CustomerModel();
		final YFormMappingsData yFormMappingsData = new YFormMappingsData();

		final TitleModel titleModel = new TitleModel();
		titleModel.setCode("TestTitle");

		final AddressData addressData = new AddressData();
		final CountryData countryData = new CountryData();
		countryData.setIsocode("TestCountry");

		addressData.setLine1("Test Address line 1");
		addressData.setLine2("Test Address line 2");
		addressData.setCountry(countryData);
		addressData.setPostalCode("TestPostalCode");
		addressData.setTown("TestTown");
		addressData.setPhone("TestPhone");

		customerModel.setName("Test User");
		customerModel.setTitle(titleModel);

		populator.setUserFacade(userFacade);

		Mockito.when(xyFormFacade.getDefaultAddressForUser(customerModel)).thenReturn(addressData);

		Mockito.when(userFacade.isAnonymousUser()).thenReturn(true);

		populator.populate(customerModel, yFormMappingsData);

		Assert.assertEquals("Test", yFormMappingsData.getFirstName());
		Assert.assertEquals("User", yFormMappingsData.getLastName());

		Assert.assertEquals("Test Address line 1", yFormMappingsData.getAddressLine1());
		Assert.assertEquals("Test Address line 2", yFormMappingsData.getAddressLine2());
		Assert.assertEquals("TestPostalCode", yFormMappingsData.getPostcode());
		Assert.assertEquals("TestTown", yFormMappingsData.getCity());
		Assert.assertEquals("TestPhone", yFormMappingsData.getPhone());
		Assert.assertEquals("TestCountry", yFormMappingsData.getCountry());
		Assert.assertEquals("TestTitle", yFormMappingsData.getTitle());
		Assert.assertNull("TestTitle", yFormMappingsData.getEmail());

	}

}
