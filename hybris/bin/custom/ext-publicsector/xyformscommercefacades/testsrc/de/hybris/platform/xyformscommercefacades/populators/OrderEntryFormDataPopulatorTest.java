/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommercefacades.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.xyformscommercefacades.strategies.impl.GetProductYFormDefinitionsStrategy;
import de.hybris.platform.xyformscommercefacades.util.YFormsHelper;
import de.hybris.platform.xyformsfacades.data.YFormDataData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformsservices.enums.YFormDataTypeEnum;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test class for OrderEntryFormDataPopulator.
 */
@UnitTest
public class OrderEntryFormDataPopulatorTest
{

	@Mock
	private GetProductYFormDefinitionsStrategy getProductYFormDefinitionsStrategy;

	@Mock
	private YFormsHelper yFormsHelper;

	@Mock
	private YFormFacade yFormFacade;

	@InjectMocks
	private OrderEntryFormDataPopulator orderEntryFormDataPopulator;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		orderEntryFormDataPopulator = new OrderEntryFormDataPopulator();
	}

	@Test
	public void testConvert() throws Exception // NOSONAR
	{
		final OrderEntryModel orderEntryModel = new OrderEntryModel();
		final OrderEntryData orderEntryData = new OrderEntryData();
		orderEntryData.setEntryNumber(1);

		final ProductData productData = new ProductData();
		productData.setCode("TestProduct");

		final OrderModel orderModel = new OrderModel();
		orderModel.setCode("TestOrder");

		orderEntryData.setProduct(productData);
		orderEntryModel.setOrder(orderModel);

		final List<YFormDefinitionData> yFormDefinitions = new ArrayList<YFormDefinitionData>();
		final YFormDefinitionData definitionData = new YFormDefinitionData();
		definitionData.setFormId("TestForm");
		yFormDefinitions.add(definitionData);

		given(getProductYFormDefinitionsStrategy.execute(orderEntryData.getProduct().getCode())).willReturn(yFormDefinitions);

		final YFormDataData yformData = new YFormDataData();
		yformData.setId("TestYForm");

		given(yFormFacade.getYFormData(definitionData.getApplicationId(), definitionData.getFormId(),
				yFormsHelper.buildYFormDataRefId(orderEntryModel.getOrder().getCode(), orderEntryData.getEntryNumber()),
				YFormDataTypeEnum.DATA)).willReturn(yformData);

		final YFormDataData yFormDataData = new YFormDataData();

		given(yFormFacade.getYFormData(yformData.getId(), YFormDataTypeEnum.DRAFT)).willReturn(yFormDataData);

		orderEntryFormDataPopulator.setyFormFacade(yFormFacade);
		orderEntryFormDataPopulator.setyFormsHelper(yFormsHelper);
		orderEntryFormDataPopulator.setGetProductYFormDefinitionsStrategy(getProductYFormDefinitionsStrategy);

		orderEntryFormDataPopulator.populate(orderEntryModel, orderEntryData);

	}
}
