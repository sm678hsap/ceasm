/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.xyformscommercefacades.data.YFormDefinitionFieldMappingsData;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import junit.framework.Assert;


@UnitTest
public class DefaultYFormDefinitionFieldMappingsPopulatorTest
{

	@InjectMocks
	private DefaultYFormDefinitionFieldMappingsPopulator yFormsProductFieldMappingsPopulator;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		yFormsProductFieldMappingsPopulator = new DefaultYFormDefinitionFieldMappingsPopulator();
	}

	@Test
	public void testConvert()
	{
		final YFormDefinitionFieldMappingsModel mappingsModel = new YFormDefinitionFieldMappingsModel();
		mappingsModel.setApplicationId("TestApplicationId");
		mappingsModel.setFormId("TestFormId");
		mappingsModel.setFormToUserMappings(new HashMap<String, String>());
		final YFormDefinitionFieldMappingsData mappingsData = new YFormDefinitionFieldMappingsData();

		yFormsProductFieldMappingsPopulator.populate(mappingsModel, mappingsData);

		Assert.assertEquals("TestApplicationId", mappingsData.getApplicationId());
		Assert.assertEquals("TestFormId", mappingsData.getYFormId());
		Assert.assertNotNull(mappingsData.getYFormFieldMappings());
	}

}
