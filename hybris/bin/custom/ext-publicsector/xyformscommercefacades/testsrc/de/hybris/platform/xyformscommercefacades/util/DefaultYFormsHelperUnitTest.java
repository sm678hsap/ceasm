/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.xyformscommercefacades.form.data.FormDetailData;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.Lists;


/**
 * The class of DefaultYFormsHelperUnitTest.
 */
@UnitTest
public class DefaultYFormsHelperUnitTest
{
	private static final String ORDER_CODE = "orderCode";

	@InjectMocks
	private DefaultYFormsHelper helper;

	@Mock
	private YFormFacade yFormFacade;

	@Before
	public void setup()
	{
		helper = new DefaultYFormsHelper();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testBuildYFormDataRefId()
	{
		final Integer orderEntryNumber = new Integer(2);
		Assert.assertEquals(ORDER_CODE + "_" + orderEntryNumber, helper.buildYFormDataRefId(ORDER_CODE, orderEntryNumber));
	}

	@Test
	public void testCreateFormDetailData()
	{
		final String applicationId = "applicationId";
		final String formId = "formId";

		final YFormDefinitionData yFormDefinitionData = new YFormDefinitionData();
		yFormDefinitionData.setApplicationId(applicationId);
		yFormDefinitionData.setFormId(formId);
		final Integer orderEntryNumber = new Integer(2);

		final FormDetailData formDetailData = helper.createFormDetailData(yFormDefinitionData, ORDER_CODE, orderEntryNumber);

		Assert.assertNotNull(formDetailData);
		Assert.assertEquals(applicationId, formDetailData.getApplicationId());
		Assert.assertEquals(formId, formDetailData.getFormId());
		Assert.assertEquals(ORDER_CODE + "_" + orderEntryNumber, formDetailData.getRefId());
	}

	@Test
	public void testCreateFormDetailDataList()
	{
		final String applicationId = "applicationId";
		final String formId1 = "formId1";
		final String formId2 = "formId2";

		final YFormDefinitionData yFormDefinitionData1 = new YFormDefinitionData();
		yFormDefinitionData1.setApplicationId(applicationId);
		yFormDefinitionData1.setFormId(formId1);

		final YFormDefinitionData yFormDefinitionData2 = new YFormDefinitionData();
		yFormDefinitionData2.setApplicationId(applicationId);
		yFormDefinitionData2.setFormId(formId2);


		final Integer orderEntryNumber = new Integer(2);

		final String expectedRefId = ORDER_CODE + "_" + orderEntryNumber;

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList(yFormDefinitionData1, yFormDefinitionData2);

		final List<FormDetailData> formDetailDataList = helper.createFormDetailData(yFormDefinitionDataList, ORDER_CODE,
				orderEntryNumber);

		Assert.assertNotNull(formDetailDataList);
		Assert.assertEquals(2, formDetailDataList.size());

		final FormDetailData formDetailData1 = formDetailDataList.get(0);

		Assert.assertEquals(applicationId, formDetailData1.getApplicationId());
		Assert.assertEquals(formId1, formDetailData1.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData1.getRefId());

		final FormDetailData formDetailData2 = formDetailDataList.get(1);

		Assert.assertEquals(applicationId, formDetailData2.getApplicationId());
		Assert.assertEquals(formId2, formDetailData2.getFormId());
		Assert.assertEquals(expectedRefId, formDetailData2.getRefId());

	}

	@Test
	public void testCreateFormDetailDataListEmptyList()
	{
		final Integer orderEntryNumber = new Integer(2);

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList();
		final List<FormDetailData> formDetailDataList = helper.createFormDetailData(yFormDefinitionDataList, ORDER_CODE,
				orderEntryNumber);

		Assert.assertNotNull(formDetailDataList);
		Assert.assertTrue(formDetailDataList.isEmpty());
	}


}
