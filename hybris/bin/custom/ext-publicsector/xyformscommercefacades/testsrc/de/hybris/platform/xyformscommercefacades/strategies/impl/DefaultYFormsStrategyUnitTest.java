/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.strategies.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.xyformscommercefacades.util.YFormsHelper;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsfacades.form.YFormFacade;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test suite for {@link DefaultYFormsStrategy}
 *
 */
@UnitTest
public class DefaultYFormsStrategyUnitTest
{
	private static final String CART = "cart";
	private static final String PRODUCT_CODE1 = "productCode1";
	private static final String PRODUCT_CODE2 = "productCode2";
	private static final String APPLICATION_ID = "sample-application";
	private static final String FORM_ID1 = "form1";

	@InjectMocks
	private DefaultYFormsStrategy yFormsStrategy;

	@Mock
	private YFormFacade yFormFacade;

	@Mock
	private YFormsHelper yFormsHelper;

	@Mock
	private GetProductYFormDefinitionsStrategy getProductYFormDefinitionsStrategy;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		yFormsStrategy = new DefaultYFormsStrategy();
		yFormsStrategy.setYFormFacade(yFormFacade);
		yFormsStrategy.setyFormsHelper(yFormsHelper);
		yFormsStrategy.setGetProductYFormDefinitionsStrategy(getProductYFormDefinitionsStrategy);
	}

	@Test
	public void testGetYFormOrderEntries() throws YFormServiceException
	{
		final OrderEntryData orderEntryData1 = getOrderEntryData(1, 1, PRODUCT_CODE1);
		final OrderEntryData orderEntryData2 = getOrderEntryData(1, 1, PRODUCT_CODE2);

		final CartData cartData = new CartData();
		cartData.setCode(CART);
		cartData.setEntries(Arrays.asList(orderEntryData1, orderEntryData2));

		final YFormDefinitionData yFormDefinitionData = new YFormDefinitionData();
		yFormDefinitionData.setApplicationId(APPLICATION_ID);
		yFormDefinitionData.setFormId(FORM_ID1);

		// considering product with code productCode1 has YForms assigned and product with code productCode2 with no YForms.
		given(yFormsStrategy.getGetProductYFormDefinitionsStrategy().execute(PRODUCT_CODE1))
				.willReturn(Collections.singletonList(yFormDefinitionData));
		given(yFormsStrategy.getGetProductYFormDefinitionsStrategy().execute(PRODUCT_CODE2)).willReturn(null);

		final List<OrderEntryData> orderEntryDataList = yFormsStrategy.getYFormOrderEntries(cartData);

		Assert.assertNotNull(orderEntryDataList);
		Assert.assertEquals(Collections.singletonList(orderEntryData1), orderEntryDataList);
	}

	@Test
	public void testGetFormDefinitionsForOrderEntry() throws YFormServiceException
	{
		final OrderEntryData orderEntryData = getOrderEntryData(1, 1, PRODUCT_CODE1);
		final YFormDefinitionData yFormDefinitionData = new YFormDefinitionData();
		yFormDefinitionData.setApplicationId(APPLICATION_ID);
		yFormDefinitionData.setFormId(FORM_ID1);

		given(yFormsStrategy.getGetProductYFormDefinitionsStrategy().execute(PRODUCT_CODE1))
				.willReturn(Collections.singletonList(yFormDefinitionData));

		final List<YFormDefinitionData> yFormDefinitionDataList = yFormsStrategy.getFormDefinitionsForOrderEntry(orderEntryData);
		Assert.assertNotNull(yFormDefinitionDataList);
		Assert.assertEquals(Collections.singletonList(yFormDefinitionData), yFormDefinitionDataList);
	}

	@Test
	public void verifyGetFormDefinitionsForOrderEntryShouldReturnNullForNonYFormProduct() throws YFormServiceException
	{
		final OrderEntryData orderEntryData = getOrderEntryData(1, 1, PRODUCT_CODE1);

		given(yFormsStrategy.getGetProductYFormDefinitionsStrategy().execute(PRODUCT_CODE1)).willReturn(null);

		final List<YFormDefinitionData> yFormDefinitionDataList = yFormsStrategy.getFormDefinitionsForOrderEntry(orderEntryData);
		Assert.assertNull(yFormDefinitionDataList);
	}

	protected OrderEntryData getOrderEntryData(final int entryNumber, final long qty, final String productCode)
	{
		final OrderEntryData orderEntryData = new OrderEntryData();
		orderEntryData.setEntryNumber(Integer.valueOf(entryNumber));
		orderEntryData.setQuantity(Long.valueOf(qty));
		orderEntryData.setProduct(new ProductData());
		orderEntryData.getProduct().setCode(productCode);

		return orderEntryData;
	}

}
