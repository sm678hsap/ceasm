/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommercefacades.strategies.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.google.common.collect.Maps;


/**
 * The class of DefaultYFormDataPreprocessorStrategyUnitTest.
 */
@UnitTest
public class DefaultYFormDataPreprocessorStrategyUnitTest
{

	@InjectMocks
	private DefaultYFormDataPreprocessorStrategy yFormDataPreprocessorStrategy;

	@Before
	public void setup()
	{
		yFormDataPreprocessorStrategy = new DefaultYFormDataPreprocessorStrategy();
	}

	@Test
	public void shouldUpdateXmlContent() throws YFormProcessorException
	{
		final String userName = "TEST_USER";
		final String userEmail = "test_user@test.com";
		final Boolean isUserLoggedIn = Boolean.TRUE;

		final String xmlContent = "<form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>"
				+ "<section-update-me><is-user-logged-in>false</is-user-logged-in>"
				+ "<label-logged-in-user/><label-unidentified-user/>" + "<user-name/><user-email/></section-update-me></form>";

		final String expectedXmlContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>" + "<section-update-me><is-user-logged-in>"
				+ isUserLoggedIn + "</is-user-logged-in><label-logged-in-user/><label-unidentified-user/>" + "<user-name>" + userName
				+ "</user-name><user-email>" + userEmail + "</user-email></section-update-me></form>";


		final Map<String, Object> params = Maps.newHashMap();

		params.put("/form/section-update-me/user-name", userName);
		params.put("/form/section-update-me/user-email", userEmail);
		params.put("/form/section-update-me/is-user-logged-in", isUserLoggedIn);

		final String resultXml = yFormDataPreprocessorStrategy.updateXmlContent(xmlContent, params);

		Assert.assertEquals(expectedXmlContent, resultXml);
	}

	@Test
	public void shouldNotUpdateXmlContentWhenXmlContentIsNull() throws YFormProcessorException
	{
		final Map<String, Object> params = Maps.newHashMap();
		params.put("/form/section-update-me/is-user-logged-in", Boolean.FALSE);

		final String resultXml = yFormDataPreprocessorStrategy.updateXmlContent(null, params);

		Assert.assertNull(resultXml);
	}

	@Test
	public void shouldNotUpdateXmlContentWhenParameterIsEmpty() throws YFormProcessorException
	{
		final String xmlContent = "<form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>"
				+ "<section-update-me><is-user-logged-in>false</is-user-logged-in>"
				+ "<label-logged-in-user/><label-unidentified-user/>" + "<user-name/><user-email/></section-update-me></form>";

		final Map<String, Object> params = Maps.newHashMap();

		final String resultXml = yFormDataPreprocessorStrategy.updateXmlContent(xmlContent, params);

		Assert.assertEquals(xmlContent, resultXml);
	}
}
