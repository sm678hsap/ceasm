/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.populators;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsservices.model.YFormDefinitionModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


@UnitTest
public class YFormsProductPopulatorTest
{

	@Mock
	private Converter<YFormDefinitionModel, YFormDefinitionData> yformDefinitionConverter;

	@InjectMocks
	private YFormsProductPopulator yFormsProductPopulator;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		yFormsProductPopulator = new YFormsProductPopulator();
	}

	@Test
	public void testConvert()
	{
		final ProductModel productModel = mock(ProductModel.class);
		final ProductData productData = mock(ProductData.class);
		final YFormDefinitionModel model = mock(YFormDefinitionModel.class);

		final YFormDefinitionData data = mock(YFormDefinitionData.class);

		final List<YFormDefinitionModel> list = new ArrayList<YFormDefinitionModel>();
		list.add(model);
		productModel.setAllYFormDefinitionList(list);

		yFormsProductPopulator.setYformDefinitionConverter(yformDefinitionConverter);

		given(yformDefinitionConverter.convert(model)).willReturn(data);

		yFormsProductPopulator.populate(productModel, productData);

	}

}
