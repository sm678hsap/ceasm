/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.xyformscommercefacades.data.YFormDefinitionFieldMappingsData;
import de.hybris.platform.xyformscommercefacades.data.YFormMappingsData;
import de.hybris.platform.xyformscommercefacades.form.data.FormDetailData;
import de.hybris.platform.xyformscommercefacades.form.impl.DefaultXYFormFacade;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;
import de.hybris.platform.xyformscommerceservices.service.YFormDefinitionFieldMappingsService;
import de.hybris.platform.xyformscommerceservices.strategies.YFormUserStrategy;
import de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.form.YFormService;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class of DefaultYFormDataPreprocessorStrategy.
 */
@UnitTest
public class PopulateYFormDataPreprocessorStrategyTest
{

	@Mock
	private UserService userService;

	@Mock
	private Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> yformDefinitionFieldsConverter;

	@Mock
	private Converter<CustomerModel, YFormMappingsData> yFormMappingsConverter;

	@Mock
	private YFormService yformService;

	@Mock
	private DefaultXYFormFacade defaultXYFormFacade;

	@Mock
	private YFormUserStrategy defaultGetUserStrategy;

	@Mock
	private YFormDefinitionFieldMappingsService yFormDefinitionFieldMappingsService;

	@InjectMocks
	private PopulateYFormDataPreprocessorStrategy yFormDataPreprocessorStrategy;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		yFormDataPreprocessorStrategy = new PopulateYFormDataPreprocessorStrategy();
		yFormDataPreprocessorStrategy.setDefaultXYFormFacade(defaultXYFormFacade);
		yFormDataPreprocessorStrategy.setUserService(userService);
		yFormDataPreprocessorStrategy.setyFormDefinitionFieldMappingsService(yFormDefinitionFieldMappingsService);
		yFormDataPreprocessorStrategy.setYformDefinitionFieldsConverter(yformDefinitionFieldsConverter);
		yFormDataPreprocessorStrategy.setYformService(yformService);
		yFormDataPreprocessorStrategy.setyFormMappingsConverter(yFormMappingsConverter);
		yFormDataPreprocessorStrategy.setUserStrategy(defaultGetUserStrategy);

	}

	/**
	 * Applies the actual transformation to a formData
	 *
	 * @param xmlContent
	 * @param params
	 * @throws de.hybris.platform.xyformsfacades.strategy.preprocessor.YFormProcessorException
	 * @throws YFormServiceException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testTransform() throws YFormProcessorException, IllegalAccessException, YFormServiceException
	{

		final String xmlString = "test";// super.transform(xmlContent, params);
		final String xmlContent = "<form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>"
				+ "<section-update-me><is-user-logged-in>false</is-user-logged-in>"
				+ "<label-logged-in-user/><label-unidentified-user/>" + "<user-name/><user-email/></section-update-me></form>";

		final FormDetailData formDetailData = new FormDetailData();
		formDetailData.setApplicationId("TestApplicationId");
		formDetailData.setFormDataId("TestFormId");

		final CustomerModel customerModel = new CustomerModel();
		customerModel.setType(CustomerType.REGISTERED);

		final Map<String, String> dataMap = new HashMap<>();

		final Map<String, Object> params = new HashMap<>();
		params.put("formDetailData", formDetailData);

		Mockito.when(defaultGetUserStrategy.getCurrentUserForCheckout()).thenReturn(customerModel);

		Mockito.when(getUserService().isAnonymousUser(customerModel)).thenReturn(false);

		final YFormMappingsData yFormMappingsData = new YFormMappingsData();
		Mockito.when(getyFormMappingsConverter().convert(customerModel)).thenReturn(yFormMappingsData);

		final Map<String, String> yformMappingsDataMap = new HashMap<>();
		yformMappingsDataMap.put("name", "TEST_USER");
		yformMappingsDataMap.put("email", "test_user@test.com");
		yformMappingsDataMap.put("isUserLoggedIn", "true");

		Mockito.when(getDefaultXYFormFacade().getMappingsData(yFormMappingsData)).thenReturn(yformMappingsDataMap);

		final YFormDefinitionFieldMappingsModel yFormDefinitionFieldMappingsModel = new YFormDefinitionFieldMappingsModel();
		Mockito.when(getyFormDefinitionFieldMappingsService().getMappingsForYForm(formDetailData.getApplicationId(),
				formDetailData.getFormId())).thenReturn(yFormDefinitionFieldMappingsModel);

		final YFormDefinitionFieldMappingsData yFormDefinitionFieldMappingsData = new YFormDefinitionFieldMappingsData();

		final Map<String, String> map = new HashMap<>();
		map.put("/form/section-update-me/user-name", "name");
		map.put("/form/section-update-me/user-email", "email");
		map.put("/form/section-update-me/is-user-logged-in", "isUserLoggedIn");
		yFormDefinitionFieldMappingsData.setYFormFieldMappings(map);

		Mockito.when(getYformDefinitionFieldsConverter().convert(yFormDefinitionFieldMappingsModel))
				.thenReturn(yFormDefinitionFieldMappingsData);

		final String resultXml = getyFormDataPreprocessorStrategy().transform(xmlContent, params);

		final String expectedXmlContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><form><section-report-graffiti>"
				+ "<address-line1/><damaged-item/><label-address-header/><address-country/>"
				+ "<address-line2/><address-city/><address-postcode/><comment/>"
				+ "<update-me/><label-required-field-hint/></section-report-graffiti>" + "<section-update-me><is-user-logged-in>"
				+ "true" + "</is-user-logged-in><label-logged-in-user/><label-unidentified-user/>" + "<user-name>" + "TEST_USER"
				+ "</user-name><user-email>" + "test_user@test.com" + "</user-email></section-update-me></form>";

		Assert.assertEquals(expectedXmlContent, resultXml);

	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the yformDefinitionFieldsConverter
	 */
	public Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> getYformDefinitionFieldsConverter()
	{
		return yformDefinitionFieldsConverter;
	}

	/**
	 * @param yformDefinitionFieldsConverter
	 *           the yformDefinitionFieldsConverter to set
	 */
	public void setYformDefinitionFieldsConverter(
			final Converter<YFormDefinitionFieldMappingsModel, YFormDefinitionFieldMappingsData> yformDefinitionFieldsConverter)
	{
		this.yformDefinitionFieldsConverter = yformDefinitionFieldsConverter;
	}

	/**
	 * @return the yFormMappingsConverter
	 */
	public Converter<CustomerModel, YFormMappingsData> getyFormMappingsConverter()
	{
		return yFormMappingsConverter;
	}

	/**
	 * @param yFormMappingsConverter
	 *           the yFormMappingsConverter to set
	 */
	public void setyFormMappingsConverter(final Converter<CustomerModel, YFormMappingsData> yFormMappingsConverter)
	{
		this.yFormMappingsConverter = yFormMappingsConverter;
	}

	/**
	 * @return the yformService
	 */
	public YFormService getYformService()
	{
		return yformService;
	}

	/**
	 * @param yformService
	 *           the yformService to set
	 */
	public void setYformService(final YFormService yformService)
	{
		this.yformService = yformService;
	}

	/**
	 * @return the defaultXYFormFacade
	 */
	public DefaultXYFormFacade getDefaultXYFormFacade()
	{
		return defaultXYFormFacade;
	}

	/**
	 * @param defaultXYFormFacade
	 *           the defaultXYFormFacade to set
	 */
	public void setDefaultXYFormFacade(final DefaultXYFormFacade defaultXYFormFacade)
	{
		this.defaultXYFormFacade = defaultXYFormFacade;
	}

	/**
	 * @return the yFormDefinitionFieldMappingsService
	 */
	public YFormDefinitionFieldMappingsService getyFormDefinitionFieldMappingsService()
	{
		return yFormDefinitionFieldMappingsService;
	}

	/**
	 * @param yFormDefinitionFieldMappingsService
	 *           the yFormDefinitionFieldMappingsService to set
	 */
	public void setyFormDefinitionFieldMappingsService(
			final YFormDefinitionFieldMappingsService yFormDefinitionFieldMappingsService)
	{
		this.yFormDefinitionFieldMappingsService = yFormDefinitionFieldMappingsService;
	}

	/**
	 * @return the yFormDataPreprocessorStrategy
	 */
	public PopulateYFormDataPreprocessorStrategy getyFormDataPreprocessorStrategy()
	{
		return yFormDataPreprocessorStrategy;
	}

	/**
	 * @param yFormDataPreprocessorStrategy
	 *           the yFormDataPreprocessorStrategy to set
	 */
	public void setyFormDataPreprocessorStrategy(final PopulateYFormDataPreprocessorStrategy yFormDataPreprocessorStrategy)
	{
		this.yFormDataPreprocessorStrategy = yFormDataPreprocessorStrategy;
	}

}
