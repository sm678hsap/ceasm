/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.xyformscommercefacades.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.model.YFormDefinitionModel;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.Lists;


/**
 * The class of GetProductYFormDefinitionsStrategyUnitTest.
 */
@UnitTest
public class GetProductYFormDefinitionsStrategyUnitTest
{

	@InjectMocks
	private GetProductYFormDefinitionsStrategy getProductYFormDefinitionsStrategy;

	@Mock
	private ProductService productService;

	@Mock
	private Converter<YFormDefinitionModel, YFormDefinitionData> yformDefinitionConverter;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		getProductYFormDefinitionsStrategy = new GetProductYFormDefinitionsStrategy();
		getProductYFormDefinitionsStrategy.setProductService(productService);
		getProductYFormDefinitionsStrategy.setYformDefinitionConverter(yformDefinitionConverter);

	}

	@Test
	public void shouldReturnYFormDefinitionsForProductWithYFormDefinitions() throws YFormServiceException
	{
		final String productCode = "productCode";
		final String applicationId = "applicationId";
		final String formId = "formId";

		final List<YFormDefinitionData> yFormDefinitionDataList = Lists.newArrayList();
		final List<YFormDefinitionModel> yFormDefinitionModelList = Lists.newArrayList();

		final YFormDefinitionModel yFormDefinitionModel = new YFormDefinitionModel();
		yFormDefinitionModel.setApplicationId(applicationId);
		yFormDefinitionModel.setFormId(formId);
		yFormDefinitionModelList.add(yFormDefinitionModel);

		final YFormDefinitionData yFormDefinitionData = new YFormDefinitionData();
		yFormDefinitionData.setApplicationId(applicationId);
		yFormDefinitionData.setFormId(formId);
		yFormDefinitionDataList.add(yFormDefinitionData);

		final ProductModel serviceProduct = new ProductModel();
		serviceProduct.setCode(productCode);
		serviceProduct.setAllYFormDefinitionList(yFormDefinitionModelList);
		Mockito.when(productService.getProductForCode(productCode)).thenReturn(serviceProduct);
		Mockito.when(yformDefinitionConverter.convert(yFormDefinitionModel)).thenReturn(yFormDefinitionData);
		final List<YFormDefinitionData> resultYFormDefinitionDataList = getProductYFormDefinitionsStrategy.execute(productCode);

		Assert.assertNotNull(resultYFormDefinitionDataList);
		Assert.assertFalse(resultYFormDefinitionDataList.isEmpty());
		Assert.assertEquals(yFormDefinitionDataList, resultYFormDefinitionDataList);
	}

	@Test
	public void shouldNotReturnYFormDefinitionsForProductWithoutYFormDefinitions() throws YFormServiceException
	{
		final String productCode = "productCode";

		final ProductModel serviceProduct = new ProductModel();
		serviceProduct.setCode(productCode);
		serviceProduct.setAllYFormDefinitionList(Collections.<YFormDefinitionModel> emptyList());
		Mockito.when(productService.getProductForCode(productCode)).thenReturn(serviceProduct);
		final List<YFormDefinitionData> resultYFormDefinitionDataList = getProductYFormDefinitionsStrategy.execute(productCode);

		Assert.assertNull(resultYFormDefinitionDataList);
	}

}
