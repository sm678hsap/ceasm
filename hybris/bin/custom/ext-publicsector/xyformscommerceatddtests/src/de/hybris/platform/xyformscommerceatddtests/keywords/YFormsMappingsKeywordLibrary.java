/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.xyformscommerceatddtests.keywords;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.xyformscommerceservices.model.YFormDefinitionFieldMappingsModel;
import de.hybris.platform.xyformscommerceservices.service.impl.DefaultYFormDefinitionFieldMappingsService;

import java.util.List;

import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * PSDraftKeywordLibrary
 *
 */
public class YFormsMappingsKeywordLibrary extends AbstractKeywordLibrary
{
	public static final String SESSION_CART_PARAMETER_NAME = "cart";

	@Autowired
	private DefaultYFormDefinitionFieldMappingsService yFormDefinitionFieldMappingsService;


	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>Get mappings for a YForm</i>
	 * <p>
	 *
	 * @param applicationId
	 * @param formId
	 * @return boolean
	 */
	public boolean getYformMappings(final String applicationId, final String formId)
	{
		final YFormDefinitionFieldMappingsModel yFormDefinitionFieldMappingsModel = getyFormDefinitionFieldMappingsService()
				.getMappingsForYForm(applicationId, formId);

		if (yFormDefinitionFieldMappingsModel != null)
		{
			return true;
		}
		return false;
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>Get all yForms mappings</i>
	 * <p>
	 *
	 * @return boolean
	 */
	public boolean getAllYformMappings()
	{
		final List<YFormDefinitionFieldMappingsModel> yFormDefinitionFieldMappingsModels = getyFormDefinitionFieldMappingsService()
				.getAllYFormsMappings();

		if (!Collections.isEmpty(yFormDefinitionFieldMappingsModels))
		{
			return true;
		}
		return false;
	}


	public DefaultYFormDefinitionFieldMappingsService getyFormDefinitionFieldMappingsService()
	{
		return yFormDefinitionFieldMappingsService;
	}


	public void setyFormDefinitionFieldMappingsService(
			final DefaultYFormDefinitionFieldMappingsService yFormDefinitionFieldMappingsService)
	{
		this.yFormDefinitionFieldMappingsService = yFormDefinitionFieldMappingsService;
	}


}
