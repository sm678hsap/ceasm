/**
 *
 */
package de.hybris.platform.publicsectordocmanagement.service.impl;

import java.io.IOException;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectordocmanagement.dao.impl.DefaultPSDocumentManagementDao;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.type.TypeService;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;



/**
 * Unit Test class for {@link DefaultPSDocumentManagementService}
 */
@UnitTest
public class DefaultPSDocumentManagementServiceTest
{
	private static final Long CUSTOMER_PK1 = 100000001L;
	private static final String CUSTOMER_ID1 = "ID-100011";
	private static final String DOC_ID1 = "DOC-100011";
	private static final String ORDER_ID1 = "ORDER-100011";
	private static final Long CUSTOMER_PK2 = 12345678912L;
	private static final String CUSTOMER_ID2 = "ID-100012";
	private static final String DOC_ID2 = "DOC-100012";
	private static final String ORDER_ID2 = "ORDER-100012";
	private static final Long CUSTOMER_PK3 = 12345678913L;
	private static final String CUSTOMER_ID3 = "ID-100013";
	private static final String DOC_ID3 = "DOC-100013";
	private static final String ORDER_ID3 = "ORDER-100013";
	private static final Long CUSTOMER_PK4 = 12345678914L;
	private static final String CUSTOMER_ID4 = "ID-100014";
	private static final String DOC_ID4 = "DOC-100014";
	private static final String ORDER_ID4 = "ORDER-100014";
	private static final Long CUSTOMER_PK5 = 12345678915L;
	private static final String CUSTOMER_ID5 = "ID-100015";
	private static final String DOC_ID5 = "DOC-100015";
	private static final String ORDER_ID5 = "ORDER-100015";

	@InjectMocks
	private DefaultPSDocumentManagementService psDocumentManagementService;

	@Mock
	private DefaultPSDocumentManagementDao psDocumentManagementDao;
	
	@Mock
	private TypeService typeService;

	@Mock
	private UserModel sourceUser;

	/**
	 * Runs before test method.
	 */
	@Before
	public void setUp()
	{
		Registry.activateMasterTenant();
		MockitoAnnotations.initMocks(this);
		psDocumentManagementService = new DefaultPSDocumentManagementService();
		psDocumentManagementService.setPsDocumentManagementDao(psDocumentManagementDao);
		psDocumentManagementService.setTypeService(typeService);
	}

	protected static class MockCustomerModel1 extends CustomerModel
	{
		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(CUSTOMER_PK1);
		}
	}

	protected static class MockCustomerModel2 extends CustomerModel
	{
		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(CUSTOMER_PK2);
		}
	}

	protected static class MockCustomerModel3 extends CustomerModel
	{
		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(CUSTOMER_PK3);
		}
	}

	protected static class MockCustomerModel4 extends CustomerModel
	{
		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(CUSTOMER_PK4);
		}
	}

	protected static class MockCustomerModel5 extends CustomerModel
	{
		@Override
		public PK getPk()
		{
			return de.hybris.platform.core.PK.fromLong(CUSTOMER_PK5);
		}
	}

	/**
	 * Test creation of customer1 folder.
	 */
	@Test
	public void testCreateCustomer1DocumentDirectory() throws IOException
	{
		//Customer1
		final CustomerModel customer1 = new MockCustomerModel1();
		customer1.setCustomerID(CUSTOMER_ID1);
		final File rootDir1 = psDocumentManagementService.createCustomerDocumentRootDirectory(customer1);
		Assert.assertNotNull(rootDir1);
		final File customerDir1 = psDocumentManagementService.createByCustomerDirectory(customer1);
		Assert.assertNotNull(customerDir1);
		final File customerThumbnailDir1 = psDocumentManagementService.createByCustomerThumbnailDirectory(customer1);
		Assert.assertNotNull(customerThumbnailDir1);

		final PSDocumentModel psDocument1 = mock(PSDocumentModel.class);
		Mockito.when(psDocument1.getDocId()).thenReturn(DOC_ID1);
		final Collection<PSDocumentModel> documents1 = Arrays.asList(psDocument1);

		final OrderModel order1 = mock(OrderModel.class);
		Mockito.when(order1.getCode()).thenReturn(ORDER_ID1);
		Mockito.when(order1.getPsDocument()).thenReturn(documents1);
		Mockito.when(order1.getUser()).thenReturn(customer1);

		final File orderFolder1 = psDocumentManagementService.createByOrderDirectory(customer1, order1);
		Assert.assertNotNull(orderFolder1);
		final File orderInboundFolder1 = psDocumentManagementService.createByOrderInboundDirectory(customer1, order1);
		Assert.assertNotNull(orderInboundFolder1);
		final File orderOutboundFolder1 = psDocumentManagementService.createByOrderOutboundDirectory(customer1, order1);
		Assert.assertNotNull(orderOutboundFolder1);

		final File orderInboundThumbnailFolder1 = psDocumentManagementService.createByOrderInboundThumbnailDirectory(customer1,
				order1);
		Assert.assertNotNull(orderInboundThumbnailFolder1);
		final File orderOutboundThumbnailFolder1 = psDocumentManagementService.createByOrderOutboundThumbnailDirectory(customer1,
				order1);
		Assert.assertNotNull(orderOutboundThumbnailFolder1);
	}

	/**
	 * Test creation of customer2 folder.
	 */
	@Test
	public void testCreateCustomer2DocumentDirectory() throws IOException
	{
		//Customer2
		final CustomerModel customer2 = new MockCustomerModel2();
		customer2.setCustomerID(CUSTOMER_ID2);
		final File rootDir2 = psDocumentManagementService.createCustomerDocumentRootDirectory(customer2);
		Assert.assertNotNull(rootDir2);
		final File customerDir2 = psDocumentManagementService.createByCustomerDirectory(customer2);
		Assert.assertNotNull(customerDir2);
		final File customerThumbnailDir2 = psDocumentManagementService.createByCustomerThumbnailDirectory(customer2);
		Assert.assertNotNull(customerThumbnailDir2);

		final PSDocumentModel psDocument2 = mock(PSDocumentModel.class);
		Mockito.when(psDocument2.getDocId()).thenReturn(DOC_ID2);
		final Collection<PSDocumentModel> documents2 = Arrays.asList(psDocument2);

		final OrderModel order2 = mock(OrderModel.class);
		Mockito.when(order2.getCode()).thenReturn(ORDER_ID2);
		Mockito.when(order2.getPsDocument()).thenReturn(documents2);
		Mockito.when(order2.getUser()).thenReturn(customer2);

		final File orderFolder2 = psDocumentManagementService.createByOrderDirectory(customer2, order2);
		Assert.assertNotNull(orderFolder2);
		final File orderInboundFolder2 = psDocumentManagementService.createByOrderInboundDirectory(customer2, order2);
		Assert.assertNotNull(orderInboundFolder2);
		final File orderOutboundFolder2 = psDocumentManagementService.createByOrderOutboundDirectory(customer2, order2);
		Assert.assertNotNull(orderOutboundFolder2);

		final File orderInboundThumbnailFolder2 = psDocumentManagementService.createByOrderInboundThumbnailDirectory(customer2,
				order2);
		Assert.assertNotNull(orderInboundThumbnailFolder2);
		final File orderOutboundThumbnailFolder2 = psDocumentManagementService.createByOrderOutboundThumbnailDirectory(customer2,
				order2);
		Assert.assertNotNull(orderOutboundThumbnailFolder2);
	}

	/**
	 * Test creation of customer3 folder.
	 */
	@Test
	public void testCreateCustomer3DocumentDirectory() throws IOException
	{
		//Customer3
		final CustomerModel customer3 = new MockCustomerModel3();
		customer3.setCustomerID(CUSTOMER_ID3);
		final File rootDir3 = psDocumentManagementService.createCustomerDocumentRootDirectory(customer3);
		Assert.assertNotNull(rootDir3);
		final File customerDir3 = psDocumentManagementService.createByCustomerDirectory(customer3);
		Assert.assertNotNull(customerDir3);
		final File customerThumbnailDir3 = psDocumentManagementService.createByCustomerThumbnailDirectory(customer3);
		Assert.assertNotNull(customerThumbnailDir3);

		final PSDocumentModel psDocument3 = mock(PSDocumentModel.class);
		Mockito.when(psDocument3.getDocId()).thenReturn(DOC_ID3);
		final Collection<PSDocumentModel> documents3 = Arrays.asList(psDocument3);

		final OrderModel order3 = mock(OrderModel.class);
		Mockito.when(order3.getCode()).thenReturn(ORDER_ID3);
		Mockito.when(order3.getPsDocument()).thenReturn(documents3);
		Mockito.when(order3.getUser()).thenReturn(customer3);

		final File orderFolder3 = psDocumentManagementService.createByOrderDirectory(customer3, order3);
		Assert.assertNotNull(orderFolder3);
		final File orderInboundFolder3 = psDocumentManagementService.createByOrderInboundDirectory(customer3, order3);
		Assert.assertNotNull(orderInboundFolder3);
		final File orderOutboundFolder3 = psDocumentManagementService.createByOrderOutboundDirectory(customer3, order3);
		Assert.assertNotNull(orderOutboundFolder3);

		final File orderInboundThumbnailFolder3 = psDocumentManagementService.createByOrderInboundThumbnailDirectory(customer3,
				order3);
		Assert.assertNotNull(orderInboundThumbnailFolder3);
		final File orderOutboundThumbnailFolder3 = psDocumentManagementService.createByOrderOutboundThumbnailDirectory(customer3,
				order3);
		Assert.assertNotNull(orderOutboundThumbnailFolder3);

	}

	/**
	 * Test creation of customer4 folder.
	 */
	@Test
	public void testCreateCustomer4DocumentDirectory() throws IOException
	{
		//Customer4
		final CustomerModel customer4 = new MockCustomerModel4();
		customer4.setCustomerID(CUSTOMER_ID4);
		final File rootDir4 = psDocumentManagementService.createCustomerDocumentRootDirectory(customer4);
		Assert.assertNotNull(rootDir4);
		final File customerDir4 = psDocumentManagementService.createByCustomerDirectory(customer4);
		Assert.assertNotNull(customerDir4);
		final File customerThumbnailDir4 = psDocumentManagementService.createByCustomerThumbnailDirectory(customer4);
		Assert.assertNotNull(customerThumbnailDir4);

		final PSDocumentModel psDocument4 = mock(PSDocumentModel.class);
		Mockito.when(psDocument4.getDocId()).thenReturn(DOC_ID4);
		final Collection<PSDocumentModel> documents4 = Arrays.asList(psDocument4);

		final OrderModel order4 = mock(OrderModel.class);
		Mockito.when(order4.getCode()).thenReturn(ORDER_ID4);
		Mockito.when(order4.getPsDocument()).thenReturn(documents4);
		Mockito.when(order4.getUser()).thenReturn(customer4);

		final File orderFolder4 = psDocumentManagementService.createByOrderDirectory(customer4, order4);
		Assert.assertNotNull(orderFolder4);
		final File orderInboundFolder4 = psDocumentManagementService.createByOrderInboundDirectory(customer4, order4);
		Assert.assertNotNull(orderInboundFolder4);
		final File orderOutboundFolder4 = psDocumentManagementService.createByOrderOutboundDirectory(customer4, order4);
		Assert.assertNotNull(orderOutboundFolder4);

		final File orderInboundThumbnailFolder4 = psDocumentManagementService.createByOrderInboundThumbnailDirectory(customer4,
				order4);
		Assert.assertNotNull(orderInboundThumbnailFolder4);
		final File orderOutboundThumbnailFolder4 = psDocumentManagementService.createByOrderOutboundThumbnailDirectory(customer4,
				order4);
		Assert.assertNotNull(orderOutboundThumbnailFolder4);
	}

	/**
	 * Test creation of customer2 folder.
	 */
	@Test
	public void testCreateCustomer5DocumentDirectory() throws IOException
	{
		//Customer5
		final CustomerModel customer5 = new MockCustomerModel5();
		customer5.setCustomerID(CUSTOMER_ID5);
		final File rootDir5 = psDocumentManagementService.createCustomerDocumentRootDirectory(customer5);
		Assert.assertNotNull(rootDir5);
		final File customerDir5 = psDocumentManagementService.createByCustomerDirectory(customer5);
		Assert.assertNotNull(customerDir5);
		final File customerThumbnailDir5 = psDocumentManagementService.createByCustomerThumbnailDirectory(customer5);
		Assert.assertNotNull(customerThumbnailDir5);

		final PSDocumentModel psDocument5 = mock(PSDocumentModel.class);
		Mockito.when(psDocument5.getDocId()).thenReturn(DOC_ID5);
		final Collection<PSDocumentModel> documents5 = Arrays.asList(psDocument5);

		final OrderModel order5 = mock(OrderModel.class);
		Mockito.when(order5.getCode()).thenReturn(ORDER_ID5);
		Mockito.when(order5.getPsDocument()).thenReturn(documents5);
		Mockito.when(order5.getUser()).thenReturn(customer5);

		final File orderFolder5 = psDocumentManagementService.createByOrderDirectory(customer5, order5);
		Assert.assertNotNull(orderFolder5);
		final File orderInboundFolder5 = psDocumentManagementService.createByOrderInboundDirectory(customer5, order5);
		Assert.assertNotNull(orderInboundFolder5);
		final File orderOutboundFolder5 = psDocumentManagementService.createByOrderOutboundDirectory(customer5, order5);
		Assert.assertNotNull(orderOutboundFolder5);

		final File orderInboundThumbnailFolder5 = psDocumentManagementService.createByOrderInboundThumbnailDirectory(customer5,
				order5);
		Assert.assertNotNull(orderInboundThumbnailFolder5);
		final File orderOutboundThumbnailFolder5 = psDocumentManagementService.createByOrderOutboundThumbnailDirectory(customer5,
				order5);
		Assert.assertNotNull(orderOutboundThumbnailFolder5);
	}
	
	

	@Test
	public void testGetDocumentsForUserRelationshipsByStatus()
	{

		final List<PSDocumentModel> docs = new ArrayList<>();
		Mockito.when(psDocumentManagementDao.getDocumentsForUserRelationshipsByStatus(sourceUser, new ComposedTypeModel(), false)).thenReturn(docs);
		Mockito.when(typeService.getComposedTypeForCode(PSDocumentModel._TYPECODE)).thenReturn(new ComposedTypeModel());

		Assert.assertNotNull(psDocumentManagementService.getDocumentsForUserRelationshipsByStatus(sourceUser, false));
	}
}
