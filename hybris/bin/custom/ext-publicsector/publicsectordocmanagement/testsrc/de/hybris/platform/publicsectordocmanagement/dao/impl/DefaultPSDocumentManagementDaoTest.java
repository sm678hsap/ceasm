/*
 * [y] hybris Platform

 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;


/**
 * Unit Testing for {@link PSDocumentManagementDaoImpl}
 */
@UnitTest
public class DefaultPSDocumentManagementDaoTest
{
	
	@Mock
	private ModelService modelService;

	@Mock
	private FlexibleSearchService flexibleSearchService;
	
	
	
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@InjectMocks
	private DefaultPSDocumentManagementDao publicSectorDocumentManagementDao;

	@Before
	public void setUp() throws ImpExException
	{
		Registry.activateMasterTenant();
		MockitoAnnotations.initMocks(this);
		publicSectorDocumentManagementDao.setModelService(modelService);
		publicSectorDocumentManagementDao.setFlexibleSearchService(flexibleSearchService);
	}

	@Test
	public void testFindActivePSDocumentByCustomerNull()
	{
		thrown.expect(IllegalArgumentException.class);
		publicSectorDocumentManagementDao.findActivePSDocumentModelByCustomer(null);
	}

	@Test
	public void testFindExpiredPSDocumentByCustomer()
	{
		thrown.expect(IllegalArgumentException.class);
		publicSectorDocumentManagementDao.findExpiredPSDocumentModelByCustomer(null);
	}
	
	
	@Test
	public void testGetDocumentsForUserRelationshipsByStatusNull()
	{
		thrown.expect(IllegalArgumentException.class);
		publicSectorDocumentManagementDao.getDocumentsForUserRelationshipsByStatus(null,null,false);
	}
	
	
	@Test
	public void testGetDocumentsForUserRelationshipsByStatus()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		final List<Object> list = new ArrayList();
		list.add(Mockito.mock(PSDocumentModel.class));
		Mockito.when(searchResult.getResult()).thenReturn(list);

		final UserModel user = new UserModel();
		user.setUid("TestUser");

		Assert.assertNotNull(publicSectorDocumentManagementDao.getDocumentsForUserRelationshipsByStatus(user, new ComposedTypeModel(),false));
	}
}
