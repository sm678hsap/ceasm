package de.hybris.platform.publicsectordocmanagement.interceptor;

import java.io.IOException;
import static org.mockito.BDDMockito.given;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;
import de.hybris.platform.publicsectorservices.encoder.PSEncoder;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PSDocumentPrepareInterceptorTest {
	
	private static final String ORDER_CODE="orderCode";
	
	private static final String FILE_NAME="fileName";
	
	private static final String DOC_ID="docId";
	
	private static final String ABSOLUTE_PATH="absolutePath";
	
	@InjectMocks
	private PSDocumentPrepareInterceptor psDocumentPrepareInterceptor;
	
	@Mock
	private PSDocumentManagementService psDocumentManagementService;

	@Mock
	private PSEncoder psEncoder;
	
	@Mock
	private PSDocumentModel psDocumentModel;
	
	@Mock
	private AbstractOrderModel abstractOrderModel;

	@Mock
	private CustomerModel customerModel;
	
	@Mock
	private File file;
	
	@Mock
	private InterceptorContext interceptor;
	
	@Mock
	private OrderModel order;
	
	@Before
	public void setUp()
	{
		psDocumentPrepareInterceptor=new PSDocumentPrepareInterceptor();
		psDocumentPrepareInterceptor.setPsDocumentManagementService(psDocumentManagementService);
		psDocumentPrepareInterceptor.setPsEncoder(psEncoder);
	}
	@Test
	public void testOnPrepare() throws InterceptorException,IOException
	{
		given(psDocumentModel.getOrder()).willReturn(abstractOrderModel);
		given(psDocumentModel.getOrder().getCode()).willReturn(ORDER_CODE);
		given(psDocumentModel.getCustomer()).willReturn(customerModel);
		given(file.getAbsolutePath()).willReturn(ABSOLUTE_PATH);
		given(psDocumentManagementService.createByOrderInboundDirectory(customerModel, order)).willReturn(file);
		given(psDocumentManagementService.createByOrderInboundThumbnailDirectory(customerModel, order)).willReturn(file);
		given(psDocumentModel.getFileName()).willReturn(FILE_NAME);
		given(psDocumentModel.getDocId()).willReturn(DOC_ID);
		psDocumentPrepareInterceptor.onPrepare(psDocumentModel, interceptor);
		
	}
}
