/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectordocmanagement.service.impex.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.translators.SingleValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;

import java.io.IOException;
import java.util.Locale;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;


/**
 * Implementation class of {@link PSFileCopyTranslator} This is file copy translator from source shard location to user
 * defined destination
 */
public class PSFileCopyTranslator extends SingleValueTranslator
{

	private static final Logger LOG = LoggerFactory.getLogger(PSFileCopyTranslator.class);
	private static final Splitter PATH_SPLITTER = Splitter.on('&');

	private static boolean initialized = false;

	private static final String DEFAULT_PS_DOCUMENT_MGMT_SERVICE = "psDocumentManagementService";
	private static PSDocumentManagementService psDocumentManagementService;

	@Override
	protected Object convertToJalo(final String codeAndPos, final Item item)
	{
		init();

		if (codeAndPos != null)
		{

			final String[] tokens = codeAndPos.split("\\|");
			if (tokens.length != 5)
			{
				LOG.warn(
						"Error reading line:: expected order id, file name, file path and flags (isThumbnail and isAbsolutePath) and got "
								+ tokens);
				return null;
			}

			// get the order id
			final String orderId = tokens[0];
			// get the source file name
			final String sourceFileName = tokens[1];
			//get the source file path
			String sourceFilePath = tokens[2];
			//get isThumbnail flag
			final Boolean documentFileType = Boolean.valueOf(tokens[3]);
			//get isAbsoluteFlag flag
			final Boolean isAbsoluteFilePath = Boolean.valueOf(tokens[4]);

			if (isJarBasedPath(sourceFilePath))
			{
				sourceFilePath = getFilePath(sourceFilePath);
			}
			try
			{
				if (BooleanUtils.isTrue(documentFileType))
				{
					psDocumentManagementService.copyOrderDocumentThumbnail(orderId, sourceFileName, sourceFilePath,
							isAbsoluteFilePath);
				}
				else
				{
					psDocumentManagementService.copyOrderDocument(orderId, sourceFileName, sourceFilePath, isAbsoluteFilePath);
				}
				return Boolean.TRUE;
			}
			catch (final IOException e)
			{
				LOG.error("Exception occurred during file copy due to : ", e);
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	private String removeControlPrefixFromPath(final String controlPrefix, final String fullPath)
	{
		return fullPath.substring(controlPrefix.length(), fullPath.length()).trim();
	}

	private String getFilePath(final String jarPath)
	{
		final String path = removeControlPrefixFromPath("jar:", jarPath);

		String resourceName;
		Class<?> classLoaderClass = null;
		if (containsCustomClassloaderClass(path))
		{
			final Iterable<String> splitResult = PATH_SPLITTER.split(path);
			Preconditions.checkState(Iterables.size(splitResult) == 2);


			final String classLoaderClassToken = Iterables.get(splitResult, 0);
			try
			{
				classLoaderClass = Class.forName(classLoaderClassToken);
			}
			catch (final ClassNotFoundException e)
			{
				LOG.error(e.getMessage(), e);
			}
			Preconditions.checkState(StringUtils.isNotBlank(classLoaderClassToken));
			resourceName = FilenameUtils.separatorsToUnix(Iterables.get(splitResult, 1));
		}
		else
		{
			resourceName = FilenameUtils.separatorsToUnix(path);
		}

		if (classLoaderClass != null)
		{
			return classLoaderClass.getResource(resourceName).getPath();
		}
		return null;
	}

	private boolean containsCustomClassloaderClass(final String path)
	{
		return path.contains("&");
	}

	@Override
	protected String convertToString(final Object o)
	{
		return null;
	}

	protected boolean isJarBasedPath(final String path)
	{
		return path.toLowerCase(Locale.ENGLISH).startsWith("jar:");
	}

	/**
	 * Statically initialize the variables, loading necessary services only once.
	 */
	protected static void init()
	{
		if (!initialized)
		{
			psDocumentManagementService = (PSDocumentManagementService) Registry.getApplicationContext()
					.getBean(DEFAULT_PS_DOCUMENT_MGMT_SERVICE);
			initialized = true;
		}
	}
}
