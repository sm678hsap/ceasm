/*
 * [y] hybris Platform

 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagement.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectordocmanagement.dao.PSDocumentManagementDao;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;
import de.hybris.platform.publicsectorservices.model.PSServiceProductModel;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;


/**
 * Implementation class of {@link PSDocumentManagementService}
 */
public class DefaultPSDocumentManagementService implements PSDocumentManagementService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPSDocumentManagementService.class.getName());


	private static final String DOC_HASH_CODE = "docCode";
	private static final String DOCUMENT_TOKEN = "psSecureDocument";
	private static final String DOCUMENT_THUMBNAIL_FOLDER = "document.thumbnail.folder";
	private static final String DOCUMENT_SHARED_FOLDER_PATH = "document.shared.folder.path";
	private static final String DOCUMENT_MODULO = "document.modulo";
	private static final String DOCUMENT_OUTBOUND_FOLDER = "document.outbound.folder";
	private static final String DOCUMENT_INBOUND_FOLDER = "document.inbound.folder";
	private static final String DOCUMENT_BY_ORDER_FOLDER = "document.byOrder.folder";
	private static final String DOCUMENT_BY_CUSTOMER_FOLDER = "document.byCustomer.folder";
	private static final String DOCUMENT_ROOT_FOLDER = "document.root.folder";

	static final String ROOTDIR = Config.getParameter(DOCUMENT_ROOT_FOLDER);
	static final String BYCUSTOMER = Config.getParameter(DOCUMENT_BY_CUSTOMER_FOLDER);
	static final String BYORDER = Config.getParameter(DOCUMENT_BY_ORDER_FOLDER);
	static final String INBOUND = Config.getParameter(DOCUMENT_INBOUND_FOLDER);
	static final String OUTBOUND = Config.getParameter(DOCUMENT_OUTBOUND_FOLDER);
	static final String MODULO = Config.getParameter(DOCUMENT_MODULO);
	static final String SHARED_FOLDER_PATH = Config.getParameter(DOCUMENT_SHARED_FOLDER_PATH);
	static final String THUMBNAIL = Config.getParameter(DOCUMENT_THUMBNAIL_FOLDER);

	private UserService userService;
	private PSDocumentManagementDao psDocumentManagementDao;
	private TypeService typeService;

	@Override
	public File createCustomerDocumentRootDirectory(final CustomerModel customer) throws IOException
	{
		final int customerPK = customer.getPk().getLong().intValue();
		final int modDir = customerPK % Integer.parseInt(MODULO);

		return createDirectory(ROOTDIR, Math.abs(modDir) + File.separator + customer.getPk().toString());
	}

	@Override
	public File createByCustomerDirectory(final CustomerModel customer) throws IOException
	{
		final File customerDir = createCustomerDocumentRootDirectory(customer);
		if (customerDir != null)
		{
			return createDirectory(customerDir.getAbsolutePath(), BYCUSTOMER);
		}
		LOG.warn("Unable to create the directory by customer");
		return null;
	}

	@Override
	public File createByCustomerThumbnailDirectory(final CustomerModel customer) throws IOException
	{
		final File customerRootDir = createCustomerDocumentRootDirectory(customer);
		if (customerRootDir != null)
		{
			final File customerDir = createDirectory(customerRootDir.getAbsolutePath(), BYCUSTOMER);
			return createDirectory(customerDir.getAbsolutePath(), THUMBNAIL);
		}
		LOG.warn("Unable to create the thumbnail directory by customer");
		return null;
	}

	@Override
	public File createByOrderDirectory(final CustomerModel customer, final OrderModel order) throws IOException
	{
		final File customerDir = createCustomerDocumentRootDirectory(customer);
		if (customerDir != null)
		{
			final File byOrderDir = new File(customerDir.getAbsolutePath(), BYORDER + File.separator + order.getCode());
			if (!byOrderDir.exists())
			{
				boolean dirCreated = byOrderDir.mkdirs();
				if (dirCreated)
				{
					LOG.info("Order directory created");;
				}
			}
			return byOrderDir;
		}
		LOG.warn("Unable to create the directory by order");
		return null;
	}

	@Override
	public File createByOrderInboundDirectory(final CustomerModel customer, final OrderModel order) throws IOException
	{
		final File byOrderDir = createByOrderDirectory(customer, order);
		if (byOrderDir != null)
		{
			return createDirectory(byOrderDir.getAbsolutePath(), INBOUND);
		}
		LOG.warn("Unable to create the inbound directory by order");
		return null;
	}

	@Override
	public File createByOrderOutboundDirectory(final CustomerModel customer, final OrderModel order) throws IOException
	{
		final File byOrderDir = createByOrderDirectory(customer, order);
		if (byOrderDir != null)
		{
			return createDirectory(byOrderDir.getAbsolutePath(), OUTBOUND);
		}
		LOG.warn("Unable to create the outbound directory by order");
		return null;
	}

	@Override
	public File createByOrderInboundThumbnailDirectory(final CustomerModel customer, final OrderModel order) throws IOException
	{
		final File byOrderDir = createByOrderDirectory(customer, order);
		if (byOrderDir != null)
		{
			final File inboundDir = createDirectory(byOrderDir.getAbsolutePath(), INBOUND);
			return createDirectory(inboundDir.getAbsolutePath(), THUMBNAIL);
		}
		LOG.warn("Unable to create the thumbnail inbound directory by order");
		return null;
	}

	@Override
	public File createByOrderOutboundThumbnailDirectory(final CustomerModel customer, final OrderModel order) throws IOException
	{
		final File byOrderDir = createByOrderDirectory(customer, order);
		if (byOrderDir != null)
		{
			final File outboundDir = createDirectory(byOrderDir.getAbsolutePath(), OUTBOUND);
			return createDirectory(outboundDir.getAbsolutePath(), THUMBNAIL);
		}
		LOG.warn("Unable to create the thumbnail outbound directory by order");
		return null;
	}

	/**
	 * Method to create directory if it doesn't exits
	 *
	 * @param parent
	 * @param child
	 * @return Directory
	 */
	private File createDirectory(final String parent, final String child)
	{
		final File inboundDir = new File(parent, child);
		if (!inboundDir.exists())
		{
			boolean dirCreated = inboundDir.mkdirs();
			if (dirCreated)
			{
				LOG.info("Directory is created");
			}
		}
		return inboundDir;
	}

	@Override
	public List<PSDocumentModel> findActivePSDocumentByCustomer(final CustomerModel customer)
	{
		if (customer != null)
		{
			return getPsDocumentManagementDao().findActivePSDocumentModelByCustomer(customer.getPk().toString());
		}
		LOG.warn("Given customer is null!! ");
		return Collections.<PSDocumentModel> emptyList();
	}

	@Override
	public List<PSDocumentModel> findExpiredPSDocumentByCustomer(final CustomerModel customer)
	{
		if (customer != null)
		{
			return getPsDocumentManagementDao().findExpiredPSDocumentModelByCustomer(customer.getPk().toString());
		}
		LOG.warn("Given customer is null!! ");
		return Collections.<PSDocumentModel> emptyList();
	}

	@Override
	public void copyOrderDocument(final String orderCode, final String sourceFileName, final String sourceFilePath,
			final Boolean isAbsolutePath) throws IOException
	{
		final OrderModel order = findOrderbyCode(orderCode);
		if (order != null)
		{
			final CustomerModel customer = (CustomerModel) order.getUser();
			final String sourcePath = getSourcePath(sourceFileName, sourceFilePath, isAbsolutePath);
			if (customer != null)
			{
				final String destinationPath = createByOrderInboundDirectory(customer, order).getAbsolutePath() + File.separator
						+ sourceFileName;
				copyFileFromSourceToDestination(sourcePath, destinationPath);
			}
		}
		else
		{
			LOG.error("Order {} does not exist." ,orderCode);
		}
	}

	@Override
	public void copyOrderDocumentThumbnail(final String orderCode, final String sourceFileName, final String sourceFilePath,
			final Boolean isAbsolutePath) throws IOException

	{
		final OrderModel order = findOrderbyCode(orderCode);
		if (order != null)
		{
			final CustomerModel customer = (CustomerModel) order.getUser();
			final String sourcePath = getSourcePath(sourceFileName, sourceFilePath, isAbsolutePath);
			if (customer != null)
			{
				final String destinationPath = createByOrderInboundThumbnailDirectory(customer, order).getAbsolutePath()
						+ File.separator + sourceFileName;
				copyFileFromSourceToDestination(sourcePath, destinationPath);
			}
		}
		else
		{
		    LOG.error("Order {} does not exist." ,orderCode);
		}
	}

	@Override
	public void copyCustomerDocument(final String customerId, final String sourceFileName, final String sourceFilePath,
			final Boolean isAbsoluteFilePath) throws IOException
	{
		final CustomerModel customer = (CustomerModel) userService.getUserForUID(customerId);
		final String sourcePath = getSourcePath(sourceFileName, sourceFilePath, isAbsoluteFilePath);
		if (customer != null)
		{
			final String destinationPath = createByCustomerDirectory(customer).getAbsolutePath() + File.separator + sourceFileName;
			copyFileFromSourceToDestination(sourcePath, destinationPath);
		}
	}

	@Override
	public void copyCustomerDocumentThumbnail(final String customerId, final String orderCode, final String sourceFileName,
			final String sourceFilePath, final Boolean isAbsoluteFilePath) throws IOException
	{
		final CustomerModel customer = (CustomerModel) userService.getUserForUID(customerId);
		final String sourcePath = getSourcePath(sourceFileName, sourceFilePath, isAbsoluteFilePath);
		if (customer != null)
		{
			final String destinationPath = createByCustomerThumbnailDirectory(customer).getAbsolutePath() + File.separator
					+ sourceFileName;
			copyFileFromSourceToDestination(sourcePath, destinationPath);
		}
	}

	private String getSourcePath(final String sourceFileName, final String sourceFilePath, final Boolean isAbsolutePath)
	{
		String sourcePath = "";
		if (!isAbsolutePath)
		{
			sourcePath = SHARED_FOLDER_PATH;
		}
		sourcePath = sourcePath + sourceFilePath + sourceFileName;
		return sourcePath;
	}

	protected void copyFileFromSourceToDestination(final String sourcePath, final String destinationPath) throws IOException
	{
		final File sourceThumbnail = new File(sourcePath);
		final File destThumbnail = new File(destinationPath);
		Files.copy(sourceThumbnail.toPath(), destThumbnail.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}

	@Override
	public PSDocumentModel findDocumentByDocumentSecurePathUrlId(final String securePathURLId)
	{
		return getPsDocumentManagementDao().findDocumentByDocumentSecurePathUrlId(securePathURLId);
	}

	@Override
	public String getDownloadUrlForDocument(final String securePathURLId) throws UnsupportedEncodingException
	{
		return DOCUMENT_TOKEN + "?" + DOC_HASH_CODE + "=" + URLEncoder.encode(securePathURLId, "UTF-8");
	}

	@Override
	public PSDocumentModel findDocumentByDocId(final String documentId)
	{
		return getPsDocumentManagementDao().findDocumentByDocId(documentId);
	}

	@Override
	public CustomerModel findCustomerByUid(final String customerUid)
	{
		return (CustomerModel) getUserService().getUserForUID(customerUid);
	}

	@Override
	public OrderModel findOrderbyCode(final String orderCode)
	{
		return getPsDocumentManagementDao().findOrderbyCode(orderCode);
	}
	
	
	@Override
	public List<PSDocumentModel> getDocumentsForUserRelationshipsByStatus(final UserModel user, boolean expired)
	{
		return getPsDocumentManagementDao().getDocumentsForUserRelationshipsByStatus(user,
				getTypeService().getComposedTypeForCode(PSDocumentModel._TYPECODE), expired);
	}
	
	@Override
	public List<PSDocumentModel> getDocumentsForUserRelationships(final UserModel user)
	{
		return getPsDocumentManagementDao().getDocumentsForUserRelationships(user,
				getTypeService().getComposedTypeForCode(PSDocumentModel._TYPECODE));
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected PSDocumentManagementDao getPsDocumentManagementDao()
	{
		return psDocumentManagementDao;
	}

	@Required
	public void setPsDocumentManagementDao(final PSDocumentManagementDao psDocumentManagementDao)
	{
		this.psDocumentManagementDao = psDocumentManagementDao;
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(TypeService typeService)
	{
		this.typeService = typeService;
	}
	
	
}
