/*
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *
 */
package de.hybris.platform.publicsectordocmanagement.interceptor;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentThumbnailModel;
import de.hybris.platform.publicsectordocmanagement.service.PSDocumentManagementService;
import de.hybris.platform.publicsectordocmanagement.service.impex.impl.PSFileCopyTranslator;
import de.hybris.platform.publicsectorservices.encoder.PSEncoder;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.io.File;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Interceptor to prepare and store file path and secure url id of the document
 */
public class PSDocumentPrepareInterceptor implements PrepareInterceptor<PSDocumentModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(PSDocumentPrepareInterceptor.class);

	private PSDocumentManagementService psDocumentManagementService;

	private PSEncoder psEncoder;

	@Override
	public void onPrepare(final PSDocumentModel psDocumentModel, final InterceptorContext ctx) throws InterceptorException
	{
		final StringBuilder absoluteFilePath = new StringBuilder();
		if (psDocumentModel.getOrder() != null) {
			try {

				final OrderModel order = psDocumentManagementService.findOrderbyCode(psDocumentModel.getOrder().getCode());

				if (psDocumentModel instanceof PSDocumentThumbnailModel) {
					final File inboundThumbnailFolder = psDocumentManagementService
							.createByOrderInboundThumbnailDirectory(psDocumentModel.getCustomer(), order);
					absoluteFilePath.append(inboundThumbnailFolder.getAbsolutePath());
					psDocumentModel.setFilePath(absoluteFilePath.toString());
				} else {
					final File inboundFolder = psDocumentManagementService.createByOrderInboundDirectory(psDocumentModel.getCustomer(),
							order);
					absoluteFilePath.append(inboundFolder.getAbsolutePath());
					psDocumentModel.setFilePath(absoluteFilePath.toString());
				}
			} catch (final IOException e) {
				LOG.error("Exception occurred during file handling due to : ", e);
			}
		}
		absoluteFilePath.append("/").append(psDocumentModel.getFileName());

		psDocumentModel.setSecurePathURLId(psEncoder.encrypt(absoluteFilePath.toString()));

	}

	@Required
	public void setPsDocumentManagementService(final PSDocumentManagementService psDocumentManagementService)
	{
		this.psDocumentManagementService = psDocumentManagementService;
	}

	@Required
	public void setPsEncoder(final PSEncoder psEncoder)
	{
		this.psEncoder = psEncoder;
	}
}
