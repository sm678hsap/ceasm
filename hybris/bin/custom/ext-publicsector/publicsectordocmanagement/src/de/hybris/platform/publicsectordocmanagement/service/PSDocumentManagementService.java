/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagement.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;


/**
 * Interface class for Public Sector Document Management.
 */
public interface PSDocumentManagementService
{
	/**
	 * Creates folder root directory per customer.
	 *
	 * @param customer
	 *        	CustomerModel for customer details
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error         
	 * @return File the customer root directory.
	 */
	public File createCustomerDocumentRootDirectory(final CustomerModel customer) throws IOException;

	/**
	 * Creates byCustomer folder directory.
	 *
	 * @param customer
	 *       	CustomerModel for customer details
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error       
	 * @return File the byCustomer folder
	 */
	public File createByCustomerDirectory(final CustomerModel customer) throws IOException;

	/**
	 * Creates byCustomer->thumbnail folder directory.
	 *
	 * @param customer
	 *          CustomerModel for customer details
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error           
	 * @return File the byCustomer-thumbnail folder
	 */
	public File createByCustomerThumbnailDirectory(final CustomerModel customer) throws IOException;

	/**
	 * Creates byOrder folder directory.
	 * 
	 * @param customer
	 *          CustomerModel for customer details
	 * @param order
	 *        	OrderModel for order details
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error            
	 * @return File the byOrder folder
	 */
	public File createByOrderDirectory(final CustomerModel customer, final OrderModel order) throws IOException;

	/**
	 * Creates byOrder->inbound folder directory.
	 * 
	 * @param customer
	 *        	CustomerModel for customer details
	 * @param order
	 *        	OrderModel for order details
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error             
	 * @return File the byOrder-inbound folder
	 */
	public File createByOrderInboundDirectory(final CustomerModel customer, final OrderModel order) throws IOException;

	/**
	 * Creates byOrder->outbound folder directory.
	 * 
	 * @param customer
	 *        	 CustomerModel for customer details
	 * @param order
	 *         	 OrderModel for order details
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error      
	 * @return File the byOrder-outbound folder
	 */
	public File createByOrderOutboundDirectory(final CustomerModel customer, final OrderModel order) throws IOException;

	/**
	 * Creates byOrder->inbound->thumbnail folder directory
	 * 
	 * @param customer
	 *        	CustomerModel for customer details
	 * @param order
	 *       	 OrderModel for order details
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error      
	 * @return File byOrder-inbound-thumbnail
	 */
	public File createByOrderInboundThumbnailDirectory(final CustomerModel customer, final OrderModel order) throws IOException;

	/**
	 * Creates byOrder->outbound->thumbnail folder directory
	 * 
	 * @param customer
	 *        	CustomerModel for customer details
	 * @param order
	 *        	OrderModel for order details
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error      
	 * @return File byOrder-outbound-thumbnail
	 */
	public File createByOrderOutboundThumbnailDirectory(final CustomerModel customer, final OrderModel order) throws IOException;

	/**
	 * Copy a customer document from source location to destination
	 *
	 * @param orderCode
	 *        	 order id for document
	 * @param sourceFileName
	 *        	 source file name for document
	 * @param sourceFilePath
	 *        	 source file path for document
	 * @param isAbsoluteFilePath
	 *        	flag is true if the path is absolute otherwise false
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error or copying files faces some issue    
	 */
	public void copyOrderDocument(final String orderCode, final String sourceFileName, String sourceFilePath,
			final Boolean isAbsoluteFilePath) throws IOException;

	/**
	 * Copy a customer document from source location to destination
	 *
	 * @param customerId
	 *        	 customer id
	 * @param sourceFileName
	 *        	 source file name for document
	 * @param sourceFilePath
	 *        	 source file path for document
	 * @param isAbsoluteFilePath
	 *        	flag is true if the path is absolute otherwise false
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error or copying files faces some issue   
	 */
	public void copyCustomerDocument(final String customerId, final String sourceFileName, String sourceFilePath,
			final Boolean isAbsoluteFilePath) throws IOException;

	/**
	 * Copy a customer document's thumbnail location from source to destination
	 *
	 * @param customerId
	 *        	 customer id
	 * @param orderCode
	 *        	 order id for document
	 * @param sourceFileName
	 *        	 source file name for document
	 * @param sourceFilePath
	 *        	 source file path for document
	 * @param isAbsoluteFilePath
	 *        	flag is true if the path is absolute otherwise false
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error or copying files faces some issue  
	 */
	public void copyCustomerDocumentThumbnail(final String customerId, final String orderCode, final String sourceFileName,
			final String sourceFilePath, final Boolean isAbsoluteFilePath) throws IOException;

	/**
	 * Copy an order's document's thumbnail location from source to destination
	 *
	 * @param orderCode
	 *        	 order id for document
	 * @param sourceFileName
	 *        	 source file name for document
	 * @param sourceFilePath
	 *        	 source file path for document
	 * @param isAbsoluteFilePath
	 *        	flag is true if the path is absolute otherwise false
	 * @throws IOException 
	 *         	Exception occurs if customer root directory creation encounters an error or copying files faces some issue  
	 */
	public void copyOrderDocumentThumbnail(final String orderCode, final String sourceFileName, final String sourceFilePath,
			final Boolean isAbsoluteFilePath) throws IOException;

	/**
	 * Retrieves Active Public Sector Documents by Customer.
	 *
	 * @param customer
	 *       	 CustomerModel for customer details 
	 * @return List<PSDocumentModel> of active documents specific to customer
	 */
	public List<PSDocumentModel> findActivePSDocumentByCustomer(final CustomerModel customer);

	/**
	 * Retrieves Expired Public Sector Documents by Customer.
	 *
	 * @param customer
	 *       	 CustomerModel for customer details 
	 * @return List<PSDocumentModel> of expired documents specific to customer 
	 */
	public List<PSDocumentModel> findExpiredPSDocumentByCustomer(final CustomerModel customer);

	/**
	 * Retrieves document for the given secure document path URL ID.
	 *
	 * @param securePathURLId
	 *        	 secure path url id
	 * @return PSDocumentModel for document details specific to document secure path url id
	 */
	public PSDocumentModel findDocumentByDocumentSecurePathUrlId(final String securePathURLId);

	/**
	 * Retrieves document download url for the given secure path URL ID.
	 *
	 * @param securePathURLId
	 *        	 secure path url id
	 * @throws UnsupportedEncodingException
	 *         	Exception occurs when url encoding encounters an error
	 * @return String representing a download url for document
	 */
	public String getDownloadUrlForDocument(String securePathURLId) throws UnsupportedEncodingException;

	/**
	 * Retrieves Public Sector Document using document code.
	 *
	 * @param documentId
	 *           document id.
	 * @return PSDocumentModel for document details specific to document id
	 */
	public PSDocumentModel findDocumentByDocId(final String documentId);

	/**
	 * Retrieves Public Sector Document's associated customer using customer uid.
	 *
	 * @param customerUid
	 *           customer's uid.
	 * @return CustomerModel for customer details specific to customer uid
	 */
	public CustomerModel findCustomerByUid(final String customerUid);

	/**
	 * Retrieves Order by order code.
	 *
	 * @param orderCode
	 *            order code
	 * @return OrdeModel for order details specific to a order code
	 */
	public OrderModel findOrderbyCode(final String orderCode);

	/**
	 * Retrieves users relationships documents based on document expiry status
	 *
	 * @param user
	 *       	 UserModel for user details
	 * @param expired
	 *           flag returns true if the document is expired
	 * @return List<PSDocumentModel> of documents for user relationships
	 */
	List<PSDocumentModel> getDocumentsForUserRelationshipsByStatus(UserModel user, boolean expired);
	
	/**
	 * Retrieves users relationships documents.
	 *
	 * @param user
	 *       	 UserModel for user details
	 * @return List<PSDocumentModel> of documents for user relationships
	 */
	List<PSDocumentModel> getDocumentsForUserRelationships(UserModel user);
}
