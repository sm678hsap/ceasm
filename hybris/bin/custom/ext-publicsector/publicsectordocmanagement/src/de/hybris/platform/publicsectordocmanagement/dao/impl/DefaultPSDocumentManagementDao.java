/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagement.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.publicsectordocmanagement.dao.PSDocumentManagementDao;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.relationshipservices.enums.PSPermissionStatus;
import de.hybris.platform.relationshipservices.enums.PSRelationshipStatus;
import de.hybris.platform.relationshipservices.model.AbstractPSPermissibleAreaModel;
import de.hybris.platform.relationshipservices.model.PSPermissibleAreaItemTypeModel;
import de.hybris.platform.relationshipservices.model.PSPermissionModel;
import de.hybris.platform.relationshipservices.model.PSRelationshipModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;


/**
 * Implementation class of {@link PSDocumentManagementDao}
 */
public class DefaultPSDocumentManagementDao extends AbstractItemDao implements PSDocumentManagementDao
{
	private static final int ZERO = 0;

	//minor sonar recommendations
	private static final String DATE_FORMAT_PARAM = "yyyy-MM-dd hh:mm:ss";
	private static final String CURRENT_DATE_PARAM_NAME = "currentDate";

	private static final String USER = "user";
	private static final String SHAREABLE_TYPE = "shareableType";
	private static final String STATUS = "status";
	private static final String PERMISSION_STATUS = "permissionStatus";
	private static final String ACTIVE = "active";
	private static final String SOURCE_POA_HOLDER = "isSourcePoaHolder";
	private static final String TARGET_POA_HOLDER = "isTargetPoaHolder";

	private static final String SECURE_PATH = "securePathURLId";
	private static final String CUSTOMER_PK = "customerPk";
	private static final String DOCUMENT_ID = "documentId";
	private static final String ORDER_CODE = "orderCode";

	protected static final String SELECTCLAUSE = "SELECT {" + PSDocumentModel.PK + "} " + "FROM {" + PSDocumentModel._TYPECODE
			+ "! AS p} ";

	protected static final String SELECTCLAUSE_PSDOCUMENT_CUSTOMER_JOIN = "SELECT {" + PSDocumentModel.PK + "} " + "FROM {"
			+ PSDocumentModel._TYPECODE + "! AS p " + "JOIN " + CustomerModel._TYPECODE + " AS c ON {p:" + PSDocumentModel.CUSTOMER
			+ "}={c:" + CustomerModel.PK + "}} ";

	protected static final String ACTIVE_PSDOCUMENT_QUERY = SELECTCLAUSE_PSDOCUMENT_CUSTOMER_JOIN + "WHERE {c:" + CustomerModel.PK
			+ "}=?customerPk AND ({ p:" + PSDocumentModel.EXPIRESON + "} >= ?currentDate OR {p:" + PSDocumentModel.EXPIRESON
			+ "} IS NULL) ORDER BY { p:" + PSDocumentModel.ISSUEDON + "} DESC";

	protected static final String EXPIRED_PSDOCUMENT_QUERY = SELECTCLAUSE_PSDOCUMENT_CUSTOMER_JOIN + "WHERE {c:" + CustomerModel.PK
			+ "}=?customerPk AND { p:" + PSDocumentModel.EXPIRESON + "} <= ?currentDate";

	protected static final String PSDOCUMENT_QUERY_BY_SECUREPATHURLID = SELECTCLAUSE + "WHERE  { p:"
			+ PSDocumentModel.SECUREPATHURLID + "} = ?securePathURLId";

	protected static final String PSDOCUMENT_QUERY_BY_DOCID = SELECTCLAUSE + "WHERE  { p:" + PSDocumentModel.DOCID
			+ "} = ?documentId";

	protected static final String PSDOCUMENT_ORDER_BY_ID = "SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE
			+ "} WHERE {" + AbstractOrder.CODE + "} = ?orderCode ORDER BY {" + OrderModel.PK + "} ASC";

	private static final String CUSTOMERID_NOT_NULL = "customerId must not be null";
	
	
	private static final String SELECT_PERMISSIBLE_ITEM = "SELECT {" + PSPermissibleAreaItemTypeModel.PK + "} FROM {"
			+ PSPermissibleAreaItemTypeModel._TYPECODE + "}" + " WHERE {" + AbstractPSPermissibleAreaModel.ACTIVE + "} = ?active"
			+ " AND {" + PSPermissibleAreaItemTypeModel.SHAREABLETYPE + "} =?shareableType";
	
	private static final String FETCH_POA_RECEIVERS_FOR_USER = "SELECT ( CASE WHEN {" + PSRelationshipModel.SOURCEUSER
			+ "} = ?user" + " THEN {" + PSRelationshipModel.TARGETUSER + "} ELSE {" + PSRelationshipModel.SOURCEUSER
			+ "} END ) AS usr_rel_per FROM" + " {" + PSRelationshipModel._TYPECODE + "} WHERE ( ({" + PSRelationshipModel.SOURCEUSER
			+ "} = ?user AND {" + PSRelationshipModel.ISSOURCEPOAHOLDER + "} = ?isSourcePoaHolder)" + " OR ({" + PSRelationshipModel.TARGETUSER
			+ "} = ?user AND {" + PSRelationshipModel.ISTARGETPOAHOLDER + "} = ?isTargetPoaHolder) )" + " AND {"
			+ PSRelationshipModel.RELATIONSHIPSTATUS + "} = ?status";

	private static final String FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE = "SELECT uniontable.usr_rel_per FROM ( {{ "
			+ FETCH_POA_RECEIVERS_FOR_USER + " }} UNION ALL {{ SELECT {" + PSPermissionModel.SOURCEUSER + "} AS usr_rel_per FROM {"
			+ PSPermissionModel._TYPECODE + "} WHERE {" + PSPermissionModel.TARGETUSER + "} = ?user AND {"
			+ PSPermissionModel.PERMISSIBLEAREAITEMTYPE + "} = ( {{ " + SELECT_PERMISSIBLE_ITEM + " }} ) AND {"
			+ PSPermissionModel.PERMISSIONSTATUS + "} = ?permissionStatus }} ) uniontable";

	protected static final String FETCH_EXPIRED_DOCUMENTS_COUNT_FOR_USER_RELATIONSHIPS = SELECTCLAUSE_PSDOCUMENT_CUSTOMER_JOIN + "WHERE {c:" + CustomerModel.PK
			+ "} IN ( {{ "
			+ FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE + " }} ) AND { p:" + PSDocumentModel.EXPIRESON + "} <= ?currentDate";

	protected static final String FETCH_DOCUMENTS_COUNT_FOR_USER_RELATIONSHIPS = SELECTCLAUSE_PSDOCUMENT_CUSTOMER_JOIN + "WHERE {c:" + CustomerModel.PK
			+ "} IN ( {{ "
			+ FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE + " }} )";

	protected static final String FETCH_ACTIVE_DOCUMENTS_COUNT_FOR_USER_RELATIONSHIPS = SELECTCLAUSE_PSDOCUMENT_CUSTOMER_JOIN + "WHERE {c:" + CustomerModel.PK
			+ "} IN ( {{ "
			+ FETCH_USER_RELATIONS_WITH_PERMISSION_TO_SHAREABLE_TYPE + " }} ) AND ({ p:" + PSDocumentModel.EXPIRESON + "} >= ?currentDate OR {p:" + PSDocumentModel.EXPIRESON
			+ "} IS NULL) ORDER BY { p:" + PSDocumentModel.ISSUEDON + "} DESC";
	
	private static final String USER_NOT_NULL = "User should not be null";

	@Override
	public List<PSDocumentModel> getDocumentsForUserRelationshipsByStatus(final UserModel user, final ComposedTypeModel serviceProductComposedType, boolean expired)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(USER, user);
		params.put(SHAREABLE_TYPE, serviceProductComposedType);

		params.put(ACTIVE, Boolean.TRUE);
		params.put(SOURCE_POA_HOLDER, Boolean.TRUE);
		params.put(TARGET_POA_HOLDER, Boolean.TRUE);

		params.put(STATUS, PSRelationshipStatus.ACTIVE);
		params.put(PERMISSION_STATUS, PSPermissionStatus.ACTIVE);

		final SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PARAM);
		params.put(CURRENT_DATE_PARAM_NAME, df.format(new Date()));
		
		FlexibleSearchQuery query = null;
		if(expired){
			query = new FlexibleSearchQuery(FETCH_EXPIRED_DOCUMENTS_COUNT_FOR_USER_RELATIONSHIPS);
		}else{
			query = new FlexibleSearchQuery(FETCH_ACTIVE_DOCUMENTS_COUNT_FOR_USER_RELATIONSHIPS);
		}
		query.addQueryParameters(params);

		final SearchResult<PSDocumentModel> documents = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(documents.getResult()) ? documents.getResult() : null;
	}

	@Override
	public List<PSDocumentModel> getDocumentsForUserRelationships(final UserModel user, final ComposedTypeModel serviceProductComposedType)
	{
		ServicesUtil.validateParameterNotNull(user, USER_NOT_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(USER, user);
		params.put(SHAREABLE_TYPE, serviceProductComposedType);

		params.put(ACTIVE, Boolean.TRUE);
		params.put(SOURCE_POA_HOLDER, Boolean.TRUE);
		params.put(TARGET_POA_HOLDER, Boolean.TRUE);

		params.put(STATUS, PSRelationshipStatus.ACTIVE);
		params.put(PERMISSION_STATUS, PSPermissionStatus.ACTIVE);
		
		final SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PARAM);
		params.put(CURRENT_DATE_PARAM_NAME, df.format(new Date()));
		
		FlexibleSearchQuery query = null;
		query = new FlexibleSearchQuery(FETCH_DOCUMENTS_COUNT_FOR_USER_RELATIONSHIPS);
		query.addQueryParameters(params);

		final SearchResult<PSDocumentModel> documents = getFlexibleSearchService().search(query);
		return CollectionUtils.isNotEmpty(documents.getResult()) ? documents.getResult() : null;
	}

	@Override
	public List<PSDocumentModel> findActivePSDocumentModelByCustomer(final String customerPk)
	{
		ServicesUtil.validateParameterNotNull(customerPk, CUSTOMERID_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(CUSTOMER_PK, customerPk);

		final SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PARAM);
		params.put(CURRENT_DATE_PARAM_NAME, df.format(new Date()));

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(ACTIVE_PSDOCUMENT_QUERY, params);
		final SearchResult<PSDocumentModel> result = getFlexibleSearchService().search(fsq);

		return result.getCount() > 0 ? result.getResult() : null;
	}

	@Override
	public List<PSDocumentModel> findExpiredPSDocumentModelByCustomer(final String customerPk)
	{
		ServicesUtil.validateParameterNotNull(customerPk, CUSTOMERID_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(CUSTOMER_PK, customerPk);

		final SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PARAM);
		params.put(CURRENT_DATE_PARAM_NAME, df.format(new Date()));

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(EXPIRED_PSDOCUMENT_QUERY, params);
		final SearchResult<PSDocumentModel> result = getFlexibleSearchService().search(fsq);

		return result.getCount() > 0 ? result.getResult() : null;
	}

	@Override
	public PSDocumentModel findDocumentByDocumentSecurePathUrlId(final String securePathURLId)
	{
		ServicesUtil.validateParameterNotNull(securePathURLId, CUSTOMERID_NOT_NULL);

		final Map<String, Object> params = new HashMap<>();
		params.put(SECURE_PATH, securePathURLId);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PSDOCUMENT_QUERY_BY_SECUREPATHURLID, params);
		final SearchResult<PSDocumentModel> result = getFlexibleSearchService().search(fsq);

		return result.getCount() > ZERO ? result.getResult().get(0) : null;
	}

	@Override
	public PSDocumentModel findDocumentByDocId(final String documentId)
	{
		ServicesUtil.validateParameterNotNull(documentId, "document id must not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put(DOCUMENT_ID, documentId);

		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(PSDOCUMENT_QUERY_BY_DOCID, params);
		final SearchResult<PSDocumentModel> result = getFlexibleSearchService().search(fsq);

		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

	@Override
	public OrderModel findOrderbyCode(final String orderCode)
	{
		ServicesUtil.validateParameterNotNull(orderCode, "order code must not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put(ORDER_CODE, orderCode);

		final List<OrderModel> orders = getFlexibleSearchService().<OrderModel> search(PSDOCUMENT_ORDER_BY_ID, params).getResult();
		return orders.isEmpty() ? null : orders.iterator().next();
	}
}
