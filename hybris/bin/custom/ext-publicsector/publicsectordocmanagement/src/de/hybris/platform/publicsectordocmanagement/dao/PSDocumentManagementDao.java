/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.publicsectordocmanagement.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 * Interface class for Document Management DAO.
 */
public interface PSDocumentManagementDao extends Dao
{
	/**
	 * Retrieves all Active Public Sector Documents by Customer.
	 *
	 * @param customerPk
	 * @return List<PSDocumentModel
	 */
	public List<PSDocumentModel> findActivePSDocumentModelByCustomer(final String customerPk);

	/**
	 * Retrieves all Expired Public Sector Documents by Customer.
	 *
	 * @param customerPk
	 * @return List<PSDocumentModel
	 */
	public List<PSDocumentModel> findExpiredPSDocumentModelByCustomer(final String customerPk);

	/**
	 * Retrieves document for the given secure path url ID.
	 *
	 * @param securePathURLId
	 * @return PSDocumentModel
	 */
	public PSDocumentModel findDocumentByDocumentSecurePathUrlId(final String securePathURLId);

	/**
	 * Retrieves Public Sector Document using document code.
	 *
	 * @param documentId
	 *           String The document id.
	 * @return PSDocumentModel
	 */
	public PSDocumentModel findDocumentByDocId(final String documentId);

	/**
	 * Retrieves Public Sector Order using order code.
	 *
	 * @param orderCode
	 *           String The order code.
	 * @return OrderModel
	 */
	public OrderModel findOrderbyCode(final String orderCode);

	List<PSDocumentModel> getDocumentsForUserRelationshipsByStatus(UserModel user, ComposedTypeModel serviceProductComposedType,
			boolean expired);

	/**
	 * Retrieves users relationships documents
	 *
	 * @param user
	 * @param serviceProductComposedType
	 * @return PSDocumentModel
	 */
	List<PSDocumentModel> getDocumentsForUserRelationships(UserModel user, ComposedTypeModel serviceProductComposedType);
}
