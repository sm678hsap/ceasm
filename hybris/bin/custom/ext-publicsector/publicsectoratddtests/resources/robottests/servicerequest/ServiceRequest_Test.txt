*** Settings ***
Test Setup        ServiceRequest_Keywords.import sampledata
Resource		  		impex/Impex_Keywords.txt
Resource          commerce/CommerceServices_Keywords.txt
Resource          yformServices/YForm_Keywords.txt
Resource          publicsector/ServiceRequest_Keywords.txt



*** Test Cases ***
Test_Create_Service_Request
	[Documentation]    Test to create a ServiceRequest after the order is placed.
	create customer "test@test.com" with currency "USD"
	login customer with id "test@test.com"
	verify that service product "report-graffiti" has y form definition data
	add product "report-graffiti" to cart
	${sessionCart}=    get session cart
	${refId}=	 Catenate    ${sessionCart.code}_0
	create y form data by "publicsector" and "Report-Graffiti-Form" and "55ba893a657df5595c228bc60c0a195baa5e8be6" and "${refId}" and "content"
	verify y form data "55ba893a657df5595c228bc60c0a195baa5e8be6" exists with given values "${refId}" and "content"
   verify the service request creation
    
