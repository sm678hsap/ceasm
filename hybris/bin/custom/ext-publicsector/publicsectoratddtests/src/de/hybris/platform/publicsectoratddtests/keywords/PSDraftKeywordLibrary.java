/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectoratddtests.keywords;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.publicsectorfacades.order.PSCartFacade;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * PSDraftKeywordLibrary
 *
 */
public class PSDraftKeywordLibrary extends AbstractKeywordLibrary
{
	public static final String SESSION_CART_PARAMETER_NAME = "cart";

	@Autowired
	private PSCartFacade psCartFacade;


	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>Set return URL in cart</i>
	 * <p>
	 *
	 * @param returnUrl
	 *           the URL used for restoring cart
	 */
	public void updateReturnUrlInCart(final String returnUrl)
	{
		psCartFacade.setDraftCartDetails(returnUrl);
	}
}
