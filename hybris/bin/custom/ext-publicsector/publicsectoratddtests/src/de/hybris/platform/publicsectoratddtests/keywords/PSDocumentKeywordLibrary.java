/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectoratddtests.keywords;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.publicsectordocmanagement.data.PSDocumentData;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentModel;
import de.hybris.platform.publicsectordocmanagement.model.PSDocumentThumbnailModel;
import de.hybris.platform.publicsectorfacades.docmanagement.PSDocumentManagementFacade;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * PSDocumentKeywordLibrary class
 */
public class PSDocumentKeywordLibrary extends AbstractKeywordLibrary
{
	static final String MODULO = Config.getParameter("document.modulo");
	static final String ROOTDIR = Config.getParameter("document.root.folder");
	static final String BYORDER = Config.getParameter("document.byOrder.folder");
	static final String INBOUND = Config.getParameter("document.inbound.folder");

	@Autowired
	private UserService userService;

	@Autowired
	private PSDocumentManagementFacade docFacade;



	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify customer with id exist</i>
	 * <p>
	 *
	 * @param customer
	 */
	public void verifyCustomerWithIdExist(final String customer)
	{
		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customer);
		Assert.assertNotNull(customerModel);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify customer has documents</i>
	 * <p>
	 *
	 * @param customer
	 */
	public void verifyCustomerHasDocuments(final String customer)
	{
		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customer);
		Assert.assertTrue(CollectionUtils.isNotEmpty(customerModel.getPsDocument()));
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify document exists</i>
	 * <p>
	 *
	 * @param customer
	 * @param orderCode
	 * @param fileName
	 */
	public void verifyDocumentExists(final String customer, final String orderCode, final String fileName) throws IOException
	{
		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customer);

		Assert.assertNotNull(customerModel);
		Assert.assertTrue(CollectionUtils.isNotEmpty(customerModel.getPsDocument()));

		final StringBuilder filePath = getDocumentPath(orderCode, customerModel);
		filePath.append(fileName);

		Assert.assertTrue(new File(filePath.toString()).exists());
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify thumbnail document exists</i>
	 * <p>
	 *
	 * @param customer
	 * @param orderCode
	 * @param fileName
	 */
	public void verifyThumbnailDocumentExists(final String customer, final String orderCode, final String fileName)
			throws IOException
	{
		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customer);

		Assert.assertNotNull(customerModel);
		Assert.assertTrue(CollectionUtils.isNotEmpty(customerModel.getPsDocument()));

		final StringBuilder filePath = getThumbnailDocumentPath(orderCode, customerModel);
		filePath.append(fileName);
		Assert.assertTrue(new File(filePath.toString()).exists());
	}

	/**
	 * Method to create thumbnail document path for order
	 *
	 * @param orderCode
	 * @param customerModel
	 * @return filePath
	 */
	private StringBuilder getThumbnailDocumentPath(final String orderCode, final UserModel customerModel)
	{
		final StringBuilder filePath = getDocumentPath(orderCode, customerModel);
		// Append thumbnail to existing file path
		return filePath.append("thumbnail").append(File.separator);
	}

	/**
	 * Method to create document path for order
	 *
	 * @param orderCode
	 * @param customerModel
	 * @return filePath
	 */
	private StringBuilder getDocumentPath(final String orderCode, final UserModel customerModel)
	{
		// Create document path here.
		final int customerPK = customerModel.getPk().getLong().intValue();
		final int modDir = customerPK % Integer.parseInt(MODULO);

		final StringBuilder filePath = new StringBuilder();
		// Create root dir for the user
		filePath.append(ROOTDIR).append(File.separator).append(Math.abs(modDir)).append(File.separator)
				.append(customerModel.getPk().toString());
		// Create order dir on top of root dir
		filePath.append(BYORDER).append(File.separator).append(orderCode).append(INBOUND).append(File.separator);
		return filePath;
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify document has thumbnail</i>
	 * <p>
	 *
	 * @param customer
	 * @param orderCode
	 * @param fileName
	 */
	public void verifyDocumentHasThumbnail(final String customer, final String fileName)
	{
		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customer);
		final Collection<PSDocumentModel> documents = customerModel.getPsDocument();

		Assert.assertTrue(CollectionUtils.isNotEmpty(documents));
		final PSDocumentModel document = getPSDocumentModel(documents);

		Assert.assertNotNull(document);
		Assert.assertNotNull(document.getDocumentThumbnail()); //NOSONAR
		Assert.assertEquals(document.getFileName(), fileName);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify thumbnail is missing</i>
	 * <p>
	 *
	 * @param customer
	 */
	public void verifyThumbnailIsMissing(final String customer)
	{
		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customer);
		final Collection<PSDocumentModel> documents = customerModel.getPsDocument();

		Assert.assertTrue(CollectionUtils.isNotEmpty(documents));
		final PSDocumentModel document = getPSDocumentModel(documents);

		Assert.assertNotNull(document);
		Assert.assertNull(document.getDocumentThumbnail()); //NOSONAR
	}

	/**
	 * Method to return PSDocument model from a collection of models
	 *
	 * @param documents
	 * @return PSDocumentModel
	 */
	private PSDocumentModel getPSDocumentModel(final Collection<PSDocumentModel> documents)
	{
		for (final PSDocumentModel document : documents)
		{
			if (!(document instanceof PSDocumentThumbnailModel))
			{
				return document;
			}
		}
		return null;
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify document is expired</i>
	 * <p>
	 *
	 * @param customer
	 * @param orderCode
	 * @param fileName
	 */
	public void verifyDocumentIsExpired(final String customer, final String orderCode, final String fileName)
	{
		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customer);
		Assert.assertNotNull(customerModel);
		final PSDocumentModel document = getPSDocumentModelForOrderWithFileName(customerModel, orderCode, fileName);

		Assert.assertNotNull(document);
		Assert.assertTrue(
				(document.getExpiresOn().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).isBefore(LocalDate.now())); //NOSONAR
	}

	/**
	 * Method to get document for the order with filename.
	 *
	 * @param customer
	 * @param orderCode
	 * @param fileName
	 * @return PSDocumentModel
	 */
	private PSDocumentModel getPSDocumentModelForOrderWithFileName(final CustomerModel customer, final String orderCode,
			final String fileName)
	{
		final Collection<PSDocumentModel> documents = customer.getPsDocument();
		if (CollectionUtils.isNotEmpty(documents))
		{
			for (final PSDocumentModel document : documents)
			{
				if (document.getOrder().getCode().equals(orderCode) && document.getFileName().equals(fileName))
				{
					return document;
				}
			}
		}
		return null;
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>fetches users relationships docs </i>
	 * <p>
	 *
	 * @param sourceUserEmail
	 * @param expectedCount
	 */
	public void verifyRetrievingUsersRelationshipsDocumentsFor(final String sourceUserEmail, final int expectedCount)
	{
		final List<PSDocumentData> docs = docFacade.getDocumentsForUserRelationshipsByStatus(sourceUserEmail, false);
		Assert.assertEquals(expectedCount, docs.size());
	}

}
