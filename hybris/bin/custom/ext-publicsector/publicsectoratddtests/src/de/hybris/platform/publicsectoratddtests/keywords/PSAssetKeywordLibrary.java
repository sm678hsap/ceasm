/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectoratddtests.keywords;

import static org.junit.Assert.fail;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.commercefacades.asset.data.PSAssetData;
import de.hybris.platform.publicsectorfacades.asset.PSAssetFacade;

import java.util.List;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * PSAssetKeywordLibrary
 *
 */
public class PSAssetKeywordLibrary extends AbstractKeywordLibrary
{
	@Autowired
	private PSAssetFacade psAssetFacade;


	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>Verify Assets For User</i>
	 * <p>
	 *
	 * @param customerId
	 * @param expectedCount
	 */
	public void verifyAssetsForUser(final String customerId, final int expectedCount)
	{

		final List<PSAssetData> assets = psAssetFacade.getAssetsForUser(customerId);

		if (assets != null)
		{
			Assert.assertEquals(expectedCount, assets.size());
		}
		else
		{
			fail("Assets count do not match for user: " + customerId);
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>get asset by code</i>
	 * <p>
	 *
	 * @param code
	 */
	public void getAssetByCode(final String code)
	{

		final PSAssetData assetData = psAssetFacade.getAssetDetailsByCode(code);

		Assert.assertNotNull(assetData);
	}


	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>Get Asset Attribute Details</i>
	 * <p>
	 *
	 * @param code
	 * @param assetType
	 */
	public void getAssetAttributeDetails(final String code, final String assetType)
	{

		final String assetAttributesTemplate = psAssetFacade.getAssetAttributeDetails(code, assetType);

		Assert.assertNotNull(assetAttributesTemplate);
	}
}
