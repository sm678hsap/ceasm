/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectoratddtests.keywords;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.publicsectorfacades.order.PSCartFacade;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.form.YFormService;
import de.hybris.platform.xyformsservices.model.YFormDefinitionModel;

import org.springframework.beans.factory.annotation.Autowired;


public class PSRestoreDraftKeywordsLibrary extends AbstractKeywordLibrary
{

	@Autowired
	private PSCartFacade psCartFacade;

	@Autowired
	private CartService cartService;

	@Autowired
	private YFormService yformservice;

	@Autowired
	private BaseSiteService baseSiteService;

	/**
	 * Keyword implementation for retrieve saved cart with cart code
	 *
	 * @param cartCode
	 *           the cart code to be retrieved
	 * @return a cart data
	 * @throws CommerceSaveCartException
	 */
	public CartData restoreSavedDraftWithCode(final String cartCode) throws CommerceSaveCartException
	{
		return psCartFacade.restoreCart(cartCode);
	}

	/**
	 * Keyword implementation for updating yform version
	 *
	 * @throws YFormServiceException
	 */
	public void updateYformDefinitionVersion() throws YFormServiceException
	{
		final CartModel cart = cartService.getSessionCart();
		final YFormDefinitionModel yformDefinition = cart.getEntries().get(0).getYFormData().get(0).getFormDefinition();

		yformservice.createYFormDefinition(yformDefinition.getApplicationId(), yformDefinition.getFormId(), "title", "description",
				"content", "documentId");

		psCartFacade.updateCartOnYFormVersionUpdate();
	}

	/**
	 * Keyword implementation for retrieving saved draft for guest user
	 *
	 * @param guestEmailCode
	 * @param guestCartCode
	 * @return CartData
	 * @throws CommerceCartRestorationException
	 */
	public CartData retrieveSavedDraftForGuestUserForEmailAndCode(final String guestEmailCode, final String guestCartCode,
			final String site) throws CommerceCartRestorationException
	{
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(site), true);
		return psCartFacade.restoreCartForDraftNumberAndEmailAddress(guestCartCode, guestEmailCode);
	}

	/**
	 * Keyword implementation for setting base site in session.
	 *
	 * @param site
	 */
	public void setBasesiteInSession(final String site)
	{
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(site), true);
	}
}
