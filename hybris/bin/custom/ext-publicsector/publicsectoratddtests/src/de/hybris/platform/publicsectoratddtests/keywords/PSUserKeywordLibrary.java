/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectoratddtests.keywords;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.billfacades.bill.data.PSBillPaymentData;
import de.hybris.platform.billservices.enums.BillPaymentStatus;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.publicsectorfacades.customer.PSCustomerFacade;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * PS User Keyword Library
 */
public class PSUserKeywordLibrary extends AbstractKeywordLibrary
{
	@Autowired
	private ModelService modelService;

	@Autowired
	private UserService userService;

	@Autowired
	private PSCustomerFacade psCustomerfacade;

	/**
	 *
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>update given customer with name</i>
	 * <p>
	 *
	 * @param userUID
	 * @param uesrName
	 * @return CustomerModel
	 */
	public CustomerModel updateGivenCustomerWithName(final String userUID, final String uesrName)
	{
		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(userUID);
		customerModel.setName(uesrName);
		modelService.save(customerModel);
		return customerModel;
	}


	/**
	 *
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>get customer with UID</i>
	 * <p>
	 *
	 * @param userUID
	 * @return CustomerModel
	 */
	public CustomerModel getCustomerWithUID(final String userUID)
	{
		return (CustomerModel) userService.getUserForUID(userUID);
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>fetches users relationships bills </i>
	 * <p>
	 *
	 * @param sourceUserEmail
	 * @param expectedCount
	 */
	public void verifyRetrievingUsersRelationshipsBillsFor(final String sourceUserEmail, final int expectedCount)
	{
		final List<BillPaymentStatus> statuses = new ArrayList<>();
		statuses.add(BillPaymentStatus.UNPAID);
		final List<PSBillPaymentData> bills = psCustomerfacade.getBillsForUserRelationshipsByStatus(sourceUserEmail, statuses);
		Assert.assertEquals(expectedCount, bills.size());
	}

}
