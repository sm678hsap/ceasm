/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectoratddtests.keywords;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.publicsectorfacades.product.PSProductFacade;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.xyformscommercefacades.strategies.impl.GetProductYFormDefinitionsStrategy;
import de.hybris.platform.xyformsfacades.data.YFormDefinitionData;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * ServiceRequestKeywordLibrary
 *
 */
public class PSServiceRequestKeywordLibrary extends AbstractKeywordLibrary
{
	private static final Logger LOG = LoggerFactory.getLogger(PSServiceRequestKeywordLibrary.class);


	@Autowired
	private CartService cartService;

	@Autowired
	private CommerceCheckoutService commerceCheckoutService;

	@Autowired
	private Converter<OrderModel, OrderData> orderConverter;

	@Autowired
	private GetProductYFormDefinitionsStrategy getProductYFormDefinitionsStrategy;

	@Autowired
	private PSProductFacade psProductFacade;

	/**
	 *
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify service request</i>
	 * <p>
	 * *
	 *
	 */
	public void verifyServiceRequest()
	{
		try
		{
			final CartModel cartModel = cartService.getSessionCart();

			final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cartModel);
			final CommerceOrderResult commerceOrderResult = commerceCheckoutService.placeOrder(parameter);
			final OrderData ordrData = orderConverter.convert(commerceOrderResult.getOrder());

			assertNotNull(ordrData);

			// Getting Service Product Order Entry
			final OrderEntryData orderEntryData = ordrData.getEntries().get(0);
			assertNotNull(orderEntryData.getFormDataData());
		}
		catch (final InvalidCartException e)
		{
			LOG.error("Cart validation failed it is invalid cart", e);
			fail("Cart validation failed it is invalid cart");
		}
	}

	/**
	 *
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify service product has y form definition data</i>
	 * <p>
	 *
	 * @param productCode
	 *
	 */
	public void verifyServiceProductHasYFormDefinitionData(final String productCode)
	{
		try
		{
			if (psProductFacade.isServiceProduct(productCode))
			{
				final List<YFormDefinitionData> yFormDefinitionDataList = getProductYFormDefinitionsStrategy.execute(productCode);

				if (CollectionUtils.isEmpty(yFormDefinitionDataList))
				{
					Assert.fail("Service Product [" + productCode + "] has no YFomDefinitions assigned.");
				}
			}
			else
			{
				Assert.fail("Product [" + productCode + "] is not a Service Product.");
			}
		}
		catch (final YFormServiceException e)
		{
			LOG.error("Could not retrieve YFormDefinitions for Product[code=\"" + productCode + "\"]", e);
			fail(e.getMessage());
		}
	}
}
