/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.publicsectoratddtests.keywords;

import static org.junit.Assert.fail;

import de.hybris.platform.atddengine.keywords.AbstractKeywordLibrary;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.publicsectorfacades.bundle.selection.BundleSelectionFacade;
import de.hybris.platform.publicsectorservices.bundle.selection.BundleSelectionService;

import java.util.List;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Robot Test Keywords class for Service Product Bundle.
 */
public class PSBundleTestKeywordLibrary extends AbstractKeywordLibrary
{

	@Autowired
	private BundleSelectionService bundleSelectionService;

	@Autowired
	private BundleSelectionFacade bundleSelectionFacade;

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify has bundle addons</i>
	 * <p>
	 *
	 * @param productCode
	 *           the code of the product to check.
	 */
	public void verifyHasBundleAddOns(final String productCode)
	{
		final Boolean bol = bundleSelectionService.hasBundleAddons(productCode);
		Assert.assertTrue(bol.booleanValue());
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>get parent bundle template</i>
	 * <p>
	 *
	 * @param productCode
	 *           the code of the product to with bundle.
	 */
	public String getParentBundleTemplate(final String productCode)
	{
		final BundleTemplateModel bundle = bundleSelectionService.getParentBundleTemplateByProduct(productCode);
		if (bundle != null)
		{
			return bundle.getId();
		}
		return null;
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify number of cart entries</i>
	 * <p>
	 *
	 * @param sessionCart
	 *           the session cart.
	 * @param expectedCartCount
	 *           the expected number of child carts
	 */
	public void verifyNumberOfCartEntries(final CartModel sessionCart, final int expectedCartCount)
	{
		if (sessionCart.getEntries() != null)
		{
			Assert.assertEquals(expectedCartCount, sessionCart.getEntries().size());
		}
		else
		{
			fail("No cart entries on cart: " + sessionCart.getCode());
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify number of bundles not in cart</i>
	 * <p>
	 *
	 * @param productCode
	 *           the service to add to cart.
	 * @param expectedCartCount
	 *           the expected number of child carts
	 */
	public void verifyNumberOfBundlesNotInCart(final String productCode, final int expectedCartCount)
	{
		final List<Integer> bundleIndexList = bundleSelectionFacade.getChildBundlesNotInCart(productCode);
		if (bundleIndexList != null && !bundleIndexList.isEmpty())
		{
			Assert.assertEquals(expectedCartCount, bundleIndexList.size());
		}
		else
		{
			fail("No child bundles exist on product: " + productCode);
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify number of child bundles of product</i>
	 * <p>
	 *
	 * @param productCode
	 *           the service to add to cart.
	 * @param expectedChildCount
	 *           the expected number of children bundles
	 */
	public void verifyNumberOfChildBundlesOfProduct(final String productCode, final int expectedChildCount)
	{
		final List<BundleTemplateData> childBundleList = bundleSelectionFacade.getChildrenTemplates(productCode);
		if (childBundleList != null && !childBundleList.isEmpty())
		{
			Assert.assertEquals(expectedChildCount, childBundleList.size());
		}
		else
		{
			fail("No child bundles exist on product: " + productCode);
		}
	}

	/**
	 * Java implementation of the robot keyword <br>
	 * <p>
	 * <i>verify number of disabled products</i>
	 * <p>
	 *
	 * @param productCode
	 *           the service to add to cart.
	 * @param expectedDisabledProdsCount
	 *           the expected number of disabled products
	 */
	public void verifyNumberOfDisabledProducts(final String productCode, final int expectedDisabledProdsCount)
	{
		final List<ProductData> disabledProductList = bundleSelectionFacade.returnDisabledProducts(productCode);
		if (disabledProductList != null && !disabledProductList.isEmpty())
		{
			Assert.assertEquals(expectedDisabledProdsCount, disabledProductList.size());
		}
		else
		{
			fail("No disable products for product: " + productCode);
		}
	}
}
