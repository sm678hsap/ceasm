#!/bin/bash
# Hybris 6.4 production ZIPs for some reason seems to break Orbeon forms:
# INFO   | jvm 1    | main    | 2017/10/10 11:41:06.045 |  de.hybris.platform.xyformsservices.exception.YFormServiceException: YFormData with applicationId[publicsector], formId[Report-Graffiti-Form] , refId[00000000_0], type[DATA]  does not exist
# even when hybris/bin/custom/orbeon/ was zipped as well
# So taking the step of zipping everything up
set -e

OUTPUT_ZIP_DIR="/data/deploy/zips"

rm -rf $OUTPUT_ZIP_DIR
mkdir -p $OUTPUT_ZIP_DIR

cd /data/hybris/bin/platform/
. ./setantenv.sh
ant clean all
cd /data
zip -rq $OUTPUT_ZIP_DIR/hybris.zip ./

