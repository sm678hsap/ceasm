#!/bin/bash
# Builds Hybris
# Expects to be in the root directory of the hybris code git repo

#orbeon_location="https://s3-ap-southeast-2.amazonaws.com/hybris-software-repo/orbeon/orbeon-hcea-6.4.tar.gz"
orbeon_location="https://s3-ap-southeast-2.amazonaws.com/hybris-software-repo/orbeon/orbeon-hcea-6.4-updated.tar.gz"

if [ ! -d hybris ]; then
    echo "Error, expected to be the root directory of the git repo"
    exit 2
fi

# Exit on any errors:
set -e

# Add amazoncloud extension (todo: ask devs to add this properly)
sed -i -e 's/\(.*"backoffice",.*\)/\1 "amazoncloud",/' /data/installer/recipes/publicsector/build.gradle
# Change directory path
sed -i -e 's/\.\.\/orbeon-module/orbeonservices/' /data/installer/recipes/publicsector/build.gradle

# Unzip the orbeon tarball to /data/
mkdir -p /data/orbeonservices/
cd /data/orbeonservices/
curl checkip.amazonaws.com
wget "$orbeon_location"
tar xvf *.tar.gz


# Install publicsector recipe
cd /data/installer
./install.sh -r publicsector

