#!/bin/bash
# Builds Hybris
# Expects to be in the root directory of the hybris code git repo

if [ ! -d hybris ]; then
    echo "Error, expected to be the root directory of the git repo"
    exit 2
fi

# Exit on any errors:
set -e

# Add amazoncloud extension (todo: ask devs to add this properly)
sed -i 's/.*publicsectorbackoffice.*/\"amazoncloud\",&/' /data/installer/recipes/publicsector/build.gradle
#sed -i -e 's/\(.*"backoffice",.*\)/\1 "amazoncloud",/' /data/installer/recipes/publicsector/build.gradle

echo "Publicsector reciepe run"
# Install publicsector recipe
cd /data/installer
./install.sh -r publicsector

