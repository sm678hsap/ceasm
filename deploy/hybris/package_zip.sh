#!/bin/bash
# Generate hybris production ZIPs into deploy/zips/
set -e

OUTPUT_ZIP_DIR="/data/deploy/zips"

cd /data/hybris/bin/platform/
. ./setantenv.sh
ant clean all production

# The above generates ZIPs in /data/hybris/temp/hybris/hybrisServer/hybrisServer-*.zip
rm -rf $OUTPUT_ZIP_DIR
mkdir -p $OUTPUT_ZIP_DIR
mv -v /data/hybris/temp/hybris/hybrisServer/hybrisServer-*.zip $OUTPUT_ZIP_DIR

