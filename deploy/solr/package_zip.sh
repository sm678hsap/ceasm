#!/bin/bash
# Generate a ZIP for Solr from Hybris's included Solr

# Variables:
HYBRIS_SOLR_BASE="/data/hybris/bin/ext-commerce/solrserver/resources/solr"
OUTPUT_ZIP_DIR="/data/deploy/zips"
OUTPUT_ZIP="$OUTPUT_ZIP_DIR/solr.zip"

# Exit on any errors:
set -e

# Make zip output dir
mkdir -p $OUTPUT_ZIP_DIR

# Delete zip if it already exists (should never be the case BTW)
rm -vf $OUTPUT_ZIP

# Go to hybris' included solr and zip it up
cd $HYBRIS_SOLR_BASE
zip -r $OUTPUT_ZIP ./

echo "Output zip directory is in $OUTPUT_ZIP_DIR"
echo "Solr zip is $OUTPUT_ZIP"
