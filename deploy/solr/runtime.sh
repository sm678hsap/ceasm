#!/bin/bash
# Triggered only when an instance boots up (or by CodeDeploy)

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ansible-playbook $SCRIPT_DIR/runtime.yml
