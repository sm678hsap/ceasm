#!/bin/bash
# "ant sonar" in Hybris does not seem to work with the latest version of SonarJava
# SonarJava, from version 4.12, now makes the sonar.java.binaries mandatory for every
# single java project, but Hybris's "ant sonar" does not do this.
# In addition, I noticed many Hybris sites are missing major CSS checks which "ant sonar"
# seems to skip.
# Also it works on exclusions rather than inclusions, so including the list of OOTB extensions
# to skip is a pain
#
# The ant XML isn't too complicated, so can be replicated and improved on with this script
# that generates a sonar-project.properties file which can be fed to sonar-scanner
# Sonar Scanner is now (2017) recommended as the default scanner:
# https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner


output_file="/tmp/sonar-project.properties"

# This script expects to be run from the repo root directory:
if [ ! -d hybris ] ; then
    echo "
        This script should be run as:
        ./deploy/sonar/generate_project.sh
        so cd into the repo root directory
    "
    exit 1
fi

# Directories containing the extensions to scan
# Every extensionb has an extensioninfo.xml file, so check the
# custom directory for that
hybris_extensions=$(find hybris/bin/custom -name extensioninfo.xml | xargs  -L1 dirname)


cat << EOF > $output_file

sonar.java.source=1.8
sonar.sourceEncoding=UTF-8

# Jacoco coverage (generated with jacocoagent.jar in local.properties)
sonar.jacoco.reportPaths=/data/hybris/log/jacoco.exec
sonar.junit.reportPaths=/data/hybris/log/junit

# Example config to test a sonar scan manually:
#sonar.host.url=https://sonar.hpsa.odxc.info:9000
#sonar.login=<a token from a user>
#sonar.projectDescription=Test, delete me
#sonar.projectKey=TEST
#sonar.projectName=Test-deleteme
#sonar.exclusions=**/bootstrap/**,**/responsive/lib/**
#sonar.projectVersion=0.1

EOF

# See https://docs.sonarqube.org/display/PLUG/Java+Plugin+and+Bytecode
# Get a list of OOTB classes to append to the binaries
# to avoid "The following classes needed for analysis were missing" warnings
# command separated without a comma at the end
# Note that checking binaries is VERY VERY CPU intensive. You can LITERALLY
# bring down the run time from 30 minutes to 3 minutes JUST by uncommenting
# the following two lines (and nothing else), at the expense of missing a
# few checks
ootb_classes=$(find `pwd`/hybris/bin/ -type d -name classes ! -path '*custom*' | tr '\n' ',' | sed 's/,$//')

ootb_jars=$(find `pwd`/hybris/bin/ -name '*.jar' | sort  | grep -v custom | cut -d '/' -f 1-5 | uniq | awk '{print $1 "/**/*.jar"}' | tr '\n' ',' | sed 's/,$//')

# See https://docs.sonarqube.org/display/PLUG/Java+Plugin+and+Bytecode

# This array has the list of extensions
sonar_modules=()

# Outputs the sonar config for a module to the output_file depending
# on which directories exist for the given module_path
# Input: configuration_name module_path list of dirctories to check
function add_config() {
    local configuration_name="$1"
    shift
    local module_path="$1"
    shift
    # Rest of arguments is the directories
    local check_dirs=$@

    local module_sources=()
    # Customize module settings depending on what directories exist
    for dir in $check_dirs; do
        if [ -d $module_path/$dir ]; then
            if [[ "$configuration_name" =~ .*libraries ]]; then
                # check if the directory has any jars
                find $module_path/$dir -type f -name '*.jar' | grep -q ".*"
                local ret=$?
                if [[ "$ret" != 0 ]]; then
                    continue;
                fi
                module_sources+=("$dir/**/*.jar")
            else
                module_sources+=("$dir")
            fi
        fi
    done

    sources_comma_separated=$(IFS=, ; echo "${module_sources[*]}")

    if [[ "$configuration_name" =~ .*binaries ]]; then
        echo "$configuration_name=$sources_comma_separated,$ootb_classes" >> $output_file
    elif [[ "$configuration_name" =~ .*libraries ]]; then
        echo "$configuration_name=$sources_comma_separated,$ootb_jars" >> $output_file
    else
        echo "$configuration_name=$sources_comma_separated" >> $output_file
    fi
}

# Runs a git clean to remove generated code except for
# exceptions in the arguments (like binary files needed by sonar)
# Input: module_path dirs of classes to exclude from clean
function git_clean() {
    local path=$1
    shift
    local exclude_dirs=$@

    git_excludes=""
    for exclude_dir in $exclude_dirs; do
        git_excludes="$git_excludes --exclude $path/$exclude_dir"
    done

    local oldpwd="$PWD"
    cd $path
    # Output in standard error:
    >&2 git clean -dxf $git_excludes
    cd "$oldpwd"
}

# Check each extension:
for module_path in $hybris_extensions; do
    # extension looks like for eg. hybris/bin/custom/,
    module=$(basename $module_path)

    sonar_modules+=("$module")

    add_config "$module.sonar.sources" "$module_path" src web

    add_config "$module.sonar.tests" "$module_path" testsrc

    add_config "$module.sonar.java.binaries" "$module_path" classes bin web/webroot/WEB-INF/classes

    add_config "$module.sonar.java.libraries" "$module_path" lib web/webroot/WEB-INF/lib

    git_clean "$module_path" gensrc classes bin web/webroot/WEB-INF/classes lib web/webroot/WEB-INF/lib

    echo "$module.sonar.projectBaseDir=$module_path" >> $output_file

    # Newline to make it easier to read:
    echo "" >> $output_file
done

# Output list of modules
modules_comma_separated=$(IFS=, ; echo "${sonar_modules[*]}")

echo "sonar.modules=$modules_comma_separated" >> $output_file

cat $output_file
